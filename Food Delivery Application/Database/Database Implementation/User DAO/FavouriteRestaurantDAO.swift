//
//  FavouriteRestaurantDAO.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 15/02/22.
//

import Foundation
import SQLite3

class FavouriteRestaurantDAO : Database,FavouriteRestaurantDAOProtocol{
    
    
    func persistFavouriteRestaurant(userId: Int,restaurantId : Int) throws -> Bool{
        
        var isExecuted = false
        let insertQuery = "INSERT INTO favourite_restaurant(user_id,restaurant_id) VALUES (?,?)"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_int(insertQueryStatement , 1, Int32(userId)) == SQLITE_OK &&
                sqlite3_bind_int(insertQueryStatement , 2, Int32(restaurantId)) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE{
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(insertQueryStatement)
        
        return isExecuted
    }
    
    func deleteFavouriteRestaurant(favouriteRestaurantId : Int) throws -> Bool{
        
        var isExecuted = false
        let Query = "DELETE FROM favourite_restaurant WHERE favourite_restaurant_id = ? ;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        
        guard sqlite3_bind_int(QueryStatement , 1, Int32(favouriteRestaurantId)) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(QueryStatement) == SQLITE_DONE{
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
        sqlite3_finalize(QueryStatement)
        
        return isExecuted
    }
    
    func getFavouriteRestaurantIds(userId : Int) throws -> [Int : Int] {
        
        var favouriteRestaurants : [Int : Int] = [:]
        let query = "SELECT favourite_restaurant_id, restaurant_id FROM favourite_restaurant WHERE user_id = ?;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard sqlite3_bind_int(queryStatement , 1, Int32(userId)) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
            while sqlite3_step(queryStatement) == SQLITE_ROW{
                
                let favouriteRestaurantId =  Int(sqlite3_column_int(queryStatement, 0))

                let restaurantId = Int(sqlite3_column_int(queryStatement, 1))
                
                favouriteRestaurants[favouriteRestaurantId] = restaurantId
            }
        
        sqlite3_finalize(queryStatement)
  
        return favouriteRestaurants
    }
    
    func getFavouriteRestaurants(userId : Int) throws -> [RestaurantContentDetails]{
        
        var restaurantContentDetailsList : [RestaurantContentDetails]  = []
        let query = "SELECT favourite_restaurant.restaurant_id ,restaurant_image , restaurant_name, restaurant_cuisine, restaurant_description ,restaurant_phone_number , restaurant_door_number_and_building_name_and_building_number, restaurant_street_name , restaurant_landmark ,restaurant_locality , restaurant_city , restaurant_state , restaurant_country,restaurant_pincode,restaurant_rating_out_of_5,restaurant_total_number_of_ratings,restaurant_isavailable, restaurant_opens_next_at, restaurant_account_status,restaurant_food_packaging_charges FROM favourite_restaurant INNER JOIN restaurant_accounts ON favourite_restaurant.restaurant_id = restaurant_accounts.restaurant_id where user_id = ?;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard sqlite3_bind_int(queryStatement , 1, Int32(userId)) == SQLITE_OK
            else {
            throw SQLiteError.Bind(message: errorMessage)
            }
        
        while sqlite3_step(queryStatement) == SQLITE_ROW{
            var restaurantContentDetails : RestaurantContentDetails = RestaurantContentDetails()
            var restaurantDetails = RestaurantDetails()
            var restaurantAddress = AddressDetails()
            restaurantContentDetails.restaurantId =   Int(sqlite3_column_int(queryStatement, 0))
            restaurantDetails.restaurantProfileImage =  NSData(bytes: sqlite3_column_blob(queryStatement, 1), length: Int(sqlite3_column_bytes(queryStatement, 1))) as Data
            restaurantDetails.restaurantName = String(cString: sqlite3_column_text(queryStatement, 2))
            restaurantDetails.restaurantCuisine =  String(cString: sqlite3_column_text(queryStatement, 3))
            restaurantDetails.restaurantDescription =  String(cString: sqlite3_column_text(queryStatement,4 ))
            restaurantDetails.restaurantContactNumber =  String(cString: sqlite3_column_text(queryStatement,5 ))
            restaurantContentDetails.restaurantDetails = restaurantDetails
            restaurantAddress.doorNoAndBuildingNameAndBuildingNo = String(cString: sqlite3_column_text(queryStatement,6))
            restaurantAddress.streetName =  String(cString: sqlite3_column_text(queryStatement,7))
            
            restaurantAddress.landmark =  String(cString: sqlite3_column_text(queryStatement,8))
            restaurantAddress.localityName =  String(cString: sqlite3_column_text(queryStatement,9))
            restaurantAddress.city =  String(cString: sqlite3_column_text(queryStatement,10))
            restaurantAddress.state =  String(cString: sqlite3_column_text(queryStatement,11))
            restaurantAddress.country =  String(cString: sqlite3_column_text(queryStatement,12))
            restaurantAddress.pincode =  String(cString: sqlite3_column_text(queryStatement,13))
            restaurantContentDetails.restaurantAddress =  restaurantAddress
            restaurantContentDetails.restaurantStarRating =  String(cString: sqlite3_column_text(queryStatement,14 ))
            restaurantContentDetails.totalNumberOfRating =  Int( sqlite3_column_int(queryStatement,15))
            restaurantContentDetails.restaurantIsAvailable =  Int( sqlite3_column_int(queryStatement,16))
            restaurantContentDetails.restaurantOpensNextAt =  String(cString: sqlite3_column_text(queryStatement,17))
            restaurantContentDetails.restaurantAccountStatus =  Int( sqlite3_column_int(queryStatement,18))
            restaurantContentDetails.restaurantFoodPackagingCharges =  Int( sqlite3_column_int(queryStatement,19))
            restaurantContentDetailsList.append(restaurantContentDetails)
        }
        
            sqlite3_finalize(queryStatement)
        
        return restaurantContentDetailsList
    }
    
}
