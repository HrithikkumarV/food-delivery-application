//
//  UserRegistration.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 09/11/21.
//

import Foundation
import SQLite3
class UserAccountsDAO : Database , UserAccountsDAOProtocols , UserWelcomePageDAOProtocol{
    
    
    
    
    func persistUserAccountCreateAccountDetails(user: UserDetails) throws -> Bool{
      
        var isExecuted = false
        let insertQuery = "INSERT INTO user_accounts(user_name , user_phone_number , user_last_signin , user_joined_at) VALUES ( ? , ? , datetime('now','localtime') , datetime('now','localtime') )"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
            guard sqlite3_bind_text(insertQueryStatement , 1, (user.userName as NSString).utf8String, -1, nil) == SQLITE_OK &&
                    sqlite3_bind_text(insertQueryStatement , 2, (user.userPhoneNumber as NSString).utf8String, -1, nil) == SQLITE_OK
            else {
                        throw SQLiteError.Bind(message: errorMessage)
                    }
        if( sqlite3_step(insertQueryStatement) == SQLITE_DONE){
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
            sqlite3_finalize(insertQueryStatement)
  
        return isExecuted
    }
    
    
    func checkIfPhoneNumberExists(phoneNumber: String) throws -> Bool {
        
        var isExist = false
        let query = "SELECT user_id FROM user_accounts where user_phone_number = ?;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_text(queryStatement , 1, (phoneNumber as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
        if(sqlite3_step(queryStatement) == SQLITE_ROW){
            isExist = true
        }
        
        
            sqlite3_finalize(queryStatement)
       
        return isExist
    }
    
    
    
    
    func getUserId(phoneNumber: String) throws -> Int {
      
        var user_id = 0
        let query = "SELECT user_id FROM user_accounts where user_phone_number = ?;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_text(queryStatement , 1, (phoneNumber as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
        if(sqlite3_step(queryStatement) == SQLITE_ROW){
                user_id = Int(sqlite3_column_int(queryStatement, 0))
            }
        
        
            sqlite3_finalize(queryStatement)
       
        return user_id
    }
    
    
    

    func getUserDetails(userId : Int) throws -> UserDetails{
      
        var userDetails : UserDetails = UserDetails()
        let query = "SELECT user_name , user_phone_number FROM user_accounts where user_id = ?;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard sqlite3_bind_int(queryStatement , 1, Int32(userId)) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        if sqlite3_step(queryStatement) == SQLITE_ROW{
            userDetails.userName = String(cString: sqlite3_column_text(queryStatement, 0))
            userDetails.userPhoneNumber =  String(cString: sqlite3_column_text(queryStatement, 1))
            }
        
            sqlite3_finalize(queryStatement)
       
        return userDetails
    }
    
    func updateUserName(userId : Int, userName :String) throws -> Bool{
        
        var isExecuted =  false
        let query = "update user_accounts SET user_name = ? where user_id = ?"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard  sqlite3_bind_text(queryStatement , 1, (userName as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_int(queryStatement, 2, Int32(userId)) == SQLITE_OK
            else{
                    throw SQLiteError.Bind(message: errorMessage)
                }
            if( sqlite3_step(queryStatement) == SQLITE_DONE){
                isExecuted = true
            }
                else{
                    throw SQLiteError.Step(message: errorMessage)
                }
            sqlite3_finalize(queryStatement)
        
            return isExecuted
    }
    
    func updateUserPhoneNumber(userId : Int, phoneNumber :String) throws -> Bool{
        
            var isExecuted =  false
            let query = "update user_accounts SET user_phone_number = ? where user_id = ?"
            
            let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard  sqlite3_bind_text(queryStatement , 1, (phoneNumber as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_int(queryStatement, 2, Int32(userId)) == SQLITE_OK
            else{
                    throw SQLiteError.Bind(message: errorMessage)
                }
            if( sqlite3_step(queryStatement) == SQLITE_DONE){
                isExecuted = true
            }
                else{
                    throw SQLiteError.Step(message: errorMessage)
                }
            sqlite3_finalize(queryStatement)
        
            return isExecuted
    }
    
    
    
    func updateUserDetails(userId : Int, userDetails : UserDetails) throws -> Bool{
       
            var isExecuted =  false
            let query = "update user_accounts SET user_name = ? , user_phone_number = ? where user_id = ?"
            
            let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard  sqlite3_bind_text(queryStatement , 1, (userDetails.userName as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 1, (userDetails.userPhoneNumber as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_int(queryStatement, 2, Int32(userId)) == SQLITE_OK
            else{
                    throw SQLiteError.Bind(message: errorMessage)
                }
            if( sqlite3_step(queryStatement) == SQLITE_DONE){
                isExecuted = true
            }
                else{
                    throw SQLiteError.Step(message: errorMessage)
                }
            sqlite3_finalize(queryStatement)
        
            return isExecuted
    }
    
    
   
    
    
    func PersistUserAddress(userId: Int, userAddressDetails : UserAddressDetails) throws -> Bool{
        
        var isExecuted = false
        let insertQuery = "INSERT INTO user_addresses(user_id,user_address_id,user_address_tag ,user_door_number_and_building_name_and_building_number, user_street_name, user_locality,user_landmark, user_city, user_state,user_country, user_pincode) VALUES ( ? ,?, ? , ? , ? , ? , ? , ? , ? , ? , ?)"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_int(insertQueryStatement, 1, Int32(userId) ) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 2, ( userAddressDetails.userAddressId as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 3, ( userAddressDetails.addressTag as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 4, (userAddressDetails.addressDetails.doorNoAndBuildingNameAndBuildingNo as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 5, (userAddressDetails.addressDetails.streetName as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 6, (userAddressDetails.addressDetails.localityName as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 7, (userAddressDetails.addressDetails.landmark as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 8, (userAddressDetails.addressDetails.city as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 9, (userAddressDetails.addressDetails.state as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 10, (userAddressDetails.addressDetails.country as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 11, (userAddressDetails.addressDetails.pincode as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE{
            isExecuted = true
        }
        else{
            throw SQLiteError.Step(message: errorMessage)
        }
            sqlite3_finalize(insertQueryStatement)
        
        return isExecuted
    }
    
    func updateUserAddress(userAddressDetails: UserAddressDetails) throws -> Bool{
        
        var addressUpdated  : Bool = false
        let query = "UPDATE  user_addresses SET user_address_tag = ? , user_door_number_and_building_name_and_building_number = ? , user_street_name = ?, user_locality = ? ,user_landmark = ?, user_city = ? , user_state = ? ,user_country = ? , user_pincode = ? where user_address_id = ?"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard
                
            sqlite3_bind_text(queryStatement , 1, ( userAddressDetails.addressTag as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 2, (userAddressDetails.addressDetails.doorNoAndBuildingNameAndBuildingNo as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 3, (userAddressDetails.addressDetails.streetName as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 4, (userAddressDetails.addressDetails.localityName as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 5, (userAddressDetails.addressDetails.landmark as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 6, (userAddressDetails.addressDetails.city as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 7, (userAddressDetails.addressDetails.state as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 8, (userAddressDetails.addressDetails.country as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 9, (userAddressDetails.addressDetails.pincode as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_text(queryStatement , 10, ( userAddressDetails.userAddressId as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        if sqlite3_step(queryStatement) == SQLITE_DONE{
            addressUpdated = true
        }
        else{
            throw SQLiteError.Step(message: errorMessage)
        }
            sqlite3_finalize(queryStatement)
        
        return addressUpdated
    }
    
    func deleteUserAddress(userAddressId :String) throws -> Bool{
        
        var isExecuted : Bool = false
        let Query = "UPDATE user_addresses SET user_address_status = 0 WHERE user_address_id = ?;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        guard
                
            sqlite3_bind_text(QueryStatement , 1, ( userAddressId as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
            throw SQLiteError.Bind(message: errorMessage)
        }
        if sqlite3_step(QueryStatement) == SQLITE_DONE{
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(QueryStatement)
        
        return isExecuted
    }
    
    func getUserAddresses(userId: Int) throws -> [UserAddressDetails] {
        
        var userAddresses  : [UserAddressDetails] = []
       
        let query = "SELECT user_address_id, user_address_tag ,user_door_number_and_building_name_and_building_number, user_street_name, user_locality,user_landmark, user_city, user_state,user_country, user_pincode FROM user_addresses WHERE user_id = ? and user_address_status = 1;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_int(queryStatement, 1, Int32(userId)) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
            while(sqlite3_step(queryStatement) == SQLITE_ROW){
                var userAddress : UserAddressDetails = UserAddressDetails()
                var address = AddressDetails()
                userAddress.userAddressId = String(cString: sqlite3_column_text(queryStatement, 0))
                userAddress.addressTag = String(cString: sqlite3_column_text(queryStatement, 1))
                address.doorNoAndBuildingNameAndBuildingNo = String(cString: sqlite3_column_text(queryStatement, 2))
                address.streetName =  String(cString: sqlite3_column_text(queryStatement, 3))
                address.localityName =  String(cString: sqlite3_column_text(queryStatement, 4))
                address.landmark =  String(cString: sqlite3_column_text(queryStatement, 5))
                address.city =  String(cString: sqlite3_column_text(queryStatement, 6))
                address.state =  String(cString: sqlite3_column_text(queryStatement, 7))
                address.country =  String(cString: sqlite3_column_text(queryStatement, 8))
                address.pincode =  String(cString: sqlite3_column_text(queryStatement, 9))
                userAddress.addressDetails = address
                userAddresses.append(userAddress)
            }
        
        
            sqlite3_finalize(queryStatement)
        
        
        return userAddresses
    }
    
    func getUserAddress(userAddressId: String) throws -> UserAddressDetails {
        
        let query = "SELECT user_address_id, user_address_tag ,user_door_number_and_building_name_and_building_number, user_street_name, user_locality,user_landmark, user_city, user_state,user_country, user_pincode FROM user_addresses WHERE user_address_id = ?;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard  sqlite3_bind_text(queryStatement , 1, ( userAddressId as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        var userAddressDetails = UserAddressDetails()
        
            while(sqlite3_step(queryStatement) == SQLITE_ROW){
                userAddressDetails.userAddressId = String(cString: sqlite3_column_text(queryStatement, 0))
                userAddressDetails.addressTag = String(cString: sqlite3_column_text(queryStatement, 1))
                print(String(cString: sqlite3_column_text(queryStatement, 1)))
                var address = AddressDetails()
                address.doorNoAndBuildingNameAndBuildingNo = String(cString: sqlite3_column_text(queryStatement, 2))
                address.streetName =  String(cString: sqlite3_column_text(queryStatement, 3))
                address.localityName =  String(cString: sqlite3_column_text(queryStatement, 4))
                address.landmark =  String(cString: sqlite3_column_text(queryStatement, 5))
                address.city =  String(cString: sqlite3_column_text(queryStatement, 6))
                address.state =  String(cString: sqlite3_column_text(queryStatement, 7))
                address.country =  String(cString: sqlite3_column_text(queryStatement, 8))
                address.pincode =  String(cString: sqlite3_column_text(queryStatement, 9))
                userAddressDetails.addressDetails = address
            }
        
            sqlite3_finalize(queryStatement)
        
        return userAddressDetails
    }
    
    
    
    
    
   
    
    
    func persistUserId(userId: Int) throws -> Bool{
        
        var isExecuted = false
        let insertQuery = "INSERT INTO local_persist_user_Id(user_id) VALUES (?)"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_int(insertQueryStatement , 1, Int32(userId)) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE{
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(insertQueryStatement)
        
        return isExecuted
    }
    
    func deleteUserId() throws -> Bool {
        
        var isExecuted = false
        let Query = "DELETE FROM local_persist_user_Id;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        
        if sqlite3_step(QueryStatement) == SQLITE_DONE{
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
        sqlite3_finalize(QueryStatement)
        
        return isExecuted
    }
    
    func getUserID() throws -> Int {
        
        var userId = 0
        let query = "SELECT user_id FROM local_persist_user_Id;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            while sqlite3_step(queryStatement) == SQLITE_ROW{
               userId =  Int(sqlite3_column_int(queryStatement, 0))
            }
        
        sqlite3_finalize(queryStatement)
        
        return userId
    }
    
    
    
    
    func PersistLocalPersistedUserAddress(userAddressDetails : UserAddressDetails) throws -> Bool{
        
        var isExecuted = false
        let insertQuery = "INSERT INTO local_persisted_previous_used_user_address( user_address_id,user_address_tag ,user_door_number_and_building_name_and_building_number, user_street_name, user_locality,user_landmark, user_city, user_state,user_country, user_pincode) VALUES ( ? , ? , ? , ? , ?,?,?,?,?,?)"
        
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_text(insertQueryStatement , 1, ( userAddressDetails.userAddressId as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 2, ( userAddressDetails.addressTag as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 3, (userAddressDetails.addressDetails.doorNoAndBuildingNameAndBuildingNo as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 4, (userAddressDetails.addressDetails.streetName as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 5, (userAddressDetails.addressDetails.localityName as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 6, (userAddressDetails.addressDetails.landmark as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 7, (userAddressDetails.addressDetails.city as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 8, (userAddressDetails.addressDetails.state as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 9, (userAddressDetails.addressDetails.country as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 10, (userAddressDetails.addressDetails.pincode as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE{
            isExecuted = true
        }
        else{
            throw SQLiteError.Step(message: errorMessage)
        }
        
        sqlite3_finalize(insertQueryStatement)
        
        return isExecuted
    }

    
    func getLocalPersistedUserAddress() throws -> UserAddressDetails {
        
        
        let query = "SELECT user_address_id, user_address_tag ,user_door_number_and_building_name_and_building_number, user_street_name, user_locality,user_landmark, user_city, user_state,user_country, user_pincode FROM local_persisted_previous_used_user_address WHERE previous_used_user_address_id = 1 ;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
            if(sqlite3_step(queryStatement) == SQLITE_ROW){
                var userAddress  = UserAddressDetails()
                userAddress.userAddressId = String(cString: sqlite3_column_text(queryStatement, 0))
                userAddress.addressTag = String(cString: sqlite3_column_text(queryStatement, 1))
                var address = AddressDetails()
                address.doorNoAndBuildingNameAndBuildingNo = String(cString: sqlite3_column_text(queryStatement, 2))
                address.streetName =  String(cString: sqlite3_column_text(queryStatement, 3))
                address.localityName =  String(cString: sqlite3_column_text(queryStatement, 4))
                address.landmark =  String(cString: sqlite3_column_text(queryStatement, 5))
                address.city =  String(cString: sqlite3_column_text(queryStatement, 6))
                address.state =  String(cString: sqlite3_column_text(queryStatement, 7))
                address.country =  String(cString: sqlite3_column_text(queryStatement, 8))
                address.pincode =  String(cString: sqlite3_column_text(queryStatement, 9))
                userAddress.addressDetails = address
                
                return userAddress
            }
        
    
        sqlite3_finalize(queryStatement)
        
        return UserAddressDetails()
    }
    
    func deleteLocalPersistedUserAddress() throws -> Bool {
        
        var isExecuted = false
        let Query = "DELETE FROM local_persisted_previous_used_user_address;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        
        if sqlite3_step(QueryStatement) == SQLITE_DONE{
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(QueryStatement)
      
        return isExecuted
    }
    
    func updateUserAddressIdInLocalPersistedUserAddress(userAddressId : String) throws -> Bool{
        
        var updatedAddressId =  false
        let query = "update local_persisted_previous_used_user_address SET user_address_id = ? where previous_used_user_address_id = 1"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard  sqlite3_bind_text(queryStatement , 1, (userAddressId as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if( sqlite3_step(queryStatement) == SQLITE_DONE){
            updatedAddressId = true
        }
            else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(queryStatement)
        
        return updatedAddressId
    }
    
    func updateLocalPersistedAddress(userAddressDetails : UserAddressDetails) throws -> Bool{
        
        var addressUpdated  : Bool = false
        let query = "UPDATE  local_persisted_previous_used_user_address SET user_address_tag = ? ,user_door_number_and_building_name_and_building_number = ? , user_street_name = ?, user_locality = ?,user_landmark = ?, user_city = ? , user_state = ?,user_country = ? , user_pincode = ? where user_address_id = ?"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard
                
            sqlite3_bind_text(queryStatement , 1, ( userAddressDetails.addressTag as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 2, (userAddressDetails.addressDetails.doorNoAndBuildingNameAndBuildingNo as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 3, (userAddressDetails.addressDetails.streetName as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 4, (userAddressDetails.addressDetails.localityName as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 5, (userAddressDetails.addressDetails.landmark as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 6, (userAddressDetails.addressDetails.city as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 7, (userAddressDetails.addressDetails.state as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 8, (userAddressDetails.addressDetails.country as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 9, (userAddressDetails.addressDetails.pincode as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_text(queryStatement , 10, ( userAddressDetails.userAddressId as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        if sqlite3_step(queryStatement) == SQLITE_DONE{
            addressUpdated = true
        }
        else{
            throw SQLiteError.Step(message: errorMessage)
        }
            sqlite3_finalize(queryStatement)
       
        return addressUpdated
    }
    
}
