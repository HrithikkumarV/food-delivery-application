//
//  DatabaseConnection.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 08/11/21.
//

import Foundation
import SQLite3
class DatabaseConnection{
    
    static private var databaseConnection : OpaquePointer? = nil
    static private var filePath : URL? = nil
    private init (){
        DatabaseConnection.filePath = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent("FoodDeliveryAppDb.sqlite")
        print(DatabaseConnection.filePath?.path)
        
    }
    
    
    static func getDatabaseConenction() -> OpaquePointer? {
        if(filePath == nil){
            DatabaseConnection()
        }
        do{
            if(databaseConnection == nil){
                try openDatabase()
            }
            
         }
         catch {
             print(error)
         }
        return databaseConnection
    }
    
    static private func openDatabase() throws{
        if sqlite3_open_v2(filePath?.path ,&DatabaseConnection.databaseConnection,SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_SHAREDCACHE | SQLITE_OPEN_FULLMUTEX ,nil) != SQLITE_OK {
            closeDatabaseConnection()
             throw SQLiteError.OpenDatabase(message: String(cString: sqlite3_errmsg(DatabaseConnection.databaseConnection)))
            
         }
    }
    
    static func closeDatabaseConnection(){
        if sqlite3_close_v2(databaseConnection) != SQLITE_OK {
            print(String(cString: sqlite3_errmsg(self.databaseConnection)))
        }
        print("Closed DB")
        databaseConnection = nil
    }

}




