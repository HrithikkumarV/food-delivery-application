//
//  InjectDataBaseInstance.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 27/10/21.
//

import Foundation

class InjectDAOUtils{
    private static var userAccountsDAO : UserAccountsDAO?  = nil
    private static var restaurantAccountsDAO : RestaurantAccountsDAO? = nil
    private static var restaurantsDAO : DisplayRestaurantsDAO? = nil
    private static var menuDetailsDAO :  MenuDetailsDAO? = nil
    private static var favouriteRestaurantDAO : FavouriteRestaurantDAO? = nil
    private static var searchMenuAndRestaurantDAO : SearchMenuAndRestaurantDAO? = nil
    private static var userCartDAO : UserCartDAO? = nil
    
    private static var couponDAO : CouponDAO? = nil
    
    private static var userOrdersDAO : UserOrdersDAO? = nil
    
    private static var restaurantOrdersDAO : RestaurantOrdersDAO? = nil
    private init(){
    }
    
    static func getUserAccountDAO() -> UserAccountsDAO{
        if(userAccountsDAO == nil){
            userAccountsDAO = UserAccountsDAO()
        }
        return userAccountsDAO!
    }
    
    static func deinitUserAccounts(){
        userAccountsDAO = nil
    }
    
    static func getRestaurantAccountDAO() -> RestaurantAccountsDAO{
        if(restaurantAccountsDAO == nil){
            restaurantAccountsDAO = RestaurantAccountsDAO()
        }
        return restaurantAccountsDAO!
    }
    
    static func getRestaurantsDAO() -> DisplayRestaurantsDAO{
        if(restaurantsDAO == nil){
            restaurantsDAO = DisplayRestaurantsDAO()
        }
        return restaurantsDAO!
    }
    
    static func getMenuDetailsDAO() -> MenuDetailsDAO{
        if(menuDetailsDAO == nil){
            menuDetailsDAO = MenuDetailsDAO()
        }
        return menuDetailsDAO!
    }
    
    static func getFavouriteRestaurantDAO() -> FavouriteRestaurantDAO{
        if(favouriteRestaurantDAO == nil){
            favouriteRestaurantDAO = FavouriteRestaurantDAO()
        }
        return favouriteRestaurantDAO!
    }
    
    static func getSearchMenuAndRestaurantDAO() -> SearchMenuAndRestaurantDAO{
        if(searchMenuAndRestaurantDAO == nil){
            searchMenuAndRestaurantDAO = SearchMenuAndRestaurantDAO()
        }
        return searchMenuAndRestaurantDAO!
    }
    
    static func getUserCartDAO() -> UserCartDAO{
        if(userCartDAO == nil){
            userCartDAO = UserCartDAO()
        }
        return userCartDAO!
    }
    
    static func getCouponDAO() -> CouponDAO{
        if(couponDAO == nil){
            couponDAO = CouponDAO()
        }
        return couponDAO!
    }
    
    
    
    static func getUserOrdersDAO() -> UserOrdersDAO{
        if(userOrdersDAO == nil){
            userOrdersDAO = UserOrdersDAO()
        }
        return userOrdersDAO!
    }
    
    static func getRestaurantOrdersDAO() -> RestaurantOrdersDAO{
        if(restaurantOrdersDAO == nil){
            restaurantOrdersDAO = RestaurantOrdersDAO()
        }
        return restaurantOrdersDAO!
    }
    
}
