//
//  restaurantDataBaseProtocols.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 27/10/21.
//

import Foundation
protocol RestaurantAccountsDAOProtocols : RestaurantCreateAccountDAOProtocol ,  RestaurantLoginDAOProtocol,RestaurantChangePasswordDAOProtocol ,LocalPersistRestaurantIdDAOProtocol,RestaurantAccountsPageDAOProtocol{
    
}
protocol RestaurantCreateAccountDAOProtocol {
    func persistRestaurantCreateAccount(restaurantAccount : RestaurantAccount, restaurantDetails : RestaurantDetails ,restaurantAddress : AddressDetails) throws -> Bool 
    func checkIfPhoneNumberExists(phoneNumber : String) throws -> Bool
    func getRestaurantId(phoneNumber : String) throws -> Int
}



protocol RestaurantLoginDAOProtocol {
    func getRestaurantPassword(phoneNumber : String) throws -> String
    func getRestaurantId(phoneNumber : String) throws -> Int
}

protocol RestaurantChangePasswordDAOProtocol {
    func checkIfPhoneNumberExists(phoneNumber : String) throws -> Bool
    func UpdatePassword(phoneNumber : String , password : String) throws -> Bool
}

protocol LocalPersistRestaurantIdDAOProtocol {
    func persistRestaurantId(restaurantId : Int) throws -> Bool
    func deleteRestaurantId() throws -> Bool
    func getRestaurantId() throws -> Int
}


protocol RestaurantMenuDetailsDAOProtocol : AddAndEditMenuDetailsDAOProtocol,RestaurantDisplayMenuDetailsPageDAOProtocol{
    
}

protocol AddAndEditMenuDetailsDAOProtocol {
    func persistMenuDetails(menuDetails : MenuDetails) throws -> Bool
    func persistMenuCategory(restaurantId : Int , categoryType : String) throws -> Bool
    func getCategoryDetails(restaurantId : Int) throws -> [(categoryId : Int , categoryName : String)]
    func getCategoryId(categoryName : String , restaurantId : Int) throws -> Int
    func updateMenuDetails(menuContentDetails: MenuContentDetails) throws -> Bool
}

protocol RestaurantDisplayMenuDetailsPageDAOProtocol{
    func getMenuDetailsofRestaurant(restaurantId : Int) throws -> [MenuContentDetails]
    func getCategoryDetails(restaurantId : Int) throws -> [(categoryId : Int , categoryName : String)]
    func removeMenu(menuId : Int) throws -> Bool
    func removeCategory(categoryId : Int) throws -> Bool
    func UpdateCategoryName(CategoryName : String , categoryId : Int) throws -> Bool
    func persistMenuCategory(restaurantId : Int , categoryType : String) throws -> Bool
    func removeMenuOfCategory(categoryId: Int) throws -> Bool
}

protocol CreateRestaurantAccountTableProtocol {
    func createRestaurantAccountsTable() throws -> Bool
}

protocol CreateMenuTableProtocol{
    func createMenuDetailsTable() throws -> Bool
}


protocol RestaurantOrdersDAOProtocol{
    func updateOrderStatus(orderId : String, orderStatus : OrderStatus) throws -> Bool
    
    
    
    func getMenuDetailsInOrderFoodDetails(orderId: String) throws -> [OrderMenuDetails]
    
    
   
    func getMenuDetailsInActiveOrderFoodDetails(restaurantId : Int) throws -> [ String : [OrderMenuDetails]]
    
    
    func getMenuDetailsInpastOrderFoodDetails(orderIdListString : String) throws -> [String :[OrderMenuDetails]]
    
    
    
    
    func getActiveOrderDetails(restaurantId : Int)throws -> [OrderDetails]
    
    
    func getpastOrderDetails(restaurantId : Int,offSet: Int, limit: Int) throws -> [OrderDetails]
    
    
    func updateOrderCancellationReason(orderId : String , orderCancellationReason : String) throws -> Bool
    
    
    func getOrderCancellationReason(orderId : String) throws -> String
    
    func getUserDetails(orderId : String) throws -> UserDetails
    
    func getBillDetailsOfOrder(orderId : String) throws -> BillDetailsModel
}

protocol RestaurantAccountsPageDAOProtocol{
    func getRestaurantDetails(restaurantId : Int) throws -> RestaurantContentDetails
    func updateRestaurantName(restaurantID : Int , restaurantName : String) throws -> Bool
    func updateRestaurantCuisine(restaurantID : Int , restaurantCuisine : String) throws -> Bool
    func updateRestaurantDescription(restaurantID : Int , restaurantDescription : String) throws -> Bool
    func updateRestaurantPhoneNumber(restaurantID : Int , restaurantPhoneNumber : String) throws -> Bool
    func updateRestaurantAddress(restaurantID : Int , restaurantAddress : AddressDetails) throws -> Bool
    func updateRestaurantAvailablityAndNextOpensAt(restaurantID: Int, restaurantIsAvailable : Int,restaurantOpensNextAt: String) throws -> Bool
    
    func updateRestaurantStatus(restaurantID: Int, restaurantStatus : Int) throws -> Bool

    func updateRestaurantFoodPackagingCharges(restaurantID: Int, restaurantFoodPackagingCharge : Int) throws -> Bool
    func updateRestaurantProfileImage(restaurantID: Int, profileImage: Data) throws -> Bool
    
}
