//
//  Common-DAOProtocols.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 19/01/22.
//

import Foundation
import UIKit

protocol CreateDatabaseTablesProtocol{
    func createTables() -> Bool
}


