//
//  UserAddress.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 27/10/21.
//

import Foundation

struct AddressDetails : Codable{
    
    var doorNoAndBuildingNameAndBuildingNo : String = ""
    var streetName : String = ""
    var localityName : String = ""
    var landmark : String = ""
    var city : String = ""
    var state : String = ""
    var pincode : String = ""
    var country : String = ""

}
