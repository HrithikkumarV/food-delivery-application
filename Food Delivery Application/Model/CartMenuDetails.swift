//
//  CartMenuDetails.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 15/03/22.
//

import Foundation
struct CartMenuDetails{
    var menuId :Int = 0
    var menuTarianType : String = ""
    var quantity : Int = 0
    var menuName : String = ""
    var price : Int = 0
    var menuIsAvailable : Int = 0
    var menuNextAvailableAt : String = ""
}
