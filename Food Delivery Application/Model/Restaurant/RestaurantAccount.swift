//
//  RestaurentAccount.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 16/12/21.
//

import Foundation

struct RestaurantAccount{
    
     var restaurantId : Int = 0
     var restaurantPhoneNumber : String = ""
     var restaurantPassword : String = ""
    
}

