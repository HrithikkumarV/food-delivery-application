//
//  restaurantDetails.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 27/10/21.
//

import Foundation
import UIKit

struct RestaurantDetails {
     var restaurantProfileImage : Data = UIImage(systemName: "fork.knife.circle")!.pngData()!
     var restaurantName : String = ""
     var restaurantDescription : String = ""
     var restaurantCuisine : String = ""
     var restaurantContactNumber : String = ""
   
}
