//
//  MenuContentDetails.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 10/02/22.
//

import Foundation

struct MenuContentDetails{
     var menuDetails : MenuDetails = MenuDetails()
     var menuStatus : Int = 0
     var menuIsAvailable : Int = 0
     var menuNextAvailableAt : String = ""
     var menuId : Int = 0
   
}

