//
//  UserDetails.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 27/10/21.
//

import Foundation

struct UserDetails{
    
     var userName : String = ""
     var userPhoneNumber : String = ""
    
}
