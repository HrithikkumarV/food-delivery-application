//
//  UserAddressDetails.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 12/04/22.
//

import Foundation

struct UserAddressDetails{
    var userAddressId = ""
    var addressDetails = AddressDetails()
    var addressTag = ""
}
