//
//  UserDefaultManager.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 12/04/22.
//

import Foundation

class UserDefaultManager{
     static let shared = UserDefaultManager()
     let defaults =  UserDefaults(suiteName: "com.hrithik.FoodDeliveryApplication")
    
//    func getUserId() -> Int{
//        if let userId = defaults?.value(forKey: "UserId") as? Int{
//                return userId
//        }
//        else{
//            return 0
//        }
//    }
//
//    func getRestaurantId() -> Int{
//        if let restaurantId = defaults?.value(forKey: "RestaurantId") as? Int{
//            return restaurantId
//        }
//        else{
//            return 0
//        }
//    }
//
//
//
//    func setUserId(userId : Int) -> Bool{
//
//        ((defaults?.setValue(userId, forKey: "UserId")) != nil)
//    }
//
//    func setRestaurantId(restaurantId : Int) -> Bool{
//        ((defaults?.setValue(restaurantId, forKey: "RestaurantId")) != nil)
//    }
//
//    func deleteUserId() -> Bool{
//        ((defaults?.removeObject(forKey: "UserId")) != nil)
//    }
//
//    func deleteRestaurantId() -> Bool{
//        ((defaults?.removeObject(forKey: "")) != nil)
//    }
    
//    func setUserDeliveryAddress(deliveryAddress : (userAddressId: String, addressDetails: Data, addressTag: String)) -> Bool{
//
//        ((defaults?.setValue(deliveryAddress, forKey: "DeliveryAddress")) != nil)
//    }
//
//    func getUserDeliveryAddress() ->(userAddressId: String, addressDetails: Data, addressTag: String){
//        if let deliveryAddress = defaults?.value(forKey: "DeliveryAddress") as? (userAddressId: String, addressDetails: Data, addressTag: String){
//            return deliveryAddress
//        }
//        else{
//            return (userAddressId: "", addressDetails: Data(), addressTag: "")
//        }
//    }
//
//    func deleteUserDeliveryAddress() -> Bool{
//        ((defaults?.removeObject(forKey: "DeliveryAddress")) != nil)
//    }
//
//    func updateUserDeliveryAddressId(userAddressId: String) -> Bool{
//        if var deliveryAddress = defaults?.value(forKey: "DeliveryAddress") as? (userAddressId: String, addressDetails: Data, addressTag: String){
//            deliveryAddress.userAddressId = userAddressId
//            return setUserDeliveryAddress(deliveryAddress: deliveryAddress)
//        }
//        return false
//
//    }
}
