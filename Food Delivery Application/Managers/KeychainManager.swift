//
//  KeychainManager.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 12/04/22.
//

import Foundation

class KeychainManager{
    private static let service: String = "com.hrithik.food-delivery-app"
    
    enum KeychainError: Error {
            
            case itemNotFound
            
            
            case duplicateItem
            
            
            case invalidItemFormat
            
            
            case unexpectedStatus(OSStatus)
        }
    
    
    static func saveValue(value: Data, account: String) throws -> Bool{
        var isExecuted : Bool = false
        let query: [String: AnyObject] = [
            
            kSecAttrService as String: service as AnyObject,
            kSecAttrAccount as String: account as AnyObject,
            kSecClass as String: kSecClassGenericPassword,
            
           
            kSecValueData as String: value as AnyObject
        ]
        
        
        let status = SecItemAdd(
            query as CFDictionary,
            nil
        )

        
        if status == errSecDuplicateItem {
            throw KeychainError.duplicateItem
        }

        
        if status == errSecSuccess {
            isExecuted = true
        } else {
            throw KeychainError.unexpectedStatus(status)
        }
        return isExecuted
    }
    
    static func readValue(account: String) throws -> Data {
        let query: [String: AnyObject] = [
            kSecAttrService as String: service as AnyObject,
            kSecAttrAccount as String: account as AnyObject,
            kSecClass as String: kSecClassGenericPassword,
            
            
            kSecMatchLimit as String: kSecMatchLimitOne,

            
            kSecReturnData as String: kCFBooleanTrue
        ]

        
        var itemCopy: AnyObject?
        let status = SecItemCopyMatching(
            query as CFDictionary,
            &itemCopy
        )

        
        guard status != errSecItemNotFound else {
            throw KeychainError.itemNotFound
        }
        
        
        guard status == errSecSuccess else {
            throw KeychainError.unexpectedStatus(status)
        }

       
        guard let password = itemCopy as? Data else {
            throw KeychainError.invalidItemFormat
        }

        return password
    }
    
    static func updateValue(value: Data, account: String) throws -> Bool{
        var isExecuted : Bool = false
        let query: [String: AnyObject] = [
            // kSecAttrService,  kSecAttrAccount, and kSecClass
            // uniquely identify the item to update in Keychain
            kSecAttrService as String: service as AnyObject,
            kSecAttrAccount as String: account as AnyObject,
            kSecClass as String: kSecClassGenericPassword
        ]
        
        
        let attributes: [String: AnyObject] = [
            kSecValueData as String: value as AnyObject
        ]
        
        
        let status = SecItemUpdate(
            query as CFDictionary,
            attributes as CFDictionary
        )

        
        guard status != errSecItemNotFound else {
            throw KeychainError.itemNotFound
        }

        
        if status == errSecSuccess {
            isExecuted = true
        } else {
            throw KeychainError.unexpectedStatus(status)
        }
        return isExecuted
    }
    
    
    static func deleteValue(account: String) throws -> Bool {
        var isExecuted : Bool = false
        let query: [String: AnyObject] = [
            
            kSecAttrService as String: service as AnyObject,
            kSecAttrAccount as String: account as AnyObject,
            kSecClass as String: kSecClassGenericPassword
        ]

        
        let status = SecItemDelete(query as CFDictionary)

        
        if status == errSecSuccess {
            isExecuted = true
        } else {
            throw KeychainError.unexpectedStatus(status)
        }
        return isExecuted
    }
}
