//
//  PincodeDetailsUtils.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 05/04/22.
//

import Foundation
import UIKit
class PincodeDetailsUtils{
    static func getDetailsOfPincode( pincode : String ,complition: @escaping (_ pincodeDetails : PincodeDetails) -> Void){
        APICallerManager.shared.getDetailsOfPincode(pincode: pincode) { pincodeDetails in
            complition(pincodeDetails)
        }
        
    }
   
}


struct PincodeDetails{
    var country : String = ""
    var state : String = ""
    var city : String = ""
    var pincode : String = ""
    var localities : [String] = []
    
}


