//
//  ViewUtils.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 24/01/22.
//

import Foundation
import UIKit

class ViewUtils{
    private let view : UIView
    init(view : UIView){
        self.view = view
    }
    func createViewTapGestureRecognizer() -> UITapGestureRecognizer{
        let tapRecognizer = UITapGestureRecognizer()
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(tapRecognizer)
        return tapRecognizer
    }
    
    
}

public enum BorderSide {
    case top, bottom, left, right
}

extension UIView {
    func addBorder(side: BorderSide, color: UIColor, width: CGFloat) {
        let border = UIView()
        border.translatesAutoresizingMaskIntoConstraints = false
        border.backgroundColor = color
        self.addSubview(border)

        let topConstraint = topAnchor.constraint(equalTo: border.topAnchor,constant: width)
        let rightConstraint = trailingAnchor.constraint(equalTo: border.trailingAnchor,constant: width)
        let bottomConstraint = bottomAnchor.constraint(equalTo: border.bottomAnchor,constant: -width)
        let leftConstraint = leadingAnchor.constraint(equalTo: border.leadingAnchor,constant: -width)
        let heightConstraint = border.heightAnchor.constraint(equalToConstant: width)
        let widthConstraint = border.widthAnchor.constraint(equalToConstant: width)


        switch side {
        case .top:
            NSLayoutConstraint.activate([leftConstraint, topConstraint, rightConstraint, heightConstraint])
        case .right:
            NSLayoutConstraint.activate([topConstraint, rightConstraint, bottomConstraint, widthConstraint])
        case .bottom:
            NSLayoutConstraint.activate([rightConstraint, bottomConstraint, leftConstraint, heightConstraint])
        case .left:
            NSLayoutConstraint.activate([bottomConstraint, leftConstraint, topConstraint, widthConstraint])
        }
    }

    
}

extension UIView{
    func addLabelToTopBorder(labelText : String,borderColor : UIColor , borderWidth : CGFloat,leftPadding : CGFloat,topPadding : CGFloat,textColor : UIColor , fontSize : CGFloat){
        self.addBorder(side: .top, color: borderColor, width: borderWidth)
        self.addBorder(side: .right, color: borderColor, width: borderWidth)
        self.addBorder(side: .left, color: borderColor, width: borderWidth)
        self.addBorder(side: .bottom, color: borderColor, width: borderWidth)
        let topBorderLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width - 20, height: fontSize))
        topBorderLabel.text = labelText
        topBorderLabel.textColor = textColor
        topBorderLabel.backgroundColor = .white
        self.addSubview(topBorderLabel)
        topBorderLabel.translatesAutoresizingMaskIntoConstraints = false
        topBorderLabel.centerYAnchor.constraint(equalTo: self.topAnchor,constant: topPadding).isActive = true
        topBorderLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: leftPadding).isActive = true
        topBorderLabel.tag = 111
        self.bringSubviewToFront(topBorderLabel)
    }
        
        func hideTopBorderLabelInView(){
            self.viewWithTag(111)?.isHidden = true
        }
        func showTopBorderLabelInView(){
            self.viewWithTag(111)?.isHidden = false
        }
    
//    func addLabelToTopBorderView(labelText : String,borderColor : UIColor , borderWidth : CGFloat,leftPadding : CGFloat,topPadding : CGFloat,textColor : UIColor , fontSize : CGFloat) -> UIView{
//        let view = UIView()
//        let topBorderLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width - 20, height: fontSize))
//        topBorderLabel.text = labelText
//        topBorderLabel.textColor = textColor
//        topBorderLabel.backgroundColor = .white
//        view.addSubview(topBorderLabel)
//        topBorderLabel.translatesAutoresizingMaskIntoConstraints = false
//        topBorderLabel.centerYAnchor.constraint(equalTo: view.topAnchor,constant: topPadding).isActive = true
//        topBorderLabel.leftAnchor.constraint(equalTo: view.leftAnchor,constant: leftPadding).isActive = true
//        topBorderLabel.tag = 111
//        view.bringSubviewToFront(topBorderLabel)
//        view.frame = self.bounds
//        view.addSubview(self)
//        return view
//    }
//        
//        func hideTopBorderLabelInView(){
//            self..viewWithTag(111)?.isHidden = true
//        }
//        func showTopBorderLabelInView(){
//            self.viewWithTag(111)?.isHidden = false
//        }
}

extension UIView{
    func dropShadow() {
        self.layer.shadowColor = .init(red: 0.6, green: 0.7, blue: 0.8, alpha: 0.9)
        self.layer.shadowOffset  = CGSize.zero
        self.layer.shadowOpacity = 0.9
        self.layer.shadowRadius = 3
        self.layer.masksToBounds = false
      }
}



extension UIView{
    
    func addBorderWithTag(side: BorderSide, color: UIColor, width: CGFloat, tag : Int) {
            let border = UIView()
            border.translatesAutoresizingMaskIntoConstraints = false
            border.backgroundColor = color
            border.tag  = tag
            self.addSubview(border)

            let topConstraint = topAnchor.constraint(equalTo: border.topAnchor,constant: width)
            let rightConstraint = trailingAnchor.constraint(equalTo: border.trailingAnchor,constant: width)
            let bottomConstraint = bottomAnchor.constraint(equalTo: border.bottomAnchor,constant: -width)
            let leftConstraint = leadingAnchor.constraint(equalTo: border.leadingAnchor,constant: -width)
            let heightConstraint = border.heightAnchor.constraint(equalToConstant: width)
            let widthConstraint = border.widthAnchor.constraint(equalToConstant: width)


            switch side {
            case .top:
                NSLayoutConstraint.activate([leftConstraint, topConstraint, rightConstraint, heightConstraint])
            case .right:
                NSLayoutConstraint.activate([topConstraint, rightConstraint, bottomConstraint, widthConstraint])
            case .bottom:
                NSLayoutConstraint.activate([rightConstraint, bottomConstraint, leftConstraint, heightConstraint])
            case .left:
                NSLayoutConstraint.activate([bottomConstraint, leftConstraint, topConstraint, widthConstraint])
            }
        

    }
    
    func removeBorderWithTag(tag : Int){
        for subview in self.subviews{
            if subview.tag == tag{
                subview.removeFromSuperview()
            }
        }
    }
    
    func changeBorderColorWithTag(tag : Int,color : UIColor){
        for subview in self.subviews{
            if subview.tag == tag{
                subview.backgroundColor = color
            }
        }
    }
}


extension UIView{
    
    func addErrorLabel(borderWidth : CGFloat,leftPadding : CGFloat,bottomPadding : CGFloat){
        self.addBorderWithTag(side: .top, color: .clear, width: borderWidth, tag: 123)
        self.addBorderWithTag(side: .left, color: .clear, width: borderWidth, tag: 123)
        self.addBorderWithTag(side: .right, color: .clear, width: borderWidth, tag: 123)
        self.addBorderWithTag(side: .bottom, color: .clear, width: borderWidth, tag: 123)
        let errorLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width - 20, height: 15))
        errorLabel.textColor = .systemRed
        errorLabel.backgroundColor = .white
        self.addSubview(errorLabel)
        errorLabel.translatesAutoresizingMaskIntoConstraints = false
        errorLabel.centerYAnchor.constraint(equalTo: self.bottomAnchor,constant: bottomPadding).isActive = true
        errorLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: leftPadding).isActive = true
        errorLabel.tag = 123
        self.bringSubviewToFront(errorLabel)
    }
        
    func hideErrorLabelInView(){
        let label = self.viewWithTag(123) as? UILabel
        label?.isHidden = true
        label?.text = ""
        self.changeBorderColorWithTag(tag: 123, color: .clear)
    }
    func showErrorLabelInView(errorText : String){
        let label = self.viewWithTag(123) as? UILabel
        label?.isHidden = false
        label?.text = errorText
        self.changeBorderColorWithTag(tag: 123, color: .systemRed)
    }
}



extension UIView{
    func removeHeightAnchorConstraints(){
        for constraint in constraints {
            guard constraint.firstAnchor == heightAnchor else { continue }
            constraint.isActive = false
            break
        }
    }
    
    func removeWidthAnchorConstraints(){
        for constraint in constraints {
            guard constraint.firstAnchor == widthAnchor else { continue }
            constraint.isActive = false
            break
        }
    }
}
