//
//  InjectIDUtils.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/12/21.
//

import Foundation

class UserIDUtils : UserIdProtocols{
    
    
    static let shared = UserIDUtils()
    private static var userId : Int = 0

    private let userIdDAO : LocalPersistUserIdDAOProtocol = InjectDAOUtils.getUserAccountDAO()
    
    private init(){
    }
    
    
    
    func setUserId(userId : Int) -> Bool{
        
        do{
            return try userIdDAO.persistUserId(userId: userId)
        }
        catch {
            print(error)
        }
        return false
//        return UserDefaultManager.shared.setUserId(userId: userId)
//        do{
//            return try KeychainManager.saveValue(value: userId.description.data(using: .utf8) ?? Data() , account: "UserId")
//        }
//        catch {
//            print(error)
//        }
//        return false
        
        
    }
    
   
    
    func getUserId()  -> Int{
        if(UserIDUtils.userId == 0){
            
            do{
                try UserIDUtils.userId = userIdDAO.getUserID()

            }
            catch {
                print(error)
            }
        }
//        UserIDUtils.userId = UserDefaultManager.shared.getUserId()
//        do{
//            try UserIDUtils.userId = Int(String(decoding : KeychainManager.readValue(account: "UserId"),as: UTF8.self)) ?? 0
//        }
//        catch{
//            print(error)
//        }
//
//
        return UserIDUtils.userId
    }
    
    
    
    func deleteUserId() -> Bool{
        UserIDUtils.userId = 0
        
        do{

            return try userIdDAO.deleteUserId()

        }
        catch {
            print(error)
        }
        return false
//        return UserDefaultManager.shared.deleteUserId()
//        do{
//            return try KeychainManager.deleteValue(account: "UserId")
//        }
//        catch{
//            print(error)
//        }
//        return false
    }
    
    
}

class RestaurantIDUtils : RestaurantIdProtocols{
    static let shared = RestaurantIDUtils()
    private static var restaurantId : Int = 0
    private let restaurentIdDAO : LocalPersistRestaurantIdDAOProtocol  = InjectDAOUtils.getRestaurantAccountDAO()
    
    private init(){
    }
    
    
    
    func setRestaurantId(restaurantId : Int) -> Bool{
        do{
            return try restaurentIdDAO.persistRestaurantId(restaurantId: restaurantId)
        }
        catch {
            print(error)
        }
        return false
//        return UserDefaultManager.shared.setRestaurantId(restaurantId: restaurantId)
          
//        do{
//            return try KeychainManager.saveValue(value: userId.description.data(using: .utf8) ?? Data() , account: "RestauerantId")
//        }
//        catch {
//            print(error)
//        }
//        return false
    }
    
    
    
    func getRestaurantId() -> Int{
        if(RestaurantIDUtils.restaurantId == 0){

            do{
                try RestaurantIDUtils.restaurantId = restaurentIdDAO.getRestaurantId()
            }
            catch {
                print(error)
            }
            return RestaurantIDUtils.restaurantId
        }
        //            RestaurantIDUtils.restaurantId = UserDefaultManager.shared.getRestaurantId()
//        do{
//            try  RestaurantIDUtils.restaurantId = Int(String(decoding : KeychainManager.readValue(account: "UserId"),as: UTF8.self)) ?? 0
//        }
//        catch{
//            print(error)
//        }
        
        
        
        return RestaurantIDUtils.restaurantId
      
    }
    
    
    func deleteRestaurantId() -> Bool {
        RestaurantIDUtils.restaurantId = 0

        do{

            return try restaurentIdDAO.deleteRestaurantId()

        }
        catch let error{
            print(error)
        }
        return false
        //        return UserDefaultManager.shared.deleteRestaurantId()
//        do{
//
//            return try KeychainManager.deleteValue(account: "RestaurantId")
//
//        }
//        catch {
//            print(error)
//        }
    }
}


protocol UserIdProtocols{
    func setUserId(userId : Int) -> Bool
    func getUserId()  -> Int
    func deleteUserId() -> Bool
}

protocol RestaurantIdProtocols{
    func setRestaurantId(restaurantId : Int) -> Bool
    func getRestaurantId() -> Int
    func deleteRestaurantId() -> Bool
}
