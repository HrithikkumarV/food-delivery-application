//
//  KeyboardUtils.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 23/01/22.
//

import Foundation
import UIKit

class KeyboardUtils {
    
    private let scrollView : UIScrollView
    
    init(scrollView : UIScrollView){
        self.scrollView = scrollView
    }
    
    
    func keyboardWillShow(notification: NSNotification, maxYofLastObjectInView : CGFloat, activeArea : UIView?) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
                return
              }
       
        if(keyboardSize.minY <= maxYofLastObjectInView + 100){
                    scrollView.isScrollEnabled = true
            let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
            
        }
        if activeArea != nil
            {
            self.scrollView.scrollRectToVisible(activeArea!.frame, animated: true)
            }
             
    }
    
    func keyboardWillHide() {
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
}
