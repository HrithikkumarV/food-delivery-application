//
//  File.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 17/03/22.
//

import Foundation

class RestaurantGST {
    private static let GSTPersent : Double = 5
    static func getGSTCost(totalPrice : Int) -> Double{
        let gst = ((Double(totalPrice)) * GSTPersent) / 100
        return gst
    }
}


class Delivery{
    
    private static let  deliveryFee  : Int = 30
    private static let deliveryTime : Int = 30
    static func getDefaultDeliveryFee() -> Int{
        return deliveryFee
    }
    
    static func getDeaultDelieveryTimeInMins() -> Int{
        return deliveryTime
    }
    
}

class ContactTeam{
    private static let contactTeam : String = "abc@foodzy.com"
    static func getContactTeamEmailId() -> String{
        return contactTeam
    }
}

class RestaurantCuisines{
    private static  let cuisineTypes = ["ameroonian cuisine",
                                            "Chadian cuisine",
                                            "Congolese cuisine",
                                            "Centrafrican cuisine",
                                            "Equatorial Guinea cuisine",
                                            "Gabonese cuisine",
                                            "Santomean cuisine",
                                            "East African cuisine",
                                            "Burundian cuisine",
                                            "Djiboutian cuisine",
                                            "Eritrean cuisine",
                                            "Ethiopian cuisine",
                                            "Kenyan cuisine",
                                            "Maasai cuisine",
                                            "Rwandan cuisine",
                                            "Somali cuisine",
                                            "South Sudanese cuisine",
                                            "Tanzanian cuisine",
                                            "Zanzibari cuisine",
                                            "Ugandan cuisine",
                                            "North African cuisine",
                                            "Algerian cuisine",
                                            "Berber cuisine",
                                            "Egyptian cuisine",
                                            "Libyan cuisine",
                                            "Mauritanian cuisine",
                                            "Moroccan cuisine",
                                            "Sudanese cuisine",
                                            "Tunisian cuisine",
                                            "Western Saharan cuisine",
                                            "Southern African cuisine",
                                            "Botswana cuisine",

                                            "Eswatini cuisine",
                                            "Lesotho cuisine",
                                            "Malagasy cuisine",
                                            "Malawian cuisine",
                                            "Mauritian cuisine",
                                            "Mozambican cuisine",
                                            "Namibian cuisine",
                                            "Seychellois cuisine",
                                            "South African cuisine",
                                            "Zambian cuisine",
                                            "Zimbabwean cuisine",
                                            "West African cuisine",
                                            "Benin cuisine",

                                            "Burkinabé cuisine",
                                            "Cape Verdean cuisine",
                                            "Gambian cuisine",
                                            "Ghanaian cuisine",
                                            "Guinean cuisine",
                                            "Guinea-Bissauan cuisine",
                                            "Ivorian cuisine",
                                            "Liberian cuisine",
                                            "Malian cuisine",
                                            "Niger cuisine",
                                            "Nigerian cuisine",
                                            "Senegalese cuisine",
                                            "Sierra Leone cuisine",
                                            "Togolese cuisine",
                                             
                                            "Latin American cuisine",
                                            "Native American",
                                            "Inuit cuisine",
                                            "Tlingit cuisine",
                                            "North American cuisine",
                                            "Canadian cuisine",
                                            "cuisine of the Maritimes",
                                            "cuisine of Quebec",
                                            "cuisine of Toronto",
                                            "Mexican cuisine",
                                            "cuisine of Chiapas",
                                            "Oaxacan cuisine",
                                            "cuisine of Veracruz",
                                            "American cuisine",
                                            "Midwestern American cuisine",
                                            "cuisine of Chicago",
                                            "cuisine of North Dakota",
                                            "cuisine of Omaha",
                                            "St. Louis cuisine",
                                            "cuisine of Wisconsin",
                                            "Northeastern American cuisine",
                                            "cuisine of New England",
                                            "cuisine of the Mid-Atlantic United States",
                                            "cuisine of Baltimore",
                                            "cuisine of New Jersey",
                                            "cuisine of New York City",
                                            "cuisine of the Pennsylvania Dutch",
                                            "cuisine of Philadelphia",
                                            "cuisine of Pittsburgh",
                                            "Southern American cuisine",
                                            "cuisine of Atlanta ",
                                            "Floribbean cuisine",
                                            "cuisine of Kentucky",
                                            "Louisiana Creole cuisine",
                                            "Cajun cuisine",
                                            "cuisine of New Orleans",
                                            "Lowcountry cuisine",
                                            "Soul food",
                                            "Southwestern American cuisine",
                                            "New Mexican cuisine",
                                            "Texan cuisine",
                                            "Tex-Mex cuisine",
                                            "Western American cuisine",
                                            "cuisine of California",
                                            "California cuisine",
                                            "Hawaiian cuisine",
                                            "Pacific Northwest cuisine",
                                            "Rocky Mountain cuisine",
                                            "Fusion cuisine",
                                            "Chinese American cuisine",
                                            "Filipino-American cuisine",
                                            "Greek-American cuisine",
                                            "Italian-American cuisine",
                                            "Mexican-American cuisine",
                                            "New American cuisine",
                                            "Central American cuisine",
                                            "Belizean cuisine",
                                            "Costa Rican cuisine",
                                            "Salvadoran cuisine",
                                            "Guatemalan cuisine",
                                            "Honduran cuisine",
                                            "Nicaraguan cuisine",
                                            "Panamanian cuisine",
                                            "South American cuisine",
                                            "Argentinian cuisine",
                                            "Bolivian cuisine",
                                            "Brazilian cuisine",
                                            "Chilean cuisine",
                                            "Colombian cuisine",
                                            "Ecuadorian cuisine",
                                            "French Guianan cuisine",
                                            "Paraguayan cuisine",
                                            "cuisine of Asunción",
                                            "Peruvian cuisine",
                                            "Peruvian-Chinese cuisine",
                                            "Surinamese cuisine",
                                            "Uruguayan cuisine",
                                            "cuisine of Montevideo",
                                            "Venezuelan cuisine",
                                            "Caribbean cuisine",
                                            "Anguillan cuisine",
                                            "Antigua and Barbuda cuisine",
                                            "Bahamian cuisine",
                                            "Barbadian cuisine",
                                            "Bermudian cuisine",
                                            "British Virgin Islands cuisine",
                                            "Cayman Islands cuisine",
                                            "Cuban cuisine",
                                            "Curaçao cuisine",
                                            "Dominica cuisine",
                                            "Dominican Republic cuisine",
                                            "Grenadian cuisine",
                                            "Guyanese cuisine",
                                            "Haitian cuisine",
                                            "Jamaican cuisine",
                                            "Martinican cuisine",
                                            "Montserrat cuisine",
                                            "Puerto Rican cuisine",
                                            "St. Kitts and Nevis cuisine",
                                            "Saint Lucian cuisine",
                                            "Trinidad and Tobago cuisine",
                                            "Turks and Caicos cuisine",
                                            "US Virgin Islands cuisine",
                                            "Asian cuisine",

                                            "Central Asian cuisine",

                                            "Bukharan Jewish cuisine",
                                            "Kazakh cuisine",
                                            "Kyrgyz cuisine",
                                            "Tajik cuisine",
                                            "Turkmen cuisine",
                                            "Uzbek cuisine",
                                            "East Asian cuisine",

                                            "Hong Kong cuisine",
                                            "Japanese cuisine",
                                            "Ainu cuisine",
                                            "Nagoya cuisine",
                                            "Okinawan cuisine",
                                            "Korean cuisine",
                                            "North Korean cuisine",
                                            "South Korean cuisine",
                                            "Macanese cuisine",
                                            "Mongolian cuisine",
                                            "Taiwanese cuisine",
                                            "Chinese cuisine",
                                            "Anhui cuisine",
                                            "Beijing cuisine",
                                            "Chinese aristocrat cuisine",
                                            "Chinese imperial cuisine",
                                            "Liaoning cuisine",
                                            "Cantonese cuisine",
                                            "Chaoshan (Teochew) cuisine",
                                            "Chiuchow cuisine",
                                            "Fujian cuisine",
                                            "Guangxi cuisine",
                                            "Guizhou cuisine",
                                            "Hainan cuisine",
                                            "Hakka cuisine",
                                            "Henan cuisine",
                                            "Hubei cuisine",
                                            "Hunan cuisine",
                                            "Jiangsu cuisine",
                                            "Huaiyang cuisine",
                                            "Jiangxi cuisine",
                                            "Liaoning cuisine",
                                            "Manchu cuisine",
                                            "Northeastern Chinese cuisine",
                                            "Putian (Henghwa) cuisine",
                                            "Shaanxi cuisine",
                                            "Shandong cuisine",
                                            "Shanghai cuisine",
                                            "Haipai cuisine",
                                            "Shanxi cuisine",
                                            "Sichuan cuisine",
                                            "Tianjin cuisine",
                                            "Tibetan cuisine",
                                            "Xinjiang (Uyghur) cuisine",
                                            "Yunnan cuisine",
                                            "Zhejiang cuisine",
                                            "Chinese religious cuisine",
                                            "Chinese Buddhist cuisine",
                                            "Chinese Islamic cuisine",
                                            "Taoist diet",
                                            "Overseas Chinese cuisine",
                                            "American Chinese cuisine",
                                            "Australian Chinese cuisine",
                                            "Burmese Chinese cuisine",
                                            "Canadian Chinese cuisine",
                                            "Caribbean Chinese cuisine",
                                            "Filipino Chinese cuisine",
                                            "Indian Chinese cuisine",
                                            "Indonesian Chinese cuisine",
                                            "Japanese Chinese cuisine",
                                            "Latin American Chinese cuisine",
                                            "Peruvian Chinese cuisine",
                                            "Puerto Rican Chinese cuisine",
                                            "Korean Chinese cuisine",
                                            "Malaysian Chinese cuisine",
                                            "Pakistani Chinese cuisine",
                                            "Peranakan cuisine",
                                            "Singaporean Chinese cuisine",
                                            "South Asian cuisine",
                                            "Afghan cuisine",

                                            "Bangladeshi cuisine",
                                            "Bengali cuisine",
                                            "Bhutanese cuisine",
                                            "Hazaragi cuisine",
                                            "Maldivian cuisine",
                                            "Nepalese cuisine",
                                            "Newari cuisine",
                                            "Sri Lankan cuisine",
                                            "Sylheti cuisine",
                                            "Indian cuisine",
                                            "West Indian cuisine",
                                            "Goan cuisine",
                                            "Goan Catholic cuisine",
                                            "Gujarati cuisine",
                                            "Maharashtrian cuisine",
                                            "Malvani cuisine",
                                            "Parsi cuisine",
                                            "Rajasthani cuisine",
                                            "North Indian cuisine",
                                            "Awadhi cuisine",
                                            "Bihari cuisine",
                                            "Bhojpuri cuisine",
                                            "Kashmiri cuisine",
                                            "Mughlai cuisine",
                                            "Punjabi cuisine",
                                            "Rajasthani cuisine",
                                            "Uttar Pradesh cuisine",
                                            "South Indian cuisine",
                                            "Andhra cuisine",
                                            "Hyderabadi cuisine",
                                            "Karnataka cuisine",
                                            "Kerala cuisine",
                                            "Mangalorean cuisine",
                                            "Tamil cuisine",
                                            "Telugu cuisine",
                                            "North East Indian cuisine",
                                            "Assamese cuisine",
                                            "Naga cuisine",
                                            "Sikkimese cuisine",
                                            "Tripuri cuisine",
                                            "East Indian cuisine",
                                            "Bengali cuisine",
                                            "Jharkhandi cuisine",
                                            "Odia cuisine",
                                            "Oriya cuisine",
                                            "Other Indian cuisine",
                                            "Anglo-Indian cuisine",
                                            "Chettinad cuisine",
                                            "Indian Chinese cuisine",
                                            "Indian fast food",
                                            "Jain cuisine",
                                            "Sindhi cuisine",
                                            "Udupi cuisine",
                                            "Pakistani cuisine",
                                            "Balochi cuisine",
                                            "Kashmiri cuisine",
                                            "Mughlai cuisine",
                                            "Muhajir cuisine",
                                            "Punjabi cuisine",
                                            "Lahori cuisine",
                                            "Saraiki cuisine",
                                            "Pashtun cuisine",
                                            "Sindhi cuisine",
                                            "Southeast Asian cuisine",
                                            "Bruneian cuisine",
                                            "Burmese cuisine",
                                            "Cambodian cuisine",
                                            "Christmas Island cuisine",
                                            "East Timor cuisine",
                                            "Filipino cuisine",
                                            "Kapampangan cuisine",
                                            "Lao cuisine",
                                            "Hmong cuisine",
                                            "Malaysian cuisine",
                                            "Eurasian cuisine",
                                            "Malay cuisine",
                                            "Malaysian Chinese cuisine",
                                            "Malaysian Indian cuisine",
                                            "Peranakan cuisine",
                                            "Sabahan cuisine",
                                            "Sarawakian cuisine",
                                            "Singaporean cuisine",
                                            "Thai cuisine",
                                            "Vietnamese cuisine",
                                            "Indonesian cuisine",
                                            "Acehnese cuisine",
                                            "Balinese cuisine",
                                            "Banjar cuisine",
                                            "Batak cuisine",
                                            "Indo cuisine",
                                            "Indonesian Arab cuisine",
                                            "Indonesian Chinese cuisine",
                                            "Indonesian Indian cuisine",
                                            "Javanese cuisine",
                                            "Betawi cuisine",
                                            "Madurese cuisine",
                                            "Sundanese cuisine",
                                            "Makassar cuisine",
                                            "Malay cuisine",
                                            "Manado cuisine",
                                            "Padang food",
                                            "Palembang cuisine",
                                            "Peranakan cuisine",
                                            "West Asian cuisine",
                                            "Eastern Arabian cuisine",
                                            "Bahraini cuisine",
                                            "Emirati cuisine",
                                            "Kuwaiti cuisine",
                                            "Omani cuisine",
                                            "Qatari cuisine",
                                            "Saudi Arabian cuisine",
                                            "Yemeni cuisine",

                                            "Levantine cuisine",
                                            "Iraqi cuisine",
                                            "Kurdish cuisine",
                                            "Israeli cuisine",
                                            "Jordanian cuisine",
                                            "Lebanese cuisine",
                                            "Mizrahi Jewish cuisine",
                                            "Palestinian cuisine",
                                            "Syrian cuisine",
                                            "Syrian Jewish cuisine",
                                            "Assyrian cuisine",
                                            "Caucasian cuisine",
                                            "Armenian cuisine",
                                            "Azerbaijani cuisine",
                                            "Circassian cuisine",
                                            "Georgian cuisine",
                                            "Iranian cuisine",
                                            "Mazanderani cuisine",
                                            "Turkish cuisine",
                                            "European cuisine",
                                            "Central European cuisine",
                                            "Austrian cuisine",
                                            "Viennese cuisine",
                                            "Czech cuisine",
                                            "Hungarian cuisine",
                                            "Liechtensteiner cuisine",
                                            "Polish cuisine",
                                            "Silesian cuisine",
                                            "Slovak cuisine",
                                            "Slovenian cuisine",
                                            "Swiss cuisine",
                                            "German cuisine",
                                            "Baden cuisine",
                                            "Bavarian cuisine",
                                            "Brandenburg cuisine",
                                            "Franconian cuisine",
                                            "Hamburg cuisine",
                                            "Hessian cuisine",
                                            "Lower Saxon cuisine",
                                            "Mecklenburg cuisine",
                                            "Palatine cuisine",
                                            "Pomeranian cuisine",
                                            "Rhenish-Hessian cuisine",
                                            "Saxon cuisine",
                                            "Schleswig-Holstein cuisine",
                                            "Silesian cuisine",
                                            "Swabian cuisine",
                                            "Eastern European cuisine",
                                            "Belarusian cuisine",
                                            "Bulgarian cuisine",
                                            "Moldovan cuisine",
                                            "Romani cuisine",
                                            "Romanian cuisine",
                                            "Russian cuisine",
                                            "Bashkir cuisine",
                                            "Chechen cuisine",
                                            "Chukchi cuisine",
                                            "Tatar cuisine",
                                            "Mordovian cuisine",
                                            "Yamal cuisine",
                                            "Ukrainian cuisine",
                                            "Crimean Tatar cuisine",
                                            "Northern European cuisine",
                                            "Baltic cuisine",
                                            "Estonian cuisine",
                                            "Latvian cuisine",
                                            "Lithuanian cuisine",
                                            "Livonian cuisine",
                                            "cuisines of the Islands of the North Atlantic (IONA)",
                                            "British cuisine",
                                            "Channel Islands cuisine",
                                            "English cuisine",
                                            "Cornish cuisine",
                                            "Devonian cuisine",
                                            "Dorset cuisine",
                                            "Northern Irish cuisine",
                                            "Scottish cuisine",
                                            "Welsh cuisine",
                                            "cuisine of Carmarthenshire",
                                            "cuisine of Ceredigion",
                                            "cuisine of Gower",
                                            "cuisine of Monmouthshire",
                                            "cuisine of Pembrokeshire",
                                            "Irish cuisine",
                                            "Nordic cuisine",
                                            "Danish cuisine",
                                            "Faroese cuisine",
                                            "New Nordic cuisine",
                                            "Finnish cuisine",
                                            "Icelandic cuisine",
                                            "Norwegian cuisine",
                                            "Sami cuisine",
                                            "Swedish cuisine",
                                            "Southern European cuisine",
                                            "Cypriot cuisine",
                                            "Balkan cuisine",
                                            "Albanian cuisine",
                                            "Aromanian cuisine",
                                            "Bosnian cuisine",
                                            "Croatian cuisine",
                                            "Greek cuisine",
                                            "Macedonian cuisine",
                                            "Kosovan cuisine",
                                            "Macedonian cuisine",
                                            "Montenegrin cuisine",
                                            "Serbian cuisine",
                                            "Gibraltarian cuisine",
                                            "Italian cuisine",
                                            "Abruzzese and Molisan cuisine",
                                            "Arbëreshë cuisine",
                                            "Basilicatan (Lucanian) cuisine",
                                            "Lazian or Roman",
                                            "Ligurian cuisine",
                                            "Lombard cuisine",
                                            "Mantuan cuisine",
                                            "Neapolitan cuisine",
                                            "Pugliese cuisine",
                                            "Sardinian cuisine",
                                            "Sicilian cuisine",
                                            "Tuscan cuisine",
                                            "Venetian cuisine",
                                            "Maltese cuisine",
                                            "Portuguese cuisine",
                                            "Sammarinese cuisine",
                                            "Spanish cuisine",
                                            "Andalusian cuisine",
                                            "Asturian cuisine",
                                            "Aragonese cuisine",
                                            "Balearic cuisine",
                                            "Basque cuisine",
                                            "Canarian cuisine",
                                            "Cantabrian cuisine",
                                            "Castilian-Leonese cuisine",
                                            "Castilian-Manchego cuisine",
                                            "Catalan cuisine",
                                            "Andorran cuisine",
                                            "Extremaduran cuisine",
                                            "Galician cuisine",
                                            "Leonese cuisine",
                                            "Madrid cuisine",
                                            "Minorcan cuisine",
                                            "Valencian cuisine",
                                            "Turkish cuisine",
                                            "Western European cuisine",
                                            "Belgian cuisine",
                                            "Dutch cuisine",
                                            "French cuisine",
                                            "Basque cuisine",
                                            "Corsican cuisine",
                                            "Haute cuisine",
                                            "Lyonnaise cuisine",
                                            "Nouvelle cuisine",
                                            "Luxembourgian cuisine",
                                            "Monégasque cuisine",
                                            "Occitan cuisine",
                                            "Oceanic cuisine",
                                            "Australasian cuisine",
                                            "Australian cuisine",
                                            "Tasmanian cuisine",
                                            "Christmas Island cuisine",
                                            "New Zealand cuisine",
                                            "Melanesian cuisine",
                                            "Fijian cuisine",
                                            "Papua New Guinean cuisine",
                                            "Solomon Islands cuisine",
                                            "Vanuatuan cuisine",
                                            "Micronesian cuisine",
                                            "Guamanian cuisine",
                                            "Mariana Islands cuisine",
                                            "Marshallese cuisine",
                                            "Nauruan cuisine",
                                            "Palauan cuisine",
                                            "Polynesian cuisine",
                                            "Hawaiian cuisine",
                                            "Niuean cuisine",
                                            "Pascuense (Easter Island) cuisine",
                                            "Pitcairn Islands cuisine",
                                            "Samoan cuisine",
                                            "Tuvaluan cuisine",
                                            "Wallis and Futuna cuisine",
                                            "Fast food",
                                            "Fusion cuisine",
                                            "Haute cuisine",
                                            "Molecular gastronomy",
                                            "Note by Note cuisine",
                                            "Nouvelle cuisine",
                                            "Vegan cuisine",
                                            "Vegetarian cuisine",
                                            "Religious cuisine",
                                            "Buddhist cuisine",
                                            "Christian dietGoan Catholic cuisine",
                                            "Mangalorean Catholic cuisine",
                                            "Mennonite cuisine",
                                            "Hindu diet",
                                            "Islamic diet",
                                            "Chinese Islamic cuisine",
                                            "Jain diet",
                                            "Jewish cuisine",
                                            "American Jewish cuisine",
                                            "Ashkenazi Jewish cuisine",
                                            "Bukharan Jewish cuisine",
                                            "Ethiopian Jewish cuisine",
                                            "Mizrahi Jewish cuisine",
                                            "Sephardic Jewish cuisine",
                                            "Syrian Jewish cuisine",
                                            "Sikh diet"]
    static func getRestaurantCuisines() -> [String]{
        return cuisineTypes
    }
}



