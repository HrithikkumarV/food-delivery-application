//
//  NetworkMonitorUtils.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 06/04/22.
//

import Foundation
import Network
class NetworkMonitor {
    static let shared = NetworkMonitor()

    let monitor = NWPathMonitor()
    private var status: NWPath.Status = .requiresConnection
    var isReachable: Bool { status == .satisfied }
    var isReachableOnCellular: Bool = true

    func startMonitoring() {
        let queue = DispatchQueue(label: "NetworkMonitor")
        monitor.start(queue: queue)
        monitor.pathUpdateHandler = { [weak self] path in
            self?.status = path.status
            self?.isReachableOnCellular = path.isExpensive

            if path.status == .satisfied {
                print("We're connected!")
                
            } else {
                print("No connection.")
                
            }
            print(path.isExpensive)
        }

        
    }

    func stopMonitoring() {
        monitor.cancel()
    }
}
