//
//  File.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 03/03/22.
//

import Foundation
import UIKit

extension UILabel{
    func addLeftImage(imageName : String ,size : CGFloat,leftPadding : CGFloat,imageColor : UIColor){
        let image = UIImageView(frame: CGRect(x: 10, y: 5, width: size, height: size))
        image.image = UIImage(systemName: imageName)?.withTintColor(imageColor, renderingMode: .alwaysOriginal)
        image.backgroundColor = .white
        self.addSubview(image)
    }
}
