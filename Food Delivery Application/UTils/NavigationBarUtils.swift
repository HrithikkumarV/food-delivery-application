//
//  NavigationUtils.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 24/01/22.
//

import Foundation
import UIKit

class NavigationBarUtils{
    
    func createBackNavigationBarButton() -> UIBarButtonItem{
        let backArrowButton =  UIBarButtonItem()
        backArrowButton.image = UIImage(systemName: "arrow.backward")?.withTintColor(.black, renderingMode: .alwaysOriginal)
        backArrowButton.customView?.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backArrowButton.imageInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
        return backArrowButton
    }
    

    
}

extension UIViewController {
    func setDefaultNavigationBarApprearance(){
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .white
        appearance.shadowImage = UIImage()
        self.navigationController?.navigationBar.standardAppearance = appearance
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    func setScrollEdgeApprearance(){
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        self.tabBarController?.navigationController?.navigationBar.scrollEdgeAppearance = appearance
            
    }
}

