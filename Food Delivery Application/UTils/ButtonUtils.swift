//
//  ButtonUtils.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 26/01/22.
//

import Foundation
import UIKit

extension UIButton{
    
    func addRightImageToButton(systemName : String ,imageColor : UIColor,padding : CGFloat){
        self.setImage(UIImage(systemName: systemName)?.withTintColor(imageColor, renderingMode: .alwaysOriginal), for: .normal)
        self.setPreferredSymbolConfiguration(UIImage.SymbolConfiguration(pointSize: 24), forImageIn: .normal)
        self.imageView?.translatesAutoresizingMaskIntoConstraints = false
        self.imageView?.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        self.imageView?.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: padding).isActive = true
        self.imageView?.contentMode = .right
        self.semanticContentAttribute = .forceRightToLeft
    }
    
    
    
}

extension UIButton{
    func addCountBatch(batchColor : UIColor , size : CGFloat){
        let countBatch = UILabel(frame: CGRect(x: self.frame.maxX - size, y: 0, width: size, height: size))
        countBatch.tag = 888
        countBatch.textColor = .white
        countBatch.textAlignment = .center
        countBatch.adjustsFontSizeToFitWidth = true
        countBatch.layer.cornerRadius = 12.5
        countBatch.layer.masksToBounds = true
        countBatch.backgroundColor = batchColor.withAlphaComponent(0.9)
        countBatch.isHidden = true
        self.addSubview(countBatch)
    }
    
    func updateCountBatchAndModifyItBasedOnTheCount(count : Int){
        let countBatch =  self.viewWithTag(888) as! UILabel
        countBatch.text = String(count)
        if(count == 0){
            countBatch.isHidden = true
        }
        else{
            countBatch.isHidden = false
        }
    }
    
}

extension UIButton{
    func addBlurEffect()
        {
            let blur = UIVisualEffectView(effect: UIBlurEffect(style: .light))
            blur.frame = self.bounds
            blur.layer.cornerRadius = 10
            blur.isUserInteractionEnabled = false
            self.insertSubview(blur, at: 0)
            if let imageView = self.imageView{
                self.bringSubviewToFront(imageView)
            }
        }
    func removeBlurEffect(){
        for subview in self.subviews {
            if subview is UIVisualEffectView {
                subview.removeFromSuperview()
            }
        }
    }
}
