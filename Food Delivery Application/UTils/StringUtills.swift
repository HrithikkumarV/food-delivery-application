//
//  StringUtills.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 14/12/21.
//

import Foundation

import UIKit

extension String {
    
   func isValidEmail() -> Bool {
      let regularExpressionForEmail = "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}$"
      let testEmail = NSPredicate(format:"SELF MATCHES %@", regularExpressionForEmail)
       return testEmail.evaluate(with: self)
   }
   func isValidPhoneNumber() -> Bool {
      let regularExpressionForPhoneNumber = "^\\d{10}$"
      let testPhoneNumber = NSPredicate(format:"SELF MATCHES %@", regularExpressionForPhoneNumber)
      return testPhoneNumber.evaluate(with: self)
   }
    
    func isValidPassword() -> Bool {
       let regularExpressionForPhone = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,15}$"
       let testPassword = NSPredicate(format:"SELF MATCHES %@", regularExpressionForPhone)
       return testPassword.evaluate(with: self)
    }
    
    func encryptString() -> String{
        let password = self
        var encryptedPassword = ""
        for i in password{
            encryptedPassword.append(String(Character(UnicodeScalar(i.asciiValue! + 1))))
        }
        return encryptedPassword
    }
    
    func decryptString() -> String{
        let password = self
        var decryptedPassword = ""
        for i in password{
            decryptedPassword.append(String(Character(UnicodeScalar(i.asciiValue! - 1))))
        }
        return decryptedPassword
    }
    
    
}
