//
//  TextViewPlaceHolderUtils.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 23/01/22.
//

import Foundation
import UIKit

class TextViewPlaceHolderUtils : NSObject, UITextViewDelegate{
    
    private var targetTextView : UITextView!
    private var placeHolderText : String!
    private var descriptionLabelText: String!
    private var descriptionLabel : UILabel?
    private var view : UIView!
    private var placeHolderColor : UIColor!
    private var fontSizeForLabel : CGFloat!
    
    init(targetTextView : UITextView , placeHolderText : String, descriptionLabelText : String,fontSizeForLabel : CGFloat,view : UIView,placeHolderColor : UIColor){
        super.init()
        self.targetTextView = targetTextView
        self.placeHolderText = placeHolderText
        self.descriptionLabelText = descriptionLabelText
        self.view = view
        self.placeHolderColor = placeHolderColor
        self.fontSizeForLabel = fontSizeForLabel
        targetTextView.text = placeHolderText
        targetTextView.textColor = placeHolderColor
        
    }
    
    func addLabelToTopBorder(frame : CGRect){
        print(frame)
        if(descriptionLabel == nil){
            descriptionLabel = UILabel()
        }
        descriptionLabel?.frame = CGRect(x: frame.minX + 15, y: frame.minY - 12, width: frame.size.width - 15, height: fontSizeForLabel)
        descriptionLabel?.textAlignment = .left
        descriptionLabel?.text = " " + descriptionLabelText + " "
        descriptionLabel?.textColor = .lightGray
        descriptionLabel?.backgroundColor = .white
        descriptionLabel?.sizeToFit()
        descriptionLabel?.isHidden = true
        view.addSubview(descriptionLabel!)
    }
    
    func textDidChange(){
        if targetTextView.text!.isEmpty{
            hideTopBorderLabel()
        }
        else{
            showTopBorderLabel()
        }
    }
    func hideTopBorderLabel(){
        descriptionLabel?.isHidden = true

    }
    func showTopBorderLabel(){
        
        descriptionLabel?.isHidden = false
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if let cursorPosition = textView.position(from: textView.beginningOfDocument, offset: 1){
            textView.selectedTextRange = textView.textRange(from: cursorPosition, to:cursorPosition )
        }
        if let cursorPosition = textView.position(from: textView.beginningOfDocument, offset: 0){
            textView.selectedTextRange = textView.textRange(from: cursorPosition, to:cursorPosition )
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        textDidChange()
        if(textView == targetTextView){
            if (targetTextView.textColor == placeHolderColor) {
                var text : String! = textView.text
                if(text.contains(placeHolderText)){
                    text = text.replacingOccurrences(of: placeHolderText, with: "", options: [.caseInsensitive, .regularExpression])
                    targetTextView.text = text
                    targetTextView.textColor = .black
                }
            }
            
            if (targetTextView.text == ""){
                targetTextView.text = placeHolderText
                targetTextView.textColor = placeHolderColor
                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            }
            
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        textDidChange()
        if targetTextView.text == "" {
            targetTextView.text = placeHolderText
            targetTextView.textColor = placeHolderColor
        }
        
        textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if(textView == targetTextView){
            if(textView.text == placeHolderText && textView.textColor == placeHolderColor){
                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            }
        }
    }
}
