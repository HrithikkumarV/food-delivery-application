//
//  TableCiewCellUtils.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 11/04/22.
//

import Foundation
import UIKit
extension UITableViewCell {
     func removeSubviews(){
        for subview in self.subviews{
            subview.removeFromSuperview()
        }
    }

}
