//
//  ToolBarUtils.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 26/01/22.
//

import Foundation
import UIKit

class ToolBarUtils{
    func createBarButton(title : String) ->  UIBarButtonItem {
        
        let nextButton = UIBarButtonItem()
        nextButton.title = title
        nextButton.style = .done
        return nextButton
    }
    
    func createKeyboardToolBar() -> UIToolbar{
        let keyboardToolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        keyboardToolBar.sizeToFit()
        return keyboardToolBar
    }
    
    func createFlexibleSpaceForToolBar() -> UIBarButtonItem{
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        return flexibleSpace
    }
}
