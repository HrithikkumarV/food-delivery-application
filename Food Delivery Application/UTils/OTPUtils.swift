//
//  SMSUtils.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 16/12/21.
//

import Foundation

class OTPUtils{
    
    static func sendOTP(message: String , phoneNumber : String){
        var dataTask : URLSessionDataTask?
        dataTask?.cancel()
        let url = URL(string: "https://www.fast2sms.com/dev/bulkV2")
        var request = URLRequest(url: url!)
        let header = ["authorization": "2krxTyjFlWGc6uNZAEiDvVliziEtYHrWXVjxtfTp3uvixoGZdipTddNLZhRq"]
        request.allHTTPHeaderFields = header
        request.httpBody = "variables_values=\(message)&route=otp&numbers=\(phoneNumber)".data(using: String.Encoding.utf8)
        request.httpMethod = "POST"
        dataTask = URLSession.shared.dataTask(with: request) { (data,response, error) in
            if error == nil && data != nil {
                do {
                    let dic  = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String : Any]
                    print(dic as Any)
                }
                catch{
                    print("Error parsing json")
                }
            }
        }
        dataTask?.resume()
        
        print(message,phoneNumber)

    }
    
    
    
}
