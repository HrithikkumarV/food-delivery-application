//
//  GestureUtils.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 19/04/22.
//

import UIKit

extension UIViewController : UIGestureRecognizerDelegate{
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
            return true
        }
}
