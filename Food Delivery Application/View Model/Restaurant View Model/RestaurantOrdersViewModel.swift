//
//  RestaurantOrdersViewModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 14/03/22.
//

import Foundation

class RestaurantOrdersViewModel {
    private let restaurantOrdersDAO : RestaurantOrdersDAOProtocol  = InjectDAOUtils.getRestaurantOrdersDAO()
    private var activeOrderIdList : [String] = []
    private var activeOrderDetailsInDictionaryFormat : [String : OrderDetails] = [:]
    private var activeOrderFoodDetails : [String : [OrderMenuDetails]] = [:]
    
    private var pastOrderIdList : [String] = []
    private var pastOrderDetailsInDictionaryFormat : [String : OrderDetails] = [:]
    private var pastOrderFoodDetails : [String : [OrderMenuDetails]] = [:]
    
    func updateOrderStatus(orderId : String, orderStatus : OrderStatus)  -> Bool{
        do{
            return try restaurantOrdersDAO.updateOrderStatus(orderId: orderId, orderStatus: orderStatus)
            
        }
        catch {
            print(error)
        }
        return false
    }
    
    func getMenuDetailsInOrderFoodDetails(orderId: String) -> [OrderMenuDetails]{
        do{
            return try restaurantOrdersDAO.getMenuDetailsInOrderFoodDetails(orderId: orderId)
            
        }
        catch {
            print(error)
        }
        return []
    }
    
    func getMenuDetailsInActiveOrderFoodDetails(restaurantId : Int)  -> [String : [OrderMenuDetails]]{
        do{
            activeOrderFoodDetails  = [:]
            activeOrderFoodDetails = try restaurantOrdersDAO.getMenuDetailsInActiveOrderFoodDetails(restaurantId: restaurantId)
           
            
        }
        catch {
            print(error)
        }
        return activeOrderFoodDetails
    }
    
    func getMenuDetailsInpastOrderFoodDetails(orderIdList : [String])  -> [String: [OrderMenuDetails]]{
        do{
            pastOrderFoodDetails  = [:]
            let orderIdListString = convertOrderIdListToOrderIdListString(orderIdList: orderIdList)
            pastOrderFoodDetails = try restaurantOrdersDAO.getMenuDetailsInpastOrderFoodDetails(orderIdListString: orderIdListString)
            print(pastOrderFoodDetails,orderIdListString)
                    }
        catch {
            print(error)
        }
        
        return pastOrderFoodDetails
    }
    
    private func convertOrderIdListToOrderIdListString(orderIdList : [String]) -> String{
        var orderIdListString : String = "("
        for index in 0..<orderIdList.count{
            orderIdListString += "'\(orderIdList[index])'"
            if(index < orderIdList.count-1){
                orderIdListString += ","
            }
        }
        orderIdListString += ")"
        return orderIdListString
    }
    
    func getActiveOrderDetails(restaurantId : Int) -> (activeOrderDetailsInDictionaryFormat : [String :OrderDetails], activeOrderIdList : [String]){
        do{
            let orderDetails =  try restaurantOrdersDAO.getActiveOrderDetails(restaurantId: restaurantId)
            activeOrderIdList = []
            activeOrderDetailsInDictionaryFormat  = [:]
            for orders in orderDetails{
                activeOrderIdList.append(orders.orderModel.orderId)
                activeOrderDetailsInDictionaryFormat[orders.orderModel.orderId] = orders
            }
            return (activeOrderDetailsInDictionaryFormat : activeOrderDetailsInDictionaryFormat, activeOrderIdList : activeOrderIdList)
        }
        catch {
            print(error)
        }
        
        return (activeOrderDetailsInDictionaryFormat : [:], activeOrderIdList : [])
    }
    
    func getpastOrderDetails(restaurantId : Int , offSet : Int , limit : Int)  -> (pastOrderDetailsInDictionaryFormat : [String :OrderDetails], pastOrderIdList : [String]){
        do{
            let orderDetails =  try restaurantOrdersDAO.getpastOrderDetails(restaurantId: restaurantId, offSet: offSet, limit: limit)
            pastOrderIdList = []
            pastOrderDetailsInDictionaryFormat  = [:]
            for orders in orderDetails{
                pastOrderIdList.append(orders.orderModel.orderId)
                pastOrderDetailsInDictionaryFormat[orders.orderModel.orderId] = orders
            }
            return (pastOrderDetailsInDictionaryFormat : pastOrderDetailsInDictionaryFormat, pastOrderIdList : pastOrderIdList)
        }
        catch {
            print(error)
        }
        
        return (pastOrderDetailsInDictionaryFormat : [:], pastOrderIdList : [])
    }
    
    func getSplitOrderIdsBasedOnTheOrderStatus() -> [OrderStatus : [String]]{
        var splitOrderIdsBasedOnOrderStatus : [OrderStatus : [String]] = [:]
        for orderId in activeOrderIdList{
            splitOrderIdsBasedOnOrderStatus[(activeOrderDetailsInDictionaryFormat[orderId]!.orderStatus)] = splitOrderIdsBasedOnOrderStatus[activeOrderDetailsInDictionaryFormat[orderId]!.orderStatus] ?? []
            splitOrderIdsBasedOnOrderStatus[activeOrderDetailsInDictionaryFormat[orderId]!.orderStatus]!.append(orderId)
        }
        print(activeOrderDetailsInDictionaryFormat,activeOrderFoodDetails,activeOrderIdList,splitOrderIdsBasedOnOrderStatus)
        return splitOrderIdsBasedOnOrderStatus
    }
    
    
   
    
    func getMenuNamesAndQuantityList(orderId : String) -> [(menuName: String, quantity: Int)]{
        var menuNamesAndQuantityList : [(menuName: String, quantity: Int)] = []
        for menuDetailsInOrder in activeOrderFoodDetails[orderId]!{
            menuNamesAndQuantityList.append((menuName: menuDetailsInOrder.menuName, quantity: menuDetailsInOrder.quantity))
        }
        return menuNamesAndQuantityList
        
    }
    
    
    func getMenuNamesAndQuantityListInPastOrders(pastOrderFoodDetails : [OrderMenuDetails]) -> [(menuName: String, quantity: Int)]{
        var menuNamesAndQuantityList : [(menuName: String, quantity: Int)] = []
        for menuDetailsInOrder in pastOrderFoodDetails{
            menuNamesAndQuantityList.append((menuName: menuDetailsInOrder.menuName, quantity: menuDetailsInOrder.quantity))
        }
        return menuNamesAndQuantityList
        
    }
    
    func getUserDetailsOfOrder(orderId : String) -> UserDetails{
        do{
            return try restaurantOrdersDAO.getUserDetails(orderId: orderId)
        }
        catch {
            print(error)
        }
        return UserDetails()
    }
    
    
    func getBillDetails(orderId : String) -> BillDetailsModel{
        do{
            return try restaurantOrdersDAO.getBillDetailsOfOrder(orderId: orderId)
        }
        catch {
            print(error)
        }
        return BillDetailsModel()
    }
    
    func updateOrderCancellationReason(orderId: String, orderCancellationReason: String) -> Bool{
        do{
            return try restaurantOrdersDAO.updateOrderCancellationReason(orderId: orderId, orderCancellationReason: orderCancellationReason)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func getOrderCancellationReason(orderId: String) -> String{
        do{
            return try restaurantOrdersDAO.getOrderCancellationReason(orderId: orderId)
        }
        catch {
            print(error)
        }
        return ""
    }
}
