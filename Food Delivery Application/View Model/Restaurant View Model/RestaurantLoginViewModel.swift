//
//  RestaurentLoginViewModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 16/12/21.
//

import Foundation

class RestaurantLoginViewModel{
    
    private let restaurantLoginDAO : RestaurantLoginDAOProtocol = InjectDAOUtils.getRestaurantAccountDAO()
    
    func getRestaurantLoginPassword(phoneNumber: String) -> String{
        do{
            return try restaurantLoginDAO.getRestaurantPassword(phoneNumber: phoneNumber)
        }
        catch {
            print(error)
        }
        return ""
    }
    
    func getRestaurantId(phoneNumber : String) -> Int{
        do{
            return try restaurantLoginDAO.getRestaurantId(phoneNumber: phoneNumber)
        }
        catch {
            print(error)
        }
        return 0
    }
    
}
