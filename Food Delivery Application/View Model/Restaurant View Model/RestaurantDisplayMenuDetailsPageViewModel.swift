//
//  RestaurantDisplayMenuDetailsPageViewModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/02/22.
//

import Foundation
class RestaurantDisplayMenuDetailsPageViewModel{
    private let displayMenuDetailsDAO : RestaurantDisplayMenuDetailsPageDAOProtocol  = InjectDAOUtils.getMenuDetailsDAO()
    private var menuContentDetails : [MenuContentDetails] = []
    private var menuCategories : [(categoryId : Int , categoryName : String)] = []
    
    private func getMenuDetailsofRestaurantFromDB(restaurantId :Int) {
        do{
            try menuContentDetails =  displayMenuDetailsDAO.getMenuDetailsofRestaurant(restaurantId: restaurantId)
        }
        catch {
            print(error)
        }

    }
    
    func removeMenu(menuId : Int) -> Bool{
        do{
            return try  displayMenuDetailsDAO.removeMenu(menuId: menuId)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func removeMenuOfCategory(categoryId: Int) -> Bool{
        do{
            return try  displayMenuDetailsDAO.removeMenuOfCategory(categoryId: categoryId)
        }
        catch {
            print(error)
        }
        return false
    }
    
    
    func removeCategory(categoryId : Int) -> Bool{
        do{
            return try  displayMenuDetailsDAO.removeCategory(categoryId: categoryId)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func updateCategory(categoryId : Int,categoryName : String) -> Bool{
        do{
            return try  displayMenuDetailsDAO.UpdateCategoryName(CategoryName: categoryName, categoryId: categoryId)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func persistMenuCategoryType(restaurantId : Int , categoryType : String) -> Bool{
        do{
            return try displayMenuDetailsDAO.persistMenuCategory(restaurantId: restaurantId, categoryType: categoryType)
            
        }
        catch {
            print(error)
        }
        return false
    }
    
    func checkIfCategoryExistsInTheList(categoryName : String) -> Bool{
        for i in 0 ..< menuCategories.count{
            if(menuCategories[i].categoryName == categoryName){
                return true
            }
        }
        
        return false
    }
    
    func getMenuContents(restaurantId : Int) -> (menuContentsInDictionaryFormat : [ Int : MenuContentDetails] , orderedKeys : [Int]){
        getMenuDetailsofRestaurantFromDB(restaurantId : restaurantId)
        var menuContentsInDictionaryFormat : [ Int : MenuContentDetails]  = [:]
        var orderedKey : [Int] = []
        for menu in menuContentDetails{
            menuContentsInDictionaryFormat[menu.menuId] = menu
            orderedKey.append(menu.menuId)
        }
        return (menuContentsInDictionaryFormat : menuContentsInDictionaryFormat, orderedKeys : orderedKey)
    }
    
    func getMenuCategoryTypes(restaurantId : Int) -> [(categoryId : Int , categoryName : String)] {
        do{
            try menuCategories = displayMenuDetailsDAO.getCategoryDetails(restaurantId: restaurantId)
            
        }
        catch {
            print(error)
        }
        return menuCategories
    }
    
    
}
