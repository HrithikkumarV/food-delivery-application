//
//  ChangePasswordViewModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 20/12/21.
//


import Foundation


class RestaurantChangePasswordViewModel{
    private let restaurantChangePasswordDAO : RestaurantChangePasswordDAOProtocol = InjectDAOUtils.getRestaurantAccountDAO()
    private var restaurantPhoneNumber : String = ""
    
    func setPhoneNumber(phoneNumber : String){
        self.restaurantPhoneNumber = phoneNumber
    }
    
    func checkIfPhoneNumberExists(phoneNumber : String) -> Bool{
        do{
            return try restaurantChangePasswordDAO.checkIfPhoneNumberExists(phoneNumber: phoneNumber)
        }
        catch {
            print(error)
        }
        return true
    }
    
    func updatePassword(password : String) -> Bool{
        do{
           return try restaurantChangePasswordDAO.UpdatePassword(phoneNumber: restaurantPhoneNumber, password: password)
        }
        catch {
            print(error)
        }
        return false
    }
    
}
