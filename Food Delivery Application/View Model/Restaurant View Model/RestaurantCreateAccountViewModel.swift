//
//  restaurantRegistrationRequest.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 27/10/21.
//

import Foundation
import UIKit

class RestaurantCreateAccountViewModel{
    private var restaurantDetails : RestaurantDetails!
    private var restaurantAddressDetails : AddressDetails!
    private var restaurantAccount : RestaurantAccount = RestaurantAccount()
    private let restaurantCreateAccountDAO : RestaurantCreateAccountDAOProtocol = InjectDAOUtils.getRestaurantAccountDAO()
    
    
    func setPhoneNumber(phoneNumber : String){
        restaurantAccount.restaurantPhoneNumber =  phoneNumber
    }
    
    func setRestaurantDetails(restaurentDetails : RestaurantDetails){
        self.restaurantDetails = restaurentDetails
    }
    
    func setRestaurentAddress(restaurentAddress : AddressDetails){
        self.restaurantAddressDetails = restaurentAddress
    }
    
    func setRestaurantPassword(Password : String){
        restaurantAccount.restaurantPassword =  Password
    }
    
    func checkIFphoneNumberExists(phoneNumber : String) -> Bool{
        do{
            return try restaurantCreateAccountDAO.checkIfPhoneNumberExists(phoneNumber: phoneNumber)
        }
        catch {
            print(error)
        }
        return true
    }
    
    
    
    func restaurantCreateAccount() -> Bool{
        do{
            return try restaurantCreateAccountDAO.persistRestaurantCreateAccount(restaurantAccount : restaurantAccount, restaurantDetails: restaurantDetails, restaurantAddress: restaurantAddressDetails)
        }
        catch {
            print(error)
        }
        return false
    }
    
    
    
    func getSecretCode(completion: @escaping (_ code : String?) -> Void){
        APICallerManager.shared.getSecretCode { code in
            completion(code)
        }
    }
    
    func getRestaurantId() -> Int{
        do{
            return try restaurantCreateAccountDAO.getRestaurantId(phoneNumber: restaurantAccount.restaurantPhoneNumber)
        }
        catch {
            print(error)
        }
        return 0
    }
    
}
