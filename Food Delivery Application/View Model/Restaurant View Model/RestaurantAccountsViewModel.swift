//
//  RestaurantAccountsViewModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 19/03/22.
//

import Foundation

class RestaurantAccountsViewModel{
    private let restaurantAccountsDAO : RestaurantAccountsPageDAOProtocol  = InjectDAOUtils.getRestaurantAccountDAO()
    
    func getRestaurantDetails(restaurantId : Int) -> RestaurantContentDetails{
        do{
            return try restaurantAccountsDAO.getRestaurantDetails(restaurantId: restaurantId)
        }
        catch {
            print(error)
        }
        return RestaurantContentDetails()
    }
    
    func updateRestaurantName(restaurantID: Int, restaurantName: String) -> Bool{
        do{
            return try restaurantAccountsDAO.updateRestaurantName(restaurantID: restaurantID, restaurantName: restaurantName)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func updateRestaurantImage(restaurantID: Int, profileImage: Data) -> Bool{
        do{
            return try restaurantAccountsDAO.updateRestaurantProfileImage(restaurantID: restaurantID, profileImage: profileImage)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func updateRestaurantCuisine(restaurantID: Int, restaurantCuisine: String) -> Bool{
        do{
            return try restaurantAccountsDAO.updateRestaurantCuisine(restaurantID: restaurantID, restaurantCuisine: restaurantCuisine)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func updateRestaurantDescription(restaurantID: Int, restaurantDescription: String) -> Bool{
        do{
            return try restaurantAccountsDAO.updateRestaurantDescription(restaurantID: restaurantID, restaurantDescription: restaurantDescription)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func updateRestaurantPhoneNumber(restaurantID: Int, restaurantPhoneNumber: String) -> Bool{
        do{
            return try restaurantAccountsDAO.updateRestaurantPhoneNumber(restaurantID: restaurantID, restaurantPhoneNumber: restaurantPhoneNumber)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func updateRestaurantAddress(restaurantID: Int, restaurantAddress: AddressDetails) -> Bool{
        do{
            return try restaurantAccountsDAO.updateRestaurantAddress(restaurantID: restaurantID, restaurantAddress: restaurantAddress)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func updateRestaurantAvailablityAndNextOpensAt(restaurantID: Int, restaurantIsAvailable: Int, restaurantOpensNextAt: String) -> Bool{
        do{
            return try restaurantAccountsDAO.updateRestaurantAvailablityAndNextOpensAt(restaurantID: restaurantID, restaurantIsAvailable: restaurantIsAvailable, restaurantOpensNextAt: restaurantOpensNextAt)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func updateRestaurantFoodPackagingCharges(restaurantID: Int, restaurantFoodPackagingCharge: Int) -> Bool{
        do{
            return try restaurantAccountsDAO.updateRestaurantFoodPackagingCharges(restaurantID: restaurantID, restaurantFoodPackagingCharge: restaurantFoodPackagingCharge)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func updateRestaurantStatus(restaurantID: Int , restaurantStatus: Int) -> Bool{
        do{
            return try restaurantAccountsDAO.updateRestaurantStatus(restaurantID: restaurantID, restaurantStatus: restaurantStatus)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func getSecretCode(completion: @escaping (_ code : String?) -> Void){
        APICallerManager.shared.getSecretCode { code in
            completion(code)
        }
    }
    
    
    
}
