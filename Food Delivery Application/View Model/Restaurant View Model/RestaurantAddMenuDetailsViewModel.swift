//
//  RestaurantAddMenuViewModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 29/01/22.
//

import Foundation

class RestaurantAddAndEditMenuDetailsViewModel{
    private let menuDetailsDAO : AddAndEditMenuDetailsDAOProtocol! = InjectDAOUtils.getMenuDetailsDAO()
    private var categoryList : [(categoryId : Int , categoryName : String)] = []
    
    
    func persistMenuDetails(menuDetails : MenuDetails) -> Bool{
        do{
            return try menuDetailsDAO.persistMenuDetails(menuDetails:  menuDetails)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func updateMenuDetails(menuContentDetails : MenuContentDetails) -> Bool{
        do{
            return try menuDetailsDAO.updateMenuDetails(menuContentDetails: menuContentDetails)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func persistMenuCategoryType(restaurantId : Int , categoryType : String) -> Bool{
        do{
            return try menuDetailsDAO.persistMenuCategory(restaurantId: restaurantId, categoryType: categoryType)
            
        }
        catch {
            print(error)
        }
        return false
    }
    
    func checkIfCategoryExistsInTheList(categoryName : String) -> Bool{
        for i in 0 ..< categoryList.count{
            if(categoryList[i].categoryName == categoryName){
                return true
            }
        }
        
        return false
    }
    
    func getCategoryId(categoryName : String , restaurantId : Int) -> Int{
        do{
            return try menuDetailsDAO.getCategoryId(categoryName: categoryName, restaurantId: restaurantId)
            
        }
        catch {
            print(error)
        }
        return 0
    }
    
    func getCategoryDropDownListDetails(restaurantId : Int) -> [(ID : Int , name : String)]{
        var dropDownList : [(ID : Int , name : String)] = []
        
        do{
            categoryList = try menuDetailsDAO.getCategoryDetails(restaurantId: restaurantId)
            for i in 0 ..< categoryList.count{
                dropDownList.append((ID: categoryList[i].categoryId, name: categoryList[i].categoryName))
            }
            dropDownList.append((ID: 0, name: "Add New Category"))
            return dropDownList
        }
        catch {
            print(error)
        }
        
        return dropDownList
    }
}
