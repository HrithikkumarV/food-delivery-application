//
//  UserWelcomePageViewModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 01/02/22.
//

import Foundation

class UserWelcomePageViewModel{
    private let userWelcomePageDAO : UserWelcomePageDAOProtocol  = InjectDAOUtils.getUserAccountDAO()
    
    func getUserName(userId : Int) -> String{
        do{
            let userDetails =  try userWelcomePageDAO.getUserDetails(userId: userId)
            return userDetails.userName
        }
        catch {
            print(error)
        }
        return ""
    }
}
