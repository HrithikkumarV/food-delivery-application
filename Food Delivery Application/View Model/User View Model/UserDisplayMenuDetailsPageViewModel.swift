//
//  UserMenuDisplayPageViewModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 02/02/22.
//

import Foundation

class UserDisplayMenuDetailsPageViewModel{
    private let displayMenuDetailsDAO : UserDisplayMenuDetailsPageDAOProtocol  = InjectDAOUtils.getMenuDetailsDAO()
    private var menuContentDetails : [MenuContentDetails] = []
    
    private let favouriteRestaurantViewModel = FavouriteRestaurantViewModel()
    
    private func getMenuDetailsofRestaurantFromDB(restaurantId :Int) {
        do{
            try menuContentDetails =  displayMenuDetailsDAO.getMenuDetailsofRestaurantForUser(restaurantId: restaurantId)
        }
        catch {
            print(error)
        }

    }
    
    func isFavouriteRestaurant(restaurantId : Int) -> Bool{
        return favouriteRestaurantViewModel.isFavouriteRestaurant(restaurantId: restaurantId)
    }
    
    
    private func sortMenusBasedOnAvailability(){
        var index = 0
        while( index < menuContentDetails.count){
            if((menuContentDetails[index].menuIsAvailable) == 0){
                let menuContentDetail = menuContentDetails[index]
                menuContentDetails.remove(at: index)
                menuContentDetails.append(menuContentDetail)
            }
            index += 1
        }
    }
    
    func getMenuContents(restaurantId : Int) -> (menuContentsInDictionaryFormat : [ Int : MenuContentDetails] , orderedKeys : [Int]){
        getMenuDetailsofRestaurantFromDB(restaurantId : restaurantId)
        sortMenusBasedOnAvailability()
        var menuContentsInDictionaryFormat : [ Int : MenuContentDetails]  = [:]
        var orderedKey : [Int] = []
        for menu in menuContentDetails{
            menuContentsInDictionaryFormat[menu.menuId] = menu
            orderedKey.append(menu.menuId)
        }
        return (menuContentsInDictionaryFormat : menuContentsInDictionaryFormat, orderedKeys : orderedKey)
    }
    
    func getMenuCategoryTypes(restaurantId : Int) -> [(categoryId : Int , categoryName : String)] {
        do{
            return try  displayMenuDetailsDAO.getCategoryDetails(restaurantId: restaurantId)
            
        }
        catch {
            print(error)
        }
        return []
    }
    
    
    
    func addToFavouriteRestaurants(restaurantId : Int) -> Bool{
        return favouriteRestaurantViewModel.persistFavouriteRestaurant(userId: (UserIDUtils.shared.getUserId()), restaurantId: restaurantId)
    }
    
    func removeFromFavouriteRestaurants(restaurantId : Int) -> Bool{
        return favouriteRestaurantViewModel.removeFromFavouriteRestaurants(restaurantId: restaurantId)
    }
}
