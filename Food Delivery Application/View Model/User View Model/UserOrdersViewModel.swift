//
//  UserOrdersViewModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 15/03/22.
//

import Foundation

class UserOrdersViewModel{
    private let userOrdersDAO : UserOrdersDAOProtocol = InjectDAOUtils.getUserOrdersDAO()
    private var activeOrderIdList : [String] = []
    private var activeOrderDetailsInDictionaryFormat : [String : OrderDetails] = [:]
    private var activeOrderFoodDetails : [String : [OrderMenuDetails]] = [:]
    private var pastOrderIdList : [String] = []
    private var pastOrderDetailsInDictionaryFormat : [String : OrderDetails] = [:]
    private var pastOrderFoodDetails : [String : [OrderMenuDetails]] = [:]
    
    func persistMenuToOrderedFoodDetails(menuId: Int, orderId: String, quantity: Int) -> Bool{
        do{
            return try userOrdersDAO.persistMenuToOrderedFoodDetails(orderedFoodDetails: (menuId: menuId, orderId: orderId, quantity: quantity))
        }
        catch {
            print(error)
        }
        return false
    }
    
    func persistOrderDetails(orderDetails : OrderModel) -> Bool{
        do{
            return try userOrdersDAO.persistOrderDetails(orderDetails: orderDetails)
        }
        catch {
            print(error)
        }
        
        return false
    }
    
    func persistPaymentDetails(paymentDetails : PaymentDetails) -> Bool{
        do{
            return try userOrdersDAO.persistPaymentDetails(paymentDetails: paymentDetails)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func persistTackOrderDetails(orderStatus: OrderStatus, orderId: String) -> Bool{
        do{
            return try userOrdersDAO.persistTrackOrderDetails(orderStatus: orderStatus, orderId: orderId)
        }
        catch {
            print(error)
        }
        return false
    }
    
    
    func persistInstructionToRestaurantDetails(instructionToRestaurant: String, orderId: String) -> Bool{
        do{
            return try userOrdersDAO.persistInstructionToRestaurantDetails(instructionToRestaurant: instructionToRestaurant, orderId: orderId)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func updateOrderCancellationReason(orderId: String, orderCancellationReason: String) -> Bool{
        do{
            return try userOrdersDAO.updateOrderCancellationReason(orderId: orderId, orderCancellationReason: orderCancellationReason)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func getOrderCancellationReason(orderId: String) -> String{
        do{
            return try userOrdersDAO.getOrderCancellationReason(orderId: orderId)
        }
        catch {
            print(error)
        }
        return ""
    }
    
    
    func getActiveOrderDetails(userId : Int) -> (activeOrderDetailsInDictionaryFormat : [String :OrderDetails], activeOrderIdList : [String]){
        do{
            let orderDetails =  try userOrdersDAO.getActiveOrderDetails(userId: userId)
            activeOrderIdList  = []
            activeOrderDetailsInDictionaryFormat = [:]
            for orders in orderDetails{
                activeOrderIdList.append(orders.orderModel.orderId)
                activeOrderDetailsInDictionaryFormat[orders.orderModel.orderId] = orders
            }
            return (activeOrderDetailsInDictionaryFormat : activeOrderDetailsInDictionaryFormat, activeOrderIdList : activeOrderIdList)
        }
        catch {
            print(error)
        }
        
        return (activeOrderDetailsInDictionaryFormat : [:], activeOrderIdList : [])
    }
    
    func getpastOrderDetails(userId : Int , offSet : Int , limit : Int)  -> (pastOrderDetailsInDictionaryFormat : [String :OrderDetails], pastOrderIdList : [String]){
        do{
            let orderDetails =  try userOrdersDAO.getpastOrderDetails(userId: userId, offSet: offSet, limit: limit)
            pastOrderIdList  = []
            pastOrderDetailsInDictionaryFormat  = [:]
            
            for orders in orderDetails{
                pastOrderIdList.append(orders.orderModel.orderId)
                pastOrderDetailsInDictionaryFormat[orders.orderModel.orderId] = orders
            }
            return (pastOrderDetailsInDictionaryFormat : pastOrderDetailsInDictionaryFormat, pastOrderIdList : pastOrderIdList)
        }
        catch {
            print(error)
        }
        
        return (pastOrderDetailsInDictionaryFormat : [:], pastOrderIdList : [])
    }
    
    func getMenuDetailsInActiveOrderFoodDetails(userId : Int)  -> [ String: [OrderMenuDetails]]{
        do{
            activeOrderFoodDetails = [:]
            activeOrderFoodDetails = try userOrdersDAO.getMenuDetailsInActiveOrderFoodDetails(userId: userId)
        }
        catch {
            print(error)
        }
        
        return activeOrderFoodDetails
    }
    
    
    func getMenuDetailsInpastOrderFoodDetails(orderIdList : [String])  -> [String: [OrderMenuDetails]]{
        do{
            pastOrderFoodDetails  = [:]
            let orderIdListString = convertOrderIdListToOrderIdListString(orderIdList: orderIdList)
            pastOrderFoodDetails = try userOrdersDAO.getMenuDetailsInpastOrderFoodDetails(orderIdListString: orderIdListString)
        }
        catch {
            print(error)
        }
        
        return pastOrderFoodDetails
    }
    
    private func convertOrderIdListToOrderIdListString(orderIdList : [String]) -> String{
        var orderIdListString : String = "("
        for index in 0..<orderIdList.count{
            orderIdListString += "'\(orderIdList[index])'"
            if(index < orderIdList.count-1){
                orderIdListString += ","
            }
        }
        orderIdListString += ")"
        return orderIdListString
    }
    
    func getSplitOrderIdsBasedOnTheOrderStatus() -> [OrderStatus : [String]]{
        var splitOrderIdsBasedOnOrderStatus : [OrderStatus : [String]] = [:]
        for orderId in activeOrderIdList{
            splitOrderIdsBasedOnOrderStatus[(activeOrderDetailsInDictionaryFormat[orderId]!.orderStatus)] = splitOrderIdsBasedOnOrderStatus[activeOrderDetailsInDictionaryFormat[orderId]!.orderStatus] ?? []
            splitOrderIdsBasedOnOrderStatus[activeOrderDetailsInDictionaryFormat[orderId]!.orderStatus]!.append(orderId)
        }

        return splitOrderIdsBasedOnOrderStatus
    }
    
    func getMenuNamesAndQuantityListInActiveOrders(orderId : String) -> [(menuName: String, quantity: Int)]{
        var menuNamesAndQuantityList : [(menuName: String, quantity: Int)] = []
        for menuDetailsInOrder in activeOrderFoodDetails[orderId]!{
            menuNamesAndQuantityList.append((menuName: menuDetailsInOrder.menuName, quantity: menuDetailsInOrder.quantity))
        }
        return menuNamesAndQuantityList
        
    }
    
    func getMenuNamesAndQuantityListInPastOrders(pastOrderFoodDetails : [OrderMenuDetails]) -> [(menuName: String, quantity: Int)]{
        var menuNamesAndQuantityList : [(menuName: String, quantity: Int)] = []
        for menuDetailsInOrder in pastOrderFoodDetails{
            menuNamesAndQuantityList.append((menuName: menuDetailsInOrder.menuName, quantity: menuDetailsInOrder.quantity))
        }
        return menuNamesAndQuantityList
        
    }

    
    func persistBillDetails(billDetails : BillDetailsModel) -> Bool{
        do{
            return try userOrdersDAO.persistBillDetails(billDetails: billDetails)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func getBillDetails(orderId : String) -> BillDetailsModel{
        do{
            return try userOrdersDAO.getBillDetailsOfOrder(orderId: orderId)
        }
        catch {
            print(error)
        }
        return BillDetailsModel()
    }
    
    func updateOrderStatus(orderId : String, orderStatus : OrderStatus)  -> Bool{
        do{
            return try userOrdersDAO.updateOrderStatus(orderId: orderId, orderStatus: orderStatus)
            
        }
        catch {
            print(error)
        }
        return false
    }
    
    func getOrderStatus(orderId : String) -> OrderStatus{
        do{
            return try userOrdersDAO.getOrderStatus(orderId: orderId)
            
        }
        catch {
            print(error)
        }
        return OrderStatus.Pending
    }
    
    func getRestaurantDetails(restaurantId : Int) -> RestaurantContentDetails{
        do{
            return try userOrdersDAO.getRestaurantDetails(restaurantId: restaurantId)
        }
        catch {
            print(error)
        }
        return RestaurantContentDetails()
    }
    
    func updateStarRatingForFood(starRating : Int ,orderId : String) -> Bool{
        do{
            return try userOrdersDAO.updateFoodRating(orderId: orderId, starRating: starRating)
        }
        catch {
            print(error)
        }
        return false
    }
    
    
    func updateRestaurantStarRating(starRating : Int , restaurantId : Int) -> Bool{
        do{
            return try userOrdersDAO.updateRestaurantStarRating(starRating: starRating, restaurantId: restaurantId)
        }
        catch {
            print(error)
        }
        return false
    }
    
    
    func updateFeedBack(feedback : String , orderId : String) -> Bool{
        do{
            return try userOrdersDAO.updateFoodFeedback(orderId: orderId, feedback: feedback)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func createFoodratingRow(orderId : String) -> Bool{
        
        do{
            return try userOrdersDAO.createFoodRatingRow(orderId: orderId)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func clearStoredData(){
        activeOrderIdList  = []
        activeOrderDetailsInDictionaryFormat  = [:]
        activeOrderFoodDetails = [:]
        pastOrderIdList = []
        pastOrderDetailsInDictionaryFormat = [:]
        pastOrderFoodDetails = [:]
    }
}
