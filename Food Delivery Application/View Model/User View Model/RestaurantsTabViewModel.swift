//
//  RestaurantsTabViewModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 19/01/22.
//

import Foundation

class RestaurantsTabViewModel{
    
    private let restaurantsTabContentsDAO : RestaurantsTabDAOProtocol = DisplayRestaurantsDAO()
    private var restaurantsTabContentDetails : [RestaurantContentDetails]!
    private let favouriteRestaurantViewModel  = FavouriteRestaurantViewModel()
    
    
    private func getRestaurantsTabContentsFromDB(locality : String,pincode : String){
        do{
            try restaurantsTabContentDetails = restaurantsTabContentsDAO.getRestaurentsTabContentDetails(locality: locality, pincode: pincode)
        }
        catch {
            print(error)
        }
    }
    
    
    private func sortRestaurantsBasedOnFavouriteRestaurantsList(){
        var index = 0
        var favouriteRestaurantsInList : [RestaurantContentDetails] = []
        while( index < restaurantsTabContentDetails.count){
            if(favouriteRestaurantViewModel.isFavouriteRestaurant(restaurantId: restaurantsTabContentDetails[index].restaurantId)){
                let restaurantContentDetails = restaurantsTabContentDetails[index]
                restaurantsTabContentDetails.remove(at: index)
                favouriteRestaurantsInList.append(restaurantContentDetails)
            }
            else{
                index += 1
            }
        }
        restaurantsTabContentDetails = favouriteRestaurantsInList + restaurantsTabContentDetails
    }
    
    private func sortRestaurantsBasedOnAvailability(){
        var index = 0
        var unAvailableRestaurants : [RestaurantContentDetails] = []
        while(index < restaurantsTabContentDetails.count){
            if((restaurantsTabContentDetails[index].restaurantIsAvailable) == 0){
                let restaurantContentDetails = restaurantsTabContentDetails[index]
                restaurantsTabContentDetails.remove(at: index)
                unAvailableRestaurants.append(restaurantContentDetails)
            }
            else{
                index += 1
            }
        }
        restaurantsTabContentDetails = restaurantsTabContentDetails + unAvailableRestaurants
    }
    
    func getRestaurantsTabContents(locality : String,pincode : String) -> [RestaurantContentDetails]{
        restaurantsTabContentDetails = []
        getRestaurantsTabContentsFromDB(locality: locality, pincode: pincode)
        sortRestaurantsBasedOnFavouriteRestaurantsList()
        sortRestaurantsBasedOnAvailability()
        return restaurantsTabContentDetails
    }
}

