//
//  UserCreateAccountViewModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/12/21.
//

import Foundation

class UserCreateAccountViewModel{
    private let userCreateAccountDAO : UserCreateAccountDAOProtocol = InjectDAOUtils.getUserAccountDAO()
    private var userDetails : UserDetails!
    
    func ckeckIfPhoneNumberExists(phoneNumber : String) -> Bool{
        do{
             return try userCreateAccountDAO.checkIfPhoneNumberExists(phoneNumber: phoneNumber)
        }
        catch {
            print(error)
        }
        return true
    }
    
    func setUserDetails(userDetails : UserDetails){
        self.userDetails = userDetails
    }
    
    func createUserAccount() -> Bool {
        do{
              return try self.userCreateAccountDAO.persistUserAccountCreateAccountDetails(user: self.userDetails)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func getUserId(phoneNumber : String) -> Int {
        do{
            return try userCreateAccountDAO.getUserId(phoneNumber: phoneNumber)
        }
        catch {
            print(error)
        }
        return 0
    }
}
