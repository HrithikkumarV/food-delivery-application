//
//  UserAccountsViewModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 17/03/22.
//

import Foundation

class UserAccountsViewModel {
    private let userAccountsDAO : UserAccountsDAOProtocols = InjectDAOUtils.getUserAccountDAO()
    
    func getUserDetails(userId : Int) -> UserDetails{
        do{
            return try userAccountsDAO.getUserDetails(userId: userId)
        }
        catch {
            print(error)
        }
        return UserDetails()
    }
    
    func updateUserName(userId: Int, userName: String) -> Bool{
        do{
            return try userAccountsDAO.updateUserName(userId: userId, userName: userName)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func updateUserPhoneNumber(userId: Int, phoneNumber: String) -> Bool{
        do{
            return try userAccountsDAO.updateUserPhoneNumber(userId: userId, phoneNumber: phoneNumber)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func checkIfPhoneNumberExists(phoneNumber: String) -> Bool{
        do{
            return try userAccountsDAO.checkIfPhoneNumberExists(phoneNumber:phoneNumber)
        }
        catch {
            print(error)
        }
        return false
    }
}
