//
//  FavouriteRestaurantViewModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 15/02/22.
//

import Foundation

class FavouriteRestaurantViewModel{
    private let favouriteRestaurantDAO : FavouriteRestaurantDAOProtocol  = InjectDAOUtils.getFavouriteRestaurantDAO()
    private var favouriteRestaurants : [Int : Int] = [:]
    
   
        
        func persistFavouriteRestaurant(userId : Int , restaurantId : Int) -> Bool{
        do{
            return try favouriteRestaurantDAO.persistFavouriteRestaurant(userId: userId, restaurantId: restaurantId)
        }
        catch {
            print(error)
        }
        return false
    }
    
    private func deleteFavouriteRestaurant(favouriteRestaurantId : Int) -> Bool{
        do{
            return try favouriteRestaurantDAO.deleteFavouriteRestaurant(favouriteRestaurantId: favouriteRestaurantId)
        }
        catch {
            print(error)
        }
        return false
    }
    
    private func getFavouriteRestaurantIds(){
        do{
            return  favouriteRestaurants = try favouriteRestaurantDAO.getFavouriteRestaurantIds(userId: (UserIDUtils.shared.getUserId() ))
        }
        catch {
            print(error)
        }
    }
    
    
    
    func isFavouriteRestaurant(restaurantId : Int) -> Bool{
        getFavouriteRestaurantIds()
        if(favouriteRestaurants.values.firstIndex(of: restaurantId) != nil){
            return true
        }
        return false
    }
    
    func removeFromFavouriteRestaurants(restaurantId : Int) -> Bool{
        if let index =  favouriteRestaurants.values.firstIndex(of: restaurantId){
            let favouriteRestaurantId = favouriteRestaurants.keys[index]
            return deleteFavouriteRestaurant(favouriteRestaurantId: favouriteRestaurantId)
        }
        return false
    }
    
    func getFavouriteRestaurants(userId : Int) -> [RestaurantContentDetails]{
        do{
            return try favouriteRestaurantDAO.getFavouriteRestaurants(userId: userId)
        }
        catch {
            print(error)
        }
        return []
    }
}
