//
//  UserAddressesViewModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 22/12/21.
//

import Foundation

class UserAddressesViewModel{
    
    private let userAddressDAO : UserAddressDAOProtocol! = InjectDAOUtils.getUserAccountDAO()
    private let localPersistedUserAddressDAO: LocalPersistedUserAddressDAOProtocol! = InjectDAOUtils.getUserAccountDAO()
    
    func persistUserAddress(userId : Int ,userAddressDetails: UserAddressDetails) -> Bool{
        do{
           return try userAddressDAO.PersistUserAddress(userId: userId, userAddressDetails: userAddressDetails)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func getUserAddresses(userId : Int) -> [UserAddressDetails]{
        do{
            return try userAddressDAO.getUserAddresses(userId: userId)
        }
        catch {
            print(error)
        }
        return []
    }
    
    func UpdateUserAddress(userAddressDetails : UserAddressDetails) -> Bool{
        do{
            return try userAddressDAO.updateUserAddress(userAddressDetails: userAddressDetails)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func getUserAddress(userAddressId : String) -> UserAddressDetails{
            do{
                return try userAddressDAO.getUserAddress(userAddressId: userAddressId)
            }
            catch let error{
                print(error)
            }
                return UserAddressDetails()
        
    }
    
    func deleteUserAddress(userAddressId : String) -> Bool{
        do{
            return try userAddressDAO.deleteUserAddress(userAddressId: userAddressId)
        }
        catch {
            print(error)
        }
            return false
    }
    
    
    
    func getLocalPersistedUserAddress() -> UserAddressDetails{
        do{
            
            return try localPersistedUserAddressDAO.getLocalPersistedUserAddress()

            
        }
        catch {
            print(error)
        }
        return UserAddressDetails()
       
    }

    func deleteLocalPersistedUserAddress() -> Bool{
        
        do{
           return try localPersistedUserAddressDAO.deleteLocalPersistedUserAddress()
        }
        catch let error{
            print(error)
        }

        return false
    }
    
    func PersistLocalPersistedUserAddress(userAddressDetails  : UserAddressDetails) -> Bool{
        do{
            return try localPersistedUserAddressDAO.PersistLocalPersistedUserAddress(userAddressDetails: userAddressDetails)
           
        }
        catch let error{
            print(error)
        }
        return false
        
    }
    
    func updateUserAddressIdInLocalPersistedUserAddress(userAddressId : String)  -> Bool{
        do{
             return try localPersistedUserAddressDAO.updateUserAddressIdInLocalPersistedUserAddress(userAddressId: userAddressId)
        }
        catch let error{
            print(error)
        }
            return false
        
    }
    
    func updateLocalPersistedAddress(userAddressDetails : UserAddressDetails) -> Bool{
        do{
            return try localPersistedUserAddressDAO.updateLocalPersistedAddress(userAddressDetails: userAddressDetails)
            
        }
        catch let error{
            print(error)
        }
            return false
       
    }
}
