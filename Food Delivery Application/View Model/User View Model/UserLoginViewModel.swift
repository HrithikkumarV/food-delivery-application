//
//  UserRegistrationViewmodel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 27/10/21.
//

import Foundation

class UserLoginViewModel{
    
    private let userLoginDAO : UserLoginDAOProtocol = InjectDAOUtils.getUserAccountDAO()
    
    
    func ckeckIfPhoneNumberExists(phoneNumber : String) -> Bool{
        do{
             return try userLoginDAO.checkIfPhoneNumberExists(phoneNumber: phoneNumber)
        }
        catch {
            print(error)
        }
        return true
    }
    
    func getUserId(phoneNumber : String) -> Int {
        do{
            return try userLoginDAO.getUserId(phoneNumber: phoneNumber)
        }
        catch {
            print(error)
        }
        return 0
    }
    
}

