//
//  UserCartViewModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 07/03/22.
//

import Foundation

class UserCartViewModel{
    
    
    private init(){
        getTheCartDetails()
    }
    private let userCartDAO : UserCartDAOProtocol = InjectDAOUtils.getUserCartDAO()
    private let couponDAO : CouponDAOProtocol = InjectDAOUtils.getCouponDAO()
    private static var userCartViewModel : UserCartViewModel? = nil
    static var cartItems : [Int : Int]  = [:]
    static var cartItemsCount : Int = 0
    static var totalPrice : Int = 0
    static var restaurantNameInCart : String = ""
    static func getUserCartViewModel()  -> UserCartViewModel?{
        if userCartViewModel == nil{
            userCartViewModel =  UserCartViewModel()
        }
        return userCartViewModel
    }
    
    private func getTheCartDetails(){
        DispatchQueue.global().sync{ [self] in
            UserCartViewModel.cartItems = getCartMenuItems()
            UserCartViewModel.restaurantNameInCart = getRestaurantNameFromCartMenu()
            self.getTotalPriceAndTotalCountOfMenusInCart()
                DispatchQueue.main.async{
                    
                }
            }
    }

    
    private func getTotalPriceAndTotalCountOfMenusInCart(){
        for  menuId in UserCartViewModel.cartItems.keys{
            let menuPrice = getMenuPrice(menuId : menuId)
            UserCartViewModel.totalPrice += menuPrice * (UserCartViewModel.cartItems[menuId] ?? 0)
            UserCartViewModel.cartItemsCount += UserCartViewModel.cartItems[menuId] ?? 0
        }
    }
    
    private func sendNotificationToTabBarControllerToModifymoveToFullMenuView(){
        NotificationCenter.default.post(name: Notification.Name(moveToViewNotificationNames.updateMenuCountAndPriceInmoveToView.rawValue), object: nil)
        NotificationCenter.default.post(name: Notification.Name(moveToViewNotificationNames.hideOrShowmoveToViewBasedOnMenuCount.rawValue), object: nil)
    }
    
    private func sendNotificationToTabBarControllerToUpdateNameOfRestaurantInCart(){
        NotificationCenter.default.post(name: Notification.Name(moveToViewNotificationNames.updateNameOfRestaurantInCart.rawValue), object: nil)
    }
    
    
    private func getMenuPrice(menuId : Int) -> Int{
        do{
            return try userCartDAO.getMenuPrice(menuId: menuId)
        }
        catch {
            print(error)
        }
        return 0
    }
    
    func getCartMenuItems() -> [Int : Int]{
            do{
                return try userCartDAO.getCartMenuItems()
            }
            catch let error{
                print(error)
            }
        return [:]
    }
    
    
    func addMenuToCart(cartDetails : (menuId : Int , restaurantId : Int , quantity : Int)) -> Bool {
        do{
            return try userCartDAO.insertMenuToCart(cartDetails: cartDetails)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func updateMenuInCartAndSendNotificationTomoveToFullMenuView(menuId : Int , quantity : Int) -> Bool{
        sendNotificationTomoveToFullMenuView()
        return updateMenuInCart(menuId : menuId , quantity : quantity)
    }
    
    func sendNotificationTomoveToFullMenuView(){
        DispatchQueue.main.async{ [self] in
            sendNotificationToTabBarControllerToModifymoveToFullMenuView()
            sendNotificationToTabBarControllerToUpdateNameOfRestaurantInCart()
        }
    }
    
    
    private func updateMenuInCart(menuId : Int , quantity : Int) -> Bool{
        do{
            
            return try userCartDAO.updateMenuInCart(menuId: menuId, quantity: quantity)
        }
        catch {
            print(error)
        }
        
        return false
    }
    
    
    
    func getRestaurantNameFromCartMenu() -> String {
        do{
            UserCartViewModel.restaurantNameInCart = try userCartDAO.getRestaurantNameFromUserCart()
        }
        catch {
            print(error)
        }
        return UserCartViewModel.restaurantNameInCart
    }
    
    func getRestaurantDetailsFromCartMenu() -> RestaurantContentDetails {
        do{
            return try userCartDAO.getRestaurantDetailsFromUserCart()
        }
        catch {
            print(error)
        }
        return RestaurantContentDetails()
    }
    
    func removeMenuFromCart(menuId : Int) -> Bool{
        do{
            return try userCartDAO.removeMenuFromCart(menuId: menuId)
        }
        catch {
            print(error)
        }
        return false
    }
    
    
    func clearCart() -> Bool{
        do{
            return try userCartDAO.clearCart()
        }
        catch {
            print(error)
        }
        return false
    }
    
    
    func getMenuDetailsOfCart() -> ( menuDetailsInCartInDictionaryFormat : [Int : CartMenuDetails], menuDetailsOrderedKey : [Int]){
        var menuDetailsInCart : [CartMenuDetails] = []
        var menuDetailsInCartInDictionaryFormat : [Int : CartMenuDetails] = [:]
        var menuDetailsOrderedKey : [Int] = []
        do{
            menuDetailsInCart =  try userCartDAO.getMenuDetailsInCart()
        }
        catch {
            print(error)
        }
        
        for menus in menuDetailsInCart{
            menuDetailsInCartInDictionaryFormat[menus.menuId] = menus
            menuDetailsOrderedKey.append(menus.menuId)
        }
        return ( menuDetailsInCartInDictionaryFormat : menuDetailsInCartInDictionaryFormat, menuDetailsOrderedKey : menuDetailsOrderedKey)
    }
    
    func getCouponCode(couponCode : String, completion: @escaping (_ couponCode : String? ,_ discountPercentage : Int , _ maximumDiscountAmount : Int) -> Void){
        APICallerManager.shared.getCouponCode(couponCode: couponCode) { couponCode, discountPercentage, maximumDiscountAmount in
            completion(couponCode, discountPercentage, maximumDiscountAmount)
        }
        
    }
    
    func getCouponCode() -> String{
        do{
            return try couponDAO.getCouponCode()
        }
        catch {
            print(error)
        }
        return ""
    }
    
    func deleteCouponCode() -> Bool{
        do{
            return try couponDAO.deleteCouponCode()
        }
        catch {
            print(error)
        }
        return false
    }
    
    func persistCouponCode(couponCode : String) -> Bool{
        do{
            return try couponDAO.persistCouponCode(couponCode: couponCode)
        }
        catch {
            print(error)
        }
        return false
    }
    
}

struct CouponCode : Codable{
    let couponCode : String
    let discountPercentage , maximumDiscountAmount  : Int
    
}
