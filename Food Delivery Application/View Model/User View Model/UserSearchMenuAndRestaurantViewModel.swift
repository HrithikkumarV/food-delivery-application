//
//  UserSearchMenuAndRestaurantViewModekl.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 28/02/22.
//

import Foundation


class UserSearchMenuAndRestaurantViewModel{
    private let searchMenuAndRestaurantDAO : SearchMenuAndRestaurantDAOProtocol = InjectDAOUtils.getSearchMenuAndRestaurantDAO()
    private var searchHistoryAndSuggestionItems : [SearchSuggessionModel] = []
    private let favouriteRestaurantViewModel  = FavouriteRestaurantViewModel()
    
    private var menuContentDetailsWithRestaurantDetails : [(menu : MenuContentDetails,restaurant : RestaurantContentDetails)] = []
    private var restaurantContentDetails : [RestaurantContentDetails] = []
    
    
    private let userAddressesViewModel  = UserAddressesViewModel()
   
    func getDeliveryAddress() -> UserAddressDetails{
        return userAddressesViewModel.getLocalPersistedUserAddress()
    }
    
    func getDisplayTypeBasedOnCountOfSearchResults() -> searchResult {
        if(restaurantContentDetails.count > menuContentDetailsWithRestaurantDetails.count){
            return searchResult.restaurant
        }
        else{
            return searchResult.menu
        }
    }
    
    
    
    func getRestaurantsForSearchedItemFromDB(searchItem : String, locality : String , pincode : String){
        do{
            restaurantContentDetails =  try searchMenuAndRestaurantDAO.getRestaurantsForSearchedItem(searchItem: searchItem, locality: locality, pincode: pincode)
        }
        catch {
            print(error)
        }
        
    }
    
    
    private func sortRestaurantsBasedOnFavouriteRestaurantsList(){
        var index = 0
        var favouriteRestaurantsInList : [RestaurantContentDetails] = []
        while( index < restaurantContentDetails.count){
            if(favouriteRestaurantViewModel.isFavouriteRestaurant(restaurantId:  restaurantContentDetails[index].restaurantId)){
                let restaurantDetails = restaurantContentDetails[index]
                restaurantContentDetails.remove(at: index)
                favouriteRestaurantsInList.append(restaurantDetails)
            }
            else{
                index += 1
            }
        }
        restaurantContentDetails = favouriteRestaurantsInList + restaurantContentDetails
    }
    
    private func sortRestaurantsBasedOnAvailability(){
        var index = 0
        var notAvailableRestaurants  : [RestaurantContentDetails] = []
        while(index < restaurantContentDetails.count){
            if((restaurantContentDetails[index].restaurantIsAvailable) == 0){
                let restaurantDetails = restaurantContentDetails[index]
                restaurantContentDetails.remove(at: index)
                notAvailableRestaurants.append(restaurantDetails)
            }
            else{
                index += 1
            }
        }
        restaurantContentDetails = restaurantContentDetails + notAvailableRestaurants
    }
    
    func getRestaurantsForSearchedItem(searchItem : String, locality : String , pincode  :String) -> [RestaurantContentDetails]{
        getRestaurantsForSearchedItemFromDB(searchItem : searchItem, locality : locality , pincode  :pincode)
        sortRestaurantsBasedOnFavouriteRestaurantsList()
        sortRestaurantsBasedOnAvailability()
        return restaurantContentDetails
    }
    
    func getMenusForSearchedItemFromDB(searchItem : String, locality : String , pincode  :String) {
        do{
            menuContentDetailsWithRestaurantDetails = try searchMenuAndRestaurantDAO.getMenusForSearchedItem(searchItem: searchItem, locality: locality, pincode: pincode)
        }
        catch {
            print(error)
        }
        
    }
    
    private func sortMenusBasedOnFavouriteRestaurantsList(){
        var index = 0
        var menusFromFavouriteRestauransInList : [(menu : MenuContentDetails,restaurant : RestaurantContentDetails)] = []
        while( index < menuContentDetailsWithRestaurantDetails.count){
            if(favouriteRestaurantViewModel.isFavouriteRestaurant(restaurantId: menuContentDetailsWithRestaurantDetails[index].restaurant.restaurantId)){
                let contentDetails = menuContentDetailsWithRestaurantDetails[index]
                menuContentDetailsWithRestaurantDetails.remove(at: index)
                menusFromFavouriteRestauransInList.append(contentDetails)
            }
            else{
                index += 1
            }
        }
        menuContentDetailsWithRestaurantDetails = menusFromFavouriteRestauransInList + menuContentDetailsWithRestaurantDetails
    }
    
    private func sortMenusBasedOnAvailability(){
        var index = 0
        while( index < menuContentDetailsWithRestaurantDetails.count){
            if((menuContentDetailsWithRestaurantDetails[index].menu.menuIsAvailable) == 0){
                let menuContentDetail = menuContentDetailsWithRestaurantDetails[index]
                menuContentDetailsWithRestaurantDetails.remove(at: index)
                menuContentDetailsWithRestaurantDetails.append(menuContentDetail)
            }
            index += 1
        }
    }
    
    func getSearchedMenuContents(searchItem : String, locality : String , pincode  :String) -> (menuContentsInDictionaryFormat : [ Int : MenuContentDetails] , restaurantContentsInDictionaryFormat : [Int : RestaurantContentDetails],orderedKeys : [Int]){
        getMenusForSearchedItemFromDB(searchItem : searchItem, locality : locality , pincode  : pincode)
        sortMenusBasedOnFavouriteRestaurantsList()
        sortMenusBasedOnAvailability()
        var menuContentsInDictionaryFormat : [ Int : MenuContentDetails]  = [:]
        var restaurantContentsInDictionaryFormat : [Int : RestaurantContentDetails] = [:]
        var orderedKey : [Int] = []
        for menuAndRestaurantDetails in menuContentDetailsWithRestaurantDetails{
            menuContentsInDictionaryFormat[menuAndRestaurantDetails.menu.menuId] = menuAndRestaurantDetails.menu
            restaurantContentsInDictionaryFormat[menuAndRestaurantDetails.menu.menuId] = menuAndRestaurantDetails.restaurant
            orderedKey.append(menuAndRestaurantDetails.menu.menuId)
        }
        return (menuContentsInDictionaryFormat : menuContentsInDictionaryFormat, restaurantContentsInDictionaryFormat ,orderedKeys : orderedKey)
    }
    
    
    func persistSearchHistory(searchItemName: String, searchItemType: SearchItemType) -> Bool{
        do{
            return try searchMenuAndRestaurantDAO.persistSearchHistory(searchItemName: searchItemName, searchItemType: searchItemType)
        }
        catch {
            print(error)
        }
        return false
    }
    
    func clearSearchHistory() -> Bool{
        do{
            return try searchMenuAndRestaurantDAO.clearSearchHistory()
        }
        catch {
            print(error)
        }
        return false
    }
    
    func getSearchHistoryItems() -> [(searchItemName: String, searchItemType: SearchItemType)]{
        do{
            return try searchMenuAndRestaurantDAO.getSearchHistoryItems()
        }
        catch {
            print(error)
        }
        return []
    }
    
    
    func getSearchHistoryItemsLikeTheSearchItem(searchItem: String) -> [(searchItemName: String, searchItemType: SearchItemType)]{
        do{
            return try searchMenuAndRestaurantDAO.getSearchHistoryItemsLikeTheSearchItem(searchItem: searchItem)
        }
        catch {
            print(error)
        }
        return []
    }

    
    
    func getMenusForSearchedItemSuggestion(searchItem: String, locality: String, pincode: String) -> Set<String>{
        do{
            return try searchMenuAndRestaurantDAO.getMenusForSearchedItemSuggestion(searchItem: searchItem, locality: locality, pincode: pincode)
        }
        catch {
            print(error)
        }
        return []
    }
    
    func getRestaurantsForSearchedItemSuggestion(searchItem: String, locality: String, pincode: String) -> Set<String>{
        do{
            return try searchMenuAndRestaurantDAO.getRestaurantsForSearchedItemSuggestion(searchItem: searchItem, locality: locality, pincode: pincode)
        }
        catch {
            print(error)
        }
        return []
    }
    
    
    
    func initialSearchHistoryDetails() -> [SearchSuggessionModel]{
        let searchHistoryItems = getSearchHistoryItems()
        searchHistoryAndSuggestionItems = []
        for searchHistoryItem in searchHistoryItems {
            searchHistoryAndSuggestionItems.append(SearchSuggessionModel(searchItemName: searchHistoryItem.searchItemName, searchItemType: searchHistoryItem.searchItemType, searchType: .searchHistoryItem))
           
        }
        return searchHistoryAndSuggestionItems
    }
    
    func getSearchItemResults(searchItem : String,pincode : String , locality : String) -> [SearchSuggessionModel]{
        searchHistoryAndSuggestionItems = []
        let searchHistoryItemsLikeTheSearchItems = getSearchHistoryItemsLikeTheSearchItem(searchItem: searchItem)
        let searchMenuItems = getMenusForSearchedItemSuggestion(searchItem: searchItem, locality: locality, pincode: pincode)
        let searchRestaurantItems = getRestaurantsForSearchedItemSuggestion(searchItem: searchItem, locality: locality, pincode: pincode)
        for searchHistoryItemsLikeTheSearchItem in searchHistoryItemsLikeTheSearchItems {
            searchHistoryAndSuggestionItems.append(SearchSuggessionModel(searchItemName: searchHistoryItemsLikeTheSearchItem.searchItemName, searchItemType: searchHistoryItemsLikeTheSearchItem.searchItemType, searchType: .searchHistoryItem))
        }
        for searchRestaurantItem in searchRestaurantItems {
            searchHistoryAndSuggestionItems.append(SearchSuggessionModel(searchItemName: searchRestaurantItem, searchItemType: .Restaurant, searchType: .searchSuggetionItem))
        }
        
        for searchMenuItem in searchMenuItems {
            searchHistoryAndSuggestionItems.append(SearchSuggessionModel(searchItemName: searchMenuItem, searchItemType: .Menu, searchType: .searchSuggetionItem))
        }
        return searchHistoryAndSuggestionItems
    }
}

enum searchResult{
    case menu
    case restaurant
}
