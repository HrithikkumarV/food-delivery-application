//
//  ToastUtils.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 16/03/22.
//

import Foundation
import UIKit



extension UIViewController{

    func showToast(message : String, font: UIFont ,fontColor : UIColor ,backGroundColor : UIColor) {

    let toastLabel = UILabel(frame: CGRect(x: 40, y: self.view.frame.size.height-150, width: self.view.frame.size.width - 80, height: 50))
    toastLabel.backgroundColor = backGroundColor
    toastLabel.textColor = fontColor
    toastLabel.font = font
    toastLabel.textAlignment = .center;
    toastLabel.text = message
        toastLabel.adjustsFontSizeToFitWidth = true
    toastLabel.alpha = 1.0
    toastLabel.layer.cornerRadius = 10;
    toastLabel.clipsToBounds  =  true
    toastLabel.isUserInteractionEnabled = true
    toastLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapToast(sender:))))
    
        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
            let sceneDelegate = windowScene.delegate as? SceneDelegate
          else {
            return
          }
        sceneDelegate.window?.rootViewController?.presentedViewController?.view.addSubview(toastLabel)
        sceneDelegate.window?.rootViewController?.presentedViewController?.view.bringSubviewToFront(toastLabel)
    UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
         toastLabel.alpha = 0.0
    }, completion: {(isCompleted) in
        toastLabel.removeFromSuperview()
    })
}
    
    @objc private func didTapToast(sender: UIView){
        sender.removeFromSuperview()
        
    }
    
}
