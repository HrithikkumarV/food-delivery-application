//
//  FeedbackElement.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 18/04/22.
//

import UIKit

class FeedbackElement : UIAlertController{
    
    private var feedabackTextViewDelegate: TextViewPlaceHolderUtils!
    
    lazy var feedbackTextView : UITextView = {
        let feedbackTextView = UITextView()
        feedbackTextView.backgroundColor = .clear
        feedbackTextView.autocorrectionType = .no
        feedbackTextView.returnKeyType = .default
        feedbackTextView.font = UIFont(name: "ArialHebrew", size: 16)
        feedbackTextView.translatesAutoresizingMaskIntoConstraints = false
        feedbackTextView.layer.borderWidth = 1
        feedbackTextView.layer.borderColor = UIColor.systemGray.cgColor
        feedbackTextView.layer.cornerRadius = 10
        feedabackTextViewDelegate = TextViewPlaceHolderUtils(targetTextView: feedbackTextView, placeHolderText: "Tell us something..", descriptionLabelText: "", fontSizeForLabel: 15, view: view, placeHolderColor: .systemGray)
        feedbackTextView.delegate = feedabackTextViewDelegate
        return feedbackTextView
    }()
    
    private var preferStyle : UIAlertController.Style?
    
    init(){
        super.init(nibName: nil, bundle: nil)
    }
    
    public convenience init(title: String?, message: String?, preferredStyle: UIAlertController.Style){
        self.init()
        self.title = title
        self.message = message
        self.preferStyle = preferredStyle
    
        self.view.addSubview(feedbackTextView)
        self.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.heightAnchor.constraint(equalToConstant: 210).isActive = true
        view.layoutIfNeeded()
        feedbackTextView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 50).isActive = true
        feedbackTextView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -10).isActive = true
        feedbackTextView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 10).isActive = true
        feedbackTextView.heightAnchor.constraint(equalToConstant: 100).isActive = true
    }

   
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredStyle: UIAlertController.Style {
        return preferStyle ?? .alert
        }
    
      
   
    
}
