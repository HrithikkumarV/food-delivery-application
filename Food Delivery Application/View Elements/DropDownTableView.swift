//
//  DropDownTableView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 27/01/22.
//

import Foundation
import UIKit

class DropDownTableViewUtils : UIViewController, UISearchBarDelegate{
   
    
    
    private var searchBar : UISearchBar!
    private var transparentView : UIView!
    private var tableView : UITableView!
    private var selectedButton : UIButton!
    private var dataSource : [String]!
    private var filteredTableData : [String] = []
    private var ViewTap : UITapGestureRecognizer!
    private var searchBarIsActive: Bool = false
    var complition : (() -> Void)?
    override func viewDidLoad() {
        addTransparentView()
        addDropDownTableView()
    }
    
   
    func setRequiredDataForDropDownTable(selectedButton : UIButton, dataSourse : [String]) {
        self.selectedButton = selectedButton
        self.dataSource = dataSourse
        self.filteredTableData = dataSourse
    }
    
      
           
    func addTransparentView() {
        transparentView = UIView()
            transparentView.frame = self.view.frame
            self.view.addSubview(transparentView)
            transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        ViewTap = UITapGestureRecognizer(target: self, action: #selector(closeDropDownTable))
            transparentView.addGestureRecognizer(ViewTap)
            transparentView.alpha = 0
        
    }
    
    private func addDropDownTableView(){
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "DropDownCell")
        tableView.frame = CGRect(x: view.frame.minX + 40, y: view.frame.minY + 100 , width: view.frame.width - 80, height: 0)
        tableView.layer.cornerRadius = 5
        self.view.addSubview(tableView)
        initiateDropDownTable()
    }
        
    
    private func initiateDropDownTable(){
        var height : CGFloat = 80
        height += CGFloat(dataSource.count * 50)
        if(height > view.frame.size.height - 200){
            height = view.frame.size.height - 200
        }
        UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: { [self] in
                transparentView.alpha = 0.5
            tableView.frame = CGRect(x: view.frame.minX + 40, y: view.frame.minY + 100 , width: view.frame.width - 80, height: height)
            selectedButton.addRightImageToButton(systemName: "chevron.up", imageColor: .lightGray, padding: 20)
                tableView.reloadData()
            }, completion: nil)
        
    }
    
    @objc func closeDropDownTable() {
        UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: { [self] in
                transparentView.alpha = 0
            tableView.frame = CGRect(x: view.frame.minX + 40, y: view.frame.minY + 100 , width: view.frame.width - 80, height: 0)
            selectedButton.addRightImageToButton(systemName: "chevron.down", imageColor: .lightGray, padding: 20)
                dismiss(animated: false, completion: nil)
                complition?()
            }, completion: nil)
        }
    override func viewWillDisappear(_ animated: Bool) {
              super.viewWillDisappear(animated)
              presentingViewController?.viewWillAppear(true)
          }

    
}

extension DropDownTableViewUtils: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if(searchBarIsActive) {
                  return filteredTableData.count
              } else {
                  return dataSource.count
              }
        }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        searchBar = UISearchBar()
        searchBar.delegate = self
        return searchBar
    }
    
   
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        40
    }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownCell", for: indexPath)
            if(searchBarIsActive) {
                cell.textLabel?.text = filteredTableData[indexPath.row]
                return cell
              } else {
                  cell.textLabel?.text = dataSource[indexPath.row]
                  return cell
              }
           
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if(searchBarIsActive){
                selectedButton.setTitle("\(filteredTableData[indexPath.row])", for: .normal)
            }
            else{
                selectedButton.setTitle("\(dataSource[indexPath.row])", for: .normal)
            }
            selectedButton.setTitleColor(.black, for: .normal)
            selectedButton.showTopBorderLabelInView()
            closeDropDownTable()
        }
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterData(searchText :searchText)
        self.tableView.reloadData()
    }
    
    func filterData(searchText :String){
        filteredTableData.removeAll()
        filteredTableData = dataSource.filter({$0.localizedCaseInsensitiveContains(searchText) })
        if(searchText.count == 0){
            filteredTableData = dataSource
        }
        
    }
    
   
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBarIsActive = true
        return true
    }
    
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBarIsActive = false
        return true
    }
   
    
}
