//
//  StarRatingElement.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 18/04/22.
//

import UIKit

class StarRatingElement : UIAlertController{
    private lazy var firstStar : UIButton = {
        return getStarButtonForStarRating()
    }()
    private lazy var secondStar : UIButton =  {
        return getStarButtonForStarRating()
    }()
    private lazy var thirdStar : UIButton =   {
        return getStarButtonForStarRating()
    }()
    private lazy var fourthStar : UIButton =   {
        return getStarButtonForStarRating()
    }()
    private lazy var fifthStar : UIButton =  {
        return getStarButtonForStarRating()
    }()
    
    var starRating : Int = 0
    var starRatingStackView : UIStackView!
    private var preferStyle : UIAlertController.Style?
    
    init(){
        super.init(nibName: nil, bundle: nil)
    }
    
    public convenience init(title: String?, message: String?, preferredStyle: UIAlertController.Style){
        self.init()
        self.title = title
        self.message = message
        self.preferStyle = preferredStyle
        addStarButtonsToStarRating()
    }
    
    
    
    
    
    override var preferredStyle: UIAlertController.Style {
        return preferStyle ?? .alert
       }

 
    
   
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func getStarButtonForStarRating() -> UIButton{
        let starButton = UIButton()
        starButton.setImage(getStarEmptyImage(), for: .normal)
        starButton.contentVerticalAlignment = .fill
        starButton.contentHorizontalAlignment = .fill
        starButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 8, bottom: 10, right: 8)
        starButton.backgroundColor = .clear
        starButton.addTarget(self, action: #selector(didTapStar), for: .touchUpInside)
        return starButton
    }
    
    private func getStarEmptyImage() -> UIImage?{
        UIImage(systemName: "star")?.withTintColor(UIColor(displayP3Red: 0.9882352941, green: 0.7607843137, blue: 0, alpha: 1), renderingMode: .alwaysOriginal)
    }
    
    private func getStarFillImage() -> UIImage?{
        UIImage(systemName: "star.fill")?.withTintColor(UIColor(displayP3Red: 0.9882352941, green: 0.7607843137, blue: 0, alpha: 1), renderingMode: .alwaysOriginal)
    }
    
    @objc private  func didTapStar(sender : UIButton){
        switch sender{
        case firstStar :
            firstStar.setImage(getStarFillImage(), for: .normal)
            secondStar.setImage(getStarEmptyImage(), for: .normal)
            thirdStar.setImage(getStarEmptyImage(), for: .normal)
            fourthStar.setImage(getStarEmptyImage(), for: .normal)
            fifthStar.setImage(getStarEmptyImage(), for: .normal)
            message = "VERY BAD"
            starRating = 1
        case secondStar :
            firstStar.setImage(getStarFillImage(), for: .normal)
            secondStar.setImage(getStarFillImage(), for: .normal)
            thirdStar.setImage(getStarEmptyImage(), for: .normal)
            fourthStar.setImage(getStarEmptyImage(), for: .normal)
            fifthStar.setImage(getStarEmptyImage(), for: .normal)
            message = "BAD"
            starRating = 2
        case thirdStar :
            firstStar.setImage(getStarFillImage(), for: .normal)
            secondStar.setImage(getStarFillImage(), for: .normal)
            thirdStar.setImage(getStarFillImage(), for: .normal)
            fourthStar.setImage(getStarEmptyImage(), for: .normal)
            fifthStar.setImage(getStarEmptyImage(), for: .normal)
            message = "AVERAGE"
            starRating = 3
        case fourthStar :
            firstStar.setImage(getStarFillImage(), for: .normal)
            secondStar.setImage(getStarFillImage(), for: .normal)
            thirdStar.setImage(getStarFillImage(), for: .normal)
            fourthStar.setImage(getStarFillImage(), for: .normal)
            fifthStar.setImage(getStarEmptyImage(), for: .normal)
            message = "GOOD"
            starRating = 4
        case fifthStar :
            firstStar.setImage(getStarFillImage(), for: .normal)
            secondStar.setImage(getStarFillImage(), for: .normal)
            thirdStar.setImage(getStarFillImage(), for: .normal)
            fourthStar.setImage(getStarFillImage(), for: .normal)
            fifthStar.setImage(getStarFillImage(), for: .normal)
            message = "LOVED IT!"
            starRating = 5
        default:
            break
        }
        
    }
    
    
    private func getStarRatingHorizontalStackView(firstStar : UIButton,secondStar : UIButton,thirdStar : UIButton, fourthStar : UIButton,fifthStar : UIButton){
        starRatingStackView = UIStackView(arrangedSubviews: [firstStar,secondStar,thirdStar,fourthStar,fifthStar])
        starRatingStackView.axis = .horizontal
        starRatingStackView.distribution = .fillEqually
        starRatingStackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(starRatingStackView)
       view.translatesAutoresizingMaskIntoConstraints = false
      view.heightAnchor.constraint(equalToConstant: 150).isActive = true
        view.layoutIfNeeded()
     starRatingStackView.topAnchor.constraint(equalTo: view.topAnchor, constant: 55).isActive = true
        starRatingStackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -30).isActive = true
        starRatingStackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 30).isActive = true
        starRatingStackView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    private func addStarButtonsToStarRating(){
        getStarRatingHorizontalStackView(firstStar: firstStar, secondStar: secondStar, thirdStar: thirdStar, fourthStar: fourthStar, fifthStar: fifthStar)
        
    }
    
    
}

