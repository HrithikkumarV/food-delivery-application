//
//  File.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 06/04/22.
//

import Foundation
import UIKit
extension UIViewController{
    func showAlert(message : String){
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel,handler: nil))
        present(alert,animated: true)
    }
}
