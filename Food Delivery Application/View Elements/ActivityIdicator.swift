//
//  ActivityIdicatorUtils.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 06/04/22.
//

import Foundation
import UIKit

extension UIViewController{
    
    
    func showActivityIndicator() {
        let activityView = UIActivityIndicatorView(style: .large)
        activityView.center = self.view.center
        self.view.addSubview(activityView)
        self.view.isUserInteractionEnabled = false
        activityView.startAnimating()
    }

    func hideActivityIndicator(){
        for subview in view.subviews{
            if subview is UIActivityIndicatorView {
                subview.removeFromSuperview()
            }
        }
        self.view.isUserInteractionEnabled = true
        
    }
    
}
