//
//  ViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 27/10/21.
//

import UIKit

class MainViewController: UIViewController {
    private var mainView : MainViewProtocol!
    private var restaurantId : Int = 0
    private var userAddress : UserAddressDetails = UserAddressDetails()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialiseView()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getInitialDataFromDatabase()
    }
    

    
    private func initialiseView(){
         view.backgroundColor = .white
         mainView = MainView()
         mainView.translatesAutoresizingMaskIntoConstraints = false
         mainView.delegate = self
         view.addSubview(mainView)
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
    
    
    private func getInitialDataFromDatabase(){
        showActivityIndicator()
        DispatchQueue.global().async { [self] in
                userAddress = UserAddressesViewModel().getLocalPersistedUserAddress()
                restaurantId = (RestaurantIDUtils.shared.getRestaurantId())
            DispatchQueue.main.async {
                self.hideActivityIndicator()
            }
        }
        
    }
    
}

extension MainViewController : MainViewDelegate {
    
     func didTapUserButton(){
        if(userAddress.addressTag != ""){
            let userTabBar = UINavigationController(rootViewController: UserTabBarController())
            userTabBar.modalPresentationStyle = .fullScreen
            present(userTabBar,animated: true)
        }
        else{
            let welcomPage = UINavigationController(rootViewController: UserWelcomePageViewController())
            welcomPage.modalPresentationStyle = .fullScreen
            present(welcomPage,animated: true)
        }

    
    }
    
     func didTapRestaurantButton(){
        if( restaurantId != 0){
            let restaurantTabBar = UINavigationController(rootViewController: RestaurantTabBarController())
            restaurantTabBar.modalPresentationStyle = .fullScreen
            present(restaurantTabBar,animated: true)
        }
        else{
            let restaurantCreateAccountVC = UINavigationController(rootViewController: RestaurantLoginViewController())
            restaurantCreateAccountVC.modalPresentationStyle = .fullScreen
            present(restaurantCreateAccountVC,animated: true)
        }
    }
}


// setup layouts

extension MainViewController{
    
   
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let contentViewConstraints = [mainView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor),
                                      mainView.bottomAnchor.constraint(equalTo : view.safeAreaLayoutGuide.bottomAnchor),
                                      mainView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                                      mainView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)]
            return contentViewConstraints
    }
}
    


    
    
    
    
    
    
    


