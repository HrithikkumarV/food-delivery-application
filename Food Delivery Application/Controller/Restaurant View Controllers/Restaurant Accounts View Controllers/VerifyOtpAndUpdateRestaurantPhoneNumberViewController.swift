//
//  VerifyOtpAndUpdateRestaurantPhoneNumberViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 31/03/22.
//


import UIKit

class VerifyOTPAndUpdateRestaurantPhoneNumberViewController :  RestaurantCreateAccountOTPFormViewController{
    
    private var restaurantAccountsViewModel : RestaurantAccountsViewModel =  RestaurantAccountsViewModel()
    
    
    
    override func didTapContinueToVerify(){
        if(restaurantCreateAccountOTPFormView.OTPTextField.text! == OTP){
            DispatchQueue.global().async { [self] in
                if(restaurantAccountsViewModel.updateRestaurantPhoneNumber(restaurantID: RestaurantIDUtils.shared.getRestaurantId(), restaurantPhoneNumber: restaurantPhoneNumber)){
                    DispatchQueue.main.async { [self] in
                        showToast(message: " Updated Phone Number Successfully", font: UIFont.systemFont(ofSize: 25, weight: .medium), fontColor: .white, backGroundColor: .systemBlue.withAlphaComponent(0.9))
                        navigationController?.popViewController(animated: true)
                    }
                }
            }
            
        }
        else{
            showAlert(message: "Enter valid OTP")
        }
    }
}
