//
//  RestaurantMoreOptionPageViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 01/04/22.
//

import Foundation
import UIKit

class RestaurantMoreOptionPageViewController : UIViewController, UITextFieldDelegate,RestaurantMoreOptionsPageViewDelegate {
    private var restaurantAccountsViewModel : RestaurantAccountsViewModel = RestaurantAccountsViewModel()
    private var restaurantContentDetails : RestaurantContentDetails!
    private var restaurantMoreOptionsPageView :RestaurantMoreOptionsPageViewProtocol!
    private var keyboardUtils : KeyboardUtils!
    private var activeArea : UIView? = nil

    let scrollView  :UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
        scrollView.backgroundColor = .clear
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
   
    lazy var backArrowButton : UIBarButtonItem =  {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.target = self
        backArrowButton.action = #selector(didTapBackArrow)
        return backArrowButton
    }()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addTargetsAndActionsForElements()
        addDelegatesForElements()
        addObserver()
        setupRestaurantDetailsInViewElements()
    }
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.global().async { [self] in
            restaurantContentDetails = restaurantAccountsViewModel.getRestaurantDetails(restaurantId: (RestaurantIDUtils.shared.getRestaurantId()))
        }
    }
    
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapCancelUpdateRestaurantPackaingChargesButton(){
        restaurantMoreOptionsPageView.restaurantPackagingChargesTextField.resignFirstResponder()
        restaurantMoreOptionsPageView.restaurantPackagingChargesTextField.text = String(restaurantContentDetails.restaurantFoodPackagingCharges)
        restaurantMoreOptionsPageView.updateAndCancelRestaurantPackagingChargesHorizontalStackView.isHidden = true
    }
    
    @objc func didTapUpdateRestaurantPackaingChargesButton(){
        let restaurantPackagingChargesTextFieldText = restaurantMoreOptionsPageView.restaurantPackagingChargesTextField.text ?? "0"
        DispatchQueue.global().async { [self] in
            if(restaurantAccountsViewModel.updateRestaurantFoodPackagingCharges(restaurantID: restaurantContentDetails.restaurantId, restaurantFoodPackagingCharge: Int( restaurantPackagingChargesTextFieldText) ?? 0)){
                DispatchQueue.main.async { [self] in
                    restaurantContentDetails.restaurantFoodPackagingCharges =  Int(restaurantPackagingChargesTextFieldText) ?? 0
                    showToast(message: "Updated Restaurant Packaing Charges Successfully", font: UIFont.systemFont(ofSize: 25, weight: .medium), fontColor: .white, backGroundColor: .systemBlue.withAlphaComponent(0.9))
                    didTapCancelUpdateRestaurantPackaingChargesButton()
                }
            }
        }
        
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if( textField == restaurantMoreOptionsPageView.restaurantPackagingChargesTextField){

            var res = ""
            if restaurantMoreOptionsPageView.restaurantPackagingChargesTextField.text != "" || string != "" {
                    res = (textField.text ?? "") + string
                }
            return  Double(res) != nil
        }
        
        return true
    }
   
    
    @objc func textFieldDidChange(sender : UITextField){
        if(sender == restaurantMoreOptionsPageView.restaurantPackagingChargesTextField){
            restaurantPackagingChargesTextFieldTextfieldChangesActions()
        }
        
    }
    
    
    
    @objc func didTapView(){
        self.view.endEditing(true)
    }
    
    private func restaurantPackagingChargesTextFieldTextfieldChangesActions(){
        restaurantMoreOptionsPageView.updateAndCancelRestaurantPackagingChargesHorizontalStackView.isHidden = false
        if(restaurantMoreOptionsPageView.restaurantPackagingChargesTextField.text == String(restaurantContentDetails.restaurantFoodPackagingCharges) || restaurantMoreOptionsPageView.restaurantPackagingChargesTextField.text!.count == 0){
            restaurantMoreOptionsPageView.updateRestaurantPackagingChargesButton.backgroundColor = .systemBlue.withAlphaComponent(0.6)
            restaurantMoreOptionsPageView.updateRestaurantPackagingChargesButton.isUserInteractionEnabled = false
                
        }
        else{
            restaurantMoreOptionsPageView.updateRestaurantPackagingChargesButton.backgroundColor = .systemBlue
            restaurantMoreOptionsPageView.updateRestaurantPackagingChargesButton.isUserInteractionEnabled = true
            
        }
    }
    
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        keyboardUtils.keyboardWillShow(notification: notification, maxYofLastObjectInView: restaurantMoreOptionsPageView.stackView.frame.maxY, activeArea: activeArea)
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardUtils.keyboardWillHide()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeArea = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeArea = nil
    }
    
    
    
    
    
    private func setupRestaurantDetailsInViewElements(){
        restaurantContentDetails = restaurantAccountsViewModel.getRestaurantDetails(restaurantId: (RestaurantIDUtils.shared.getRestaurantId()))
        restaurantMoreOptionsPageView.restaurantPackagingChargesTextField.text = String(restaurantContentDetails.restaurantFoodPackagingCharges)
        restaurantMoreOptionsPageView.updateAndCancelRestaurantPackagingChargesHorizontalStackView.isHidden = true
        
    }
    
    
    private func initialiseView(){
        title = "MORE OPTIONS"
        view.backgroundColor = .white
        
        view.addSubview(scrollView)
        restaurantMoreOptionsPageView = RestaurantMoreOptionsPageView()
        restaurantMoreOptionsPageView.translatesAutoresizingMaskIntoConstraints = false
        restaurantMoreOptionsPageView.delegate = self
        scrollView.addSubview(restaurantMoreOptionsPageView)
        
       
        NSLayoutConstraint.activate(getScrollViewConstraints())
        NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
    private func initialiseViewElements(){
        

        navigationItem.leftBarButtonItem = backArrowButton
        
    }
    
    private func addTargetsAndActionsForElements(){
        
        restaurantMoreOptionsPageView.restaurantPackagingChargesTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .allEditingEvents)
       
        
       
    }
    
    private func addDelegatesForElements(){
        restaurantMoreOptionsPageView.restaurantPackagingChargesTextField.delegate = self
    }
    
    private func addObserver(){
       keyboardUtils = KeyboardUtils(scrollView: scrollView)
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
     
   }
    
   
}

extension RestaurantMoreOptionPageViewController{
    
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                         scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                         scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                         scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [restaurantMoreOptionsPageView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               restaurantMoreOptionsPageView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               restaurantMoreOptionsPageView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               restaurantMoreOptionsPageView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               restaurantMoreOptionsPageView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
        if(restaurantMoreOptionsPageView.stackView.frame.maxY > 0){
            restaurantMoreOptionsPageView.removeHeightAnchorConstraints()
            restaurantMoreOptionsPageView.heightAnchor.constraint(equalToConstant:  restaurantMoreOptionsPageView.stackView.frame.maxY + 100).isActive = true
        }
        else{
            restaurantMoreOptionsPageView.removeHeightAnchorConstraints()
            restaurantMoreOptionsPageView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
    }
    
}


