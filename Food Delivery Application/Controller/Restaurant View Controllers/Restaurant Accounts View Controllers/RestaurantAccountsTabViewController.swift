//
//  RestaurantAccountsTabViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 11/11/21.
//

import Foundation
import UIKit

class RestaurantAccountsTabViewController : UIViewController,RestaurantAccountsPageViewDelegate{
     private var restaurantAccountsPageView : RestaurantAccountsPageViewProtocol!
     private var restaurantOrdersViewModel : RestaurantOrdersViewModel = RestaurantOrdersViewModel()
     private var restaurantAccountsViewModel : RestaurantAccountsViewModel = RestaurantAccountsViewModel()
     
     private var restaurantContentDetails : RestaurantContentDetails!
     private var pastOrderIdList : [String] = []
     private var ordersTableViewHeightConstraint : NSLayoutConstraint!
     private var pastOrderDetailsInDictionaryFormat : [String : OrderDetails] = [:]
     private var pastOrderFoodDetails : [String : [OrderMenuDetails]] = [:]
     private var pastOrderOffset : Int = 0
    let scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
        scrollView.backgroundColor = .white
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    override func viewDidLoad(){
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addDelegatesToViewElements()
        
    }
    
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.global().async { [self] in
            getRestaurantDetails()
            getPastOrderDetails()
            DispatchQueue.main.async { [self] in
                updateRestaurantDetailsInUI()
                restaurantAccountsPageView.orderDetailsTableView.reloadData()
                updateOrderTableViewHeight()
                displayViewMoreButtonBasedOnTheCountOfPastOrders()
            }
        }
    }
    
    func updateOrderTableViewHeight(){
        ordersTableViewHeightConstraint?.isActive = false
        ordersTableViewHeightConstraint = restaurantAccountsPageView.orderDetailsTableView.bottomAnchor.constraint(equalTo: restaurantAccountsPageView.orderDetailsTableView.topAnchor, constant: CGFloat(pastOrderIdList.count > 0 ? 60 : 0)  + (CGFloat(pastOrderIdList.count) * restaurantAccountsPageView.getOrderTabelViewCellHeight()))
        ordersTableViewHeightConstraint?.isActive = true
    }
    
    func displayViewMoreButtonBasedOnTheCountOfPastOrders(){
        if(pastOrderIdList.count >= 5){
            restaurantAccountsPageView.viewMoreOrdersButton.isHidden = false
        }
        else{
            restaurantAccountsPageView.viewMoreOrdersButton.isHidden = true
        }
    }
    
     func getPastOrderDetails(){
        pastOrderOffset = 0
         let pastOrderDetails = restaurantOrdersViewModel.getpastOrderDetails(restaurantId: (RestaurantIDUtils.shared.getRestaurantId()), offSet: pastOrderOffset, limit: 5)
        pastOrderIdList = pastOrderDetails.pastOrderIdList
        pastOrderDetailsInDictionaryFormat = pastOrderDetails.pastOrderDetailsInDictionaryFormat
        pastOrderFoodDetails = restaurantOrdersViewModel.getMenuDetailsInpastOrderFoodDetails(orderIdList: pastOrderDetails.pastOrderIdList)
         
    }
    
     func getPastOrderDetailsAndMergeWithCurrentPastOrderDetails(){
        let pastOrderDetails = restaurantOrdersViewModel.getpastOrderDetails(restaurantId: (RestaurantIDUtils.shared.getRestaurantId()),  offSet: pastOrderOffset, limit: 5)
        pastOrderIdList += pastOrderDetails.pastOrderIdList
        pastOrderDetailsInDictionaryFormat.merge(pastOrderDetails.pastOrderDetailsInDictionaryFormat) {  (current, _) in current
        }
        pastOrderFoodDetails.merge(restaurantOrdersViewModel.getMenuDetailsInpastOrderFoodDetails( orderIdList: pastOrderDetails.pastOrderIdList)){ (current, _) in current
        }

    }
    
    func getRestaurantDetails(){
        restaurantContentDetails = restaurantAccountsViewModel.getRestaurantDetails(restaurantId: (RestaurantIDUtils.shared.getRestaurantId()))
       
        
    }
    
    func updateRestaurantDetailsInUI(){
        restaurantAccountsPageView.updateRestaurantDetails(restaurantContentDetails: restaurantContentDetails)
        updateRestaurantAvailableStatus()
    }
    
    func updateRestaurantAvailableStatus(){
        
        if(restaurantContentDetails.restaurantIsAvailable == 0){
            restaurantAccountsPageView.restaurantIsAvailableToggleSwitch.setOn(true, animated: false)

        }
        else{
            restaurantAccountsPageView.restaurantIsAvailableToggleSwitch.setOn(false, animated: false)
            
        }
        restaurantAccountsPageView.hideSetNextAvailablityItems()
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"
        restaurantContentDetails.restaurantOpensNextAt =  (restaurantContentDetails.restaurantOpensNextAt == ""  ? dateFormatter.string(from: Date()) : "")
        restaurantAccountsPageView.datePicker.setDate(dateFormatter.date(from: restaurantContentDetails.restaurantOpensNextAt) ?? Date(), animated: false)
    }
    
    func didTapEditButton(){
        tabBarController?.navigationController?.pushViewController(EditRestaurantDetailsPageViewController(), animated: true)
    }
    
    func didTapChangePassword(){
        tabBarController?.navigationController?.pushViewController(RestaurantChangePasswordPhoneNumberFormViewController(), animated: true)
    }
    
    func didTapMoreButton(){
        tabBarController?.navigationController?.pushViewController(RestaurantMoreOptionPageViewController(), animated: true)
    }
    
    func didTapSettingsButton(){
        tabBarController?.navigationController?.pushViewController(RestaurantAccountSettingsPageViewController(), animated: true)
    }

    
    func didTapViewMorePastOrders(){
        DispatchQueue.global().async { [self] in
            pastOrderOffset += 5
            getPastOrderDetailsAndMergeWithCurrentPastOrderDetails()
            DispatchQueue.main.async { [self] in
                restaurantAccountsPageView.orderDetailsTableView.reloadData()
                updateOrderTableViewHeight()
                if(pastOrderOffset != pastOrderIdList.count){
                    restaurantAccountsPageView.viewMoreOrdersButton.setTitle("No More Orders", for: .normal)
                    restaurantAccountsPageView.viewMoreOrdersButton.setTitleColor(.systemGray, for: .normal)
                }
            }
        }
        
    }
    
    func didTapRestaurantIsAvailableSwitch(sender: UISwitch){
        if(sender.isOn){
            restaurantAccountsPageView.showSetNextAvailablityItems()
            restaurantContentDetails.restaurantIsAvailable =  0
    
        }
        else{
            restaurantAccountsPageView.hideSetNextAvailablityItems()
            restaurantContentDetails.restaurantIsAvailable =  1
            DispatchQueue.global().async { [self] in
                if(restaurantAccountsViewModel.updateRestaurantAvailablityAndNextOpensAt(restaurantID: restaurantContentDetails.restaurantId, restaurantIsAvailable: restaurantContentDetails.restaurantIsAvailable, restaurantOpensNextAt: restaurantContentDetails.restaurantOpensNextAt)){
                    DispatchQueue.main.async { [self] in
                        restaurantAccountsPageView.updateRestaurantStatus(restaurantContentDetails: restaurantContentDetails)
                    }
                }
            }
        }
    }
    
     func didChangeDateAndtime(sender: UIDatePicker){
    
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"
        let selectedDate: String = dateFormatter.string(from: sender.date)
        restaurantContentDetails.restaurantOpensNextAt =  selectedDate
        
    }
    
    func compareDateWithNow(date : String) -> Bool{
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"
        if(Date() < dateFormatter.date(from: date) ?? Date()){
            return true
        }
        return false
    }
    
     func didTapUpdateRestaurantAvailablity(){
        if(compareDateWithNow(date: restaurantContentDetails.restaurantOpensNextAt)){
            DispatchQueue.global().async { [self] in
                if(restaurantAccountsViewModel.updateRestaurantAvailablityAndNextOpensAt(restaurantID: restaurantContentDetails.restaurantId, restaurantIsAvailable: restaurantContentDetails.restaurantIsAvailable, restaurantOpensNextAt: restaurantContentDetails.restaurantOpensNextAt)){
                    
                    DispatchQueue.main.async { [self] in
                        restaurantAccountsPageView.updateRestaurantStatus(restaurantContentDetails: restaurantContentDetails)
                        restaurantAccountsPageView.hideSetNextAvailablityItems()
                    }
                }
            }
        }
        else{
            showAlert(message: "Update Next Available Date and Time")
        }
    }
    
     func didTapCancelUpdateRestaurantAvailablity(){
        DispatchQueue.global().async { [self] in
            getRestaurantDetails()
            DispatchQueue.main.async { [self] in
                updateRestaurantDetailsInUI()
            }
        }
        
    }
    func didTapLogout(){
        let alert = UIAlertController(title: "LOGOUT", message: "Are you sure you want to Logout ? ", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel,handler: nil))
        alert.addAction(UIAlertAction(title: "Yes,Logout", style: .default,handler: { [self]_ in
            if(RestaurantIDUtils.shared.deleteRestaurantId()){
                dismissToRootViewControllerAndPresentRestaurantLoginPage()
            }
            
        }))
        present(alert,animated: true)
        
    }
    
   
    
     private func initialiseView(){
        view.backgroundColor = .white
        title = "Accounts"
        view.addSubview(scrollView)
         restaurantAccountsPageView = RestaurantAccountsPageView()
         restaurantAccountsPageView.translatesAutoresizingMaskIntoConstraints = false
         restaurantAccountsPageView.delegate = self
         scrollView.addSubview(restaurantAccountsPageView)
         
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
     private func initialiseViewElements(){
         addDismissButton()
         setDefaultNavigationBarApprearance()
         setScrollEdgeApprearance()
    }
    
    func addDelegatesToViewElements(){
        restaurantAccountsPageView.orderDetailsTableView.delegate = self
        restaurantAccountsPageView.orderDetailsTableView.dataSource =  self
    }
    
    
    
    
    func addDismissButton(){
        let dismiss = UIButton(frame: CGRect(x: 0,y : 0, width: 100, height: 20))
        dismiss.setTitle("Switch APP", for: .normal)
        dismiss.setTitleColor(.systemBlue, for: .normal)
        dismiss.backgroundColor = .white
        dismiss.addTarget(self, action: #selector(didTapDismiss), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: dismiss)
    }
    
    @objc func didTapDismiss(){
        dismiss(animated: true, completion: nil)
    }
    
     func dismissToRootViewControllerAndPresentRestaurantLoginPage(){
        hideSubviewsOfRootViewController()
        self.view.window?.rootViewController?.dismiss(animated: true, completion: {
            let restaurantLoginPage = UINavigationController(rootViewController: RestaurantLoginViewController())
            restaurantLoginPage.modalPresentationStyle = .fullScreen
            UIApplication.shared.windows.first?.rootViewController?.present(restaurantLoginPage,animated: true)
        })
        showSubviewsOfRootViewControllerWithADelay()
    }
    
     func showSubviewsOfRootViewControllerWithADelay(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIApplication.shared.windows.first?.rootViewController?.view.subviews.forEach { $0.isHidden = false }
        }
    }
    
     func hideSubviewsOfRootViewController(){
        UIApplication.shared.windows.first?.rootViewController?.view.subviews.forEach { $0.isHidden = true }
    }
    
}

extension RestaurantAccountsTabViewController : UITableViewDelegate, UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            return UIView()
        }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let orderStatusLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width , height: 50))
        orderStatusLabel.backgroundColor = .white
        orderStatusLabel.font = UIFont.systemFont(ofSize: 25, weight: .bold)
        orderStatusLabel.layer.cornerRadius  = 5
        orderStatusLabel.backgroundColor = .white
        orderStatusLabel.text = "Past Orders"
        orderStatusLabel.textColor = .black
        return orderStatusLabel
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return pastOrderIdList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let orderId = pastOrderDetailsInDictionaryFormat[pastOrderIdList[indexPath.row]]!.orderModel.orderId
       
        return restaurantAccountsPageView.createOrdersTableViewCell(indexPath: indexPath, tableView: tableView, orderCellContents: (orderId: orderId, orderStatus: pastOrderDetailsInDictionaryFormat[orderId]!.orderStatus, orderTotal: pastOrderDetailsInDictionaryFormat[orderId]!.orderTotal, menuNameAndQuantities: restaurantOrdersViewModel.getMenuNamesAndQuantityListInPastOrders(pastOrderFoodDetails: pastOrderFoodDetails[orderId]!), dateAndTime: pastOrderDetailsInDictionaryFormat[orderId]!.orderModel.dateAndTime ))
            
       
    }
    
    
   
                
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        restaurantAccountsPageView.getOrderTabelViewCellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let orderId = pastOrderDetailsInDictionaryFormat[pastOrderIdList[indexPath.row]]!.orderModel.orderId
        let orderDetailsPageViewController = PastOrderDetailsViewController()
       
            orderDetailsPageViewController.setOrderDetailsAndOrderMenuDetails(orderDetails: pastOrderDetailsInDictionaryFormat[orderId]!, orderMenuDetailsList: pastOrderFoodDetails[orderId]!)
        navigationController?.pushViewController(orderDetailsPageViewController, animated: true)
    }
    
}

extension RestaurantAccountsTabViewController{
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if(restaurantAccountsPageView.viewMoreOrdersButton.frame.maxY > (view.frame.maxY + 100)){
            restaurantAccountsPageView.removeHeightAnchorConstraints()
            restaurantAccountsPageView.heightAnchor.constraint(equalToConstant:  restaurantAccountsPageView.viewMoreOrdersButton.frame.maxY + 100).isActive = true
           
        }
        
        else if(restaurantAccountsPageView.logOutButton.frame.maxX > 0){
            restaurantAccountsPageView.removeHeightAnchorConstraints()
            restaurantAccountsPageView.heightAnchor.constraint(equalToConstant: restaurantAccountsPageView.logOutButton.frame.maxY + 300).isActive = true
        }
        else{
            restaurantAccountsPageView.removeHeightAnchorConstraints()
            restaurantAccountsPageView.heightAnchor.constraint(equalToConstant: view.frame.maxY + 100).isActive = true
        }
    }
    
    private func getScrollViewConstraints() -> [NSLayoutConstraint]{
        let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                     scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                     scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                     scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
        return scrollViewConstraints
    }

    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [restaurantAccountsPageView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               restaurantAccountsPageView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               restaurantAccountsPageView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               restaurantAccountsPageView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               restaurantAccountsPageView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }

}


