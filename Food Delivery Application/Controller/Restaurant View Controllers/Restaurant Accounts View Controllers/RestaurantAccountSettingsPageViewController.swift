//
//  RestaurantAccountSettingsPageViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 01/04/22.
//

import Foundation
import UIKit

class RestaurantAccountSettingsPageViewController : UIViewController , UITextFieldDelegate{
    private var restaurantAccountsViewModel : RestaurantAccountsViewModel = RestaurantAccountsViewModel()
    private var restaurantContentDetails : RestaurantContentDetails!
    private var restaurantAccountSettingPageView : RestaurantAccountSettingPageViewProtocol!
    private var keyboardUtils : KeyboardUtils!
    private var activeArea : UIView? = nil
   
    let scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
        scrollView.backgroundColor = .clear
        scrollView.translatesAutoresizingMaskIntoConstraints = false
       return scrollView
    }()
    
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.target = self
        backArrowButton.action = #selector(didTapBackArrow)
        return backArrowButton
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addObserver()
        setupRestaurantDetailsInViewElements()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.global().async { [self] in
            restaurantContentDetails = restaurantAccountsViewModel.getRestaurantDetails(restaurantId: (RestaurantIDUtils.shared.getRestaurantId()))
        }
    }
    
    
    
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapRestaurantDelistSwitch(sender: UISwitch){
        if sender.isOn{
            DispatchQueue.global().async { [self] in
                if(restaurantAccountsViewModel.updateRestaurantStatus(restaurantID: restaurantContentDetails.restaurantId, restaurantStatus: 0)){
                    if(restaurantAccountsViewModel.updateRestaurantAvailablityAndNextOpensAt(restaurantID: restaurantContentDetails.restaurantId, restaurantIsAvailable: 0, restaurantOpensNextAt: "")){
                        DispatchQueue.main.async {
                            self.restaurantContentDetails.restaurantAccountStatus =  0
                        }
                    }
                    
                }
            }
            
           
        }
        else{
            DispatchQueue.global().async { [self] in
                if(restaurantAccountsViewModel.updateRestaurantStatus(restaurantID: restaurantContentDetails.restaurantId, restaurantStatus: 1)){
                    DispatchQueue.main.async {
                        self.restaurantContentDetails.restaurantAccountStatus =  1
                    }
                }
            }
        }
    }
    
    @objc func didTapView(){
        self.view.endEditing(true)
    }
    
   
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        keyboardUtils.keyboardWillShow(notification: notification, maxYofLastObjectInView: restaurantAccountSettingPageView.stackView.frame.maxY, activeArea: activeArea)
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardUtils.keyboardWillHide()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeArea = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeArea = nil
    }
    
    
    
    
    
    private func setupRestaurantDetailsInViewElements(){
        DispatchQueue.global().async { [self] in
            restaurantContentDetails = restaurantAccountsViewModel.getRestaurantDetails(restaurantId: (RestaurantIDUtils.shared.getRestaurantId()))
            DispatchQueue.main.async { [self] in
                restaurantAccountSettingPageView.restaurantDelistToggleSwitch.setOn(restaurantContentDetails.restaurantAccountStatus == 1 ? false : true, animated: false)
            }
        }
        
    }
    
    
    private func initialiseView(){
        title = "ACCOUNT SETTING"
        view.backgroundColor = .white
        view.addSubview(scrollView)
        restaurantAccountSettingPageView = RestaurantAccountSettingPageView()
        restaurantAccountSettingPageView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(restaurantAccountSettingPageView)
        
        NSLayoutConstraint.activate(getScrollViewConstraints())
        NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
    private func initialiseViewElements(){
        
       
        
        navigationItem.leftBarButtonItem = backArrowButton
       
    }
    
  
    
    private func addObserver(){
       keyboardUtils = KeyboardUtils(scrollView: scrollView)
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
     
   }
    
}

extension RestaurantAccountSettingsPageViewController{
        
        
            private func getScrollViewConstraints() -> [NSLayoutConstraint]{
                let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                             scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                             scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                             scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
                return scrollViewConstraints
            }
        
        private func getContentViewConstraints() -> [NSLayoutConstraint]{
            let viewConstraints = [restaurantAccountSettingPageView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                                   restaurantAccountSettingPageView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                                   restaurantAccountSettingPageView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                                   restaurantAccountSettingPageView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                                   restaurantAccountSettingPageView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                         ]
            return viewConstraints
        }
        
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
        if(restaurantAccountSettingPageView.stackView.frame.maxY > 0){
            restaurantAccountSettingPageView.removeHeightAnchorConstraints()
            restaurantAccountSettingPageView.heightAnchor.constraint(equalToConstant:  restaurantAccountSettingPageView.stackView.frame.maxY + 100).isActive = true
        }
        else{
            restaurantAccountSettingPageView.removeHeightAnchorConstraints()
            restaurantAccountSettingPageView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
    }
   



}
