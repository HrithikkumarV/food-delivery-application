//
//  EditRestaurantAddressViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 31/03/22.
//


import UIKit
class EditRestaurantAddressViewController : RestaurantCreateAccountAddressViewController{
    private var restaurantAddress: AddressDetails!
    private var restaurantAccountsViewModel : RestaurantAccountsViewModel =  RestaurantAccountsViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        setAddressDetailsInViewElements()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        restaurantCreateAccountAddressView.pincodeTextField.resignFirstResponder()
    }
    
    
    func setAddressDetailsInViewElements(){
        restaurantCreateAccountAddressView.doorNoAndBuildingNameAndBuildingNoTextField.text = restaurantAddress.doorNoAndBuildingNameAndBuildingNo
        restaurantCreateAccountAddressView.doorNoAndBuildingNameAndBuildingNoTextField.showTopBorderLabel()
        restaurantCreateAccountAddressView.streetNameTextfield.text = restaurantAddress.streetName
        restaurantCreateAccountAddressView.streetNameTextfield.showTopBorderLabel()
        restaurantCreateAccountAddressView.landmarkTextfield.text = restaurantAddress.landmark
        restaurantCreateAccountAddressView.landmarkTextfield.showTopBorderLabel()
        restaurantCreateAccountAddressView.localityButton.setTitleColor(.black, for: .normal)
        restaurantCreateAccountAddressView.localityButton.setTitle(restaurantAddress.localityName, for: .normal)
        restaurantCreateAccountAddressView.localityButton.showTopBorderLabelInView()
        restaurantCreateAccountAddressView.cityTextfield.text = restaurantAddress.city
        restaurantCreateAccountAddressView.cityTextfield.showTopBorderLabel()
        restaurantCreateAccountAddressView.stateTextField.text = restaurantAddress.state
        restaurantCreateAccountAddressView.stateTextField.showTopBorderLabel()
        restaurantCreateAccountAddressView.countryTextfield.text = restaurantAddress.country
        restaurantCreateAccountAddressView.countryTextfield.showTopBorderLabel()
        restaurantCreateAccountAddressView.pincodeTextField.text = restaurantAddress.pincode
        restaurantCreateAccountAddressView.pincodeTextField.showTopBorderLabel()
    }
    
    func setRestaurantAddressDetails(restaurantAddress : AddressDetails){
        self.restaurantAddress = restaurantAddress
    }
    
    override func didTapContinue(){
        
        if(!restaurantCreateAccountAddressView.doorNoAndBuildingNameAndBuildingNoTextField.text!.isEmpty && !restaurantCreateAccountAddressView.streetNameTextfield.text!.isEmpty && !restaurantCreateAccountAddressView.landmarkTextfield.text!.isEmpty && restaurantCreateAccountAddressView.localityButton.titleLabel?.text != "Locality" && !restaurantCreateAccountAddressView.cityTextfield.text!.isEmpty && !restaurantCreateAccountAddressView.stateTextField.text!.isEmpty && !restaurantCreateAccountAddressView.countryTextfield.text!.isEmpty){
            if(!(restaurantCreateAccountAddressView.doorNoAndBuildingNameAndBuildingNoTextField.text == restaurantAddress.doorNoAndBuildingNameAndBuildingNo && restaurantCreateAccountAddressView.streetNameTextfield.text == restaurantAddress.streetName && restaurantCreateAccountAddressView.landmarkTextfield.text == restaurantAddress.landmark &&  restaurantCreateAccountAddressView.localityButton.titleLabel?.text == restaurantAddress.localityName && restaurantCreateAccountAddressView.cityTextfield.text == restaurantAddress.city && restaurantCreateAccountAddressView.stateTextField.text == restaurantAddress.state && restaurantCreateAccountAddressView.countryTextfield.text == restaurantAddress.country && restaurantCreateAccountAddressView.pincodeTextField.text == restaurantAddress.pincode)){
                let address = AddressDetails(doorNoAndBuildingNameAndBuildingNo: restaurantCreateAccountAddressView.doorNoAndBuildingNameAndBuildingNoTextField.text!, streetName: restaurantCreateAccountAddressView.streetNameTextfield.text!, localityName: restaurantCreateAccountAddressView.localityButton.titleLabel!.text!, landmark: restaurantCreateAccountAddressView.landmarkTextfield.text!, city: restaurantCreateAccountAddressView.cityTextfield.text!, state: restaurantCreateAccountAddressView.stateTextField.text!, pincode: restaurantCreateAccountAddressView.pincodeTextField.text!, country: restaurantCreateAccountAddressView.countryTextfield.text!)
                if(restaurantAccountsViewModel.updateRestaurantAddress(restaurantID: (RestaurantIDUtils.shared.getRestaurantId()), restaurantAddress: address)){
                    navigationController?.popViewController(animated: true)
                    showToast(message: "UPDATED ADDRESS SUCCESSFULLY", font: UIFont.systemFont(ofSize: 20, weight: .medium), fontColor: .white, backGroundColor: .systemBlue.withAlphaComponent(0.9))
                }
            }
            else{
                showAlert(message: "The address in unedited")
            }
            
        }
        else{
            showAlert(message: "Please Fill all the fields")
        }
    }
    
    
    
}
