//
//  PastOrderDetailsViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 20/03/22.
//

import Foundation
import UIKit

class PastOrderDetailsViewController : UIViewController{
    
     private var orderDetailsPageView : PastOrdersDetailsViewProtocol!
     private var restaurantOrdersViewModel : RestaurantOrdersViewModel = RestaurantOrdersViewModel()
     private var orderDetails : OrderDetails!
     private var orderMenuDetailsList :[OrderMenuDetails]!
     private var billDetails : BillDetailsModel!
     
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
     
    let scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
        scrollView.backgroundColor = .white
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.isScrollEnabled = true
        scrollView.bounces = false
        return scrollView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        activateViewElementsConstraints()
        getInitialDataFromDBAndUpdateView()
        addDelegatesAndDatasource()
       
    }
    
    
    
   
    
    func setOrderDetailsAndOrderMenuDetails(orderDetails : OrderDetails,orderMenuDetailsList : [OrderMenuDetails]){
        self.orderDetails = orderDetails
        self.orderMenuDetailsList = orderMenuDetailsList
        
    }
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    func getOrderTotalQuantity() -> Int{
        var quantity = 0
        for order in orderMenuDetailsList{
            quantity += order.quantity
        }
        return quantity
    }
    
    
     private func initialiseView(){
        view.backgroundColor = .white
         
        view.addSubview(scrollView)
         orderDetailsPageView = PastOrdersDetailsView()
         orderDetailsPageView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(orderDetailsPageView)
         
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
    
     private func initialiseViewElements(){
        
         
        
         
        if(orderDetails.orderStatus == .Cancelled || orderDetails.orderStatus == .Declined){
           
            orderDetailsPageView.restaurantRatingLabel.isHidden = true
        }
         
        navigationItem.leftBarButtonItem = backArrowButton
         navigationItem.titleView = orderDetailsPageView.navigationBarTitleView
        setDefaultNavigationBarApprearance()
    }
    
    private func activateViewElementsConstraints(){
        orderDetailsPageView.updateMenuDetailsTableViewHeightLayoutConstraints(numberOfRows: orderMenuDetailsList.count)
    }
    
    private func getInitialDataFromDBAndUpdateView(){
        DispatchQueue.global().async { [self] in
            billDetails = restaurantOrdersViewModel.getBillDetails(orderId: orderDetails.orderModel.orderId)
            let userDetails = restaurantOrdersViewModel.getUserDetailsOfOrder(orderId: orderDetails.orderModel.orderId)
            var cancelOrderReason = ""
            if(orderDetails.orderStatus == .Cancelled){
                cancelOrderReason = restaurantOrdersViewModel.getOrderCancellationReason(orderId: orderDetails.orderModel.orderId)
            }
            DispatchQueue.main.async { [self] in
                orderDetailsPageView.updateNavigationBarTitleViewLabel(menuCount: getOrderTotalQuantity() , totalPrice: orderDetails.orderTotal, orderStatus: orderDetails.orderStatus)
                orderDetailsPageView.updateUserDetailsAndOrderDetailsLabel(userName: userDetails.userName, userPhoneNumber: userDetails.userPhoneNumber, dateAndTime: orderDetails.orderModel.dateAndTime, orderId: orderDetails.orderModel.orderId)
                orderDetailsPageView.updateInstructionToRestaurantTextView(instructionsToRestaurantText: orderDetails.instructionToRestaurant)
                orderDetailsPageView.updateBillDetails(itemTotalPrice: billDetails.itemTotal, restaurantPackagingChargePrice: billDetails.restaurantPackagingCharges, deliveryFeePrice: billDetails.deliveryFee, totalAmount: billDetails.totalCost,discountAmount: billDetails.itemDiscount,restaurantGST: billDetails.restaurantGST)
                orderDetailsPageView.updateRestaurantRatingLabel(rating: orderDetails.starRating, restaurantFeedback: orderDetails.feedback)
                if(orderDetails.orderStatus == .Cancelled){
                    orderDetailsPageView.updateCancelOrderReasonLabel(cancelReason: cancelOrderReason, orderStatus: orderDetails.orderStatus)
                }
            }
        }
    }
    
    
     private func addDelegatesAndDatasource(){
         orderDetailsPageView.menuDetailsTableView.delegate = self
         orderDetailsPageView.menuDetailsTableView.dataSource = self
    }
    
    
}

extension PastOrderDetailsViewController : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        orderMenuDetailsList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menuDetails = orderMenuDetailsList[indexPath.row]
        return orderDetailsPageView.createMenuDetailsContentView(indexPath: indexPath, tableView: tableView, menuCellContents:(menuName: menuDetails.menuName, menuTarianType: menuDetails.menuTarianType, menuPriceSubTotal: (menuDetails.price * menuDetails.quantity), quantity: menuDetails.quantity))
       
    }
}


extension PastOrderDetailsViewController{
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        orderDetailsPageView.activateContentViewBottomAnchorWithRespectToRestaurantRatingLabelConstraints()
        
    }
    
    private func getScrollViewConstraints() -> [NSLayoutConstraint]{
        let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                     scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                     scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                     scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
        return scrollViewConstraints
    }

    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [orderDetailsPageView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               orderDetailsPageView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               orderDetailsPageView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               orderDetailsPageView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               orderDetailsPageView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }
    
}
