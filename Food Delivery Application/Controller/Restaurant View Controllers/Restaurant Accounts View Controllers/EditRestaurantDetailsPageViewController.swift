//
//  EditRestaurantDetailsPageViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 30/03/22.
//

import Foundation
import UIKit

class EditRestaurantDetailsPageViewController : UIViewController, UITextFieldDelegate, UITextViewDelegate,EditRestaurantDetailsPageViewDelegate{
    
    
    private var restaurantAccountsViewModel : RestaurantAccountsViewModel =  RestaurantAccountsViewModel()
    private var restaurantContentDetails : RestaurantContentDetails!
    private var editRestaurantDetailsPageView : EditRestaurantDetailsPageView!
    private var keyboardUtils : KeyboardUtils!
    private var activeArea : UIView? = nil
    
    private var scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
        scrollView.backgroundColor = .clear
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.target = self
        backArrowButton.action = #selector(didTapBackArrow)
        return backArrowButton
    }()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        addTargetsAndActionsForElements()
        addDelegatesForElements()
        addObserver()
        navigationItem.leftBarButtonItem = backArrowButton
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
       
        
    }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.global().async { [self] in
            restaurantContentDetails = restaurantAccountsViewModel.getRestaurantDetails(restaurantId: (RestaurantIDUtils.shared.getRestaurantId()))
            DispatchQueue.main.async { [self] in
                setupRestaurantDetailsInViewElements()
            }
        }
        
    }

    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    func didTapChangeProfileImage(){
        let  imagePickerViewController = UIImagePickerController()
        imagePickerViewController.allowsEditing = true
        imagePickerViewController.delegate = self
        imagePickerViewController.sourceType = .photoLibrary
        present(imagePickerViewController, animated: true)
    }
    
    func didTapCancelUpdateNameButton(){
        editRestaurantDetailsPageView.restaurantNameTextField.resignFirstResponder()
        editRestaurantDetailsPageView.restaurantNameTextField.text = restaurantContentDetails.restaurantDetails.restaurantName
        editRestaurantDetailsPageView.updateAndCancelNameHorizontalStackView.isHidden = true
    }

    func didTapUpdateNameButton(){
        let restaurantNameTextFieldText = editRestaurantDetailsPageView.restaurantNameTextField.text!
        DispatchQueue.global().async { [self] in
            if(restaurantAccountsViewModel.updateRestaurantName(restaurantID: (RestaurantIDUtils.shared.getRestaurantId()), restaurantName:restaurantNameTextFieldText )){
                
                DispatchQueue.main.async { [self] in
                    showToast(message: "Updated Name Successfully", font: UIFont.systemFont(ofSize: 25, weight: .medium), fontColor: .white, backGroundColor: .systemBlue.withAlphaComponent(0.9))
                    restaurantContentDetails.restaurantDetails.restaurantName =  restaurantNameTextFieldText
                    didTapCancelUpdateNameButton()
                }
            }
        }

    }

    func didTapVerifyPhoneNumberButton(){
       let verifyOTPAndUpdateRestaurantPhoneNumberViewController = VerifyOTPAndUpdateRestaurantPhoneNumberViewController()
        verifyOTPAndUpdateRestaurantPhoneNumberViewController.setRestaurantPhoneNumber(phoneNumber: editRestaurantDetailsPageView.restaurantPhoneNumberTextField.text!)
        navigationController?.pushViewController(verifyOTPAndUpdateRestaurantPhoneNumberViewController, animated: true)
        restaurantContentDetails.restaurantDetails.restaurantContactNumber =  editRestaurantDetailsPageView.restaurantPhoneNumberTextField.text!
        didTapCancelUpdatePhoneNumberButton()
    }

     func didTapCancelUpdatePhoneNumberButton(){
         editRestaurantDetailsPageView.restaurantPhoneNumberTextField.resignFirstResponder()
         editRestaurantDetailsPageView.restaurantPhoneNumberTextField.text = restaurantContentDetails.restaurantDetails.restaurantContactNumber
         editRestaurantDetailsPageView.verifyAndCancelPhoneNumberHorizontalStackView.isHidden = true
    }
    
     func didTapUpdateCuisineButton(){
         let restaurantCuisineButtonTitleLabelText = editRestaurantDetailsPageView.restaurantCuisineButton.titleLabel!.text!
        DispatchQueue.global().async { [self] in
            if(restaurantAccountsViewModel.updateRestaurantCuisine(restaurantID: (RestaurantIDUtils.shared.getRestaurantId()), restaurantCuisine:restaurantCuisineButtonTitleLabelText )){
                DispatchQueue.main.async { [self] in
                    restaurantContentDetails.restaurantDetails.restaurantCuisine =  restaurantCuisineButtonTitleLabelText
                    showToast(message: "Updated Restautant Cuisine Successfully", font: UIFont.systemFont(ofSize: 25, weight: .medium), fontColor: .white, backGroundColor: .systemBlue.withAlphaComponent(0.9))
                    didTapCancelUpdateCuisineButton()
                }
            }
        }
    }
    
     func didTapCancelUpdateCuisineButton(){
         editRestaurantDetailsPageView.restaurantCuisineButton.resignFirstResponder()
         editRestaurantDetailsPageView.restaurantCuisineButton.setTitle(restaurantContentDetails.restaurantDetails.restaurantCuisine, for: .normal)
         editRestaurantDetailsPageView.updateAndCancelCuisineHorizontalStackView.isHidden = true
    }
    
    func didTapUpdateDescriptionButton(){
        let restaurantDescriptionTextViewText = editRestaurantDetailsPageView.restaurantDescriptionTextView.text!
        DispatchQueue.global().async { [self] in
            if(restaurantAccountsViewModel.updateRestaurantDescription(restaurantID: (RestaurantIDUtils.shared.getRestaurantId()), restaurantDescription: restaurantDescriptionTextViewText)){
                DispatchQueue.main.async { [self] in
                    restaurantContentDetails.restaurantDetails.restaurantDescription = restaurantDescriptionTextViewText
                    showToast(message: "Updated Restautant Description Successfully", font: UIFont.systemFont(ofSize: 25, weight: .medium), fontColor: .white, backGroundColor: .systemBlue.withAlphaComponent(0.9))
                    didTapCancelUpdateDescriptionButton()
                }
            }
        }
    }
    
    func didTapCancelUpdateDescriptionButton(){
        editRestaurantDetailsPageView.restaurantDescriptionTextView.resignFirstResponder()
        editRestaurantDetailsPageView.restaurantDescriptionTextView.text = restaurantContentDetails.restaurantDetails.restaurantDescription
        editRestaurantDetailsPageView.updateAndCancelDescriptionHorizontalStackView.isHidden = true
    }
    
    func didTapVerifySecretCodeButton(){
        if(NetworkMonitor.shared.isReachable){
            showActivityIndicator()
            restaurantAccountsViewModel.getSecretCode { [weak self] code in
                self?.hideActivityIndicator()
                if(code == self?.editRestaurantDetailsPageView.secretCodeTextField.text){
                    let editRestaurantAddressViewController = EditRestaurantAddressViewController()
                    editRestaurantAddressViewController.setRestaurantAddressDetails(restaurantAddress: (self?.restaurantContentDetails.restaurantAddress)!)
                    self?.navigationController?.pushViewController(editRestaurantAddressViewController, animated: true)
                    self?.didTapCancelUpdateAddressButton()
                }
            }
        }
        else{
            showAlert(message: "Please Connect To Internet")
        }
    }
    
    func didTapCancelUpdateAddressButton(){
        editRestaurantDetailsPageView.secretCodeTextField.resignFirstResponder()
        editRestaurantDetailsPageView.secretCodeTextField.text = ""
        editRestaurantDetailsPageView.verifyAndCancelAddressHorizontalStackView.isHidden = true
    }


    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if( textField == editRestaurantDetailsPageView.restaurantPhoneNumberTextField){
            let maxLength = 10
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            var res = ""
            if editRestaurantDetailsPageView.restaurantPhoneNumberTextField.text != "" || string != "" {
                    res = (textField.text ?? "") + string
                }
            return newString.length <= maxLength && Double(res) != nil
        }
        else if(textField == editRestaurantDetailsPageView.secretCodeTextField){
            let maxLength = 9
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            var res = ""
            if editRestaurantDetailsPageView.secretCodeTextField.text != "" || string != "" {
                    res = (textField.text ?? "") + string
                }
            return newString.length <= maxLength && Double(res) != nil
        }
        return true
    }

    @objc func textFieldDidChange(sender : UITextField){
        
        switch sender{
        case editRestaurantDetailsPageView.restaurantNameTextField :
            restaurantNameTextfieldChangesActions()
            break
        case editRestaurantDetailsPageView.restaurantPhoneNumberTextField :
            restaurantPhoneNumberTextFieldChangesActions()
            break
        case editRestaurantDetailsPageView.secretCodeTextField :
            secretCodeTextFieldChangesActions()
        default:
            break
        }
        
       
    }

   
    @objc func didTapView(){
        self.view.endEditing(true)
    }
    
    private func restaurantNameTextfieldChangesActions(){
        editRestaurantDetailsPageView.updateAndCancelNameHorizontalStackView.isHidden = false
        if(editRestaurantDetailsPageView.restaurantNameTextField.text == restaurantContentDetails.restaurantDetails.restaurantName || editRestaurantDetailsPageView.restaurantNameTextField.text!.count == 0){
            editRestaurantDetailsPageView.updateNameButton.backgroundColor = .systemBlue.withAlphaComponent(0.6)
            editRestaurantDetailsPageView.updateNameButton.isUserInteractionEnabled = false

        }
        else{
            editRestaurantDetailsPageView.updateNameButton.backgroundColor = .systemBlue
            editRestaurantDetailsPageView.updateNameButton.isUserInteractionEnabled = true

        }
    }
    
    private func restaurantPhoneNumberTextFieldChangesActions(){
        editRestaurantDetailsPageView.verifyAndCancelPhoneNumberHorizontalStackView.isHidden  = false
        if(editRestaurantDetailsPageView.restaurantPhoneNumberTextField.text == restaurantContentDetails.restaurantDetails.restaurantContactNumber){

            editRestaurantDetailsPageView.verifyPhoneNumberButton.backgroundColor = .systemBlue.withAlphaComponent(0.6)
            editRestaurantDetailsPageView.verifyPhoneNumberButton.isUserInteractionEnabled = false


        }
        else if(editRestaurantDetailsPageView.restaurantPhoneNumberTextField.text?.count == 10){
            editRestaurantDetailsPageView.verifyPhoneNumberButton.backgroundColor = .systemBlue
            editRestaurantDetailsPageView.verifyPhoneNumberButton.isUserInteractionEnabled = true

        }
        else{
            editRestaurantDetailsPageView.verifyPhoneNumberButton.backgroundColor = .systemBlue.withAlphaComponent(0.6)
            editRestaurantDetailsPageView.verifyPhoneNumberButton.isUserInteractionEnabled = false
        }
    }
    
    @objc  func didTapRestaurantCuisine(){
        let dropDownTable =  DropDownTableViewUtils()
        dropDownTable.setRequiredDataForDropDownTable(selectedButton: editRestaurantDetailsPageView.restaurantCuisineButton, dataSourse: RestaurantCuisines.getRestaurantCuisines())
        dropDownTable.modalPresentationStyle = .overCurrentContext
        present(dropDownTable, animated: false, completion : nil)
        dropDownTable.complition = { [self] in
            
            editRestaurantDetailsPageView.updateAndCancelCuisineHorizontalStackView.isHidden = false
            if(editRestaurantDetailsPageView.restaurantCuisineButton.titleLabel!.text! == restaurantContentDetails.restaurantDetails.restaurantCuisine){
                editRestaurantDetailsPageView.updateCuisineButton.backgroundColor = .systemBlue.withAlphaComponent(0.6)
                editRestaurantDetailsPageView.updateCuisineButton.isUserInteractionEnabled = false

                    }
                    else{
                        editRestaurantDetailsPageView.updateCuisineButton.backgroundColor = .systemBlue
                        editRestaurantDetailsPageView.updateCuisineButton.isUserInteractionEnabled = true

                    }
                }
        }
    private func secretCodeTextFieldChangesActions(){
        editRestaurantDetailsPageView.verifyAndCancelAddressHorizontalStackView.isHidden = false
        if(editRestaurantDetailsPageView.secretCodeTextField.text!.count !=  9){
            editRestaurantDetailsPageView.verifysecretCodeToUpdateAddressButton.backgroundColor = .systemBlue.withAlphaComponent(0.6)
            editRestaurantDetailsPageView.verifysecretCodeToUpdateAddressButton.isUserInteractionEnabled = false

        }
        else{
            editRestaurantDetailsPageView.verifysecretCodeToUpdateAddressButton.backgroundColor = .systemBlue
            editRestaurantDetailsPageView.verifysecretCodeToUpdateAddressButton.isUserInteractionEnabled = true

        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textViewDidChange(textView)
    }
    

    func textViewDidChange(_ textView: UITextView) {
        editRestaurantDetailsPageView.updateAndCancelDescriptionHorizontalStackView.isHidden = false
        if(editRestaurantDetailsPageView.restaurantDescriptionTextView.text == restaurantContentDetails.restaurantDetails.restaurantDescription || editRestaurantDetailsPageView.restaurantDescriptionTextView.text!.count == 0){
            editRestaurantDetailsPageView.updateDescriptionButton.backgroundColor = .systemBlue.withAlphaComponent(0.6)
            editRestaurantDetailsPageView.updateDescriptionButton.isUserInteractionEnabled = false

        }
        else{
            editRestaurantDetailsPageView.updateDescriptionButton.backgroundColor = .systemBlue
            editRestaurantDetailsPageView.updateDescriptionButton.isUserInteractionEnabled = true

        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        keyboardUtils.keyboardWillShow(notification: notification, maxYofLastObjectInView: editRestaurantDetailsPageView.stackView.frame.maxY, activeArea: activeArea)
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardUtils.keyboardWillHide()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeArea = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeArea = nil
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        activeArea = textView
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        activeArea = nil
        return true
    }
    
    
    
    private func setupRestaurantDetailsInViewElements(){
        editRestaurantDetailsPageView.restaurantProfileImageView.image = UIImage(data: restaurantContentDetails.restaurantDetails.restaurantProfileImage)
        editRestaurantDetailsPageView.restaurantNameTextField.text = restaurantContentDetails.restaurantDetails.restaurantName
        editRestaurantDetailsPageView.restaurantCuisineButton.setTitle(restaurantContentDetails.restaurantDetails.restaurantCuisine, for: .normal)
        editRestaurantDetailsPageView.restaurantDescriptionTextView.text = restaurantContentDetails.restaurantDetails.restaurantDescription
        editRestaurantDetailsPageView.restaurantPhoneNumberTextField.text = restaurantContentDetails.restaurantDetails.restaurantContactNumber
        editRestaurantDetailsPageView.restaurantAddressLabel.text = "\(restaurantContentDetails.restaurantAddress.doorNoAndBuildingNameAndBuildingNo), \(restaurantContentDetails.restaurantAddress.streetName), \(restaurantContentDetails.restaurantAddress.landmark), \(restaurantContentDetails.restaurantAddress.localityName), \(restaurantContentDetails.restaurantAddress.city), \(restaurantContentDetails.restaurantAddress.state), \(restaurantContentDetails.restaurantAddress.country), \(restaurantContentDetails.restaurantAddress.pincode)"
    }
    
    
    private func initialiseView(){
        title = "EDIT ACCOUNT"
        view.backgroundColor = .white
        
        view.addSubview(scrollView)
        editRestaurantDetailsPageView = EditRestaurantDetailsPageView()
        editRestaurantDetailsPageView.translatesAutoresizingMaskIntoConstraints = false
        editRestaurantDetailsPageView.delegate  = self
        scrollView.addSubview(editRestaurantDetailsPageView)
        
        NSLayoutConstraint.activate(getScrollViewConstraints())
        NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
   
    
    private func addTargetsAndActionsForElements(){
        
        editRestaurantDetailsPageView.restaurantNameTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .allEditingEvents)
        editRestaurantDetailsPageView.restaurantPhoneNumberTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .allEditingEvents)
        
        editRestaurantDetailsPageView.secretCodeTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .allEditingEvents)
        
    }
    
    private func addDelegatesForElements(){
        editRestaurantDetailsPageView.restaurantNameTextField.delegate = self
        editRestaurantDetailsPageView.restaurantPhoneNumberTextField.delegate = self
        editRestaurantDetailsPageView.restaurantDescriptionTextView.delegate = self
        editRestaurantDetailsPageView.secretCodeTextField.delegate = self
        
    }
    
    private func addObserver(){
       keyboardUtils = KeyboardUtils(scrollView: scrollView)
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
     
   }
    
    
   
}

extension EditRestaurantDetailsPageViewController : UIImagePickerControllerDelegate & UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage{
            editRestaurantDetailsPageView.restaurantProfileImageView.image = image
            DispatchQueue.global().async { [self] in
                if(restaurantAccountsViewModel.updateRestaurantImage(restaurantID: RestaurantIDUtils.shared.getRestaurantId(), profileImage: image.jpeg(.medium)!)){
                    DispatchQueue.main.async {
                        return
                    }
                }
            }
        }
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension EditRestaurantDetailsPageViewController{
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
        if(editRestaurantDetailsPageView.stackView.frame.maxY > 0){
            editRestaurantDetailsPageView.removeHeightAnchorConstraints()
            editRestaurantDetailsPageView.heightAnchor.constraint(equalToConstant:  editRestaurantDetailsPageView.stackView.frame.maxY + 100).isActive = true
        }
        else{
            editRestaurantDetailsPageView.removeHeightAnchorConstraints()
            editRestaurantDetailsPageView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
    }
    
    private func getScrollViewConstraints() -> [NSLayoutConstraint]{
        let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                     scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                     scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                     scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
        return scrollViewConstraints
    }

    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [editRestaurantDetailsPageView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               editRestaurantDetailsPageView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               editRestaurantDetailsPageView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               editRestaurantDetailsPageView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               editRestaurantDetailsPageView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }

    
}
