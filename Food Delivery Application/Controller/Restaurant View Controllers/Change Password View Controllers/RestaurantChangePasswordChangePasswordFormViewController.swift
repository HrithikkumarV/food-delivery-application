//
//  RestaurantChangePasswordChangePasswordViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 20/12/21.
//

import Foundation

import UIKit
class RestaurantChangePasswordChangePasswordFormViewController: UIViewController, UITextFieldDelegate,ChangePasswordChangePasswordFormViewDelegate{
    
     private var restaurantChangePasswordChangePasswordFormView: ChangePasswordChangePasswordFormViewProtocol!
     private var restaurantChangePasswordViewModel : RestaurantChangePasswordViewModel!
    private var activeArea : UITextField? = nil
    private var keyboardUtils : KeyboardUtils!
    lazy var backArrowButton : UIBarButtonItem = {
        let  backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
    
    
     
    let scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
       scrollView.backgroundColor = .white
       scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
     
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialiseView()
        initialiseViewElements()
        addDelegatesForElements()
        addObserver()
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if(restaurantChangePasswordChangePasswordFormView.stackView.frame.maxY > 0){
            restaurantChangePasswordChangePasswordFormView.removeHeightAnchorConstraints()
            restaurantChangePasswordChangePasswordFormView.heightAnchor.constraint(equalToConstant:  restaurantChangePasswordChangePasswordFormView.stackView.frame.maxY + 100).isActive = true
        }
        else{
            restaurantChangePasswordChangePasswordFormView.removeHeightAnchorConstraints()
            restaurantChangePasswordChangePasswordFormView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
       
        case restaurantChangePasswordChangePasswordFormView.passwordTextField :
            restaurantChangePasswordChangePasswordFormView.confirmPasswordTextField.becomeFirstResponder()
        case restaurantChangePasswordChangePasswordFormView.confirmPasswordTextField :
            restaurantChangePasswordChangePasswordFormView.confirmPasswordTextField.resignFirstResponder()
        default:
            return true
        }
        return true
    }
    
    @objc func didTapView(){
        view.endEditing(true)
    }
    
    @objc func didTapSaveChangesButton(){
        if(!restaurantChangePasswordChangePasswordFormView.passwordTextField.text!.isEmpty && !restaurantChangePasswordChangePasswordFormView.confirmPasswordTextField.text!.isEmpty){
            if(restaurantChangePasswordChangePasswordFormView.passwordTextField.text == restaurantChangePasswordChangePasswordFormView.confirmPasswordTextField.text){
                if(restaurantChangePasswordChangePasswordFormView.passwordTextField.text!.isValidPassword()){
                    let passwordText =  restaurantChangePasswordChangePasswordFormView.passwordTextField.text!
                        DispatchQueue.global().async { [self] in
                            if(restaurantChangePasswordViewModel.updatePassword(password: passwordText.encryptString())){
                                DispatchQueue.main.async {
                                    self.navigationController?.popToRootViewController(animated: true)
                                    self.showToast(message: "Successfully changed Password", font: UIFont.systemFont(ofSize: 25, weight: .medium), fontColor: .white, backGroundColor: .systemGreen.withAlphaComponent(0.9))
                                }
                            }
                        }
                        
                   
                }
            }
        }
    }
    
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    func showEmptyFieldAlert(message : String){
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel,handler:{ _ in
            
            if(self.restaurantChangePasswordChangePasswordFormView.passwordTextField.text!.isEmpty){
                self.restaurantChangePasswordChangePasswordFormView.passwordTextField.becomeFirstResponder()
            }
            else{
                self.restaurantChangePasswordChangePasswordFormView.confirmPasswordTextField.becomeFirstResponder()
            }
        }))
        present(alert,animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        restaurantChangePasswordChangePasswordFormView.passwordTextField.becomeFirstResponder()
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        keyboardUtils.keyboardWillShow(notification: notification, maxYofLastObjectInView: restaurantChangePasswordChangePasswordFormView.stackView.frame.maxY, activeArea: activeArea)
       
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        
        keyboardUtils.keyboardWillHide()
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeArea = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeArea = nil
    }
    
    func setRestaurantForGotPasswordViewModel(restaurantChangePasswordViewModel : RestaurantChangePasswordViewModel){
        self.restaurantChangePasswordViewModel = restaurantChangePasswordViewModel
    }
    
    
     private func initialiseView(){
        title = "Authentication Details"
        view.backgroundColor = .white
        
        view.addSubview(scrollView)
         restaurantChangePasswordChangePasswordFormView = ChangePasswordChangePasswordFormView()
         restaurantChangePasswordChangePasswordFormView.translatesAutoresizingMaskIntoConstraints = false
         restaurantChangePasswordChangePasswordFormView.delegate = self
        scrollView.addSubview(restaurantChangePasswordChangePasswordFormView)
//
//         NSLayoutConstraint.activate(restaurantChangePasswordChangePasswordFormView.getScrollViewConstraints())
//         NSLayoutConstraint.activate(restaurantChangePasswordChangePasswordFormView.getContentViewConstraints())
    }
    
     private func initialiseViewElements(){
        
        
        
        navigationItem.leftBarButtonItem = backArrowButton 
        
        
    }
    
   
     private func addDelegatesForElements(){
         restaurantChangePasswordChangePasswordFormView.passwordTextField.delegate = self
         restaurantChangePasswordChangePasswordFormView.confirmPasswordTextField.delegate = self
    }
    
     private func addObserver(){
        keyboardUtils = KeyboardUtils(scrollView: scrollView)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
    }
}
