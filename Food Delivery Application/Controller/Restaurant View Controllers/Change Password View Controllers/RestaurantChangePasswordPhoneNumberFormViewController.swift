//
//  ChangePasswordViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 20/12/21.
//

import Foundation
import UIKit

class RestaurantChangePasswordPhoneNumberFormViewController : UIViewController,UITextFieldDelegate,ChangePasswordPhoneNumberFormViewDelegate{
    
     private var restaurentChangePasswordPhoneNumberFormView : ChangePasswordPhoneNumberFormViewProtocol!
     private var restaurantChangePasswordViewModel : RestaurantChangePasswordViewModel = RestaurantChangePasswordViewModel()
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
     
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        
        addDelegatesForElements()
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func didTapVerify(){
        if(restaurentChangePasswordPhoneNumberFormView.phoneNumberTextField.text?.isValidPhoneNumber() == true){
            let phoneNumberText = restaurentChangePasswordPhoneNumberFormView.phoneNumberTextField.text!
            DispatchQueue.global().async { [self] in
                if(restaurantChangePasswordViewModel.checkIfPhoneNumberExists(phoneNumber: phoneNumberText) == true){
                    DispatchQueue.main.async { [self] in
                        if(NetworkMonitor.shared.isReachable){
                            let restaurantChangePasswordOTPFormViewController = RestaurantChangePasswordOTPFormViewController()
                            restaurantChangePasswordOTPFormViewController.setRestaurantPhoneNumber(phoneNumber: phoneNumberText)
                            restaurantChangePasswordOTPFormViewController.setRestaurantForGotPasswordViewModel(restaurantChangePasswordViewModel: restaurantChangePasswordViewModel)
                                navigationController?.pushViewController(restaurantChangePasswordOTPFormViewController, animated: true)
                            
                        }
                        else{
                            showAlert(message: "Please Connect to Internet")
                        }
                    }
                }
                else{
                    DispatchQueue.main.async {
                        self.showAlert(message: "Phone number does not exist")
                    }
                }
            }
            
        }
        else{
            showAlert(message: "Please enter a valid Phone number")
        }
    }
    
    
    @objc func didTapPhoneNumberkeyboardNextBarButton(){
        didTapVerify()
    }
    
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        restaurentChangePasswordPhoneNumberFormView.phoneNumberTextField.becomeFirstResponder()
    }
    
   
    
     private func initialiseView(){
        title = "Change password"
        view.backgroundColor = .white
         restaurentChangePasswordPhoneNumberFormView = ChangePasswordPhoneNumberFormView()
         restaurentChangePasswordPhoneNumberFormView.translatesAutoresizingMaskIntoConstraints = false
         restaurentChangePasswordPhoneNumberFormView.delegate = self
        view.addSubview(restaurentChangePasswordPhoneNumberFormView)
        NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
     private func initialiseViewElements(){
         
      
        navigationItem.leftBarButtonItem = backArrowButton 
        
    }
    
    
    
     private func addDelegatesForElements(){
         restaurentChangePasswordPhoneNumberFormView.phoneNumberTextField.delegate = self
    }
}
extension RestaurantChangePasswordPhoneNumberFormViewController{
    
   
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let contentViewConstraints = [restaurentChangePasswordPhoneNumberFormView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor),
                                      restaurentChangePasswordPhoneNumberFormView.bottomAnchor.constraint(equalTo : view.safeAreaLayoutGuide.bottomAnchor),
                                      restaurentChangePasswordPhoneNumberFormView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                                      restaurentChangePasswordPhoneNumberFormView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)]
            return contentViewConstraints
    }
}
  
