//
//  RestaurantChangePasswordOTPFormViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 20/12/21.
//

import Foundation

import UIKit

class RestaurantChangePasswordOTPFormViewController : RestaurantCreateAccountOTPFormViewController{
    
    
     private var restaurantChangePasswordViewModel : RestaurantChangePasswordViewModel!
    
   
    
   @objc override func didTapContinueToVerify(){
       if(restaurantCreateAccountOTPFormView.OTPTextField.text! == OTP){
            let restaurantChangePasswordChangePasswordFormViewController =
            RestaurantChangePasswordChangePasswordFormViewController()
            restaurantChangePasswordChangePasswordFormViewController.setRestaurantForGotPasswordViewModel(restaurantChangePasswordViewModel: restaurantChangePasswordViewModel)
            restaurantChangePasswordViewModel.setPhoneNumber(phoneNumber: restaurantPhoneNumber!)
            navigationController?.pushViewController(restaurantChangePasswordChangePasswordFormViewController, animated: true)
        }
        else{
            showAlert(message: "Enter valid OTP")
        }
    }
    
    
    
    func setRestaurantForGotPasswordViewModel(restaurantChangePasswordViewModel : RestaurantChangePasswordViewModel){
        self.restaurantChangePasswordViewModel = restaurantChangePasswordViewModel
    }
    
}
