//
//  OrdersTabViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 11/11/21.
//

import Foundation
import UIKit

class OrdersTabViewController : UIViewController,OrderPageViewDelegate{
     private var orderPageView : OrdersPageViewProtocol!
     private var restaurantOrdersViewModel : RestaurantOrdersViewModel = RestaurantOrdersViewModel()
     private var activeOrderIdList : [String] = []
     private var activeOrderDetailsInDictionaryFormat : [String : OrderDetails] = [:]
     private var activeOrderFoodDetails : [String : [OrderMenuDetails]] = [:]
     private var splitOrderIdsBasedOnOrderStatus : [OrderStatus : [String]] = [:]
     private var orderStatusOrderList : [OrderStatus] = [.Pending,.Preparing,.Ready]
    override func viewDidLoad(){
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addDelegates()
    }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.global().async {
            self.getOrderData()
            DispatchQueue.main.async { [self] in
                showWaitingForOrderLabelBasedOnOrdersCount()
                orderPageView.orderDetailsTableView.reloadData()
            }
        }
    }
    
    
    func removeUnavailableOrderStatus(){
        var index = 0
        while index  < orderStatusOrderList.count{
            if splitOrderIdsBasedOnOrderStatus[orderStatusOrderList[index]] != nil{
                index += 1
            }
            else{
                orderStatusOrderList.remove(at: index)
            }
        }
    }
    
    
    func getOrderData(){
        let orderDetails = restaurantOrdersViewModel.getActiveOrderDetails(restaurantId: (RestaurantIDUtils.shared.getRestaurantId()))
        activeOrderDetailsInDictionaryFormat = orderDetails.activeOrderDetailsInDictionaryFormat
        activeOrderIdList = orderDetails.activeOrderIdList
        activeOrderFoodDetails = restaurantOrdersViewModel.getMenuDetailsInActiveOrderFoodDetails(restaurantId: (RestaurantIDUtils.shared.getRestaurantId()))
        splitOrderIdsBasedOnOrderStatus = restaurantOrdersViewModel.getSplitOrderIdsBasedOnTheOrderStatus()
        orderStatusOrderList = [.Pending,.Preparing,.Ready]
        removeUnavailableOrderStatus()
    }
    
    func showWaitingForOrderLabelBasedOnOrdersCount(){
        if(activeOrderIdList.count != 0){
            orderPageView.waitingForOrdersLabel.isHidden  = true
            orderPageView.refreshPageButton.isHidden = true
            orderPageView.orderDetailsTableView.isHidden = false
        }
        else{
            orderPageView.waitingForOrdersLabel.isHidden = false
            orderPageView.refreshPageButton.isHidden = false
            orderPageView.orderDetailsTableView.isHidden = true
        }
    }
    
    func didTapRefreshButton(){
        showActivityIndicator()
        DispatchQueue.global(qos: .userInitiated).async { [self] in
            getOrderData()
            DispatchQueue.main.async{ [self] in
                showWaitingForOrderLabelBasedOnOrdersCount()
                orderPageView.orderDetailsTableView.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.hideActivityIndicator()
                }
            }
        }
       
        
    }
    
     private func initialiseView(){
        view.backgroundColor = .white
        title = "Orders"
         orderPageView = OrderPageView()
         orderPageView.translatesAutoresizingMaskIntoConstraints = false
         orderPageView.delegate = self
        view.addSubview(orderPageView)
        NSLayoutConstraint.activate(getContentViewConstraints())
        
    }
    
    
    
     private func initialiseViewElements(){
         setDefaultNavigationBarApprearance()
         setScrollEdgeApprearance()
    }
    
    
    
     private func addDelegates(){
         orderPageView.orderDetailsTableView.delegate = self
         orderPageView.orderDetailsTableView.dataSource = self
         
     }
}

extension OrdersTabViewController : UITableViewDelegate, UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        splitOrderIdsBasedOnOrderStatus.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            return UIView()
        }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let orderStatusLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width , height: 50))
        orderStatusLabel.backgroundColor = .white
        orderStatusLabel.font = UIFont.systemFont(ofSize: 25, weight: .bold)
        orderStatusLabel.layer.cornerRadius  = 5
        orderStatusLabel.backgroundColor = .white
        switch orderStatusOrderList[section]{
        case .Pending:
            orderStatusLabel.text = "Pending Orders"
            orderStatusLabel.textColor = .systemRed
            
        case .Preparing:
            orderStatusLabel.text = "Preparing Orders"
            orderStatusLabel.textColor = .systemGreen
        case .Ready:
            orderStatusLabel.text = "Ready Orders"
            orderStatusLabel.textColor = .systemGreen
        default:
            orderStatusLabel.text = ""
            orderStatusLabel.textColor = .white
        }
        
        return orderStatusLabel
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(splitOrderIdsBasedOnOrderStatus , orderStatusOrderList)
        return splitOrderIdsBasedOnOrderStatus[orderStatusOrderList[section]]!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let orderId = splitOrderIdsBasedOnOrderStatus[orderStatusOrderList[indexPath.section]]![indexPath.row]
        
        return orderPageView.createOrdersTableViewCell(indexPath: indexPath, tableView: tableView, orderCellContents: (orderId: orderId, orderStatus: orderStatusOrderList[indexPath.section], orderTotal: activeOrderDetailsInDictionaryFormat[orderId]!.orderTotal, menuNameAndQuantities: restaurantOrdersViewModel.getMenuNamesAndQuantityList(orderId: orderId), dateAndTime: activeOrderDetailsInDictionaryFormat[orderId]!.orderModel.dateAndTime ))
    }
    
    
   
                
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        orderPageView.getOrderTabelViewCellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let orderId = splitOrderIdsBasedOnOrderStatus[orderStatusOrderList[indexPath.section]]![indexPath.row]
        let orderDetailsPageViewController = orderDetailsPageViewController()
        orderDetailsPageViewController.setOrderDetailsAndOrderMenuDetails(orderDetails: activeOrderDetailsInDictionaryFormat[orderId]!, orderMenuDetailsList: activeOrderFoodDetails[orderId]!)
        tabBarController?.navigationController?.pushViewController(orderDetailsPageViewController, animated: true)
    }
    
}

extension OrdersTabViewController{
    
   
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let contentViewConstraints = [orderPageView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor),
                                      orderPageView.bottomAnchor.constraint(equalTo : view.safeAreaLayoutGuide.bottomAnchor),
                                      orderPageView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                                      orderPageView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)]
            return contentViewConstraints
    }
}
    


    
    
    
    
    
    
    


