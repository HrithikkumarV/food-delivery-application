//
//  OrderDetailsPageViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 16/03/22.
//

import Foundation
import UIKit

class orderDetailsPageViewController : UIViewController,OrderDetailsPageViewDelegate{
    
    
     private var orderDetailsPageView : OrderDetailsPageViewProtocol!
     private var restaurantOrdersViewModel : RestaurantOrdersViewModel = RestaurantOrdersViewModel()
     private var orderDetails : OrderDetails!
     private var orderMenuDetailsList :[OrderMenuDetails]!
     private var billDetails : BillDetailsModel!
    
    lazy var backArrowButton : UIBarButtonItem = {
        let  backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
    
    let scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
        scrollView.backgroundColor = .white
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.isScrollEnabled = true
        scrollView.bounces = false
        return scrollView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        getDataFromDBAndUpdateView()
        addDelegatesAndDatasource()
       
    }
    

    
   
    
    func setOrderDetailsAndOrderMenuDetails(orderDetails : OrderDetails,orderMenuDetailsList : [OrderMenuDetails]){
        self.orderDetails = orderDetails
        self.orderMenuDetailsList = orderMenuDetailsList
        
    }
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    
    
    func didTapDeclineOrder(){
        
        let alertController = UIAlertController(title: "Decline Order", message: "Please give the reason to decline the order", preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.placeholder = "Reason to Decline Order"
            textField.becomeFirstResponder()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let DeclineAction = UIAlertAction(title: "Decline", style: .destructive) { [self] _ in
            let reason = alertController.textFields![0].text
            if(reason!.count > 0){
                DispatchQueue.global().async { [self] in
                    if(restaurantOrdersViewModel.updateOrderCancellationReason(orderId: orderDetails.orderModel.orderId, orderCancellationReason: reason!)){
                        if(restaurantOrdersViewModel.updateOrderStatus(orderId: orderDetails.orderModel.orderId, orderStatus: .Declined)){
                            DispatchQueue.main.async { [self] in
                                showToast(message: "Order Declined", font: UIFont.systemFont(ofSize: 25, weight: .medium), fontColor: .white, backGroundColor: .systemRed.withAlphaComponent(0.9))
                                navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                }
               
            }

        }

        alertController.addAction(cancelAction)
        alertController.addAction(DeclineAction)

        present(alertController, animated: true, completion: nil)
        
        
    }
    
    func updateOrderStatusButtonTitle(){
        if(orderDetails.orderStatus == .Pending){
            orderDetailsPageView.updateOrderStatusButton.setTitle("Confirm", for: .normal)

        }
        else if(orderDetails.orderStatus == .Preparing){
            orderDetailsPageView.updateOrderStatusButton.setTitle("Ready", for: .normal)
                
        }
        else if(orderDetails.orderStatus == .Ready) {
            orderDetailsPageView.updateOrderStatusButton.setTitle("Picked-UP", for: .normal)
            }
        else{
            orderDetailsPageView.updateOrderStatusButton.isHidden = true
            orderDetailsPageView.declineOrderButton.isHidden = true
        }
    }
    
    func didTapUpdateOrderStatusButton(){
        if(orderDetails.orderStatus == .Pending){
            DispatchQueue.global().async { [self] in
                if(restaurantOrdersViewModel.updateOrderStatus(orderId: orderDetails.orderModel.orderId, orderStatus: .Preparing)){
                    DispatchQueue.main.async { [self] in
                        orderDetailsPageView.updateOrderStatusButton.setTitle("Ready", for: .normal)
                        orderDetails.orderStatus =  .Preparing
                        orderDetailsPageView.updateOrderStatus(orderStatus: .Preparing)
                    }
                }
            }
        }
        else if(orderDetails.orderStatus == .Preparing){
            DispatchQueue.global().async { [self] in
                if(restaurantOrdersViewModel.updateOrderStatus(orderId: orderDetails.orderModel.orderId, orderStatus: .Ready)){
                    DispatchQueue.main.async { [self] in
                        orderDetailsPageView.updateOrderStatusButton.setTitle("Picked-UP", for: .normal)
                        orderDetails.orderStatus =  .Ready
                        orderDetailsPageView.updateOrderStatus(orderStatus: .Ready)
                    }
                }
            }
            
        }
            else if(orderDetails.orderStatus == .Ready){
                DispatchQueue.global().async { [self] in
                    if(restaurantOrdersViewModel.updateOrderStatus(orderId: orderDetails.orderModel.orderId, orderStatus: .Delivered)){
                            
                        DispatchQueue.main.async { [self] in
                            orderDetails.orderStatus =  .Delivered
                            orderDetailsPageView.updateOrderStatus(orderStatus: .Delivered)
                            self.showToast(message: "Order Completed", font: UIFont.systemFont(ofSize: 20, weight: .medium), fontColor: .white, backGroundColor: .systemGreen.withAlphaComponent(0.9))
                            navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        updateOrderStatusButtonTitle()
    }
    
    func getOrderTotalQuantity() -> Int{
        var quantity = 0
        for order in orderMenuDetailsList{
            quantity += order.quantity
        }
        return quantity
    }
    
     private func initialiseView(){
        view.backgroundColor = .white
        
        view.addSubview(scrollView)
         orderDetailsPageView = OrderDetailsPageView()
         orderDetailsPageView.translatesAutoresizingMaskIntoConstraints = false
         orderDetailsPageView.delegate = self
         scrollView.addSubview(orderDetailsPageView)
         
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
    
     private func initialiseViewElements(){
         orderDetailsPageView.updateMenuDetailsTableViewHeightLayoutConstraints(numberOfRows: orderMenuDetailsList.count)
        navigationItem.leftBarButtonItem = backArrowButton 
         navigationItem.titleView = orderDetailsPageView.navigationBarTitleView
         setDefaultNavigationBarApprearance()
    }
    
    
    
     private func addDelegatesAndDatasource(){
         orderDetailsPageView.menuDetailsTableView.delegate = self
         orderDetailsPageView.menuDetailsTableView.dataSource = self
    }
    
    private func getDataFromDBAndUpdateView(){
        DispatchQueue.global().async { [self] in
            billDetails = restaurantOrdersViewModel.getBillDetails(orderId: orderDetails.orderModel.orderId)
            let userDetails = restaurantOrdersViewModel.getUserDetailsOfOrder(orderId: orderDetails.orderModel.orderId)
            DispatchQueue.main.async { [self] in
                orderDetailsPageView.updateNavigationBarTitleViewLabel(menuCount: getOrderTotalQuantity() , totalPrice: orderDetails.orderTotal, orderStatus: orderDetails.orderStatus)
                orderDetailsPageView.updateUserDetailsAndOrderDetailsLabel(userName: userDetails.userName, userPhoneNumber: userDetails.userPhoneNumber, dateAndTime: orderDetails.orderModel.dateAndTime, orderId: orderDetails.orderModel.orderId)
                orderDetailsPageView.updateInstructionToRestaurantTextView(instructionsToRestaurantText: orderDetails.instructionToRestaurant)
                orderDetailsPageView.updateBillDetails(itemTotalPrice: billDetails.itemTotal, restaurantPackagingChargePrice: billDetails.restaurantPackagingCharges, deliveryFeePrice: billDetails.deliveryFee, totalAmount: billDetails.totalCost,discountAmount: billDetails.itemDiscount,restaurantGST: billDetails.restaurantGST)
            }
        }
    }
    
    
}

extension orderDetailsPageViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        orderMenuDetailsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menuDetails = orderMenuDetailsList[indexPath.row]
        return orderDetailsPageView.createMenuDetailsContentView(indexPath: indexPath, tableView: tableView, menuCellContents: (menuName: menuDetails.menuName, menuTarianType: menuDetails.menuTarianType, menuPriceSubTotal: (menuDetails.price * menuDetails.quantity), menuAvailable: menuDetails.menuIsAvailable, quantity: menuDetails.quantity))
    }
}

extension orderDetailsPageViewController{
    
    private func getScrollViewConstraints() -> [NSLayoutConstraint]{
        let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                     scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                     scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                     scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
        return scrollViewConstraints
    }

private func getContentViewConstraints() -> [NSLayoutConstraint]{
    let viewConstraints = [orderDetailsPageView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                           orderDetailsPageView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                           orderDetailsPageView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                           orderDetailsPageView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                           orderDetailsPageView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                 ]
    return viewConstraints
}
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        orderDetailsPageView.activateContentViewBottomAnchorWithRespectToBillView()
        
    }
}
