//
//  RestaurantEditMenuDetailsViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 18/02/22.
//

import Foundation
import UIKit

class RestaurantEditMenuDetailsViewController : RestaurantAddMenuDetailsViewController,RestaurantEditMenuDetailsViewDelegate {
     private var menuDetails : MenuContentDetails!
     private var menuEditedDetails : MenuContentDetails = MenuContentDetails()
     private var editMenuDetailsView : RestaurantEditMenuDetailsViewProtocol!
     private var restaurantEditMenuDetailsViewModel : RestaurantAddAndEditMenuDetailsViewModel = RestaurantAddAndEditMenuDetailsViewModel()
    
    override func initialiseView(){
        title = "Menu Details"
        view.backgroundColor = .white
        view.addSubview(scrollView)
         editMenuDetailsView = RestaurantEditMenuDetailsView()
         menuDetailsView = editMenuDetailsView
         menuDetailsView.translatesAutoresizingMaskIntoConstraints = false
         menuDetailsView.delegate = self
         editMenuDetailsView.subDelegate = self
         scrollView.addSubview(editMenuDetailsView)
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addDescriptionBoxLabel()
    }
    
    func addDescriptionBoxLabel(){
        menuDescriptionPlaceHolderTextViewDelegate.addLabelToTopBorder(frame:  editMenuDetailsView.stackViewForMenuDetails.convert(editMenuDetailsView.menuDescriptionTextView.frame, to: self.editMenuDetailsView))
        menuDescriptionPlaceHolderTextViewDelegate.showTopBorderLabel()
    }
    
    func setMenuDetails(menuDetails : MenuContentDetails,menuCategoryName: String){
        self.menuDetails = menuDetails
        self.menuCategoryName = menuCategoryName
    }
    
     func setMenuDetailsInViewElements(){
        let menu = menuDetails.menuDetails
         editMenuDetailsView.menuNameTextField.text = menu.menuName
         editMenuDetailsView.menuNameTextField.showTopBorderLabel()
         editMenuDetailsView.menuDescriptionTextView.text = menu.menuDescription
         editMenuDetailsView.menuDescriptionTextView.textColor = .black
        menuCategoryId = menu.menuCategoryId
         editMenuDetailsView.menuCategoryTypeLabel.tag = menuCategoryId
         editMenuDetailsView.menuTarianTypeDropDownListButton.setTitle("\(menu.menuTarianType)", for: .normal)
         editMenuDetailsView.menuTarianTypeDropDownListButton.setTitleColor(.black, for: .normal)
         editMenuDetailsView.menuTarianTypeDropDownListButton.showTopBorderLabelInView()
         editMenuDetailsView.menuPriceTextField.text = String(Int(menu.menuPrice))
         editMenuDetailsView.menuPriceTextField.showTopBorderLabel()
        if let menuImage = menu.menuImage{
            editMenuDetailsView.menuImageView.image = UIImage(data: menuImage)
        }
         editMenuDetailsView.changeAddOrChangeImageButtonTitle(menuImage: editMenuDetailsView.menuImageView.image, changeMenuImageButton: editMenuDetailsView.changeMenuImageButton)
         editMenuDetailsView.menuDelistToggleSwitch.setOn(menuDetails.menuStatus == 0 ? true : false, animated: false)
        menuEditedDetails.menuId =  menuDetails.menuId
        menuEditedDetails.menuStatus =  menuDetails.menuStatus
        menuEditedDetails.menuIsAvailable =  menuDetails.menuIsAvailable
        menuEditedDetails.menuNextAvailableAt =  menuDetails.menuNextAvailableAt
        if(menuDetails.menuIsAvailable == 0){
            editMenuDetailsView.menuIsAvailableToggleSwitch.setOn(true, animated: false)
            editMenuDetailsView.showSetNextAvailablityItems()
        }
        else{
            editMenuDetailsView.menuIsAvailableToggleSwitch.setOn(false, animated: false)
            editMenuDetailsView.hideSetNextAvailablityItems()
        }
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"
         editMenuDetailsView.datePicker.setDate(dateFormatter.date(from: menuDetails.menuNextAvailableAt) ?? Date(), animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addElementsToStackView()
        setMenuDetailsInViewElements()
    }
    
    
    
    
    
    @objc override func didTapSave(){
        if(!editMenuDetailsView.menuNameTextField.text!.isEmpty && !editMenuDetailsView.menuPriceTextField.text!.isEmpty &&  editMenuDetailsView.menuDescriptionTextView.text! != "Write the Description about your menu" && editMenuDetailsView.menuTarianTypeDropDownListButton.currentTitle != " Select Tarian Type" && menuCategoryId != 0){
            
            let menu = MenuDetails(restaurantId: RestaurantIDUtils.shared.getRestaurantId() , menuName: editMenuDetailsView.menuNameTextField.text!, menuDescription: editMenuDetailsView.menuDescriptionTextView.text!, menuPrice: Int(editMenuDetailsView.menuPriceTextField.text!)!, menuCategoryId: menuCategoryId, menuTarianType: editMenuDetailsView.menuTarianTypeDropDownListButton.currentTitle!, menuImage: editMenuDetailsView.menuImageView.image?.jpeg(.medium))
            menuEditedDetails.menuDetails =  menu
            DispatchQueue.global().async { [self] in
                if(restaurantEditMenuDetailsViewModel.updateMenuDetails(menuContentDetails: menuEditedDetails)){
                    DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
            
        }
        else{
            showAlert(message : "Please fill all the fields")
        }
    }
    

     func didTapChangeMenuImage(){
        let imagePickerViewController = UIImagePickerController()
        imagePickerViewController.allowsEditing = true
        imagePickerViewController.delegate = self
        imagePickerViewController.sourceType = .photoLibrary
        present(imagePickerViewController, animated: true, completion: nil)
    }
    
     func didTapMenuDelistSwitch(sender: UISwitch){
        if sender.isOn{
           menuEditedDetails.menuStatus =  0
        }
        else{
            menuEditedDetails.menuStatus =  1
            menuEditedDetails.menuIsAvailable =  0
        }
    }
    
     func didTapMenuIsAvailableSwitch(sender: UISwitch){
        if(sender.isOn){
            editMenuDetailsView.showSetNextAvailablityItems()
            menuEditedDetails.menuIsAvailable =  0
        }
        else{
            editMenuDetailsView.hideSetNextAvailablityItems()
            menuEditedDetails.menuIsAvailable =  1
        }
    }
    
    func didChangeMenuDateAndtime(sender: UIDatePicker){
    
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"
        let selectedDate: String = dateFormatter.string(from: sender.date)
        menuEditedDetails.menuNextAvailableAt =  selectedDate
        
    }
    
    func didTapRemoveMenuImage(){
        editMenuDetailsView.menuImageView.image = nil
        editMenuDetailsView.changeAddOrChangeImageButtonTitle(menuImage: editMenuDetailsView.menuImageView.image, changeMenuImageButton: editMenuDetailsView.changeMenuImageButton)
    }
    
    
    override func addElementsToStackView(){
        editMenuDetailsView.stackViewForMenuDetails.addArrangedSubview(editMenuDetailsView.menuNameTextField)
        editMenuDetailsView.stackViewForMenuDetails.addArrangedSubview(editMenuDetailsView.menuDescriptionTextView)
        editMenuDetailsView.stackViewForMenuDetails.addArrangedSubview(editMenuDetailsView.menuTarianTypeDropDownListButton)
        editMenuDetailsView.stackViewForMenuDetails.addArrangedSubview(editMenuDetailsView.menuCategoryTypeLabel)
        editMenuDetailsView.stackViewForMenuDetails.addArrangedSubview(editMenuDetailsView.menuPriceTextField)
        editMenuDetailsView.stackViewForMenuDetails.addArrangedSubview(editMenuDetailsView.menuImageSectionView)
        editMenuDetailsView.stackViewForMenuDetails.addArrangedSubview(editMenuDetailsView.menuDelistToggleSwitchView)
        editMenuDetailsView.stackViewForMenuDetails.addArrangedSubview(editMenuDetailsView.menuIsAvailableView)
    }
   
    
     
    
     
    
    
     
     
    
}

// add menu image
extension RestaurantEditMenuDetailsViewController {
    
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage{
            editMenuDetailsView.menuImageView.image = image
            editMenuDetailsView.changeAddOrChangeImageButtonTitle(menuImage: editMenuDetailsView.menuImageView.image, changeMenuImageButton: editMenuDetailsView.changeMenuImageButton)
        }
        picker.dismiss(animated: true, completion: nil)
        
    }
    
}
