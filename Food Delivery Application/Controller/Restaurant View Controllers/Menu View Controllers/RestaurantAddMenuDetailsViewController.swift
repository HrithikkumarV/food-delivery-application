//
//  MenuTabViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 11/11/21.
//

import Foundation
import UIKit

class RestaurantAddMenuDetailsViewController : UIViewController , UITextViewDelegate , UITextFieldDelegate,RestaurantAddMenuDetailsViewDelegate{
     
    var menuDetailsView : RestaurantAddMenuDetailsViewProtocol!
     var restaurantAddMenuDetailsViewModel : RestaurantAddAndEditMenuDetailsViewModel = RestaurantAddAndEditMenuDetailsViewModel()
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
     
     var menuDescriptionPlaceHolderTextViewDelegate : TextViewPlaceHolderUtils!
    
     var menuCategoryId : Int = 0
     var menuCategoryName : String = ""
     
     var keyboardUtils : KeyboardUtils!
     var activeArea : UIView? = nil
     var viewLayoutAdjustmentBasedOnKeyBoard: KeyboardUtils!
     
    let scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
       scrollView.translatesAutoresizingMaskIntoConstraints = false
       scrollView.backgroundColor = .white
        return scrollView
    }()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addDelegatesForElements()
        addObserver()
    }
    
    
   
    override func viewWillAppear(_ animated: Bool) {
        addElementsToStackView()
        menuDetailsView.menuNameTextField.becomeFirstResponder()

        }
    
    
    func setCategoryName(categoryName : String,categoryId : Int){
        menuCategoryName =  categoryName
        menuCategoryId = categoryId
    }
    
     
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
       
        case menuDetailsView.menuNameTextField :
            menuDetailsView.menuDescriptionTextView.becomeFirstResponder()
       
        default:
            return true
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if  textField == menuDetailsView.menuPriceTextField && (textField.text != "" || string != "" ){
            let res = (textField.text ?? "") + string
            return Double(res) != nil
        }
        return true
    }
    func didTapView(){
       view.endEditing(true)
   }
   
    
    @objc func didTapSave(){
        if(!menuDetailsView.menuNameTextField.text!.isEmpty && !menuDetailsView.menuPriceTextField.text!.isEmpty &&  menuDetailsView.menuDescriptionTextView.text! != "Write the Description about your menu" && menuDetailsView.menuTarianTypeDropDownListButton.currentTitle != " Select Tarian Type"){
            let menuDetails = MenuDetails(restaurantId: RestaurantIDUtils.shared.getRestaurantId(), menuName: menuDetailsView.menuNameTextField.text!, menuDescription: menuDetailsView.menuDescriptionTextView.text!, menuPrice: Int(menuDetailsView.menuPriceTextField.text!)!, menuCategoryId: menuCategoryId, menuTarianType: menuDetailsView.menuTarianTypeDropDownListButton.currentTitle!, menuImage: menuDetailsView.menuImageView.image?.jpeg(.medium))
            DispatchQueue.global().async { [self] in
                if(restaurantAddMenuDetailsViewModel.persistMenuDetails(menuDetails: menuDetails)){
                    DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
            
            }

        else{
            showAlert(message : "Please fill all the fields")
        }
    }
    
    
    
    
    
    
    
     func didTapMenuDescriptionKeyboardNextBarButton(){
        didTapMenuTarianType()
    }
    
     func didTapMenuPriceKeyboardNextBarButton(){
        didTapAddMenuImage()
         menuDetailsView.menuPriceTextField.resignFirstResponder()
    }
   
    
    
    
    @objc func didTapMenuTarianType(){
        let dropDownTable =  DropDownTableViewUtils()
        dropDownTable.setRequiredDataForDropDownTable(selectedButton: menuDetailsView.menuTarianTypeDropDownListButton, dataSourse: ["Veg",  "Non Veg",  "Egg"])
        dropDownTable.modalPresentationStyle = .overCurrentContext
        present(dropDownTable, animated: false, completion: nil)
        dropDownTable.complition = {
            if(self.menuDetailsView.menuPriceTextField.text!.isEmpty){
                self.menuDetailsView.menuPriceTextField.becomeFirstResponder()
            }
        }
    }
    
    
    @objc func didTapAddMenuImage(){
        let imagePickerViewController = UIImagePickerController()
        imagePickerViewController.allowsEditing = true
        imagePickerViewController.delegate = self
        imagePickerViewController.sourceType = .photoLibrary
        present(imagePickerViewController, animated: true, completion: nil)
    }
    
    
   
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    
   
    
       
    @objc func keyboardWillShow(notification: NSNotification) {
        keyboardUtils.keyboardWillShow(notification: notification, maxYofLastObjectInView: menuDetailsView.saveButton.frame.maxY, activeArea: activeArea)
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardUtils.keyboardWillHide()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeArea = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeArea = nil
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        activeArea = textView
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        activeArea = nil
        return true
    }
    
    
    
    func addElementsToStackView(){
        menuDetailsView.stackViewForMenuDetails.addArrangedSubview(menuDetailsView.menuNameTextField)
        menuDetailsView.stackViewForMenuDetails.addArrangedSubview(menuDetailsView.menuDescriptionTextView)
        menuDetailsView.stackViewForMenuDetails.addArrangedSubview(menuDetailsView.menuTarianTypeDropDownListButton)
        menuDetailsView.stackViewForMenuDetails.addArrangedSubview(menuDetailsView.menuCategoryTypeLabel)
        menuDetailsView.stackViewForMenuDetails.addArrangedSubview(menuDetailsView.menuPriceTextField)
        menuDetailsView.stackViewForMenuDetails.addArrangedSubview(menuDetailsView.menuImageSectionView)
    }
    
   
   
    
    func initialiseView(){
        title = "Menu Details"
        view.backgroundColor = .white
        
        view.addSubview(scrollView)
         menuDetailsView = RestaurantAddMenuDetailsView()
         menuDetailsView.translatesAutoresizingMaskIntoConstraints = false
         menuDetailsView.delegate = self
        scrollView.addSubview(menuDetailsView)
         
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
   
    
    
     private func initialiseViewElements(){
         
       
         menuDetailsView.menuCategoryTypeLabel.text = " \(menuCategoryName)"
         menuDetailsView.menuCategoryTypeLabel.tag = menuCategoryId
        
        navigationItem.leftBarButtonItem = backArrowButton
        
         
         
    }
    
    
     private func addDelegatesForElements(){
         menuDetailsView.menuNameTextField.delegate = self
         menuDetailsView.menuPriceTextField.delegate = self
         menuDescriptionPlaceHolderTextViewDelegate = TextViewPlaceHolderUtils(targetTextView: menuDetailsView.menuDescriptionTextView, placeHolderText: "Write the Description about your menu", descriptionLabelText: "Menu Description", fontSizeForLabel: 15, view: menuDetailsView, placeHolderColor: .systemGray3)
         menuDetailsView.menuDescriptionTextView.delegate = menuDescriptionPlaceHolderTextViewDelegate
         
    }
    
     private func addObserver(){
        keyboardUtils = KeyboardUtils(scrollView: scrollView)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
      
    }
     
    
}

// add menu image
extension RestaurantAddMenuDetailsViewController : UIImagePickerControllerDelegate & UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage{
            menuDetailsView.menuImageView.image = image
        }
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension RestaurantAddMenuDetailsViewController{
    
    
         func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                         scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                         scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                         scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
     func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [menuDetailsView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               menuDetailsView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               menuDetailsView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               menuDetailsView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               menuDetailsView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if(menuDetailsView.saveButton.frame.maxY > 0){
            menuDetailsView.removeHeightAnchorConstraints()
            menuDetailsView.heightAnchor.constraint(equalToConstant:menuDetailsView.saveButton.frame.maxY + 100).isActive = true
        }
        else{
            menuDetailsView.removeHeightAnchorConstraints()
            menuDetailsView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
        menuDescriptionPlaceHolderTextViewDelegate.addLabelToTopBorder(frame:  menuDetailsView.stackViewForMenuDetails.convert(menuDetailsView.menuDescriptionTextView.frame, to: self.menuDetailsView))
    }
}


