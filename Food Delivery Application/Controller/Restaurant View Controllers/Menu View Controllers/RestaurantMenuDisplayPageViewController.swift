//
//  RestaurantDisplayMenuDetailsPageViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 20/02/22.
//

import Foundation

import UIKit

class RestaurantMenuDisplayPageViewController : UIViewController, UINavigationBarDelegate, UITextFieldDelegate,DisplayMenuDetailsInRestaurantViewDelegate{
     private var restaurantDisplayMenuDetailsPageViewModel : RestaurantDisplayMenuDetailsPageViewModel = RestaurantDisplayMenuDetailsPageViewModel()
     private var restaurantId = RestaurantIDUtils.shared.getRestaurantId()
     private var menuDetailsOfRestaurant : [Int : MenuContentDetails] = [:]
     private var orderedMenuKeys : [Int] = []
     private var orderedCategoryKeys : [Int] = []
     private var menuSplitCategoryWiseKeys : [Int : [Int]] = [:]
     private var menuTableViewHeightConstraint : NSLayoutConstraint?
     private var menuCategoryTypes :  [(categoryId : Int , categoryName : String)] = []
     private var categoryButtons : [Int : UIButton] = [:]
     private var categoryHeaderViews : [Int : UIView] = [:]
     private  var addMenuButtons : [Int: UIButton] = [:]
     private var removeCategoryButtons : [Int : UIButton] = [:]
     private var hideCategories : [Int] = []
     private var numberOfMenusDisplayed : Int = 0
    lazy var searchFoodIconButton : UIButton = {
        let searchFoodIconButton = UIButton(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
        searchFoodIconButton.setImage(UIImage(systemName: "magnifyingglass")?.withTintColor(.black , renderingMode: .alwaysOriginal), for: .normal)
        searchFoodIconButton.backgroundColor = .white
        searchFoodIconButton.contentVerticalAlignment = .fill
        searchFoodIconButton.contentHorizontalAlignment = .fill
        searchFoodIconButton.addTarget(self, action: #selector(didTapSearchFoodButton), for: .touchUpInside)
        return searchFoodIconButton
    }()
     var menuDisplayPageView : DisplayMenuDetailsInRestaurantViewProtocol!
    
    let scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
        scrollView.backgroundColor = .clear
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
     
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addDelegatesForElements()
        
    }
    
   
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        refreshMenuConentsInTable()
       
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == menuDisplayPageView.addMenuCategoryTextField){
            didTapSaveMenuCategoryButton()
            menuDisplayPageView.addMenuCategoryTextField.text = ""
        }
        return true
    }
    
     func createMenuElementsInCategoryWise(){
        for i in 0..<menuCategoryTypes.count{
            orderedCategoryKeys.append(menuCategoryTypes[i].categoryId)
            splitCategoryWiseSplitMenus(menuCategory: menuCategoryTypes[i])
        }
    }
    
    func updateMenuTableViewHeight(){
        menuTableViewHeightConstraint?.isActive = false
        menuTableViewHeightConstraint = menuDisplayPageView.menuTableView.heightAnchor.constraint(equalToConstant: getTableViewHeight())
        menuTableViewHeightConstraint?.isActive = true
    }
    
    func getTableViewHeight() -> CGFloat{
        let height = CGFloat((menuSplitCategoryWiseKeys.count * 180) + (Int(menuDisplayPageView.getMenuTabelViewCellHeight()) * numberOfMenusDisplayed))
        print(height)
        return height
    }
    
     func splitCategoryWiseSplitMenus(menuCategory : (categoryId : Int , categoryName : String)) {
        var categoryWiseSplitMenusKeys : [Int] = []
        for menuKeys in orderedMenuKeys {
            let menu = menuDetailsOfRestaurant[menuKeys]!
            if menu.menuDetails.menuCategoryId == menuCategory.categoryId{
                    categoryWiseSplitMenusKeys.append(menu.menuId)
            }
        }
        menuSplitCategoryWiseKeys[menuCategory.categoryId] =  categoryWiseSplitMenusKeys
    }
    
    func refreshMenuConentsInTable(){
        DispatchQueue.global().async { [self] in
            getMenuDetails()
            menuSplitCategoryWiseKeys = [:]
            createMenuElementsInCategoryWise()
            DispatchQueue.main.async { [self] in
                if(orderedCategoryKeys.count > 0){
                    menuDisplayPageView.setUpCategoryAndMenusLabel.isHidden  = true
                    menuDisplayPageView.menuTableView.isHidden = false
                    
                }
                else{
                    menuDisplayPageView.setUpCategoryAndMenusLabel.isHidden = false
                    menuDisplayPageView.menuTableView.isHidden = true
                }
                menuDisplayPageView.menuTableView.reloadData()
                updateMenuTableViewHeight()
                menuDisplayPageView.menuTableView.layoutIfNeeded()
                viewDidLayoutSubviews()
            }
        }
    }
    
    @objc func didTapMenuCategoryButton(sender : UIButton){
        showOrHideTableViewBasedOnMenuCategoryHiddenStatus(menuCategoryButton : sender)
    }
    
     func showOrHideTableViewBasedOnMenuCategoryHiddenStatus(menuCategoryButton : UIButton){

         if(hideCategories.contains(menuCategoryButton.tag)){
             showRowsOfCategory(menuCategoryId: menuCategoryButton.tag)
                }
                 else{
                     hideRowsOfCategory(menuCategoryId: menuCategoryButton.tag)
                 }
     }

     
     func hideRowsOfCategory(menuCategoryId : Int ){
         categoryButtons[menuCategoryId]?.addRightImageToButton(systemName: "chevron.down", imageColor: .lightGray, padding: 20)
         categoryHeaderViews[menuCategoryId]?.frame.size.height = 60
         addMenuButtons[menuCategoryId]?.isHidden = true
         removeCategoryButtons[menuCategoryId]?.isHidden = true
         
         hideCategories.append(menuCategoryId)
         numberOfMenusDisplayed -= menuSplitCategoryWiseKeys[menuCategoryId]?.count ?? 0
         updateMenuTableViewHeight()
         menuDisplayPageView.menuTableView.reloadData()
    }

     func showRowsOfCategory(menuCategoryId : Int ){
         categoryButtons[menuCategoryId]?.addRightImageToButton(systemName: "chevron.up", imageColor: .lightGray, padding: 20)
         categoryHeaderViews[menuCategoryId]?.frame.size.height = 120
         addMenuButtons[menuCategoryId]?.isHidden = false
         removeCategoryButtons[menuCategoryId]?.isHidden = false
         hideCategories.remove(at: hideCategories.firstIndex(of: menuCategoryId)!)
         numberOfMenusDisplayed += menuSplitCategoryWiseKeys[menuCategoryId]?.count ?? 0
         updateMenuTableViewHeight()
         menuDisplayPageView.menuTableView.reloadData()
    }
    
     func didTapEditCategoryButton(sender : UIButton){
        let alertController = UIAlertController(title: "Edit", message: "Edit the name of the Category, The Category Name cannot be empty", preferredStyle: .alert)

        alertController.addTextField { [self] (textField) in
            textField.placeholder = "Enter Category Name"
            textField.text = menuCategoryTypes[orderedCategoryKeys.firstIndex(of: sender.tag)!].categoryName
            textField.becomeFirstResponder()
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let saveAction = UIAlertAction(title: "Save", style: .default) { _ in
            let CategoryName = alertController.textFields![0].text
            if(CategoryName!.count > 0){
                if(self.restaurantDisplayMenuDetailsPageViewModel.updateCategory(categoryId: sender.tag, categoryName: CategoryName!)){
                    self.categoryButtons[sender.tag]?.setTitle(CategoryName!, for: .normal)
                }
                
            }

        }

        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
     func didTapAddMenuButton(sender : UIButton){
        let addMenuVC = RestaurantAddMenuDetailsViewController()
        addMenuVC.setCategoryName(categoryName: menuCategoryTypes[orderedCategoryKeys.firstIndex(of: sender.tag)!].categoryName, categoryId: sender.tag)
        navigationController?.pushViewController(addMenuVC, animated: true)
        
    }
    
    @objc func  didTapRemoveCategoryButton(sender: UIButton){
        let alert = UIAlertController(title: "Remove Category", message: "Are you sure you want to permanently remove this Category and its menus ?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel,handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default,handler: {_ in
            DispatchQueue.global().async {
                if(self.restaurantDisplayMenuDetailsPageViewModel.removeCategory(categoryId: sender.tag)){
                    if(self.restaurantDisplayMenuDetailsPageViewModel.removeMenuOfCategory(categoryId: sender.tag)){
                        DispatchQueue.main.async {
                            self.refreshMenuConentsInTable()
                        }
                    }
                }
            }
            
            
        }))
        present(alert,animated: true)
    }
    
    @objc func didTapEditMenu(sender : UIButton){
        let editMenuVC = RestaurantEditMenuDetailsViewController()
        var categoryId = 0
        for categoryKey in menuSplitCategoryWiseKeys.keys{
            if((menuSplitCategoryWiseKeys[categoryKey]?.contains(sender.tag)) != nil){
                categoryId = categoryKey
                break
            }
        }
        editMenuVC.setMenuDetails(menuDetails:  menuDetailsOfRestaurant[sender.tag]!, menuCategoryName: menuCategoryTypes[orderedCategoryKeys.firstIndex(of: categoryId)!].categoryName)
        navigationController?.pushViewController(editMenuVC, animated: true)
    }
    
    @objc func didTapRemoveMenu(sender : UIButton){
        let alert = UIAlertController(title: "Remove Menu", message: "Are you sure you want to permanently remove this menu ?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel,handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default,handler: {_ in
            let menuId = sender.tag
            DispatchQueue.global().async {
                if(self.restaurantDisplayMenuDetailsPageViewModel.removeMenu(menuId: menuId )){
                    DispatchQueue.main.async {
                        self.refreshMenuConentsInTable()
                    }
                }
            }
            
        }))
        present(alert,animated: true)
    }
    
    
    
    @objc func didTapSearchFoodButton(){
        let searchVC = SearchMenuInDisplayedRestaurantMenuDetailsViewController()
        searchVC.setMenuDetailsOfRestaurant(menuDetailsOfRestaurant: menuDetailsOfRestaurant, orderedMenuDetailsDisplayedKeys: orderedMenuKeys)
        navigationController?.pushViewController(searchVC, animated: true)
        
    }
    
    @objc func didTapAddMenuCategoryButton(){
        if menuDisplayPageView.addMenuCategoryTextField.isHidden{
            menuDisplayPageView.showAddMenuCategoryViewItems()
        }
        else{
            menuDisplayPageView.hideAddMenuCategoryViewItems()
        }
    }
    
    func didTapSaveMenuCategoryButton(){
        let addMenuCategoryTextFieldText = menuDisplayPageView.addMenuCategoryTextField.text!
        if(menuDisplayPageView.addMenuCategoryTextField.text! != "" ){
            if(!restaurantDisplayMenuDetailsPageViewModel.checkIfCategoryExistsInTheList(categoryName: addMenuCategoryTextFieldText)){
                DispatchQueue.global().async { [self] in
                    if(restaurantDisplayMenuDetailsPageViewModel.persistMenuCategoryType(restaurantId: restaurantId, categoryType: addMenuCategoryTextFieldText)){
                        refreshMenuConentsInTable()
                        DispatchQueue.main.async { [self] in
                            menuDisplayPageView.hideAddMenuCategoryViewItems()
                            menuDisplayPageView.addMenuCategoryTextField.resignFirstResponder()
                        }
                    }
                }
                
            }
            else{
                showAlert(message: "Menu Category already exists")
            }
        }
        else{
            showAlert(message: "Please enter the category name")
        }
    }
    
    
     func getMenuDetails(){
        let menusInRestaurant = restaurantDisplayMenuDetailsPageViewModel.getMenuContents(restaurantId: restaurantId)
        menuDetailsOfRestaurant = menusInRestaurant.menuContentsInDictionaryFormat
        orderedMenuKeys = menusInRestaurant.orderedKeys
        menuCategoryTypes = restaurantDisplayMenuDetailsPageViewModel.getMenuCategoryTypes(restaurantId: restaurantId)
        orderedCategoryKeys = []
        numberOfMenusDisplayed = orderedMenuKeys.count
    }
    
    func didTapView(){
        view.endEditing(true)
    }
    
     private func initialiseView(){
         title = "Menu"
         view.backgroundColor = .white
         view.addSubview(scrollView)
         menuDisplayPageView = DisplayMenuDetailsInRestaurantView()
         menuDisplayPageView.translatesAutoresizingMaskIntoConstraints = false
         menuDisplayPageView.delegate = self
         scrollView.addSubview(menuDisplayPageView)
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
    
    
     private func initialiseViewElements(){
         
         setDefaultNavigationBarApprearance()
         setScrollEdgeApprearance()
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: searchFoodIconButton)
        tabBarController?.tabBar.barTintColor = .white
        
    }
    
     
    
     private func addDelegatesForElements(){
         menuDisplayPageView.addMenuCategoryTextField.delegate = self
         menuDisplayPageView.menuTableView.delegate = self
         menuDisplayPageView.menuTableView.dataSource = self
    }
    
     
    
}

extension RestaurantMenuDisplayPageViewController : UITableViewDelegate , UITableViewDataSource,RestaurantMenuTableViewCellDelegate,MenuCategoryHeaderViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        print(menuSplitCategoryWiseKeys.count ,menuDetailsOfRestaurant.count, "Helll")
        return menuSplitCategoryWiseKeys.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(hideCategories.contains(orderedCategoryKeys[section])){
            
            return categoryHeaderViews[orderedCategoryKeys[section]]
        }
        else if(categoryHeaderViews[orderedCategoryKeys[section]] != nil){
            return categoryHeaderViews[orderedCategoryKeys[section]]
        }
        else{
            
            let menuCategoryView = menuDisplayPageView.getMenuCategoryView()
            menuCategoryView.tag = menuCategoryTypes[section].categoryId
            menuCategoryView.delegate = self
            menuCategoryView.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 120)
            menuCategoryView.editCategoryButton.tag = menuCategoryView.tag
            menuCategoryView.menuCategoryButton.tag = menuCategoryView.tag
            menuCategoryView.menuCategoryButton.setTitle(menuCategoryTypes[section].categoryName, for: .normal)

            categoryButtons[menuCategoryTypes[section].categoryId] = menuCategoryView.menuCategoryButton
            menuCategoryView.addMenuButton.tag = menuCategoryTypes[section].categoryId
       
            addMenuButtons[menuCategoryTypes[section].categoryId] = menuCategoryView.addMenuButton
            menuCategoryView.removeCategoryButton.tag = menuCategoryTypes[section].categoryId
            removeCategoryButtons[menuCategoryTypes[section].categoryId] = menuCategoryView.removeCategoryButton
                categoryHeaderViews[menuCategoryTypes[section].categoryId] = menuCategoryView
            
            return menuCategoryView
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(hideCategories.contains(orderedCategoryKeys[section])){
            return 0
        }
        else{
             return menuSplitCategoryWiseKeys[orderedCategoryKeys[section]]!.count
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  menuSplitCategoryWiseKeys[orderedCategoryKeys[indexPath.section]]!.count > 0 {
            let menuContentDetails = menuDetailsOfRestaurant[menuSplitCategoryWiseKeys[orderedCategoryKeys[indexPath.section]]![indexPath.row]]!
            let menuDetails = menuContentDetails.menuDetails
            
            let cell = menuDisplayPageView.createMenuTableViewCell(indexPath: indexPath, tableView: tableView, menuCellContents: (menuImage: menuDetails.menuImage, menuName: menuDetails.menuName, menuDescription: menuDetails.menuDescription, menuPrice: menuDetails.menuPrice, menuTarianType: menuDetails.menuTarianType, menuAvailableNextAt: menuContentDetails.menuNextAvailableAt, menuStatus: menuContentDetails.menuStatus, menuAvailable: menuContentDetails.menuIsAvailable,menuId : menuContentDetails.menuId))
            cell.delegate = self
                        return cell
                    }
            return UITableViewCell()
        }
            
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(hideCategories.contains(orderedCategoryKeys[section])){
            return 60
        }
        return 120
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        menuDisplayPageView.getMenuTabelViewCellHeight()
    }
    
    
    
    
    
}
extension RestaurantMenuDisplayPageViewController{
    
    
    
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                         scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                         scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                         scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [menuDisplayPageView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               menuDisplayPageView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               menuDisplayPageView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               menuDisplayPageView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               menuDisplayPageView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateMenuTableViewHeight()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
        if(menuDisplayPageView.menuTableView.frame.maxY > 0){
            menuDisplayPageView.removeHeightAnchorConstraints()
            menuDisplayPageView.heightAnchor.constraint(equalToConstant:  menuDisplayPageView.menuTableView.frame.maxY + 100).isActive = true
        }
        else{
            menuDisplayPageView.removeHeightAnchorConstraints()
            menuDisplayPageView.heightAnchor.constraint(equalToConstant:  view.frame.maxY + 100).isActive = true
        }
        print(menuDisplayPageView.frame)
    }
}


