//
//  SearchMenuInDisplayedRestaurantMenuDetailsViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 18/02/22.
//

import Foundation
import UIKit

class SearchMenuInDisplayedRestaurantMenuDetailsViewController : UIViewController,  UISearchBarDelegate,SearchMenuInDisplayedRestaurantMenuDetailsViewDelegate{
     private var searchMenuInDisplayedMenuDetailsView : SearchMenusInDisplayedRestaurantMenuDetailsViewProtocol!
    private var restaurantDisplayMenuDetailsPageViewModel : RestaurantDisplayMenuDetailsPageViewModel = RestaurantDisplayMenuDetailsPageViewModel()
     private var menuDetailsOfRestaurant : [Int : MenuContentDetails] = [:]
     private var orderedMenuKeys : [Int] = []
     private var orderedSearchMenusKeys : [Int] = []
     private var searchResultMenus : [Int : MenuContentDetails] = [:]
    
    private lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
    
    private var searchMenuBar : UISearchBar = {
        let  searchMenuBar = UISearchBar()
        searchMenuBar.placeholder = "Enter dish name"
        searchMenuBar.backgroundColor = .white
        searchMenuBar.keyboardType = .default
        searchMenuBar.returnKeyType = .search
        return searchMenuBar
    }()
     
    override func viewDidLoad() {
        initialiseView()
        initialiseViewElements()
        addDelegatesForElements()
      
    }
    

    func didTapView(){
        searchMenuBar.resignFirstResponder()
    }
     private func initialiseView(){
        view.backgroundColor = .white
         searchMenuInDisplayedMenuDetailsView = SearchMenuInDisplayedRestaurantMenuDetailsView()
         searchMenuInDisplayedMenuDetailsView.delegate = self
         searchMenuInDisplayedMenuDetailsView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(searchMenuInDisplayedMenuDetailsView)
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
     private func initialiseViewElements(){
        searchMenuBar.becomeFirstResponder()
        navigationItem.titleView = searchMenuBar
        navigationItem.leftBarButtonItem =  backArrowButton 
    }
    
     private func addDelegatesForElements(){
        searchMenuBar.delegate = self
         searchMenuInDisplayedMenuDetailsView.menuTableView.delegate = self
         searchMenuInDisplayedMenuDetailsView.menuTableView.dataSource = self
    }
    
     
    
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        getMenusWithCharactersLikeInSearchBar(menuName : searchText)
        searchMenuInDisplayedMenuDetailsView.menuTableView.reloadData()
    }
    

    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchMenuBar.resignFirstResponder()
    }
    
     func  getMenusWithCharactersLikeInSearchBar(menuName : String){
        searchResultMenus = [:]
        orderedSearchMenusKeys = []
        for menukey in orderedMenuKeys{
            
            let menu = menuDetailsOfRestaurant[menukey]!
            if menu.menuDetails.menuName.contains(menuName){
                
                searchResultMenus[menukey] = menu
                orderedSearchMenusKeys.append(menukey)
               
            }
        }
    }
     
    
    
    func setMenuDetailsOfRestaurant(menuDetailsOfRestaurant : [Int : MenuContentDetails],orderedMenuDetailsDisplayedKeys : [Int]){
        self.menuDetailsOfRestaurant = menuDetailsOfRestaurant
        self.orderedMenuKeys = orderedMenuDetailsDisplayedKeys
    }

    
    func didTapEditMenu(sender : UIButton){
        navigationController?.pushViewController(RestaurantEditMenuDetailsViewController(), animated: true)
    }
    
    func didTapRemoveMenu(sender : UIButton){
        let alert = UIAlertController(title: "Remove Menu", message: "Are you sure you want to permanently remove this menu ?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel,handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default,handler: { [self]_ in
            let menuId = sender.tag
            DispatchQueue.global().async {
                if(self.restaurantDisplayMenuDetailsPageViewModel.removeMenu(menuId: menuId)){
                    DispatchQueue.main.async { [self] in
                        menuDetailsOfRestaurant.removeValue(forKey: menuId)
                        orderedMenuKeys.remove(at: orderedMenuKeys.firstIndex(of: menuId)!)
                        orderedSearchMenusKeys.remove(at: orderedSearchMenusKeys.firstIndex(of: menuId)!)
                        searchResultMenus.removeValue(forKey: menuId)
                        searchMenuInDisplayedMenuDetailsView.menuTableView.reloadData()
                    }
                }
            }
            
        }))
        present(alert,animated: true)
    }
    
    
}
extension SearchMenuInDisplayedRestaurantMenuDetailsViewController : UITableViewDelegate , UITableViewDataSource,RestaurantMenuTableViewCellDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return searchResultMenus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menuContentDetails =   searchResultMenus[orderedSearchMenusKeys[ indexPath.row]]!
        let menuDetails = menuContentDetails.menuDetails
       
        let cell = searchMenuInDisplayedMenuDetailsView.createMenuTableViewCell(indexPath: indexPath, tableView: tableView, menuCellContents: (menuImage: menuDetails.menuImage, menuName: menuDetails.menuName, menuDescription: menuDetails.menuDescription, menuPrice: menuDetails.menuPrice, menuTarianType: menuDetails.menuTarianType, menuAvailableNextAt: menuContentDetails.menuNextAvailableAt, menuStatus: menuContentDetails.menuStatus, menuAvailable: menuContentDetails.menuIsAvailable, menuId : menuContentDetails.menuId))
        cell.delegate = self
        return cell
        }
                
       

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        searchMenuInDisplayedMenuDetailsView.getMenuTabelViewCellHeight()
    }
    
    
    
    
    
    
}


extension SearchMenuInDisplayedRestaurantMenuDetailsViewController{
   
        
        
        func getContentViewConstraints() -> [NSLayoutConstraint]{
            let contentViewConstraints = [searchMenuInDisplayedMenuDetailsView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor),
                                          searchMenuInDisplayedMenuDetailsView.bottomAnchor.constraint(equalTo : view.safeAreaLayoutGuide.bottomAnchor),
                                          searchMenuInDisplayedMenuDetailsView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                                          searchMenuInDisplayedMenuDetailsView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)]
                return contentViewConstraints
        }
    
}
