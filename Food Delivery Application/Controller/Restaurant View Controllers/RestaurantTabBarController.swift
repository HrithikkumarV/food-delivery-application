//
//  RestaurantTabviewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 12/11/21.
//

import Foundation
import UIKit
class RestaurantTabBarController : UITabBarController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseTabBar()
        addViewControllersToTabBar()
        addSymbolsToViewControllerTabs()
        setDefaultNavigationBarApprearance()
        setScrollEdgeApprearance()
    }
     func addViewControllersToTabBar(){
        let menuTab = UINavigationController(rootViewController: RestaurantMenuDisplayPageViewController())
        let ordersTab = UINavigationController(rootViewController:OrdersTabViewController())
        let accountsTab = UINavigationController(rootViewController: RestaurantAccountsTabViewController())
        menuTab.title = "Menu"
        ordersTab.title = "Orders"
        accountsTab.title = "Accounts"
        self.setViewControllers([menuTab,ordersTab,accountsTab], animated: false)
        self.tabBar.backgroundColor = .white
    }
     func addSymbolsToViewControllerTabs(){
        guard let tabs = self.tabBar.items else{
            return
        }
        let images = ["fork.knife.circle" ,"note.text", "person"]
        
        for i in 0..<tabs.count {
            tabs[i].image = UIImage(systemName: images[i])
        }
    }
    
     func initialiseTabBar(){
        self.view.backgroundColor = .white
        self.tabBar.tintColor = .black.withAlphaComponent(0.9)
        self.tabBar.unselectedItemTintColor = .systemGray
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true

       }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
}
