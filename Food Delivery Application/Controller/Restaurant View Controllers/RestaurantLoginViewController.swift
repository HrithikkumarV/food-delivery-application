//
//  RestaurantLoginPage.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 11/11/21.
//

import Foundation
import UIKit

class RestaurantLoginViewController : UIViewController, UITextFieldDelegate,RestaurantLoginViewDelegate{
     private var restaurantLoginView : RestaurantLoginViewProtocol!
     let restaurantLoginViewModel : RestaurantLoginViewModel = RestaurantLoginViewModel()
     
    
    let scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
     private var activeArea : UIView? = nil
     private var keyboardUtils : KeyboardUtils!
    
    private lazy var dismissButton : UIBarButtonItem = {
       let dismissBarButton = UIBarButtonItem()
       dismissBarButton.title = "Dismiss"
       dismissBarButton.style = .done
        dismissBarButton.target = self
        dismissBarButton.action = #selector(didTapDismiss)
       return dismissBarButton
   }()
     
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addDelegatesForElements()
        addObserver()
    }
   
    
   
    
   
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if( textField == restaurantLoginView.phoneNumberTextField){
            let maxLength = 10
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            var res = ""
            if restaurantLoginView.phoneNumberTextField.text != "" || string != "" {
                    res = (textField.text ?? "") + string
                }
            return newString.length <= maxLength && Double(res) != nil
        }
        return true
    }
    
     func didTapLogin(){
         if(!restaurantLoginView.passwordTextField.text!.isEmpty && !restaurantLoginView.phoneNumberTextField.text!.isEmpty){
            if(restaurantLoginView.phoneNumberTextField.text?.isValidPhoneNumber() ==  true){
                let restaurantPhoneNumberText = restaurantLoginView.phoneNumberTextField.text!
                let passwordText = restaurantLoginView.passwordTextField.text!
                DispatchQueue.global().async { [self] in
                    let restaurentPassword = restaurantLoginViewModel.getRestaurantLoginPassword(phoneNumber: restaurantPhoneNumberText)
                    let restaurantId = restaurantLoginViewModel.getRestaurantId(phoneNumber: restaurantPhoneNumberText)
                    if(restaurantId != 0){
                        if(passwordText == restaurentPassword.decryptString()){
                            DispatchQueue.main.async {
                                self.loginVerficationSucceeded(restaurantId: restaurantId)
                            }
                          
                        }
                        else{
                            DispatchQueue.main.async {
                                self.showAlert(message: "Incorrect password")
                            }
                            
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            self.showAlert(message: "Phone number does not exist")
                        }
                    }
                }
                
                
            }
            else{
                showAlert(message: "please enter valid phone number")
            }
        }
        else{
            if(restaurantLoginView.passwordTextField.text!.isEmpty && restaurantLoginView.phoneNumberTextField.text!.isEmpty){
                showAlert(message: "Please enter  Phone number and password")
            }
            else if(restaurantLoginView.phoneNumberTextField.text!.isEmpty){
                showAlert(message: "Please enter Phone number")
            }
            else{
                showAlert(message: "Please enter Password")
            }
        }
    }
    
     func didTapCreateAccount(){
        let restaurantCreateAccountPhoneNumberFormViewController = RestaurantCreateAccountPhoneNumberFormViewController()
        restaurantCreateAccountPhoneNumberFormViewController.setRestaurantCreateAccountViewModel(restaurantCreateAccountViewModel : RestaurantCreateAccountViewModel())
        self.navigationController?.pushViewController(restaurantCreateAccountPhoneNumberFormViewController, animated: true)
    }
    
     func didTapChangePassword(){
        self.navigationController?.pushViewController(RestaurantChangePasswordPhoneNumberFormViewController(), animated: true)
    }
    
   
    
     func didTapView(){
        view.endEditing(true)
    }
    
     func didTapPhoneNumberkeyboardNextBarButton(){
         restaurantLoginView.passwordTextField.becomeFirstResponder()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == restaurantLoginView.phoneNumberTextField{
            restaurantLoginView.passwordTextField.becomeFirstResponder()
        }
        else{
            restaurantLoginView.passwordTextField.resignFirstResponder()
            didTapLogin()
        }
        return true
    }
    
    
     func loginVerficationSucceeded(restaurantId : Int){
         DispatchQueue.global().async {
             if(RestaurantIDUtils.shared.setRestaurantId(restaurantId: restaurantId)){
                 DispatchQueue.main.async {
                     self.dismissToRootViewControllerAndPresentRestaurantTabBarPage()
                 }
             }
         }
        
    }
    
     func dismissToRootViewControllerAndPresentRestaurantTabBarPage(){
        hideSubviewsOfRootViewController()
        self.view.window?.rootViewController?.dismiss(animated: true, completion: {
            let restaurantTabBar = UINavigationController(rootViewController:  RestaurantTabBarController())
            restaurantTabBar.modalPresentationStyle = .fullScreen
            UIApplication.shared.windows.first?.rootViewController?.present(restaurantTabBar,animated: true)
        })
        showSubviewsOfRootViewControllerWithADelay()
    }
     func showSubviewsOfRootViewControllerWithADelay(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIApplication.shared.windows.first?.rootViewController?.view.subviews.forEach { $0.isHidden = false }
        }
    }
    
     func hideSubviewsOfRootViewController(){
        UIApplication.shared.windows.first?.rootViewController?.view.subviews.forEach { $0.isHidden = true }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        //adjust layout
        keyboardUtils.keyboardWillShow(notification: notification, maxYofLastObjectInView: restaurantLoginView.createAccountButton.frame.maxY, activeArea: activeArea)
       
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardUtils.keyboardWillHide()
    }
    
    @objc func didTapDismiss(){
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeArea = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeArea = nil
    }
    

    
     private func initialiseView(){
        title = "Login"
        view.backgroundColor = .white
        view.addSubview(scrollView)
        restaurantLoginView = RestaurantLoginView()
        restaurantLoginView.translatesAutoresizingMaskIntoConstraints = false
        restaurantLoginView.delegate = self
        scrollView.addSubview(restaurantLoginView)
        NSLayoutConstraint.activate(getScrollViewConstraints())
        NSLayoutConstraint.activate(getContentViewConstraints())
        
    }
    
     private func initialiseViewElements(){
       
    
        navigationItem.rightBarButtonItem = dismissButton
        
    }
    
   
    
    
    
     private func addDelegatesForElements(){
         restaurantLoginView.phoneNumberTextField.delegate = self
         restaurantLoginView.passwordTextField.delegate = self
    }
    
     private func addObserver(){
        keyboardUtils = KeyboardUtils(scrollView: scrollView)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
    }
    

}

extension RestaurantLoginViewController{
    
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                         scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                         scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                         scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [restaurantLoginView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               restaurantLoginView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               restaurantLoginView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               restaurantLoginView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               restaurantLoginView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }
    
   
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
        if(restaurantLoginView.createAccountButton.frame.maxY > 0){
            restaurantLoginView.removeHeightAnchorConstraints()
            restaurantLoginView.heightAnchor.constraint(equalToConstant:  restaurantLoginView.createAccountButton.frame.maxY + 100).isActive = true
        }
        else{
            restaurantLoginView.removeHeightAnchorConstraints()
            restaurantLoginView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
    }
}


