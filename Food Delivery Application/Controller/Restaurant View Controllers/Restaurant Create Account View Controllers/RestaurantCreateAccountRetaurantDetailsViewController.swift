//
//  RestaurantCreateAccountRetaurentDetailsViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 08/12/21.
//

import Foundation
import UIKit

class RestaurantCreateAccountRestaurantDetailsViewController : UIViewController, UITextFieldDelegate , UITextViewDelegate,RestaurantCreateAccountRestaurantDetailsViewDelegate{
    
     private var restaurantCreateAccountRestaurantDetailsView : RestaurantCreateAccountRestaurantDetailsViewProtocol!
     private var restaurantCreateAccountViewModel : RestaurantCreateAccountViewModel!
     private var restaurantDescriptionPlaceHolderTextViewDelegate : TextViewPlaceHolderUtils!
    
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
    
     private var restaurantDetailsContainer : RestaurantDetails!
     
     private var activeArea : UIView? = nil
     private var keyboardUtils : KeyboardUtils!
    
    
    let scrollView : UIScrollView = {
        let  scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
       scrollView.translatesAutoresizingMaskIntoConstraints = false
       scrollView.backgroundColor = .white
        return scrollView
    }()
     
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addDelegatesForElements()
        addObserver()
    }
    
    
    
    
    
     func didTapView(){
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
       
        case restaurantCreateAccountRestaurantDetailsView.restaurantNameTextField :
            didTapRestaurantCuisine()
            restaurantCreateAccountRestaurantDetailsView.restaurantDescriptionTextView.becomeFirstResponder()
       
        default:
            return true
        }
        return true
    }
    
     func didTapContinueToSetAddress(){
         if(!restaurantCreateAccountRestaurantDetailsView.restaurantNameTextField.text!.isEmpty && restaurantCreateAccountRestaurantDetailsView.restaurantCuisineButton.titleLabel?.text != "Restaurant Cuisine" && restaurantCreateAccountRestaurantDetailsView.restaurantDescriptionTextView.text != "Write the Description about your restaurant" &&  restaurantCreateAccountRestaurantDetailsView.restaurantProfileImageView.image != nil){
            let restaurantCreateAccountAddressViewController = RestaurantCreateAccountAddressViewController()
             restaurantCreateAccountViewModel.setRestaurantDetails(restaurentDetails:RestaurantDetails(restaurantProfileImage: restaurantCreateAccountRestaurantDetailsView.restaurantProfileImageView.image!.jpeg(.medium)!, restaurantName: restaurantCreateAccountRestaurantDetailsView.restaurantNameTextField.text!, restaurantDescription: restaurantCreateAccountRestaurantDetailsView.restaurantDescriptionTextView.text!, restaurantCuisine: restaurantCreateAccountRestaurantDetailsView.restaurantCuisineButton.titleLabel!.text!, restaurantContactNumber: ""))
            restaurantCreateAccountAddressViewController.setRestaurantCreateAccountViewModel(restaurantCreateAccountViewModel: restaurantCreateAccountViewModel)
                    navigationController?.pushViewController(restaurantCreateAccountAddressViewController, animated: true)
        }
        else{
            showAlert(message : "Please fill all the fields")
        }
    }
    
    
    
     func didTapRestaurantDescriptionKeyboardNextBarButton(){
        didTapAddRestaurantProfileImage()
         restaurantCreateAccountRestaurantDetailsView.restaurantDescriptionTextView.resignFirstResponder()
    }
    
   
     func didTapRestaurantCuisine(){
        let dropDownTable =  DropDownTableViewUtils()
         dropDownTable.setRequiredDataForDropDownTable(selectedButton: restaurantCreateAccountRestaurantDetailsView.restaurantCuisineButton, dataSourse: RestaurantCuisines.getRestaurantCuisines())
        dropDownTable.modalPresentationStyle = .overCurrentContext
        present(dropDownTable, animated: false, completion: nil)
    }
    
     func didTapAddRestaurantProfileImage(){
       let imagePickerViewController = UIImagePickerController()
        imagePickerViewController.allowsEditing = true
        imagePickerViewController.delegate = self
        imagePickerViewController.sourceType = .photoLibrary
        present(imagePickerViewController, animated: true, completion: nil)
    }
    
    @objc func didTapBackArrow(){
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: RestaurantCreateAccountPhoneNumberFormViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    
    

    override func viewDidAppear(_ animated: Bool) {
        restaurantCreateAccountRestaurantDetailsView.restaurantNameTextField.becomeFirstResponder()
        restaurantDescriptionPlaceHolderTextViewDelegate.addLabelToTopBorder(frame: restaurantCreateAccountRestaurantDetailsView.stackViewForRestaurantDetails.convert(restaurantCreateAccountRestaurantDetailsView.restaurantDescriptionTextView.frame, to: self.restaurantCreateAccountRestaurantDetailsView))
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        keyboardUtils.keyboardWillShow(notification: notification, maxYofLastObjectInView: restaurantCreateAccountRestaurantDetailsView.stackViewForRestaurantDetails.frame.maxY, activeArea: activeArea)
       
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardUtils.keyboardWillHide()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeArea = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeArea = nil
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        activeArea = textView
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        activeArea = nil
        return true
    }
    
    
    
    func setRestaurantCreateAccountViewModel(restaurantCreateAccountViewModel :RestaurantCreateAccountViewModel){
        self.restaurantCreateAccountViewModel = restaurantCreateAccountViewModel
    }
    
     private func initialiseView(){
        title = "Restaurant Details"
        view.backgroundColor = .white
       
        view.addSubview(scrollView)
         restaurantCreateAccountRestaurantDetailsView = RestaurantCreateAccountRestaurantDetailsView()
         restaurantCreateAccountRestaurantDetailsView.translatesAutoresizingMaskIntoConstraints = false
         restaurantCreateAccountRestaurantDetailsView.delegate = self
        scrollView.addSubview(restaurantCreateAccountRestaurantDetailsView)
         
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
   
    
    
     private func initialiseViewElements(){
        navigationItem.leftBarButtonItem = backArrowButton
    }
    
     private func addDelegatesForElements(){
         restaurantCreateAccountRestaurantDetailsView.restaurantNameTextField.delegate = self
         restaurantDescriptionPlaceHolderTextViewDelegate = TextViewPlaceHolderUtils(targetTextView: restaurantCreateAccountRestaurantDetailsView.restaurantDescriptionTextView, placeHolderText: "Write the Description about your restaurant", descriptionLabelText: "Restaurant Description", fontSizeForLabel: 15, view: restaurantCreateAccountRestaurantDetailsView, placeHolderColor: .systemGray3)
        
         restaurantCreateAccountRestaurantDetailsView.restaurantDescriptionTextView.delegate = restaurantDescriptionPlaceHolderTextViewDelegate
    }
    
     private func addObserver(){
        keyboardUtils = KeyboardUtils(scrollView: scrollView)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
    }
}

// add restaurant image
extension RestaurantCreateAccountRestaurantDetailsViewController : UIImagePickerControllerDelegate & UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage{
            restaurantCreateAccountRestaurantDetailsView.restaurantProfileImageView.image = image
        }
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension RestaurantCreateAccountRestaurantDetailsViewController{
    
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                         scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                         scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                         scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [restaurantCreateAccountRestaurantDetailsView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               restaurantCreateAccountRestaurantDetailsView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               restaurantCreateAccountRestaurantDetailsView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               restaurantCreateAccountRestaurantDetailsView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               restaurantCreateAccountRestaurantDetailsView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
        if(restaurantCreateAccountRestaurantDetailsView.continueTosetAddressButton.frame.maxY  > 0){
            restaurantCreateAccountRestaurantDetailsView.removeHeightAnchorConstraints()
            restaurantCreateAccountRestaurantDetailsView.heightAnchor.constraint(equalToConstant:  restaurantCreateAccountRestaurantDetailsView.continueTosetAddressButton.frame.maxY + 100).isActive = true
        }
        else{
            restaurantCreateAccountRestaurantDetailsView.removeHeightAnchorConstraints()
            restaurantCreateAccountRestaurantDetailsView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
    }
    
}


