//
//  RestaurantCreateAccountAuthenticationFormDetailsViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 08/12/21.
//

import Foundation
import UIKit
class RestaurantCreateAccountAuthenticationDetailsViewController : UIViewController, UITextFieldDelegate,RestaurantCreateAccountAuthenticationDetailsViewDelegate{
    
     private var restaurantCreateAccountAuthenticationDetailsView : RestaurantCreateAccountAuthenticationDetailsViewProtocol!
     private var restaurantCreateAccountViewModel : RestaurantCreateAccountViewModel!
     private var activeArea : UITextField? = nil
     private var keyboardUtils : KeyboardUtils!
     let scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
       scrollView.backgroundColor = .white
       scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialiseView()
        initialiseViewElements()
        addTargetsAndActionsForElements()
        addDelegatesForElements()
        addObserver()
    }
    
   
    
    
    
     
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
       
        case restaurantCreateAccountAuthenticationDetailsView.secretCodeTextField :
            restaurantCreateAccountAuthenticationDetailsView.passwordTextField.becomeFirstResponder()
        case restaurantCreateAccountAuthenticationDetailsView.passwordTextField :
            restaurantCreateAccountAuthenticationDetailsView.confirmPasswordTextField.becomeFirstResponder()
        case restaurantCreateAccountAuthenticationDetailsView.confirmPasswordTextField :
            restaurantCreateAccountAuthenticationDetailsView.confirmPasswordTextField.resignFirstResponder()
        default:
            return true
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if( textField == restaurantCreateAccountAuthenticationDetailsView.secretCodeTextField){
            let maxLength = 9
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            var res = ""
            if restaurantCreateAccountAuthenticationDetailsView.secretCodeTextField.text != "" || string != "" {
                    res = (textField.text ?? "") + string
                }
            return newString.length <= maxLength && Double(res) != nil
        }
        return true
    }
    
    
    func didTapView(){
       view.endEditing(true)
   }
    
    func didTapCreateAccountButton(){
        if(!restaurantCreateAccountAuthenticationDetailsView.secretCodeTextField.text!.isEmpty && !restaurantCreateAccountAuthenticationDetailsView.passwordTextField.text!.isEmpty && !restaurantCreateAccountAuthenticationDetailsView.confirmPasswordTextField.text!.isEmpty){
            if(restaurantCreateAccountAuthenticationDetailsView.passwordTextField.text == restaurantCreateAccountAuthenticationDetailsView.confirmPasswordTextField.text){
                if(restaurantCreateAccountAuthenticationDetailsView.passwordTextField.text!.isValidPassword()){
                        showActivityIndicator()
                        restaurantCreateAccountViewModel.getSecretCode(){ [weak self]
                            code in
                            self?.hideActivityIndicator()
                            if(self?.restaurantCreateAccountAuthenticationDetailsView.secretCodeTextField.text == code)
                               {
                                self!.restaurantCreateAccountViewModel.setRestaurantPassword(Password: self!.restaurantCreateAccountAuthenticationDetailsView.passwordTextField.text!.encryptString())
                                DispatchQueue.global().async {
                                    if(self!.restaurantCreateAccountViewModel.restaurantCreateAccount()){
                                        DispatchQueue.main.async {
                                            self?.proceedToEnterAfterSuccessfullCreateAccount()
                                        }
                                    }
                                    else{
                                        self?.showAlert(message: "Error while creating account")
                                    }
                                }
                            }
                            else{
                                self?.showAlert(message: "Invalid secret code , please enter valid secret code")
                            }
                    }
                }
                    else{
                        showAlert(message: "Invalid Password , please enter a password contains at least one digit(0-9) , Password length should be between 8 to 15 characters, Password should contain at least one lowercase letter(a-z), Password should contain at least one uppercase letter(A-Z), Password should contain at least one special character ( @, #, %, &, !, $, etc….).")
                    }
            }
            else{
                showAlert(message: "Password didnot match")
            }
        }
        else{
            showAlert(message: "Please fill the fields")
        }
    }
    
     func proceedToEnterAfterSuccessfullCreateAccount(){
    
         DispatchQueue.global().async {
             if(RestaurantIDUtils.shared.setRestaurantId(restaurantId: self.restaurantCreateAccountViewModel.getRestaurantId())){
                 DispatchQueue.main.async {
                     self.dismissToRootViewControllerAndPresentRestaurantTabBarPage()
                 }
             }
         }
        
    }
    
    
    
     func dismissToRootViewControllerAndPresentRestaurantTabBarPage(){
        hideSubviewsOfRootViewController()
        self.view.window?.rootViewController?.dismiss(animated: true, completion: {
            let restaurantTabBar = UINavigationController(rootViewController:  RestaurantTabBarController())
            restaurantTabBar.modalPresentationStyle = .fullScreen
            UIApplication.shared.windows.first?.rootViewController?.present(restaurantTabBar,animated: true)
        })
        showSubviewsOfRootViewControllerWithADelay()
    }
     func showSubviewsOfRootViewControllerWithADelay(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIApplication.shared.windows.first?.rootViewController?.view.subviews.forEach { $0.isHidden = false }
        }
    }
    
     func hideSubviewsOfRootViewController(){
        UIApplication.shared.windows.first?.rootViewController?.view.subviews.forEach { $0.isHidden = true }
    }
   
    
    func didTapSecretCodeKeyboardNextBarButton(){
        restaurantCreateAccountAuthenticationDetailsView.passwordTextField.becomeFirstResponder()
    }
   
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        restaurantCreateAccountAuthenticationDetailsView.secretCodeTextField.becomeFirstResponder()
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        keyboardUtils.keyboardWillShow(notification: notification, maxYofLastObjectInView: restaurantCreateAccountAuthenticationDetailsView.stackViewForAuthenticationDetails.frame.maxY, activeArea: activeArea)
       
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardUtils.keyboardWillHide()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeArea = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeArea = nil
    }
    
    func setRestaurantCreateAccountViewModel(restaurantCreateAccountViewModel :RestaurantCreateAccountViewModel){
        self.restaurantCreateAccountViewModel = restaurantCreateAccountViewModel
    }
    
     private func initialiseView(){
        title = "Authentication Details"
        view.backgroundColor = .white
        
        view.addSubview(scrollView)
         restaurantCreateAccountAuthenticationDetailsView = RestaurantCreateAccountAuthenticationDetailsView()
         restaurantCreateAccountAuthenticationDetailsView.translatesAutoresizingMaskIntoConstraints = false
         restaurantCreateAccountAuthenticationDetailsView.delegate = self
        scrollView.addSubview(restaurantCreateAccountAuthenticationDetailsView)
         
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
     private func initialiseViewElements(){
        
        
         
        navigationItem.leftBarButtonItem = backArrowButton 
        
        
    }
    
    
     private func addTargetsAndActionsForElements(){
        
        
       
    }
    
     private func addDelegatesForElements(){
         restaurantCreateAccountAuthenticationDetailsView.secretCodeTextField.delegate = self
         restaurantCreateAccountAuthenticationDetailsView.passwordTextField.delegate = self
         restaurantCreateAccountAuthenticationDetailsView.confirmPasswordTextField.delegate = self
    }
    
     private func addObserver(){
        keyboardUtils = KeyboardUtils(scrollView: scrollView)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
    }
}

extension RestaurantCreateAccountAuthenticationDetailsViewController{
    
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                         scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                         scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                         scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [restaurantCreateAccountAuthenticationDetailsView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               restaurantCreateAccountAuthenticationDetailsView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               restaurantCreateAccountAuthenticationDetailsView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               restaurantCreateAccountAuthenticationDetailsView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               restaurantCreateAccountAuthenticationDetailsView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
        if(restaurantCreateAccountAuthenticationDetailsView.stackViewForAuthenticationDetails.frame.maxY > 0){
            restaurantCreateAccountAuthenticationDetailsView.removeHeightAnchorConstraints()
            restaurantCreateAccountAuthenticationDetailsView.heightAnchor.constraint(equalToConstant:  restaurantCreateAccountAuthenticationDetailsView.stackViewForAuthenticationDetails.frame.maxY + 100).isActive = true
        }
        else{
            restaurantCreateAccountAuthenticationDetailsView.removeHeightAnchorConstraints()
            restaurantCreateAccountAuthenticationDetailsView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
    }
}


