//
//  RestaurantCreateAccountAddressViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 08/12/21.
//

import Foundation
import UIKit

class RestaurantCreateAccountAddressViewController : UIViewController , UITextFieldDelegate,AddressViewDelegate{
     private var restaurantCreateAccountViewModel : RestaurantCreateAccountViewModel!
      var localities :[String] = []
      var activeArea: UITextField? = nil
      var keyboardUtils : KeyboardUtils!
      var restaurantCreateAccountAddressView : AddressView!
    
    let scrollView : UIScrollView = {
        let  scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.keyboardDismissMode = .onDrag
        return scrollView
    }()
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
       backArrowButton.action = #selector(didTapBackArrow)
       backArrowButton.target = self
        return backArrowButton
    }()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addDelegatesForElements()
        addObserver()
    }
    
    
     func didTapContinue(){
        
        if(!restaurantCreateAccountAddressView.doorNoAndBuildingNameAndBuildingNoTextField.text!.isEmpty && !restaurantCreateAccountAddressView.streetNameTextfield.text!.isEmpty && !restaurantCreateAccountAddressView.landmarkTextfield.text!.isEmpty && restaurantCreateAccountAddressView.localityButton.titleLabel?.text! != "Locality" && !restaurantCreateAccountAddressView.cityTextfield.text!.isEmpty && !restaurantCreateAccountAddressView.stateTextField.text!.isEmpty && !restaurantCreateAccountAddressView.countryTextfield.text!.isEmpty){
            
            let restaurantCreateAccountAuthenticationDetailsViewController = RestaurantCreateAccountAuthenticationDetailsViewController()
            
            restaurantCreateAccountViewModel.setRestaurentAddress(restaurentAddress: AddressDetails(doorNoAndBuildingNameAndBuildingNo: restaurantCreateAccountAddressView.doorNoAndBuildingNameAndBuildingNoTextField.text!, streetName: restaurantCreateAccountAddressView.streetNameTextfield.text!, localityName: restaurantCreateAccountAddressView.localityButton.titleLabel!.text!, landmark: restaurantCreateAccountAddressView.landmarkTextfield.text!, city: restaurantCreateAccountAddressView.cityTextfield.text!, state: restaurantCreateAccountAddressView.stateTextField.text!, pincode: restaurantCreateAccountAddressView.pincodeTextField.text!, country: restaurantCreateAccountAddressView.countryTextfield.text!))
            restaurantCreateAccountAuthenticationDetailsViewController.setRestaurantCreateAccountViewModel(restaurantCreateAccountViewModel: restaurantCreateAccountViewModel)
            navigationController?.pushViewController(restaurantCreateAccountAuthenticationDetailsViewController, animated: true)
        }
        else{
            showAlert(message: "Please Fill all the fields")
        }
        
        
    }
    
    
    
     func didTapPincodeKeyboardDoneBarButton(){
        if(NetworkMonitor.shared.isReachable){
            restaurantCreateAccountAddressView.pincodeTextField.resignFirstResponder()
            showActivityIndicator()
            PincodeDetailsUtils.getDetailsOfPincode(pincode: restaurantCreateAccountAddressView.pincodeTextField.text ?? "") { [weak self] pincodeDetails in
                self?.hideActivityIndicator()
                if(pincodeDetails.localities.count > 0){
                    self?.restaurantCreateAccountAddressView.countryTextfield.text = pincodeDetails.country
                    self?.restaurantCreateAccountAddressView.countryTextfield.showTopBorderLabel()
                    self?.restaurantCreateAccountAddressView.stateTextField.text = pincodeDetails.state
                    self?.restaurantCreateAccountAddressView.stateTextField.showTopBorderLabel()
                    self?.restaurantCreateAccountAddressView.cityTextfield.text = pincodeDetails.city
                    self?.restaurantCreateAccountAddressView.cityTextfield.showTopBorderLabel()
                    self?.restaurantCreateAccountAddressView.localityButton.setTitle("Locality", for: .normal)
                    self?.restaurantCreateAccountAddressView.localityButton.setTitleColor(.systemGray3, for: .normal)
                    self?.restaurantCreateAccountAddressView.localityButton.hideTopBorderLabelInView()
                    self?.localities = pincodeDetails.localities
                    self?.restaurantCreateAccountAddressView.localityButton.isUserInteractionEnabled = true
                }
                else{
                    self?.showAlert(message: "Please Enter Valid Pincode")
                    self?.restaurantCreateAccountAddressView.countryTextfield.text = ""
                    self?.restaurantCreateAccountAddressView.countryTextfield.hideTopBorderLabel()
                    self?.restaurantCreateAccountAddressView.stateTextField.text = ""
                    self?.restaurantCreateAccountAddressView.stateTextField.hideTopBorderLabel()
                    self?.restaurantCreateAccountAddressView.cityTextfield.text = ""
                    self?.restaurantCreateAccountAddressView.cityTextfield.hideTopBorderLabel()
                    self?.restaurantCreateAccountAddressView.localityButton.setTitle("Locality", for: .normal)
                    self?.restaurantCreateAccountAddressView.localityButton.setTitleColor(.systemGray3, for: .normal)
                    self?.restaurantCreateAccountAddressView.localityButton.hideTopBorderLabelInView()
                    self?.localities = []
                    self?.restaurantCreateAccountAddressView.localityButton.isUserInteractionEnabled = false
                    self?.restaurantCreateAccountAddressView.pincodeTextField.becomeFirstResponder()
                }
            }
        }
        else{
            showAlert(message: "Please Connect To Internet")
        }
    }
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
     func didTapView(){
        view.endEditing(true)
    }
    
    
     func didTapLocality(){
        let dropDownTable =  DropDownTableViewUtils()
        dropDownTable.setRequiredDataForDropDownTable(selectedButton: restaurantCreateAccountAddressView.localityButton, dataSourse: localities)
        dropDownTable.modalPresentationStyle = .overCurrentContext
        present(dropDownTable, animated: false, completion: nil)
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        keyboardUtils.keyboardWillShow(notification: notification, maxYofLastObjectInView: restaurantCreateAccountAddressView.getMaxYOfLastObject(), activeArea: activeArea)
             
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardUtils.keyboardWillHide()
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField{
        case restaurantCreateAccountAddressView.doorNoAndBuildingNameAndBuildingNoTextField :
            restaurantCreateAccountAddressView.streetNameTextfield.becomeFirstResponder()
        case restaurantCreateAccountAddressView.streetNameTextfield :
            restaurantCreateAccountAddressView.landmarkTextfield.becomeFirstResponder()
        case restaurantCreateAccountAddressView.landmarkTextfield :
            restaurantCreateAccountAddressView.landmarkTextfield.resignFirstResponder()
       
        default:
            return true
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        if( textField == restaurantCreateAccountAddressView.pincodeTextField){
            let maxLength = 6
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            var res = ""
            if restaurantCreateAccountAddressView.pincodeTextField.text != "" || string != "" {
                    res = (textField.text ?? "") + string
                }
            return newString.length <= maxLength && Double(res) != nil
        }
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeArea = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeArea = nil
    }
    
    

    
    func setRestaurantCreateAccountViewModel(restaurantCreateAccountViewModel :RestaurantCreateAccountViewModel){
        self.restaurantCreateAccountViewModel = restaurantCreateAccountViewModel
    }
    
    override func viewDidAppear(_ animated: Bool) {
        restaurantCreateAccountAddressView.pincodeTextField.becomeFirstResponder()
       
    }
    
     private func initialiseView(){
        title = "Address"
        view.backgroundColor = .white
        view.addSubview(scrollView)
         restaurantCreateAccountAddressView = AddressView()
         restaurantCreateAccountAddressView.translatesAutoresizingMaskIntoConstraints = false
         restaurantCreateAccountAddressView.delegate = self
         scrollView.addSubview(restaurantCreateAccountAddressView)
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())
        
    }
    
     private func initialiseViewElements(){
        navigationItem.leftBarButtonItem = backArrowButton
    }
    
    
    
    
     private func addDelegatesForElements(){
         restaurantCreateAccountAddressView.doorNoAndBuildingNameAndBuildingNoTextField.delegate = self
         restaurantCreateAccountAddressView.streetNameTextfield.delegate = self
         restaurantCreateAccountAddressView.landmarkTextfield.delegate = self
         restaurantCreateAccountAddressView.cityTextfield.delegate = self
         restaurantCreateAccountAddressView.stateTextField.delegate = self
         restaurantCreateAccountAddressView.countryTextfield.delegate = self
         restaurantCreateAccountAddressView.pincodeTextField.delegate = self
    }
    
     private func addObserver(){
        keyboardUtils = KeyboardUtils(scrollView: scrollView)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
    }
}

extension RestaurantCreateAccountAddressViewController{
    
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                         scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                         scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                         scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [restaurantCreateAccountAddressView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               restaurantCreateAccountAddressView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               restaurantCreateAccountAddressView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               restaurantCreateAccountAddressView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               restaurantCreateAccountAddressView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                               ]
       
        return viewConstraints
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if(restaurantCreateAccountAddressView.getMaxYOfLastObject() > 0){
            restaurantCreateAccountAddressView.removeHeightAnchorConstraints()
            restaurantCreateAccountAddressView.heightAnchor.constraint(equalToConstant:restaurantCreateAccountAddressView.getMaxYOfLastObject() + 100).isActive = true
        }
        else{
            restaurantCreateAccountAddressView.removeHeightAnchorConstraints()
            restaurantCreateAccountAddressView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
        
    }
    
}
