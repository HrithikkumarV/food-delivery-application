//
//  RestaurantCreateAccountPhoneNumberFormViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 08/12/21.
//

import Foundation

import UIKit

class RestaurantCreateAccountPhoneNumberFormViewController : UIViewController, UITextFieldDelegate,RestaurantCreateAccountPhoneNumberFormViewDelegate{
    
     private var restaurantCreateAccountPhoneNumberFormView : RestaurantCreateAccountPhoneNumberFormViewProtocol!
     private var restaurantCreateAccountViewModel : RestaurantCreateAccountViewModel!
    lazy var backArrowButton : UIBarButtonItem = {
        let  backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addDelegatesForElements()
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if( textField == restaurantCreateAccountPhoneNumberFormView.phoneNumberTextField){
            let maxLength = 10
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            var res = ""
            if restaurantCreateAccountPhoneNumberFormView.phoneNumberTextField.text != "" || string != "" {
                    res = (textField.text ?? "") + string
                }
            return newString.length <= maxLength && Double(res) != nil
        }
        return true
    }
    
     func didTapVerifyWithOTP(){
         if(restaurantCreateAccountPhoneNumberFormView.phoneNumberTextField.text?.isValidPhoneNumber() == true){
            let phoneNumberText = restaurantCreateAccountPhoneNumberFormView.phoneNumberTextField.text!
            DispatchQueue.global().async { [self] in
                if(restaurantCreateAccountViewModel.checkIFphoneNumberExists(phoneNumber: phoneNumberText) == false){
                    DispatchQueue.main.async { [self] in
                        if(NetworkMonitor.shared.isReachable){
                            let restaurantCreateAccountOTPFormViewController = RestaurantCreateAccountOTPFormViewController()
                            restaurantCreateAccountOTPFormViewController.setRestaurantPhoneNumber(phoneNumber: phoneNumberText)
                            restaurantCreateAccountOTPFormViewController.setRestaurantCreateAccountViewModel(restaurantCreateAccountViewModel: restaurantCreateAccountViewModel)
                            navigationController?.pushViewController(restaurantCreateAccountOTPFormViewController, animated: true)
                        }
                        else{
                            showAlert(message: "Please Connect to Internet")
                        }
                    }
                }
                else{
                    DispatchQueue.main.async {
                        self.showAlert(message: "Phone number already exits")
                    }
                }
            }
        }
        else{
            showAlert(message: "Please enter a valid Phone number")
        }
    }
    
    
     func didTapPhoneNumberkeyboardNextBarButton(){
        didTapVerifyWithOTP()
    }
    
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        restaurantCreateAccountPhoneNumberFormView.phoneNumberTextField.becomeFirstResponder()
    }
    func setRestaurantCreateAccountViewModel(restaurantCreateAccountViewModel :RestaurantCreateAccountViewModel){
        self.restaurantCreateAccountViewModel = restaurantCreateAccountViewModel
    }
    
    
     private func initialiseView(){
        title = "Phone Number"
        view.backgroundColor = .white
         restaurantCreateAccountPhoneNumberFormView = RestaurantCreateAccountPhoneNumberFormView()
         restaurantCreateAccountPhoneNumberFormView.translatesAutoresizingMaskIntoConstraints = false
         restaurantCreateAccountPhoneNumberFormView.delegate = self
        view.addSubview(restaurantCreateAccountPhoneNumberFormView)
         
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
     private func initialiseViewElements(){
         
       
       
        navigationItem.leftBarButtonItem = backArrowButton 
        
    }
    
   
    
     private func addDelegatesForElements(){
         restaurantCreateAccountPhoneNumberFormView.phoneNumberTextField.delegate = self
    }
}
    
extension RestaurantCreateAccountPhoneNumberFormViewController{
    
   
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let contentViewConstraints = [restaurantCreateAccountPhoneNumberFormView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor),
                                      restaurantCreateAccountPhoneNumberFormView.bottomAnchor.constraint(equalTo : view.safeAreaLayoutGuide.bottomAnchor),
                                      restaurantCreateAccountPhoneNumberFormView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                                      restaurantCreateAccountPhoneNumberFormView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)]
            return contentViewConstraints
    }
}
