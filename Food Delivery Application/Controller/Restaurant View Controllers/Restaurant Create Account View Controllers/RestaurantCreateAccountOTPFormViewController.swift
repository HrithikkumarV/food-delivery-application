//
//  ontroller.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 08/12/21.
//

import Foundation

import UIKit

class RestaurantCreateAccountOTPFormViewController : UIViewController, UITextFieldDelegate,OTPFormViewDelegate{
    
     var restaurantCreateAccountOTPFormView : OTPFormViewProtocol!
     var restaurantCreateAccountViewModel : RestaurantCreateAccountViewModel!
     var OTP : String!
     var restaurantPhoneNumber : String!
    
    let scrollView : UIScrollView = {
            let scrollView = UIScrollView()
            scrollView.translatesAutoresizingMaskIntoConstraints = false
            scrollView.keyboardDismissMode = .onDrag
            return scrollView
    }()
    
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
       backArrowButton.action = #selector(didTapBackArrow)
       backArrowButton.target = self
        return backArrowButton
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addDelegatesForElements()
        updateDataInViewElements()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sendOTP()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        restaurantCreateAccountOTPFormView.OTPTextField.becomeFirstResponder()
    }
    
    
    
    func setRestaurantCreateAccountViewModel(restaurantCreateAccountViewModel :RestaurantCreateAccountViewModel){
        self.restaurantCreateAccountViewModel = restaurantCreateAccountViewModel
    }
    
    func setRestaurantPhoneNumber(phoneNumber : String){
        self.restaurantPhoneNumber = phoneNumber
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if( textField == restaurantCreateAccountOTPFormView.OTPTextField){
            let maxLength = 4
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            var res = ""
            if restaurantCreateAccountOTPFormView.OTPTextField.text != "" || string != "" {
                    res = (textField.text ?? "") + string
                }
            return newString.length <= maxLength && Double(res) != nil
        }
        return true
    }
    
    
   
    
     func didTapContinueToVerify(){
        if(restaurantCreateAccountOTPFormView.OTPTextField.text! == OTP){
            let restaurantCreateAccountRestaurantDetailsViewController = RestaurantCreateAccountRestaurantDetailsViewController()
            restaurantCreateAccountViewModel.setPhoneNumber(phoneNumber: restaurantPhoneNumber)
            restaurantCreateAccountRestaurantDetailsViewController.setRestaurantCreateAccountViewModel(restaurantCreateAccountViewModel: restaurantCreateAccountViewModel)
            navigationController?.pushViewController(restaurantCreateAccountRestaurantDetailsViewController, animated: true)
        }
        else{
            showAlert(message: "Enter valid OTP")
        }
    }
    
    
    func didTapOTPkeyboardDoneBarButton(){
        didTapContinueToVerify()
    }
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    func didTapResendOTP(){
        sendOTP()
        showAlert(message: "Resent OTP")
    }
    
    func didTapChangePhoneNumber(){
        navigationController?.popViewController(animated: true)
    }
    
   
    
    func sendOTP(){
        OTP = String(Int.random(in: 1000..<10000))
        OTPUtils.sendOTP(message: OTP!, phoneNumber: restaurantPhoneNumber)
    }
    
    
     private func initialiseView(){
        title = "OTP Verification"
        view.backgroundColor = .white
        view.addSubview(scrollView)
        restaurantCreateAccountOTPFormView = OTPFormView()
         restaurantCreateAccountOTPFormView.translatesAutoresizingMaskIntoConstraints = false
        restaurantCreateAccountOTPFormView.delegate = self
        scrollView.addSubview(restaurantCreateAccountOTPFormView)
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())
        
    }
    
     private func initialiseViewElements(){
        navigationItem.leftBarButtonItem = backArrowButton
        
    }
    
  
     private func addDelegatesForElements(){
         restaurantCreateAccountOTPFormView.OTPTextField.delegate = self
    }
    
    private func updateDataInViewElements(){
        restaurantCreateAccountOTPFormView.updatePhonenumberInVerificationExplanationLabel(phoneNumber: restaurantPhoneNumber)
    }
}


extension RestaurantCreateAccountOTPFormViewController {
   
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor),
                                         scrollView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                                         scrollView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
                                         scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [restaurantCreateAccountOTPFormView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               restaurantCreateAccountOTPFormView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               restaurantCreateAccountOTPFormView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               restaurantCreateAccountOTPFormView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               restaurantCreateAccountOTPFormView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if(restaurantCreateAccountOTPFormView.getMaxYOfLastObject() > 0){
            restaurantCreateAccountOTPFormView.removeHeightAnchorConstraints()
            restaurantCreateAccountOTPFormView.heightAnchor.constraint(equalToConstant:restaurantCreateAccountOTPFormView.getMaxYOfLastObject() + 100).isActive = true
        }
        else{
            restaurantCreateAccountOTPFormView.removeHeightAnchorConstraints()
            restaurantCreateAccountOTPFormView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
        
    }
}
