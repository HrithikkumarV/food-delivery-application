//
//  UserCreateAccountPhoneNumberFormViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/12/21.
//

import Foundation
import UIKit

class UserCreateAccountUserDetailsFormViewController : UIViewController,UITextFieldDelegate,UserCreateAccountUserDetailsViewDelegate{
    
     private var userCreateAccountUserDetailsView : UserCreateAccountUserDetailsViewProtocol!
     private var userCreateAccountViewModel : UserCreateAccountViewModel = UserCreateAccountViewModel()
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
     
    private let scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
        scrollView.backgroundColor = .white
        scrollView.translatesAutoresizingMaskIntoConstraints =  false
        return scrollView
    }()
     
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addDelegatesForElements()
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if(userCreateAccountUserDetailsView.stackViewForUserDetails.frame.maxY > 0){
            userCreateAccountUserDetailsView.removeHeightAnchorConstraints()
            userCreateAccountUserDetailsView.heightAnchor.constraint(equalToConstant:  userCreateAccountUserDetailsView.stackViewForUserDetails.frame.maxY + 100).isActive = true
        }
        else{
            userCreateAccountUserDetailsView.removeHeightAnchorConstraints()
            userCreateAccountUserDetailsView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
            }
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
       
        case userCreateAccountUserDetailsView.userNameTextField :
            userCreateAccountUserDetailsView.phoneNumberTextField.becomeFirstResponder()
        case userCreateAccountUserDetailsView.phoneNumberTextField :
            userCreateAccountUserDetailsView.phoneNumberTextField.resignFirstResponder()
        
        default:
            return true
        }
        return true
    }
    
    
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            if( textField == userCreateAccountUserDetailsView.phoneNumberTextField){
                let maxLength = 10
                let currentString: NSString = (textField.text ?? "") as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                var res = ""
                if userCreateAccountUserDetailsView.phoneNumberTextField.text != "" || string != "" {
                        res = (textField.text ?? "") + string
                    }
                return newString.length <= maxLength && Double(res) != nil
            }
            return true
        }
        
    
    
    @objc func didTapView(){
        view.endEditing(true)
    }
    
    @objc func didTapContinue(){
        if(!userCreateAccountUserDetailsView.userNameTextField.text!.isEmpty && !userCreateAccountUserDetailsView.phoneNumberTextField.text!.isEmpty){
            if(userCreateAccountUserDetailsView.phoneNumberTextField.text?.isValidPhoneNumber() == true){
                let userPhoneNumberText = userCreateAccountUserDetailsView.phoneNumberTextField.text!
                let userNameText = userCreateAccountUserDetailsView.userNameTextField.text!
                DispatchQueue.global().async { [self] in
                    if(userCreateAccountViewModel.ckeckIfPhoneNumberExists(phoneNumber:userPhoneNumberText ) ==  false){
                        DispatchQueue.main.async { [self] in
                            if(NetworkMonitor.shared.isReachable){
                                let userCreateAccountOTPFormViewController = UserCreateAccountOTPFormViewController()
                                userCreateAccountOTPFormViewController.setUserPhoneNumber(phoneNumber: userPhoneNumberText)
                                userCreateAccountViewModel.setUserDetails(userDetails: UserDetails(userName:userNameText, userPhoneNumber: userPhoneNumberText))
                                userCreateAccountOTPFormViewController.setUserCreateAccountViewModel(userCreateAccountViewModel: userCreateAccountViewModel)
                                navigationController?.pushViewController(userCreateAccountOTPFormViewController, animated: true)
                            }
                            else{
                                showAlert(message: "Please Connect to Internet")
                            }
                        }
                    }
                    else{
                        showAlert(message: "Phone number Already exists")
                    }
                }
            }
            else{
                showAlert(message: "Please enter a valid Phone number")
            }
        }
        else{
            showAlert(message: "Please fill all the fields")
        }
    }
    
    
    @objc func didTapPhoneNumberkeyboardNextBarButton(){
        didTapContinue()
    }
    
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        userCreateAccountUserDetailsView.userNameTextField.becomeFirstResponder()
    }
    
    func setUserCreateAccountViewModel(userCreateAccountViewModel : UserCreateAccountViewModel){
        self.userCreateAccountViewModel = userCreateAccountViewModel
    }
    
    
    
     private func initialiseView(){
        title = "User Details"
        view.backgroundColor = .white
        
        view.addSubview(scrollView)
         userCreateAccountUserDetailsView = UserCreateAccountUserDetailsView()
         userCreateAccountUserDetailsView.translatesAutoresizingMaskIntoConstraints = false
         userCreateAccountUserDetailsView.delegate = self
        scrollView.addSubview(userCreateAccountUserDetailsView)
         
         NSLayoutConstraint.activate(getContentViewConstraints())
         NSLayoutConstraint.activate(getScrollViewConstraints())
    }
    
     private func initialiseViewElements(){
         
        
       
        navigationItem.leftBarButtonItem = backArrowButton 
       
    }
    
    
     private func addDelegatesForElements(){
         userCreateAccountUserDetailsView.phoneNumberTextField.delegate = self
         userCreateAccountUserDetailsView.userNameTextField.delegate = self
    }
}

extension UserCreateAccountUserDetailsFormViewController{
    
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                         scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                         scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                         scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [userCreateAccountUserDetailsView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               userCreateAccountUserDetailsView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               userCreateAccountUserDetailsView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               userCreateAccountUserDetailsView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               userCreateAccountUserDetailsView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }
}
