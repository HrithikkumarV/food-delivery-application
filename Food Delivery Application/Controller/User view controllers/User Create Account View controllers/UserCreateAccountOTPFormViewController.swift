//
//  UserCreateAccountOTPFormViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/12/21.
//

import Foundation

import UIKit

class UserCreateAccountOTPFormViewController : UserLoginOTPFormViewController{
    
     private var userCreateAccountViewModel : UserCreateAccountViewModel!
    
    func setUserCreateAccountViewModel(userCreateAccountViewModel : UserCreateAccountViewModel){
        self.userCreateAccountViewModel = userCreateAccountViewModel
    }
    
    @objc override func didTapContinueToVerify(){
        if(userLoginOTPFormView.OTPTextField.text! == OTP){
            DispatchQueue.global().async { [self] in
                if(userCreateAccountViewModel.createUserAccount()){
                    let userId = self.userCreateAccountViewModel.getUserId(phoneNumber: self.userPhoneNumber)
                    if(UserIDUtils.shared.setUserId(userId: userId)){
                        DispatchQueue.main.async {
                            self.showToast(message: "Account Created Successfully", font: UIFont.systemFont(ofSize: 25, weight: .medium), fontColor: .white, backGroundColor: .systemGreen.withAlphaComponent(0.9))
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                    
                }
            }
        }
        else{
            showAlert(message: "Enter valid OTP")
        }
    }
    
}
