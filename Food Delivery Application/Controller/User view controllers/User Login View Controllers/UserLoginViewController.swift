//
//  UserLoginPageViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 12/11/21.
//

import Foundation
import UIKit

class UserLoginViewController : UIViewController, UITextFieldDelegate,UserLoginViewDelegate{
     private var userLoginView : UserLoginViewProtocol!
     private let userLoginViewModel : UserLoginViewModel = UserLoginViewModel()
     
    let scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
       scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
     private var activeArea : UIView? = nil
     private var keyboardUtils : KeyboardUtils!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addDelegatesForElements()
        addObserver()
    }
    
     func clearInputFields() {
         userLoginView.phoneNumberTextField.text = ""
         userLoginView.phoneNumberTextField.hideTopBorderLabelInView()
    }
    
    @objc func didTapLogin(){
        if(userLoginView.phoneNumberTextField.text?.isValidPhoneNumber() == true){
            let userPhoneNumberText = userLoginView.phoneNumberTextField.text!
            DispatchQueue.global().async { [self] in
                if(userLoginViewModel.ckeckIfPhoneNumberExists(phoneNumber:userPhoneNumberText) ==  true){
                    DispatchQueue.main.async { [self] in
                        if(NetworkMonitor.shared.isReachable){
                            let userLoginOTPFormViewController = UserLoginOTPFormViewController()
                            userLoginOTPFormViewController.setUserPhoneNumber(phoneNumber: userPhoneNumberText)
                            navigationController?.pushViewController(userLoginOTPFormViewController, animated: true)
                            clearInputFields()
                        }
                        else{
                            showAlert(message: "Please Connect To Internet")
                        }
                    }
                }
                else{
                    DispatchQueue.main.async {
                        self.showAlert(message: "Phone number does not exist , please Create Account")
                    }
                }
            }
            
            
        }
        else{
            showAlert(message: "Please enter a valid phone number")
        }
    }
    
    @objc func didTapCreateAccount(){
        let userCreateAccountUserDetailsFormViewController = UserCreateAccountUserDetailsFormViewController()
        userCreateAccountUserDetailsFormViewController.setUserCreateAccountViewModel(userCreateAccountViewModel: UserCreateAccountViewModel())
        self.navigationController?.pushViewController(userCreateAccountUserDetailsFormViewController, animated: true)
    }
    
    
    
   
    
     func didTapView(){
        view.endEditing(true)
    }
    
    func didTapPhoneNumberkeyboardNextBarButton(){
        didTapLogin()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == userLoginView.phoneNumberTextField{
            didTapLogin()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if( textField == userLoginView.phoneNumberTextField){
            let maxLength = 10
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            var res = ""
            if userLoginView.phoneNumberTextField.text != "" || string != "" {
                    res = (textField.text ?? "") + string
                }
            return newString.length <= maxLength && Double(res) != nil
        }
        return true
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        //adjust layout
        keyboardUtils.keyboardWillShow(notification: notification, maxYofLastObjectInView: userLoginView.createAccountButton.frame.maxY, activeArea: activeArea)
       
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardUtils.keyboardWillHide()
    }
    
    @objc func didTapBackArrow(){
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeArea = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeArea = nil
    }
    

    
     private func initialiseView(){
        title = "Login"
        view.backgroundColor = .white
        
        view.addSubview(scrollView)
         userLoginView = UserLoginView()
         userLoginView.translatesAutoresizingMaskIntoConstraints = false
         userLoginView.delegate = self
        scrollView.addSubview(userLoginView)
         
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())

    }
    
     private func initialiseViewElements(){
         
        
        navigationItem.leftBarButtonItem = backArrowButton 
        
    }
    
    
     private func addDelegatesForElements(){
         userLoginView.phoneNumberTextField.delegate = self
    }
    
     private func addObserver(){
        keyboardUtils = KeyboardUtils(scrollView: scrollView)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
    }
    
    
    
}

    
extension UserLoginViewController{
    
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                         scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                         scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                         scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [userLoginView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               userLoginView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               userLoginView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               userLoginView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               userLoginView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
        if(userLoginView.createAccountButton.frame.maxY > 0){
            userLoginView.removeHeightAnchorConstraints()
            userLoginView.heightAnchor.constraint(equalToConstant:  userLoginView.createAccountButton.frame.maxY + 100).isActive = true
        }
        else{
            userLoginView.removeHeightAnchorConstraints()
            userLoginView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
    }
}



    
    
    
    
    
    
    
    
