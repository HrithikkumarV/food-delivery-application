//
//  UserLoginOTPFormViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/12/21.
//

import Foundation

import UIKit

class UserLoginOTPFormViewController : UIViewController, UITextFieldDelegate,OTPFormViewDelegate{
     var userLoginOTPFormView : OTPFormViewProtocol!
     let userLoginViewModel : UserLoginViewModel = UserLoginViewModel()
     var OTP : String!
     var userPhoneNumber : String!
    
    let scrollView : UIScrollView = {
       let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.keyboardDismissMode = .onDrag
        return scrollView
    }()
    
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
     
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addDelegatesForElements()
        updateDataInViewElements()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        userLoginOTPFormView.OTPTextField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sendOTP()
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func setUserPhoneNumber(phoneNumber : String){
        self.userPhoneNumber = phoneNumber
    }
    
    func sendOTP(){
        OTP = String(Int.random(in: 1000..<10000))
        OTPUtils.sendOTP(message: OTP!, phoneNumber: userPhoneNumber ?? "")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if( textField == userLoginOTPFormView.OTPTextField){
            let maxLength = 4
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            var res = ""
            if userLoginOTPFormView.OTPTextField.text != "" || string != "" {
                    res = (textField.text ?? "") + string
                }
            return newString.length <= maxLength && Double(res) != nil
        }
        return true
    }
    
     
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    func didTapContinueToVerify(){
       if(userLoginOTPFormView.OTPTextField.text! == OTP){
           DispatchQueue.global().async { [self] in
               let userId = userLoginViewModel.getUserId(phoneNumber: userPhoneNumber)
              
               if(UserIDUtils.shared.setUserId(userId: userId)){
                   DispatchQueue.main.async {
                       self.dismiss(animated: true, completion: nil)
                   }
               }
           }
           
       }
       else{
           showAlert(message: "Enter valid OTP")
       }
   }
   
   
    func didTapOTPkeyboardDoneBarButton(){
       didTapContinueToVerify()
   }
    
    func didTapResendOTP(){
        sendOTP()
        showAlert(message: "Resent OTP")
    }
    
     func didTapChangePhoneNumber(){
        navigationController?.popViewController(animated: true)
    }
    

     private func initialiseView(){
        title = "OTP Verification"
        view.backgroundColor = .white
        view.addSubview(scrollView)
        userLoginOTPFormView = OTPFormView()
        userLoginOTPFormView.translatesAutoresizingMaskIntoConstraints = false
        userLoginOTPFormView.delegate = self
        scrollView.addSubview(userLoginOTPFormView)
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())
        
        
    }
    
     private func initialiseViewElements(){
        navigationItem.leftBarButtonItem = backArrowButton 
        
    }
    
     
    
     private func addDelegatesForElements(){
         userLoginOTPFormView.OTPTextField.delegate = self
    }
    
    private func updateDataInViewElements(){
        userLoginOTPFormView.updatePhonenumberInVerificationExplanationLabel(phoneNumber: userPhoneNumber ?? "")
    }
}


extension UserLoginOTPFormViewController{
    
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                         scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                         scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                         scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [userLoginOTPFormView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               userLoginOTPFormView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               userLoginOTPFormView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               userLoginOTPFormView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               userLoginOTPFormView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if(userLoginOTPFormView.getMaxYOfLastObject() > 0){
            userLoginOTPFormView.removeHeightAnchorConstraints()
            userLoginOTPFormView.heightAnchor.constraint(equalToConstant:userLoginOTPFormView.getMaxYOfLastObject() + 100).isActive = true
        }
        else{
            userLoginOTPFormView.removeHeightAnchorConstraints()
            userLoginOTPFormView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
        
    }
}
