//
//  UserSearchMenuAndRestaurantViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 28/02/22.
//

import UIKit

class UserSearchMenuAndRestaurantViewController : UIViewController, UISearchBarDelegate,UserSearchMenuAndRestaurantsViewDelegate{
     private var userSearchMenuAndRestaurantViewModel : UserSearchMenuAndRestaurantViewModel = UserSearchMenuAndRestaurantViewModel()
    private var userCartViewModel : UserCartViewModel = UserCartViewModel.getUserCartViewModel()!
     private var userSearchMenuAndRestaurantView : UserSearchMenuAndRestaurantViewProtocol!
     private var didEnterRestuarant : Bool = false
     private var searchHistoryAndSuggestionItems : [SearchSuggessionModel] = []
     private var restaurantsContentsDetails : [RestaurantContentDetails] = []
     private var menuDetailsOfRestaurant : [Int : MenuContentDetails] = [:]
     private var restaurantDetailsOfMenu : [Int : RestaurantContentDetails] = [:]
     private var menuCountLabels : [Int :UILabel] = [:]
     private var addMenuButtonViews : [Int : UIView] = [:]
     private var orderedMenuKeys : [Int] = []
     private var deliveryAddress : UserAddressDetails!
     private var menuPricePair : [Int : Int] = [:]
     private var menuItemsInCart : [Int: Int] = [:]
     private var isDeliverable : Bool =  true
     private var showHeaderOfSearchSuggestion = true
     private var tableSelected : TableSelected = .searchHistory
    private var searchText  : String = ""
     var searchMenuAndRestaurantBar : UISearchBar = {
        let searchMenuAndRestaurantBar = UISearchBar()
        searchMenuAndRestaurantBar.placeholder = "Enter dish name,Restaurant Name"
        searchMenuAndRestaurantBar.backgroundColor = .white
        searchMenuAndRestaurantBar.keyboardType = .default
        searchMenuAndRestaurantBar.returnKeyType = .search
        return searchMenuAndRestaurantBar
    }()
    
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
         backArrowButton.action = #selector(didTapBackArrow)
         backArrowButton.target = self
         return backArrowButton
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addTargetsAndActionsForElements()
        addDelegatesAndDataSource()
        getInitialDataFromDB()
        setInitialView()
    }
    
     
    
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.hidesBackButton = true
        updateMenuCount()
    }
    

    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.global().async{ [self] in
            deliveryAddress = userSearchMenuAndRestaurantViewModel.getDeliveryAddress()
            
        }
        searchMenuAndRestaurantBar.becomeFirstResponder()
    }
    
  
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchMenuAndRestaurantBar.resignFirstResponder()
    }
    
    func updateMenuCount(){
            menuItemsInCart = [:]
        DispatchQueue.global().async { [self] in
            menuItemsInCart = userCartViewModel.getCartMenuItems()
            DispatchQueue.main.async { [self] in
                if(addMenuButtonViews.count > 0){
                    for menuId in addMenuButtonViews.keys{
                        updateMenuCountLabelsBasedOnCartItems(menuId : menuId)
                    }
                  
                }
            }
        }
            
        
    }
    
    
    func didTapDishButton(){
        tableSelected = .menu
        userSearchMenuAndRestaurantView.tableView.reloadData()
        userSearchMenuAndRestaurantView.dishButton.backgroundColor = .lightGray
        userSearchMenuAndRestaurantView.restaurantButton.backgroundColor = .white
        if(menuDetailsOfRestaurant.isEmpty){
            userSearchMenuAndRestaurantView.noMatchFoundLabel.isHidden = false
            userSearchMenuAndRestaurantView.browseRestaurantsButton.isHidden  = false
            userSearchMenuAndRestaurantView.tableView.isHidden = true
        }
        else{
            userSearchMenuAndRestaurantView.noMatchFoundLabel.isHidden = true
            userSearchMenuAndRestaurantView.browseRestaurantsButton.isHidden = true
            userSearchMenuAndRestaurantView.tableView.isHidden = false
        }
    }
    
     func didTapRestaurantButton(){
         tableSelected = .restaurant
         userSearchMenuAndRestaurantView.tableView.reloadData()
         userSearchMenuAndRestaurantView.dishButton.backgroundColor = .white
         userSearchMenuAndRestaurantView.restaurantButton.backgroundColor = .lightGray
        if(restaurantsContentsDetails.isEmpty){
            userSearchMenuAndRestaurantView.noMatchFoundLabel.isHidden = false
            userSearchMenuAndRestaurantView.browseRestaurantsButton.isHidden = false
            userSearchMenuAndRestaurantView.tableView.isHidden = true
        }
        else{
            userSearchMenuAndRestaurantView.noMatchFoundLabel.isHidden = true
            userSearchMenuAndRestaurantView.browseRestaurantsButton.isHidden = true
            userSearchMenuAndRestaurantView.tableView.isHidden = false
        }
    }
    
     func didTapBrowseRestaurant(){
        didTapBackArrow()
        self.tabBarController?.selectedIndex  = 0
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchText = searchText
        userSearchMenuAndRestaurantView.dishButton.isHidden = true
        userSearchMenuAndRestaurantView.restaurantButton.isHidden = true
        tableSelected = .searchHistory
        userSearchMenuAndRestaurantView.noMatchFoundLabel.isHidden = true
        userSearchMenuAndRestaurantView.browseRestaurantsButton.isHidden = true
        DispatchQueue.global().async { [self] in
            searchHistoryAndSuggestionItems = userSearchMenuAndRestaurantViewModel.getSearchItemResults(searchItem:  searchText, pincode: deliveryAddress.addressDetails.pincode, locality: deliveryAddress.addressDetails.localityName)
            if(searchText.count != 0 ){
                showHeaderOfSearchSuggestion = false
            }
            else{
                showHeaderOfSearchSuggestion = true
            }
            DispatchQueue.main.async {
                self.userSearchMenuAndRestaurantView.tableView.reloadData()
            }
        }
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchText = searchBar.text!
        DispatchQueue.global().async { [self] in
            getRestaurantContentDetails(searchItem: searchText)
            getMenuTableContentDetails(searchItem: searchText)
            var alreadyPersisted = false
            if(restaurantsContentsDetails.count > 0 || menuDetailsOfRestaurant.count > 0){
                for searchHistoryAndSuggestionItem in searchHistoryAndSuggestionItems{
                    if(searchText == searchHistoryAndSuggestionItem.searchItemName && (searchHistoryAndSuggestionItem.searchType == .searchHistoryItem || searchHistoryAndSuggestionItem.searchType == .searchSuggetionItem)){
                        alreadyPersisted = true
                    }
                }
                if(!alreadyPersisted){
                    if(userSearchMenuAndRestaurantViewModel.persistSearchHistory(searchItemName: searchText, searchItemType: .Search)){
                        print("persisted search item")
                    }
                }
            }
            DispatchQueue.main.async { [self] in
                updateMenuCount()
                userSearchMenuAndRestaurantView.tableView.reloadData()
                userSearchMenuAndRestaurantView.restaurantButton.isHidden = false
                userSearchMenuAndRestaurantView.dishButton.isHidden = false
                displayRestaurantsOrMenuBasedOnCount()
                navigationItem.leftBarButtonItem = backArrowButton
                searchBar.resignFirstResponder()
                searchHistoryAndSuggestionItems = []
                
            }
        }
    }
    
    @objc func didTapBackArrow(){
        DispatchQueue.global(qos: .userInteractive).async { [self] in
            searchHistoryAndSuggestionItems = userSearchMenuAndRestaurantViewModel.initialSearchHistoryDetails()
            DispatchQueue.main.async { [self] in
                navigationItem.leftBarButtonItem = nil
                userSearchMenuAndRestaurantView.dishButton.backgroundColor = .white
                userSearchMenuAndRestaurantView.restaurantButton.backgroundColor = .white
                userSearchMenuAndRestaurantView.dishButton.isHidden = true
                userSearchMenuAndRestaurantView.restaurantButton.isHidden = true
                restaurantsContentsDetails  = []
                menuDetailsOfRestaurant = [:]
                restaurantDetailsOfMenu = [:]
                orderedMenuKeys = []
                tableSelected = .searchHistory
                searchMenuAndRestaurantBar.text = ""
                userSearchMenuAndRestaurantView.noMatchFoundLabel.isHidden = true
                userSearchMenuAndRestaurantView.browseRestaurantsButton.isHidden = true
                showHeaderOfSearchSuggestion = true
                searchText = ""
                userSearchMenuAndRestaurantView.tableView.reloadData()
            }
        }
        
        
    }
    
     func displayRestaurantsOrMenuBasedOnCount(){
        if(userSearchMenuAndRestaurantViewModel.getDisplayTypeBasedOnCountOfSearchResults() == .menu){
            didTapDishButton()
        }
        else{
            didTapRestaurantButton()
        }
    }
    
    
  
    
     private func initialiseView(){
        view.backgroundColor = .white
         userSearchMenuAndRestaurantView = UserSearchMenuAndRestaurantsView()
         userSearchMenuAndRestaurantView.translatesAutoresizingMaskIntoConstraints = false
         userSearchMenuAndRestaurantView.delegate = self
        view.addSubview(userSearchMenuAndRestaurantView)
         
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
     private func initialiseViewElements(){
        
        
         searchMenuAndRestaurantBar.inputAccessoryViewController?.dismissKeyboard()
        
         userSearchMenuAndRestaurantView.tableView.keyboardDismissMode = .onDrag
        
        navigationItem.titleView = searchMenuAndRestaurantBar
        setDefaultNavigationBarApprearance()
         
    }
    
   
    
    private func getInitialDataFromDB(){
        DispatchQueue.global().async { [self] in
            self.searchHistoryAndSuggestionItems = userSearchMenuAndRestaurantViewModel.initialSearchHistoryDetails()
        }
    }
    
    
    
    private func addDelegatesAndDataSource(){
       
        userSearchMenuAndRestaurantView.tableView.delegate = self
        userSearchMenuAndRestaurantView.tableView.dataSource = self
        searchMenuAndRestaurantBar.delegate = self
    }
    
     private func addTargetsAndActionsForElements(){
        
        
        
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self


        
    }
    
     private func setInitialView(){
         userSearchMenuAndRestaurantView.dishButton.isHidden = true
         userSearchMenuAndRestaurantView.restaurantButton.isHidden = true
         userSearchMenuAndRestaurantView.noMatchFoundLabel.isHidden = true
         userSearchMenuAndRestaurantView.browseRestaurantsButton.isHidden  = true
    }
    
     func getMenuTableContentDetails(searchItem : String){
        let menuTableContents = userSearchMenuAndRestaurantViewModel.getSearchedMenuContents(searchItem: searchItem, locality: deliveryAddress.addressDetails.localityName, pincode: deliveryAddress.addressDetails.pincode)
        menuDetailsOfRestaurant = menuTableContents.menuContentsInDictionaryFormat
        restaurantDetailsOfMenu = menuTableContents.restaurantContentsInDictionaryFormat
        orderedMenuKeys = menuTableContents.orderedKeys
        print(menuTableContents,searchItem)
        for menuKeys in menuDetailsOfRestaurant.keys{
            menuPricePair[menuKeys] = menuDetailsOfRestaurant[menuKeys]?.menuDetails.menuPrice
        }
    }
    
     func getRestaurantContentDetails(searchItem : String){
        restaurantsContentsDetails = userSearchMenuAndRestaurantViewModel.getRestaurantsForSearchedItem(searchItem: searchItem,locality: deliveryAddress.addressDetails.localityName, pincode: deliveryAddress.addressDetails.pincode)
        
    }
}

extension UserSearchMenuAndRestaurantViewController : UITableViewDelegate , UITableViewDataSource,SearchSuggestionTableHeaderViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableSelected == .menu){
            return orderedMenuKeys.count
        }
        else if(tableSelected == .restaurant ){
            return restaurantsContentsDetails.count
        }
        else if(tableSelected == .searchHistory){
            return searchHistoryAndSuggestionItems.count
        }
        return 0
        
    }
    
  
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableSelected == .menu){
                if(orderedMenuKeys.count > 0){
                    return getMenuDetailsCell(indexPath: indexPath, tableView: tableView)
                }
                else{
                    return UITableViewCell()
                }

        
            }
        else if(tableSelected == .restaurant){
                if(restaurantsContentsDetails.count > 0){
                    return getRestaurantDetailsCell(indexPath: indexPath)
                }
                else{
                    return UITableViewCell()
                }
            }
        else if(tableSelected == .searchHistory){
                if(searchHistoryAndSuggestionItems.count > 0){
                    return userSearchMenuAndRestaurantView.createSearchSuggestionTableViewCell(indexPath: indexPath, seacrhSuggesionCellContents: searchHistoryAndSuggestionItems[indexPath.row])
                }
                else{
                    return UITableViewCell()
                }
            }
        return UITableViewCell()
        }
                
     

    
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       if(tableSelected == .menu){
            return userSearchMenuAndRestaurantView.getMenuTabelViewCellHeight()
        }
       else if(tableSelected == .restaurant){
            return userSearchMenuAndRestaurantView.getRestaurantTableViewCellHeight()
        }
       else if(tableSelected == .searchHistory){
          return userSearchMenuAndRestaurantView.getsearchSuggestionAndSearchHistoryTableViewCellHeight()
       }
       return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(tableSelected == .searchHistory && searchText.count == 0){
            let headerView = userSearchMenuAndRestaurantView.searchSuggestionAndSearchHistoryTableViewHeaderView
            headerView.delegate = self
            return headerView
        }
        return UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 30))
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(tableSelected == .searchHistory && showHeaderOfSearchSuggestion){
            return 30
        }
        else if(tableSelected == .searchHistory && searchText.count != 0){
            return 0
        }
        else{
            return 30
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableSelected == .restaurant){
            pushToRestaurantDisplayMenusVC(index: indexPath.row)
            didEnterRestuarant = true

        }
        else if(tableSelected == .searchHistory){
            searchMenuAndRestaurantBar.text = searchHistoryAndSuggestionItems[indexPath.row].searchItemName
            if(searchHistoryAndSuggestionItems[indexPath.row].searchType == .searchSuggetionItem){
                DispatchQueue.global().async { [self] in
                    if(userSearchMenuAndRestaurantViewModel.persistSearchHistory(searchItemName: searchHistoryAndSuggestionItems[indexPath.row].searchItemName, searchItemType: searchHistoryAndSuggestionItems[indexPath.row].searchItemType)){
                        print("Persisted")
                    }
                }
            }
            searchBarSearchButtonClicked(searchMenuAndRestaurantBar)
            print("search tap")
        }
    }
    
   
}


extension UserSearchMenuAndRestaurantViewController{
    func getSearchSuggestionDetailsCell(indexPath : IndexPath , tableView : UITableView) -> UITableViewCell{
        return userSearchMenuAndRestaurantView.createSearchSuggestionTableViewCell(indexPath: indexPath, seacrhSuggesionCellContents: SearchSuggessionModel(searchItemName: "" , searchItemType: .Menu, searchType: .searchHistoryItem))
    }
    
    @objc func didTapClearHistory(){
        if(userSearchMenuAndRestaurantViewModel.clearSearchHistory()){
            searchHistoryAndSuggestionItems = []
            tableSelected = .searchHistory
            userSearchMenuAndRestaurantView.tableView.reloadData()
        }
    }
}
    
extension UserSearchMenuAndRestaurantViewController : UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenuDelegate{
    
    func getMenuDetailsCell(indexPath : IndexPath , tableView : UITableView) -> UITableViewCell{
        let menuContentDetails =  menuDetailsOfRestaurant[orderedMenuKeys[indexPath.row]]!
        let menuDetails = menuContentDetails.menuDetails
        
       
        
        let cell = userSearchMenuAndRestaurantView.createMenuTableViewCell(indexPath: indexPath, menuCellContents: (menuImage: menuDetails.menuImage, menuName: menuDetails.menuName, menuDescription: menuDetails.menuDescription, menuPrice: menuDetails.menuPrice, menuTarianType: menuDetails.menuTarianType,menuAvailable : menuContentDetails.menuIsAvailable                                                  ,menuAvailableNextAt: menuContentDetails.menuNextAvailableAt, menuId :menuContentDetails.menuId),restaurantDetails : restaurantDetailsOfMenu[menuContentDetails.menuId] ?? RestaurantContentDetails())
        
        cell.subDelegate = self
        
        modifyAddMenuButtonAndViewBasedOnRestaurantAvailablity(addMenuButton: cell.addMenuCellButton, plusButton: cell.plusButton, minusButton: cell.minusButton, menuCountLabel: cell.menuCountLabel, addMenuButtonView: cell.addMenuButtonView, menuId: menuContentDetails.menuId)
        menuCountLabels[menuContentDetails.menuId] = cell.menuCountLabel
        addMenuButtonViews[menuContentDetails.menuId] = cell.addMenuButtonView
        updateMenuCountLabelsBasedOnCartItems(menuId: menuContentDetails.menuId)
        return cell
    }
    
    func updateMenuCountLabelsBasedOnCartItems(menuId : Int){
        if(menuItemsInCart[menuId] != nil && menuDetailsOfRestaurant[menuId]?.menuIsAvailable == 1){
            addMenuButtonViews[menuId]?.isHidden = false
            menuCountLabels[menuId]?.text = String(menuItemsInCart[menuId]!)
        }
        else{
            addMenuButtonViews[menuId]?.isHidden = true
            menuCountLabels[menuId]?.text = "\(0)"

        }
    }
    
    func modifyAddMenuButtonAndViewBasedOnRestaurantAvailablity(addMenuButton: UIButton, plusButton: UIButton, minusButton: UIButton, menuCountLabel: UILabel,addMenuButtonView : UIView,menuId  : Int){
        if(restaurantDetailsOfMenu[menuId]?.restaurantIsAvailable == 0){
            addMenuButton.isHidden = true
            plusButton.isHidden = true
            minusButton.isHidden = true
            menuCountLabel.isHidden = true
            addMenuButtonView.isHidden = true
        }
    }
    
    
   
  
    
    func didTapRestaurantDetailsView(sender : UITapGestureRecognizer){
        let menuDisplayDetailsPageViewController = UserMenuDisplayPageViewController()
        menuDisplayDetailsPageViewController.setRestaurantContentDetails(restaurantContentDetails: restaurantDetailsOfMenu[sender.view!.tag]!)

        tabBarController?.navigationController?.pushViewController(menuDisplayDetailsPageViewController, animated: true)
    }
    
    
    @objc func didTapAddMenu(sender : UIButton){
        let menudId = sender.tag
        if(UserCartViewModel.restaurantNameInCart != restaurantDetailsOfMenu[sender.tag]?.restaurantDetails.restaurantName && UserCartViewModel.restaurantNameInCart != ""){
            showAlertToClearCart(sender: sender)
        }
        else{
            DispatchQueue.global(qos: .userInteractive).async { [self] in
                if(userCartViewModel.addMenuToCart(cartDetails: (menuId: menudId, restaurantId: restaurantDetailsOfMenu[menudId]!.restaurantId , quantity: 0 ))){
                    DispatchQueue.main.async { [self] in
                        addMenuButtonViews[menudId]!.isHidden = false
                        didTapPlusButton(sender: sender)
                    }
                }
            }
            
        }
    }
    
    @objc func didTapPlusButton(sender : UIButton){
       let menudId = sender.tag
        UserCartViewModel.totalPrice += menuPricePair[menudId] ?? 0
        UserCartViewModel.cartItemsCount += 1
        menuItemsInCart[sender.tag]  = (menuItemsInCart[menudId] ?? 0) + 1
        DispatchQueue.global().async { [self] in
            if(userCartViewModel.updateMenuInCartAndSendNotificationTomoveToFullMenuView(menuId: menudId, quantity: menuItemsInCart[menudId] ?? 0)){
                DispatchQueue.main.async { [self] in
                    menuCountLabels[menudId]!.text = String(Int(menuCountLabels[menudId]!.text!)! + 1)
                }
            }
        }
       
        
    }
    
    @objc func didTapMinusButton(sender : UIButton){
        let menudId = sender.tag
        UserCartViewModel.totalPrice -= menuPricePair[menudId] ?? 0
        UserCartViewModel.cartItemsCount -= 1
        menuItemsInCart[menudId]  = (menuItemsInCart[menudId] ?? 0) - 1
        DispatchQueue.global(qos: .userInteractive).async { [self] in
            if(userCartViewModel.updateMenuInCartAndSendNotificationTomoveToFullMenuView(menuId: menudId, quantity: menuItemsInCart[menudId] ?? 0)){
                DispatchQueue.main.async { [self] in
                    menuCountLabels[menudId]!.text = String(Int(menuCountLabels[menudId]!.text!)! - 1)
                }
            }
        }
        if(menuCountLabels[menudId]!.text! == "0"){
            DispatchQueue.global(qos: .userInteractive).async { [self] in
                if(userCartViewModel.removeMenuFromCart(menuId: menudId)){
                    DispatchQueue.main.async { [self] in
                        addMenuButtonViews[menudId]!.isHidden = true
                        menuItemsInCart.removeValue(forKey: menudId)
                    }
                }
            }
           
        }
        
        
    }
    
    
     func  showAlertToClearCart(sender : UIButton){
         let alert = UIAlertController(title: "Replace Cart Item?", message: "Your cart contains dishes from \(UserCartViewModel.restaurantNameInCart). Do you want to discard the selection and add dishes from \(restaurantDetailsOfMenu[sender.tag]?.restaurantDetails.restaurantName ?? "")?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .cancel,handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default,handler: {_ in
            DispatchQueue.global(qos: .userInteractive).async {
                if(self.userCartViewModel.clearCart()){
                    DispatchQueue.main.async {
                        UserCartViewModel.cartItemsCount = 0
                        UserCartViewModel.totalPrice = 0
                        UserCartViewModel.restaurantNameInCart = ""
                        self.didTapAddMenu(sender: sender)
                    }
                }
            }
        }))
        present(alert,animated: true)
    }
    
    
    
}

extension UserSearchMenuAndRestaurantViewController {
    
    func getRestaurantDetailsCell(indexPath : IndexPath) -> UITableViewCell{
        let restaurantDetails = restaurantsContentsDetails[indexPath.row].restaurantDetails
        restaurantsContentsDetails[indexPath.row].isDeliverable =  isDeliverable
        let cell = userSearchMenuAndRestaurantView.createRestaurantTableViewCell(indexPath: indexPath, restaurentCellContents: (restaurantProfileImage: restaurantDetails.restaurantProfileImage, restaurantName: restaurantDetails.restaurantName, restaurantStarRating: restaurantsContentsDetails[indexPath.row].restaurantStarRating, restaurantCuisine: restaurantDetails.restaurantCuisine, restaurantLocality: restaurantsContentsDetails[indexPath.row].restaurantAddress.localityName, deliveryTiming: 30, restaurantOpensNextAt: restaurantsContentsDetails[indexPath.row].restaurantOpensNextAt,restaurantIsAvailable : restaurantsContentsDetails[indexPath.row].restaurantIsAvailable,isDeliverable: restaurantsContentsDetails[indexPath.row].isDeliverable))
        return cell
    }
    
    func pushToRestaurantDisplayMenusVC(index : Int){
        let menuDisplayDetailsPageViewController = UserMenuDisplayPageViewController()
        menuDisplayDetailsPageViewController.setRestaurantContentDetails(restaurantContentDetails: restaurantsContentsDetails[index])
        tabBarController?.navigationController?.pushViewController(menuDisplayDetailsPageViewController, animated: true)
    }
    
    
   
}


enum TableSelected{
    case menu
    case restaurant
    case searchHistory
}


extension UserSearchMenuAndRestaurantViewController{
    
   
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let contentViewConstraints = [userSearchMenuAndRestaurantView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor),
                                      userSearchMenuAndRestaurantView.bottomAnchor.constraint(equalTo : view.safeAreaLayoutGuide.bottomAnchor),
                                      userSearchMenuAndRestaurantView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                                      userSearchMenuAndRestaurantView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)]
            return contentViewConstraints
    }
}
