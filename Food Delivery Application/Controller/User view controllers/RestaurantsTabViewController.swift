//
//  RestaurantTabViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 11/11/21.
//

import Foundation
import UIKit

class RestaurantsTabViewController : UIViewController,RestaurantsTabViewDelegate,RestaurantsTapNavigationBarTitleViewDelegate{
    
     private var restaurantsTabView : RestaurantsTabViewProtocol!
     private var restaurantsTabViewModel : RestaurantsTabViewModel = RestaurantsTabViewModel()
    
     private var navigationBarTitleView : RestaurantsTabNavigationBarTitleViewProtocol!
     
     private var userAddressesViewModel : UserAddressesViewModel = UserAddressesViewModel()
     private var deliveryAddress : UserAddressDetails!
     private var isDeliverable : Bool = true
     private var restaurantsContentsDetails : [RestaurantContentDetails] = []
     
    
    override func viewDidLoad(){
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        getInitialDataFromDbAndUpdateView()
        addDelegatesAndDataSourceForElements()
        
    }
    
    
    
    @objc func didTapLocalityLabel(){
        let selectDeliveryAddressViewController = SelectDeliveryAddressViewController()
       let navigateToSelectDeliveryAddress = UINavigationController(rootViewController: selectDeliveryAddressViewController)
        navigateToSelectDeliveryAddress.modalPresentationStyle = .fullScreen
        present(navigateToSelectDeliveryAddress, animated: true, completion: nil)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadRestaurantsTabBasedOnTheDeliveryAddress()
    }
    

    
    
     func reloadRestaurantsTabBasedOnTheDeliveryAddress(){
        
         DispatchQueue.global().async { [self] in
             if(!checkIfSameAddress()){
                 deliveryAddress = userAddressesViewModel.getLocalPersistedUserAddress()
                 DispatchQueue.main.async { [self] in
                     UpdateDeliveryAddressInNavigationBar()
                     UpdateRestaurantsTableViewContent()
                     
                 }
                 
             }
         }
        
    }
    
     func updateViewBasedOnTheRestaurantAvailableCount(){
        if(restaurantsContentsDetails.count == 0){
            restaurantsTabView.restaurantsTableView.isHidden = true
            restaurantsTabView.changeLocationButton.isHidden = false
            restaurantsTabView.noRestaurantsAvailableLabel.isHidden = false
        }
        else{
            restaurantsTabView.restaurantsTableView.isHidden = false
            restaurantsTabView.changeLocationButton.isHidden = true
            restaurantsTabView.noRestaurantsAvailableLabel.isHidden = true
        }
    }
    
     func UpdateRestaurantsTableViewContent(){
         DispatchQueue.global().async { [self] in
             restaurantsContentsDetails = restaurantsTabViewModel.getRestaurantsTabContents(locality: deliveryAddress.addressDetails.localityName, pincode: deliveryAddress.addressDetails.pincode)
             DispatchQueue.main.async {
                 self.restaurantsTabView.restaurantsTableView.reloadData()
                 self.updateViewBasedOnTheRestaurantAvailableCount()
             }
         }
        
    }
    
     func UpdateDeliveryAddressInNavigationBar(){
       
        navigationBarTitleView.updateAddressTagAndAddressLabel(addressTag: deliveryAddress.addressTag, address: deliveryAddress.addressDetails)
    }
    
     func checkIfSameAddress() -> Bool{
        let checkDeliveryAddress = userAddressesViewModel.getLocalPersistedUserAddress()
        if(checkDeliveryAddress.addressDetails.pincode == deliveryAddress.addressDetails.pincode &&  checkDeliveryAddress.addressDetails.localityName == deliveryAddress.addressDetails.localityName && checkDeliveryAddress.addressTag == deliveryAddress.addressTag){
            return true
        }
        return false
    }
    
    
    
     private func initialiseView(){
        view.backgroundColor = .white
         
         restaurantsTabView = RestaurantsTabView()
         restaurantsTabView.translatesAutoresizingMaskIntoConstraints = false
         view.addSubview(restaurantsTabView)
         restaurantsTabView.delegate = self
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
     private func addDelegatesAndDataSourceForElements(){
         restaurantsTabView.restaurantsTableView.delegate = self
         restaurantsTabView.restaurantsTableView.dataSource = self
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
     private func initialiseViewElements(){
         
        
        navigationBarTitleView = RestaurantsTabNavigationBarTitleView()
        navigationBarTitleView.delegate = self
        navigationItem.titleView = navigationBarTitleView
         
        setDefaultNavigationBarApprearance()
       
    }
    
    private func  getInitialDataFromDbAndUpdateView(){
        DispatchQueue.global().async { [self] in
            deliveryAddress = userAddressesViewModel.getLocalPersistedUserAddress()
            DispatchQueue.main.async { [self] in
                UpdateDeliveryAddressInNavigationBar()
                UpdateRestaurantsTableViewContent()
            }
        }
       
    }
    
     
    
    
    
    
    
}

extension RestaurantsTabViewController :  UITableViewDelegate ,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurantsContentsDetails.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let restaurantDetails = restaurantsContentsDetails[indexPath.row].restaurantDetails
        restaurantsContentsDetails[indexPath.row].isDeliverable =  isDeliverable
        let cell = restaurantsTabView.createRestaurantTableViewCell(indexPath: indexPath, restaurentCellContents: (restaurantProfileImage: restaurantDetails.restaurantProfileImage, restaurantName: restaurantDetails.restaurantName, restaurantStarRating: restaurantsContentsDetails[indexPath.row].restaurantStarRating, restaurantCuisine: restaurantDetails.restaurantCuisine, restaurantLocality: restaurantsContentsDetails[indexPath.row].restaurantAddress.localityName, deliveryTiming: 30, restaurantOpensNextAt: restaurantsContentsDetails[indexPath.row].restaurantOpensNextAt,restaurantIsAvailable: restaurantsContentsDetails[indexPath.row].restaurantIsAvailable,isDeliverable:restaurantsContentsDetails[indexPath.row].isDeliverable))
            
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        restaurantsTabView.getRestaurantTableViewCellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuDisplayDetailsPageViewController = UserMenuDisplayPageViewController()
        menuDisplayDetailsPageViewController.setRestaurantContentDetails(restaurantContentDetails: restaurantsContentsDetails[indexPath.row])


        tabBarController?.navigationController?.pushViewController(menuDisplayDetailsPageViewController, animated: true)
    }
}

extension RestaurantsTabViewController{
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let contentViewConstraints = [restaurantsTabView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor),
                                      restaurantsTabView.bottomAnchor.constraint(equalTo : view.safeAreaLayoutGuide.bottomAnchor),
                                      restaurantsTabView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                                      restaurantsTabView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)]
            return contentViewConstraints
    }
}
