//
//  UserWelcomePageViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 31/01/22.
//

import Foundation
import UIKit

class UserWelcomePageViewController : UIViewController,UserWelcomePageViewDelegate{
     
     private var userWelcomePageView : UserWelcomePageViewProtocol!
     private var userAddressesViewModel : UserAddressesViewModel = UserAddressesViewModel()
     private var userWelcomePageViewModel : UserWelcomePageViewModel = UserWelcomePageViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
       initialiseView()
       
    
    }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        if(userAddressesViewModel.getLocalPersistedUserAddress().addressTag != ""){
            navigateToUserTabBar()
        }
        else{
            actionsBasedOnIfUserLogedIn()
            userWelcomePageView.isHidden = false
        }
    }
    
     func actionsBasedOnIfUserLogedIn(){
         DispatchQueue.global().async { [self] in
             if(UserIDUtils.shared.getUserId() != 0){
                 DispatchQueue.main.async {
                     self.userLogedIn()
                 }
                 // if user delivery address is  available then proceed to select addresses page
                 
                 if(userAddressesViewModel.getUserAddresses(userId: (UserIDUtils.shared.getUserId() )).count != 0){
                     DispatchQueue.main.async {
                         self.navigateToSelectDeliveryAddress()
                     }
                 }
                 else{
                     // if user delivery address is  available then display user welcome label
                     let userName = userWelcomePageViewModel.getUserName(userId: (UserIDUtils.shared.getUserId()))
                     DispatchQueue.main.async {
                         self.userWelcomePageView.userWelcomeMessageLabel.text = "Welcome \(userName)"
                     }
                 }
             }
         }
    }
    
     func navigateToUserTabBar(){
        hideSubviewsOfRootViewController()
        self.dismiss(animated: true, completion: {
            let userTabBar = UINavigationController(rootViewController: UserTabBarController())
            userTabBar.modalPresentationStyle = .fullScreen
            UIApplication.shared.windows.first?.rootViewController?.present(userTabBar,animated: true)
        })
        showSubviewsOfRootViewControllerWithADelay()
    }
    
    
    
     func showSubviewsOfRootViewControllerWithADelay(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIApplication.shared.windows.first?.rootViewController?.view.subviews.forEach { $0.isHidden = false }
        }
    }
    
     func hideSubviewsOfRootViewController(){
        UIApplication.shared.windows.first?.rootViewController?.view.subviews.forEach { $0.isHidden = true }
    }
    
     func navigateToSelectDeliveryAddress(){
        let navigateToSelectAddressVC = UINavigationController(rootViewController: SelectDeliveryAddressViewController())
        navigateToSelectAddressVC.modalPresentationStyle = .fullScreen
        present(navigateToSelectAddressVC, animated: true, completion: nil)
       
    }
    
     func userLogedIn(){
         userWelcomePageView.haveAnAccountLabel.isHidden = true
         userWelcomePageView.loginButton.isHidden = true
         userWelcomePageView.changeAccountButton.isHidden = false
         userWelcomePageView.userWelcomeMessageLabel.isHidden = false
    }
    
    
    
    @objc func didTapSetDeliveryAddress(){
        let addressVC = UINavigationController(rootViewController: AddressViewController())
        addressVC.modalPresentationStyle = .fullScreen
        present(addressVC, animated: true, completion: nil)
        userWelcomePageView.isHidden = true
    }
    
    @objc func didTapLogin(){
        let login = UINavigationController(rootViewController: UserLoginViewController())
        login.modalPresentationStyle = .fullScreen
        present(login, animated: true, completion: nil)
        userWelcomePageView.isHidden = true
    }
    
    @objc func didTapChangeAccount(){
        didTapLogin()
    }
    
     private func initialiseView(){
        view.backgroundColor = .white
         userWelcomePageView = UserWelcomePageView()
         userWelcomePageView.translatesAutoresizingMaskIntoConstraints = false
         userWelcomePageView.delegate = self
         view.addSubview(userWelcomePageView)
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
     
}


extension UserWelcomePageViewController{
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let contentViewConstraints = [userWelcomePageView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor),
                                      userWelcomePageView.bottomAnchor.constraint(equalTo : view.safeAreaLayoutGuide.bottomAnchor),
                                      userWelcomePageView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                                      userWelcomePageView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)]
            return contentViewConstraints
    }
}
