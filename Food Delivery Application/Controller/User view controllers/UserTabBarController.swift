//
//  UserTabBar.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 12/11/21.
//

import Foundation
import UIKit

class UserTabBarController : UITabBarController, UITabBarControllerDelegate,MoveToFullMenuViewDelegate{
    private var moveToFullMenuView : MoveToFullMenuViewProtocol!
    private var userCartViewModel :  UserCartViewModel = UserCartViewModel.getUserCartViewModel()!
    private var userAddressesViewModel : UserAddressesViewModel = UserAddressesViewModel()
    private var moveToFirstVC : Bool = false
    
    override func viewDidLoad() {
           super.viewDidLoad()
        
        addRespectiveViewControllersToTabBar()
        addSymbolsToViewControllerTabs()
        addmoveToFullMenuView()
        addNotificationObservers()
        initFullMenuView()
        initialiseTabBar()
        setDefaultNavigationBarApprearance()
        setScrollEdgeApprearance()
      
    }
    
    
   
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        moveToFirstVCBasedOnMoveToFirstVCBool()

       }
    
      func moveToFirstVCBasedOnMoveToFirstVCBool(){
        if(moveToFirstVC){
            selectedIndex = 0
            moveToFirstVC = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
     func addRespectiveViewControllersToTabBar(){
        let restaurantTab = UINavigationController(rootViewController: RestaurantsTabViewController())
        let searchTab = UINavigationController(rootViewController: UserSearchMenuAndRestaurantViewController())
        let accountsTab = UINavigationController(rootViewController: UserAccountsTabViewController())
        restaurantTab.title = "Restaurants"
        searchTab.title = "Search"
        accountsTab.title = "Accounts"
        self.setViewControllers([restaurantTab,searchTab,accountsTab], animated: false)
        self.tabBar.backgroundColor = .white
    }
    
    
    
    
    
     func addSymbolsToViewControllerTabs(){
        guard let tabs = self.tabBar.items else{
            return
        }
        let images = ["house" ,"magnifyingglass", "person"]
        
        for i in 0..<tabs.count {
            tabs[i].image = UIImage(systemName: images[i])
       }
    }
    
     func initialiseTabBar(){
        self.view.backgroundColor = .white
        self.tabBar.tintColor = .black.withAlphaComponent(0.9)
        self.tabBar.unselectedItemTintColor = .systemGray
        self.tabBar.backgroundColor = .white
        self.tabBar.barTintColor = .white
    }
    
   
    
    
}



extension UserTabBarController{
    func initFullMenuView(){
        updateMenuCountAndTotalPriceInmoveToFullMenuView()
        hideOrShowmoveToFullMenuViewBasedOnMenuCount()
        updateNameOfRestaurantInCart()
        
         
    }
    
    func addmoveToFullMenuView(){
        moveToFullMenuView = MoveToFullMenuView()
        moveToFullMenuView.translatesAutoresizingMaskIntoConstraints = false
        moveToFullMenuView.delegate = self
        view.addSubview(moveToFullMenuView)
        NSLayoutConstraint.activate(getmoveToFullMenuViewLayoutConstraints())
    }
    
   
    
     func didTapMoveToFullMenu(){
        var restaurantContentDetails = userCartViewModel.getRestaurantDetailsFromCartMenu()
        var isDeliverable : Bool = false
        let deliveryAddress = userAddressesViewModel.getLocalPersistedUserAddress()
        if(restaurantContentDetails.restaurantAddress.localityName == deliveryAddress.addressDetails.localityName && restaurantContentDetails.restaurantAddress.pincode == deliveryAddress.addressDetails.pincode){
            isDeliverable = true
            restaurantContentDetails.isDeliverable =  isDeliverable
        }
        let userMenuDisplayPageViewController = UserMenuDisplayPageViewController()
        userMenuDisplayPageViewController.setRestaurantContentDetails(restaurantContentDetails: restaurantContentDetails)
        navigationController?.pushViewController(userMenuDisplayPageViewController, animated: true)
        
    }
    
    
    func addNotificationObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateMenuCountAndTotalPriceInmoveToFullMenuView), name: Notification.Name(moveToViewNotificationNames.updateMenuCountAndPriceInmoveToView.rawValue), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideOrShowmoveToFullMenuViewBasedOnMenuCount), name: Notification.Name(moveToViewNotificationNames.hideOrShowmoveToViewBasedOnMenuCount.rawValue), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateNameOfRestaurantInCart), name: Notification.Name(moveToViewNotificationNames.updateNameOfRestaurantInCart.rawValue), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showMoveToFullMenuView), name: Notification.Name(moveToViewNotificationNames.showMoveToFullMenuView.rawValue), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideMoveToFullMenuView), name: Notification.Name(moveToViewNotificationNames.hideMoveToFullMenuView.rawValue), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.moveToFirstVCInTabBarController), name: Notification.Name(moveToFirstVcInTabBar.moveToFirstVc.rawValue), object: nil)
    }
    
    @objc func updateMenuCountAndTotalPriceInmoveToFullMenuView(){
        moveToFullMenuView.updateMenuItemCountAndPriceLabelText(menuCount: UserCartViewModel.cartItemsCount, totalPrice: UserCartViewModel.totalPrice)
        print(UserCartViewModel.cartItemsCount , UserCartViewModel.totalPrice)
       
    }
    
    @objc func hideOrShowmoveToFullMenuViewBasedOnMenuCount(){
        moveToFullMenuView.hideOrShowmoveToFullMenuViewBasedOnMenuCount(menuCount: UserCartViewModel.cartItemsCount)
    }
    
    @objc func updateNameOfRestaurantInCart(){
        if(UserCartViewModel.cartItemsCount == 0){
            UserCartViewModel.restaurantNameInCart = ""
        }
        else if(UserCartViewModel.cartItemsCount == 1){
            UserCartViewModel.restaurantNameInCart = userCartViewModel.getRestaurantNameFromCartMenu()
            
        }
        moveToFullMenuView.updateRestaurantNameInCartLabelText(restaurantNameInUserCart:UserCartViewModel.restaurantNameInCart)
        
    }
    
    @objc func showMoveToFullMenuView(){
        
        view.addSubview(moveToFullMenuView)
        NSLayoutConstraint.activate(getmoveToFullMenuViewLayoutConstraints())

    }
    
    @objc func hideMoveToFullMenuView(){
        moveToFullMenuView.removeFromSuperview()
    }
    
    @objc func moveToFirstVCInTabBarController(){
        moveToFirstVC = true
    }
    
}


extension UserTabBarController{
   
        private func getmoveToFullMenuViewLayoutConstraints() -> [NSLayoutConstraint]{
            let viewContraints = [
                moveToFullMenuView.bottomAnchor.constraint(equalTo: tabBar.topAnchor),                moveToFullMenuView.leftAnchor.constraint(equalTo: view.leftAnchor,constant: 10),
                moveToFullMenuView.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -10),
                moveToFullMenuView.heightAnchor.constraint(equalToConstant: 50)]
            return viewContraints
        }
}


enum moveToViewNotificationNames : String{
    case updateNameOfRestaurantInCart
    case updateMenuCountAndPriceInmoveToView
    case hideOrShowmoveToViewBasedOnMenuCount
    case showMoveToFullMenuView
    case hideMoveToFullMenuView
}

enum moveToFirstVcInTabBar : String{
    case moveToFirstVc
}
