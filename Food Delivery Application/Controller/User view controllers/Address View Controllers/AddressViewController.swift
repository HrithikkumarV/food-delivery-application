//
//  ModifiedAddressViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 04/04/22.
//

import UIKit

class AddressViewController : UIViewController , UITextFieldDelegate,AddressViewDelegate{

    var localities :[String] = []
    var keyboardUtils : KeyboardUtils!
    var activeArea : UIView? = nil
    var stackView : UIStackView!
    var addressView : AddressViewProtocol!
    var isSubmitted : Bool = false
    var address : AddressDetails!
    
    
    
    private let scrollView : UIScrollView = {
        let  scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.keyboardDismissMode = .onDrag
        return scrollView
    }()
    
    
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
       backArrowButton.action = #selector(didTapBackArrow)
       backArrowButton.target = self
        return backArrowButton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addTargetsAndActionsForElements()
        addDelegatesForElements()
        addObserver()
    }
    


    override func viewDidAppear(_ animated: Bool) {
        addressView.pincodeTextField.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField{
        case addressView.doorNoAndBuildingNameAndBuildingNoTextField :
            addressView.streetNameTextfield.becomeFirstResponder()
        case addressView.streetNameTextfield :
            addressView.landmarkTextfield.becomeFirstResponder()
        case addressView.landmarkTextfield :
            addressView.landmarkTextfield.resignFirstResponder()
        
        default:
            return true
        }
        return true
    }

   
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if( textField == addressView.pincodeTextField){
            let maxLength = 6
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            var res = ""
            if addressView.pincodeTextField.text != "" || string != "" {
                    res = (textField.text ?? "") + string
                }
            return newString.length <= maxLength && Double(res) != nil
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeArea = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeArea = nil
    }

    
    @objc func keyboardWillShow(notification: NSNotification) {
        keyboardUtils.keyboardWillShow(notification: notification, maxYofLastObjectInView: addressView.getMaxYOfLastObject(), activeArea: activeArea)
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardUtils.keyboardWillHide()
    }
    
    
   
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
        if((navigationController?.isBeingPresented) != nil){
            dismiss(animated: true, completion: nil)
        }
    }
    
    
    func getAddress() -> AddressDetails{
        return address
    }
    
     private func initialiseView(){
         title = "Set Address"
         view.backgroundColor = .white
         view.addSubview(scrollView)
         addressView = AddressView()
         addressView.translatesAutoresizingMaskIntoConstraints = false
         addressView.delegate = self
         scrollView.addSubview(addressView)
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
     private func initialiseViewElements(){
        navigationItem.leftBarButtonItem = backArrowButton
        setDefaultNavigationBarApprearance()
    }
    
     private func addTargetsAndActionsForElements(){
       
       
    }
    
     private func addDelegatesForElements(){
         addressView.doorNoAndBuildingNameAndBuildingNoTextField.delegate = self
         addressView.streetNameTextfield.delegate = self
         addressView.landmarkTextfield.delegate = self
         addressView.cityTextfield.delegate = self
         addressView.stateTextField.delegate = self
         addressView.countryTextfield.delegate = self
         addressView.pincodeTextField.delegate = self
    }
    
     private func addObserver(){
        keyboardUtils = KeyboardUtils(scrollView: scrollView)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
    }
    
    func didTapContinue(){
        if(!addressView.doorNoAndBuildingNameAndBuildingNoTextField.text!.isEmpty && !addressView.streetNameTextfield.text!.isEmpty && !addressView.landmarkTextfield.text!.isEmpty && addressView.localityButton.titleLabel?.text! != "Locality" && !addressView.cityTextfield.text!.isEmpty && !addressView.stateTextField.text!.isEmpty && !addressView.countryTextfield.text!.isEmpty){
            address = AddressDetails(doorNoAndBuildingNameAndBuildingNo: addressView.doorNoAndBuildingNameAndBuildingNoTextField.text!, streetName: addressView.streetNameTextfield.text!, localityName: addressView.localityButton.titleLabel!.text!, landmark: addressView.landmarkTextfield.text!, city: addressView.cityTextfield.text!, state: addressView.stateTextField.text!, pincode: addressView.pincodeTextField.text!, country: addressView.countryTextfield.text!)
            isSubmitted = true
    
                let addressTagViewController = ViewAndSaveAddressViewController()
                addressTagViewController.setAddress(addressDetails: address)
                navigationController?.pushViewController(addressTagViewController, animated: true)
        }
        else{
            showAlert(message: "Please Fill all the fields")
        }
        
        
    }
    
    
    func didTapPincodeKeyboardDoneBarButton(){
        
        if(NetworkMonitor.shared.isReachable){
            addressView.pincodeTextField.resignFirstResponder()
            showActivityIndicator()
            let pincode = addressView.pincodeTextField.text ?? ""
            DispatchQueue.global(qos: .userInitiated).async {
                PincodeDetailsUtils.getDetailsOfPincode(pincode: pincode) { [weak self] pincodeDetails in
                    self?.hideActivityIndicator()
                    if(pincodeDetails.localities.count > 0){
                        self?.addressView.countryTextfield.text = pincodeDetails.country
                        self?.addressView.countryTextfield.showTopBorderLabel()
                        self?.addressView.stateTextField.text = pincodeDetails.state
                        self?.addressView.stateTextField.showTopBorderLabel()
                        self?.addressView.cityTextfield.text = pincodeDetails.city
                        self?.addressView.cityTextfield.showTopBorderLabel()
                        self?.addressView.localityButton.setTitle("Locality", for: .normal)
                        self?.addressView.localityButton.setTitleColor(.systemGray3, for: .normal)
                        self?.addressView.localityButton.hideTopBorderLabelInView()
                        self?.localities = pincodeDetails.localities
                        self?.addressView.localityButton.isUserInteractionEnabled = true
                    }
                    else{
                        self?.showAlert(message: "Please Enter Valid Pincode")
                        self?.addressView.countryTextfield.text = ""
                        self?.addressView.countryTextfield.hideTopBorderLabel()
                        self?.addressView.stateTextField.text = ""
                        self?.addressView.stateTextField.hideTopBorderLabel()
                        self?.addressView.cityTextfield.text = ""
                        self?.addressView.cityTextfield.hideTopBorderLabel()
                        self?.addressView.localityButton.setTitle("Locality", for: .normal)
                        self?.addressView.localityButton.setTitleColor(.systemGray3, for: .normal)
                        self?.addressView.localityButton.hideTopBorderLabelInView()
                        self?.localities = []
                        self?.addressView.localityButton.isUserInteractionEnabled = false
                        self?.addressView.pincodeTextField.becomeFirstResponder()
                    }
                }
            }
        }
        else{
            showAlert(message: "Please Connect To Internet")
        }
    }
    
    
    
    func didTapLocality(){
        let dropDownTable =  DropDownTableViewUtils()
        dropDownTable.setRequiredDataForDropDownTable(selectedButton: addressView.localityButton, dataSourse: localities)
        dropDownTable.modalPresentationStyle = .overCurrentContext
        present(dropDownTable, animated: false, completion: nil)
        dropDownTable.complition = {
            self.addressView.doorNoAndBuildingNameAndBuildingNoTextField.becomeFirstResponder()
        }
    }
    
    func didTapView(){
        self.view.endEditing(true)
    }
}


extension AddressViewController{
    
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                         scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                         scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                         scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [addressView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                                     addressView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                                     addressView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                                     addressView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                                     addressView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if(addressView.getMaxYOfLastObject() > 0){
            addressView.removeHeightAnchorConstraints()
            addressView.heightAnchor.constraint(equalToConstant:addressView.getMaxYOfLastObject() + 100).isActive = true
        }
        else{
            addressView.removeHeightAnchorConstraints()
            addressView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
        
    }
}


