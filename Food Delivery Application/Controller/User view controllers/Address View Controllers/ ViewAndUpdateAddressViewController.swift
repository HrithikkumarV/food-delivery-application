//
//  UpdateAddressTagViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/03/22.
//

import Foundation
import UIKit

class ViewAndUpdateAddressViewController : ViewAndSaveAddressViewController{
    private var userAddressId : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
            addressTagView.saveButton.setTitle("UPDATE", for: .normal)
    }
    
    override func didTapSave() {
        if(!addressTagView.otherAddressTagTextField.text!.isEmpty){
            addressTag = addressTagView.otherAddressTagTextField.text!
        }
        if(addressTag != ""){
            DispatchQueue.global().async { [self] in
                if(userAddressesViewModel.UpdateUserAddress(userAddressDetails: UserAddressDetails(userAddressId: userAddressId, addressDetails: addressDetails, addressTag: addressTag))){
                    if(userAddressesViewModel.getLocalPersistedUserAddress().userAddressId == userAddressId){
                        if(userAddressesViewModel.updateLocalPersistedAddress(userAddressDetails: UserAddressDetails(userAddressId: userAddressId, addressDetails: addressDetails, addressTag: addressTag))){
                            DispatchQueue.main.async {
                                for controller in self.navigationController!.viewControllers as Array {
                                                    if controller.isKind(of: ManageAddressesViewController.self) {
                                                        self.navigationController!.popToViewController(controller, animated: true)
                                                        break
                                                    }
                                                }
                            }
                                        

                        }
                        else{
                            showAlert(message: "Error while updating , retry in some time")
                        }
                        }
                    }
                else{
                    showAlert(message: "Error while updateing address")
                }
            }
            

                
            
    }
        else{
            showAlert(message: "Please select the address tag")
        }
        
    }
    
    func setUserAddressId(userAddressId : String){
        self.userAddressId = userAddressId
    }
}

