//
//  ManageAddressesPageViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/03/22.
//

import Foundation
import UIKit
class ManageAddressesViewController : UIViewController,ManageAddressesViewCellDelegate,ManageAddressesViewDelegate{
    
     private var manageAddressesView : ManageAddressesViewProtocol!
     private var userAddressViewModel : UserAddressesViewModel = UserAddressesViewModel()
     private var userAddresses  : [UserAddressDetails] = []
    
    lazy var backArrowButton  : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        initialiseView()
        initialiseViewElements()
        addDelegateAndDataSourseForElements()
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        getUserAddresses()
        manageAddressesView.savedAddressesTableView.reloadData()
    }
    
     func showOrHideSavedAddressesLabel(){
         if(userAddresses.count > 0){
             manageAddressesView.savedAddressesLabel.text = "Saved Addresses"
             manageAddressesView.savedAddressesLabel.textColor = .black
         }
         else{
             manageAddressesView.savedAddressesLabel.text = "Saved Addresses Will Appear Here"
             manageAddressesView.savedAddressesLabel.textColor = .systemGray
         }
    }
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    func didTapAddAddress(){
        navigationController?.pushViewController(AddressViewController(), animated: true)
    }
    
    
    
    
     private func initialiseView(){
        view.backgroundColor = .white
         manageAddressesView = ManageAddressesView()
         manageAddressesView.translatesAutoresizingMaskIntoConstraints = false
         manageAddressesView.delegate = self
        view.addSubview(manageAddressesView)
        NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
     private func initialiseViewElements(){
        
        navigationItem.leftBarButtonItem = backArrowButton
         
    }

    
     private func addDelegateAndDataSourseForElements(){
         manageAddressesView.savedAddressesTableView.delegate = self
         manageAddressesView.savedAddressesTableView.dataSource = self
    }
    
     func getUserAddresses(){
         DispatchQueue.global().async { [self] in
             userAddresses = userAddressViewModel.getUserAddresses(userId: (UserIDUtils.shared.getUserId() ))

             DispatchQueue.main.async {
                 self.showOrHideSavedAddressesLabel()
             }
         }
        
    }
}


extension ManageAddressesViewController :  UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userAddresses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
        let addressTableViewCell = manageAddressesView.createAddressTableViewCell(indexPath: indexPath, userAddress: userAddresses[indexPath.row])
        addressTableViewCell.delegate = self
        return addressTableViewCell
    }
    

    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return manageAddressesView.getAddressTableViewCellHeight()
        
    }
    
    
    
    @objc func didTapEditButton(sender: UIButton){
        let editAddressViewController = EditAddressViewController()
        editAddressViewController.setUserAddressDetails(userAddress: userAddresses[sender.tag])
        navigationController?.pushViewController(editAddressViewController, animated: true)
    }
    
    @objc func didTapDelete(sender: UIButton){
        let alert = UIAlertController(title: "Delete Address", message: "Are you sure you want to delete this address ?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel,handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default,handler: { [self]_ in
            if(userAddressViewModel.deleteUserAddress(userAddressId: userAddresses[sender.tag].userAddressId)){
                if(userAddressViewModel.getLocalPersistedUserAddress().userAddressId == userAddresses[sender.tag].userAddressId){
                    if(userAddressViewModel.updateUserAddressIdInLocalPersistedUserAddress(userAddressId: "")){
                        print("Updated user address id")
                    }
                }
            }
            let indexPath = IndexPath(item: sender.tag, section: 0)
            userAddresses.remove(at: sender.tag)
            manageAddressesView.savedAddressesTableView.deleteRows(at: [indexPath], with: .none)
            manageAddressesView.savedAddressesTableView.reloadData()
        }))
        present(alert,animated: true)
    }
}


extension ManageAddressesViewController{
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let contentViewConstraints = [manageAddressesView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor),
                                      manageAddressesView.bottomAnchor.constraint(equalTo : view.safeAreaLayoutGuide.bottomAnchor),
                                      manageAddressesView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                                      manageAddressesView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)]
            return contentViewConstraints
    }
}
