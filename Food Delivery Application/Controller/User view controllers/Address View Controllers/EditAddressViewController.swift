//
//  EditAddressViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/03/22.
//

import Foundation
import UIKit

class EditAddressViewController : AddressViewController{
    private var userAddress : UserAddressDetails!
    override func viewDidLoad() {
        super.viewDidLoad()
        setAddressDetailsInViewElements()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        addressView.pincodeTextField.resignFirstResponder()
    }
    
    
    
    
    func setAddressDetailsInViewElements(){
        showActivityIndicator()
        DispatchQueue.global().async {
            PincodeDetailsUtils.getDetailsOfPincode(pincode: self.userAddress.addressDetails.pincode) { [weak self] pincodeDetails  in
                self?.localities = pincodeDetails.localities
                self?.addressView.doorNoAndBuildingNameAndBuildingNoTextField.text = self?.userAddress.addressDetails.doorNoAndBuildingNameAndBuildingNo
                self?.addressView.doorNoAndBuildingNameAndBuildingNoTextField.showTopBorderLabel()
                self?.addressView.streetNameTextfield.text = self?.userAddress.addressDetails.streetName
                self?.addressView.streetNameTextfield.showTopBorderLabel()
                self?.addressView.landmarkTextfield.text = self?.userAddress.addressDetails.landmark
                self?.addressView.landmarkTextfield.showTopBorderLabel()
                self?.addressView.localityButton.setTitleColor(.black, for: .normal)
                self?.addressView.localityButton.setTitle(self?.userAddress.addressDetails.localityName, for: .normal)
                self?.addressView.localityButton.showTopBorderLabelInView()
                self?.addressView.localityButton.isUserInteractionEnabled = true
                self?.addressView.cityTextfield.text = self?.userAddress.addressDetails.city
                self?.addressView.cityTextfield.showTopBorderLabel()
                self?.addressView.stateTextField.text = self?.userAddress.addressDetails.state
                self?.addressView.stateTextField.showTopBorderLabel()
                self?.addressView.countryTextfield.text = self?.userAddress.addressDetails.country
                self?.addressView.countryTextfield.showTopBorderLabel()
                self?.addressView.pincodeTextField.text = self?.userAddress.addressDetails.pincode
                self?.addressView.pincodeTextField.showTopBorderLabel()
                self?.hideActivityIndicator()
            }
        }
    }
    
    func setUserAddressDetails(userAddress : UserAddressDetails){
        self.userAddress = userAddress
    }
    
    @objc override func didTapContinue(){
        
        if(!addressView.doorNoAndBuildingNameAndBuildingNoTextField.text!.isEmpty && !addressView.streetNameTextfield.text!.isEmpty && !addressView.landmarkTextfield.text!.isEmpty && addressView.localityButton.titleLabel?.text != "Locality" && !addressView.cityTextfield.text!.isEmpty && !addressView.stateTextField.text!.isEmpty && !addressView.countryTextfield.text!.isEmpty){
            if(!(addressView.doorNoAndBuildingNameAndBuildingNoTextField.text == userAddress.addressDetails.doorNoAndBuildingNameAndBuildingNo && addressView.streetNameTextfield.text == userAddress.addressDetails.streetName && addressView.landmarkTextfield.text == userAddress.addressDetails.landmark &&  addressView.localityButton.titleLabel?.text == userAddress.addressDetails.localityName && addressView.cityTextfield.text == userAddress.addressDetails.city && addressView.stateTextField.text == userAddress.addressDetails.state && addressView.countryTextfield.text == userAddress.addressDetails.country && addressView.pincodeTextField.text == userAddress.addressDetails.pincode)){
                address = AddressDetails(doorNoAndBuildingNameAndBuildingNo: addressView.doorNoAndBuildingNameAndBuildingNoTextField.text!, streetName: addressView.streetNameTextfield.text!, localityName: (addressView.localityButton.titleLabel!.text!), landmark: addressView.landmarkTextfield.text!, city: addressView.cityTextfield.text!, state: addressView.stateTextField.text!, pincode: addressView.pincodeTextField.text!, country: addressView.countryTextfield.text!)
                
                    let addressTagViewController = ViewAndUpdateAddressViewController()
                    addressTagViewController.setAddress(addressDetails: address)
                addressTagViewController.setUserAddressId(userAddressId: userAddress.userAddressId)
                navigationController?.pushViewController(addressTagViewController, animated: true)
            }
            else{
                showAlert(message: "The address is unedited")
            }
            
        }
        else{
            showAlert(message: "Please Fill all the fields")
        }
    }
    
    
    
}
