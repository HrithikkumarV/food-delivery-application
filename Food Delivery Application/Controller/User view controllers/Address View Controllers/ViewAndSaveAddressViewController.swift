//
//  AddressTagViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/12/21.
//

import Foundation
import UIKit

class ViewAndSaveAddressViewController: UIViewController, UITextFieldDelegate,AddressTagViewDelegate{
     var  addressTagView : AddressTagViewProtocol!
     var userAddressesViewModel : UserAddressesViewModel = UserAddressesViewModel()
     
    let scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .onDrag
       scrollView.translatesAutoresizingMaskIntoConstraints = false
       scrollView.backgroundColor = .white
        return scrollView
    }()
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
    
     var keyboardUtils : KeyboardUtils!
     var activeArea  : UIView? = nil
     var addressDetails : AddressDetails!
     var addressTag : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialiseView()
        initialiseViewElements()
        addDelegatesForElements()
        addObserver()
    }
    
    
    @objc func didTapBackArrow(){
       navigationController?.popViewController(animated: true)
        
    }
    
     func didTapView(){
        view.endEditing(true)
    }
    
     func didTapHomeTag(){
         addressTagView.homeAddressTagButton.backgroundColor = .systemYellow
         addressTagView.otherAddressTagButton.backgroundColor = .white
         addressTagView.workAddressTagButton.backgroundColor = .white
         addressTagView.otherAddressTagTextField.text = ""
         addressTagView.otherAddressTagTextField.isHidden = true
        addressTag = "Home"
    }
    
     func didTapWorkTag(){
         addressTagView.homeAddressTagButton.backgroundColor = .white
         addressTagView.otherAddressTagButton.backgroundColor = .white
         addressTagView.workAddressTagButton.backgroundColor = .systemYellow
         addressTagView.otherAddressTagTextField.text = ""
         addressTagView.otherAddressTagTextField.isHidden = true
        addressTag = "Work"
    }
    
     func didTapOtherTag(){
         addressTagView.homeAddressTagButton.backgroundColor = .white
         addressTagView.otherAddressTagButton.backgroundColor = .systemYellow
         addressTagView.workAddressTagButton.backgroundColor = .white
         addressTagView.otherAddressTagTextField.text = ""
         addressTagView.otherAddressTagTextField.isHidden = false
        addressTag = "Other"
    }
    
     func didTapSave(){
        
         if(!addressTagView.otherAddressTagTextField.text!.isEmpty){
            addressTag = addressTagView.otherAddressTagTextField.text!
            
        }
        if(addressTag != ""){
            DispatchQueue.global().async { [self] in
                if(userAddressesViewModel.deleteLocalPersistedUserAddress()){
                    if(UserIDUtils.shared.getUserId()  != 0){
                        let userAddressId = UUID().uuidString
                        if(userAddressesViewModel.persistUserAddress(userId: UserIDUtils.shared.getUserId() , userAddressDetails: UserAddressDetails(userAddressId: userAddressId, addressDetails: addressDetails, addressTag: addressTag))){
                            if( userAddressesViewModel.PersistLocalPersistedUserAddress(userAddressDetails: UserAddressDetails(userAddressId: userAddressId, addressDetails: addressDetails, addressTag: addressTag))){
                                DispatchQueue.main.async {
                                    self.dismiss(animated: true)
                                }
                            }
                            else{
                                showAlert(message: "Error while saving, please try again")
                            }
                        }
                        else{
                            showAlert(message: "Error while saving, please try again")
                        }
                       
                        
                        
                    }
                    else{
                        if(userAddressesViewModel.PersistLocalPersistedUserAddress(userAddressDetails: UserAddressDetails(userAddressId: "", addressDetails: addressDetails, addressTag: addressTag))){
                            DispatchQueue.main.async {
                                self.dismiss(animated: true)
                            }
                        }
                        
                    }
                }
                
            }
            
        }
        else{
            showAlert(message: "Please select the address tag")
        }
        
    }
    
    func setAddress(addressDetails : AddressDetails){
        self.addressDetails = addressDetails
    }
    
    func getAddressTag() -> String{
        return addressTag
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {

        keyboardUtils.keyboardWillShow(notification: notification, maxYofLastObjectInView: addressTagView.saveButton.frame.maxY, activeArea: activeArea)
        
       
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardUtils.keyboardWillHide()
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeArea = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeArea = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        didTapView()
        return true
    }
    
    
    
    
     private func initialiseView(){
        title = "Select tag"
         view.backgroundColor = .white
        
        view.addSubview(scrollView)
         addressTagView = AddressTagView()
         addressTagView.translatesAutoresizingMaskIntoConstraints = false
         addressTagView.delegate = self
         scrollView.addSubview(addressTagView)
        NSLayoutConstraint.activate(getScrollViewConstraints())
        NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
     private func initialiseViewElements(){
         addressTagView.addressLabel.text = "\(addressDetails.doorNoAndBuildingNameAndBuildingNo), \(addressDetails.streetName), \(addressDetails.landmark), \(addressDetails.localityName), \(addressDetails.city), \(addressDetails.state), \(addressDetails.country), \(addressDetails.pincode)"
        addressTagView.addressLabel.numberOfLines = 6
        navigationItem.leftBarButtonItem = backArrowButton
    }
    
    
 
     private func addDelegatesForElements(){
         addressTagView.otherAddressTagTextField.delegate = self
    }
    
    
     private func addObserver(){
        keyboardUtils = KeyboardUtils(scrollView: scrollView)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
    }
    
}

extension ViewAndSaveAddressViewController{
    
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                         scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                         scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                         scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [addressTagView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               addressTagView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               addressTagView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               addressTagView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               addressTagView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if(addressTagView.saveButton.frame.maxY != 0){
            addressTagView.removeHeightAnchorConstraints()
            addressTagView.heightAnchor.constraint(equalToConstant: addressTagView.saveButton.frame.maxY + 100).isActive = true
        }
        else{
            addressTagView.removeHeightAnchorConstraints()
            addressTagView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
    }
}
