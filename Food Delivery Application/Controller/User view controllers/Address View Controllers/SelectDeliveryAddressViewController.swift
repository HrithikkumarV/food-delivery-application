//
//  selectDeliveryAddressViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 14/01/22.
//

import Foundation
import UIKit

class SelectDeliveryAddressViewController : UIViewController,SelectDeliveryAddressViewDelegate{
    
     private var selectDeliveryAddressView : SelectDeliveryAddressViewProtocol!
     private var userAddressViewModel : UserAddressesViewModel = UserAddressesViewModel()
     private var userAddresses  : [UserAddressDetails]!
    
    lazy var backArrowButton : UIBarButtonItem = {
       let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
       backArrowButton.action = #selector(didTapBackArrow)
       backArrowButton.target = self
        return backArrowButton
   }()
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addDelegateAndDataSourseForElements()
        getUserAddresses()
    }
    
    
    
     func showOrHideSavedAddressesLabel(){
        if(userAddresses.count > 0){
            selectDeliveryAddressView.savedAddressesLabel.text = "Saved Addresses"
            selectDeliveryAddressView.savedAddressesLabel.textColor = .black
        }
        else{
            selectDeliveryAddressView.savedAddressesLabel.text = "Saved Addresses Will Appear Here"
            selectDeliveryAddressView.savedAddressesLabel.textColor = .systemGray
        }
    }
    
    @objc func didTapBackArrow(){
        dismiss(animated: true)
    }
    
     func didTapSetAddress(){
        let addressViewController =  AddressViewController()
        navigationController?.pushViewController(addressViewController, animated: true)
    }
    
    
     private func initialiseView(){
        view.backgroundColor = .white
         selectDeliveryAddressView = SelectDeliveryAddressView()
         selectDeliveryAddressView.translatesAutoresizingMaskIntoConstraints = false
         selectDeliveryAddressView.delegate = self
        view.addSubview(selectDeliveryAddressView)
        NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
     private func initialiseViewElements(){
         
       
         
        navigationItem.leftBarButtonItem = backArrowButton
         
    }
    
   
     private func addDelegateAndDataSourseForElements(){
         selectDeliveryAddressView.savedAddressesTableView.delegate = self
         selectDeliveryAddressView.savedAddressesTableView.dataSource = self
    }
    
     func getUserAddresses(){
        
         DispatchQueue.global().async { [self] in
             userAddresses = userAddressViewModel.getUserAddresses(userId: (UserIDUtils.shared.getUserId()))
             DispatchQueue.main.async {
                 self.showOrHideSavedAddressesLabel()
             }
         }
        
    }
}


extension SelectDeliveryAddressViewController :  UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userAddresses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return selectDeliveryAddressView.createAddressTableViewCell(indexPath: indexPath, userAddress: userAddresses[indexPath.row])
    }
    
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(userAddressViewModel.deleteLocalPersistedUserAddress()){
            if(userAddressViewModel.PersistLocalPersistedUserAddress(userAddressDetails: UserAddressDetails(userAddressId: userAddresses[indexPath.row].userAddressId, addressDetails: userAddresses[indexPath.row].addressDetails, addressTag: userAddresses[indexPath.row].addressTag))){
                dismiss(animated: true)
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return selectDeliveryAddressView.getAddressTableViewCellHeight()
        
    }
    
}

extension SelectDeliveryAddressViewController{
    
   
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let contentViewConstraints = [selectDeliveryAddressView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor),
                                      selectDeliveryAddressView.bottomAnchor.constraint(equalTo : view.safeAreaLayoutGuide.bottomAnchor),
                                      selectDeliveryAddressView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                                      selectDeliveryAddressView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)]
            return contentViewConstraints
    }
}
    
