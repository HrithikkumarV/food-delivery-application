//
//  UserCartViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 03/03/22.
//

import Foundation
import UIKit

class UserCartViewController : UIViewController, UITextFieldDelegate,cartBottomViewDelegate,UserCartViewDelegate{
    
     private var userCartView : UserCartViewProtocol!
     private var userCartViewModel : UserCartViewModel = UserCartViewModel.getUserCartViewModel()!
     private var userAddressesViewModel : UserAddressesViewModel = UserAddressesViewModel()
     private var userOrderViewModel : UserOrdersViewModel = UserOrdersViewModel()
     private var deliveryAddress : UserAddressDetails = UserAddressDetails()
     private var toPayAmount : Int = 0
     private var restaurantContentDetails : RestaurantContentDetails = RestaurantContentDetails()
     private var menuDetailsInCart : [Int : CartMenuDetails] = [:]
     private var menuDetailsOrderedKey : [Int] = []
     private var menuCountLabels : [Int : UILabel] = [:]
     private var addMenuButtonViews : [Int : UIView] = [:]
     private var menuSubTotalLabels : [Int : UILabel] = [:]
     private var menuIdSubTotalPricePair : [Int : Int] = [:]
    
     private var cartMenuDetailsTableViewHeightConstraint : NSLayoutConstraint?
     private var instructionToRestaurantPlaceHolderTextViewDelegate : TextViewPlaceHolderUtils!
     private var itemDiscountPrice : Int = 0
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.target = self
        backArrowButton.action = #selector(didTapBackArrow)
        return backArrowButton
    }()
      var scrollView : UserCartScrollViewProtocol!
    var cartBottomView  : CartBottomViewProtocol!
     private var unavailableMenuItems : [Int : CartMenuDetails] = [:]
     private var instructionToRestaurant : String = ""
    override func viewDidLoad(){
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        getDataFromDBAndUpdateView()
        addDelegatesAndDataSourceForElements()
    }
    
    
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        modifyBasedOnDeliveryAddress()
        modifyViewBasedOnUserLogin()
        modifyViewBasedOnMenuAvailablity()
        modifyViewBaseOnRestaurantAvailablity()
    }
    
    func sendNotificationToTabBarToMoveToFirstVC(){
        NotificationCenter.default.post(name: Notification.Name(moveToFirstVcInTabBar.moveToFirstVc.rawValue), object: nil)
    }
    
     func modifyViewBaseOnRestaurantAvailablity(){
        if(restaurantContentDetails.restaurantIsAvailable == 0){
            cartBottomView.browseRestaurantsButton.isHidden = false
            cartBottomView.restaurantUnavailableLabel.isHidden = false
            
        }
        else{
            cartBottomView.browseRestaurantsButton.isHidden = true
            cartBottomView.restaurantUnavailableLabel.isHidden = true
        }
    }
     func modifyViewBasedOnMenuAvailablity(){
        for menuId in menuDetailsInCart.keys{
            if menuDetailsInCart[menuId]?.menuIsAvailable == 0 {
                unavailableMenuItems[menuId] = menuDetailsInCart[menuId]
            }
        }
        
        if unavailableMenuItems.count > 0{
            cartBottomView.menuItemUnavailableLabel.isHidden = false
            cartBottomView.removeUnavailableItemsButton.isHidden = false
        }
        else{
            cartBottomView.menuItemUnavailableLabel.isHidden = true
            cartBottomView.removeUnavailableItemsButton.isHidden = true
        }
    }
    
     func modifyViewBasedOnUserLogin(){
        if(UserIDUtils.shared.getUserId()  != 0){
            cartBottomView.loginButton.isHidden = true
        }
        else{
            cartBottomView.loginButton.isHidden = false
        }
    }
    
    func modifyBasedOnDeliveryAddress(){
        DispatchQueue.global().async { [self] in
            deliveryAddress = userAddressesViewModel.getLocalPersistedUserAddress()
            DispatchQueue.main.async { [self] in
                cartBottomView.updateAddressDetailsInLabel(addressTag: deliveryAddress.addressTag, address: deliveryAddress.addressDetails)
                if(restaurantContentDetails.restaurantAddress.localityName == deliveryAddress.addressDetails.localityName && restaurantContentDetails.restaurantAddress.pincode == deliveryAddress.addressDetails.pincode){
                    cartBottomView.notDeliverableLabel.isHidden = true
                    
                }
                else{
                    cartBottomView.notDeliverableLabel.isHidden = false
                }
            }
        }
        
    }
    
    func didTapLoginButton(){
        let login = UINavigationController(rootViewController: UserLoginViewController())
        login.modalPresentationStyle = .fullScreen
        present(login, animated: true, completion: nil)
    }
    
    func didTapRemoveUnAvailableItems(){
        for menuid in unavailableMenuItems.keys{
            UserCartViewModel.totalPrice -= (Int(menuDetailsInCart[menuid]?.price ?? 0) * menuDetailsInCart[menuid]!.quantity)
            UserCartViewModel.cartItemsCount -= menuDetailsInCart[menuid]!.quantity
            menuDetailsInCart[menuid]?.quantity =  0
        if(userCartViewModel.updateMenuInCartAndSendNotificationTomoveToFullMenuView(menuId: menuid, quantity: menuDetailsInCart[menuid]?.quantity ?? 0)){
            updateMenuCountAndSubTotalabels(menuId: menuid)
            updatePriceLabels()
        }
            removeItem(menuId : menuid)
        }
        cartBottomView.menuItemUnavailableLabel.isHidden = true
        cartBottomView.removeUnavailableItemsButton.isHidden = true
    }
    
    
     func removeItem(menuId : Int){
         DispatchQueue.global().async { [self] in
             if(userCartViewModel.removeMenuFromCart(menuId: menuId)){
                 DispatchQueue.main.async { [self] in
                     let indexPath = IndexPath(item: menuDetailsOrderedKey.firstIndex(of: menuId)!, section: 0)
                     menuDetailsOrderedKey.remove(at: menuDetailsOrderedKey.firstIndex(of: menuId)!)
                     userCartView.menuDetailsTableView.deleteRows(at: [indexPath], with: .none)
                     addMenuButtonViews.removeValue(forKey: menuId)
                     menuCountLabels.removeValue(forKey: menuId)
                     menuDetailsInCart.removeValue(forKey: menuId)
                     updateCartTableViewHeight()
                     didTapApplyCouponCode()
                     DispatchQueue.global().async { [self] in
                         if(UserCartViewModel.cartItemsCount == 0){
                             if(userCartViewModel.deleteCouponCode()){
                                 DispatchQueue.main.async { [self] in
                                     userCartView.isHidden = true
                                     navigationItem.titleView?.isHidden = true
                                     scrollView.isScrollEnabled = false
                                     scrollView.emptyCartLabel.isHidden = false
                                     cartBottomView.browseRestaurantsButton.isHidden = false
                                     cartBottomView.deliveryAddressView.isHidden = true
                                 }
                             }
                         }
                     }
                 }
             }
         }
       
    }
    
    func didTabBrowseRestaurant(){
        DispatchQueue.global().async {
            if(self.userCartViewModel.clearCart()){
                DispatchQueue.main.async { [self] in
                    UserCartViewModel.totalPrice = 0
                    UserCartViewModel.cartItemsCount = 0
                    UserCartViewModel.restaurantNameInCart = ""
                    sendNotificationToTabBarToMoveToFirstVC()
                    navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
    
    
    func didTapPlaceOrderButton(){
        
        DispatchQueue.global().async { [self] in
            if(deliveryAddress.userAddressId == ""){
                persistDeliveryAddressAndGetUserAddressId()
            }
            if(persistOrderDetails()){
                DispatchQueue.main.async { [self] in
                    clearCart()
                    exitPageWithAToast()
                }
            }
        }
    }
    
    func getInstructionToRestaurant(){
        instructionToRestaurant = userCartView.instructionsToRestaurantTextView.text
    }
    
     func clearCart(){
         DispatchQueue.global().async { [self] in
             if(userCartViewModel.clearCart()){
                 UserCartViewModel.totalPrice = 0
                 UserCartViewModel.cartItemsCount = 0
                 UserCartViewModel.cartItems = [:]
                 if(userCartViewModel.deleteCouponCode()){
                     DispatchQueue.main.async { [self] in
                         sendNotificationToTabBarControllerhideOrShowmoveToCartViewBasedOnMenuCount()
                         sendNotificationToTabBarToMoveToFirstVC()
                     }
                 }
                 
             }
         }
       
        
    }
     func exitPageWithAToast(){
        self.showToast(message: "Order Placed Succesfully", font: UIFont.systemFont(ofSize: 20, weight: .medium), fontColor: .white, backGroundColor: .systemGreen.withAlphaComponent(0.9))
        navigationController?.popToRootViewController(animated: true)
    }
    
     func persistOrderDetails() -> Bool{
        let paymentId = UUID().uuidString
        let orderDateAndTime = getDateAndTime()
        let orderId = UUID().uuidString
         if(userOrderViewModel.persistPaymentDetails(paymentDetails: PaymentDetails(paymentId: paymentId, paymentMode: .COD, paymentAmount: toPayAmount, paymentReferanceNumber: "", paymentStatus: "", bankName: ""))){
             if(userOrderViewModel.persistOrderDetails(orderDetails: OrderModel(orderId: orderId, restaurantId: restaurantContentDetails.restaurantId, userId: (UserIDUtils.shared.getUserId() ), paymentId: paymentId, dateAndTime: orderDateAndTime, userAddressId: deliveryAddress.userAddressId))){
                 if(userOrderViewModel.persistBillDetails(billDetails: BillDetailsModel(orderId: orderId, itemTotal: UserCartViewModel.totalPrice, deliveryFee: Delivery.getDefaultDeliveryFee(), restaurantGST: RestaurantGST.getGSTCost(totalPrice: UserCartViewModel.totalPrice), restaurantPackagingCharges: restaurantContentDetails.restaurantFoodPackagingCharges, itemDiscount: itemDiscountPrice, totalCost: toPayAmount))){
                     if(userOrderViewModel.persistInstructionToRestaurantDetails(instructionToRestaurant: "Instruction From Customer : " + ( instructionToRestaurant != "Write instructions to restaurant" ? instructionToRestaurant : ""), orderId: orderId)){
                         if(userOrderViewModel.persistTackOrderDetails(orderStatus: .Pending, orderId: orderId)){
                             for menuId in menuDetailsOrderedKey{
                                 if(userOrderViewModel.persistMenuToOrderedFoodDetails(menuId: menuDetailsInCart[menuId]?.menuId ?? 0, orderId: orderId , quantity: menuDetailsInCart[menuId]?.quantity ?? 0)){
                                     
                                 }
                             }
                             if(userOrderViewModel.createFoodratingRow(orderId: orderId)){
                                 return true
                             }
                         }
                     }
                 }
             }
         }
        
        return false
         
    }
    
    
    
     func persistDeliveryAddressAndGetUserAddressId(){
        let userAddressId = UUID().uuidString
         if(userAddressesViewModel.persistUserAddress(userId: (UserIDUtils.shared.getUserId()), userAddressDetails: UserAddressDetails(userAddressId: userAddressId, addressDetails: deliveryAddress.addressDetails, addressTag: deliveryAddress.addressTag))){
             if(userAddressesViewModel.updateUserAddressIdInLocalPersistedUserAddress(userAddressId: userAddressId)){
                 deliveryAddress.userAddressId = userAddressId
             }
         }
        
    }
    
    
    
    func getDateAndTime()  -> String{
        let date = DateFormatter()
        date.dateFormat = "HH:mm E, d MMM y"
        return date.string(from: Date())
    }
    
    func sendNotificationToTabBarControllerhideOrShowmoveToCartViewBasedOnMenuCount(){
        NotificationCenter.default.post(name: Notification.Name(moveToViewNotificationNames.hideOrShowmoveToViewBasedOnMenuCount.rawValue), object: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        instructionToRestaurantPlaceHolderTextViewDelegate.addLabelToTopBorder(frame: userCartView.instructionsToRestaurantTextView.frame)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == userCartView.applyCouponTextField){
            didTapApplyCouponCode()
        }
        return true
    }
    
     func didTapAddMoreMenu(){
        navigationController?.popViewController(animated: true)
    }
    
     func didTapApplyCouponCode(){
        showActivityIndicator()
         let couponCodeText  = userCartView.applyCouponTextField.text!
        DispatchQueue.global().async { [self] in
            userCartViewModel.getCouponCode(couponCode:couponCodeText , completion: { [weak self]couponCode,discountPercentage,maximumDiscountAmount in
                self?.hideActivityIndicator()
                if(maximumDiscountAmount != 0 ){
                    if(self!.userCartViewModel.deleteCouponCode()){
                        if(self!.userCartViewModel.persistCouponCode(couponCode: couponCode!)){
                            DispatchQueue.main.async { [self] in
                                self?.applyDiscount(discountPercentage: discountPercentage, maximumDiscountAmmount: maximumDiscountAmount)
                                self?.updatePriceLabels()
                                self?.userCartView.applyCouponTextField.isUserInteractionEnabled = false
                                self?.userCartView.updateCouponAppliedLabel(couponCode: couponCode!, itemDiscountPrice: self!.itemDiscountPrice)
                                self?.userCartView.removeCouponButton.isHidden = false
                            }
                        }
                    }
                }
                
            })
        }
        
    }
    
    
    
    
    func updateCartTableViewHeight(){
        cartMenuDetailsTableViewHeightConstraint?.isActive = false
        cartMenuDetailsTableViewHeightConstraint = userCartView.menuDetailsTableView.bottomAnchor.constraint(equalTo: userCartView.menuDetailsTableView.topAnchor, constant:  userCartView.menuDetailsTableView.contentSize.height)
        cartMenuDetailsTableViewHeightConstraint?.isActive = true
    }
    func applyDiscount(discountPercentage : Int ,maximumDiscountAmmount : Int){
        var discountAmount = (UserCartViewModel.totalPrice * discountPercentage)/100
        if(discountAmount > maximumDiscountAmmount){
            discountAmount = maximumDiscountAmmount
        }
        itemDiscountPrice = discountAmount
        
    }
    
     func didTapRemoveCoupon(){
        DispatchQueue.global().async { [self] in
            if(userCartViewModel.deleteCouponCode()){
                DispatchQueue.main.async { [self] in
                    itemDiscountPrice = 0
                    userCartView.updateCouponAppliedLabel(couponCode: "", itemDiscountPrice: itemDiscountPrice)
                    updatePriceLabels()
                    userCartView.removeCouponButton.isHidden = true
                    userCartView.applyCouponTextField.text = ""
                    userCartView.applyCouponTextField.hideTopBorderLabel()
                    userCartView.applyCouponTextField.isUserInteractionEnabled = true
                }
            }
        }
       
    }
    
    
    
     func didTapView(){
        self.view.endEditing(true)
    }
    
     func didTapChangeAddress(){
        let navigateToSelectAddressVC = UINavigationController(rootViewController:SelectDeliveryAddressViewController())
        navigateToSelectAddressVC.modalPresentationStyle = .fullScreen
        present(navigateToSelectAddressVC, animated: true, completion: nil)
    }
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
     private func initialiseView(){
         view.backgroundColor = .white
         scrollView = UserCartScrollView()
         scrollView.translatesAutoresizingMaskIntoConstraints = false
         view.addSubview(scrollView)
         userCartView = UserCartView()
         userCartView.translatesAutoresizingMaskIntoConstraints = false
         userCartView.delegate = self
         scrollView.addSubview(userCartView)
         cartBottomView = userCartView.cartBottomView
         cartBottomView.translatesAutoresizingMaskIntoConstraints = false
         cartBottomView.delegate = self
         view.addSubview(cartBottomView)
         
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())
         NSLayoutConstraint.activate(getCartBottomViewLayoutConstraints())
    }
    
    
    
       
    
     private func initialiseViewElements(){
        scrollView.emptyCartLabel.isHidden = true
        userCartView.removeCouponButton.isHidden = true
        cartBottomView.updatePaymentMode(paymentMode: "COD")
        navigationItem.leftBarButtonItem = backArrowButton
        self.setDefaultNavigationBarApprearance()
        
    }
    
   
    
    private func getDataFromDBAndUpdateView(){
        DispatchQueue.global().async { [self] in
            restaurantContentDetails = userCartViewModel.getRestaurantDetailsFromCartMenu()
            let restaurantDetails = restaurantContentDetails.restaurantDetails
            let menuDetailsInCartFromDb = userCartViewModel.getMenuDetailsOfCart()
            menuDetailsInCart = menuDetailsInCartFromDb.menuDetailsInCartInDictionaryFormat
            menuDetailsOrderedKey = menuDetailsInCartFromDb.menuDetailsOrderedKey
           let couponCode = userCartViewModel.getCouponCode()
            DispatchQueue.main.async { [self] in
                userCartView.updateRestaurantDetailsContents(restaurantProfileImage: restaurantDetails.restaurantProfileImage, restaurantName: restaurantDetails.restaurantName, restaurantLocality: restaurantContentDetails.restaurantAddress.localityName)
                if(couponCode != ""){
                    userCartView.applyCouponTextField.text = couponCode
                    userCartView.removeCouponButton.isHidden = false
                    didTapApplyCouponCode()
                }
                 updatePriceLabels()
                viewWillAppear(true)
            }
        }
      
    }
    
    private func updatePriceLabels(){
        userCartView.updateBillDetails(itemTotalPrice: UserCartViewModel.totalPrice , restaurantPackagingChargePrice: restaurantContentDetails.restaurantFoodPackagingCharges, deliveryFeePrice: Delivery.getDefaultDeliveryFee(), itemDiscountPrice: itemDiscountPrice)
        toPayAmount = userCartView.getTopayAmount(itemTotalPrice: UserCartViewModel.totalPrice , restaurantPackagingChargePrice: restaurantContentDetails.restaurantFoodPackagingCharges, deliveryFeePrice: Delivery.getDefaultDeliveryFee(), itemDiscountPrice: itemDiscountPrice)
        userCartView.addNavigationBarTitleView(restaurantName: UserCartViewModel.restaurantNameInCart, menuCount: UserCartViewModel.cartItemsCount, totalPrice: toPayAmount)
        navigationItem.titleView = userCartView.navigationBarTitleView
        
    }
    
     
     private func addDelegatesAndDataSourceForElements(){
         userCartView.menuDetailsTableView.delegate = self
         userCartView.menuDetailsTableView.dataSource = self
         userCartView.applyCouponTextField.delegate =  self
         instructionToRestaurantPlaceHolderTextViewDelegate = TextViewPlaceHolderUtils(targetTextView: userCartView.instructionsToRestaurantTextView, placeHolderText: "Write instructions to restaurant", descriptionLabelText: "Instructions to Restaurant", fontSizeForLabel: 15, view: userCartView, placeHolderColor: .systemGray3)
         userCartView.instructionsToRestaurantTextView.delegate = instructionToRestaurantPlaceHolderTextViewDelegate
    }
   
}

    


extension UserCartViewController : UITableViewDelegate, UITableViewDataSource,CartMenuDetailsTableViewCellDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("count", menuDetailsInCart.count)
        return menuDetailsOrderedKey.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return userCartView.getCartMenuCellHeight()
         
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menuId = menuDetailsOrderedKey[indexPath.row]
        let  cell = userCartView.createMenuDetailsContentView(indexPath: indexPath, tableView: tableView, menuCellContents: (menuName: menuDetailsInCart[menuId]!.menuName, menuTarianType: menuDetailsInCart[menuId]!.menuTarianType, menuAvailable: menuDetailsInCart[menuId]!.menuIsAvailable, menuAvailableNextAt: menuDetailsInCart[menuId]!.menuNextAvailableAt,menuId : menuId))
        modifyAddMenuButtonAndViewBasedOnRestaurantAvailablity(plusButton: cell.plusButton, minusButton: cell.minusButton, menuCountLabel: cell.menuCountLabel, addMenuButtonView: cell.addMenuButtonView, menuId: menuId)
        menuCountLabels[menuId] = cell.menuCountLabel
        addMenuButtonViews[menuId] = cell.addMenuButtonView
        menuSubTotalLabels[menuId] = cell.menuSubTotalPriceCellLabel
        cell.delegate = self
        updateMenuCountAndSubTotalabels(menuId: menuId)
        return cell

    }
    
    
    
    func updateMenuCountAndSubTotalabels(menuId : Int){
        updateMenuCountLabel(menuId: menuId)
        updateMenuSubTotalLabel(menuId: menuId)
    }
    
    
    func modifyAddMenuButtonAndViewBasedOnRestaurantAvailablity( plusButton: UIButton, minusButton: UIButton, menuCountLabel: UILabel,addMenuButtonView : UIView,menuId  : Int){
        if(restaurantContentDetails.restaurantIsAvailable == 0){
            plusButton.isHidden = true
            minusButton.isHidden = true
            menuCountLabel.isHidden = true
            addMenuButtonView.isHidden = true
            
        }
    }
    
    
   
   
    func updateMenuCountLabel(menuId : Int){
        menuCountLabels[menuId]?.text =   String((menuDetailsInCart[menuId]?.quantity ?? 0))
    }
    
    
    
    
    func updateMenuSubTotalLabel(menuId : Int){
        menuSubTotalLabels[menuId]?.text =   "₹\((menuDetailsInCart[menuId]?.quantity ?? 0) * (menuDetailsInCart[menuId]?.price ?? 0 ))"
    }
    
    
    
     func didTapPlusButton(sender : UIButton){
         let menuId = sender.tag
        UserCartViewModel.totalPrice += Int(menuDetailsInCart[menuId]?.price ?? 0)
        UserCartViewModel.cartItemsCount += 1
        menuDetailsInCart[menuId]?.quantity +=  1
         DispatchQueue.global().async { [self] in
             if(userCartViewModel.updateMenuInCartAndSendNotificationTomoveToFullMenuView(menuId: menuId, quantity: menuDetailsInCart[menuId]?.quantity ?? 0)){
                 DispatchQueue.main.async { [self] in
                     updateMenuCountAndSubTotalabels(menuId: menuId)
                     updatePriceLabels()
                 }
             }
         }
        
    }
    
     func didTapMinusButton(sender : UIButton){
         let menuId = sender.tag
        UserCartViewModel.totalPrice -= Int(menuDetailsInCart[menuId]?.price ?? 0)
        UserCartViewModel.cartItemsCount -= 1
        menuDetailsInCart[menuId]?.quantity -=  1
         DispatchQueue.global().async { [self] in
             if(userCartViewModel.updateMenuInCartAndSendNotificationTomoveToFullMenuView(menuId: menuId, quantity: menuDetailsInCart[menuId]?.quantity ?? 0)){
                 DispatchQueue.main.async { [self] in
                     updateMenuCountAndSubTotalabels(menuId: menuId)
                     updatePriceLabels()
                     if(menuDetailsInCart[menuId]?.quantity == 0){
                         removeItem(menuId: menuId)
                     }
                 }
             }
             
             
         }
    }
    
}

extension UserCartViewController{
    
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                         scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                         scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                         scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor,constant: -120)]
            return scrollViewConstraints
        }
    
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [userCartView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               userCartView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               userCartView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               userCartView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               userCartView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if(userCartView.cancelOrderInstructionsLabel.frame.maxY > 0){
            userCartView.removeHeightAnchorConstraints()
            userCartView.heightAnchor.constraint(equalToConstant:  userCartView.cancelOrderInstructionsLabel.frame.maxY + 100).isActive = true
        }
        else{
            userCartView.removeHeightAnchorConstraints()
            userCartView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
    }
    
    func getCartBottomViewLayoutConstraints() -> [NSLayoutConstraint]{
        let viewContraints = [
            cartBottomView.bottomAnchor.constraint(equalTo: self.view
                .safeAreaLayoutGuide.bottomAnchor),
            cartBottomView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
            cartBottomView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
            cartBottomView.heightAnchor.constraint(equalToConstant: 120)]
        return viewContraints
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        
        updateCartTableViewHeight()
        
    }
    
}
