//
//  searchMenuInDisplayedMenuViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 16/02/22.
//

import Foundation
import UIKit


class SearchMenuInDisplayedUserMenuDetailsViewController : UIViewController,  UISearchBarDelegate,SearchMenuInDisplayedUserMenuDetailsViewDelegate{
     private var searchMenuInDisplayedMenuDetailsView : SearchMenuInDisplayedUserMenuDetailsViewProtocol!
     private var userCartViewModel : UserCartViewModel = UserCartViewModel.getUserCartViewModel()!
     private var menuDetailsOfRestaurant : [Int : MenuContentDetails] = [:]
     private var orderedMenuKeys : [Int] = []
     private var orderedSearchMenusKeys : [Int] = []
     private var menuPricePair : [Int : Int] = [:]
     private var menuItemsInCart : [Int:Int] = [:]
     private var restaurantContentDetails :RestaurantContentDetails!
     private var searchResultMenus : [Int : MenuContentDetails] = [:]
     private var menuCountLabels : [Int :UILabel] = [:]
     private var addMenuButtonViews : [Int : UIView] = [:]
    
    private var searchMenuBar : UISearchBar = {
        let searchMenuBar = UISearchBar()
        searchMenuBar.placeholder = "Enter dish name"
        searchMenuBar.backgroundColor = .white
        searchMenuBar.keyboardType = .default
        searchMenuBar.returnKeyType = .search
        return searchMenuBar
        
    }()
    
    private lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
    
    override func viewDidLoad() {
        initialiseView()
        initialiseViewElements()
        getDataFromDBAndUpdateUI()
        addDelegatesForElements()
        
    }
    
    
    
    @objc func didTapView(){
        searchMenuBar.resignFirstResponder()
    }
    
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        getMenusWithCharactersLikeInSearchBar(menuName : searchText)
        searchMenuInDisplayedMenuDetailsView.menuTableView.reloadData()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    
    
     func  getMenusWithCharactersLikeInSearchBar(menuName : String){
        searchResultMenus = [:]
        orderedSearchMenusKeys = []
        for menukey in orderedMenuKeys{
            let menu = menuDetailsOfRestaurant[menukey]!
            if menu.menuDetails.menuName.contains(menuName){
                searchResultMenus[menukey] = menu
                orderedSearchMenusKeys.append(menukey)
            }
        }
    }
     
    
    
    func setMenuDetailsOfRestaurant(menuDetailsOfRestaurant : [Int : MenuContentDetails],orderedMenuDetailsDisplayedKeys : [Int],menuPricePair : [Int : Int]){
        self.menuDetailsOfRestaurant = menuDetailsOfRestaurant
        self.orderedMenuKeys = orderedMenuDetailsDisplayedKeys
        self.menuPricePair = menuPricePair
    }
    
    func setRestaurantDetails(restaurantContentDetails :  RestaurantContentDetails){
        self.restaurantContentDetails = restaurantContentDetails
    }

    
     func didTapAddMenu(sender : UIButton){
         let menuId = sender.tag
        if(UserCartViewModel.restaurantNameInCart != "" && UserCartViewModel.restaurantNameInCart != restaurantContentDetails.restaurantDetails.restaurantName){
            showAlertToClearCart(sender: sender)
        }
        else{
            DispatchQueue.global(qos: .userInteractive).async { [self] in
                if(userCartViewModel.addMenuToCart(cartDetails: (menuId: menuId, restaurantId: restaurantContentDetails.restaurantId , quantity: 0 ))){
                    DispatchQueue.main.async { [self] in
                        addMenuButtonViews[menuId]!.isHidden = false
                        didTapPlusButton(sender: sender)
                    }
                }
            }
        }
    }
    
     func didTapPlusButton(sender : UIButton){
         let menuId = sender.tag
         UserCartViewModel.totalPrice += menuPricePair[menuId] ?? 0
         UserCartViewModel.cartItemsCount += 1
         menuItemsInCart[menuId]  = (menuItemsInCart[menuId] ?? 0) + 1
         DispatchQueue.global(qos: .userInteractive).async { [self] in
             if(userCartViewModel.updateMenuInCartAndSendNotificationTomoveToFullMenuView(menuId: menuId, quantity: (menuItemsInCart[menuId] ?? 0))){
                 DispatchQueue.main.async { [self] in
                     menuCountLabels[menuId]!.text = String(Int(menuCountLabels[menuId]!.text!)! + 1)
                 }
             }
         }
        
    }
    
     func didTapMinusButton(sender : UIButton){
         let menuId = sender.tag
         UserCartViewModel.totalPrice -= menuPricePair[menuId] ?? 0
         UserCartViewModel.cartItemsCount -= 1
         menuItemsInCart[menuId]  = (menuItemsInCart[menuId] ?? 0) - 1
         DispatchQueue.global(qos: .userInteractive).async { [self] in
             if(userCartViewModel.updateMenuInCartAndSendNotificationTomoveToFullMenuView(menuId: menuId, quantity: (menuItemsInCart[menuId] ?? 0))){
                 DispatchQueue.main.async { [self] in
                     
                     menuCountLabels[menuId]!.text = String(Int(menuCountLabels[menuId]!.text!)! - 1)
                     if(menuCountLabels[menuId]!.text! == "0"){
                         DispatchQueue.global(qos: .userInteractive).async {
                             if(self.userCartViewModel.removeMenuFromCart(menuId: menuId)){
                                 DispatchQueue.main.async { [self] in
                                     addMenuButtonViews[sender.tag]!.isHidden = true
                                     menuItemsInCart.removeValue(forKey: menuId)
                                 }
                             }
                         }
                     }
                 }
             }
         }
        
        
    
        
    }
    
    
     func  showAlertToClearCart(sender : UIButton){
         let alert = UIAlertController(title: "Replace Cart Item?", message: "Your cart contains dishes from \(UserCartViewModel.restaurantNameInCart). Do you want to discard the selection and add dishes from \(restaurantContentDetails.restaurantDetails.restaurantName)?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .cancel,handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default,handler: {_ in
            DispatchQueue.global().async { [self] in
                if(self.userCartViewModel.clearCart()){
                    DispatchQueue.main.async { [self] in
                        UserCartViewModel.totalPrice = 0
                        UserCartViewModel.cartItemsCount = 0
                        UserCartViewModel.restaurantNameInCart = ""
                        self.didTapAddMenu(sender: sender)
                    }
                }
            }
        }))
        present(alert,animated: true)
    }
    
    private func initialiseView(){
        view.backgroundColor = .white
        searchMenuInDisplayedMenuDetailsView = SearchMenuInDisplayedUserMenuDetailsView()
        searchMenuInDisplayedMenuDetailsView.translatesAutoresizingMaskIntoConstraints = false
        searchMenuInDisplayedMenuDetailsView.delegate = self
       view.addSubview(searchMenuInDisplayedMenuDetailsView)
        NSLayoutConstraint.activate(getContentViewConstraints())
   }
   
    private func initialiseViewElements(){
       searchMenuBar.becomeFirstResponder()
       navigationItem.titleView = searchMenuBar
      
       addMoveToCartViewNotificationObservers()
       
       navigationItem.leftBarButtonItem = backArrowButton
   }
    private func getDataFromDBAndUpdateUI(){
        DispatchQueue.global().async { [self] in
            menuItemsInCart = userCartViewModel.getCartMenuItems()
            DispatchQueue.main.async { [self] in
                initMoveToCartView()
            }
        }
    }
   
    private func addDelegatesForElements(){
       searchMenuBar.delegate = self
        searchMenuInDisplayedMenuDetailsView.menuTableView.delegate = self
        searchMenuInDisplayedMenuDetailsView.menuTableView.dataSource = self
   }
   
   
   
    
}
extension SearchMenuInDisplayedUserMenuDetailsViewController : UITableViewDelegate , UITableViewDataSource,MenuTableViewCellDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return searchResultMenus.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let menuContentDetails =  searchResultMenus[orderedSearchMenusKeys[indexPath.row]]!
        let menuDetails = menuContentDetails.menuDetails
        
       
                        let cell = searchMenuInDisplayedMenuDetailsView.createMenuTableViewCell(indexPath: indexPath, tableView: tableView, menuCellContents: (menuImage: menuDetails.menuImage, menuName: menuDetails.menuName, menuDescription: menuDetails.menuDescription, menuPrice: menuDetails.menuPrice, menuTarianType: menuDetails.menuTarianType,menuAvailable : menuContentDetails.menuIsAvailable
                                                                                                                                                               ,menuAvailableNextAt: menuContentDetails.menuNextAvailableAt, menuId : menuContentDetails.menuId))
        cell.delegate = self
        menuCountLabels[menuContentDetails.menuId] = cell.menuCountLabel
        addMenuButtonViews[menuContentDetails.menuId] = cell.addMenuButtonView
        modifyAddMenuButtonAndViewBasedOnRestaurantAvailablityAndDeliverable(addMenuButton: cell.addMenuCellButton, plusButton: cell.plusButton, minusButton: cell.minusButton, menuCountLabel: cell.menuCountLabel, addMenuButtonView: cell.addMenuButtonView)
        updateMenuCountLabelsBasedOnCartItems(menuId: menuContentDetails.menuId)
                        return cell
        }
                
       

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        searchMenuInDisplayedMenuDetailsView.getMenuTabelViewCellHeight()
    }
    
    func updateMenuCountLabelsBasedOnCartItems(menuId : Int){
        if(menuItemsInCart[menuId] != nil){
            addMenuButtonViews[menuId]!.isHidden = false
            menuCountLabels[menuId]!.text! = String(menuItemsInCart[menuId]!)
        }
    }
    
    func modifyAddMenuButtonAndViewBasedOnRestaurantAvailablityAndDeliverable(addMenuButton: UIButton, plusButton: UIButton, minusButton: UIButton, menuCountLabel: UILabel,addMenuButtonView : UIView){
        if(restaurantContentDetails.restaurantIsAvailable == 0 || !restaurantContentDetails.isDeliverable){
            addMenuButton.isHidden = true
            plusButton.isHidden = true
            minusButton.isHidden = true
            menuCountLabel.isHidden = true
            addMenuButtonView.isHidden = true
        }
    }
    
    

    
    
    
    
}

extension SearchMenuInDisplayedUserMenuDetailsViewController :MoveToCartViewDelegate{
    func initMoveToCartView(){
        searchMenuInDisplayedMenuDetailsView.moveToCartView.delegate = self
        updateMenuCountAndTotalPriceInmoveToCartView()
        hideOrShowmoveToCartViewBasedOnMenuCount()
        updateNameOfRestaurantInCart()
    }
    
    
    
    func didTapMoveToCart(){
        if(UserCartViewModel.restaurantNameInCart == restaurantContentDetails.restaurantDetails.restaurantName){
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: UserMenuDisplayPageViewController.self) {
                    navigationController?.popViewController(animated: false)
                    controller.navigationController?.pushViewController(UserCartViewController(), animated: true)
                    break
                }
            }
        }
        else{
            let userMenuDisplayPageViewController = UserMenuDisplayPageViewController()
            var restaurantDetails = userCartViewModel.getRestaurantDetailsFromCartMenu()
            restaurantDetails.isDeliverable =  restaurantContentDetails.isDeliverable
            userMenuDisplayPageViewController.setRestaurantContentDetails(restaurantContentDetails: restaurantDetails)
            navigationController?.pushViewController(userMenuDisplayPageViewController, animated: false)
            navigationController?.pushViewController(UserCartViewController(), animated: true)
        }
        
        
    }
    
    
    func addMoveToCartViewNotificationObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateMenuCountAndTotalPriceInmoveToCartView), name: Notification.Name(moveToViewNotificationNames.updateMenuCountAndPriceInmoveToView.rawValue), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideOrShowmoveToCartViewBasedOnMenuCount), name: Notification.Name(moveToViewNotificationNames.hideOrShowmoveToViewBasedOnMenuCount.rawValue), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateNameOfRestaurantInCart), name: Notification.Name(moveToViewNotificationNames.updateNameOfRestaurantInCart.rawValue), object: nil)
        
        
    }
    
    @objc func updateMenuCountAndTotalPriceInmoveToCartView(){
        searchMenuInDisplayedMenuDetailsView.moveToCartView.updateMenuItemCountAndPriceLabelText(menuCount: UserCartViewModel.cartItemsCount, totalPrice: UserCartViewModel.totalPrice)
    }
    
    @objc func hideOrShowmoveToCartViewBasedOnMenuCount(){
        searchMenuInDisplayedMenuDetailsView.moveToCartView.hideOrShowmoveToCartViewBasedOnMenuCount(menuCount: UserCartViewModel.cartItemsCount)
    }
    
    @objc func updateNameOfRestaurantInCart(){
        if(UserCartViewModel.cartItemsCount == 0){
            UserCartViewModel.restaurantNameInCart = ""
        }
        else if(UserCartViewModel.cartItemsCount == 1){
            UserCartViewModel.restaurantNameInCart = userCartViewModel.getRestaurantNameFromCartMenu()
            
        }
        searchMenuInDisplayedMenuDetailsView.moveToCartView.updateRestaurantNameInCartLabelText(restaurantNameInUserCart:UserCartViewModel.restaurantNameInCart)
        
    }
    
    
    
    
    
}


extension SearchMenuInDisplayedUserMenuDetailsViewController{
    
    
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let contentViewConstraints = [searchMenuInDisplayedMenuDetailsView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor),
                                      searchMenuInDisplayedMenuDetailsView.bottomAnchor.constraint(equalTo : view.safeAreaLayoutGuide.bottomAnchor),
                                      searchMenuInDisplayedMenuDetailsView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                                      searchMenuInDisplayedMenuDetailsView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)]
            return contentViewConstraints
    }
}
