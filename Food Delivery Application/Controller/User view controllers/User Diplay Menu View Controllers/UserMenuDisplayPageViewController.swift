//
//  MenuDisplayPageViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 02/02/22.
//

import Foundation
import UIKit

class UserMenuDisplayPageViewController : UIViewController,MenuTableViewCellDelegate,UserDisplayMenuDetailsInRestaurantPageViewDelegate{
     private var userDisplayMenuDetailsPageViewModel : UserDisplayMenuDetailsPageViewModel = UserDisplayMenuDetailsPageViewModel()
     private var userCartViewModel : UserCartViewModel = UserCartViewModel.getUserCartViewModel()!
     private var userAddressesViewModel : UserAddressesViewModel = UserAddressesViewModel()
     private var menuDetailsOfRestaurant : [Int : MenuContentDetails] = [:]
     private var orderedMenuKeys : [Int] = []
     private var menuDetailsDisplayed : [Int : MenuContentDetails] = [:]
     private var menuPricePair : [Int : Int] = [:]
     private var menuItemsInCart : [Int:Int] = [:]
     private var orderedMenuDetailsDisplayedKeys : [Int] = []
     private var vegOnly : Bool = false
     private var orderedCategoryKeys : [Int] = []
     private var menuSplitCategoryWiseKeys : [Int : [Int]] = [:]
     private var restaurantContentDetails :RestaurantContentDetails!
     private var menuCategoryTypes :  [(categoryId : Int , categoryName : String)] = []
     private var menuCountLabels : [Int : UILabel] = [:]
     private var addMenuButtonViews : [Int : UIView] = [:]
    private var favouriteRestaurantStateBasedOnConditions : FavouriteRestaurantStateBasedOnConditions!
   private var menuDisplayPageView : UserDisplayMenuDetailsPageViewProtocol!
    private var menuTableViewHeightConstraint : NSLayoutConstraint?
   private var categoryButtons : [Int : UIButton] = [:]
   private var hideCategories : [Int] = []
   private var numberOfMenusDisplayed : Int = 0
    lazy var moveToCartView : MoveToCartViewProtocol = {
        let moveToCartView = MoveToCartView()
        moveToCartView.translatesAutoresizingMaskIntoConstraints = false
        return moveToCartView
    }()
     lazy var addFavouriteRestaurantButton : UIButton = {
        let addFavouriteRestaurantIconButton = UIButton(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
        addFavouriteRestaurantIconButton.setImage(UIImage(systemName: "heart")?.withTintColor(.red , renderingMode: .alwaysOriginal), for: .normal)
        addFavouriteRestaurantIconButton.backgroundColor = .white
        addFavouriteRestaurantIconButton.contentVerticalAlignment = .fill
        addFavouriteRestaurantIconButton.contentHorizontalAlignment = .fill
         addFavouriteRestaurantIconButton.addTarget(self, action: #selector(didTapAddFavouriteResraurant), for: .touchUpInside)
         
        return addFavouriteRestaurantIconButton
    }()
     
     lazy var searchFoodButton : UIButton = {
        let searchFoodIconButton = UIButton(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
        searchFoodIconButton.setImage(UIImage(systemName: "magnifyingglass")?.withTintColor(.black , renderingMode: .alwaysOriginal), for: .normal)
        searchFoodIconButton.backgroundColor = .white
        searchFoodIconButton.contentVerticalAlignment = .fill
        searchFoodIconButton.contentHorizontalAlignment = .fill
         searchFoodIconButton.addTarget(self, action: #selector(didTapSearchFoodButton), for: .touchUpInside)
        return searchFoodIconButton
    }()
     
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
     private var navigationBarTitleView : UserDisplayMenuDetailsPageNavigationBarTitleViewProtocol!
    private var scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .white
        scrollView.keyboardDismissMode = .onDrag
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
     
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        getDataFromDbAndUpdateView()
        addDelegatesForElements()
        
    }
    
    
    
   
    
   
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView.contentOffset.y > 0){
            navigationBarTitleView.isHidden = false
        }
        else{
            navigationBarTitleView.isHidden = true
        }
    }

    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        showActivityIndicator()
        DispatchQueue.global().async { [self] in
            checkRestaurantFavouriteAndToggleButton()
            checkIfDeliverableToCustomer()
            updateMenuCount()
            DispatchQueue.main.async {
                self.hideActivityIndicator()
            }
        }
        
    }
    
    func checkIfDeliverableToCustomer(){
       
            let deliveryAddress  = userAddressesViewModel.getLocalPersistedUserAddress()
            if(restaurantContentDetails.restaurantAddress.localityName == deliveryAddress.addressDetails.localityName && restaurantContentDetails.restaurantAddress.pincode == deliveryAddress.addressDetails.pincode){
                restaurantContentDetails.isDeliverable = true
            }
            else{
                restaurantContentDetails.isDeliverable = false
                DispatchQueue.main.async { [self] in
                    menuDisplayPageView.menuTableView.reloadData()
                    menuDisplayPageView.updateRestaurantStatus(deliveryTiming: Delivery.getDeaultDelieveryTimeInMins(), restaurantContentDetails: restaurantContentDetails)
                }
            }
        
    }
    
    
    
    func updateMenuCount(){
        
            menuItemsInCart = [:]
            menuItemsInCart = userCartViewModel.getCartMenuItems()
            DispatchQueue.main.async { [self] in
                if(addMenuButtonViews.count > 0){
                    for menuId in addMenuButtonViews.keys{
                        updateMenuCountLabelsBasedOnCartItems(menuId : menuId)
                    }
                }
            }
        
    }
    
    
    
    func setRestaurantContentDetails(restaurantContentDetails : RestaurantContentDetails){
        self.restaurantContentDetails = restaurantContentDetails
        
    }
    
     func createMenuElementsInCategoryWise(){
        orderedCategoryKeys = []
        for i in 0..<menuCategoryTypes.count{
            splitCategoryWiseSplitMenus(menuCategory: menuCategoryTypes[i])
        }
       
        for i in 0..<menuCategoryTypes.count{
            if(menuSplitCategoryWiseKeys[menuCategoryTypes[i].categoryId]?.count == 0){
                menuSplitCategoryWiseKeys.removeValue(forKey: menuCategoryTypes[i].categoryId)
            }
            else{
                orderedCategoryKeys.append(menuCategoryTypes[i].categoryId)
            }
        }
    }
    
    func updateMenuTableViewHeight(){
        menuTableViewHeightConstraint?.isActive = false
        menuTableViewHeightConstraint = menuDisplayPageView.menuTableView.heightAnchor.constraint(equalToConstant: getTableViewHeight())
        menuTableViewHeightConstraint?.isActive = true
        menuDisplayPageView.menuTableView.layoutIfNeeded()
    }
    
    func getTableViewHeight() -> CGFloat{
        let height = CGFloat((menuSplitCategoryWiseKeys.count * 80) + (Int(menuDisplayPageView.getMenuTabelViewCellHeight()) * numberOfMenusDisplayed))
        print(height)
        return height
    }
    
     func splitCategoryWiseSplitMenus(menuCategory : (categoryId : Int , categoryName : String)) {
        var categoryWiseSplitMenusKeys : [Int] = []
        for menuKeys in orderedMenuKeys {
            menuPricePair[menuKeys] = menuDetailsOfRestaurant[menuKeys]?.menuDetails.menuPrice
            let menu = menuDetailsOfRestaurant[menuKeys]!
            if menu.menuDetails.menuCategoryId == menuCategory.categoryId{
                if(vegOnly){
                    if(menu.menuDetails.menuTarianType == menuTarianTypeEnum.veg.rawValue){
                        categoryWiseSplitMenusKeys.append(menu.menuId)
                    }
                }
                else{
                    categoryWiseSplitMenusKeys.append(menu.menuId)
                }
            }
        }
        menuSplitCategoryWiseKeys[menuCategory.categoryId] =  categoryWiseSplitMenusKeys
        
    }
    
    
     func addMenuTableView(){
        updateMenuTableViewHeight()
    }
    
    @objc func didTapSwitchMenuOnlyVeg(sender : UISwitch){
        menuSplitCategoryWiseKeys = [:]
        if(sender.isOn == true){
            vegOnly = true
            menuDetailsDisplayed = [:]
            orderedMenuDetailsDisplayedKeys = []
            for menuKeys in orderedMenuKeys {
                let menu = menuDetailsOfRestaurant[menuKeys]!
                if menu.menuDetails.menuTarianType == menuTarianTypeEnum.veg.rawValue
                {
                    menuDetailsDisplayed[menu.menuId] = menu
                    orderedMenuDetailsDisplayedKeys.append(menu.menuId)
                }
            }
        }
        else{
            vegOnly = false
            menuDetailsDisplayed = menuDetailsOfRestaurant
            orderedMenuDetailsDisplayedKeys = orderedMenuKeys
        }
        createMenuElementsInCategoryWise()
        numberOfMenusDisplayed = orderedMenuDetailsDisplayedKeys.count
        menuDisplayPageView.menuTableView.reloadData()
        updateMenuTableViewHeight()
    }
    
    @objc func didTapMenuCategoryButton(sender : UIButton){
        showOrHideTableViewBasedOnMenuCategoryHiddenStatus(menuCategoryButton : sender)
    }
    
    
     func showOrHideTableViewBasedOnMenuCategoryHiddenStatus(menuCategoryButton : UIButton){
        if(hideCategories.contains(menuCategoryButton.tag)){
            showRowsOfCategory(menuCategoryId : menuCategoryButton.tag)
                }
                else{
                    hideRowsOfCategory(menuCategoryId : menuCategoryButton.tag)
                }
            }


     func hideRowsOfCategory(menuCategoryId : Int ){
        categoryButtons[menuCategoryId]?.addRightImageToButton(systemName: "chevron.down", imageColor: .lightGray, padding: 20)
        hideCategories.append(menuCategoryId)
        numberOfMenusDisplayed -= menuSplitCategoryWiseKeys[menuCategoryId]?.count ?? 0
        updateMenuTableViewHeight()
         menuDisplayPageView.menuTableView.reloadData()
    }

     func showRowsOfCategory(menuCategoryId : Int ){
        categoryButtons[menuCategoryId]?.addRightImageToButton(systemName: "chevron.up", imageColor: .lightGray, padding: 20)
        hideCategories.remove(at: hideCategories.firstIndex(of: menuCategoryId)!)
        numberOfMenusDisplayed += menuSplitCategoryWiseKeys[menuCategoryId]?.count ?? 0
        updateMenuTableViewHeight()
         menuDisplayPageView.menuTableView.reloadData()
    }
    
    
    
    
    
     
    
     func checkRestaurantFavouriteAndToggleButton(){
        
             if(UserIDUtils.shared.getUserId()  != 0){
                 if(userDisplayMenuDetailsPageViewModel.isFavouriteRestaurant(restaurantId: restaurantContentDetails.restaurantId)){
                     favouriteRestaurantStateBasedOnConditions = .favOn
                     DispatchQueue.main.async { [self] in
                         addFavouriteRestaurantButton.setImage(UIImage(systemName: "heart.fill")?.withTintColor(.red , renderingMode: .alwaysOriginal), for: .normal)
                     }
                 }
                 else if(favouriteRestaurantStateBasedOnConditions == .waitingForLogin){
                     if( userDisplayMenuDetailsPageViewModel.addToFavouriteRestaurants(restaurantId: restaurantContentDetails.restaurantId)){
                         favouriteRestaurantStateBasedOnConditions = .favOn
                         DispatchQueue.main.async { [self] in
                             addFavouriteRestaurantButton.setImage(UIImage(systemName: "heart.fill")?.withTintColor(.red , renderingMode: .alwaysOriginal), for: .normal)
                         }
                     }
                     
                 }
             }
             else{
                 if(favouriteRestaurantStateBasedOnConditions == .waitingForLogin){
                     favouriteRestaurantStateBasedOnConditions = .favOff
                 }
             }
         
        
    }
    
    @objc func didTapAddFavouriteResraurant(){
        if(favouriteRestaurantStateBasedOnConditions == .favOff){
            modifyAddFavouriteButtonBasedOnUserLoggedIn()
        }
        else{
            DispatchQueue.global().async { [self] in
                if(userDisplayMenuDetailsPageViewModel.removeFromFavouriteRestaurants(restaurantId: restaurantContentDetails.restaurantId)){
                    favouriteRestaurantStateBasedOnConditions = .favOff
                    DispatchQueue.main.async { [self] in
                        addFavouriteRestaurantButton.setImage(UIImage(systemName: "heart")?.withTintColor(.red , renderingMode: .alwaysOriginal), for: .normal)
                    }
                }
            }
        }
    }
    
    
     func modifyAddFavouriteButtonBasedOnUserLoggedIn(){
         DispatchQueue.global().async { [self] in
             if(UserIDUtils.shared.getUserId()  != 0){
                 favouriteRestaurantStateBasedOnConditions = .favOn
                 if(userDisplayMenuDetailsPageViewModel.addToFavouriteRestaurants(restaurantId: restaurantContentDetails.restaurantId)){
                     DispatchQueue.main.async { [self] in
                         addFavouriteRestaurantButton.setImage(UIImage(systemName: "heart.fill")?.withTintColor(.red , renderingMode: .alwaysOriginal), for: .normal)
                     }
                 }
                 
             }
             else{
                 favouriteRestaurantStateBasedOnConditions = .waitingForLogin
                 DispatchQueue.main.async { [self] in
                     let login = UINavigationController(rootViewController: UserLoginViewController())
                     login.modalPresentationStyle = .fullScreen
                     present(login, animated: true, completion: nil)
                 }
                 
             }
         }
    }
    
    @objc func didTapSearchFoodButton(){
        let searchVC = SearchMenuInDisplayedUserMenuDetailsViewController()
        searchVC.setMenuDetailsOfRestaurant(menuDetailsOfRestaurant: menuDetailsDisplayed, orderedMenuDetailsDisplayedKeys: orderedMenuDetailsDisplayedKeys, menuPricePair: menuPricePair)
        searchVC.setRestaurantDetails(restaurantContentDetails: restaurantContentDetails)
        navigationController?.pushViewController(searchVC, animated: true)
    }
    
    
    
    
    
    
    
     private func initialiseView(){
        view.backgroundColor = .white
        
         view.addSubview(scrollView)
         menuDisplayPageView = UserDisplayMenuDetailsInRestaurantPageView()
         menuDisplayPageView.translatesAutoresizingMaskIntoConstraints = false
         menuDisplayPageView.delegate = self
         scrollView.addSubview(menuDisplayPageView)
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
     private func initialiseViewElements(){
       
         
        navigationBarTitleView = UserDisplayMenuDetailsInRestaurantPageViewNavigationBarTitleView()
         navigationBarTitleView.updateRestaurantDetails(restaurantContentDetails: restaurantContentDetails)
        menuDisplayPageView.updateRestaurantDetails(restaurantContentDetails: restaurantContentDetails)
         favouriteRestaurantStateBasedOnConditions = .favOff
        addMoveToCartView()
        addMoveToCartViewNotificationObservers()
        addMenuTableView()
        setDefaultNavigationBarApprearance()
        setScrollEdgeApprearance()
        navigationItem.titleView = navigationBarTitleView
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: addFavouriteRestaurantButton) ,UIBarButtonItem(customView: searchFoodButton)]
        navigationItem.leftBarButtonItem = backArrowButton 
    }
    
    private func getDataFromDbAndUpdateView(){
        DispatchQueue.global().async { [self] in
            menuItemsInCart = userCartViewModel.getCartMenuItems()
            let menusInRestaurant = userDisplayMenuDetailsPageViewModel.getMenuContents(restaurantId: restaurantContentDetails.restaurantId)
            menuDetailsOfRestaurant = menusInRestaurant.menuContentsInDictionaryFormat
            orderedMenuKeys = menusInRestaurant.orderedKeys
            menuDetailsDisplayed = menuDetailsOfRestaurant
            orderedMenuDetailsDisplayedKeys = orderedMenuKeys
            numberOfMenusDisplayed = orderedMenuDetailsDisplayedKeys.count
            menuCategoryTypes = userDisplayMenuDetailsPageViewModel.getMenuCategoryTypes(restaurantId: restaurantContentDetails.restaurantId)
            DispatchQueue.main.async { [self] in
                createMenuElementsInCategoryWise()
                menuDisplayPageView.menuTableView.reloadData()
                updateMenuTableViewHeight()
                initMoveToCartView()
            }
        }
    }
    
     private func addDelegatesForElements(){
        scrollView.delegate  = self
         menuDisplayPageView.menuTableView.delegate = self
         menuDisplayPageView.menuTableView.dataSource = self
    }
    
    
    
}

extension UserMenuDisplayPageViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
        return menuSplitCategoryWiseKeys.count
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            return UIView()
        }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(hideCategories.contains(orderedCategoryKeys[section])){
            return categoryButtons[orderedCategoryKeys[section]]
        }
        else if(categoryButtons[orderedCategoryKeys[section]] != nil){
            return categoryButtons[orderedCategoryKeys[section]]
        }
        else{
            let menuCategory = menuCategoryTypes[section]
            let menuCategoryButton = menuDisplayPageView.createMenuCategoryButton(menuCategory: menuCategory.categoryName)
            menuCategoryButton.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50)
            menuCategoryButton.tag = menuCategory.categoryId
            categoryButtons[menuCategory.categoryId] = menuCategoryButton
            return menuCategoryButton
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if( hideCategories.contains(orderedCategoryKeys[section])){
            return 0
        }
        else{
            let count =  menuSplitCategoryWiseKeys[orderedCategoryKeys[section]]!.count
            
            return count
        }
            
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  menuSplitCategoryWiseKeys[orderedCategoryKeys[indexPath.section]]!.count > 0 {
            
            let menuContentDetails = menuDetailsOfRestaurant[menuSplitCategoryWiseKeys[orderedCategoryKeys[indexPath.section]]![indexPath.row]]!
           
            let menuDetails = menuContentDetails.menuDetails
            
            
            
            let cell = menuDisplayPageView.createMenuTableViewCell(indexPath: indexPath, tableView: tableView, menuCellContents: (menuImage: menuDetails.menuImage, menuName: menuDetails.menuName, menuDescription: menuDetails.menuDescription, menuPrice: menuDetails.menuPrice, menuTarianType: menuDetails.menuTarianType,menuAvailable : menuContentDetails.menuIsAvailable, menuAvailableNextAt: menuContentDetails.menuNextAvailableAt,menuId : menuContentDetails.menuId))
            cell.delegate = self
            menuCountLabels[menuContentDetails.menuId] = cell.menuCountLabel
            addMenuButtonViews[menuContentDetails.menuId] = cell.addMenuButtonView
            modifyAddMenuButtonAndViewBasedOnRestaurantAvailablityAndDeliverable(addMenuButton: cell.addMenuCellButton, plusButton: cell.plusButton, minusButton: cell.minusButton, menuCountLabel: cell.menuCountLabel, addMenuButtonView: cell.addMenuButtonView)
                updateMenuCountLabelsBasedOnCartItems(menuId: menuContentDetails.menuId)
                return cell
            }
            return UITableViewCell()
        }
                
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        menuDisplayPageView.getMenuTabelViewCellHeight()
    }
    
   
    func updateMenuCountLabelsBasedOnCartItems(menuId : Int){
        if(menuItemsInCart[menuId] != nil && restaurantContentDetails.restaurantIsAvailable != 0 && restaurantContentDetails.isDeliverable && menuDetailsDisplayed[menuId]?.menuIsAvailable != 0){
            addMenuButtonViews[menuId]?.isHidden = false
            menuCountLabels[menuId]?.text = String(menuItemsInCart[menuId]!)
        }
        else{
            addMenuButtonViews[menuId]?.isHidden = true
            menuCountLabels[menuId]?.text = "\(0)"

        }
    }
    
    func modifyAddMenuButtonAndViewBasedOnRestaurantAvailablityAndDeliverable(addMenuButton: UIButton, plusButton: UIButton, minusButton: UIButton, menuCountLabel: UILabel,addMenuButtonView : UIView){
        if(restaurantContentDetails.restaurantIsAvailable == 0 || !restaurantContentDetails.isDeliverable){
            addMenuButton.removeFromSuperview()
            plusButton.removeFromSuperview()
            minusButton.removeFromSuperview()
            menuCountLabel.removeFromSuperview()
            addMenuButtonView.removeFromSuperview()
        }
    }
    
    @objc func didTapAddMenu(sender : UIButton){
        let menuId = sender.tag
        if(UserCartViewModel.restaurantNameInCart != "" && UserCartViewModel.restaurantNameInCart != restaurantContentDetails.restaurantDetails.restaurantName){
            showAlertToClearCart(sender: sender)
        }
        else{
            
            DispatchQueue.global().async { [self] in
                if(userCartViewModel.addMenuToCart(cartDetails: (menuId: menuId, restaurantId: restaurantContentDetails.restaurantId , quantity: 0 ))){
                    DispatchQueue.main.async { [self] in
                        addMenuButtonViews[sender.tag]!.isHidden = false
                        didTapPlusButton(sender: sender)
                    }
                }
            }
            
        }
    }
    
    @objc func didTapPlusButton(sender : UIButton){
        let menuId = sender.tag
        menuItemsInCart[menuId]  = (menuItemsInCart[menuId] ?? 0) + 1
        UserCartViewModel.totalPrice += menuPricePair[menuId] ?? 0
        UserCartViewModel.cartItemsCount += 1
        DispatchQueue.global(qos: .userInteractive).async { [self] in
            if(userCartViewModel.updateMenuInCartAndSendNotificationTomoveToFullMenuView(menuId: menuId, quantity: (menuItemsInCart[menuId] ?? 0) )){
               
                DispatchQueue.main.async { [self] in
                    self.menuCountLabels[menuId]!.text = String(Int(self.menuCountLabels[menuId]!.text!)! + 1)
                }
            }
        }
        
    }
    
    @objc func didTapMinusButton(sender : UIButton){
        let menuId = sender.tag
        menuItemsInCart[menuId]  = (menuItemsInCart[menuId] ?? 0) - 1
        UserCartViewModel.totalPrice -= self.menuPricePair[menuId] ?? 0
        UserCartViewModel.cartItemsCount -= 1
        DispatchQueue.global(qos: .userInteractive).async { [self] in
            if(userCartViewModel.updateMenuInCartAndSendNotificationTomoveToFullMenuView(menuId: menuId, quantity:(menuItemsInCart[menuId] ?? 0))){
                
                DispatchQueue.main.async { [self] in
                    self.menuCountLabels[menuId]!.text = String(Int(self.menuCountLabels[menuId]!.text!)! - 1)
                    if(self.menuCountLabels[menuId]!.text! == "0"){
                        DispatchQueue.global().async { [self] in
                            if(userCartViewModel.removeMenuFromCart(menuId: menuId)){
                                DispatchQueue.main.async { [self] in
                                    addMenuButtonViews[menuId]!.isHidden = true
                                    menuItemsInCart.removeValue(forKey: menuId)
                                }
                            }
                        }
                    }
                }
            }
            
        }
        
        
        
        
    }
   
    func  showAlertToClearCart(sender : UIButton){
       let alert = UIAlertController(title: "Replace Cart Item?", message: "Your cart contains dishes from \(UserCartViewModel.restaurantNameInCart). Do you want to discard the selection and add dishes from \(restaurantContentDetails.restaurantDetails.restaurantName)?", preferredStyle: .alert)
       alert.addAction(UIAlertAction(title: "No", style: .cancel,handler: nil))
       alert.addAction(UIAlertAction(title: "Yes", style: .default,handler: {_ in
           DispatchQueue.global().async {
               if(self.userCartViewModel.clearCart()){
                   DispatchQueue.main.async {
                       UserCartViewModel.totalPrice = 0
                       UserCartViewModel.cartItemsCount = 0
                       UserCartViewModel.restaurantNameInCart = ""
                       self.didTapAddMenu(sender: sender)
                   }
               }
           }
         
       }))
       present(alert,animated: true)
   }
    
   
    
    
}


extension UserMenuDisplayPageViewController : MoveToCartViewDelegate{
    func initMoveToCartView(){
        updateMenuCountAndTotalPriceInmoveToCartView()
        hideOrShowmoveToCartViewBasedOnMenuCount()
        updateNameOfRestaurantInCart()
    }
    
    
    
    func addMoveToCartView(){
        moveToCartView.delegate = self
        self.view.addSubview(moveToCartView)
        self.view.bringSubviewToFront(moveToCartView)
        NSLayoutConstraint.activate(getmoveToCartViewLayoutConstraints())
    }
    
    private func getmoveToCartViewLayoutConstraints() -> [NSLayoutConstraint]{
        let viewContraints = [moveToCartView.bottomAnchor.constraint(equalTo:  self.view.safeAreaLayoutGuide.bottomAnchor,constant: -10),
                              moveToCartView.leftAnchor.constraint(equalTo: self.view.leftAnchor,constant: 10),
                              moveToCartView.rightAnchor.constraint(equalTo: self.view.rightAnchor,constant: -10),
            moveToCartView.heightAnchor.constraint(equalToConstant: 50)]
        return viewContraints
    }
    
    @objc func didTapMoveToCart(){
        if(UserCartViewModel.restaurantNameInCart == restaurantContentDetails.restaurantDetails.restaurantName){
            navigationController?.pushViewController(UserCartViewController(), animated: true)
        }
        else{
            let userMenuDisplayPageViewController = UserMenuDisplayPageViewController()
            var restaurantDetails = userCartViewModel.getRestaurantDetailsFromCartMenu()
            let deliveryAddress = UserAddressesViewModel().getLocalPersistedUserAddress()
            if(deliveryAddress.addressDetails.localityName == restaurantDetails.restaurantAddress.localityName && deliveryAddress.addressDetails.pincode == restaurantDetails.restaurantAddress.pincode){
                restaurantDetails.isDeliverable =  true
            }
            userMenuDisplayPageViewController.setRestaurantContentDetails(restaurantContentDetails: restaurantDetails)
            navigationController?.pushViewController(userMenuDisplayPageViewController, animated: false)
            navigationController?.pushViewController(UserCartViewController(), animated: true)
        }
    }
    
    
    func addMoveToCartViewNotificationObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateMenuCountAndTotalPriceInmoveToCartView), name: Notification.Name(moveToViewNotificationNames.updateMenuCountAndPriceInmoveToView.rawValue), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideOrShowmoveToCartViewBasedOnMenuCount), name: Notification.Name(moveToViewNotificationNames.hideOrShowmoveToViewBasedOnMenuCount.rawValue), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateNameOfRestaurantInCart), name: Notification.Name(moveToViewNotificationNames.updateNameOfRestaurantInCart.rawValue), object: nil)
        
        
    }
    
    @objc func updateMenuCountAndTotalPriceInmoveToCartView(){
        moveToCartView.updateMenuItemCountAndPriceLabelText(menuCount: UserCartViewModel.cartItemsCount, totalPrice: UserCartViewModel.totalPrice)
    }
    
    @objc func hideOrShowmoveToCartViewBasedOnMenuCount(){
        moveToCartView.hideOrShowmoveToCartViewBasedOnMenuCount(menuCount: UserCartViewModel.cartItemsCount)
    }
    
    @objc func updateNameOfRestaurantInCart(){
        if(UserCartViewModel.cartItemsCount == 0){
            UserCartViewModel.restaurantNameInCart = ""
        }
        else if(UserCartViewModel.cartItemsCount == 1){
            UserCartViewModel.restaurantNameInCart = userCartViewModel.getRestaurantNameFromCartMenu()
            
        }
        moveToCartView.updateRestaurantNameInCartLabelText(restaurantNameInUserCart:UserCartViewModel.restaurantNameInCart)
        
    }
    
    
    
    
    
}

enum FavouriteRestaurantStateBasedOnConditions{
    case favOn
    case favOff
    case waitingForLogin
}


extension UserMenuDisplayPageViewController{
    
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [scrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                         scrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                         scrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                         scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
    private func getContentViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [menuDisplayPageView.topAnchor.constraint(equalTo:scrollView.topAnchor),
                               menuDisplayPageView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
                               menuDisplayPageView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
                               menuDisplayPageView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                               menuDisplayPageView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                     ]
        return viewConstraints
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if(menuDisplayPageView.menuTableView.frame.maxY > 0){
            menuDisplayPageView.removeHeightAnchorConstraints()
            menuDisplayPageView.heightAnchor.constraint(equalToConstant:  menuDisplayPageView.menuTableView.frame.maxY + 100).isActive = true
        }
        else{
            menuDisplayPageView.removeHeightAnchorConstraints()
            menuDisplayPageView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
        
    }
}


