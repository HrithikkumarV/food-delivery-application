//
//  AddressViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 13/11/21.
//

import Foundation
import UIKit

class AddressViewController : UIViewController , UITextFieldDelegate{
    
    private var doorNoAndBuildingNameAndBuildingNo  : UITextField!
    private var streetName : UITextField!
    private var locality : UITextField!
    private var landmark : UITextField!
    private var city : UITextField!
    private var state : UITextField!
    private var country : UITextField!
    private var pincode : UITextField!
    private var continueButton : UIButton!
    private var backArrowButton : UIButton!
    private var pincodeKeyBoardToolBar : UIToolbar!
    private var pincodeKeyBoardDoneBarButton : UIBarButtonItem!
    private var keyboardUtils : KeyboardUtils!
    private var flexibleSpaceForToolBar : UIBarButtonItem!
    private var activeArea : UIView? = nil
    private var stackView : UIStackView!
    private var addressView : AddressViewProtocol!
    private var contentView : UIView!
    private var scrollView : UIScrollView!
    private var viewTap  : UITapGestureRecognizer!
    var isSubmitted : Bool = false
    var address : AddressDetails!
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addTargetsAndActionsForElements()
        addDelegatesForElements()
        addObserver()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        NSLayoutConstraint.activate(addressView.getStackViewConstraints())
        NSLayoutConstraint.activate(addressView.getContinueButtonContraints())
        NSLayoutConstraint.activate(addressView.getScrollViewConstraints())
        NSLayoutConstraint.activate(addressView.getContentViewConstraints())
    }
    
    @objc private func didTapContinue(){
        
        if(!doorNoAndBuildingNameAndBuildingNo.text!.isEmpty && !streetName.text!.isEmpty && !landmark.text!.isEmpty && !locality.text!.isEmpty && !city.text!.isEmpty && !state.text!.isEmpty && !country.text!.isEmpty){
            address = AddressDetails(doorNoAndBuildingNameAndBuildingNo: doorNoAndBuildingNameAndBuildingNo.text!, streetName: streetName.text!, localityName: locality.text!, landmark: landmark.text!, city: city.text!, state: state.text!, pincode: pincode.text!, country: country.text!)
            isSubmitted = true

    
                let addressTagViewController = AddressTagViewController()
                addressTagViewController.setAddress(addressDetails: address)
                navigationController?.pushViewController(addressTagViewController, animated: true)
        }
        else{
            showAlert(message: "Please Fill all the fields")
        }
        
        
    }
    
    
    
    @objc func didTapPincodeKeyboardDoneBarButton(){
        pincode.resignFirstResponder()
    }
    
    @objc private func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    private func isModal() -> Bool {
        if self.presentingViewController != nil {
            return true
        } else if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController  {
            return true
        } else if self.tabBarController?.presentingViewController is UITabBarController {
            return true
        }

        return false
    }
    
    @objc private func dismissKeyboard(){
        view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        doorNoAndBuildingNameAndBuildingNo.becomeFirstResponder()
        scrollView.contentSize = CGSize(width: view.frame.size.width, height: continueButton.frame.maxY + 100)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField{
        case doorNoAndBuildingNameAndBuildingNo :
            streetName.becomeFirstResponder()
        case streetName :
            landmark.becomeFirstResponder()
        case landmark :
            locality.becomeFirstResponder()
        case locality :
            city.becomeFirstResponder()
        case city :
            state.becomeFirstResponder()
        case state :
            country.becomeFirstResponder()
        case country :
            pincode.becomeFirstResponder()
        case pincode :
            pincode.resignFirstResponder()
        default:
            return true
        }
        return true
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        keyboardUtils.keyboardWillShow(notification: notification, maxYofLastObjectInView: continueButton.frame.maxY, activeArea: activeArea)
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardUtils.keyboardWillHide()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeArea = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeArea = nil
    }

    func getAddress() -> AddressDetails{
        return address
    }
    
    func showAlert(message : String){
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel,handler: nil))
        present(alert,animated: true)
    }
    
    private func initialiseView(){
        title = "Set Address"
        view.backgroundColor = .white
        scrollView = UIScrollView()
        scrollView.backgroundColor = .white
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        contentView = UIView()
        contentView.backgroundColor = .white
        contentView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(contentView)
    }
    
    private func initialiseViewElements(){
        addressView = AddressView(contentView: contentView,scrollView: scrollView,rootView: view)
        doorNoAndBuildingNameAndBuildingNo  = addressView.createDoorNoAndBuildingNameAndBuildingNoTextField()
        streetName = addressView.createStreetNameTextField()
        locality = addressView.createLocalityNameTextField()
        landmark = addressView.createLandmarkNameTextField()
        city = addressView.createCityNameTextField()
        state = addressView.createStateNameTextField()
        country = addressView.createCountryNameTextField()
        pincode = addressView.createPincodeTextField()
        backArrowButton = addressView.createBackNavigationBarButton()
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backArrowButton)
       
        pincodeKeyBoardDoneBarButton = addressView.createNextBarButton()
        pincodeKeyBoardToolBar = addressView.createKeyboardToolBar()
        
        flexibleSpaceForToolBar = addressView.createFlexibleSpaceForToolBar()
        pincodeKeyBoardToolBar.items = [flexibleSpaceForToolBar,pincodeKeyBoardDoneBarButton]
        pincode.inputAccessoryView = pincodeKeyBoardToolBar
        continueButton = addressView.createContinueButton()
        stackView = addressView.setAddressTextFieldsStackView(doorNoAndHouseName: doorNoAndBuildingNameAndBuildingNo , streetName: streetName,  landMark: landmark, locality: locality, city: city, state: state, country: country, pincode: pincode)
        viewTap = addressView.createViewTapGestureRecognizer()
    }
    
    private func addTargetsAndActionsForElements(){
        pincodeKeyBoardDoneBarButton.target = self
        pincodeKeyBoardDoneBarButton.action = #selector(didTapPincodeKeyboardDoneBarButton)
        continueButton.addTarget(self, action: #selector(didTapContinue), for: .touchUpInside)
        backArrowButton.addTarget(self, action: #selector(didTapBackArrow), for: .touchUpInside)
        viewTap.addTarget(self, action: #selector(dismissKeyboard))
    }
    
    private func addDelegatesForElements(){
        doorNoAndBuildingNameAndBuildingNo.delegate = self
        streetName.delegate = self
        locality.delegate = self
        landmark.delegate = self
        city.delegate = self
        state.delegate = self
        country.delegate = self
        pincode.delegate = self
    }
    
    private func addObserver(){
        keyboardUtils = KeyboardUtils(scrollView: scrollView)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
    }
}
