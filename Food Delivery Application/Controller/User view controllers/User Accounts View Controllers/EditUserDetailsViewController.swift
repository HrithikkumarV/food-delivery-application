//
//  EditUserDetailsViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/03/22.
//

import Foundation
import UIKit

class EditUserDetailsViewController : UIViewController, UITextFieldDelegate,EditUserDetailsPageViewDelegate{
    private var userAccountsViewModel : UserAccountsViewModel = UserAccountsViewModel()
    private var userDetails : UserDetails!
    private var editUserDetailsPageView : EditUserDetailsPageViewProtocol!
    private var keyboardUtils : KeyboardUtils!
    private var activeArea : UIView? = nil
    private let editUserDetailsScrollView : UIScrollView = {
        let editUserDetailsScrollView = UIScrollView()
        editUserDetailsScrollView.backgroundColor = .clear
        editUserDetailsScrollView.keyboardDismissMode = .onDrag
        editUserDetailsScrollView.translatesAutoresizingMaskIntoConstraints = false
        return editUserDetailsScrollView
    }()

    private lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.target = self
        backArrowButton.action = #selector(didTapBackArrow)
        return backArrowButton
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addTargetsAndActionsForElements()
        addDelegatesForElements()
        addObserver()
        setupUserDetailsInViewElements()
    }
    
   
    
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.global().async { [self] in
            userDetails = userAccountsViewModel.getUserDetails(userId: (UserIDUtils.shared.getUserId()))
            DispatchQueue.main.async {
                return
            }
        }
    }
    
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    
    
     func didTapUpdateNameButton(){
         let userNameText =   editUserDetailsPageView.userNameTextField.text ?? ""
        DispatchQueue.global().async { [self] in
            if(userAccountsViewModel.updateUserName(userId: UserIDUtils.shared.getUserId(), userName: userNameText)){
                DispatchQueue.main.async { [self] in
                    userDetails.userName = userNameText
                    showToast(message: "Updated Name Successfully", font: UIFont.systemFont(ofSize: 25, weight: .medium), fontColor: .white, backGroundColor: .systemBlue.withAlphaComponent(0.9))
                    didTapCancelUpdateNameButton()
                }
            }
        }
        
        
    }
    
     func didTapCancelUpdateNameButton(){
         editUserDetailsPageView.userNameTextField.resignFirstResponder()
         editUserDetailsPageView.userNameTextField.text = userDetails.userName
         editUserDetailsPageView.updateAndCancelNameHorizontalStackView.isHidden = true
    }
    
     func didTapVerifyPhoneNumberButton(){
         let userPhoneNumber = editUserDetailsPageView.phoneNumberTextField.text!
         editUserDetailsPageView.phoneNumberTextField.resignFirstResponder()
         DispatchQueue.global().async { [self] in
             if(!userAccountsViewModel.checkIfPhoneNumberExists(phoneNumber:userPhoneNumber)){
                 
                 DispatchQueue.main.async { [self] in
                     let verifyOTPAndUpdatePhoneNumberViewController = VerifyOTPAndUpdatePhoneNumberViewController()
                     verifyOTPAndUpdatePhoneNumberViewController.setUserPhoneNumber(phoneNumber: userPhoneNumber)
                     userDetails.userPhoneNumber =  userPhoneNumber
                      navigationController?.pushViewController(verifyOTPAndUpdatePhoneNumberViewController, animated: true)
                     didTapCancelUpdatePhoneNumberButton()
                 }
            }
            else{
                DispatchQueue.main.async {
                    self.showAlert(message: "This phone number already exists")
                }
            }
         }
         
    }
    
     func didTapCancelUpdatePhoneNumberButton(){
         editUserDetailsPageView.phoneNumberTextField.resignFirstResponder()
         editUserDetailsPageView.phoneNumberTextField.text = userDetails.userPhoneNumber
         editUserDetailsPageView.verifyAndCancelPhoneNumberHorizontalStackView.isHidden = true
    }
    
    

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        if( textField == editUserDetailsPageView.phoneNumberTextField){
            let maxLength = 10
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            var res = ""
            if editUserDetailsPageView.phoneNumberTextField.text != "" || string != "" {
                    res = (textField.text ?? "") + string
                }
            return newString.length <= maxLength && Double(res) != nil
        }
        return true
    }
    
    @objc func textFieldDidChange(sender : UITextField){
        if(sender == editUserDetailsPageView.userNameTextField){
            userNameTextfieldChangesActions()
        }
        else{
            phoneNumberTextfieldChangesActions()
        }
    }
    
    
     func didTapView(){
        self.view.endEditing(true)
    }
    
    private func userNameTextfieldChangesActions(){
        editUserDetailsPageView.updateAndCancelNameHorizontalStackView.isHidden = false
        if(editUserDetailsPageView.userNameTextField.text == userDetails.userName || editUserDetailsPageView.userNameTextField.text!.count == 0){
            editUserDetailsPageView.updateNameButton.backgroundColor = .systemBlue.withAlphaComponent(0.6)
            editUserDetailsPageView.updateNameButton.isUserInteractionEnabled = false
                
        }
        else{
            editUserDetailsPageView.updateNameButton.backgroundColor = .systemBlue
            editUserDetailsPageView.updateNameButton.isUserInteractionEnabled = true
            
        }
    }
    
    private func phoneNumberTextfieldChangesActions(){
        editUserDetailsPageView.verifyAndCancelPhoneNumberHorizontalStackView.isHidden = false
        if(editUserDetailsPageView.phoneNumberTextField.text == userDetails.userPhoneNumber){
           
            editUserDetailsPageView.verifyPhoneNumberButton.backgroundColor = .systemBlue.withAlphaComponent(0.6)
            editUserDetailsPageView.verifyPhoneNumberButton.isUserInteractionEnabled = false
            
           
        }
        else if(editUserDetailsPageView.phoneNumberTextField.text?.count == 10){
            editUserDetailsPageView.verifyPhoneNumberButton.backgroundColor = .systemBlue
            
            editUserDetailsPageView.verifyPhoneNumberButton.isUserInteractionEnabled = true
           
        }
        else{
            editUserDetailsPageView.verifyPhoneNumberButton.backgroundColor = .systemBlue.withAlphaComponent(0.6)
            editUserDetailsPageView.verifyPhoneNumberButton.isUserInteractionEnabled = false
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        keyboardUtils.keyboardWillShow(notification: notification, maxYofLastObjectInView: editUserDetailsPageView.stackView.frame.maxY, activeArea: activeArea)
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardUtils.keyboardWillHide()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeArea = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeArea = nil
    }
    
    private func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        activeArea = textView
        return true
    }
    
    private func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        activeArea = nil
        return true
    }
    
    
    
    private func setupUserDetailsInViewElements(){
        userDetails = userAccountsViewModel.getUserDetails(userId: (UserIDUtils.shared.getUserId() ))
        editUserDetailsPageView.userNameTextField.text = userDetails.userName
        editUserDetailsPageView.updateAndCancelNameHorizontalStackView.isHidden = true
        editUserDetailsPageView.phoneNumberTextField.text = userDetails.userPhoneNumber
        editUserDetailsPageView.verifyAndCancelPhoneNumberHorizontalStackView.isHidden = true
        
    }
    
    
    private func initialiseView(){
        title = "EDIT ACCOUNT"
        view.backgroundColor = .white
         view.addSubview(editUserDetailsScrollView)
        editUserDetailsPageView = EditUserDetailsPageView()
        editUserDetailsPageView.translatesAutoresizingMaskIntoConstraints = false
        editUserDetailsScrollView.addSubview(editUserDetailsPageView)
        editUserDetailsPageView.delegate = self
        NSLayoutConstraint.activate(getContentViewConstraints())
        NSLayoutConstraint.activate(getScrollViewConstraints())
         
    }
    
    private func initialiseViewElements(){
        
       
        
        navigationItem.leftBarButtonItem = backArrowButton
        
    }
    
    private func addTargetsAndActionsForElements(){
       
        editUserDetailsPageView.userNameTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .allEditingEvents)
        editUserDetailsPageView.phoneNumberTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .allEditingEvents)
    }
    
    private func addDelegatesForElements(){
        editUserDetailsPageView.userNameTextField.delegate = self
        editUserDetailsPageView.phoneNumberTextField.delegate = self
    }
    
    private func addObserver(){
       keyboardUtils = KeyboardUtils(scrollView: editUserDetailsScrollView)
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil);
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil);
     
   }
    
    
    
    
}

extension EditUserDetailsViewController{
    
    private func getScrollViewConstraints() -> [NSLayoutConstraint]{
        let scrollViewConstraints = [editUserDetailsScrollView.topAnchor.constraint(equalTo:self.view.safeAreaLayoutGuide.topAnchor),
                                     editUserDetailsScrollView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
                                     editUserDetailsScrollView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
                                     editUserDetailsScrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)]
        return scrollViewConstraints
    }

private func getContentViewConstraints() -> [NSLayoutConstraint]{
    let viewConstraints = [editUserDetailsPageView.topAnchor.constraint(equalTo:editUserDetailsScrollView.topAnchor),
                           editUserDetailsPageView.leftAnchor.constraint(equalTo: editUserDetailsScrollView.leftAnchor),
                           editUserDetailsPageView.rightAnchor.constraint(equalTo: editUserDetailsScrollView.rightAnchor),
                           editUserDetailsPageView.bottomAnchor.constraint(equalTo: editUserDetailsScrollView.bottomAnchor),
                           editUserDetailsPageView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                                 ]
    return viewConstraints
}

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if(editUserDetailsPageView.stackView.frame.maxY > 0){
            editUserDetailsPageView.removeHeightAnchorConstraints()
            editUserDetailsPageView.heightAnchor.constraint(equalToConstant:  editUserDetailsPageView.stackView.frame.maxY + 100).isActive = true
        }
        else{
            editUserDetailsPageView.removeHeightAnchorConstraints()
            editUserDetailsPageView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
    }
    

}

