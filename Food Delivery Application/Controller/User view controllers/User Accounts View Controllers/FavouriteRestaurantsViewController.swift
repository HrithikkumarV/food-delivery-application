//
//  FavouriteRestaurantsViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/03/22.
//

import Foundation

import UIKit

class FavouriteRestaurantsViewController : UIViewController,FavouriteRestaurantsViewDelegate{
     private var favouriteRestaurantsView : FavouriteRestaurantsViewProtocol!
     private var favouriteRestaurantsViewModel : FavouriteRestaurantViewModel = FavouriteRestaurantViewModel()
     private var userAddressesViewModel : UserAddressesViewModel = UserAddressesViewModel()
     private var deliveryAddress : UserAddressDetails!
     private var restaurantsContentsDetails : [RestaurantContentDetails] = []
    
    lazy var backArrowButton : UIBarButtonItem = {
        let backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.target = self
        backArrowButton.action = #selector(didTapBackArrow)
        return backArrowButton
    }()
    
   
   
    
    override func viewDidLoad(){
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        addDelegatesAndDataSourceForElements()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.global().async {
            self.getInitialDataFromDB()
            DispatchQueue.main.async { [self] in
                UpdateRestaurantsTableViewContent()
                updateViewBasedOnTheRestaurantAvailableCount()
            }
        }
       
    }
    

    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    func didTapBrowseRestaurant(){
        navigationController?.popViewController(animated: true)
        sendNotificationToTabBarToMoveToFirstVC()
    }
    
    func sendNotificationToTabBarToMoveToFirstVC(){
        NotificationCenter.default.post(name: Notification.Name(moveToFirstVcInTabBar.moveToFirstVc.rawValue), object: nil)
    }
    
    
    
    
     private func updateViewBasedOnTheRestaurantAvailableCount(){
        if(restaurantsContentsDetails.count == 0){
            favouriteRestaurantsView.restaurantsTableView.isHidden = true
            favouriteRestaurantsView.noRestaurantsAvailableLabel.isHidden = false
            favouriteRestaurantsView.browseRestaurantsButton.isHidden = false
        }
        else{
            favouriteRestaurantsView.restaurantsTableView.isHidden = false
            favouriteRestaurantsView.noRestaurantsAvailableLabel.isHidden = true
            favouriteRestaurantsView.browseRestaurantsButton.isHidden = true
        }
    }
    
     private func UpdateRestaurantsTableViewContent(){
        
         favouriteRestaurantsView.restaurantsTableView.reloadData()
    }
    
    private func getInitialDataFromDB(){
        restaurantsContentsDetails = favouriteRestaurantsViewModel.getFavouriteRestaurants(userId: (UserIDUtils.shared.getUserId()))
         deliveryAddress = userAddressesViewModel.getLocalPersistedUserAddress()
    }
 
    
    
    
     private func initialiseView(){
         title = "Favourite Restaurants"
         view.backgroundColor = .white
         favouriteRestaurantsView = FavouriteRestaurantsView()
         favouriteRestaurantsView.translatesAutoresizingMaskIntoConstraints = false
         favouriteRestaurantsView.delegate = self
         view.addSubview(favouriteRestaurantsView)
         NSLayoutConstraint.activate(getContentViewConstraints())
        
    }
    
     private func addDelegatesAndDataSourceForElements(){
         favouriteRestaurantsView.restaurantsTableView.delegate = self
         favouriteRestaurantsView.restaurantsTableView.dataSource = self
         self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
     private func initialiseViewElements(){
        setDefaultNavigationBarApprearance()
        navigationItem.leftBarButtonItem = backArrowButton
    }
    
}

extension FavouriteRestaurantsViewController :  UITableViewDelegate ,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurantsContentsDetails.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let restaurantDetails = restaurantsContentsDetails[indexPath.row].restaurantDetails
        var isDeliverable :Bool = false
        print(restaurantsContentsDetails[indexPath.row].restaurantAddress.localityName, deliveryAddress.addressDetails.localityName , restaurantsContentsDetails[indexPath.row].restaurantAddress.pincode , deliveryAddress.addressDetails.pincode)
        if(restaurantsContentsDetails[indexPath.row].restaurantAddress.localityName == deliveryAddress.addressDetails.localityName && restaurantsContentsDetails[indexPath.row].restaurantAddress.pincode == deliveryAddress.addressDetails.pincode){
            isDeliverable = true
            
        }
        restaurantsContentsDetails[indexPath.row].isDeliverable =  isDeliverable
        let cell = favouriteRestaurantsView.createRestaurantTableViewCell(indexPath: indexPath, restaurentCellContents: (restaurantProfileImage: restaurantDetails.restaurantProfileImage, restaurantName: restaurantDetails.restaurantName, restaurantStarRating: restaurantsContentsDetails[indexPath.row].restaurantStarRating, restaurantCuisine: restaurantDetails.restaurantCuisine, restaurantLocality: restaurantsContentsDetails[indexPath.row].restaurantAddress.localityName, deliveryTiming: 30, restaurantOpensNextAt: restaurantsContentsDetails[indexPath.row].restaurantOpensNextAt,restaurantIsAvailable: restaurantsContentsDetails[indexPath.row].restaurantIsAvailable,isDeliverable:restaurantsContentsDetails[indexPath.row].isDeliverable))
            
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        favouriteRestaurantsView.getRestaurantTableViewCellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuDisplayDetailsPageViewController = UserMenuDisplayPageViewController()
        menuDisplayDetailsPageViewController.setRestaurantContentDetails(restaurantContentDetails: restaurantsContentsDetails[indexPath.row])
        navigationController?.pushViewController(menuDisplayDetailsPageViewController, animated: true)
    }
}


extension FavouriteRestaurantsViewController {
    
    
        private func getContentViewConstraints() -> [NSLayoutConstraint]{
            let contentViewConstraints = [favouriteRestaurantsView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor),
                                          favouriteRestaurantsView.bottomAnchor.constraint(equalTo : view.safeAreaLayoutGuide.bottomAnchor),
                                          favouriteRestaurantsView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                                          favouriteRestaurantsView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)]
                return contentViewConstraints
        }

    
}
