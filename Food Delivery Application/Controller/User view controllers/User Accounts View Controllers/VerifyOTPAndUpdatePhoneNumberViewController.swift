//
//  VerifyOTPAndUpdateUserDetailsViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/03/22.
//

import Foundation
import UIKit

class VerifyOTPAndUpdatePhoneNumberViewController : UserLoginOTPFormViewController {
    
    private var userAccountsViewModel : UserAccountsViewModel = UserAccountsViewModel()
    
    
    
    @objc override func didTapContinueToVerify(){
        if(userLoginOTPFormView.OTPTextField.text! == OTP){
            DispatchQueue.global().async { [self] in
                if(userAccountsViewModel.updateUserPhoneNumber(userId: UserIDUtils.shared.getUserId()  , phoneNumber: userPhoneNumber)){
                    DispatchQueue.main.async { [self] in
                        showToast(message: " Updated Phone Number Successfully", font: UIFont.systemFont(ofSize: 25, weight: .medium), fontColor: .white, backGroundColor: .systemBlue.withAlphaComponent(0.9))
                        navigationController?.popViewController(animated: true)
                    }
                }
            }
            
        }
        else{
            showAlert(message: "Enter valid OTP")
        }
    }
}
