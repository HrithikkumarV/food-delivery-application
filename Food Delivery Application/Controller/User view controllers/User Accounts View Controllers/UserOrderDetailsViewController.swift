//
//  UserOrderDetailsViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 20/03/22.
//

import Foundation
import UIKit

class UserOrderDetailsViewController : UIViewController,UserOrderDetailsPageViewDelegate{
    
     private var orderDetailsPageView : UserOrderDetailsPageViewProtocol!
     private var userOrdersViewModel : UserOrdersViewModel = UserOrdersViewModel()
     private var userAddressViewModel : UserAddressesViewModel = UserAddressesViewModel()
     private var  userCartViewModel : UserCartViewModel = UserCartViewModel.getUserCartViewModel()!
     private var orderDetails : OrderDetails!
     private var orderMenuDetailsList :[OrderMenuDetails]!
     private var billDetails : BillDetailsModel!
     private var deliveryAddress : UserAddressDetails!
     private var cancellationReason  = ""
     private var navigationBarTitleView : UserOrderDetailsPageViewNavigationBarTitleViewProtocol!
    
    private lazy var backArrowButton : UIBarButtonItem = {
        let  backArrowButton = NavigationBarUtils().createBackNavigationBarButton()
        backArrowButton.action = #selector(didTapBackArrow)
        backArrowButton.target = self
        return backArrowButton
    }()
    
    private var orderDetailsScrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .white
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.isScrollEnabled = true
        scrollView.bounces = false
        scrollView.keyboardDismissMode = .onDrag
        return scrollView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        initialiseViewElements()
        getDataAndUpdateUI()
        addDelegatesAndDatasource()
        setDefaultNavigationBarApprearance()
    }
    
    
    
    
    func setOrderDetailsAndOrderMenuDetails(orderDetails : OrderDetails,orderMenuDetailsList : [OrderMenuDetails]){
        self.orderDetails = orderDetails
        self.orderMenuDetailsList = orderMenuDetailsList
        
    }
    
    @objc func didTapBackArrow(){
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
       modifyViewElementsBasedOnOrderStatus()
        modifyViewBasedOnRatingAndFeedback()
    }
    
    func modifyViewElementsBasedOnOrderStatus(){
        if(orderDetails.orderStatus == .Pending){
            orderDetailsPageView.cancelOrderButton.isHidden =  false
            orderDetailsPageView.restaurantRatingLabel.isHidden = true
            orderDetailsPageView.reOrderButton.isHidden = true
            orderDetailsPageView.rateOrderButton.isHidden = true
        }
        else if(orderDetails.orderStatus == .Delivered){
            orderDetailsPageView.cancelOrderButton.isHidden = true
            orderDetailsPageView.reOrderButton.isHidden = false
            orderDetailsPageView.rateOrderButton.isHidden = false
            orderDetailsPageView.restaurantRatingLabel.isHidden = false
        }
        else if(orderDetails.orderStatus == .Cancelled || orderDetails.orderStatus == .Declined){
            orderDetailsPageView.cancelOrderButton.isHidden = true
            orderDetailsPageView.rateOrderButton.isHidden = true
            orderDetailsPageView.reOrderButton.isHidden = false
            orderDetailsPageView.restaurantRatingLabel.isHidden = true
            orderDetailsPageView.reOrderButton.rightAnchor.constraint(equalTo: orderDetailsPageView.rightAnchor,constant: -10).isActive = true
            orderDetailsPageView.reOrderButton.leftAnchor.constraint(equalTo: orderDetailsPageView.leftAnchor,constant: 10).isActive = true
        }
        else{
            orderDetailsPageView.cancelOrderButton.isHidden = true
            orderDetailsPageView.reOrderButton.isHidden = true
            orderDetailsPageView.rateOrderButton.isHidden = true
            orderDetailsPageView.restaurantRatingLabel.isHidden = true
        }
        
    }
    
    func modifyViewBasedOnRatingAndFeedback(){
        if(orderDetails.starRating != 0 && orderDetails.feedback == ""){
            orderDetailsPageView.rateOrderButton.removeFromSuperview()
            orderDetailsPageView.feedbackButton.isHidden = false
            orderDetailsPageView.updateRatingAndFeedback(rating: orderDetails.starRating, restaurantFeedback: orderDetails.feedback)
        }
        else if(orderDetails.starRating != 0 && orderDetails.feedback != ""){
            orderDetailsPageView.updateRatingAndFeedback(rating: orderDetails.starRating, restaurantFeedback: orderDetails.feedback)
            orderDetailsPageView.feedbackButton.removeFromSuperview()
            orderDetailsPageView.rateOrderButton.removeFromSuperview()
            orderDetailsPageView.reOrderButton.leftAnchor.constraint(equalTo:orderDetailsPageView.leftAnchor,constant: 10).isActive = true
        }
        else{
            orderDetailsPageView.feedbackButton.isHidden = true
        }
     
    }
    
    
    
     func didTapCancelOrder(){
        
        let alertController = UIAlertController(title: "Cancel Order", message: "Please give the reason to Cancel the order", preferredStyle: .alert)

        alertController.addTextField { (textField) in
            textField.placeholder = "Reason to Cancel Order"
            textField.becomeFirstResponder()
        }

        let cancelAction = UIAlertAction(title: "leave It", style: .cancel, handler: nil)
        let DeclineAction = UIAlertAction(title: "Cancel Order", style: .destructive) { [self] _ in
            let reason = alertController.textFields![0].text
            if(reason!.count > 0){
                DispatchQueue.global().async { [self] in
                    if(userOrdersViewModel.getOrderStatus(orderId: orderDetails.orderModel.orderId) == .Pending){
                        if(userOrdersViewModel.updateOrderCancellationReason(orderId: orderDetails.orderModel.orderId, orderCancellationReason: reason!)){
                            if(userOrdersViewModel.updateOrderStatus(orderId: orderDetails.orderModel.orderId, orderStatus: .Cancelled)){
                                DispatchQueue.main.async { [self] in
                                    showToast(message: "Order Cancelled", font: UIFont.systemFont(ofSize: 25, weight: .medium), fontColor: .white, backGroundColor: .systemRed.withAlphaComponent(0.9))
                                    navigationController?.popViewController(animated: true)
                                }
                            }
                    }
                   
                }
                    else{
                        DispatchQueue.main.async { [self] in
                            showToast(message: "Order Cannot be cancelled now", font: UIFont.systemFont(ofSize: 25, weight: .medium), fontColor: .white, backGroundColor: .systemBlue.withAlphaComponent(0.9))
                        }
                    }
                }
                }

        }

        alertController.addAction(cancelAction)
        alertController.addAction(DeclineAction)

        present(alertController, animated: true, completion: nil)
        
        
    }
    
      func didTapReorderButton(){
          DispatchQueue.global().async { [self] in
              if(UserCartViewModel.restaurantNameInCart != "" && UserCartViewModel.restaurantNameInCart != orderDetails.restaurantName){
                  DispatchQueue.main.async { [self] in
                      let alert = UIAlertController(title: "Replace Cart Item?", message: "Your cart contains dishes from \(UserCartViewModel.restaurantNameInCart). Do you want to discard the selection and add dishes from \(orderDetails.restaurantName)?", preferredStyle: .alert)
                      alert.addAction(UIAlertAction(title: "No", style: .cancel,handler: nil))
                      alert.addAction(UIAlertAction(title: "Yes", style: .default,handler: { [self]_ in
                          DispatchQueue.global().async { [self] in
                              if(userCartViewModel.clearCart()){
                                  UserCartViewModel.totalPrice = self.billDetails.itemTotal
                                  UserCartViewModel.cartItemsCount = getOrderTotalQuantity()
                                  UserCartViewModel.restaurantNameInCart = orderDetails.restaurantName
                                  for order in orderMenuDetailsList{
                                      if(userCartViewModel.addMenuToCart(cartDetails: (menuId: order.menuId, restaurantId: orderDetails.orderModel.restaurantId, quantity: order.quantity))){
                                          continue
                                      }
                                  }
                                  DispatchQueue.main.async { [self] in
                                      userCartViewModel.sendNotificationTomoveToFullMenuView()
                                      pushToRestaurantPageAndCartToReorder()
                                  }
                              }
                             
                          }
                         
                      }))
                      present(alert,animated: true)
                  }
                 
              }
              else{
                  if(self.userCartViewModel.clearCart()){
                      UserCartViewModel.totalPrice = self.billDetails.itemTotal
                      UserCartViewModel.cartItemsCount = getOrderTotalQuantity()
                      UserCartViewModel.restaurantNameInCart = orderDetails.restaurantName
                      for order in orderMenuDetailsList{
                          if(userCartViewModel.addMenuToCart(cartDetails: (menuId: order.menuId, restaurantId: orderDetails.orderModel.restaurantId, quantity: order.quantity))){
                              continue
                          }
                      }
                      DispatchQueue.main.async { [self] in
                          userCartViewModel.sendNotificationTomoveToFullMenuView()
                          pushToRestaurantPageAndCartToReorder()
                      }
                  }
                  
                 
              }
          }
          
        
    }
    
    
   
      func didTapRateOrderButton(){
        let starRatingAlertControllerView = StarRatingElement(title: "Enjoyed your meal ?", message: "Tap a star to rate your meal", preferredStyle: .alert)
          
        starRatingAlertControllerView.addAction(UIAlertAction(title: "NOT NOW", style: .cancel,handler: nil))
        starRatingAlertControllerView.addAction(UIAlertAction(title: "SUBMIT", style: .default,handler: { [self]_ in
            if(starRatingAlertControllerView.starRating != 0){
                DispatchQueue.global().async { [self] in
                    if(userOrdersViewModel.updateStarRatingForFood(starRating: starRatingAlertControllerView.starRating, orderId: orderDetails.orderModel.orderId)){
                        if(userOrdersViewModel.updateRestaurantStarRating(starRating: starRatingAlertControllerView.starRating, restaurantId: orderDetails.orderModel.restaurantId)){
                            DispatchQueue.main.async { [self] in
                                orderDetails.starRating =  starRatingAlertControllerView.starRating
                                modifyViewBasedOnRatingAndFeedback()
                            }
                        }
                    }
                }
               
            }
            
        }))
//          self.addChild(starRatingAlertControllerView)
//          self.view.addSubview(starRatingAlertControllerView.view)
//          starRatingAlertControllerView.view.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//          starRatingAlertControllerView.view.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
          present(starRatingAlertControllerView, animated: true)
         
    }
    
    
    
    
    
    
    
    
     func didTapFeedBack(){
        let feedBackAlertControllerView = FeedbackElement(title: "Feedback", message: "", preferredStyle: .alert)
        feedBackAlertControllerView.addAction(UIAlertAction(title: "NOT NOW", style: .cancel,handler: nil))
        feedBackAlertControllerView.addAction(UIAlertAction(title: "SUBMIT", style: .default,handler: { [self]_ in
            if(feedBackAlertControllerView.feedbackTextView.text != "" && feedBackAlertControllerView.feedbackTextView.textColor != .systemGray){
                let feedbackTextViewtext = feedBackAlertControllerView.feedbackTextView.text!
                DispatchQueue.global().async { [self] in
                    if(userOrdersViewModel.updateFeedBack(feedback:feedbackTextViewtext , orderId: orderDetails.orderModel.orderId)){
                        DispatchQueue.main.async { [self] in
                            orderDetails.feedback =  feedBackAlertControllerView.feedbackTextView.text
                            modifyViewBasedOnRatingAndFeedback()
                        }
                    }
                }
                
            }
        }))
//         self.addChild(feedBackAlertControllerView)
//         self.view.addSubview(feedBackAlertControllerView.view)
//         feedBackAlertControllerView.view.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//         feedBackAlertControllerView.view.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
         present(feedBackAlertControllerView, animated: true)
    }
    
    func pushToRestaurantPageAndCartToReorder(){
        let userMenuDisplayPageViewController = UserMenuDisplayPageViewController()
        var restaurantDetails = userOrdersViewModel.getRestaurantDetails(restaurantId: orderDetails.orderModel.restaurantId)
        if(deliveryAddress.addressDetails.localityName == restaurantDetails.restaurantAddress.localityName && deliveryAddress.addressDetails.pincode == restaurantDetails.restaurantAddress.pincode){
            restaurantDetails.isDeliverable =  true
        }
        userMenuDisplayPageViewController.setRestaurantContentDetails(restaurantContentDetails: restaurantDetails)
        navigationController?.pushViewController(userMenuDisplayPageViewController, animated: false)
        navigationController?.pushViewController(UserCartViewController(), animated: true)
    }
    
    
    
     private func initialiseView(){
        view.backgroundColor = .white
         view.addSubview(orderDetailsScrollView)
         orderDetailsPageView = UserOrderDetailsPageView()
         orderDetailsPageView.delegate = self
         orderDetailsPageView.translatesAutoresizingMaskIntoConstraints = false
         orderDetailsScrollView.addSubview(orderDetailsPageView)
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getContentViewConstraints())
    }
    
    
     private func initialiseViewElements(){
        navigationBarTitleView = UserOrderDetailsPageNavigationBarTitleView()
        navigationItem.leftBarButtonItem = backArrowButton
        navigationItem.titleView = navigationBarTitleView
        navigationController?.navigationBar.backgroundColor = .clear
    }
    
    
     private func addDelegatesAndDatasource(){
         orderDetailsPageView.menuDetailsTableView.delegate = self
         orderDetailsPageView.menuDetailsTableView.dataSource = self
    }
    
    func getDataAndUpdateUI(){
        DispatchQueue.global().async {
            [self] in
            billDetails = userOrdersViewModel.getBillDetails(orderId: orderDetails.orderModel.orderId)
            deliveryAddress = userAddressViewModel.getUserAddress(userAddressId: orderDetails.orderModel.userAddressId)
            cancellationReason = userOrdersViewModel.getOrderCancellationReason(orderId: orderDetails.orderModel.orderId)
            DispatchQueue.main.async { [self] in
                updateDataInViewElements()
            }
        }
    }
    
    private func updateDataInViewElements(){
        navigationBarTitleView.updateNavigationBarTitle(menuCount: getOrderTotalQuantity(), totalPrice: orderDetails.orderTotal, orderStatus: orderDetails.orderStatus)
        orderDetailsPageView.updateCancelOrderReason(cancelReason: cancellationReason, orderStatus: orderDetails.orderStatus)
        orderDetailsPageView.updateInstructionsToRestaurant(instructionsToRestaurantText: orderDetails.instructionToRestaurant)
        orderDetailsPageView.UpdateOrderAndUserDetailsContents(orderId: orderDetails.orderModel.orderId, dateAndTime: orderDetails.orderModel.dateAndTime , restaurantName: orderDetails.restaurantName, deliveryAddress:deliveryAddress.addressDetails)
        orderDetailsPageView.updateBillDetails(itemTotalPrice: billDetails.itemTotal, restaurantPackagingChargePrice: billDetails.restaurantPackagingCharges, deliveryFeePrice: billDetails.deliveryFee, totalAmount: billDetails.totalCost,discountAmount: billDetails.itemDiscount,restaurantGST: billDetails.restaurantGST)
        orderDetailsPageView.updateRatingAndFeedback(rating: orderDetails.starRating, restaurantFeedback: orderDetails.feedback)
        orderDetailsPageView.setMenuDetailsTableViewHeight(numberOfRows: orderMenuDetailsList.count)
    }
    
    func getOrderTotalQuantity() -> Int{
        var quantity = 0
        for order in orderMenuDetailsList{
            quantity += order.quantity
        }
        return quantity
    }
}

extension UserOrderDetailsViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        orderMenuDetailsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menuDetails = orderMenuDetailsList[indexPath.row]
        return orderDetailsPageView.createMenuDetailsContentView(indexPath: indexPath, tableView: tableView, menuCellContents:(menuName: menuDetails.menuName, menuTarianType: menuDetails.menuTarianType, menuPriceSubTotal: (menuDetails.price * menuDetails.quantity), quantity: menuDetails.quantity))
       
    }
}


extension UserOrderDetailsViewController{
    
   
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        orderDetailsPageView.activateContentViewBottomAnchorWithRespectToRestaurantRatingLabelConstraints()
    }
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [orderDetailsScrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                                         orderDetailsScrollView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                                         orderDetailsScrollView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
                                         orderDetailsScrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
        private func getContentViewConstraints() -> [NSLayoutConstraint]{
            let contentViewConstraints = [orderDetailsPageView.topAnchor.constraint(equalTo:orderDetailsScrollView.topAnchor),
                                          orderDetailsPageView.bottomAnchor.constraint(equalTo : orderDetailsScrollView.bottomAnchor),
                                          orderDetailsPageView.leftAnchor.constraint(equalTo: orderDetailsScrollView.leftAnchor),
                                          orderDetailsPageView.rightAnchor.constraint(equalTo: orderDetailsScrollView.rightAnchor),
                                          orderDetailsPageView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor)]
    
            return contentViewConstraints
        }
    
    
}

