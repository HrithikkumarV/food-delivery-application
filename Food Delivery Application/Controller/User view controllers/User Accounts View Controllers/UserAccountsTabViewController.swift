//
//  UserAccountsTabViewController.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 11/11/21.
//

import Foundation
import UIKit

class UserAccountsTabViewController : UIViewController,UserAccountsPageViewDelegate,UserAccountsPageLoginViewDelegate{
     private var userAccountsPageView : UserAccountsPageViewProtocol!
     private var userAccountsPageLoginView : UserAccountsPageLoginViewProtocol!
     private var userOrdersViewModel : UserOrdersViewModel = UserOrdersViewModel()
     private var userAccountsViewModel : UserAccountsViewModel = UserAccountsViewModel()
     private var userDetails  : UserDetails!
     private var activeOrderIdList : [String] = []
     private var activeOrderDetailsInDictionaryFormat : [String : OrderDetails] = [:]
     private var activeOrderFoodDetails : [String : [OrderMenuDetails]] = [:]
     private var pastOrderIdList : [String] = []
     private var pastOrderDetailsInDictionaryFormat : [String : OrderDetails] = [:]
     private var pastOrderFoodDetails : [String : [OrderMenuDetails]] = [:]
     private var availableOrderStatus : [OrderStatusForUser] = []
     private var splitOrdersBasedOnOrderStatus : [OrderStatusForUser : [String]] = [:]
     private var pastOrderOffset : Int = 0
     private var ordersTableViewHeightConstraint : NSLayoutConstraint!
     private var userAccountsScrollView : UIScrollView = {
        let scrollView = UIScrollView()
         scrollView.keyboardDismissMode = .onDrag
         scrollView.backgroundColor = .white
         scrollView.translatesAutoresizingMaskIntoConstraints = false
         return scrollView
    }()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        initialiseView()
        addDelegates()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        modifyViewBasedOnLoginStatus()
    }
    private func getuserDetailsAndUpdateView(){
            userDetails = userAccountsViewModel.getUserDetails(userId: (UserIDUtils.shared.getUserId()))
            DispatchQueue.main.async { [self] in
                userAccountsPageView.updateUserDetails(userName: userDetails.userName, userPhoneNumber: userDetails.userPhoneNumber)
            }
        
    }
    
    private func getOrderDetailsAndUpdateView(){
            userOrdersViewModel.clearStoredData()
            getActiveOrderDetails()
            getPastOrderDetails()
            DispatchQueue.main.async { [self] in
                splitOrdersBasedOnAvailablityOfOrderStatus()
                userAccountsPageView.orderDetailsTableView.reloadData()
                updateOrderTableViewHeight()
                displayViewMoreButtonBasedOnTheCountOfPastOrders()
            }
            
        
    }
    
    func modifyViewBasedOnLoginStatus(){
        showActivityIndicator()
        DispatchQueue.global().async { [self] in
            if(UserIDUtils.shared.getUserId() == 0){
                DispatchQueue.main.async { [self] in
                    userAccountsPageView.isHidden = true
                    userAccountsPageLoginView.isHidden = false
                    hideActivityIndicator()
                }
            }
            else{
                    getuserDetailsAndUpdateView()
                    getOrderDetailsAndUpdateView()
                    DispatchQueue.main.async { [self] in
                        userAccountsPageView.isHidden = false
                        userAccountsPageLoginView.isHidden = true
                        hideActivityIndicator()
                    }
                }
        }
    }
    
    func displayViewMoreButtonBasedOnTheCountOfPastOrders(){
        
        if(pastOrderIdList.count >= 5){
            userAccountsPageView.viewMoreOrdersButton.isHidden = false
        }
        else if(pastOrderIdList.count >= 1){
            userAccountsPageView.viewMoreOrdersButton.setTitle("No More Orders", for: .normal)
            userAccountsPageView.viewMoreOrdersButton.setTitleColor(.systemGray, for: .normal)
        }
        else{
            userAccountsPageView.viewMoreOrdersButton.isHidden = true
        }
    }
    
    func updateOrderTableViewHeight(){
        ordersTableViewHeightConstraint?.isActive = false
        ordersTableViewHeightConstraint = userAccountsPageView.orderDetailsTableView.bottomAnchor.constraint(equalTo: userAccountsPageView.orderDetailsTableView.topAnchor, constant: CGFloat(availableOrderStatus.count * 100)  + CGFloat(CGFloat((activeOrderIdList.count + pastOrderIdList.count)) * userAccountsPageView.getOrderTabelViewCellHeight() + 10))
        ordersTableViewHeightConstraint?.isActive = true
    }
    
    
    
    
    
     func getActiveOrderDetails(){
        let activeOrderDetails = userOrdersViewModel.getActiveOrderDetails(userId: UserIDUtils.shared.getUserId() )
        activeOrderIdList = activeOrderDetails.activeOrderIdList
        activeOrderDetailsInDictionaryFormat  = activeOrderDetails.activeOrderDetailsInDictionaryFormat
        activeOrderFoodDetails = userOrdersViewModel.getMenuDetailsInActiveOrderFoodDetails(userId: UserIDUtils.shared.getUserId())
    }
    
     func getPastOrderDetails(){
        pastOrderOffset = 0
        let pastOrderDetails = userOrdersViewModel.getpastOrderDetails(userId: UserIDUtils.shared.getUserId() , offSet: pastOrderOffset, limit: 5)
        pastOrderIdList = pastOrderDetails.pastOrderIdList
        pastOrderDetailsInDictionaryFormat = pastOrderDetails.pastOrderDetailsInDictionaryFormat
        pastOrderFoodDetails = userOrdersViewModel.getMenuDetailsInpastOrderFoodDetails(orderIdList: pastOrderDetails.pastOrderIdList)
    }
    
    
     func getPastOrderDetailsAndMergeWithCurrentPastOrderDetails(){
        let pastOrderDetails = userOrdersViewModel.getpastOrderDetails(userId: UserIDUtils.shared.getUserId() , offSet: pastOrderOffset, limit: 5)
        pastOrderIdList += pastOrderDetails.pastOrderIdList
        pastOrderDetailsInDictionaryFormat.merge(pastOrderDetails.pastOrderDetailsInDictionaryFormat) {  (current, _) in current
        }
        pastOrderFoodDetails.merge(userOrdersViewModel.getMenuDetailsInpastOrderFoodDetails( orderIdList: pastOrderDetails.pastOrderIdList)){ (current, _) in current
        }

    }
    
     func splitOrdersBasedOnAvailablityOfOrderStatus(){
        availableOrderStatus = []
        splitOrdersBasedOnOrderStatus = [:]
        if(activeOrderIdList.count > 0){
            availableOrderStatus.append(.ActiveOrder)
            splitOrdersBasedOnOrderStatus[.ActiveOrder] = activeOrderIdList
            
        }
        if(pastOrderIdList.count > 0){
            availableOrderStatus.append(.PastOrder)
            splitOrdersBasedOnOrderStatus[.PastOrder] = pastOrderIdList
        }
    }
    
    
    
    func addDismissButtonToNavigationBar(){
        let dismiss = UIBarButtonItem()
        dismiss.title = "Switch App"
        dismiss.style = .done
        dismiss.target = self
        dismiss.action = #selector(didTapDismiss)
        navigationItem.rightBarButtonItem = dismiss

    }
    
    
    @objc func didTapDismiss(){
        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
    
    
    func didTapLogin(){
        let logIn = UINavigationController(rootViewController: UserLoginViewController())
        logIn.modalPresentationStyle = .fullScreen
        present(logIn, animated: true, completion: nil)
    }
    
     func didTapLogout(){
        let alert = UIAlertController(title: "LOGOUT", message: "Are you sure you want to Logout ?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel,handler: nil))
        alert.addAction(UIAlertAction(title: "Yes,Logout", style: .default,handler: { [self]_ in
            if(UserIDUtils.shared.deleteUserId()){
                viewWillAppear(true)
            }
               
        }))
        present(alert,animated: true)
       
    }
    
     func didTapViewMorePastOrders(){
        pastOrderOffset += 5
         DispatchQueue.global().async { [self] in
             getPastOrderDetailsAndMergeWithCurrentPastOrderDetails()
             splitOrdersBasedOnAvailablityOfOrderStatus()
             DispatchQueue.main.async { [self] in
                userAccountsPageView.orderDetailsTableView.reloadData()
                updateOrderTableViewHeight()
                if(pastOrderOffset != pastOrderIdList.count){
                    userAccountsPageView.viewMoreOrdersButton.setTitle("No More Orders", for: .normal)
                    userAccountsPageView.viewMoreOrdersButton.setTitleColor(.systemGray, for: .normal)
                }
             }
         }
    }
    
     func didTapFavouriteRestaurants(){
        tabBarController?.navigationController?.pushViewController(FavouriteRestaurantsViewController(), animated: true)
    }
    
     func didTapManageAddresses(){

        tabBarController?.navigationController?.pushViewController(ManageAddressesViewController(), animated: true)
    }
    
     func didTapEditButton(){
        tabBarController?.navigationController?.pushViewController(EditUserDetailsViewController(), animated: true)
    }
    
    
     private func initialiseView(){
        title = "Accounts"
        view.backgroundColor  = .white
         view.addSubview(userAccountsScrollView)
         userAccountsPageLoginView = UserAccountsPageLoginView()
         userAccountsPageLoginView.translatesAutoresizingMaskIntoConstraints = false
         userAccountsPageLoginView.delegate = self
         userAccountsScrollView.addSubview(userAccountsPageLoginView)
         userAccountsPageView = UserAccountsPageView()
         userAccountsPageView.translatesAutoresizingMaskIntoConstraints = false
         userAccountsPageView.delegate = self
         userAccountsScrollView.addSubview(userAccountsPageView)
         NSLayoutConstraint.activate(getScrollViewConstraints())
         NSLayoutConstraint.activate(getUserAccountsPageViewConstraints())
         NSLayoutConstraint.activate(getUserAccountsPageLoginViewConstraints())
         setDefaultNavigationBarApprearance()
         addDismissButtonToNavigationBar()
        
    }
    
     private func addDelegates(){
         userAccountsPageView.orderDetailsTableView.delegate = self
         userAccountsPageView.orderDetailsTableView.dataSource = self
    }
    
    
}

extension UserAccountsTabViewController : UITableViewDelegate, UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        availableOrderStatus.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            return UIView()
        }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let orderStatusLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width , height: 50))
        orderStatusLabel.backgroundColor = .white
        orderStatusLabel.font = UIFont.systemFont(ofSize: 25, weight: .bold)
        orderStatusLabel.layer.cornerRadius  = 5
        orderStatusLabel.backgroundColor = .white
        
        if(availableOrderStatus[section] == .ActiveOrder){
            orderStatusLabel.text = "Active Orders"
            orderStatusLabel.textColor = .systemGreen
            
        }
        else{
            orderStatusLabel.text = "Past Orders"
            orderStatusLabel.textColor = .black
            
        }
        return orderStatusLabel
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return splitOrdersBasedOnOrderStatus[availableOrderStatus[section]]!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let orderId = splitOrdersBasedOnOrderStatus[availableOrderStatus[indexPath.section]]![indexPath.row]
        if(availableOrderStatus[indexPath.section] == .ActiveOrder){
            return userAccountsPageView.createOrdersTableViewCell(indexPath: indexPath, tableView: tableView, orderCellContents: (restaurantName: activeOrderDetailsInDictionaryFormat[orderId]!.restaurantName, orderStatus: activeOrderDetailsInDictionaryFormat[orderId]!.orderStatus, orderTotal: activeOrderDetailsInDictionaryFormat[orderId]!.orderTotal, menuNameAndQuantities: userOrdersViewModel.getMenuNamesAndQuantityListInActiveOrders(orderId: orderId), dateAndTime: activeOrderDetailsInDictionaryFormat[orderId]!.orderModel.dateAndTime ))
        }
        else{
            return userAccountsPageView.createOrdersTableViewCell(indexPath: indexPath, tableView: tableView, orderCellContents: (restaurantName: pastOrderDetailsInDictionaryFormat[orderId]!.restaurantName, orderStatus: pastOrderDetailsInDictionaryFormat[orderId]!.orderStatus, orderTotal: pastOrderDetailsInDictionaryFormat[orderId]!.orderTotal, menuNameAndQuantities: userOrdersViewModel.getMenuNamesAndQuantityListInPastOrders(pastOrderFoodDetails: pastOrderFoodDetails[orderId] ?? []), dateAndTime: pastOrderDetailsInDictionaryFormat[orderId]!.orderModel.dateAndTime ))
            
        }
       
    }
    
    
   
                
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        userAccountsPageView.getOrderTabelViewCellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let orderId = splitOrdersBasedOnOrderStatus[availableOrderStatus[indexPath.section]]![indexPath.row]
        let orderDetailsPageViewController =  UserOrderDetailsViewController()
        if(availableOrderStatus[indexPath.section] == .ActiveOrder){
            orderDetailsPageViewController.setOrderDetailsAndOrderMenuDetails(orderDetails: activeOrderDetailsInDictionaryFormat[orderId]!, orderMenuDetailsList: activeOrderFoodDetails[orderId]!)
        }
        else{
            orderDetailsPageViewController.setOrderDetailsAndOrderMenuDetails(orderDetails: pastOrderDetailsInDictionaryFormat[orderId]!, orderMenuDetailsList: pastOrderFoodDetails[orderId]!)
        }
        tabBarController?.navigationController?.pushViewController(orderDetailsPageViewController, animated: true)
    }
    
}

enum OrderStatusForUser{
    case ActiveOrder
    case PastOrder
}

extension UserAccountsTabViewController{
    
        private func getScrollViewConstraints() -> [NSLayoutConstraint]{
            let scrollViewConstraints = [userAccountsScrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                                         userAccountsScrollView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                                         userAccountsScrollView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
                                         userAccountsScrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)]
            return scrollViewConstraints
        }
    
        private func getUserAccountsPageViewConstraints() -> [NSLayoutConstraint]{
            let contentViewConstraints = [userAccountsPageView.topAnchor.constraint(equalTo:userAccountsScrollView.topAnchor),
                                          userAccountsPageView.bottomAnchor.constraint(equalTo :userAccountsScrollView.bottomAnchor),
                                          userAccountsPageView.leftAnchor.constraint(equalTo: userAccountsScrollView.leftAnchor),
                                          userAccountsPageView.rightAnchor.constraint(equalTo: userAccountsScrollView.rightAnchor),
                                          userAccountsPageView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor)]
            return contentViewConstraints
        }
    
    private func getUserAccountsPageLoginViewConstraints() -> [NSLayoutConstraint]{
        let contentViewConstraints = [userAccountsPageLoginView.topAnchor.constraint(equalTo:view.topAnchor),
                                      userAccountsPageLoginView.bottomAnchor.constraint(equalTo :view.bottomAnchor),
                                      userAccountsPageLoginView.leftAnchor.constraint(equalTo: view.leftAnchor),
                                      userAccountsPageLoginView.rightAnchor.constraint(equalTo: view.rightAnchor),
                                
                                    ]
        return contentViewConstraints
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if(userAccountsPageView.viewMoreOrdersButton.frame.maxY != 0){
            userAccountsPageView.removeHeightAnchorConstraints()
            userAccountsPageView.heightAnchor.constraint(equalToConstant:  userAccountsPageView.viewMoreOrdersButton.frame.maxY + 100).isActive = true
        }
        else{
            userAccountsPageView.removeHeightAnchorConstraints()
            userAccountsPageView.heightAnchor.constraint(equalToConstant: view.frame.maxY).isActive = true
        }
        
        
    }

    
}

        
