//
//  viewProtocols.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 15/01/22.
//

import Foundation
import UIKit

protocol MainViewProtocol : UIView{
    var delegate : MainViewDelegate? { get set }
}


protocol AddressViewProtocol : UIView{
    var delegate: AddressViewDelegate? { get set }
     
   
    var doorNoAndBuildingNameAndBuildingNoTextField  : UITextField { get}
     
    var streetNameTextfield : UITextField { get}
     
    var landmarkTextfield : UITextField { get}
     
    var localityButton : UIButton { get}
     
    var cityTextfield : UITextField { get}
    var stateTextField : UITextField { get }
     
    var countryTextfield : UITextField { get }
     
    var pincodeTextField : UITextField { get}
    
    func getMaxYOfLastObject() -> CGFloat
}

protocol OTPFormViewProtocol : UIView{
    var delegate : OTPFormViewDelegate? {get set}
    
    func updatePhonenumberInVerificationExplanationLabel(phoneNumber : String)
    
    var OTPTextField : UITextField {get}
    
    func getMaxYOfLastObject() -> CGFloat
}

protocol MoveToFullMenuViewProtocol  :UIView{
    
    
    var delegate : MoveToFullMenuViewDelegate? {get set}
    
    func updateRestaurantNameInCartLabelText(restaurantNameInUserCart : String)
    
    
    func updateMenuItemCountAndPriceLabelText(menuCount : Int,totalPrice : Int)
    
    
    func hideOrShowmoveToFullMenuViewBasedOnMenuCount(menuCount :Int)
    
   
}
