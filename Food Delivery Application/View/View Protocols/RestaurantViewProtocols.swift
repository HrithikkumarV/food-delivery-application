//
//  RestaurantViewProtocols.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 15/01/22.
//

import Foundation
import UIKit

protocol RestaurantLoginViewProtocol : UIView{
    var delegate : RestaurantLoginViewDelegate? { get set }
    var phoneNumberTextField : UITextField {get}
    var passwordTextField : UITextField {get}
    var changePasswordButton : UIButton {get}
    var createAccountButton : UIButton {get}
    var loginButton : UIButton {get}
    var stackView : UIStackView {get}
    
}

protocol RestaurantCreateAccountPhoneNumberFormViewProtocol : UIView{
    var delegate : RestaurantCreateAccountPhoneNumberFormViewDelegate? {get set}
    var phoneNumberTextField : UITextField {get}
    var verifyWithOTPButton : UIButton {get}
    var welcomeLabel : UILabel {get}
}

protocol RestaurantCreateAccountRestaurantDetailsViewProtocol : UIView{
    var delegate : RestaurantCreateAccountRestaurantDetailsViewDelegate? {get set}
   
    
    var restaurantProfileImageSectionView : UIView {get}
    
    var continueTosetAddressButton : UIButton {get}
    
    var stackViewForRestaurantDetails : UIStackView {get}
    
    
    var restaurantNameTextField : UITextField {get}
    
    
    var restaurantCuisineButton : UIButton {get}
    
    var restaurantDescriptionTextView : UITextView {get}
    
    
    var addRestaurantProfileImageButton : UIButton {get}
    
    var restaurantProfileImageView :  UIImageView {get}
    
}

protocol RestaurantCreateAccountAuthenticationDetailsViewProtocol : UIView{
    
    var delegate : RestaurantCreateAccountAuthenticationDetailsViewDelegate? {get set}
    
   
    var createAccountButton : UIButton {get}
    var stackViewForAuthenticationDetails : UIStackView {get}
    
    var secretCodeTextField : UITextField {get}
    
    var passwordTextField : UITextField {get}
    var confirmPasswordTextField : UITextField {get}
    
}

protocol ChangePasswordPhoneNumberFormViewProtocol : UIView{
    var delegate : ChangePasswordPhoneNumberFormViewDelegate? {get set}
    
   
    
    var phoneNumberTextField : UITextField {get}
    
    
    var verifyButton : UIButton {get }
    
  
    
}

protocol ChangePasswordChangePasswordFormViewProtocol : UIView{
    var delegate : ChangePasswordChangePasswordFormViewDelegate? {get set}
    
     var saveChangesButton : UIButton {get}
     var stackView : UIStackView {get}
    
     var passwordTextField : UITextField {get}
     var confirmPasswordTextField :  UITextField {get}
}

protocol RestaurantAddMenuDetailsViewProtocol : UIView{
    var delegate : RestaurantAddMenuDetailsViewDelegate? {get set}
    
    var saveButton : UIButton {get}
    var stackViewForMenuDetails : UIStackView {get}

    var menuNameTextField : UITextField {get}
    
    var menuPriceTextField :  UITextField {get}
   
    var menuDescriptionTextView : UITextView {get}
    
    var menuTarianTypeDropDownListButton : UIButton {get}
    
    var menuCategoryTypeLabel : UILabel {get}

    
    var addMenuImageButton : UIButton {get}
    var menuImageView : UIImageView  {get}
    
    var menuImageSectionView : UIView {get}
    
   
    
}

protocol RestaurantEditMenuDetailsViewProtocol : RestaurantAddMenuDetailsViewProtocol{
    var subDelegate : RestaurantEditMenuDetailsViewDelegate?{get set}
    
     var menuIsAvailableView : UIView {get}
    
     var datePicker : UIDatePicker {get}
     var  menuNextAvailbaleDatePickerLabel : UILabel {get}

    
     var  changeMenuImageButton :  UIButton {get}
    
    func changeAddOrChangeImageButtonTitle(menuImage : UIImage?,changeMenuImageButton  : UIButton)
    var removeImageButton :  UIButton {get}
    
    
    var menuDelistLabel : UILabel {get}
     var menuDelistToggleSwitch : UISwitch {get}
    
    var menuDelistToggleSwitchView : UIView {get}
    
    
    var menuIsAvailableLabel :  UILabel {get}
    
     var menuIsAvailableToggleSwitch : UISwitch {get}
   
    func hideSetNextAvailablityItems()
    
    func showSetNextAvailablityItems()
    
}
protocol MenuCategoryHeaderViewProtocol : UIView{
    var delegate : MenuCategoryHeaderViewDelegate? {get set}
    
    var menuCategoryButton : UIButton {get}
    
    var editCategoryButton : UIButton {get}
    
    var addMenuButton : UIButton {get}
    
    var removeCategoryButton : UIButton {get}
   
    
}

protocol DisplayMenuDetailsInRestaurantViewProtocol : UIView ,RestaurantMenuElementsTableViewProtocol{
    var delegate : DisplayMenuDetailsInRestaurantViewDelegate? {get set}
    
    
    
    var addMenuCategoryButton : UIButton {get}
    var addMenuCategoryTextField : UITextField {get}
    var addMenuCategoryView : UIView {get}
    var saveCategoryButton : UIButton {get}
    
     var setUpCategoryAndMenusLabel : UILabel {get}
    
    func getMenuCategoryView() -> MenuCategoryHeaderViewProtocol
    func hideAddMenuCategoryViewItems()
    func showAddMenuCategoryViewItems()
   
}

protocol RestaurantMenuElementsTableViewProtocol{
    
    var menuTableView : UITableView {get}
    
    func getMenuTabelViewCellHeight() -> CGFloat

    

    func createMenuTableViewCell(indexPath : IndexPath ,tableView : UITableView, menuCellContents : (menuImage : Data? , menuName : String , menuDescription : String, menuPrice : Int , menuTarianType : String, menuAvailableNextAt  : String ,menuStatus  : Int,menuAvailable : Int, menuId : Int)) -> RestaurantMenuTableViewCellProtocol
}


protocol RestaurantMenuTableViewCellProtocol : UITableViewCell{
    var delegate : RestaurantMenuTableViewCellDelegate? { get set }
    
    var editMenuCellButton : UIButton {get}
    
    var removeMenuCellButton : UIButton {get}
    
    
    func modifyMenuBasedOnTheAvailability(menuAvailable : Int,menuStatus : Int,menuNextAvailableAt : String)
}

protocol SearchMenusInDisplayedRestaurantMenuDetailsViewProtocol : UIView{
     var delegate : SearchMenuInDisplayedRestaurantMenuDetailsViewDelegate? { get set }
    
    var menuTableView : UITableView {get}
    
    func getMenuTabelViewCellHeight() -> CGFloat
    
    
   
    
    
    func createMenuTableViewCell(indexPath : IndexPath ,tableView : UITableView, menuCellContents : (menuImage : Data? , menuName : String , menuDescription : String, menuPrice : Int , menuTarianType : String, menuAvailableNextAt  : String ,menuStatus  : Int,menuAvailable : Int, menuId : Int)) -> RestaurantMenuTableViewCellProtocol
    
}

protocol OrderMenuDetailsTableViewProtocol{
  
    var menuDetailsTableView : UITableView {get}
    
    func getOrderMenuCellHeight() -> CGFloat
    
    func createMenuDetailsContentView(indexPath : IndexPath ,tableView : UITableView,menuCellContents : ( menuName : String, menuTarianType : String, menuPriceSubTotal : Int,  menuAvailable : Int, quantity : Int)) -> UITableViewCell
    
}

protocol OrderDetailsPageViewProtocol : UIView, OrderMenuDetailsTableViewProtocol{
    var delegate : OrderDetailsPageViewDelegate? {get set}

    var totalItemsAndPriceNavigationBarLabel : UILabel  {get set}
    var navigationBarTitleView : UIView {get}
    
    
    
     var declineOrderButton : UIButton {get}
     var updateOrderStatusButton : UIButton{get}
    var orderBottomView : UIView {get}
    
    
    func updateTotalItemsAndPriceNavigationBarLabelText(menuCount : Int,totalPrice : Int)
    
    func updateOrderStatus(orderStatus : OrderStatus)
   
    
    func updateNavigationBarTitleViewLabel(menuCount : Int,totalPrice : Int,orderStatus : OrderStatus)
   
    
    
    func updateUserDetailsAndOrderDetailsLabel(userName : String,userPhoneNumber : String,dateAndTime : String,orderId :String)
   
    
  
    
    func createMenuDetailsTableView()
    
    func updateMenuDetailsTableViewHeightLayoutConstraints(numberOfRows : Int)
    
    func createInstructionToRestaurantTextView()
    
    func updateInstructionToRestaurantTextView(instructionsToRestaurantText : String)
    
   
    
    func updateBillDetails(itemTotalPrice : Int,restaurantPackagingChargePrice : Int, deliveryFeePrice : Int , totalAmount : Int,discountAmount : Int , restaurantGST : Double)
   
    
  
    
    
    
    
    func activateContentViewBottomAnchorWithRespectToBillView()
}

protocol OrdersPageViewProtocol : UIView,OrdersTableViewProtocol {
    var delegate : OrderPageViewDelegate? {get set}
    
   
    var waitingForOrdersLabel : UILabel {get}
    
    var refreshPageButton : UIButton {get}
    
}

protocol OrdersTableViewProtocol{
    
    var orderDetailsTableView : UITableView {get}
    
    func getOrderTabelViewCellHeight() -> CGFloat
    
    func createOrdersTableViewCell(indexPath : IndexPath ,tableView : UITableView, orderCellContents : (orderId : String , orderStatus : OrderStatus,orderTotal : Int , menuNameAndQuantities : [(menuName : String, quantity : Int)],dateAndTime : String)) -> UITableViewCell
}




protocol PastOrdersMenuDetailsTableViewProtocol{
   
   
    
    func getOrderMenuCellHeight() -> CGFloat
    
    func createMenuDetailsContentView(indexPath : IndexPath ,tableView : UITableView,menuCellContents : ( menuName : String, menuTarianType : String, menuPriceSubTotal : Int, quantity : Int)) -> UITableViewCell
    
}


protocol PastOrdersDetailsViewProtocol : PastOrdersMenuDetailsTableViewProtocol,OrderDetailsPageViewProtocol{
    var cancelOrderReasonLabel : UILabel {get}
    
    var restaurantRatingLabel : UILabel {get}
    

    func updateCancelOrderReasonLabel(cancelReason : String,orderStatus : OrderStatus)
    
    
   
    
    func updateRestaurantRatingLabel(rating : Int , restaurantFeedback : String)
    
    
    func activateContentViewBottomAnchorWithRespectToRestaurantRatingLabelConstraints()
}



protocol RestaurantAccountsPageViewProtocol : OrdersTableViewProtocol, UIView{
    var delegate : RestaurantAccountsPageViewDelegate? {get set}
     
    var restaurantIsAvailableView : UIView {get}
    var restaurantIsAvailableLabel : UILabel {get}
    var restaurantIsAvailableToggleSwitch : UISwitch {get}
    var datePicker : UIDatePicker {get}
    var restaurantNextAvailbaleDatePickerLabel : UILabel {get}
    
    
    
    var cancelRestaurantAvailablityButton : UIButton {get}
    
    var updateRestaurantAvailablityButton : UIButton {get}
    
    var restaurantNextAvailablityUpdateAndCancelButtonHorizontalStackView : UIStackView {get}
    
    var restaurantProfileImageView : UIImageView {get}
    var restaurantNameLabel : UILabel {get}
    var restaurantPhoneNumberLabel : UILabel {get}
    var restaurantCuisineLabel : UILabel {get}
    var restaurantDescriptionLabel : UILabel {get}
    var restaurantAddressLabel : UILabel {get}
    
    var restaurantStarRatingLabel : UILabel {get}
    
    var totalNumberOfrestaurantStarRatingLabel : UILabel{get}
    
    var starIconImageView : UIImageView {get}
    var availableLabel : UILabel {get}
    
    var editButton : UIButton {get}
    
     var settingButton : UIButton {get}
     var moreButton : UIButton {get}
    var changePasswordButton : UIButton{get}
     var logOutButton : UIButton {get}
    var orderDetailsTableView : UITableView {get}
    func getOrderTabelViewCellHeight() -> CGFloat
    func createOrdersTableViewCell(indexPath : IndexPath ,tableView : UITableView, orderCellContents : (orderId : String , orderStatus : OrderStatus,orderTotal : Int , menuNameAndQuantities : [(menuName : String, quantity : Int)],dateAndTime : String)) -> UITableViewCell
     var viewMoreOrdersButton : UIButton {get}
    var contactUSLabel : UILabel {get}
    
    func updateRestaurantDetails(restaurantContentDetails :   RestaurantContentDetails)
    
    func updateRestaurantStatus(restaurantContentDetails : RestaurantContentDetails)

    func hideSetNextAvailablityItems()
    func showSetNextAvailablityItems()
    
}

protocol RestaurantAccountSettingPageViewProtocol : UIView {
    var delegate :  RestaurantAccountSettingPageViewDelegate? {get set}

    var stackView : UIStackView {get}

    var restaurantDelistToggleSwitch : UISwitch {get}
    
    var restaurantDelistToggleSwitchView : UIView {get}
}

protocol RestaurantMoreOptionsPageViewProtocol : UIView{
    var delegate : RestaurantMoreOptionsPageViewDelegate? {get set}
    
     var stackView : UIStackView {get}
    
    
   
    
    var restaurantPackagingChargesTextField :  UITextField {get}
    
     var cancelUpdateRestaurantPackagingChargesButton  : UIButton {get}
    
    var  updateRestaurantPackagingChargesButton : UIButton {get}
    
    var updateAndCancelRestaurantPackagingChargesHorizontalStackView : UIStackView  {get}
    
}

protocol EditRestaurantDetailsPageViewProtocol : UIView{
    
    var delegate : EditRestaurantDetailsPageViewDelegate? {get set}
   
    
    var stackView : UIStackView  {get}
   
   
   
    var updateNameButton : UIButton {get}
    var cancelUpdateNameButton : UIButton {get}
    
    var updateAndCancelNameHorizontalStackView : UIStackView {get}
   
    var verifyPhoneNumberButton : UIButton {get}
    
    var cancelUpdatePhoneNumberButton : UIButton {get}
    
    var verifyAndCancelPhoneNumberHorizontalStackView : UIStackView {get}
   
    var updateCuisineButton : UIButton {get}
    
    var cancelUpdateCuisineButton : UIButton {get}
    
    var updateAndCancelCuisineHorizontalStackView : UIStackView {get}
  
    var updateDescriptionButton : UIButton {get}
    
    var cancelUpdateDescriptionButton : UIButton {get}
    
    var updateAndCancelDescriptionHorizontalStackView : UIStackView {get}
    
   
    var verifysecretCodeToUpdateAddressButton : UIButton {get}
    
    var cancelUpdateAddressButton : UIButton {get}
    var verifyAndCancelAddressHorizontalStackView : UIStackView {get}
    var restaurantNameTextField :  UITextField {get}
    
    var restaurantCuisineButton : UIButton {get}
    
    var restaurantPhoneNumberTextField : UITextField  {get}
    
   
    
    
    
    var restaurantDescriptionTextView :  UITextView {get}
    
  
    
    var changeRestaurantProfileImageButton : UIButton {get}
    
    var restaurantProfileImageView : UIImageView {get}
    
    var restaurantProfileImageSectionView : UIView {get}
    
   
    
    var restaurantAddressLabel : UILabel {get}
    
    var secretCodeTextField :  UITextField{get}
    
   
    
    
   
    
   
}
