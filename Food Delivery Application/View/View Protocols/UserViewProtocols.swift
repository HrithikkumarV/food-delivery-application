//
//  UserViewProtocols.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 15/01/22.
//

import Foundation
import UIKit

protocol UserLoginViewProtocol : UIView{
    var delegate : UserLoginViewDelegate? {get set}
    
    
    
    var phoneNumberTextField : UITextField {get}
 
    
    
    var createAccountButton : UIButton {get}
    
    
    var loginButton : UIButton {get}
    
}


protocol AddressTagViewProtocol : UIView{
    var delegate : AddressTagViewDelegate? {get set}
    
    
    
    var stackViewForAddressTags : UIStackView {get}
    
    var addressLabel : UILabel {get}
    

    var saveThisAddressAsLabel : UILabel {get}
    var otherAddressTagTextField : UITextField {get}
    
    var homeAddressTagButton : UIButton {get}
    
    var workAddressTagButton : UIButton {get}
    
    var otherAddressTagButton : UIButton {get}
    
    var saveButton : UIButton {get}
    

}


protocol UserCreateAccountUserDetailsViewProtocol : UIView{
    var delegate : UserCreateAccountUserDetailsViewDelegate? {get set}
    
    var continueForOTPButton : UIButton {get}
    
    var stackViewForUserDetails : UIStackView {get}
    
   
    
    var userNameTextField : UITextField {get}
    
    
    
    var phoneNumberTextField : UITextField {get}
    
    
   
    
}

protocol SelectDeliveryAddressViewProtocol : UIView{
    var delegate : SelectDeliveryAddressViewDelegate? {get set}
    
    var setDeliveryAddressButton : UIButton {get}
    
    var savedAddressesLabel : UILabel {get}
    
    var savedAddressesTableView : UITableView {get}
    
    func createAddressTableViewCell(indexPath : IndexPath , userAddress : UserAddressDetails) -> UITableViewCell
    
    func getAddressTableViewCellHeight() -> CGFloat
}


protocol RestaurantsTabViewProtocol : UIView, RestaurantElementsTableViewProtocol{
    var delegate : RestaurantsTabViewDelegate? {get set}
    
    var noRestaurantsAvailableLabel : UILabel {get}
   
    var changeLocationButton : UIButton {get}
   
}


protocol RestaurantsTabNavigationBarTitleViewProtocol : UIView{
    var delegate : RestaurantsTapNavigationBarTitleViewDelegate? {get set}
    func updateAddressTagAndAddressLabel(addressTag : String , address : AddressDetails)
    
}

protocol RestaurantElementsTableViewProtocol{
   
    var restaurantsTableView : UITableView {get set}
    
    func getRestaurantTableViewCellHeight() -> CGFloat
    
    
    func createRestaurantTableViewCell(indexPath : IndexPath ,restaurentCellContents : (restaurantProfileImage : Data , restaurantName : String , restaurantStarRating : String , restaurantCuisine : String , restaurantLocality : String , deliveryTiming : Int , restaurantOpensNextAt  : String,restaurantIsAvailable : Int,isDeliverable : Bool)) -> UITableViewCell
    
    
}

protocol UserWelcomePageViewProtocol : UIView{
    var delegate : UserWelcomePageViewDelegate? { get set }
    var setDeliveryAddressButton : UIButton {get}
    var haveAnAccountLabel : UILabel {get}
    var loginButton : UIButton {get}
    var readyToOrderFromYourFavouriteRestaurantsLabel : UILabel {get}
    var appWelcomeMessageLabel : UILabel {get}
    var userWelcomeMessageLabel : UILabel {get}
    var changeAccountButton : UIButton {get}
    
}

protocol UserDisplayMenuDetailsPageNavigationBarTitleViewProtocol : UIView{
    var restaurantNameNavigationBarLabel : UILabel {get}
        
    var restaurantAddressNavigationBarLabel : UILabel {get}
    
    func updateRestaurantDetails(restaurantContentDetails  :RestaurantContentDetails)
    
}

protocol UserDisplayMenuDetailsPageViewProtocol :UIView, UserMenuElementTableViewProtocol{
    var delegate  :UserDisplayMenuDetailsInRestaurantPageViewDelegate? {get set}
    
    var restaurantNameLabel : UILabel {get}
    var restaurantCuisineLabel : UILabel {get}
    var restaurantDescriptionLabel : UILabel {get}
    var restaurantAddressLabel : UILabel {get}
    var restaurantStarRatingLabel : UILabel {get}
    var totalNumberOfrestaurantStarRatingLabel : UILabel {get}
    var starIconImageView : UIImageView {get}
    var availableLabel : UILabel {get}
    
    var menuVegOnlyToggleSwitch : UISwitch {get}
    var vegOnlyLabel : UILabel {get}
    
    func updateRestaurantDetails(restaurantContentDetails : RestaurantContentDetails)
    
    
    func updateRestaurantStatus(deliveryTiming : Int,restaurantContentDetails : RestaurantContentDetails)
    
    
    func createMenuCategoryButton(menuCategory : String) -> UIButton

    

}

protocol MoveToCartViewProtocol : UIView{
    var delegate : MoveToCartViewDelegate? {get set}
    
   

    func updateRestaurantNameInCartLabelText(restaurantNameInUserCart : String)
    
    
    func updateMenuItemCountAndPriceLabelText(menuCount : Int,totalPrice : Int)
    
    
    func hideOrShowmoveToCartViewBasedOnMenuCount(menuCount :Int)
    
    
}

protocol UserMenuElementTableViewProtocol{
  
    var menuTableView : UITableView {get}

    func getMenuTabelViewCellHeight() -> CGFloat

    func createMenuTableViewCell(indexPath : IndexPath ,tableView : UITableView, menuCellContents : (menuImage : Data? , menuName : String , menuDescription : String, menuPrice : Int , menuTarianType : String, menuAvailable : Int, menuAvailableNextAt  : String ,menuId : Int)) -> UserMenuElementTableViewCellProtocol
}

protocol UserMenuElementTableViewCellProtocol : UITableViewCell{
    var delegate : MenuTableViewCellDelegate? { get set }
    
    var addMenuCellButton : UIButton {get}
    
    var plusButton : UIButton {get}
    
    var minusButton : UIButton {get}
    
    var menuCountLabel : UILabel {get}
    
    var addMenuButtonView : UIView {get}
}

protocol CartMenuDetailsTableViewCellProtocol : UITableViewCell{
    var delegate : CartMenuDetailsTableViewCellDelegate? { get set }
    
   
    
    var plusButton : UIButton {get}
    
    var minusButton : UIButton {get}
    
    var menuCountLabel : UILabel {get}
    
    var addMenuButtonView : UIView {get}
    
    var menuSubTotalPriceCellLabel : UILabel {get}
    
}

protocol SearchMenuInDisplayedUserMenuDetailsViewProtocol : UIView, UserMenuElementTableViewProtocol{
    var delegate : SearchMenuInDisplayedUserMenuDetailsViewDelegate? {get set}
    
    
    var menuTableView : UITableView {get}
    
    
    func getMenuTabelViewCellHeight() -> CGFloat
    
    
    func createMenuTableViewCell(indexPath : IndexPath ,tableView : UITableView, menuCellContents : (menuImage : Data? , menuName : String , menuDescription : String, menuPrice : Int , menuTarianType : String, menuAvailable : Int, menuAvailableNextAt  : String ,menuId : Int)) -> UserMenuElementTableViewCellProtocol
    

    
    var moveToCartView : MoveToCartViewProtocol {get}
    
   
}

protocol UserMenuElementsTableViewInSearchMenuProtocol{
    
    func createMenuTableViewCell(indexPath : IndexPath, menuCellContents : (menuImage : Data? , menuName : String , menuDescription : String, menuPrice : Int , menuTarianType : String, menuAvailable : Int, menuAvailableNextAt  : String ,menuId : Int) , restaurantDetails : RestaurantContentDetails) -> UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenu
    
    func getMenuTabelViewCellHeight() -> CGFloat

}

protocol UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenuProtocol : UserMenuElementTableViewCellProtocol{
    var subDelegate: UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenuDelegate? { get set }
}

protocol UserSearchMenuAndRestaurantViewProtocol : UIView, SearchSuggestionAndSearchHistoryTableViewProtocol,UserMenuElementsTableViewInSearchMenuProtocol{
    
    var delegate  : UserSearchMenuAndRestaurantsViewDelegate? {get set}
    
    var restaurantButton : UIButton {get}
    
    var dishButton : UIButton {get}
    
    var noMatchFoundLabel : UILabel{get}
    
    
    var browseRestaurantsButton : UIButton {get}
   
    
    
    func createSearchSuggestionTableViewCell(indexPath : IndexPath ,seacrhSuggesionCellContents : SearchSuggessionModel) -> UITableViewCell
    
    func getsearchSuggestionAndSearchHistoryTableViewCellHeight() -> CGFloat
    
    var searchSuggestionAndSearchHistoryTableViewHeaderView : SearchSuggestionTableHeaderView {get}

    func getMenuTabelViewCellHeight() -> CGFloat
    
    
    func createMenuTableViewCell(indexPath : IndexPath , menuCellContents : (menuImage : Data? , menuName : String , menuDescription : String, menuPrice : Int , menuTarianType : String, menuAvailable : Int, menuAvailableNextAt  : String , menuId :Int) , restaurantDetails : RestaurantContentDetails) -> UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenu
    
    var tableView : UITableView {get}
   
    func getRestaurantTableViewCellHeight() -> CGFloat
    
    func createRestaurantTableViewCell(indexPath : IndexPath ,restaurentCellContents : (restaurantProfileImage : Data , restaurantName : String , restaurantStarRating : String , restaurantCuisine : String , restaurantLocality : String , deliveryTiming : Int , restaurantOpensNextAt  : String,restaurantIsAvailable : Int,isDeliverable : Bool)) -> UITableViewCell
    
}




protocol UserCartMenuDetailsTableViewProtocol{
    
    
    var menuDetailsTableView : UITableView  {get}
   
    func getCartMenuCellHeight() -> CGFloat
    
    
    func createMenuDetailsContentView(indexPath : IndexPath ,tableView : UITableView,menuCellContents : ( menuName : String, menuTarianType : String,  menuAvailable : Int, menuAvailableNextAt  : String,menuId: Int)) -> CartMenuDetailsTableViewCellProtocol
    
    
}
protocol UserCartScrollViewProtocol : UIScrollView{
    var emptyCartLabel : UILabel {get}
}

protocol UserCartViewProtocol :UIView, UserCartMenuDetailsTableViewProtocol{
    var delegate : UserCartViewDelegate? {get set}
    
    
    var navigationBarTitleView : UIView {get}
   
    var restaurantNameNavigationBarLabel : UILabel {get}
    var totalItemsAndPriceNavigationBarLabel : UILabel {get}
     var addMoreMenuButton : UIButton {get}
    
    var restaurantNameLabel : UILabel {get}
    
    var restaurantLocalityLabel : UILabel {get}
    
    var restaurantProfileImageView : UIImageView {get}
    var instructionsToRestaurantTextView : UITextView {get}
    var applyCouponTextField : UITextField {get}
     var applyCouponButton : UIButton  {get}
     var removeCouponButton :UIButton {get}
    
    var couponAppliedLabel : UILabel {get}
    
    var billView : UIView {get}
    
    var billstackView : UIStackView {get}
     var itemTotalLabelsStackView : UIStackView {get}
     var deliveryFeeLabelsStackView : UIStackView {get}
     var restaurantGSTLabelsStackView : UIStackView {get}
    
     var restaurantPackagingChargesLabelsStackView : UIStackView {get}
    
     var itemDiscountLabelsStackView : UIStackView {get}
    
     var toPayLabelsStackView : UIStackView {get}
    
    
    var cancelOrderInstructionsLabel : UILabel {get}
    
    var cartBottomView : CartBottomViewProtocol {get}
    func updateTotalItemsAndPriceNavigationBarLabelText(menuCount : Int,totalPrice : Int)
    
    
    func addNavigationBarTitleView(restaurantName : String, menuCount : Int,totalPrice : Int)
  
    
    
    func updateRestaurantDetailsContents(restaurantProfileImage : Data,restaurantName : String , restaurantLocality: String)
    
    
    var menuDetailsTableView : UITableView {get}
    
    func getCartMenuCellHeight() -> CGFloat
    
    
    
    
    func createMenuDetailsContentView(indexPath : IndexPath ,tableView : UITableView,menuCellContents : ( menuName : String, menuTarianType : String,  menuAvailable : Int, menuAvailableNextAt  : String,menuId : Int)) -> CartMenuDetailsTableViewCellProtocol
    
    
    
    
    func updateBillDetails(itemTotalPrice : Int,restaurantPackagingChargePrice : Int, deliveryFeePrice : Int , itemDiscountPrice : Int)
    
    
    func getTopayAmount(itemTotalPrice : Int,restaurantPackagingChargePrice : Int, deliveryFeePrice : Int , itemDiscountPrice : Int) -> Int
   
    func updateCouponAppliedLabel(couponCode : String , itemDiscountPrice : Int)
    
}

protocol CartBottomViewProtocol  : UIView{
    var delegate : cartBottomViewDelegate? {get set}
    
    
    
    var totalAmountToBePaid : UILabel {get}
    
    var placeOrderButton : UIButton {get}
    var paymentModeLabel : UILabel {get}
    var deliveryAddressView : UIView {get}
    
    
    
    var addressLabel : UILabel {get}
    
    
    var addressTagLabel : UILabel {get}
    
    var addressTagIconimageView : UIImageView {get}
    
    
    var loginButton : UIButton {get}
    
    
    var menuItemUnavailableLabel : UILabel {get}
    
    var removeUnavailableItemsButton : UIButton {get}
    
    var restaurantUnavailableLabel : UILabel {get}
    
    var browseRestaurantsButton : UIButton {get}
    
    var changeAddressButton : UIButton {get}
    
    var notDeliverableLabel : UILabel {get}
    
    func updatePaymentMode(paymentMode : String)
    
    func updateAddressDetailsInLabel(addressTag : String , address : AddressDetails)
    
}


protocol UserOrdersMenuDetailsTableViewProtocol{
    
    
    func getOrderMenuCellHeight() -> CGFloat
    
    func createMenuDetailsContentView(indexPath : IndexPath ,tableView : UITableView,menuCellContents : ( menuName : String, menuTarianType : String, menuPriceSubTotal : Int, quantity : Int)) -> UITableViewCell
    
}

protocol UserAccountsPageViewProtocol : UIView, OrderDetailsTableViewProtocol{
    
    var delegate : UserAccountsPageViewDelegate? { get set}
    
    func updateUserDetails(userName : String,userPhoneNumber : String)
    
    var viewMoreOrdersButton : UIButton {get}
    
}

protocol  UserAccountsPageLoginViewProtocol : UIView {
     var delegate : UserAccountsPageLoginViewDelegate? {get set}
}

protocol OrderDetailsTableViewProtocol{
    
    var orderDetailsTableView : UITableView {get}
    
    func getOrderTabelViewCellHeight() -> CGFloat

    func createOrdersTableViewCell(indexPath : IndexPath ,tableView : UITableView, orderCellContents : (restaurantName : String , orderStatus : OrderStatus,orderTotal : Int , menuNameAndQuantities : [(menuName : String, quantity : Int)],dateAndTime : String)) -> UITableViewCell
    
}

protocol UserOrderDetailsPageViewProtocol : UIView, UserOrderMenuDetailsTableViewProtocol{
    var delegate : UserOrderDetailsPageViewDelegate? { get set}
    
    
    var menuDetailsTableView : UITableView {get set}
  
    
    func getOrderMenuCellHeight() -> CGFloat
    
    
    
    func createMenuDetailsContentView(indexPath : IndexPath ,tableView : UITableView,menuCellContents : ( menuName : String, menuTarianType : String, menuPriceSubTotal : Int, quantity : Int)) -> UITableViewCell
    
    
    
    func UpdateOrderAndUserDetailsContents(orderId : String, dateAndTime  :String, restaurantName : String , deliveryAddress: AddressDetails)
    
    
  

   func setMenuDetailsTableViewHeight(numberOfRows : Int)
    
    
    
    
    func updateInstructionsToRestaurant(instructionsToRestaurantText :String)
  
    
    func updateBillDetails(itemTotalPrice : Int,restaurantPackagingChargePrice : Int, deliveryFeePrice : Int , totalAmount : Int,discountAmount : Int , restaurantGST : Double)
    
    
   
    
    func updateCancelOrderReason(cancelReason : String ,orderStatus : OrderStatus)
    
    func updateRatingAndFeedback(rating : Int , restaurantFeedback : String)
    
    
    func activateContentViewBottomAnchorWithRespectToRestaurantRatingLabelConstraints()
    
     var cancelOrderButton : UIButton {get}
   
     var reOrderButton : UIButton {get}
   
    var rateOrderButton : UIButton  {get}
   
    var feedbackButton : UIButton  {get}
   
    var cancelOrderReasonLabel : UILabel {get}
   
    var restaurantRatingLabel : UILabel  {get}
    
}

protocol UserOrderDetailsPageViewNavigationBarTitleViewProtocol : UIView{
    func updateNavigationBarTitle(menuCount : Int,totalPrice : Int,orderStatus : OrderStatus)
}

protocol UserOrderMenuDetailsTableViewProtocol{
   
    
    func getOrderMenuCellHeight() -> CGFloat
    
    func createMenuDetailsContentView(indexPath : IndexPath ,tableView : UITableView,menuCellContents : ( menuName : String, menuTarianType : String, menuPriceSubTotal : Int, quantity : Int)) -> UITableViewCell
    
}

protocol FavouriteRestaurantsViewProtocol : UIView, RestaurantElementsTableViewProtocol{
    var delegate :FavouriteRestaurantsViewDelegate? { get set }
    
    
    
     var noRestaurantsAvailableLabel  : UILabel {get}
    
    var browseRestaurantsButton : UIButton {get}

    
    func getRestaurantTableViewCellHeight() -> CGFloat
    
    
    func createRestaurantTableViewCell(indexPath : IndexPath ,restaurentCellContents : (restaurantProfileImage : Data , restaurantName : String , restaurantStarRating : String , restaurantCuisine : String , restaurantLocality : String , deliveryTiming : Int , restaurantOpensNextAt  : String,restaurantIsAvailable : Int,isDeliverable : Bool)) -> UITableViewCell
    
}

protocol EditUserDetailsPageViewProtocol : UIView{
   
    var delegate : EditUserDetailsPageViewDelegate? {get set}
    
    
    var userNameTextField : UITextField {get}
    
    var  cancelUpdateNameButton : UIButton {get}
    
    var updateNameButton : UIButton {get}
    var updateAndCancelNameHorizontalStackView : UIStackView {get}
    
    var phoneNumberTextField : UITextField {get}
    var verifyPhoneNumberButton : UIButton {get}
    
    var cancelUpdatePhoneNumberButton : UIButton {get}
    var verifyAndCancelPhoneNumberHorizontalStackView : UIStackView {get}
        
    var stackView : UIStackView {get}
    
    
}

protocol ManageAddressesViewProtocol : UIView{
    var delegate : ManageAddressesViewDelegate? {get set}
    
    
     var addDeliveryAddressButton : UIButton {get}
    
    var savedAddressesTableView : UITableView {get}
    var savedAddressesLabel : UILabel {get}
    
    
    func createAddressTableViewCell(indexPath : IndexPath , userAddress : UserAddressDetails) -> ManageAddressesTableViewCell
    
    func getAddressTableViewCellHeight() -> CGFloat
    
    
}

protocol SearchSuggestionAndSearchHistoryTableViewProtocol{
   
    func getsearchSuggestionAndSearchHistoryTableViewCellHeight() -> CGFloat
    
}


