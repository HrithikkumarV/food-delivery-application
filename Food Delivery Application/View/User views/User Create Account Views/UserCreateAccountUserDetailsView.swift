//
//  UserCreateAccountUserDetailsView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/12/21.
//

import Foundation

import UIKit
class UserCreateAccountUserDetailsView : UIView, UserCreateAccountUserDetailsViewProtocol{
    
    weak var delegate : UserCreateAccountUserDetailsViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        self.addSubview(stackViewForUserDetails)
        NSLayoutConstraint.activate(getCreateAccountUserDetailsStackViewConstraints())
    }
    
    lazy var continueForOTPButton : UIButton = {
        let continueForOTPButton = UIButton()
        continueForOTPButton.translatesAutoresizingMaskIntoConstraints = false
        continueForOTPButton.setTitle("continue", for: .normal)
        continueForOTPButton.setTitleColor(.white, for: .normal)
        continueForOTPButton.backgroundColor = .systemBlue
        continueForOTPButton.layer.borderWidth = 1
        continueForOTPButton.layer.borderColor = UIColor.systemGray6.cgColor
        continueForOTPButton.layer.cornerRadius = 5
        continueForOTPButton.addTarget(self, action: #selector(didTapContinue), for: .touchUpInside)
        return continueForOTPButton
    }()
    
    lazy var stackViewForUserDetails : UIStackView = {
        let stackViewForUserDetails = UIStackView(arrangedSubviews: [userNameTextField,phoneNumberTextField ,continueForOTPButton])
        stackViewForUserDetails.axis = .vertical
        stackViewForUserDetails.spacing = 15
        stackViewForUserDetails.distribution = .fillEqually
        stackViewForUserDetails.translatesAutoresizingMaskIntoConstraints = false
        return stackViewForUserDetails
    }()
    
   
    lazy var userNameTextField : UITextField = {
        let userNameTextField = createTextField()
        userNameTextField.placeholder = "Name"
        userNameTextField.addLabelToTopBorder(labelText: " Name ", fontSize: 15)
        userNameTextField.keyboardType = .default
        userNameTextField.returnKeyType = .next
        return userNameTextField
    }()
    
    
    lazy var phoneNumberTextField : UITextField = {
        let phoneNumberTextField = createTextField()
        phoneNumberTextField.placeholder = "Phone Number"
        phoneNumberTextField.addLabelToTopBorder(labelText: " Phone Number ", fontSize: 15)
        phoneNumberTextField.keyboardType = .phonePad
        let phoneNumberkeyboardNextBarButton = ToolBarUtils().createBarButton(title: "Next")
        phoneNumberkeyboardNextBarButton.target = self
        phoneNumberkeyboardNextBarButton.action = #selector(didTapPhoneNumberkeyboardNextBarButton)
        let phoneNumberkeyboardToolBar = ToolBarUtils().createKeyboardToolBar()
        let flexibleSpaceForToolBar = ToolBarUtils().createFlexibleSpaceForToolBar()
        phoneNumberkeyboardToolBar.items = [flexibleSpaceForToolBar,phoneNumberkeyboardNextBarButton]
        phoneNumberTextField.inputAccessoryView = phoneNumberkeyboardToolBar
        
        return phoneNumberTextField
    }()
    
    
   
    private func createTextField() -> UITextField{
        let textField = CustomTextField()
        textField .backgroundColor = .white
        textField .textColor = .black
        textField .translatesAutoresizingMaskIntoConstraints = false
        textField.autocorrectionType = .no
        return textField
    }
    
    
    

    private func getCreateAccountUserDetailsStackViewConstraints() -> [NSLayoutConstraint]{
        let stackViewConstraints = [stackViewForUserDetails.topAnchor.constraint(equalTo:self.topAnchor ,constant: 50),
                                    stackViewForUserDetails.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
                                    stackViewForUserDetails.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40),
                                    stackViewForUserDetails.heightAnchor.constraint(equalToConstant: 180)
        ]
        return stackViewConstraints
    }
    
    
    
    
    @objc func didTapPhoneNumberkeyboardNextBarButton(){
        delegate?.didTapPhoneNumberkeyboardNextBarButton()
    }
    
    @objc func didTapView(){
        delegate?.didTapView()
    }
    
    @objc func didTapContinue(){
        delegate?.didTapContinue()
    }
    
    
    
}


protocol UserCreateAccountUserDetailsViewDelegate : AnyObject{
    
    func didTapPhoneNumberkeyboardNextBarButton()
    
    func didTapView()
    
    func didTapContinue()
    
}
