//
//  UserloginView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 12/11/21.
//

import Foundation
import UIKit

class UserLoginView : UIView, UserLoginViewProtocol{
   
    
    weak var delegate : UserLoginViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
    }
    
    private func initialiseViewElements(){
        self.addSubview(phoneNumberTextField)
        self.addSubview(loginButton)
        self.addSubview(createAccountButton)
        self.addSubview(orLable)
        self.addSubview(leftLine)
        self.addSubview(rightLine)
        NSLayoutConstraint.activate(getPhoneNumberTextFieldConstraints())
        NSLayoutConstraint.activate(getLoginButtonContraints())
        NSLayoutConstraint.activate(getCreateAccountButtonContraints())
        NSLayoutConstraint.activate(getOrLabelWithLinesContraints())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var phoneNumberTextField : UITextField = {
        let phoneNumberTextField = CustomTextField()
        phoneNumberTextField.placeholder = "Phone number"
        phoneNumberTextField.addLabelToTopBorder(labelText: " Phone Number ", fontSize: 15)
        phoneNumberTextField.backgroundColor = .white
        phoneNumberTextField.translatesAutoresizingMaskIntoConstraints =  false
        phoneNumberTextField.keyboardType = .phonePad
        let phoneNumberKeyBoardDoneBarButton = ToolBarUtils().createBarButton(title: "Done")
        phoneNumberKeyBoardDoneBarButton.target = self
        phoneNumberKeyBoardDoneBarButton.action = #selector(didTapPhoneNumberkeyboardNextBarButton)
        let pincodeKeyBoardToolBar = ToolBarUtils().createKeyboardToolBar()
        let flexibleSpaceForToolBar = ToolBarUtils().createFlexibleSpaceForToolBar()
        pincodeKeyBoardToolBar.items = [flexibleSpaceForToolBar,phoneNumberKeyBoardDoneBarButton]
        phoneNumberTextField.inputAccessoryView = pincodeKeyBoardToolBar
        return phoneNumberTextField
    }()
    
    let orLable : UILabel = {
        let orLable = UILabel()
        orLable.text = "OR"
        orLable.textColor = .systemGray
        orLable.textAlignment = .center
        orLable.translatesAutoresizingMaskIntoConstraints = false
        return orLable
    }()
    
    let leftLine : UIView = {
        let leftLine = UIView()
        leftLine.backgroundColor = .systemGray
        leftLine.translatesAutoresizingMaskIntoConstraints = false
        return leftLine
    }()
    
    let rightLine : UIView = {
        let rightLine = UIView()
        rightLine.backgroundColor = .systemGray
        rightLine.translatesAutoresizingMaskIntoConstraints = false
        return rightLine
    }()
    
    lazy var createAccountButton : UIButton = {
        let createAccountButton = UIButton()
        createAccountButton.translatesAutoresizingMaskIntoConstraints = false
        createAccountButton.setTitle("Create New Account", for: .normal)
        createAccountButton.setTitleColor(.white, for: .normal)
        createAccountButton.backgroundColor = .systemGreen
        createAccountButton.layer.cornerRadius = 5
        createAccountButton.titleLabel?.adjustsFontSizeToFitWidth = true
        createAccountButton.addTarget(self, action: #selector(didTapCreateAccount), for: .touchUpInside)
        return createAccountButton
    }()
    
    lazy var loginButton : UIButton = {
        let loginButton = UIButton()
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.setTitle("Login", for: .normal)
        loginButton.setTitleColor(.white, for: .normal)
        loginButton.backgroundColor = .systemBlue
        loginButton.layer.cornerRadius = 5
        loginButton.addTarget(self, action: #selector(didTapLogin), for: .touchUpInside)
        
        return loginButton
    }()
    
    
    private func getPhoneNumberTextFieldConstraints() -> [NSLayoutConstraint]{
        let textFieldContraints = [phoneNumberTextField.topAnchor.constraint(equalTo: self.topAnchor , constant: UIScreen.main.bounds.height/4),
                                   phoneNumberTextField.leftAnchor.constraint(equalTo: self.leftAnchor ,constant: 40),
                                   phoneNumberTextField.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -40),
                                   phoneNumberTextField.heightAnchor.constraint(equalToConstant : 50)]
        return textFieldContraints
    }
    
    private func getLoginButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [ loginButton.topAnchor.constraint(equalTo: phoneNumberTextField.bottomAnchor, constant: 10),
                                 loginButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
                                 loginButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40),
                                 loginButton.heightAnchor.constraint(equalToConstant: 50)]
        return buttonContraints
    }
    
    
    
    
    
    
   
    private func getOrLabelWithLinesContraints() -> [NSLayoutConstraint]{
        let Contraints = [
            orLable.centerYAnchor.constraint(equalTo: loginButton.bottomAnchor, constant: 50),
            orLable.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            orLable.heightAnchor.constraint(equalToConstant: 30),
            orLable.widthAnchor.constraint(equalToConstant: 25),
            leftLine.heightAnchor.constraint(equalToConstant: 1),
            leftLine.leftAnchor.constraint(equalTo: self.leftAnchor ,constant: 40),
            leftLine.rightAnchor.constraint(equalTo: orLable.leftAnchor),
            leftLine.centerYAnchor.constraint(equalTo: orLable.centerYAnchor),
            rightLine.heightAnchor.constraint(equalToConstant: 1),
            rightLine.rightAnchor.constraint(equalTo: self.rightAnchor ,constant: -40),
            rightLine.leftAnchor.constraint(equalTo: orLable.rightAnchor),
            rightLine.centerYAnchor.constraint(equalTo: orLable.centerYAnchor)
        ]
        return Contraints
    }
    private func getCreateAccountButtonContraints() -> [NSLayoutConstraint]{
        let createAccountButtonConstraints = [
            createAccountButton.centerYAnchor.constraint(equalTo: orLable.bottomAnchor , constant: 40),
            createAccountButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -80),
            createAccountButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 80),
            createAccountButton.heightAnchor.constraint(equalToConstant: 50)
            ]
        return createAccountButtonConstraints
    }
    
   
    
    
    @objc func didTapPhoneNumberkeyboardNextBarButton(){
        delegate?.didTapPhoneNumberkeyboardNextBarButton()
    }
    
    @objc func didTapView(){
        delegate?.didTapView()
    }
    
    @objc func didTapCreateAccount(){
        delegate?.didTapCreateAccount()
    }
    
    @objc func didTapLogin(){
        delegate?.didTapLogin()
    }
    
}

protocol UserLoginViewDelegate : AnyObject{
     func didTapPhoneNumberkeyboardNextBarButton()
    
    
     func didTapView()
    
    
     func didTapCreateAccount()
    
    
     func didTapLogin()
    
}
