//
//  EditUserDetailsPageView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 28/03/22.
//

import Foundation
import UIKit


class EditUserDetailsPageView : UIView , EditUserDetailsPageViewProtocol{
    
    weak var delegate : EditUserDetailsPageViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        self.addSubview(stackView)
        NSLayoutConstraint.activate(getEditAccountUserDetailsStackViewConstraints())
    }
    
   
    
    
    
    
    
     lazy var userNameTextField : UITextField = {
       let userNameTextField = createTextField()
        userNameTextField.keyboardType = .default
        userNameTextField.returnKeyType = .next
        userNameTextField.addLabelToTopBorder(labelText: "NAME", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -2, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        userNameTextField.addBorder(side: .bottom, color: .gray, width: 1)
        return userNameTextField
    }()
    
     lazy var  cancelUpdateNameButton : UIButton = {
        let cancelUpdateNameButton = UIButton()
        cancelUpdateNameButton.translatesAutoresizingMaskIntoConstraints = false
        cancelUpdateNameButton.setTitle("CANCEL", for: .normal)
        cancelUpdateNameButton.setTitleColor(.systemBlue, for: .normal)
        cancelUpdateNameButton.backgroundColor = .white
        cancelUpdateNameButton.layer.borderWidth = 1
        cancelUpdateNameButton.layer.borderColor = UIColor.systemBlue.cgColor
        cancelUpdateNameButton.layer.cornerRadius = 5
        cancelUpdateNameButton.addTarget(self, action: #selector(didTapCancelUpdateNameButton), for: .touchUpInside)
        return cancelUpdateNameButton
    }()
    
     lazy var updateNameButton : UIButton = {
       let updateNameButton = UIButton()
        updateNameButton.translatesAutoresizingMaskIntoConstraints = false
        updateNameButton.setTitle("UPDATE", for: .normal)
        updateNameButton.setTitleColor(.white, for: .normal)
        updateNameButton.backgroundColor = .systemBlue
        updateNameButton.layer.cornerRadius = 5
        updateNameButton.addTarget(self, action: #selector(didTapUpdateNameButton), for: .touchUpInside)
        return updateNameButton
    }()
    
     lazy var updateAndCancelNameHorizontalStackView : UIStackView = {
        let updateAndCancelNameHorizontalStackView = UIStackView(arrangedSubviews: [updateNameButton,cancelUpdateNameButton])
        updateAndCancelNameHorizontalStackView.axis = .horizontal
        updateAndCancelNameHorizontalStackView.distribution = .fillEqually
        updateAndCancelNameHorizontalStackView.spacing = 20
         updateAndCancelNameHorizontalStackView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return updateAndCancelNameHorizontalStackView
    }()
    
     lazy var phoneNumberTextField : UITextField = {
        let phoneNumberTextField = createTextField()
        phoneNumberTextField.keyboardType = .phonePad
        phoneNumberTextField.addLabelToTopBorder(labelText: "PHONE NUMBER", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -2, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        phoneNumberTextField.addBorder(side: .bottom, color: .gray, width: 1)
        return phoneNumberTextField
    }()
     lazy var verifyPhoneNumberButton : UIButton = {
       let verifyPhoneNumberButton = UIButton()
        verifyPhoneNumberButton.translatesAutoresizingMaskIntoConstraints = false
        verifyPhoneNumberButton.setTitle("VERIFY", for: .normal)
        verifyPhoneNumberButton.setTitleColor(.white, for: .normal)
        verifyPhoneNumberButton.backgroundColor = .systemBlue
        verifyPhoneNumberButton.layer.cornerRadius = 5
        verifyPhoneNumberButton.addTarget(self, action: #selector(didTapVerifyPhoneNumberButton), for: .touchUpInside)
        return verifyPhoneNumberButton
    }()
    
     lazy var cancelUpdatePhoneNumberButton : UIButton = {
        let  cancelUpdatePhoneNumberButton = UIButton()
        cancelUpdatePhoneNumberButton.translatesAutoresizingMaskIntoConstraints = false
        cancelUpdatePhoneNumberButton.setTitle("CANCEL", for: .normal)
        cancelUpdatePhoneNumberButton.setTitleColor(.systemBlue, for: .normal)
        cancelUpdatePhoneNumberButton.backgroundColor = .white
        cancelUpdatePhoneNumberButton.layer.borderWidth = 1
        cancelUpdatePhoneNumberButton.layer.borderColor = UIColor.systemBlue.cgColor
        cancelUpdatePhoneNumberButton.layer.cornerRadius = 5
        cancelUpdatePhoneNumberButton.addTarget(self, action: #selector(didTapCancelUpdatePhoneNumberButton), for: .touchUpInside)
        return cancelUpdatePhoneNumberButton
    }()
    
     lazy var verifyAndCancelPhoneNumberHorizontalStackView : UIStackView = {
        let  verifyAndCancelPhoneNumberHorizontalStackView = UIStackView(arrangedSubviews: [verifyPhoneNumberButton,cancelUpdatePhoneNumberButton])
        verifyAndCancelPhoneNumberHorizontalStackView.axis = .horizontal
        verifyAndCancelPhoneNumberHorizontalStackView.distribution = .fillEqually
        verifyAndCancelPhoneNumberHorizontalStackView.spacing = 20
         verifyAndCancelPhoneNumberHorizontalStackView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return verifyAndCancelPhoneNumberHorizontalStackView
    }()
    
    lazy var stackView : UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [userNameTextField,updateAndCancelNameHorizontalStackView ,phoneNumberTextField,verifyAndCancelPhoneNumberHorizontalStackView])
        stackView.axis = .vertical
        stackView.spacing = 25
        stackView.distribution = .fillProportionally
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private func createTextField() -> UITextField{
        let textField = CustomTextField()
        textField .backgroundColor = .white
        textField .textColor = .black
        textField .translatesAutoresizingMaskIntoConstraints = false
        textField.autocorrectionType = .no
        textField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        return textField
    }
    

    private func getEditAccountUserDetailsStackViewConstraints() -> [NSLayoutConstraint]{
        let stackViewConstraints = [stackView.topAnchor.constraint(equalTo:self.topAnchor,constant: 10),
                                    stackView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
                                    stackView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20)
        ]
        return stackViewConstraints
    }
    
    
    @objc func didTapUpdateNameButton(){
        delegate?.didTapUpdateNameButton()
    }
    @objc func didTapVerifyPhoneNumberButton(){
        delegate?.didTapVerifyPhoneNumberButton()
    }
    @objc func didTapCancelUpdatePhoneNumberButton(){
        delegate?.didTapCancelUpdatePhoneNumberButton()
    }
    @objc func didTapCancelUpdateNameButton(){
        delegate?.didTapCancelUpdateNameButton()
    }
    
    @objc func didTapView(){
        delegate?.didTapView()
    }
    
}

protocol EditUserDetailsPageViewDelegate : AnyObject{
    func didTapUpdateNameButton()
    func didTapVerifyPhoneNumberButton()
    func didTapCancelUpdatePhoneNumberButton()
    func didTapCancelUpdateNameButton()
    func didTapView()
}
