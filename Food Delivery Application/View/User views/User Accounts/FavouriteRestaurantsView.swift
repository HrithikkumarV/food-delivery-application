//
//  FavouriteRestaurantsView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/03/22.
//

import Foundation
import UIKit
class FavouriteRestaurantsView : UIView    ,FavouriteRestaurantsViewProtocol{
    
    weak var delegate :FavouriteRestaurantsViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
    }
    
    private func initialiseViewElements(){
        self.addSubview(restaurantsTableView)
        self.addSubview(noRestaurantsAvailableLabel)
        self.addSubview(browseRestaurantsButton)
        NSLayoutConstraint.activate(getRestaurantsTableViewContraints())
        NSLayoutConstraint.activate(getNoRestaurantsAvailableLabelContraints())
        NSLayoutConstraint.activate(getBrowseRestaurantsButtonLayoutConstraints())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
     lazy var noRestaurantsAvailableLabel  : UILabel = {
       let noRestaurantsAvailableLabel = UILabel()
        noRestaurantsAvailableLabel.textColor = .systemGray
        noRestaurantsAvailableLabel.translatesAutoresizingMaskIntoConstraints = false
        noRestaurantsAvailableLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        noRestaurantsAvailableLabel.text = "Once you favorite a restaurant, it will appear here."
        noRestaurantsAvailableLabel.textAlignment = .center
        noRestaurantsAvailableLabel.lineBreakMode = .byWordWrapping
        noRestaurantsAvailableLabel.numberOfLines = 5
        noRestaurantsAvailableLabel.sizeToFit()
        return noRestaurantsAvailableLabel
    }()
    
     lazy var browseRestaurantsButton : UIButton = {
       let browseRestaurantsButton = UIButton()
        browseRestaurantsButton.translatesAutoresizingMaskIntoConstraints = false
        browseRestaurantsButton.backgroundColor = .systemGreen
        browseRestaurantsButton.setTitle("Browse Restaurants", for: .normal)
        browseRestaurantsButton.setTitleColor(.white, for: .normal)
        browseRestaurantsButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        browseRestaurantsButton.titleLabel?.adjustsFontSizeToFitWidth = true
        browseRestaurantsButton.layer.cornerRadius =  5
        browseRestaurantsButton.addTarget(self, action: #selector(didTapBrowseRestaurant), for: .touchUpInside)
        return browseRestaurantsButton
    }()
    
    lazy var restaurantsTableView : UITableView = {
       let restaurantsTableView = UITableView()
        restaurantsTableView.bounces = false
        restaurantsTableView.separatorStyle = .none
        restaurantsTableView.backgroundColor = .white
        restaurantsTableView.register(RestaurantDisplayTableViewCell.self, forCellReuseIdentifier: RestaurantDisplayTableViewCell.cellIdentifier)
        restaurantsTableView.translatesAutoresizingMaskIntoConstraints = false
       return restaurantsTableView
    }()
   
    func getRestaurantTableViewCellHeight() -> CGFloat{
        return 120
    }
    
    func createRestaurantTableViewCell(indexPath : IndexPath ,restaurentCellContents : (restaurantProfileImage : Data , restaurantName : String , restaurantStarRating : String , restaurantCuisine : String , restaurantLocality : String , deliveryTiming : Int , restaurantOpensNextAt  : String,restaurantIsAvailable : Int,isDeliverable : Bool)) -> UITableViewCell{
        let restaurantTableViewCell = restaurantsTableView.dequeueReusableCell(withIdentifier: RestaurantDisplayTableViewCell.cellIdentifier, for: indexPath) as? RestaurantDisplayTableViewCell
        restaurantTableViewCell?.cellHeight = 120
        restaurantTableViewCell?.configureContent(restaurentCellContents: restaurentCellContents)
        return restaurantTableViewCell ?? UITableViewCell()
    }
    
    
    
    
    private func getRestaurantsTableViewContraints() -> [NSLayoutConstraint]{
        let tableViewContraints = [restaurantsTableView.topAnchor.constraint(equalTo: self.topAnchor),
            restaurantsTableView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
                                   restaurantsTableView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10),
                                   restaurantsTableView.bottomAnchor.constraint(equalTo : self.bottomAnchor)]
        return tableViewContraints
    }
    
   
    
  
    
    private func getNoRestaurantsAvailableLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [noRestaurantsAvailableLabel.topAnchor.constraint(equalTo: self.centerYAnchor ,constant: -100),
                               noRestaurantsAvailableLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                               noRestaurantsAvailableLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10)]
        return labelContraints
    }
    
   
    
    
    
    private func getBrowseRestaurantsButtonLayoutConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [
            browseRestaurantsButton.topAnchor.constraint(equalTo: noRestaurantsAvailableLabel.bottomAnchor,constant: 10),
            browseRestaurantsButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 40),
            browseRestaurantsButton.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -40),
            browseRestaurantsButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    
    @objc  func didTapBrowseRestaurant(){
        delegate?.didTapBrowseRestaurant()
    }
    
}

protocol FavouriteRestaurantsViewDelegate : AnyObject{
    func didTapBrowseRestaurant()
}
