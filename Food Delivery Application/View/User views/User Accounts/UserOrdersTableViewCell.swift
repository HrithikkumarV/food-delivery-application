//
//  OrdersTableView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 20/03/22.
//

import Foundation
import UIKit

class OrderDetailsTableViewCell  : UITableViewCell{
   
    static let cellIdentifier = "orderDetailsCell"
    
    var cellHeight : CGFloat = 44
    private lazy var restaurantNameLabel : UILabel = {
        let restaurantNameLabel = UILabel()
        restaurantNameLabel.textColor = .black.withAlphaComponent(0.8)
        restaurantNameLabel.addLabelToTopBorder(labelText: "Restaurant Name ", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: 0, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        restaurantNameLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantNameLabel.font = UIFont(name:"ArialRoundedMTBold", size: 25.0)
        restaurantNameLabel.textAlignment = .left
        restaurantNameLabel.addBorder(side: .bottom, color: .systemGray4, width: 1)
        restaurantNameLabel.adjustsFontSizeToFitWidth = true
        return restaurantNameLabel
    }()
    private lazy var menuNameAndQuantityLabel : UILabel = {
       let  menuNameAndQuantityLabel = UILabel()
        menuNameAndQuantityLabel.backgroundColor = .white
        menuNameAndQuantityLabel.translatesAutoresizingMaskIntoConstraints = false
        menuNameAndQuantityLabel.textColor =  .black
        menuNameAndQuantityLabel.textAlignment = .left
        return menuNameAndQuantityLabel
    }()
    private lazy var orderTotalAmountLabel : UILabel = {
        let orderTotalAmountLabel = UILabel()
         orderTotalAmountLabel.translatesAutoresizingMaskIntoConstraints = false
         orderTotalAmountLabel.backgroundColor = .white
        orderTotalAmountLabel.textColor = .black.withAlphaComponent(0.8)
         orderTotalAmountLabel.textAlignment = .left
         orderTotalAmountLabel.adjustsFontSizeToFitWidth = true
         orderTotalAmountLabel.font = UIFont(name: "ArialRoundedMTBold", size: 25)
         return orderTotalAmountLabel
    }()
    
    private lazy var dateAndTimeLabel : UILabel = {
        let dateAndTimeLabel = UILabel()
        dateAndTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        dateAndTimeLabel.backgroundColor = .white
        dateAndTimeLabel.textColor = .black.withAlphaComponent(0.8)
        dateAndTimeLabel.textAlignment = .left
        dateAndTimeLabel.adjustsFontSizeToFitWidth = true
        dateAndTimeLabel.font = UIFont(name: "ArialRoundedMTBold", size: 20)
        return dateAndTimeLabel
    }()
    
    private lazy var orderStatusLabel : UILabel = {
       let  orderStatusLabel = UILabel()
        orderStatusLabel.backgroundColor = .white
        orderStatusLabel.translatesAutoresizingMaskIntoConstraints = false
        orderStatusLabel.layer.cornerRadius  = 10
        orderStatusLabel.font = UIFont.systemFont(ofSize: 17,weight: .bold)
        orderStatusLabel.textAlignment = .center
        orderStatusLabel.backgroundColor = .white
        return orderStatusLabel
    }()
    
    private lazy var orderDetailsTableViewCellContentView : UIView = {
      let orderDetailsTableViewCellContentView = UIView()
        orderDetailsTableViewCellContentView.addBorder(side: .bottom, color: .black, width: 1)
        orderDetailsTableViewCellContentView.backgroundColor = .white
        return orderDetailsTableViewCellContentView
    }()
   
    private func updateOrderStaus(orderStatus : OrderStatus) {
        
        switch orderStatus {
        case .Pending:
            orderStatusLabel.text = "Pending"
            orderStatusLabel.textColor = .systemRed
            
        case .Preparing:
            orderStatusLabel.text = "Preparing"
            orderStatusLabel.textColor = .systemGreen
           
        case .Ready:
            orderStatusLabel.text = "Ready"
            orderStatusLabel.textColor = .systemGreen
           
        case .Delivered:
            orderStatusLabel.text = "Delivered"
            orderStatusLabel.textColor = .systemGreen
           
        case .Cancelled:
            orderStatusLabel.text = "Cancelled"
            orderStatusLabel.textColor = .systemRed
        case .Declined :
            orderStatusLabel.text = "Declined"
            orderStatusLabel.textColor = .systemRed
            
        }
    }
    
    private func getOrderStatusLabelConstraints() -> [NSLayoutConstraint]{
            let labelContraints = [
                orderStatusLabel.topAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.topAnchor),
                orderStatusLabel.rightAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.rightAnchor),
                orderStatusLabel.widthAnchor.constraint(equalToConstant: 150),
                orderStatusLabel.heightAnchor.constraint(equalToConstant:30)]
            return labelContraints
    }
        
    
    private func getRestaurantNameLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            restaurantNameLabel.topAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.topAnchor,constant: 15),
            restaurantNameLabel.rightAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.rightAnchor),
            restaurantNameLabel.leftAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.leftAnchor),
            restaurantNameLabel.heightAnchor.constraint(equalToConstant:50)]
        return labelContraints
    }
        
    

    private func getMenuNameAndQuantityLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            menuNameAndQuantityLabel.topAnchor.constraint(equalTo: restaurantNameLabel.bottomAnchor),
            menuNameAndQuantityLabel.rightAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.rightAnchor),
            menuNameAndQuantityLabel.leftAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.leftAnchor),
            menuNameAndQuantityLabel.heightAnchor.constraint(equalToConstant:60)]
        return labelContraints
    }

    private func getOrderTotalAmountLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            orderTotalAmountLabel.topAnchor.constraint(equalTo: menuNameAndQuantityLabel.bottomAnchor,constant: 5),
            orderTotalAmountLabel.leftAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.leftAnchor),
            orderTotalAmountLabel.rightAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.centerXAnchor),
            orderTotalAmountLabel.bottomAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.bottomAnchor)]
        return labelContraints
    }
    
    private func getDateAndTimeLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            dateAndTimeLabel.topAnchor.constraint(equalTo: menuNameAndQuantityLabel.bottomAnchor,constant: 5),
            dateAndTimeLabel.leftAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.centerXAnchor),
            dateAndTimeLabel.rightAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.rightAnchor),
            dateAndTimeLabel.bottomAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.bottomAnchor)]
        return labelContraints
    }

    

   

    private func initialiseViewElements(){
        orderDetailsTableViewCellContentView.addSubview(restaurantNameLabel)
        orderDetailsTableViewCellContentView.addSubview(menuNameAndQuantityLabel)
        orderDetailsTableViewCellContentView.addSubview(orderTotalAmountLabel)
        orderDetailsTableViewCellContentView.addSubview(dateAndTimeLabel)
        orderDetailsTableViewCellContentView.addSubview(orderStatusLabel)
        
        NSLayoutConstraint.activate(getRestaurantNameLabelConstraints())
        NSLayoutConstraint.activate(getOrderStatusLabelConstraints())
        NSLayoutConstraint.activate(getMenuNameAndQuantityLabelConstraints())
        NSLayoutConstraint.activate(getOrderTotalAmountLabelConstraints())
        NSLayoutConstraint.activate(getDateAndTimeLabelConstraints())
       
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(orderDetailsTableViewCellContentView)
        initialiseViewElements()
    }

    func configureContents(orderCellContents : (restaurantName : String , orderStatus : OrderStatus,orderTotal : Int , menuNameAndQuantities : [(menuName : String, quantity : Int)],dateAndTime : String)){
        restaurantNameLabel.text = orderCellContents.restaurantName
        var menuNameAndQuantityString = ""
        for menuNameAndQuantity in orderCellContents.menuNameAndQuantities {
            menuNameAndQuantityString += "\(menuNameAndQuantity.menuName) X \(menuNameAndQuantity.quantity) "
        }
        menuNameAndQuantityLabel.text = menuNameAndQuantityString
        orderTotalAmountLabel.text = "₹\(orderCellContents.orderTotal)"
        dateAndTimeLabel.text = orderCellContents.dateAndTime
        updateOrderStaus(orderStatus: orderCellContents.orderStatus)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        orderDetailsTableViewCellContentView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: cellHeight)
        
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        restaurantNameLabel.text = nil
        menuNameAndQuantityLabel.text = nil
        orderTotalAmountLabel.text = nil
        dateAndTimeLabel.text = nil
        orderStatusLabel.text = nil
        orderStatusLabel.textColor = nil
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}





