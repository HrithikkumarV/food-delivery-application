//
//  UserOrderDetailsPage.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 20/03/22.
//

import Foundation
import UIKit

class UserOrderDetailsPageView : UIView    ,UserOrderDetailsPageViewProtocol{
   
    
    
    weak var delegate : UserOrderDetailsPageViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        
        initialiseViewElements()
        self.bringSubviewToFront(orderBottomView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
    
    private func initialiseViewElements(){
        createOrderDetailsAndRestaurantDetailsList()
        self.addSubview(menuDetailsTableView)
        self.addSubview(instructionsToRestaurant)
        createBillDetailsElements()
        self.addSubview(orderBottomView)
        self.addSubview(cancelOrderReasonLabel)
        self.addSubview(restaurantRatingLabel)
        
        NSLayoutConstraint.activate(getMenuDetailsTableViewLayoutConstraints())
        NSLayoutConstraint.activate(getInstructionsToRestaurantTextViewLayoutContrainst())
        
        NSLayoutConstraint.activate(
            getCancelOrderReasonLabelConstraints())
        NSLayoutConstraint.activate(
            getRestaurantRatingLabelConstraints())
        initialiseOrderBottomViewButtons()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        NSLayoutConstraint.activate(getOrderBottomViewLayoutConstraints())
    }
    
    private func initialiseOrderBottomViewButtons(){
        orderBottomView.addSubview(cancelOrderButton)
        orderBottomView.addSubview(reOrderButton)
        orderBottomView.addSubview(feedbackButton)
        orderBottomView.addSubview(rateOrderButton)
        NSLayoutConstraint.activate(getRateOrderButtonLayoutConstraints())
        NSLayoutConstraint.activate(getCancelOrderButtonLayoutConstraints())
        NSLayoutConstraint.activate(getFeedbackButtonLayoutConstraints())
        NSLayoutConstraint.activate(getReOrderButtonLayoutConstraints())
    }
    
    private func createOrderDetailsAndRestaurantDetailsList(){
        self.addSubview(orderDetailsStackView)
        orderDetailsStackView.addArrangedSubview(orderIdLabel)
        orderDetailsStackView.addArrangedSubview(dateAndTimeLabel)
        orderDetailsStackView.addArrangedSubview(restaurantNameLabel)
        orderDetailsStackView.addArrangedSubview(deliveryAddressLabel)
        NSLayoutConstraint.activate(getOrderStackViewContraints())
    }
   



    private var orderDetailsStackView : UIStackView  = {
        let orderDetailsStackView = UIStackView()
        orderDetailsStackView.axis = .vertical
        orderDetailsStackView.distribution = .fillProportionally
        orderDetailsStackView.spacing = 25
        orderDetailsStackView.translatesAutoresizingMaskIntoConstraints = false
        return orderDetailsStackView
    }()
    
    private lazy var orderIdLabel : UILabel = {
        let orderIdLabel = UILabel()
        orderIdLabel.backgroundColor = .white
        orderIdLabel.translatesAutoresizingMaskIntoConstraints = false
        orderIdLabel.textColor = .black
        orderIdLabel.addLabelToTopBorder(labelText: "Order ID : ", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        orderIdLabel.textAlignment = .left
        orderIdLabel.adjustsFontSizeToFitWidth = true
        orderIdLabel.numberOfLines = 1
        orderIdLabel.lineBreakMode = .byWordWrapping
        orderIdLabel.sizeToFit()
        orderIdLabel.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        return orderIdLabel
    }()
    
    private lazy var dateAndTimeLabel : UILabel = {
       let dateAndTimeLabel = UILabel()
        dateAndTimeLabel.addLabelToTopBorder(labelText: "Order Time : ", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        dateAndTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        dateAndTimeLabel.backgroundColor = .white
        dateAndTimeLabel.textColor = .black
        dateAndTimeLabel.textAlignment = .left
        dateAndTimeLabel.numberOfLines = 2
        dateAndTimeLabel.lineBreakMode = .byWordWrapping
        dateAndTimeLabel.sizeToFit()
        dateAndTimeLabel.adjustsFontSizeToFitWidth = true
        dateAndTimeLabel.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        return dateAndTimeLabel
    }()
    
    private lazy var restaurantNameLabel : UILabel = {
       let restaurantNameLabel = UILabel()
        restaurantNameLabel.addLabelToTopBorder(labelText: "Restaurant Name : ", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        restaurantNameLabel.textColor = .black
        restaurantNameLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantNameLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        restaurantNameLabel.textAlignment = .left
        restaurantNameLabel.numberOfLines = 2
        restaurantNameLabel.lineBreakMode = .byWordWrapping
        restaurantNameLabel.sizeToFit()
        return restaurantNameLabel
    }()
    
    private lazy var deliveryAddressLabel : UILabel = {
       let deliveryAddressLabel = UILabel()
        deliveryAddressLabel.addLabelToTopBorder(labelText: "Delivery Address : ", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        deliveryAddressLabel.sizeToFit()
        deliveryAddressLabel.adjustsFontSizeToFitWidth = true
        deliveryAddressLabel.numberOfLines = 5
        deliveryAddressLabel.lineBreakMode = .byWordWrapping
        deliveryAddressLabel.textColor = .black
        deliveryAddressLabel.translatesAutoresizingMaskIntoConstraints = false
        deliveryAddressLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return deliveryAddressLabel
    }()
    
    private func getOrderStackViewContraints() -> [NSLayoutConstraint]{
        let labelContraints = [orderDetailsStackView.topAnchor.constraint(equalTo: self.topAnchor , constant: 25),
                               orderDetailsStackView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                               orderDetailsStackView.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),]
        return labelContraints
    }
    
    
    var menuDetailsTableView : UITableView = {
       let menuDetailsTableView = UITableView()
        menuDetailsTableView.backgroundColor = .white
        menuDetailsTableView.isScrollEnabled = false
        menuDetailsTableView.separatorStyle = .none
        menuDetailsTableView.layer.borderColor = UIColor.systemGray4.cgColor
        menuDetailsTableView.layer.borderWidth = 1
        menuDetailsTableView.layer.cornerRadius = 5
        menuDetailsTableView.bounces = false
        menuDetailsTableView.register(OrderMenuDetailsTableViewCell.self, forCellReuseIdentifier: OrderMenuDetailsTableViewCell.cellIdentifier)
        menuDetailsTableView.translatesAutoresizingMaskIntoConstraints = false
        return menuDetailsTableView
    }()
    
  
    
    func getOrderMenuCellHeight() -> CGFloat{
        return 60
    }
    
    
    func createMenuDetailsContentView(indexPath : IndexPath ,tableView : UITableView,menuCellContents : ( menuName : String, menuTarianType : String, menuPriceSubTotal : Int, quantity : Int)) -> UITableViewCell{
        let menuDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: OrderMenuDetailsTableViewCell.cellIdentifier, for: indexPath) as? OrderMenuDetailsTableViewCell
        menuDetailsTableViewCell?.configureContents(menuCellContents: menuCellContents)
        menuDetailsTableViewCell?.cellHeight = 60
        return menuDetailsTableViewCell ?? UITableViewCell()
    }
    
    private lazy var instructionsToRestaurant : UITextView = {
        let instructionsToRestaurant = UITextView()
        instructionsToRestaurant.isUserInteractionEnabled = false
        instructionsToRestaurant.backgroundColor = .white
        instructionsToRestaurant.textColor = .systemGray
        instructionsToRestaurant.font = UIFont(name: "ArialHebrew", size: 16)
        instructionsToRestaurant.translatesAutoresizingMaskIntoConstraints = false
        instructionsToRestaurant.layer.cornerRadius = 5
        instructionsToRestaurant.layer.borderColor = UIColor.systemGray5.cgColor
        instructionsToRestaurant.layer.borderWidth = 1
        return instructionsToRestaurant
    }()
    
    
    
    private lazy var billView : UIView = {
        let billView = UIView()
        billView.translatesAutoresizingMaskIntoConstraints = false
        return billView
    }()
    
    private lazy var billstackView : UIStackView = {
       let billstackView = UIStackView()
        billstackView.axis = .vertical
        billstackView.distribution = .fillProportionally
        billstackView.spacing = 5
        billstackView.translatesAutoresizingMaskIntoConstraints = false
        return billstackView
    }()
    
    private lazy var billDetailsLabel : UILabel = {
       let billDetailsLabel = UILabel()
        billDetailsLabel.textColor = .black
        billDetailsLabel.backgroundColor = .white
        billDetailsLabel.font = UIFont(name:"ArialRoundedMTBold", size: 25.0)
        billDetailsLabel.adjustsFontSizeToFitWidth = true
        billDetailsLabel.textAlignment = .left
        billDetailsLabel.text = "Bill Details"
        billDetailsLabel.translatesAutoresizingMaskIntoConstraints = false
        return billDetailsLabel
    }()
    
    private lazy var itemTotalLabel : UILabel = {
        let itemTotalLabel = UILabel()
        itemTotalLabel.textColor = .black.withAlphaComponent(0.9)
        itemTotalLabel.backgroundColor = .white
        itemTotalLabel.font = UIFont.systemFont(ofSize: 14)
        itemTotalLabel.adjustsFontSizeToFitWidth = true
        itemTotalLabel.textAlignment = .left
        itemTotalLabel.text = "Item Total"
        itemTotalLabel.translatesAutoresizingMaskIntoConstraints = false
        return itemTotalLabel
    }()
    
    private lazy var itemTotal : UILabel = {
       let itemTotal = UILabel()
        itemTotal.textColor = .black.withAlphaComponent(0.9)
        itemTotal.backgroundColor = .white
        itemTotal.font = UIFont.systemFont(ofSize: 14)
        itemTotal.adjustsFontSizeToFitWidth = true
        itemTotal.textAlignment = .left
        itemTotal.translatesAutoresizingMaskIntoConstraints = false
        return itemTotal
    }()
    
    private lazy var deliveryFeeLabel : UILabel = {
       let deliveryFeeLabel = UILabel()
        deliveryFeeLabel.textColor = .black.withAlphaComponent(0.9)
        deliveryFeeLabel.backgroundColor = .white
        deliveryFeeLabel.font = UIFont.systemFont(ofSize: 14)
        deliveryFeeLabel.adjustsFontSizeToFitWidth = true
        deliveryFeeLabel.textAlignment = .left
        deliveryFeeLabel.text = "delivery fee"
        deliveryFeeLabel.translatesAutoresizingMaskIntoConstraints = false
        return deliveryFeeLabel
    }()
    
    private lazy var deliveryFee : UILabel = {
       let deliveryFee = UILabel()
        deliveryFee.textColor = .black.withAlphaComponent(0.9)
        deliveryFee.backgroundColor = .white
        deliveryFee.font = UIFont.systemFont(ofSize: 14)
        deliveryFee.adjustsFontSizeToFitWidth = true
        deliveryFee.textAlignment = .left
        deliveryFee.translatesAutoresizingMaskIntoConstraints = false
        return deliveryFee
        
    }()
    
    private lazy var restaurantGSTLabel : UILabel = {
        let restaurantGSTLabel = UILabel()
        restaurantGSTLabel.textColor = .black.withAlphaComponent(0.9)
        restaurantGSTLabel.backgroundColor = .white
        restaurantGSTLabel.font = UIFont.systemFont(ofSize: 14)
        restaurantGSTLabel.adjustsFontSizeToFitWidth = true
        restaurantGSTLabel.textAlignment = .left
        restaurantGSTLabel.text = "Restaurant GST @ 5%"
        restaurantGSTLabel.translatesAutoresizingMaskIntoConstraints = false
        return restaurantGSTLabel
    }()
    
    private lazy var restaurantPackagingChargesLabel : UILabel = {
            let restaurantPackagingChargesLabel = UILabel()
            restaurantPackagingChargesLabel.textColor = .black.withAlphaComponent(0.9)
            restaurantPackagingChargesLabel.backgroundColor = .white
            restaurantPackagingChargesLabel.font = UIFont.systemFont(ofSize: 14)
            restaurantPackagingChargesLabel.adjustsFontSizeToFitWidth = true
            restaurantPackagingChargesLabel.textAlignment = .left
            restaurantPackagingChargesLabel.text = "Restaurant Packaging Charges"
            restaurantPackagingChargesLabel.translatesAutoresizingMaskIntoConstraints = false
            return restaurantPackagingChargesLabel
        }()
    
    private lazy var restaurantGST : UILabel = {
       let restaurantGST = UILabel()
        restaurantGST.textColor = .black.withAlphaComponent(0.9)
        restaurantGST.backgroundColor = .white
        restaurantGST.font = UIFont.systemFont(ofSize: 14)
        restaurantGST.adjustsFontSizeToFitWidth = true
        restaurantGST.textAlignment = .left
        restaurantGST.translatesAutoresizingMaskIntoConstraints = false
        return restaurantGST
    }()
    
    private lazy var restaurantPackagingCharges : UILabel = {
        let restaurantPackagingCharges = UILabel()
        restaurantPackagingCharges.textColor = .black.withAlphaComponent(0.9)
        restaurantPackagingCharges.backgroundColor = .white
        restaurantPackagingCharges.font = UIFont.systemFont(ofSize: 14)
        restaurantPackagingCharges.adjustsFontSizeToFitWidth = true
        restaurantPackagingCharges.textAlignment = .left
        restaurantPackagingCharges.translatesAutoresizingMaskIntoConstraints = false
        return restaurantPackagingCharges
    }()
    
    private lazy var itemDiscountLabel : UILabel = {
       let itemDiscountLabel = UILabel()
        itemDiscountLabel.textColor = .systemGreen
        itemDiscountLabel.backgroundColor = .white
        itemDiscountLabel.font = UIFont.systemFont(ofSize: 14)
        itemDiscountLabel.adjustsFontSizeToFitWidth = true
        itemDiscountLabel.textAlignment = .left
        itemDiscountLabel.text = "Item Discount"
        itemDiscountLabel.translatesAutoresizingMaskIntoConstraints = false
        return itemDiscountLabel
    }()
    
    private lazy var itemDiscount : UILabel = {
        let itemDiscount = UILabel()
        itemDiscount.textColor = .systemGreen.withAlphaComponent(0.9)
        itemDiscount.backgroundColor = .white
        itemDiscount.font = UIFont.systemFont(ofSize: 14)
        itemDiscount.adjustsFontSizeToFitWidth = true
        itemDiscount.textAlignment = .left
        itemDiscount.translatesAutoresizingMaskIntoConstraints = false
        return itemDiscount
    }()
    
    private lazy var totalAmountLabel : UILabel  = {
       let totalAmountLabel = UILabel()
        totalAmountLabel.textColor = .black
        totalAmountLabel.backgroundColor = .white
        totalAmountLabel.font = UIFont(name:"ArialRoundedMTBold", size: 25.0)
        totalAmountLabel.adjustsFontSizeToFitWidth = true
        totalAmountLabel.textAlignment = .left
        totalAmountLabel.text = "Bill Total"
        totalAmountLabel.translatesAutoresizingMaskIntoConstraints = false
        return totalAmountLabel
    }()
    
    private lazy var totalAmount : UILabel = {
        let totalAmount = UILabel()
        totalAmount.textColor = .black
        totalAmount.backgroundColor = .white
        totalAmount.font = UIFont(name:"ArialRoundedMTBold", size: 25.0)
        totalAmount.adjustsFontSizeToFitWidth = true
        totalAmount.textAlignment = .left
        totalAmount.translatesAutoresizingMaskIntoConstraints = false
        return totalAmount
    }()
    private lazy var orderBottomView : UIView = {
       let orderBottomView = UIView()
        orderBottomView.translatesAutoresizingMaskIntoConstraints = false
        orderBottomView.backgroundColor = .white
        return orderBottomView
    }()
    
     lazy var cancelOrderButton : UIButton = {
        let  cancelOrderButton = UIButton()
        cancelOrderButton.translatesAutoresizingMaskIntoConstraints = false
        cancelOrderButton.backgroundColor = .white
        cancelOrderButton.setTitle("Cancel", for: .normal)
        cancelOrderButton.setTitleColor(.systemRed, for: .normal)
        cancelOrderButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        cancelOrderButton.layer.cornerRadius = 5
        cancelOrderButton.layer.borderWidth = 1
        cancelOrderButton.titleLabel?.adjustsFontSizeToFitWidth = true
        cancelOrderButton.layer.borderColor = UIColor.systemGray.cgColor
        cancelOrderButton.addTarget(self, action: #selector(didTapCancelOrder), for: .touchUpInside)
        return cancelOrderButton
    }()
    
     lazy var reOrderButton : UIButton = {
        let reOrderButton = UIButton()
        reOrderButton.translatesAutoresizingMaskIntoConstraints = false
        reOrderButton.backgroundColor = .white
        reOrderButton.setTitle("REORDER", for: .normal)
        reOrderButton.setTitleColor(.systemGreen, for: .normal)
        reOrderButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        reOrderButton.layer.cornerRadius = 5
        reOrderButton.layer.borderWidth = 1
        reOrderButton.layer.borderColor = UIColor.systemGray.cgColor
        reOrderButton.addTarget(self, action: #selector(didTapReorderButton), for: .touchUpInside)
        return reOrderButton
    }()
    
     lazy var rateOrderButton : UIButton = {
        let rateOrderButton = UIButton()
        rateOrderButton.translatesAutoresizingMaskIntoConstraints = false
        rateOrderButton.backgroundColor = .white
        rateOrderButton.setTitle("RATE ORDER", for: .normal)
        rateOrderButton.setTitleColor(.black, for: .normal)
        rateOrderButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        rateOrderButton.layer.cornerRadius = 5
        rateOrderButton.layer.borderWidth = 1
        rateOrderButton.layer.borderColor = UIColor.systemGray.cgColor
        rateOrderButton.addTarget(self, action: #selector(didTapRateOrderButton), for: .touchUpInside)
        return rateOrderButton
    }()
    
     lazy var feedbackButton : UIButton = {
        let feedbackButton = UIButton()
        feedbackButton.translatesAutoresizingMaskIntoConstraints = false
        feedbackButton.backgroundColor = .white
        feedbackButton.setTitle("FEEDBACK", for: .normal)
        feedbackButton.setTitleColor(.black, for: .normal)
        feedbackButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        feedbackButton.layer.cornerRadius = 5
        feedbackButton.layer.borderWidth = 1
        feedbackButton.layer.borderColor = UIColor.systemGray.cgColor
        feedbackButton.addTarget(self, action: #selector(didTapFeedBack), for: .touchUpInside)
        return feedbackButton
    }()
    
     lazy var cancelOrderReasonLabel : UILabel = {
        let cancelOrderReasonLabel = UILabel()
        cancelOrderReasonLabel.textColor = .black
        cancelOrderReasonLabel.backgroundColor = .white
        cancelOrderReasonLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        cancelOrderReasonLabel.adjustsFontSizeToFitWidth = true
        cancelOrderReasonLabel.textAlignment = .left
        cancelOrderReasonLabel.translatesAutoresizingMaskIntoConstraints = false
        cancelOrderReasonLabel.numberOfLines = 5
        cancelOrderReasonLabel.lineBreakMode = .byWordWrapping
        cancelOrderReasonLabel.textAlignment = .natural
        cancelOrderReasonLabel.sizeToFit()
        return cancelOrderReasonLabel
    }()
    
    lazy var restaurantRatingLabel : UILabel = {
        let restaurantRatingLabel = UILabel()
        restaurantRatingLabel.textColor = .black
        restaurantRatingLabel.backgroundColor = .white
        restaurantRatingLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        restaurantRatingLabel.textAlignment = .left
        restaurantRatingLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantRatingLabel.numberOfLines = 5
        restaurantRatingLabel.lineBreakMode = .byWordWrapping
        restaurantRatingLabel.textAlignment = .left
        restaurantRatingLabel.sizeToFit()
        restaurantRatingLabel.addLabelToTopBorder(labelText: "Restaurant Rating And Feed back", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        return restaurantRatingLabel
    }()

    
   
    
    
    
    
    
    
    func UpdateOrderAndUserDetailsContents(orderId : String, dateAndTime  :String, restaurantName : String , deliveryAddress: AddressDetails){
        orderIdLabel.text = orderId
        deliveryAddressLabel.text = "\(deliveryAddress.doorNoAndBuildingNameAndBuildingNo), \(deliveryAddress.streetName), \(deliveryAddress.landmark), \(deliveryAddress.localityName), \(deliveryAddress.city), \(deliveryAddress.state), \(deliveryAddress.country), \(deliveryAddress.pincode)"
        restaurantNameLabel.text = restaurantName
        dateAndTimeLabel.text =   dateAndTime
    }
    
  

    
    private func getMenuDetailsTableViewLayoutConstraints() -> [NSLayoutConstraint]{
            let tableViewContraints = [menuDetailsTableView.topAnchor.constraint(equalTo: deliveryAddressLabel.bottomAnchor,constant: 30),
                                       menuDetailsTableView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                                       menuDetailsTableView.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),]
            return tableViewContraints
    }
    
   func setMenuDetailsTableViewHeight(numberOfRows : Int)  {
        menuDetailsTableView.heightAnchor.constraint(equalToConstant: CGFloat(numberOfRows) * getOrderMenuCellHeight()).isActive = true
    }
    
    
    
    func updateInstructionsToRestaurant(instructionsToRestaurantText :String){
        instructionsToRestaurant.text = instructionsToRestaurantText
    }
    
    private func getInstructionsToRestaurantTextViewLayoutContrainst() -> [NSLayoutConstraint]{
        let textViewConstraints = [instructionsToRestaurant.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),instructionsToRestaurant.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),instructionsToRestaurant.topAnchor.constraint(equalTo: menuDetailsTableView.bottomAnchor,constant: 15),instructionsToRestaurant.heightAnchor.constraint(equalToConstant: 80)]
        return textViewConstraints
    }
    
    
    
    
    
    
    private func getHorizontalStackView() -> UIStackView{
        let horizontalStackView = UIStackView()
        horizontalStackView.axis = .horizontal
        horizontalStackView.distribution = .fillProportionally
        return horizontalStackView
    }
    
    func createBillDetailsElements(){
            createBillView()
            createBillStackView()
            applyBillDetailsLabelConstrainst()
            applyItemTotalLabelsConstrainst()
            applyDeliveryFeeLabelsConstrainst()
            applyRestaurantGSTLabelsConstrainst()
            applyRestaurantPackageingChargesLabelsConstrainst()
            applyItemDiscountLabelsConstrainst()
            billstackView.addArrangedSubview(createLineView(color: .systemGray5))
            applyToPayLabelsConstrainst()
            
    }
        
    private func createBillView(){
            self.addSubview(billView)
            NSLayoutConstraint.activate(getBillViewLayOutConstraints())
            
    }

    private func createBillStackView(){
            billView.addSubview(billstackView)
            NSLayoutConstraint.activate(getBillStackViewLayOutConstraints())
        }

    
    
    private func  getBillViewLayOutConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [billView.topAnchor.constraint(equalTo: instructionsToRestaurant.bottomAnchor,constant: 20),
                               billView.leftAnchor.constraint(equalTo: self.leftAnchor),
                               billView.rightAnchor.constraint(equalTo: self.rightAnchor),
                               billView.heightAnchor.constraint(equalToConstant: 200)]
        return viewConstraints
    }
    
    
    func updateBillDetails(itemTotalPrice : Int,restaurantPackagingChargePrice : Int, deliveryFeePrice : Int , totalAmount : Int,discountAmount : Int , restaurantGST : Double){
        updateItemDiscount(itemDiscountPrice : discountAmount)
        updateBillDetialsLabels(itemTotalPrice: itemTotalPrice, restaurantGSTPrice: restaurantGST, totalAmountpaid: totalAmount)
        setDeliveryAndPackageingFee(deliveryFeePrice: deliveryFeePrice, restaurantPackagingChargePrice: restaurantPackagingChargePrice)
    }
    
   
    private func updateItemDiscount(itemDiscountPrice :Int){
        if(itemDiscountPrice != 0){
            itemDiscountLabelsStackView.isHidden = false
            itemDiscount.text = "- ₹ \(itemDiscountPrice)"
        }
        else{
            itemDiscount.text = "- ₹ \(itemDiscountPrice)"
            itemDiscountLabelsStackView.isHidden = true
        }
    }
    
   
    private func updateBillDetialsLabels(itemTotalPrice : Int,restaurantGSTPrice : Double,totalAmountpaid : Int){
        itemTotal.text = "₹ \(itemTotalPrice)"
        restaurantGST.text =  "₹ \(restaurantGSTPrice)"
        totalAmount.text = "₹ \(totalAmountpaid)"
    }
    
    private func setDeliveryAndPackageingFee(deliveryFeePrice : Int,restaurantPackagingChargePrice : Int){
        deliveryFee.text = "₹ \(deliveryFeePrice)"
        restaurantPackagingCharges.text = "₹ \(restaurantPackagingChargePrice)"
    }
    
    
    
    private func getBillStackViewLayOutConstraints() -> [NSLayoutConstraint]{
        let stackConstraint = [billstackView.topAnchor.constraint(equalTo: billView.topAnchor,constant: 20),
                               billstackView.leftAnchor.constraint(equalTo: billView.leftAnchor,constant: 10),
                               billstackView.rightAnchor.constraint(equalTo: billView.rightAnchor,constant: -10),
                            
        ]
        return stackConstraint
    }
    
   
    private func applyBillDetailsLabelConstrainst(){
        billDetailsLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        billstackView.addArrangedSubview(billDetailsLabel)
    }
    
    private func applyItemTotalLabelsConstrainst(){
        let itemTotalLabelsStackView = getHorizontalStackView()
        itemTotalLabelsStackView.addArrangedSubview(itemTotalLabel)
        itemTotalLabelsStackView.addArrangedSubview(itemTotal)
        billstackView.addArrangedSubview(itemTotalLabelsStackView)
        itemTotal.widthAnchor.constraint(equalToConstant: 80).isActive = true
        itemTotal.heightAnchor.constraint(equalToConstant: 20).isActive = true
        itemTotalLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
    }
    
    private func applyDeliveryFeeLabelsConstrainst(){
        let deliveryFeeLabelsStackView = getHorizontalStackView()
        deliveryFeeLabelsStackView.addArrangedSubview(deliveryFeeLabel)
        deliveryFeeLabelsStackView.addArrangedSubview(deliveryFee)
        billstackView.addArrangedSubview(deliveryFeeLabelsStackView)
        deliveryFee.widthAnchor.constraint(equalToConstant: 80).isActive = true
        deliveryFee.heightAnchor.constraint(equalToConstant: 20).isActive = true
        deliveryFeeLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func applyRestaurantGSTLabelsConstrainst(){
        let restaurantGSTLabelsStackView = getHorizontalStackView()
        restaurantGSTLabelsStackView.addArrangedSubview(restaurantGSTLabel)
        restaurantGSTLabelsStackView.addArrangedSubview(restaurantGST)
        billstackView.addArrangedSubview(restaurantGSTLabelsStackView)
        restaurantGST.widthAnchor.constraint(equalToConstant: 80).isActive = true
        restaurantGST.heightAnchor.constraint(equalToConstant: 20).isActive = true
        restaurantGSTLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func applyRestaurantPackageingChargesLabelsConstrainst(){
        let restaurantPackagingChargesLabelsStackView = getHorizontalStackView()
        restaurantPackagingChargesLabelsStackView.addArrangedSubview(restaurantPackagingChargesLabel)
        restaurantPackagingChargesLabelsStackView.addArrangedSubview(restaurantPackagingCharges)
        billstackView.addArrangedSubview(restaurantPackagingChargesLabelsStackView)
        restaurantPackagingCharges.widthAnchor.constraint(equalToConstant: 80).isActive = true
        restaurantPackagingCharges.heightAnchor.constraint(equalToConstant: 20).isActive = true
        restaurantPackagingChargesLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private lazy var itemDiscountLabelsStackView : UIStackView =
    {
        let itemDiscountLabelsStackView = getHorizontalStackView()
        return itemDiscountLabelsStackView
    }()
    
    private func applyItemDiscountLabelsConstrainst(){
        itemDiscountLabelsStackView.addArrangedSubview(itemDiscountLabel)
        itemDiscountLabelsStackView.addArrangedSubview(itemDiscount)
        billstackView.addArrangedSubview(itemDiscountLabelsStackView)
        itemDiscount.widthAnchor.constraint(equalToConstant: 90).isActive = true
        itemDiscount.heightAnchor.constraint(equalToConstant: 20).isActive = true
        itemDiscountLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
    }

    
    private func applyToPayLabelsConstrainst(){
        let totalAmountLabelsStackView = getHorizontalStackView()
        totalAmountLabelsStackView.addArrangedSubview(totalAmountLabel)
        totalAmountLabelsStackView.addArrangedSubview(totalAmount)
        billstackView.addArrangedSubview(totalAmountLabelsStackView)
        totalAmount.widthAnchor.constraint(equalToConstant: 80).isActive = true
        totalAmount.heightAnchor.constraint(equalToConstant: 20).isActive = true
        totalAmountLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func createLineView(color : UIColor) -> UIView{
        let lineView = UIView()
        lineView.translatesAutoresizingMaskIntoConstraints = false
        lineView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        lineView.backgroundColor = color
        return lineView
    }
    
    
   
    
    private func getOrderBottomViewLayoutConstraints() -> [NSLayoutConstraint]{
        let viewContraints = [
            orderBottomView.bottomAnchor.constraint(equalTo:UIApplication.shared.windows.first?.safeAreaLayoutGuide.bottomAnchor ?? self.safeAreaLayoutGuide.bottomAnchor),
            orderBottomView.leftAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leftAnchor),
            orderBottomView.rightAnchor.constraint(equalTo: self.safeAreaLayoutGuide.rightAnchor),
            orderBottomView.heightAnchor.constraint(equalToConstant: 80)]
        return viewContraints
    }
   
    
    func updateCancelOrderReason(cancelReason : String ,orderStatus : OrderStatus){
        if(orderStatus == .Cancelled){
            cancelOrderReasonLabel.addLabelToTopBorder(labelText: "Cancellation Reason : ", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        }
        else if(orderStatus == .Declined){
            cancelOrderReasonLabel.addLabelToTopBorder(labelText: "Declination Reason : ", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        }
        cancelOrderReasonLabel.text = cancelReason
    }
    
    private func getCancelOrderReasonLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            cancelOrderReasonLabel.topAnchor.constraint(equalTo: billView.bottomAnchor,constant: 50),
            cancelOrderReasonLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            cancelOrderReasonLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            cancelOrderReasonLabel.heightAnchor.constraint(equalToConstant: 60)]
        return labelContraints
    }
    
    
    
    func updateRatingAndFeedback(rating : Int , restaurantFeedback : String){
        if(rating != 0 ){
            restaurantRatingLabel.text = "\(rating) ⭐️  |  \(restaurantFeedback.count > 0 ? restaurantFeedback : "No Feedback")"
        }
        else{
            restaurantRatingLabel.text = "Not Rated Yet"
        }
    }
    
    private func getRestaurantRatingLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            restaurantRatingLabel.topAnchor.constraint(equalTo: billView.bottomAnchor,constant: 100),
            restaurantRatingLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            restaurantRatingLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            restaurantRatingLabel.heightAnchor.constraint(equalToConstant: 60)]
        return labelContraints
    }
    
    
    
    private func getCancelOrderButtonLayoutConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [
            cancelOrderButton.topAnchor.constraint(equalTo: orderBottomView.topAnchor),
            cancelOrderButton.leftAnchor.constraint(equalTo: orderBottomView.leftAnchor,constant: 10),
            cancelOrderButton.rightAnchor.constraint(equalTo: orderBottomView.rightAnchor,constant: -10),
            cancelOrderButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    
    
    private func getReOrderButtonLayoutConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [
            reOrderButton.topAnchor.constraint(equalTo: orderBottomView.topAnchor),
            reOrderButton.leftAnchor.constraint(equalTo: orderBottomView.centerXAnchor,constant: 5),
            reOrderButton.rightAnchor.constraint(equalTo: orderBottomView.rightAnchor,constant: -10),
            reOrderButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    
    
    private func getRateOrderButtonLayoutConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [
            rateOrderButton.topAnchor.constraint(equalTo: orderBottomView.topAnchor),
            rateOrderButton.leftAnchor.constraint(equalTo: orderBottomView.leftAnchor,constant: 10),
            rateOrderButton.rightAnchor.constraint(equalTo: orderBottomView.centerXAnchor,constant: -5),
            rateOrderButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    
    
    private func getFeedbackButtonLayoutConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [
            feedbackButton.topAnchor.constraint(equalTo: orderBottomView.topAnchor),
            feedbackButton.leftAnchor.constraint(equalTo: orderBottomView.leftAnchor,constant: 10),
            feedbackButton.rightAnchor.constraint(equalTo: orderBottomView.centerXAnchor,constant: -5),
            feedbackButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    func activateContentViewBottomAnchorWithRespectToRestaurantRatingLabelConstraints(){
        if(restaurantRatingLabel.frame.maxY > 0){
            self.removeHeightAnchorConstraints()
            self.heightAnchor.constraint(equalToConstant:  restaurantRatingLabel.frame.maxY + 100).isActive = true
        }
        else{
            self.removeHeightAnchorConstraints()
            self.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.maxY).isActive = true
        }
    }
    
    
    

    
    @objc  func didTapFeedBack(){
        delegate?.didTapFeedBack()
    }
   
  
    @objc func didTapRateOrderButton(){
        delegate?.didTapRateOrderButton()
    }
    
    @objc  func didTapReorderButton(){
        delegate?.didTapReorderButton()
    }
    
    @objc func didTapCancelOrder(){
        delegate?.didTapCancelOrder()
    }
    
}


class UserOrderDetailsPageNavigationBarTitleView : UIView,UserOrderDetailsPageViewNavigationBarTitleViewProtocol{
    
    private lazy var orderStausNavigationBarLabel : UILabel = {
        let orderStausNavigationBarLabel = UILabel()
        orderStausNavigationBarLabel.textColor = .black.withAlphaComponent(0.9)
        orderStausNavigationBarLabel.backgroundColor = .clear
        orderStausNavigationBarLabel.font = UIFont(name:"ArialRoundedMTBold", size: 25.0)
        orderStausNavigationBarLabel.adjustsFontSizeToFitWidth = true
        orderStausNavigationBarLabel.textAlignment = .left
        return orderStausNavigationBarLabel
    }()
    
    private lazy var totalItemsAndPriceNavigationBarLabel : UILabel = {
       let totalItemsAndPriceNavigationBarLabel = UILabel()
        totalItemsAndPriceNavigationBarLabel.textColor = .gray
        totalItemsAndPriceNavigationBarLabel.backgroundColor = .clear
        totalItemsAndPriceNavigationBarLabel.textAlignment = .left
        totalItemsAndPriceNavigationBarLabel.font = UIFont(name:"ArialRoundedMTBold", size: 12.0)
        totalItemsAndPriceNavigationBarLabel.lineBreakMode = .byWordWrapping
        totalItemsAndPriceNavigationBarLabel.adjustsFontSizeToFitWidth = true
        return totalItemsAndPriceNavigationBarLabel
    }()
    
   
    
    init(){
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 100, height: 44))
        self.backgroundColor = .clear
        self.addSubview(orderStausNavigationBarLabel)
        self.addSubview(totalItemsAndPriceNavigationBarLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        orderStausNavigationBarLabel.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 22)
        totalItemsAndPriceNavigationBarLabel.frame = CGRect(x: 0, y: orderStausNavigationBarLabel.frame.maxY, width: self.frame.width, height: 22)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func updateTotalItemsAndPriceNavigationBarLabelText(menuCount : Int,totalPrice : Int){
        
        if(menuCount == 1){
            totalItemsAndPriceNavigationBarLabel.text =
            " \(menuCount) Item | ₹\(totalPrice)"
        }
        else{
            totalItemsAndPriceNavigationBarLabel.text = " \(menuCount) Items | ₹\(totalPrice)"
        }
    }
    
    private func updateOrderStatus(orderStatus : OrderStatus){
        switch orderStatus {
        case .Pending:
            orderStausNavigationBarLabel.text = "Order Pending"
            orderStausNavigationBarLabel.textColor = .systemRed
            totalItemsAndPriceNavigationBarLabel.textColor = .systemRed

        case .Preparing:
            orderStausNavigationBarLabel.text = "Order Preparing"
            orderStausNavigationBarLabel.textColor = .systemGreen
            totalItemsAndPriceNavigationBarLabel.textColor = .systemGreen

        case .Ready:
            orderStausNavigationBarLabel.text = "Order Ready"
            orderStausNavigationBarLabel.textColor = .systemGreen
            totalItemsAndPriceNavigationBarLabel.textColor = .systemGreen

        case .Delivered:
            orderStausNavigationBarLabel.text = "Order Delivered"
            orderStausNavigationBarLabel.textColor = .systemGreen
            totalItemsAndPriceNavigationBarLabel.textColor = .systemGreen

        case .Cancelled:
            orderStausNavigationBarLabel.text = "Order Cancelled"
            orderStausNavigationBarLabel.textColor = .systemRed
            totalItemsAndPriceNavigationBarLabel.textColor = .systemRed

        case .Declined:
            orderStausNavigationBarLabel.text = "Order Declined"
            orderStausNavigationBarLabel.textColor = .systemRed
            totalItemsAndPriceNavigationBarLabel.textColor = .systemRed

        }

    }
    
   
    
    func updateNavigationBarTitle(menuCount : Int,totalPrice : Int,orderStatus : OrderStatus){
        updateTotalItemsAndPriceNavigationBarLabelText(menuCount: menuCount, totalPrice: totalPrice)
        updateOrderStatus(orderStatus: orderStatus)
    }
}


protocol UserOrderDetailsPageViewDelegate : AnyObject{
     func didTapFeedBack()
   
  
      func didTapRateOrderButton()
    
    
      func didTapReorderButton()
    
     func didTapCancelOrder()
}
