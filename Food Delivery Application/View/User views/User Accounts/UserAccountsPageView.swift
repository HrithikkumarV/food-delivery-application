//
//  UserAccountsPageView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 17/03/22.
//

import Foundation
import UIKit
class UserAccountsPageView : UIView , UserAccountsPageViewProtocol{
   
    weak var delegate : UserAccountsPageViewDelegate?
    
    private lazy var userNameLabel : UILabel = {
       let userNameLabel = UILabel()
        userNameLabel.textColor = .black.withAlphaComponent(0.9)
        userNameLabel.translatesAutoresizingMaskIntoConstraints = false
        userNameLabel.font = UIFont(name:"ArialRoundedMTBold", size: 25.0)
        userNameLabel.textAlignment = .left
        userNameLabel.translatesAutoresizingMaskIntoConstraints = false
        return userNameLabel
    }()
    
    private lazy var userPhoneNumberLabel : UILabel = {
        let userPhoneNumberLabel = UILabel()
        userPhoneNumberLabel.textColor = .black.withAlphaComponent(0.9)
        userPhoneNumberLabel.translatesAutoresizingMaskIntoConstraints = false
        userPhoneNumberLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        userPhoneNumberLabel.textAlignment = .left
        userPhoneNumberLabel.translatesAutoresizingMaskIntoConstraints = false
        userPhoneNumberLabel.addBorder(side: .bottom, color: .systemGray4, width: 1)
        return userPhoneNumberLabel
    }()
    
    private lazy var manageAddressesButton : UIButton = {
        let manageAddressesButton = UIButton()
        manageAddressesButton.setTitle("🏠 Manage Addresses", for: .normal)
        manageAddressesButton.setTitleColor(.black, for: .normal)
        manageAddressesButton.addRightImageToButton(systemName: "chevron.backward", imageColor: .lightGray, padding: 10)
        manageAddressesButton.addBorder(side: .bottom, color: .systemGray4, width: 1)
        manageAddressesButton.translatesAutoresizingMaskIntoConstraints = false
        manageAddressesButton.contentHorizontalAlignment = .left
        manageAddressesButton.addTarget(self, action: #selector(didTapManageAddresses), for: .touchUpInside)
        return manageAddressesButton
    }()
    
    private lazy var favouriteRestaurantsButton : UIButton = {
        let favouriteRestaurantsButton = UIButton()
        favouriteRestaurantsButton.setTitle("❤️ Favourite Restaurants", for: .normal)
        favouriteRestaurantsButton.setTitleColor(.black, for: .normal)
        favouriteRestaurantsButton.addRightImageToButton(systemName: "chevron.backward", imageColor: .lightGray, padding: 10)
        favouriteRestaurantsButton.addBorder(side: .bottom, color: .systemGray4, width: 1)
        favouriteRestaurantsButton.contentHorizontalAlignment = .left
        favouriteRestaurantsButton.translatesAutoresizingMaskIntoConstraints = false
        favouriteRestaurantsButton.addTarget(self, action: #selector(didTapFavouriteRestaurants), for: .touchUpInside)
        return favouriteRestaurantsButton
    }()
    
    private lazy var contactUSLabel : UILabel = {
        let contactUSLabel = UILabel()
        contactUSLabel.text = " Contact us : " + ContactTeam.getContactTeamEmailId()
        contactUSLabel.adjustsFontSizeToFitWidth = true
        contactUSLabel.textAlignment = .left
        contactUSLabel.translatesAutoresizingMaskIntoConstraints = false
        contactUSLabel.addBorder(side: .bottom, color: .systemGray4, width: 1)
        return contactUSLabel
    }()
    
    
    private lazy var logOutButton : UIButton = {
        let logOutButton = UIButton()
        logOutButton.setTitle("LOGOUT", for: .normal)
        logOutButton.setTitleColor(.systemBlue, for: .normal)
        logOutButton.contentMode = .left
        logOutButton.contentHorizontalAlignment = .left
        logOutButton.translatesAutoresizingMaskIntoConstraints = false
        logOutButton.addTarget(self, action: #selector(didTapLogout), for: .touchUpInside)
        return logOutButton
    }()
    
    private lazy var editButton : UIButton = {
        let editButton = UIButton()
        editButton.setTitle("EDIT", for: .normal)
        editButton.setTitleColor(.systemBlue, for: .normal)
        editButton.translatesAutoresizingMaskIntoConstraints = false
        editButton.addTarget(self, action: #selector(didTapEditButton), for: .touchUpInside)
        return editButton
    }()
    
    
    
    lazy var viewMoreOrdersButton : UIButton = {
        let viewMoreOrdersButton = UIButton()
        viewMoreOrdersButton.setTitle("View More Orders", for: .normal)
        viewMoreOrdersButton.setTitleColor(.systemGreen, for: .normal)
        viewMoreOrdersButton.backgroundColor = .white
        viewMoreOrdersButton.translatesAutoresizingMaskIntoConstraints = false
        viewMoreOrdersButton.contentMode = .left
        viewMoreOrdersButton.contentHorizontalAlignment = .left
        viewMoreOrdersButton.addTarget(self, action: #selector(didTapViewMorePastOrders), for: .touchUpInside)

        return viewMoreOrdersButton
    }()
    
    
    
    var orderDetailsTableView : UITableView = {
        let orderDetailsTableView = UITableView()
        orderDetailsTableView.backgroundColor = .white
        orderDetailsTableView.isScrollEnabled = false
        orderDetailsTableView.bounces = false
        orderDetailsTableView.register(OrderDetailsTableViewCell.self, forCellReuseIdentifier: OrderDetailsTableViewCell.cellIdentifier)
        orderDetailsTableView.translatesAutoresizingMaskIntoConstraints = false
        orderDetailsTableView.separatorStyle = .none
        return orderDetailsTableView
    }()
    
    
    func getOrderTabelViewCellHeight() -> CGFloat{
        return 160
    }
    
    func createOrdersTableViewCell(indexPath : IndexPath ,tableView : UITableView, orderCellContents : (restaurantName : String , orderStatus : OrderStatus,orderTotal : Int , menuNameAndQuantities : [(menuName : String, quantity : Int)],dateAndTime : String)) -> UITableViewCell{
        let orderDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: OrderDetailsTableViewCell.cellIdentifier, for: indexPath) as? OrderDetailsTableViewCell
        orderDetailsTableViewCell?.configureContents(orderCellContents: orderCellContents)
        orderDetailsTableViewCell?.cellHeight = 160
        return orderDetailsTableViewCell ?? UITableViewCell()
    }

    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        self.addSubview(userNameLabel)
        self.addSubview(userPhoneNumberLabel)
        self.addSubview(editButton)
        self.addSubview(manageAddressesButton)
        self.addSubview(favouriteRestaurantsButton)
        self.addSubview(contactUSLabel)
        self.addSubview(logOutButton)
        self.addSubview(orderDetailsTableView)
        self.addSubview(viewMoreOrdersButton)
        NSLayoutConstraint.activate(getUserNameLabelContraints())
        NSLayoutConstraint.activate(getUserPhoneNumberLabelContraints())
        NSLayoutConstraint.activate(getEditButtonContraints())
        NSLayoutConstraint.activate(getManageAddressesButtonContraints())
        NSLayoutConstraint.activate(getFavouriteRestaurantsButtonContraints())
        NSLayoutConstraint.activate(getContactUsLabelContraints())
        NSLayoutConstraint.activate(getLogoutButtonContraints())
        NSLayoutConstraint.activate(getOrderDetailsTableViewLayoutConstraints())
        NSLayoutConstraint.activate(getviewMoreOrdersButtonContraints())
    }
    
    

    
   
    func updateUserDetails(userName : String,userPhoneNumber : String){
        userPhoneNumberLabel.text = "📞 \(userPhoneNumber)"
        userNameLabel.text = "👤 \(userName)"
        
    }
        
    private func getEditButtonContraints() -> [NSLayoutConstraint] {
        let buttonContraints = [
            editButton.topAnchor.constraint(equalTo: self.topAnchor,constant: 10),
            editButton.widthAnchor.constraint(equalToConstant: 70),
            editButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
            editButton.heightAnchor.constraint(equalToConstant: 20)]
        return buttonContraints
    }
    
    
    private func getUserNameLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [userNameLabel.topAnchor.constraint(equalTo: self.topAnchor , constant: 10 ),
                               userNameLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                               userNameLabel.rightAnchor.constraint(equalTo: editButton.leftAnchor,constant: -5),
                               userNameLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    
    private func getUserPhoneNumberLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
            userPhoneNumberLabel.topAnchor.constraint(equalTo: userNameLabel.bottomAnchor,constant: 5),
            userPhoneNumberLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            userPhoneNumberLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            userPhoneNumberLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    
    
    
    private func getManageAddressesButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [manageAddressesButton.topAnchor.constraint(equalTo: userPhoneNumberLabel.bottomAnchor,constant: 5),
                               manageAddressesButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                               manageAddressesButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
                               manageAddressesButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    private func getFavouriteRestaurantsButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [favouriteRestaurantsButton.topAnchor.constraint(equalTo: manageAddressesButton.bottomAnchor,constant: 5),
                                favouriteRestaurantsButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                                favouriteRestaurantsButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
                                favouriteRestaurantsButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    private func getContactUsLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [contactUSLabel.topAnchor.constraint(equalTo: favouriteRestaurantsButton.bottomAnchor,constant: 5),
                               contactUSLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                               contactUSLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
                               contactUSLabel.heightAnchor.constraint(equalToConstant: 40)]
        return labelContraints
    }
    
    private func getLogoutButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [logOutButton.topAnchor.constraint(equalTo: contactUSLabel.bottomAnchor,constant: 10),
                                logOutButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                                logOutButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
                                logOutButton.heightAnchor.constraint(equalToConstant: 30)]
        return buttonContraints
    }
    
    
    private func getOrderDetailsTableViewLayoutConstraints() -> [NSLayoutConstraint]{
        let tableViewConstraints = [
            orderDetailsTableView.topAnchor.constraint(equalTo: logOutButton.bottomAnchor,constant: 10),
            orderDetailsTableView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            orderDetailsTableView.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10)
            ]
        return tableViewConstraints
    }
    
    
    
    private func getviewMoreOrdersButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [viewMoreOrdersButton.topAnchor.constraint(equalTo: orderDetailsTableView.bottomAnchor,constant: 10),
                                viewMoreOrdersButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                                viewMoreOrdersButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
                                viewMoreOrdersButton.heightAnchor.constraint(equalToConstant: 25)]
        return buttonContraints
    }
    
    @objc func didTapLogout(){
        delegate?.didTapLogout()
    }
    
    
    @objc func didTapViewMorePastOrders(){
        delegate?.didTapViewMorePastOrders()
    }
    
    
    @objc func didTapFavouriteRestaurants(){
        delegate?.didTapFavouriteRestaurants()
    }
    
    
    @objc func didTapManageAddresses(){
        delegate?.didTapManageAddresses()
    }
    
    
    @objc func didTapEditButton(){
        delegate?.didTapEditButton()
    }
    
    
    
    
    

    
}

class UserAccountsPageLoginView : UIView ,UserAccountsPageLoginViewProtocol{
    
    weak var delegate : UserAccountsPageLoginViewDelegate?
    
    private lazy var loginButton : UIButton = {
        let loginButton = UIButton()
        loginButton.setTitle("Login", for: .normal)
        loginButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        loginButton.setTitleColor(.white, for: .normal)
        loginButton.backgroundColor = .systemGreen
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.layer.cornerRadius = 5
        loginButton.contentMode = .left
        loginButton.addTarget(self, action: #selector(didTapLogin), for: .touchUpInside)
        return loginButton
    }()
    
    private lazy var loginOrCreateAccountLabel : UILabel = {
        let loginOrCreateAccountLabel = UILabel()
        loginOrCreateAccountLabel.textColor = .systemGray
        loginOrCreateAccountLabel.translatesAutoresizingMaskIntoConstraints = false
        loginOrCreateAccountLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        loginOrCreateAccountLabel.text = "Login/Create Account quickly in  few steps to manage orders and account"
        loginOrCreateAccountLabel.textAlignment = .center
        loginOrCreateAccountLabel.lineBreakMode = .byWordWrapping
        loginOrCreateAccountLabel.numberOfLines = 5
        loginOrCreateAccountLabel.sizeToFit()
        
        return loginOrCreateAccountLabel
    }()
    
    
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
    }
    
    private func initialiseViewElements(){
        self.addSubview(loginButton)
        self.addSubview(loginOrCreateAccountLabel)
        NSLayoutConstraint.activate(getLogInButtonContraints())
        NSLayoutConstraint.activate(getLoginOrCreateAccountLabelContraints())
    }
    
   
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
        private func getLogInButtonContraints() -> [NSLayoutConstraint]{
            let buttonContraints = [loginButton.topAnchor.constraint(equalTo: loginOrCreateAccountLabel.bottomAnchor,constant: 10),
                                    loginButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 40),
                                    loginButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -40),
                                    loginButton.heightAnchor.constraint(equalToConstant: 40)]
            return buttonContraints
        }
    
    
    
        private func getLoginOrCreateAccountLabelContraints() -> [NSLayoutConstraint]{
            let labelContraints = [loginOrCreateAccountLabel.topAnchor.constraint(equalTo: self.centerYAnchor ,constant: -100),
                                   loginOrCreateAccountLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                                   loginOrCreateAccountLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
                                   ]
            return labelContraints
        }
    
    @objc  func didTapLogin(){
        delegate?.didTapLogin()
    }
}

protocol UserAccountsPageViewDelegate : AnyObject {
     func didTapLogout()
    
    
    
     func didTapViewMorePastOrders()
    
    
    
     func didTapFavouriteRestaurants()
    
    
    
     func didTapManageAddresses()
    
    
    
     func didTapEditButton()
    
}


protocol UserAccountsPageLoginViewDelegate : AnyObject{
     func didTapLogin()
    
}
