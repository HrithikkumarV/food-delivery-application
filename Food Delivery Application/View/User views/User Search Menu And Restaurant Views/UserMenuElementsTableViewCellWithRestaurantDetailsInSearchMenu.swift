//
//  UserMenuSearchTableView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 27/02/22.
//

import UIKit




class UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenu : UserMenuDisplayTableViewCell ,UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenuProtocol{
    
    static let subCellIdentifier = "UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenu"
    
    
    weak var subDelegate: UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenuDelegate?{
        didSet {
            delegate = subDelegate
        }
    }
    
   
    
   
    
    private lazy var restaurantNameCellLabel : UILabel = {
        let restaurantNameCellLabel = UILabel()
        
        restaurantNameCellLabel.textColor = .black
        
        restaurantNameCellLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantNameCellLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        return restaurantNameCellLabel
    }()
    
    private lazy var restaurantStarRatingCellLabel : UILabel = {
        let  restaurantStarRatingCellLabel = UILabel()
        restaurantStarRatingCellLabel.textColor = .black
        restaurantStarRatingCellLabel.translatesAutoresizingMaskIntoConstraints = false
        return restaurantStarRatingCellLabel
    }()
    
    private lazy var starIconCellImageView : UIImageView = {
        let starIconCellImageView = UIImageView()
        starIconCellImageView.image = UIImage(systemName: "star.fill")?.withTintColor(.black , renderingMode: .alwaysOriginal)
        starIconCellImageView.translatesAutoresizingMaskIntoConstraints = false
        return starIconCellImageView
    }()
    
    private lazy var availableCellLabel : UILabel = {
        let  availableCellLabel = UILabel()
        availableCellLabel.translatesAutoresizingMaskIntoConstraints = false
        availableCellLabel.textAlignment = .center
        availableCellLabel.textColor = .black
        availableCellLabel.numberOfLines = 4
        availableCellLabel.lineBreakMode = .byWordWrapping
        return availableCellLabel
    }()
    
    
    private lazy var forwordArrowImageView : UIImageView = {
        let forwordArrowImageView = UIImageView(image: UIImage(systemName: "arrow.forward")?.withTintColor(.black, renderingMode: .alwaysOriginal))
        forwordArrowImageView.backgroundColor = .white
        forwordArrowImageView.translatesAutoresizingMaskIntoConstraints = false
        return forwordArrowImageView
    }()
    
    
    private lazy  var restaurantDetailsView : UIView = {
        let restaurantDetailsView = UIView()
        restaurantDetailsView.backgroundColor = .white
        restaurantDetailsView.isUserInteractionEnabled = true
        restaurantDetailsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenu.didTapRestaurantDetailsView(sender:))))
        return restaurantDetailsView
    }()
                                                   
                                                   
    
    
    
    private func updateRestaurantStatus(deliveryTiming : Int, isDeliverable : Bool , restaurantIsAvailable : Int , restaurantOpensNextAt : String){
        if(!isDeliverable){
            availableCellLabel.text = "Not Deliverable"
            
    
        }
        else if(restaurantIsAvailable == 1){
            availableCellLabel.text = "\(deliveryTiming) mins"
    
           
        }
        else{
            availableCellLabel.text = "CLOSED"
            
        }
    }
    
    private func getRestaurantNameCellLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [restaurantNameCellLabel.topAnchor.constraint(equalTo: restaurantDetailsView.topAnchor ,constant: 5),
            restaurantNameCellLabel.leftAnchor.constraint(equalTo: restaurantDetailsView.leftAnchor,constant: 10),
            restaurantNameCellLabel.rightAnchor.constraint(equalTo: forwordArrowImageView.leftAnchor),
            restaurantNameCellLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    
    private func getArrowForwardCellLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [forwordArrowImageView.centerYAnchor.constraint(equalTo: restaurantDetailsView.centerYAnchor),
            forwordArrowImageView.widthAnchor.constraint(equalToConstant: 30),
                               forwordArrowImageView.rightAnchor.constraint(equalTo: restaurantDetailsView.rightAnchor , constant: -10),
                               forwordArrowImageView.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    
    private func getStarIconCellImageViewContraints() -> [NSLayoutConstraint]{
        let imageContraints = [
            starIconCellImageView.topAnchor.constraint(equalTo: restaurantNameCellLabel.bottomAnchor,constant: 5),
            starIconCellImageView.leftAnchor.constraint(equalTo: restaurantDetailsView.leftAnchor,constant: 10),
            starIconCellImageView.widthAnchor.constraint(equalToConstant: 20),
            starIconCellImageView.heightAnchor.constraint(equalToConstant: 20)]
        return imageContraints
    }
    
    private func getRestaurantStarRatingCellLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            restaurantStarRatingCellLabel.topAnchor.constraint(equalTo: restaurantNameCellLabel.bottomAnchor,constant: 5),
            restaurantStarRatingCellLabel.leftAnchor.constraint(equalTo: starIconCellImageView.rightAnchor),
            restaurantStarRatingCellLabel.widthAnchor.constraint(equalToConstant: 40),
            restaurantStarRatingCellLabel.heightAnchor.constraint(equalToConstant: 20)]
        return labelContraints
    }
    
    
    
    private func getAvailablityCellLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
            availableCellLabel.topAnchor.constraint(equalTo: restaurantNameCellLabel.bottomAnchor,constant: 5),
            availableCellLabel.leftAnchor.constraint(equalTo: restaurantStarRatingCellLabel.rightAnchor,constant: 5),
            availableCellLabel.rightAnchor.constraint(equalTo: forwordArrowImageView.leftAnchor),
            availableCellLabel.heightAnchor.constraint(equalToConstant: 20)]
        return labelContraints
    }
    
    private func initialiseViewElements(){
        restaurantDetailsView.addSubview(restaurantNameCellLabel)
        restaurantDetailsView.addSubview(restaurantStarRatingCellLabel)
        restaurantDetailsView.addSubview(starIconCellImageView)
        restaurantDetailsView.addSubview(forwordArrowImageView)
        restaurantDetailsView.addSubview(availableCellLabel)
        NSLayoutConstraint.activate(getArrowForwardCellLabelContraints())
        NSLayoutConstraint.activate(getRestaurantNameCellLabelContraints())
        NSLayoutConstraint.activate(getStarIconCellImageViewContraints())
        NSLayoutConstraint.activate(getRestaurantStarRatingCellLabelContraints())
        NSLayoutConstraint.activate(getAvailablityCellLabelContraints())
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(restaurantDetailsView)
        initialiseViewElements()
    }
    
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        restaurantDetailsView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 60)
        menuTableViewCellContentView.frame = CGRect(x: 0, y: restaurantDetailsView.frame.maxY, width: self.frame.width, height: 120)
        
    }
    
    func configureContent(menuCellContents : (menuImage : Data? , menuName : String , menuDescription : String, menuPrice : Int , menuTarianType : String, menuAvailable : Int, menuAvailableNextAt  : String , menuId :Int) , restaurantDetails : RestaurantContentDetails){
        configureContent(menuCellContents: menuCellContents)
        restaurantDetailsView.tag = menuCellContents.menuId
        updateRestaurantStatus(deliveryTiming: Delivery.getDeaultDelieveryTimeInMins(), isDeliverable: restaurantDetails.isDeliverable,restaurantIsAvailable: restaurantDetails.restaurantIsAvailable, restaurantOpensNextAt: restaurantDetails.restaurantOpensNextAt)
        restaurantNameCellLabel.text = restaurantDetails.restaurantDetails.restaurantName
        restaurantStarRatingCellLabel.text = String(restaurantDetails.restaurantStarRating.prefix(upTo: restaurantDetails.restaurantStarRating.index(restaurantDetails.restaurantStarRating.startIndex, offsetBy: 3)))
    }
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        restaurantNameCellLabel.text = nil
        restaurantStarRatingCellLabel.text = nil
        availableCellLabel.text = nil
        restaurantDetailsView.tag = 0x0
    }
   
    
    @objc func didTapRestaurantDetailsView(sender : UITapGestureRecognizer){
        subDelegate?.didTapRestaurantDetailsView(sender: sender)
    }
    
}
                                                   
protocol UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenuDelegate : MenuTableViewCellDelegate{
            func didTapRestaurantDetailsView(sender : UITapGestureRecognizer)
        }



