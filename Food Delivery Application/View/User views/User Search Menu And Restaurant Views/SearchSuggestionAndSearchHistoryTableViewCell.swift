//
//  UserSearchSuggestionAndSearchHistoryView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 04/04/22.
//


import UIKit

enum SearchType{
    case searchHistoryItem
    case searchSuggetionItem
}

enum SearchItemType : String{
    case Menu
    case Restaurant
    case Search
}



class SearchSuggestionAndSearchHistoryTableViewCell : UITableViewCell{
    static let cellIdentifier = "SearchSuggestionAndSearchHistoryTableViewCell"
    
   
    var cellHeight : CGFloat = 44
    
    private lazy var searchSuggestionAndSearchHistoryTableViewCellContentView : UIView = {
        let  searchSuggestionAndSearchHistoryTableViewCellContentView = UIView()
        searchSuggestionAndSearchHistoryTableViewCellContentView.backgroundColor = .white
        return searchSuggestionAndSearchHistoryTableViewCellContentView
    }()
    
    private lazy var searchTypeCellImageView : UIImageView = {
        let  searchTypeCellImageView = UIImageView()
        searchTypeCellImageView.translatesAutoresizingMaskIntoConstraints = false
        return searchTypeCellImageView
    }()
    
    private lazy var searchSuggestionAndHistoryNameCellLabel : UILabel = {
        let  searchSuggestionAndHistoryNameCellLabel = UILabel()
        
        searchSuggestionAndHistoryNameCellLabel.textColor = .black
        
        searchSuggestionAndHistoryNameCellLabel.translatesAutoresizingMaskIntoConstraints = false
        searchSuggestionAndHistoryNameCellLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        
        searchSuggestionAndHistoryNameCellLabel.adjustsFontSizeToFitWidth = true
        searchSuggestionAndHistoryNameCellLabel.textAlignment = .left
        return searchSuggestionAndHistoryNameCellLabel
    }()
    
    private lazy var searchItemTypeCellLabel : UILabel = {
        let searchItemTypeCellLabel = UILabel()
        searchItemTypeCellLabel.textColor = .systemGray
        searchItemTypeCellLabel.translatesAutoresizingMaskIntoConstraints = false
        searchItemTypeCellLabel.textAlignment = .left
        
        searchItemTypeCellLabel.adjustsFontSizeToFitWidth = true
        return searchItemTypeCellLabel
    }()
   
    
    
    
    private func updateSearchTypeCellImageView(searchType : SearchType){
       
        if(searchType == .searchHistoryItem){
            searchTypeCellImageView.image = UIImage(systemName: "clock")?.withTintColor(.systemGray , renderingMode: .alwaysOriginal)
        }
        else{
            searchTypeCellImageView.image = UIImage(systemName: "magnifyingglass")?.withTintColor(.systemGray , renderingMode: .alwaysOriginal)
        }
        
        
    }
    
    
    
   
    private func getSearchSuggestionAndHistoryNameCellLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [searchSuggestionAndHistoryNameCellLabel.topAnchor.constraint(equalTo: searchSuggestionAndSearchHistoryTableViewCellContentView.topAnchor),
                               searchSuggestionAndHistoryNameCellLabel.leftAnchor.constraint(equalTo: searchTypeCellImageView.rightAnchor,constant: 10),
                               searchSuggestionAndHistoryNameCellLabel.rightAnchor.constraint(equalTo: searchSuggestionAndSearchHistoryTableViewCellContentView.rightAnchor , constant: -10),
                               searchSuggestionAndHistoryNameCellLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    
    private func getSearchTypeCellImageViewContraints() -> [NSLayoutConstraint]{
        let imageContraints = [
            searchTypeCellImageView.topAnchor.constraint(equalTo: searchSuggestionAndSearchHistoryTableViewCellContentView.topAnchor,constant: 10),
            searchTypeCellImageView.leftAnchor.constraint(equalTo: searchSuggestionAndSearchHistoryTableViewCellContentView.leftAnchor,constant: 10),
            searchTypeCellImageView.widthAnchor.constraint(equalToConstant: 30),
            searchTypeCellImageView.heightAnchor.constraint(equalToConstant: 30)]
        return imageContraints
    }
    
    private func getSearchItemTypeCellLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            searchItemTypeCellLabel.topAnchor.constraint(equalTo: searchSuggestionAndHistoryNameCellLabel.bottomAnchor),
            searchItemTypeCellLabel.leftAnchor.constraint(equalTo: searchTypeCellImageView.rightAnchor,constant: 10),
            searchItemTypeCellLabel.rightAnchor.constraint(equalTo:searchSuggestionAndSearchHistoryTableViewCellContentView.rightAnchor),
            searchItemTypeCellLabel.heightAnchor.constraint(equalToConstant: 20)]
        return labelContraints
    }
    
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(searchSuggestionAndSearchHistoryTableViewCellContentView)
        searchSuggestionAndSearchHistoryTableViewCellContentView.addSubview(searchSuggestionAndHistoryNameCellLabel)
        searchSuggestionAndSearchHistoryTableViewCellContentView.addSubview(searchTypeCellImageView)
        searchSuggestionAndSearchHistoryTableViewCellContentView.addSubview(searchItemTypeCellLabel)
    }
    
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        searchSuggestionAndSearchHistoryTableViewCellContentView.frame =
        CGRect(x: 0, y: 0, width: self.frame.size.width, height: cellHeight)
        NSLayoutConstraint.activate(getSearchItemTypeCellLabelContraints())
        NSLayoutConstraint.activate(getSearchTypeCellImageViewContraints())
        NSLayoutConstraint.activate(getSearchSuggestionAndHistoryNameCellLabelContraints())
    }
    
    func configureContent(seacrhSuggesionCellContents : SearchSuggessionModel){
        searchSuggestionAndHistoryNameCellLabel.text = seacrhSuggesionCellContents.searchItemName
        searchItemTypeCellLabel.text = seacrhSuggesionCellContents.searchItemType.rawValue
        updateSearchTypeCellImageView(searchType: seacrhSuggesionCellContents.searchType)
    }
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        searchSuggestionAndHistoryNameCellLabel.text = nil
        searchItemTypeCellLabel.text = nil
        searchTypeCellImageView.image = nil
    }
}



class SearchSuggestionTableHeaderView : UIView{
    
    weak var delegate : SearchSuggestionTableHeaderViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        initialiseViewElements()
    }
    
    private func initialiseViewElements(){
        self.addSubview(clearHistoryButton)
        self.addSubview(recentlySearchedLabel)
        NSLayoutConstraint.activate(getClearHistoryButtonAndRecentlySearchedLabelConstraints())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var  clearHistoryButton  : UIButton = {
        let clearHistoryButton = UIButton()
        clearHistoryButton.backgroundColor = .white
        clearHistoryButton.layer.cornerRadius = 10
        clearHistoryButton.layer.borderWidth = 1
        clearHistoryButton.layer.borderColor = UIColor.lightGray.cgColor
        clearHistoryButton.setTitle("Clear All", for: .normal)
        clearHistoryButton.setTitleColor(.systemRed.withAlphaComponent(0.7), for: .normal)
        clearHistoryButton.translatesAutoresizingMaskIntoConstraints = false
        clearHistoryButton.addTarget(self, action: #selector(didTapClearHistory), for: .touchUpInside)
        return clearHistoryButton
    }()
    
    private lazy var recentlySearchedLabel : UILabel = {
        let recentlySearchedLabel = UILabel()
        recentlySearchedLabel.textColor = .black
        recentlySearchedLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        recentlySearchedLabel.translatesAutoresizingMaskIntoConstraints = false
        recentlySearchedLabel.textAlignment = .left
        recentlySearchedLabel.text = "Recently Searched"
        recentlySearchedLabel.adjustsFontSizeToFitWidth = true
        return recentlySearchedLabel
    }()
    
    
    
    private func getClearHistoryButtonAndRecentlySearchedLabelConstraints() -> [NSLayoutConstraint]{
        let constraints =  [clearHistoryButton.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
                            clearHistoryButton.topAnchor.constraint(equalTo: self.topAnchor,constant: 5),
                            clearHistoryButton.widthAnchor.constraint(equalToConstant: 80),
                            clearHistoryButton.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -5),
                            recentlySearchedLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                            recentlySearchedLabel.rightAnchor.constraint(equalTo: clearHistoryButton.leftAnchor,constant: -5),recentlySearchedLabel.topAnchor.constraint(equalTo: self.topAnchor),recentlySearchedLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ]
        return constraints
    }
    
    @objc func didTapClearHistory(){
        delegate?.didTapClearHistory()
    }
    
    
   
}

protocol SearchSuggestionTableHeaderViewDelegate :AnyObject{
    func didTapClearHistory()
}
