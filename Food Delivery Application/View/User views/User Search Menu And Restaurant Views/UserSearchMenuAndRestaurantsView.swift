//
//  UserSearchMenuAndRestaurantsView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 23/02/22.
//

import UIKit

class UserSearchMenuAndRestaurantsView : UIView ,UserSearchMenuAndRestaurantViewProtocol{
    
    weak var delegate  : UserSearchMenuAndRestaurantsViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
        self.bringSubviewToFront(dishButton)
        self.bringSubviewToFront(restaurantButton)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func initialiseViewElements(){
        
        self.addSubview(dishButton)
        self.addSubview(restaurantButton)
        self.addSubview(noMatchFoundLabel)
        self.addSubview(browseRestaurantsButton)
        self.addSubview(tableView)
        NSLayoutConstraint.activate(getBrowseRestaurantsButtonLayoutConstraints())
        NSLayoutConstraint.activate(getNoMatchFoundLabelLayoutConstraints())
        NSLayoutConstraint.activate(getRestaurantButtonConstraints())
        NSLayoutConstraint.activate(getDishesButtonConstraints())
        NSLayoutConstraint.activate(getTableViewContraints())
    }
    
    
   
    lazy var restaurantButton : UIButton = {
        let restaurantButton = UIButton()
        restaurantButton.backgroundColor = .white
        restaurantButton.layer.cornerRadius = 10
        restaurantButton.layer.borderWidth = 1
        restaurantButton.layer.borderColor = UIColor.lightGray.cgColor
        restaurantButton.setTitle("Restaurant", for: .normal)
        restaurantButton.setTitleColor(.black, for: .normal)
        restaurantButton.translatesAutoresizingMaskIntoConstraints = false
        restaurantButton.addTarget(self, action: #selector(didTapRestaurantButton), for: .touchUpInside)
        return restaurantButton
    }()
    
    lazy var dishButton : UIButton = {
        let dishButton = UIButton()
        dishButton.backgroundColor = .white
        dishButton.layer.cornerRadius = 10
        dishButton.layer.borderWidth = 1
        dishButton.layer.borderColor = UIColor.lightGray.cgColor
        dishButton.setTitle("Dishes", for: .normal)
        dishButton.setTitleColor(.black, for: .normal)
        dishButton.translatesAutoresizingMaskIntoConstraints = false
        dishButton.addTarget(self, action: #selector(didTapDishButton), for: .touchUpInside)
        return dishButton
    }()
    
    var noMatchFoundLabel : UILabel = {
        let noMatchFoundLabel = UILabel()
        noMatchFoundLabel.translatesAutoresizingMaskIntoConstraints = false
        noMatchFoundLabel.text = "We couldn't find what you are looking for in Your locality. Browse Restaurants and Menu in your locality"
        noMatchFoundLabel.backgroundColor = .white
        noMatchFoundLabel.textColor = .systemGray
        noMatchFoundLabel.numberOfLines = 5
        noMatchFoundLabel.lineBreakMode = .byWordWrapping
        noMatchFoundLabel.sizeToFit()
        noMatchFoundLabel.layer.cornerRadius = 5
        noMatchFoundLabel.textAlignment = .center
        noMatchFoundLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return noMatchFoundLabel
    }()
    lazy var browseRestaurantsButton : UIButton = {
        let browseRestaurantsButton = UIButton()
        browseRestaurantsButton.translatesAutoresizingMaskIntoConstraints = false
        browseRestaurantsButton.backgroundColor = .systemGreen
        browseRestaurantsButton.setTitle("Browse Restaurants", for: .normal)
        browseRestaurantsButton.setTitleColor(.white, for: .normal)
        browseRestaurantsButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        browseRestaurantsButton.titleLabel?.adjustsFontSizeToFitWidth = true
        browseRestaurantsButton.layer.cornerRadius =  5
        browseRestaurantsButton.addTarget(self, action: #selector(didTapBrowseRestaurant), for: .touchUpInside)
        return browseRestaurantsButton
    }()
   
    
    
    func createSearchSuggestionTableViewCell(indexPath : IndexPath ,seacrhSuggesionCellContents : SearchSuggessionModel) -> UITableViewCell{
        let searchSuggestionAndSearchHistoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: SearchSuggestionAndSearchHistoryTableViewCell.cellIdentifier, for: indexPath) as? SearchSuggestionAndSearchHistoryTableViewCell
        searchSuggestionAndSearchHistoryTableViewCell?.cellHeight = 50
        searchSuggestionAndSearchHistoryTableViewCell?.configureContent(seacrhSuggesionCellContents: seacrhSuggesionCellContents)
        
        return searchSuggestionAndSearchHistoryTableViewCell ?? UITableViewCell()
    }
    
    
    func getsearchSuggestionAndSearchHistoryTableViewCellHeight() -> CGFloat {
        50
    }
   
    lazy var searchSuggestionAndSearchHistoryTableViewHeaderView : SearchSuggestionTableHeaderView = {
        searchSuggestionAndSearchHistoryTableViewHeaderView = SearchSuggestionTableHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 30))
        return searchSuggestionAndSearchHistoryTableViewHeaderView
    }()
    
    

    func getMenuTabelViewCellHeight() -> CGFloat {
        return 180
    }
    
    
    func createMenuTableViewCell(indexPath : IndexPath , menuCellContents : (menuImage : Data? , menuName : String , menuDescription : String, menuPrice : Int , menuTarianType : String, menuAvailable : Int, menuAvailableNextAt  : String , menuId :Int) , restaurantDetails : RestaurantContentDetails) -> UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenu{
        let menuTableViewCell = tableView.dequeueReusableCell(withIdentifier: UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenu.subCellIdentifier, for: indexPath) as? UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenu
        menuTableViewCell?.cellHeight = 180
        menuTableViewCell?.configureContent(menuCellContents: menuCellContents, restaurantDetails: restaurantDetails)
        return menuTableViewCell ?? UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenu()
    }
    
    lazy var tableView : UITableView = {
       let tableView = UITableView()
        tableView.bounces = true
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.register(RestaurantDisplayTableViewCell.self, forCellReuseIdentifier: RestaurantDisplayTableViewCell.cellIdentifier)
        tableView.register(UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenu.self, forCellReuseIdentifier: UserMenuElementsTableViewCellWithRestaurantDetailsInSearchMenu.subCellIdentifier)
        tableView.register(SearchSuggestionAndSearchHistoryTableViewCell.self, forCellReuseIdentifier: SearchSuggestionAndSearchHistoryTableViewCell.cellIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
       return tableView
    }()
   
    func getRestaurantTableViewCellHeight() -> CGFloat{
        return 130
    }
    
    func createRestaurantTableViewCell(indexPath : IndexPath ,restaurentCellContents : (restaurantProfileImage : Data , restaurantName : String , restaurantStarRating : String , restaurantCuisine : String , restaurantLocality : String , deliveryTiming : Int , restaurantOpensNextAt  : String,restaurantIsAvailable : Int,isDeliverable : Bool)) -> UITableViewCell{
        let restaurantTableViewCell = tableView.dequeueReusableCell(withIdentifier: RestaurantDisplayTableViewCell.cellIdentifier, for: indexPath) as? RestaurantDisplayTableViewCell
        restaurantTableViewCell?.cellHeight = 120
        restaurantTableViewCell?.configureContent(restaurentCellContents: restaurentCellContents)
        return restaurantTableViewCell ?? UITableViewCell()
    }
    private func getRestaurantButtonConstraints() -> [NSLayoutConstraint]{
        let textViewConstraints = [restaurantButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),restaurantButton.rightAnchor.constraint(equalTo: self.centerXAnchor, constant: -10),restaurantButton.topAnchor.constraint(equalTo: self.topAnchor),restaurantButton.heightAnchor.constraint(equalToConstant: 40)]
        return textViewConstraints
    }
    
    private func getDishesButtonConstraints() -> [NSLayoutConstraint]{
        let textViewConstraints = [dishButton.leftAnchor.constraint(equalTo: self.centerXAnchor,constant: 10),dishButton.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),dishButton.topAnchor.constraint(equalTo : self.topAnchor),dishButton.heightAnchor.constraint(equalToConstant: 40)]
        return textViewConstraints
    }
    
    private func getNoMatchFoundLabelLayoutConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [
            noMatchFoundLabel.topAnchor.constraint(equalTo: self.centerYAnchor,constant: -100),
            noMatchFoundLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            noMatchFoundLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10)]
        return buttonContraints
    }
    
    
    private func getBrowseRestaurantsButtonLayoutConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [
            browseRestaurantsButton.topAnchor.constraint(equalTo: noMatchFoundLabel.bottomAnchor,constant: 10),
            browseRestaurantsButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 40),
            browseRestaurantsButton.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -40),
            browseRestaurantsButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    
   
    
    private func getTableViewContraints() -> [NSLayoutConstraint]{
        let tableViewContraints = [tableView.topAnchor.constraint(equalTo: self.topAnchor
                                                                 ),
                                   tableView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                                   tableView.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
                                   tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor)]
        return tableViewContraints
    }
    
    @objc func didTapDishButton(){
        delegate?.didTapDishButton()
    }
    
    @objc func didTapRestaurantButton(){
        delegate?.didTapRestaurantButton()
    }
    
    @objc  func didTapBrowseRestaurant(){
        delegate?.didTapBrowseRestaurant()
    }
}


protocol UserSearchMenuAndRestaurantsViewDelegate : AnyObject{
    func didTapDishButton()
    
    func didTapRestaurantButton()
    
    func didTapBrowseRestaurant()
}
