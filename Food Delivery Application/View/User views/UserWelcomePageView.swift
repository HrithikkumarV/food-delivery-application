//
//  WelcomePageView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 30/01/22.
//

import Foundation
import UIKit

class UserWelcomePageView : UIView, UserWelcomePageViewProtocol{
    
    
    
    weak var delegate : UserWelcomePageViewDelegate?
    
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
    }
    
    private func initialiseViewElements(){
        self.addSubview(setDeliveryAddressButton)
        self.addSubview(haveAnAccountLabel)
        self.addSubview(loginButton)
        self.addSubview(readyToOrderFromYourFavouriteRestaurantsLabel)
        self.addSubview(userWelcomeMessageLabel)
        self.addSubview(changeAccountButton)
        self.addSubview(appWelcomeMessageLabel)
        NSLayoutConstraint.activate(getReadyToOrderFromYourFavouriteRestaurantsLabelLayoutConstraints())
        NSLayoutConstraint.activate(getSetDeliveryAddressButtonLayoutContrainsts())
        NSLayoutConstraint.activate(getHaveAnAccountLabelLayoutContrainsts())
        NSLayoutConstraint.activate(getLoginButtonLayoutConstraints())
        NSLayoutConstraint.activate(getChangeAccountButtonLayoutContraints())
        NSLayoutConstraint.activate(getUserWelcomeMessageLabelContraints())
        NSLayoutConstraint.activate(getAppWelcomeMessageLabelLayoutConstrainsts())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var setDeliveryAddressButton : UIButton = {
        let setDeliveryAddressButton = UIButton()
        setDeliveryAddressButton.translatesAutoresizingMaskIntoConstraints = false
        setDeliveryAddressButton.setTitle("Set Delivery Address", for: .normal)
        setDeliveryAddressButton.setTitleColor(.white, for: .normal)
        setDeliveryAddressButton.backgroundColor = .systemBlue
        setDeliveryAddressButton.layer.borderWidth = 1
        setDeliveryAddressButton.layer.borderColor = UIColor.white.cgColor
        setDeliveryAddressButton.layer.cornerRadius = 5
        setDeliveryAddressButton.dropShadow()
        setDeliveryAddressButton.addTarget(self, action: #selector(didTapSetDeliveryAddress), for: .touchUpInside)
        return setDeliveryAddressButton
    }()
    var haveAnAccountLabel : UILabel = {
        let  haveAnAccountLabel = UILabel()
        haveAnAccountLabel.translatesAutoresizingMaskIntoConstraints = false
        haveAnAccountLabel.text = "Have an account?"
        haveAnAccountLabel.backgroundColor = .white
        haveAnAccountLabel.textColor = .systemGray
        haveAnAccountLabel.font = UIFont(name:"ArialRoundedMTBold", size: 14.0)
        haveAnAccountLabel.sizeToFit()
        return haveAnAccountLabel
    }()
    lazy var loginButton : UIButton = {
        let loginButton = UIButton()
        loginButton.setTitle("Login", for: .normal)
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.setTitleColor(.systemBlue, for: .normal)
        loginButton.backgroundColor = .white
        loginButton.titleLabel?.font = UIFont(name:"ArialRoundedMTBold", size: 16.0)
        loginButton.sizeToFit()
        loginButton.addTarget(self, action: #selector(didTapLogin), for: .touchUpInside)
        return loginButton
    }()
    var readyToOrderFromYourFavouriteRestaurantsLabel : UILabel = {
        let readyToOrderFromYourFavouriteRestaurantsLabel = UILabel()
        readyToOrderFromYourFavouriteRestaurantsLabel.translatesAutoresizingMaskIntoConstraints =  false
        readyToOrderFromYourFavouriteRestaurantsLabel.text = "Ready to order from your Favourite Restaurants ?"
        readyToOrderFromYourFavouriteRestaurantsLabel.textColor = .systemGray
        readyToOrderFromYourFavouriteRestaurantsLabel.backgroundColor = .white
        readyToOrderFromYourFavouriteRestaurantsLabel.adjustsFontSizeToFitWidth = true
        readyToOrderFromYourFavouriteRestaurantsLabel.sizeToFit()
        readyToOrderFromYourFavouriteRestaurantsLabel.font = UIFont(name:"ArialRoundedMTBold", size: 14.0)
        return readyToOrderFromYourFavouriteRestaurantsLabel
    }()
    var appWelcomeMessageLabel : UILabel = {
        let appWelcomeMessageLabel = UILabel()
        appWelcomeMessageLabel.translatesAutoresizingMaskIntoConstraints =  false
        appWelcomeMessageLabel.text = "Order your favourite foods from a wide range of restaurants in your locality delivered quickly to your doorstep"
        appWelcomeMessageLabel.font = UIFont(name:"ArialRoundedMTBold", size: 35.0)
        appWelcomeMessageLabel.textColor = .black
        appWelcomeMessageLabel.backgroundColor = .white
        appWelcomeMessageLabel.lineBreakMode = .byWordWrapping
        appWelcomeMessageLabel.numberOfLines = 5
        appWelcomeMessageLabel.textAlignment = .center
        appWelcomeMessageLabel.sizeToFit()
        return appWelcomeMessageLabel
    }()
    var userWelcomeMessageLabel : UILabel = {
        let  userWelcomeMessageLabel = UILabel()
        userWelcomeMessageLabel.translatesAutoresizingMaskIntoConstraints =  false
        userWelcomeMessageLabel.font = UIFont(name:"ArialRoundedMTBold", size: 30.0)
        userWelcomeMessageLabel.textColor = .darkGray
        userWelcomeMessageLabel.backgroundColor = .white
        userWelcomeMessageLabel.isHidden = true
        userWelcomeMessageLabel.sizeToFit()
        return userWelcomeMessageLabel
    }()
    lazy var changeAccountButton : UIButton = {
        let changeAccountButton = UIButton()
        changeAccountButton.translatesAutoresizingMaskIntoConstraints = false
        changeAccountButton.setTitle("Change account", for: .normal)
        changeAccountButton.titleLabel?.font = UIFont(name:"ArialRoundedMTBold", size: 16.0)
        changeAccountButton.setTitleColor(.systemBlue, for: .normal)
        changeAccountButton.backgroundColor = .white
        changeAccountButton.isHidden = true
        changeAccountButton.titleLabel?.adjustsFontSizeToFitWidth = true
        changeAccountButton.addTarget(self, action:#selector(didTapChangeAccount), for: .touchUpInside)
        return changeAccountButton
    }()
    
    
    
    
    
        
        
        
    
    
    private func getAppWelcomeMessageLabelLayoutConstrainsts() -> [NSLayoutConstraint]{
        let labelConstrainst = [appWelcomeMessageLabel.topAnchor.constraint(equalTo: self.topAnchor ,constant: UIScreen.main.bounds.width/3),
                                appWelcomeMessageLabel.leftAnchor.constraint(equalTo: self.leftAnchor ,constant: 40),
                                appWelcomeMessageLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -40),
                                appWelcomeMessageLabel.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height/3)]
        return labelConstrainst
    }
    
    private func getSetDeliveryAddressButtonLayoutContrainsts() -> [NSLayoutConstraint]{
        let labelConstrainst = [setDeliveryAddressButton.topAnchor.constraint(equalTo: readyToOrderFromYourFavouriteRestaurantsLabel.bottomAnchor , constant: 10),
                                setDeliveryAddressButton.leftAnchor.constraint(equalTo: self.leftAnchor ,constant: 40),
                                setDeliveryAddressButton.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -40),
                                setDeliveryAddressButton.heightAnchor.constraint(equalToConstant: 50)]
        return labelConstrainst
    }
    
    private func getReadyToOrderFromYourFavouriteRestaurantsLabelLayoutConstraints() -> [NSLayoutConstraint]{
        let labelConstrainst = [readyToOrderFromYourFavouriteRestaurantsLabel.topAnchor.constraint(equalTo: appWelcomeMessageLabel.bottomAnchor , constant: 100),
                                readyToOrderFromYourFavouriteRestaurantsLabel.leftAnchor.constraint(equalTo: self.leftAnchor ,constant: 40),
                                readyToOrderFromYourFavouriteRestaurantsLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -40),
                                readyToOrderFromYourFavouriteRestaurantsLabel.heightAnchor.constraint(equalToConstant: 20)]
        return labelConstrainst
    }
    
    private func getHaveAnAccountLabelLayoutContrainsts() -> [NSLayoutConstraint]{
        let labelConstrainst = [haveAnAccountLabel.topAnchor.constraint(equalTo: setDeliveryAddressButton.bottomAnchor , constant: 10),
                                haveAnAccountLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor , constant: -30),
                                haveAnAccountLabel.heightAnchor.constraint(equalToConstant: 20)]
        return labelConstrainst
    }
    
    private func getLoginButtonLayoutConstraints() -> [NSLayoutConstraint]{
        let labelConstrainst = [loginButton.topAnchor.constraint(equalTo: setDeliveryAddressButton.bottomAnchor , constant: 10),
                                loginButton.leftAnchor.constraint(equalTo: haveAnAccountLabel.rightAnchor ,constant: 5),
                                loginButton.heightAnchor.constraint(equalToConstant: 20)]
        return labelConstrainst
    }
    
    private func getChangeAccountButtonLayoutContraints() -> [NSLayoutConstraint]{
        let labelConstrainst = [changeAccountButton.topAnchor.constraint(equalTo: setDeliveryAddressButton.bottomAnchor , constant: 10),
                                changeAccountButton.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                                changeAccountButton.heightAnchor.constraint(equalToConstant: 20)]
        return labelConstrainst
    }
    
    private func getUserWelcomeMessageLabelContraints() -> [NSLayoutConstraint]{
        let labelConstrainst = [userWelcomeMessageLabel.topAnchor.constraint(equalTo: appWelcomeMessageLabel.bottomAnchor , constant: 10),
                                userWelcomeMessageLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                                userWelcomeMessageLabel.heightAnchor.constraint(equalToConstant: 40)]
        return labelConstrainst
    }
    @objc func didTapSetDeliveryAddress(){
        delegate?.didTapSetDeliveryAddress()
    }

    @objc func didTapLogin(){
        delegate?.didTapLogin()
    }

    @objc func didTapChangeAccount(){
        delegate?.didTapChangeAccount()
    }
    
}


protocol UserWelcomePageViewDelegate :AnyObject{
     func didTapSetDeliveryAddress()

     func didTapLogin()

     func didTapChangeAccount()
}
