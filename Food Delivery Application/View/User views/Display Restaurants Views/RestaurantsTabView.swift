//
//  RestaurantsTabView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 18/01/22.
//

import Foundation
import UIKit

class RestaurantsTabView : UIView ,RestaurantsTabViewProtocol {
    
    weak var delegate : RestaurantsTabViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func initialiseViewElements(){
        self.addSubview(restaurantsTableView)
        self.addSubview(changeLocationButton)
        self.addSubview(noRestaurantsAvailableLabel)
        NSLayoutConstraint.activate(getChangeLocationButtonContraints())
        NSLayoutConstraint.activate(getRestaurantsTableViewContraints())
        NSLayoutConstraint.activate(getNoRestaurantsAvailableLabelContraints())
    }
    
    
     var noRestaurantsAvailableLabel : UILabel = {
        let noRestaurantsAvailableLabel = UILabel()
        noRestaurantsAvailableLabel.textColor = .systemGray
        noRestaurantsAvailableLabel.translatesAutoresizingMaskIntoConstraints = false
        noRestaurantsAvailableLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        noRestaurantsAvailableLabel.text = "Sorry,we couldn't serve you in this location at this moment, Please try different location"
        noRestaurantsAvailableLabel.textAlignment = .center
        noRestaurantsAvailableLabel.lineBreakMode = .byWordWrapping
        noRestaurantsAvailableLabel.numberOfLines = 5
        noRestaurantsAvailableLabel.sizeToFit()
        return noRestaurantsAvailableLabel
    }()
    
     lazy var changeLocationButton : UIButton = {
        let changeLocationButton = UIButton()
        changeLocationButton.setTitle("Change Location", for: .normal)
        changeLocationButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        changeLocationButton.setTitleColor(.white, for: .normal)
        changeLocationButton.backgroundColor = .systemGreen
        changeLocationButton.translatesAutoresizingMaskIntoConstraints = false
        changeLocationButton.layer.cornerRadius = 5
         changeLocationButton.addTarget(self, action: #selector(didTapLocalityLabel), for: .touchUpInside)
        return changeLocationButton
    }()
   
    
    lazy var restaurantsTableView : UITableView = {
       let restaurantsTableView = UITableView()
        restaurantsTableView.bounces = false
        restaurantsTableView.separatorStyle = .singleLine
        restaurantsTableView.backgroundColor = .white
        restaurantsTableView.register(RestaurantDisplayTableViewCell.self, forCellReuseIdentifier: RestaurantDisplayTableViewCell.cellIdentifier)
        restaurantsTableView.translatesAutoresizingMaskIntoConstraints = false
       return restaurantsTableView
    }()
   
    func getRestaurantTableViewCellHeight() -> CGFloat{
        return 120
    }
    
    func createRestaurantTableViewCell(indexPath : IndexPath ,restaurentCellContents : (restaurantProfileImage : Data , restaurantName : String , restaurantStarRating : String , restaurantCuisine : String , restaurantLocality : String , deliveryTiming : Int , restaurantOpensNextAt  : String,restaurantIsAvailable : Int,isDeliverable : Bool)) -> UITableViewCell{
        let restaurantTableViewCell = restaurantsTableView.dequeueReusableCell(withIdentifier: RestaurantDisplayTableViewCell.cellIdentifier, for: indexPath) as? RestaurantDisplayTableViewCell
        restaurantTableViewCell?.cellHeight = 120
        restaurantTableViewCell?.configureContent(restaurentCellContents: restaurentCellContents)
        return restaurantTableViewCell ?? UITableViewCell()
    }
    
    
   
    
    
    
    private func getRestaurantsTableViewContraints() -> [NSLayoutConstraint]{
        let tableViewContraints = [restaurantsTableView.topAnchor.constraint(equalTo: self.topAnchor),
            restaurantsTableView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
                                   restaurantsTableView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10),
                                   restaurantsTableView.bottomAnchor.constraint(equalTo : self.bottomAnchor)]
        return tableViewContraints
    }
    
    
    
    
    
    private func getChangeLocationButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [changeLocationButton.topAnchor.constraint(equalTo: noRestaurantsAvailableLabel.bottomAnchor ,constant: 10),
                                changeLocationButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 40),
                                changeLocationButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -40),
                                changeLocationButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
   
    
    private func getNoRestaurantsAvailableLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [noRestaurantsAvailableLabel.topAnchor.constraint(equalTo: self.centerYAnchor ,constant: -100),
                               noRestaurantsAvailableLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                               noRestaurantsAvailableLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
                               ]
        return labelContraints
    }
    
    @objc func didTapLocalityLabel(){
        delegate?.didTapLocalityLabel()
    }
    
    
}

protocol RestaurantsTabViewDelegate : AnyObject{
    func didTapLocalityLabel()
}


class RestaurantsTabNavigationBarTitleView : UIView, RestaurantsTabNavigationBarTitleViewProtocol{
    
    weak var delegate : RestaurantsTapNavigationBarTitleViewDelegate?
    
    init(){
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44)
        self.backgroundColor = .white
        let longPress  = UILongPressGestureRecognizer(target: self, action: #selector(
            onLongPress))
        longPress.minimumPressDuration = 0
        self.addGestureRecognizer(longPress)
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func initialiseViewElements(){
        self.addSubview(addressLabel)
        self.addSubview(addressTagLabel)
        self.addSubview(locationIcon)
        NSLayoutConstraint.activate(getAddressLabelContraints())
        NSLayoutConstraint.activate(getAddressTagLabelContraints())
        NSLayoutConstraint.activate(getLocationIconContraints())
    }
    private var addressTagLabel : UILabel = {
        let addressTagLabel = UILabel()
        addressTagLabel.textColor = .black.withAlphaComponent(0.8)
        addressTagLabel.font = UIFont.systemFont(ofSize: 20.0)
        addressTagLabel.backgroundColor = .clear
        addressTagLabel.translatesAutoresizingMaskIntoConstraints = false
        return addressTagLabel
    }()
    private var addressLabel : UILabel = {
        let  addressLabel = UILabel()
        addressLabel.textColor = .black.withAlphaComponent(0.5)
        addressLabel.translatesAutoresizingMaskIntoConstraints = false
        addressLabel.backgroundColor = .clear
        return addressLabel
        
    }()
    private var locationIcon : UIImageView = {
        let locationIcon = UIImageView()
        locationIcon.image = UIImage(systemName: "location.fill")?.withTintColor(.blue, renderingMode: .alwaysOriginal)
        locationIcon.translatesAutoresizingMaskIntoConstraints = false
        return locationIcon
    }()
    
    func updateAddressTagAndAddressLabel(addressTag : String , address : AddressDetails){
        addressTagLabel.text = addressTag
        addressLabel.text = "\(address.doorNoAndBuildingNameAndBuildingNo), \(address.streetName), \(address.landmark), \(address.localityName), \(address.city), \(address.state), \(address.country), \(address.pincode)" + String(repeating: " ", count: 50)
    }
    
   
    private func getLocationIconContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            locationIcon.leftAnchor.constraint(equalTo: self.leftAnchor),
            locationIcon.widthAnchor.constraint(equalToConstant: 20),
            locationIcon.heightAnchor.constraint(equalToConstant: self.frame.height/2)]
        return labelContraints
    }
    
    private func getAddressTagLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            addressTagLabel.leftAnchor.constraint(equalTo: locationIcon.rightAnchor),
            addressTagLabel.rightAnchor.constraint(equalTo: self.rightAnchor),
            addressTagLabel.heightAnchor.constraint(equalToConstant: self.frame.height/2)]
        return labelContraints
    }
    
    private func getAddressLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
            addressLabel.topAnchor.constraint(equalTo: addressTagLabel.bottomAnchor),
            addressLabel.leftAnchor.constraint(equalTo: locationIcon.rightAnchor),
            addressLabel.rightAnchor.constraint(equalTo: self.rightAnchor),
            addressLabel.heightAnchor.constraint(equalToConstant: self.frame.height/2)]
        return labelContraints
    }
    @objc
    func onLongPress(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            animateNavTitle()
        }
        if gesture.state == .ended {
            endNavAnimate()
        }
    }
    
    func animateNavTitle() {
        UIView.animate(withDuration: 0.3, animations: {
            self.backgroundColor = .lightGray
        })
    }
    
    func endNavAnimate() {
        UIView.animate(withDuration: 0.3, animations: {
            self.backgroundColor = .white
        }, completion:  {
            _ in
            self.didTapLocalityLabel()
        })
    }
    
    func didTapLocalityLabel(){
        delegate?.didTapLocalityLabel()
    }
}


protocol RestaurantsTapNavigationBarTitleViewDelegate : AnyObject{
     func didTapLocalityLabel()
}
