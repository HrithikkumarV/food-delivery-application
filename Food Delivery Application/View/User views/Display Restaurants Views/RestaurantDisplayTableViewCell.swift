//
//  RestaurantsElementTableView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 24/02/22.
//

import Foundation
import UIKit

class RestaurantDisplayTableViewCell : UITableViewCell{
    static let cellIdentifier = "RestaurantDisplayTableViewCell"
    
    var cellHeight : CGFloat = 44
    
    private lazy var restaurantNameCellLabel : UILabel = {
       let restaurantNameCellLabel = UILabel()
        restaurantNameCellLabel.textColor = .black
        restaurantNameCellLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantNameCellLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        restaurantNameCellLabel.adjustsFontSizeToFitWidth = true
        return restaurantNameCellLabel
    }()
    
    private lazy var restaurantCuisineCellLabel : UILabel = {
       let restaurantCuisineCellLabel = UILabel()
        restaurantCuisineCellLabel.textColor = .black
        restaurantCuisineCellLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantCuisineCellLabel.adjustsFontSizeToFitWidth = true
        return restaurantCuisineCellLabel
    }()
    private lazy var restaurantStarRatingCellLabel : UILabel = {
        let  restaurantStarRatingCellLabel = UILabel()
        restaurantStarRatingCellLabel.textColor = .black
        restaurantStarRatingCellLabel.translatesAutoresizingMaskIntoConstraints = false
        
        restaurantStarRatingCellLabel.adjustsFontSizeToFitWidth = true
        return restaurantStarRatingCellLabel
    }()
    private lazy var starIconCellImageView : UIImageView = {
        let  starIconCellImageView = UIImageView()
        starIconCellImageView.image = UIImage(systemName: "star.fill")?.withTintColor(.black , renderingMode: .alwaysOriginal)
        starIconCellImageView.translatesAutoresizingMaskIntoConstraints = false
        return starIconCellImageView
    }()
    private lazy var availableCellLabel : UILabel = {
        let availableCellLabel = UILabel()
        availableCellLabel.translatesAutoresizingMaskIntoConstraints = false
        availableCellLabel.textAlignment = .center
        availableCellLabel.textColor = .black
        availableCellLabel.adjustsFontSizeToFitWidth = true
        return availableCellLabel
    }()
    private lazy var restaurantLocalityCellLabel : UILabel = {
        let  restaurantLocalityCellLabel = UILabel()
        restaurantLocalityCellLabel.textColor = .black
        restaurantLocalityCellLabel.translatesAutoresizingMaskIntoConstraints = false
        return restaurantLocalityCellLabel
    }()
    private var restaurantProfileImageCellImageView : UIImageView  = {
        let restaurantProfileImageCellImageView = UIImageView()
        restaurantProfileImageCellImageView.translatesAutoresizingMaskIntoConstraints = false
        
        restaurantProfileImageCellImageView.isUserInteractionEnabled = false
        restaurantProfileImageCellImageView.backgroundColor = .white
        restaurantProfileImageCellImageView.layer.cornerRadius = 10
        restaurantProfileImageCellImageView.contentMode = .scaleAspectFill
        restaurantProfileImageCellImageView.clipsToBounds = true
        return restaurantProfileImageCellImageView
    }()
    
    private func updateRestaurantStatus(deliveryTiming : Int, isDeliverable : Bool , restaurantIsAvailable : Int , restaurantOpensNextAt : String){
        if(!isDeliverable){
            availableCellLabel.text = "Not Deliverable"
            
        }
        else if(restaurantIsAvailable == 1){
            availableCellLabel.text = "\(deliveryTiming) mins"
           
        }
        else{
            availableCellLabel.text = "CLOSED"
            
        }
    }
    
    private lazy var restaurantTableViewCellContentView : UIView = {
        let  restaurantTableViewCellContentView = UIView()
        restaurantTableViewCellContentView.backgroundColor = .white
        return restaurantTableViewCellContentView
    }()
   
    
    private func getRestaurantProfileImageCellImageViewContraints() -> [NSLayoutConstraint]{
        let imageContraints = [
            restaurantProfileImageCellImageView.topAnchor.constraint(equalTo: restaurantTableViewCellContentView.topAnchor , constant: 10),
            restaurantProfileImageCellImageView.leftAnchor.constraint(equalTo: restaurantTableViewCellContentView.leftAnchor , constant: 10),
            restaurantProfileImageCellImageView.widthAnchor.constraint(equalToConstant: 100),
            restaurantProfileImageCellImageView.heightAnchor.constraint(equalToConstant: 100)]
        return imageContraints
    }
    
    
    private func getRestaurantNameCellLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [restaurantNameCellLabel.topAnchor.constraint(equalTo: restaurantTableViewCellContentView.topAnchor ,constant: 10),
            restaurantNameCellLabel.leftAnchor.constraint(equalTo: restaurantProfileImageCellImageView.rightAnchor,constant: 10),
            restaurantNameCellLabel.rightAnchor.constraint(equalTo: restaurantTableViewCellContentView.rightAnchor , constant: -10),
            restaurantNameCellLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    
    private func getStarIconCellImageViewContraints() -> [NSLayoutConstraint]{
        let imageContraints = [
            starIconCellImageView.topAnchor.constraint(equalTo: restaurantNameCellLabel.bottomAnchor),
            starIconCellImageView.leftAnchor.constraint(equalTo: restaurantProfileImageCellImageView.rightAnchor,constant: 10),
            starIconCellImageView.widthAnchor.constraint(equalToConstant: 20),
            starIconCellImageView.heightAnchor.constraint(equalToConstant: 20)]
        return imageContraints
    }
    
    private func getRestaurantStarRatingCellLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            restaurantStarRatingCellLabel.topAnchor.constraint(equalTo: restaurantNameCellLabel.bottomAnchor),
            restaurantStarRatingCellLabel.leftAnchor.constraint(equalTo: starIconCellImageView.rightAnchor),
            restaurantStarRatingCellLabel.widthAnchor.constraint(equalToConstant: 60),
            restaurantStarRatingCellLabel.heightAnchor.constraint(equalToConstant: 20)]
        return labelContraints
    }
    
    private func getDeliveryTimingCellLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
            availableCellLabel.topAnchor.constraint(equalTo: restaurantNameCellLabel.bottomAnchor),
            availableCellLabel.leftAnchor.constraint(equalTo: restaurantStarRatingCellLabel.rightAnchor),
            availableCellLabel.rightAnchor.constraint(equalTo : restaurantTableViewCellContentView.rightAnchor,constant: -10),
            availableCellLabel.heightAnchor.constraint(equalToConstant: 20)]
        return labelContraints
    }
    
    
    private func getRestaurantCuisineCellLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
            restaurantCuisineCellLabel.topAnchor.constraint(equalTo: restaurantStarRatingCellLabel.bottomAnchor ),
            restaurantCuisineCellLabel.leftAnchor.constraint(equalTo: restaurantProfileImageCellImageView.rightAnchor , constant: 10),
            restaurantCuisineCellLabel.rightAnchor.constraint(equalTo: restaurantTableViewCellContentView.rightAnchor , constant: -10),
            restaurantCuisineCellLabel.heightAnchor.constraint(equalToConstant: 20)]
        return labelContraints
    }
    
    private func getRestaurantLocalityCellLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
            restaurantLocalityCellLabel.topAnchor.constraint(equalTo: restaurantCuisineCellLabel.bottomAnchor),
            restaurantLocalityCellLabel.leftAnchor.constraint(equalTo: restaurantProfileImageCellImageView.rightAnchor , constant: 10),
            restaurantLocalityCellLabel.rightAnchor.constraint(equalTo: restaurantTableViewCellContentView.rightAnchor , constant: -10),
            restaurantLocalityCellLabel.heightAnchor.constraint(equalToConstant: 20)]
        return labelContraints
    }
    
    
    
    
    
    
    private func initialiseViewElements(){
        restaurantTableViewCellContentView.addSubview(restaurantProfileImageCellImageView)
        restaurantTableViewCellContentView.addSubview(restaurantNameCellLabel)
        restaurantTableViewCellContentView.addSubview(starIconCellImageView)
        restaurantTableViewCellContentView.addSubview(restaurantStarRatingCellLabel)
        restaurantTableViewCellContentView.addSubview(availableCellLabel)
        restaurantTableViewCellContentView.addSubview(restaurantCuisineCellLabel)
        restaurantTableViewCellContentView.addSubview(restaurantLocalityCellLabel)
        NSLayoutConstraint.activate(getRestaurantProfileImageCellImageViewContraints())
        NSLayoutConstraint.activate(getRestaurantNameCellLabelContraints())
        NSLayoutConstraint.activate(getStarIconCellImageViewContraints())
        NSLayoutConstraint.activate(getRestaurantStarRatingCellLabelContraints())
        NSLayoutConstraint.activate(getDeliveryTimingCellLabelContraints())
        NSLayoutConstraint.activate(getRestaurantCuisineCellLabelContraints())
        NSLayoutConstraint.activate(getRestaurantLocalityCellLabelContraints())
    }
    
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(restaurantTableViewCellContentView)
        initialiseViewElements()
    }
    
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        restaurantTableViewCellContentView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: cellHeight)
        
    }
    
    func configureContent(restaurentCellContents : (restaurantProfileImage : Data , restaurantName : String , restaurantStarRating : String , restaurantCuisine : String , restaurantLocality : String , deliveryTiming : Int , restaurantOpensNextAt  : String,restaurantIsAvailable : Int,isDeliverable : Bool)){
        restaurantNameCellLabel.text = restaurentCellContents.restaurantName
        restaurantCuisineCellLabel.text = restaurentCellContents.restaurantCuisine
        restaurantStarRatingCellLabel.text =  String(restaurentCellContents.restaurantStarRating.prefix(upTo: restaurentCellContents.restaurantStarRating.index(restaurentCellContents.restaurantStarRating.startIndex, offsetBy: 3)))
        restaurantLocalityCellLabel.text = restaurentCellContents.restaurantLocality
        restaurantProfileImageCellImageView.image = UIImage(data: restaurentCellContents.restaurantProfileImage)
        updateRestaurantStatus(deliveryTiming : restaurentCellContents.deliveryTiming, isDeliverable : restaurentCellContents.isDeliverable , restaurantIsAvailable : restaurentCellContents.restaurantIsAvailable , restaurantOpensNextAt : restaurentCellContents.restaurantOpensNextAt)
    }
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        restaurantProfileImageCellImageView.image = nil
        restaurantNameCellLabel.text = nil
        restaurantStarRatingCellLabel.text = nil
        availableCellLabel.text = nil
        restaurantCuisineCellLabel.text = nil
        restaurantLocalityCellLabel.text = nil
    }
    
}




extension UITableViewCell {
    
     func removeExistingContentView(){
        for subview in self.subviews{
            subview.removeFromSuperview()
        }
    }
    
}

