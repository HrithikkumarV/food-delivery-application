//
//  TabBarView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 08/03/22.
//

import Foundation
import  UIKit

class MoveToFullMenuView : UIView, MoveToFullMenuViewProtocol{
   
    weak var delegate : MoveToFullMenuViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .systemGreen
        self.layer.cornerRadius = 5
        self.dropShadow()
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapMoveToFullMenu)))
        initialiseViewElements()
    }
    
    @objc func didTapMoveToFullMenu(){
        delegate?.didTapMoveToFullMenu()
    }
    
   private func initialiseViewElements() {
        self.addSubview(viewFullMenuLabel)
        self.addSubview(menuCountAndPriceLabel)
        self.addSubview(restaurantNameInCartView)
        NSLayoutConstraint.activate(getMenuItemCountAndPriceLabelLayoutConstraints())
        NSLayoutConstraint.activate(getRestaurantNameLabelInCartViewLayoutConstraints())
        NSLayoutConstraint.activate(getViewFullMenuLabelLayoutConstraints())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
 
    
   
    
    private lazy var menuCountAndPriceLabel : UILabel = {
        let menuCountAndPriceLabel = UILabel()
        menuCountAndPriceLabel.textColor = .white
        menuCountAndPriceLabel.backgroundColor = .clear
        menuCountAndPriceLabel.textAlignment = .left
        menuCountAndPriceLabel.adjustsFontSizeToFitWidth = true
        menuCountAndPriceLabel.translatesAutoresizingMaskIntoConstraints = false
        menuCountAndPriceLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return menuCountAndPriceLabel
    }()
    
    private lazy var viewFullMenuLabel : UILabel = {
        let viewFullMenuLabel = UILabel()
        viewFullMenuLabel.textColor = .white
        viewFullMenuLabel.backgroundColor = .clear
        viewFullMenuLabel.text = "View Full Menu"
        viewFullMenuLabel.textAlignment = .right
        viewFullMenuLabel.translatesAutoresizingMaskIntoConstraints = false
        viewFullMenuLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return viewFullMenuLabel
    }()
    
    private lazy var restaurantNameInCartView : UILabel = {
        let restaurantNameInCartView = UILabel()
        restaurantNameInCartView.textColor = .white
        restaurantNameInCartView.backgroundColor = .clear
        restaurantNameInCartView.textAlignment = .left
        restaurantNameInCartView.adjustsFontSizeToFitWidth = true
        restaurantNameInCartView.translatesAutoresizingMaskIntoConstraints = false
        restaurantNameInCartView.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return restaurantNameInCartView
    }()
    

    
    private func getMenuItemCountAndPriceLabelLayoutConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            menuCountAndPriceLabel.topAnchor.constraint(equalTo: self.topAnchor,constant: 5),
            menuCountAndPriceLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            menuCountAndPriceLabel.rightAnchor.constraint(equalTo: self.centerXAnchor),
            menuCountAndPriceLabel.bottomAnchor.constraint(equalTo: restaurantNameInCartView.bottomAnchor,constant: -5)]
        return labelContraints
    }
    
    private func getRestaurantNameLabelInCartViewLayoutConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            restaurantNameInCartView.heightAnchor.constraint(equalToConstant: 15),
            restaurantNameInCartView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            restaurantNameInCartView.rightAnchor.constraint(equalTo: self.centerXAnchor),
            restaurantNameInCartView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -5 )]
        return labelContraints
    }
    
    private func getViewFullMenuLabelLayoutConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            viewFullMenuLabel.topAnchor.constraint(equalTo: self.topAnchor,constant: 5),
            viewFullMenuLabel.leftAnchor.constraint(equalTo: self.centerXAnchor),
            viewFullMenuLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            viewFullMenuLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -5 )]
        return labelContraints
    }
    
   
}




extension MoveToFullMenuView {
    func updateRestaurantNameInCartLabelText(restaurantNameInUserCart : String){
        restaurantNameInCartView.text = "  From : \(restaurantNameInUserCart)"
    }
    
    func updateMenuItemCountAndPriceLabelText(menuCount : Int,totalPrice : Int){
        if(menuCount == 1){
            menuCountAndPriceLabel.text = " \(menuCount) Item | ₹\(totalPrice)"
        }
        else{
            menuCountAndPriceLabel.text = " \(menuCount) Items | ₹\(totalPrice)"
        }
    }
    
    func hideOrShowmoveToFullMenuViewBasedOnMenuCount(menuCount :Int){
        if(menuCount == 0){
            self.isHidden = true
        }
        else{
            self.isHidden = false
        }
    }
}

protocol MoveToFullMenuViewDelegate : AnyObject{
    func didTapMoveToFullMenu()
}
