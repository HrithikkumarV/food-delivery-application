//
//  File.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 15/01/22.
//

import Foundation
import UIKit

class SelectDeliveryAddressView : UIView,    SelectDeliveryAddressViewProtocol{
    
    weak var delegate : SelectDeliveryAddressViewDelegate?
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        self.addSubview(setDeliveryAddressButton)
        self.addSubview(savedAddressesLabel)
        self.addSubview(savedAddressesTableView)
        NSLayoutConstraint.activate(getSetDeliveryAddressButtonContraints())
        NSLayoutConstraint.activate(getSavedAddressesTableViewContraints())
        NSLayoutConstraint.activate(getSavedAddressesLabelContraints())
    }
   
    
    lazy var setDeliveryAddressButton : UIButton = {
        let setDeliveryAddressButton = UIButton()
        setDeliveryAddressButton.setTitle("Set New Address", for: .normal)
        setDeliveryAddressButton.setTitleColor(.white, for: .normal)
        setDeliveryAddressButton.translatesAutoresizingMaskIntoConstraints = false
        setDeliveryAddressButton.backgroundColor = .systemBlue
        setDeliveryAddressButton.layer.borderWidth = 1
        setDeliveryAddressButton.layer.borderColor = UIColor.systemGray5.cgColor
        setDeliveryAddressButton.layer.cornerRadius = 10
        setDeliveryAddressButton.addTarget(self, action: #selector(didTapSetAddress), for: .touchUpInside)
        return setDeliveryAddressButton
    }()
    
    var savedAddressesLabel : UILabel = {
        let savedAddressesLabel = UILabel()
        savedAddressesLabel.textColor = .black
        savedAddressesLabel.text = "Saved Addresses"
        savedAddressesLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        savedAddressesLabel.translatesAutoresizingMaskIntoConstraints = false
        savedAddressesLabel.adjustsFontSizeToFitWidth = true
        return savedAddressesLabel
    }()
    
    lazy var savedAddressesTableView : UITableView = {
        let savedAddressesTableView = UITableView()
        savedAddressesTableView.translatesAutoresizingMaskIntoConstraints = false
        savedAddressesTableView.backgroundColor = .white
        savedAddressesTableView.register(AddressTableViewCell.self, forCellReuseIdentifier:AddressTableViewCell.cellIdentifier )
        return savedAddressesTableView
    }()
    
    
    
    
    
    
    
    
    func createAddressTableViewCell(indexPath : IndexPath , userAddress : UserAddressDetails) -> UITableViewCell{
        let addressTableViewCell = savedAddressesTableView.dequeueReusableCell(withIdentifier: AddressTableViewCell.cellIdentifier, for: indexPath) as? AddressTableViewCell
        addressTableViewCell?.configureContents(userAddress: userAddress)
        addressTableViewCell?.cellHeight = 140
        return addressTableViewCell ?? UITableViewCell()
        
    }
    
    func getAddressTableViewCellHeight() -> CGFloat{
        return 140
    }
    
    
    
    
    
    private func getSavedAddressesTableViewContraints() -> [NSLayoutConstraint] {
        let tableViewContraints = [savedAddressesTableView.topAnchor.constraint(equalTo: savedAddressesLabel.bottomAnchor, constant: 10),
                                savedAddressesTableView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
                                savedAddressesTableView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20),
                                   savedAddressesTableView.bottomAnchor.constraint(equalTo : self.bottomAnchor,constant: -50)]
        return tableViewContraints
    }
    
    
    private func getSetDeliveryAddressButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [setDeliveryAddressButton.topAnchor.constraint(equalTo: self.topAnchor),
                                setDeliveryAddressButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
                                setDeliveryAddressButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40),
                                setDeliveryAddressButton.heightAnchor.constraint(equalToConstant: 50)]
        return buttonContraints
    }
    
    private func getSavedAddressesLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [savedAddressesLabel.topAnchor.constraint(equalTo: setDeliveryAddressButton.bottomAnchor, constant: 20),
                               savedAddressesLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
                               savedAddressesLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
                               savedAddressesLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    
    @objc func didTapSetAddress(){
        delegate?.didTapSetAddress()
    }
    
}


protocol SelectDeliveryAddressViewDelegate :AnyObject{
    func didTapSetAddress()
}


