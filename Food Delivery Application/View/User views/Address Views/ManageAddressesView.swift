//
//  EditSavedAddressesView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/03/22.
//

import Foundation
import UIKit

class ManageAddressesView : UIView    ,ManageAddressesViewProtocol{
    
    weak var delegate : ManageAddressesViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        self.addSubview(addDeliveryAddressButton)
        self.addSubview(savedAddressesTableView)
        self.addSubview(savedAddressesLabel)
        NSLayoutConstraint.activate(getAddDeliveryAddressButtonContraints())
        NSLayoutConstraint.activate(getSavedAddressesTableViewContraints())
        NSLayoutConstraint.activate(getSavedAddressesLabelContraints())
    }
    
    
    lazy var addDeliveryAddressButton : UIButton = {
        let addDeliveryAddressButton = UIButton()
        addDeliveryAddressButton.setTitle("Add New Address", for: .normal)
        addDeliveryAddressButton.setTitleColor(.white, for: .normal)
        addDeliveryAddressButton.translatesAutoresizingMaskIntoConstraints = false
        addDeliveryAddressButton.backgroundColor = .systemBlue
        addDeliveryAddressButton.layer.borderWidth = 1
        addDeliveryAddressButton.layer.borderColor = UIColor.systemGray5.cgColor
        addDeliveryAddressButton.layer.cornerRadius = 10
        addDeliveryAddressButton.addTarget(self, action: #selector(didTapAddAddress), for: .touchUpInside)
        return addDeliveryAddressButton
    }()
    
    lazy var savedAddressesTableView : UITableView = {
        let savedAddressesTableView = UITableView()
        savedAddressesTableView.translatesAutoresizingMaskIntoConstraints = false
        savedAddressesTableView.backgroundColor = .white
        savedAddressesTableView.sectionIndexColor = .none
        savedAddressesTableView.allowsSelection = false
        savedAddressesTableView.register(ManageAddressesTableViewCell.self, forCellReuseIdentifier: ManageAddressesTableViewCell.cellIdentifier)
        return savedAddressesTableView
    }()
    var savedAddressesLabel : UILabel = {
        let savedAddressesLabel = UILabel()
        savedAddressesLabel.textColor = .systemGray
        savedAddressesLabel.text = "Saved Addresses"
        savedAddressesLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        savedAddressesLabel.translatesAutoresizingMaskIntoConstraints = false
        savedAddressesLabel.adjustsFontSizeToFitWidth = true
        return savedAddressesLabel
    }()
    
    func createAddressTableViewCell(indexPath : IndexPath , userAddress : UserAddressDetails) -> ManageAddressesTableViewCell{
        let manageAddressesViewCell = savedAddressesTableView.dequeueReusableCell(withIdentifier: ManageAddressesTableViewCell.cellIdentifier, for: indexPath) as? ManageAddressesTableViewCell
        manageAddressesViewCell?.configureContents(userAddress: userAddress,editButtonTag: indexPath.row , deleteButtonTag: indexPath.row)
        manageAddressesViewCell?.cellHeight = 180
        return manageAddressesViewCell ?? ManageAddressesTableViewCell()
    }
    
    func getAddressTableViewCellHeight() -> CGFloat{
        return 180
    }
    
    
    private func getSavedAddressesTableViewContraints() -> [NSLayoutConstraint] {
        let tableViewContraints = [savedAddressesTableView.topAnchor.constraint(equalTo: savedAddressesLabel.bottomAnchor, constant: 20),
                                savedAddressesTableView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
                                savedAddressesTableView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20),
                                   savedAddressesTableView.bottomAnchor.constraint(equalTo : self.bottomAnchor)]
        return tableViewContraints
    }
    
    
    private func getAddDeliveryAddressButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [addDeliveryAddressButton.topAnchor.constraint(equalTo: self.topAnchor),
                                addDeliveryAddressButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
                                addDeliveryAddressButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40),
                                addDeliveryAddressButton.heightAnchor.constraint(equalToConstant: 50)]
        return buttonContraints
    }
    
    private func getSavedAddressesLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [savedAddressesLabel.topAnchor.constraint(equalTo: addDeliveryAddressButton.bottomAnchor, constant: 20),
                               savedAddressesLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
                               savedAddressesLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),savedAddressesLabel.heightAnchor.constraint(equalToConstant: 40)]
        return labelContraints
    }
    
    @objc func didTapAddAddress(){
        delegate?.didTapAddAddress()
    }
    
    
}

protocol ManageAddressesViewDelegate : AnyObject{
     func didTapAddAddress()
}

