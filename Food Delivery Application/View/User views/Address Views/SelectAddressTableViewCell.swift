//
//  SelectAddressTableViewCell.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 20/04/22.
//

import UIKit

class AddressTableViewCell : UITableViewCell{
    
    static let cellIdentifier = "AddressTableViewCell"
    
    var cellHeight : CGFloat = 44
    
    var addressTableViewCellContentView : UIView = {
        let  addressTableViewCellContentView = UIView()
        addressTableViewCellContentView.backgroundColor = .white
        return addressTableViewCellContentView
    }()
    
    private lazy var addressCellLabel : UILabel = {
        let addressCellLabel = UILabel()
        addressCellLabel.textColor = .black
        addressCellLabel.translatesAutoresizingMaskIntoConstraints = false
        addressCellLabel.adjustsFontSizeToFitWidth = true
        addressCellLabel.numberOfLines = 4
        addressCellLabel.lineBreakMode = .byWordWrapping
        return addressCellLabel
    }()
    
    private lazy var addressTagIconimageView : UIImageView = {
        let addressTagIconimageView = UIImageView()
        addressTagIconimageView.backgroundColor = .white
        addressTagIconimageView.translatesAutoresizingMaskIntoConstraints = false
        return addressTagIconimageView
    }()
    private lazy var addressTagCellLabel : UILabel = {
        let addressTagCellLabel = UILabel()
        addressTagCellLabel.textColor = .black
        addressTagCellLabel.font = UIFont.systemFont(ofSize: 20.0)
        addressTagCellLabel.translatesAutoresizingMaskIntoConstraints = false
        return addressTagCellLabel
    }()

    
    private func updateAddressCellLabel(addressDetails : AddressDetails) {
       
        addressCellLabel.text = "\(addressDetails.doorNoAndBuildingNameAndBuildingNo), \(addressDetails.streetName), \(addressDetails.landmark), \(addressDetails.localityName), \(addressDetails.city), \(addressDetails.state),  \(addressDetails.country), \(addressDetails.pincode)"
   }
   
   
   
   private func updateAddressTagCellLabel(addressTag : String) {
       
       addressTagCellLabel.text = addressTag
      
   }
   
   private func updateAddressTagIconLabel(addressTag : String) {
       
       if(addressTag == "Home"){
           addressTagIconimageView.image = UIImage(systemName: "house")?.withTintColor(.black, renderingMode: .alwaysOriginal)
       }
       else if(addressTag == "Work"){
           addressTagIconimageView.image = UIImage(systemName: "bag")?.withTintColor(.black, renderingMode: .alwaysOriginal)
       }
       else{
           addressTagIconimageView.image = UIImage(systemName: "location")?.withTintColor(.black, renderingMode: .alwaysOriginal)
       }
      
   }
   

    

   
    
    private func getAddressTagCellLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [addressTagCellLabel.topAnchor.constraint(equalTo: addressTableViewCellContentView.topAnchor),
            addressTagCellLabel.leftAnchor.constraint(equalTo: addressTagIconimageView.rightAnchor,constant: 10),
            addressTagCellLabel.rightAnchor.constraint(equalTo: addressTableViewCellContentView.rightAnchor),
            addressTagCellLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    
    private func getAddressTagIconimageViewContraints() -> [NSLayoutConstraint]{
        let labelContraints = [addressTagIconimageView.topAnchor.constraint(equalTo: addressTableViewCellContentView.topAnchor),
                               addressTagIconimageView.leftAnchor.constraint(equalTo: addressTableViewCellContentView.leftAnchor),
                               addressTagIconimageView.widthAnchor.constraint(equalToConstant: 30),
                               addressTagIconimageView.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    
    private func getAddressCellLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            addressCellLabel.topAnchor.constraint(equalTo: addressTagCellLabel.bottomAnchor),
            addressCellLabel.leftAnchor.constraint(equalTo: addressTagIconimageView.rightAnchor,constant: 10),
            addressCellLabel.rightAnchor.constraint(equalTo: addressTableViewCellContentView.rightAnchor),
            addressCellLabel.heightAnchor.constraint(equalToConstant: 100)]
        return labelContraints
        
    }

 
    private func initialseViewElements(){
        addressTableViewCellContentView.addSubview(addressCellLabel)
        addressTableViewCellContentView.addSubview(addressTagCellLabel)
        addressTableViewCellContentView.addSubview(addressTagIconimageView)
        NSLayoutConstraint.activate(getAddressCellLabelContraints())
        NSLayoutConstraint.activate(getAddressTagIconimageViewContraints())
        NSLayoutConstraint.activate(getAddressTagCellLabelContraints())
        
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(addressTableViewCellContentView)
        initialseViewElements()
    }

    func configureContents(userAddress : UserAddressDetails){
        print(userAddress)
        updateAddressTagIconLabel(addressTag: userAddress.addressTag)
        updateAddressTagCellLabel(addressTag: userAddress.addressTag)
        updateAddressCellLabel(addressDetails: userAddress.addressDetails)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addressTableViewCellContentView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: cellHeight)

        
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        addressCellLabel.text = nil
        addressTagCellLabel.text = nil
        addressTagIconimageView.image = nil
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


