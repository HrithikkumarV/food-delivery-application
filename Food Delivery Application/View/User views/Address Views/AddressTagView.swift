//
//  AddressLabelView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/12/21.
//

import Foundation
import UIKit

class AddressTagView : UIView ,AddressTagViewProtocol{
    
    weak var delegate : AddressTagViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
    }
    
    private func initialiseViewElements(){
        self.addSubview(homeAddressTagButton)
        self.addSubview(workAddressTagButton)
        self.addSubview(otherAddressTagButton)
        self.addSubview(saveButton)
        self.addSubview(addressLabel)
        self.addSubview(saveThisAddressAsLabel)
        self.addSubview(otherAddressTagTextField)
        self.addSubview(stackViewForAddressTags)
        NSLayoutConstraint.activate(getAddressLabelContraints())
        NSLayoutConstraint.activate(getSaveThisAddressAsLabelContraints())
        NSLayoutConstraint.activate(getAddressTagsStackViewConstraints())
        NSLayoutConstraint.activate(getOtherAddressTagTextFieldConstraints())
        NSLayoutConstraint.activate(getSaveButtonContraints())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
     lazy var stackViewForAddressTags : UIStackView = {
        let  stackViewForAddressTags = UIStackView(arrangedSubviews: [homeAddressTagButton,workAddressTagButton ,otherAddressTagButton])
        stackViewForAddressTags.axis = .horizontal
        stackViewForAddressTags.spacing = 10
        stackViewForAddressTags.distribution = .fillEqually
        stackViewForAddressTags.translatesAutoresizingMaskIntoConstraints = false
        return stackViewForAddressTags
    }()
     var addressLabel : UILabel = {
        let addressLabel = UILabel()
        addressLabel.translatesAutoresizingMaskIntoConstraints = false
        addressLabel.backgroundColor = .white
        addressLabel.textColor = .black
        addressLabel.numberOfLines = 10
        addressLabel.addLabelToTopBorder(labelText: "Given Address : ", borderColor: .clear , borderWidth: 1, leftPadding: 0, topPadding: 10 , textColor: .black.withAlphaComponent(0.8), fontSize: 15)
        addressLabel.lineBreakMode = .byWordWrapping
        addressLabel.layer.borderWidth = 1
        addressLabel.layer.borderColor = UIColor.systemGray5.cgColor
        addressLabel.layer.cornerRadius = 5
        addressLabel.textAlignment = .justified
        addressLabel.adjustsFontSizeToFitWidth = true
        addressLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return addressLabel
    }()
    
     var saveThisAddressAsLabel : UILabel = {
        let saveThisAddressAsLabel = UILabel()
        saveThisAddressAsLabel.text = "Save This Address As"
        saveThisAddressAsLabel.textColor = .black
        saveThisAddressAsLabel.backgroundColor = .white
        saveThisAddressAsLabel.translatesAutoresizingMaskIntoConstraints = false
        saveThisAddressAsLabel.sizeToFit()
        return saveThisAddressAsLabel
    }()
    
     var otherAddressTagTextField : UITextField = {
        let otherAddressTagTextField = CustomTextField()
        otherAddressTagTextField.placeholder = "e.g: My place , Dad's Place"
        otherAddressTagTextField.addLabelToTopBorder(labelText: " Address Tag ", fontSize: 15)
        otherAddressTagTextField .backgroundColor = .white
        otherAddressTagTextField .textColor = .black
        otherAddressTagTextField .translatesAutoresizingMaskIntoConstraints = false
        otherAddressTagTextField.keyboardType = .default
        otherAddressTagTextField.isHidden = true
        otherAddressTagTextField.returnKeyType = .done
        return otherAddressTagTextField
    }()
    
     lazy var homeAddressTagButton : UIButton = {
        let homeButton = createAddressTagButton()
        homeButton.setTitle("Home", for: .normal)
        homeButton.setImage(UIImage(systemName: "house")?.withTintColor(.black, renderingMode: .alwaysOriginal), for: .normal)
        homeButton.addTarget(self, action: #selector(didTapHomeTag), for: .touchUpInside)
        return homeButton
    }()
     lazy var workAddressTagButton : UIButton = {
        let workButton = createAddressTagButton()
        workButton.setTitle("Work", for: .normal)
        workButton.setImage(UIImage(systemName: "bag")?.withTintColor(.black, renderingMode: .alwaysOriginal), for: .normal)
        workButton.addTarget(self, action: #selector(didTapWorkTag), for: .touchUpInside)
        return workButton
    }()
     lazy var otherAddressTagButton : UIButton = {
        let otherButton = createAddressTagButton()
        otherButton.setTitle("Other", for: .normal)
        otherButton.setImage(UIImage(systemName: "location")?.withTintColor(.black, renderingMode: .alwaysOriginal), for: .normal)
        otherButton.addTarget(self, action: #selector(didTapOtherTag), for: .touchUpInside)
        return otherButton
    }()
     lazy var saveButton : UIButton = {
        let saveButton = UIButton()
        saveButton.setTitle("save", for: .normal)
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.translatesAutoresizingMaskIntoConstraints = false
        saveButton.backgroundColor = .systemBlue
        saveButton.layer.cornerRadius = 5
        saveButton.layer.borderWidth = 2
        saveButton.layer.borderColor = UIColor.white.cgColor
        saveButton.addTarget(self, action: #selector(didTapSave), for: .touchUpInside)
        return saveButton
    }()
    
    
   
    
    
    
    private func createAddressTagButton() -> UIButton{
        let button = UIButton()
        button.setTitleColor(.black, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .white
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.systemGray5.cgColor
        button.layer.cornerRadius = 10
        button.dropShadow()
        return button
    }
    
    
    private func getAddressTagsStackViewConstraints() -> [NSLayoutConstraint]{
        let stackViewConstraints = [stackViewForAddressTags.topAnchor.constraint(equalTo: saveThisAddressAsLabel.bottomAnchor ,constant: 15),
                                    stackViewForAddressTags.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
                                    stackViewForAddressTags.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40),
                                    stackViewForAddressTags.heightAnchor.constraint(equalToConstant: 60)
        ]
        return stackViewConstraints
    }
    
    private func getSaveThisAddressAsLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [saveThisAddressAsLabel.topAnchor.constraint(equalTo: addressLabel.bottomAnchor, constant: 35),
                               saveThisAddressAsLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
                               saveThisAddressAsLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40),
                               saveThisAddressAsLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    
    private func getAddressLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [addressLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 85),
                               addressLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
                               addressLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40),
                               addressLabel.heightAnchor.constraint(equalToConstant: 200)]
        return labelContraints
    }
    
    private func getSaveButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [saveButton.topAnchor.constraint(equalTo: otherAddressTagTextField.bottomAnchor, constant: 50),
                                saveButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 80),
                                saveButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -80),
                                saveButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    private func getOtherAddressTagTextFieldConstraints() -> [NSLayoutConstraint]{
        let textFieldContraints = [otherAddressTagTextField.topAnchor.constraint(equalTo: stackViewForAddressTags.bottomAnchor , constant: 20),
                                   otherAddressTagTextField.leftAnchor.constraint(equalTo: self.leftAnchor ,constant: 40),
                                   otherAddressTagTextField.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -40),
                                   otherAddressTagTextField.heightAnchor.constraint(equalToConstant : 50)]
        return textFieldContraints
    }
    
   
    @objc func didTapView(){
        delegate?.didTapView()
    }
    
    @objc func didTapHomeTag(){
        delegate?.didTapHomeTag()
    }
    
    @objc func didTapWorkTag(){
        delegate?.didTapWorkTag()
    }
    
    @objc func didTapOtherTag(){
        delegate?.didTapOtherTag()
    }
    
    
    @objc func didTapSave(){
        delegate?.didTapSave()
    }
}


protocol AddressTagViewDelegate :AnyObject{
     func didTapView()
    
     func didTapHomeTag()
    
     func didTapWorkTag()
    
     func didTapOtherTag()
    
     func didTapSave()
}
