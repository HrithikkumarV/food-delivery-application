//
//  ManageAddressTableViewCell.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 20/04/22.
//

import UIKit

class ManageAddressesTableViewCell : UITableViewCell{
    
    static let cellIdentifier = "ManageAddressesTableViewCell"
    
    
    var cellHeight : CGFloat = 44
    weak var delegate : ManageAddressesViewCellDelegate?
    
    private let addressTableViewCellContentView : UIView = {
        let  addressTableViewCellContentView = UIView()
        addressTableViewCellContentView.backgroundColor = .white
        return addressTableViewCellContentView
    }()
    
    private lazy var addressCellLabel : UILabel = {
        let addressCellLabel = UILabel()
        addressCellLabel.textColor = .black
        addressCellLabel.translatesAutoresizingMaskIntoConstraints = false
        addressCellLabel.adjustsFontSizeToFitWidth = true
        addressCellLabel.numberOfLines = 4
        addressCellLabel.lineBreakMode = .byWordWrapping
        return addressCellLabel
    }()
    
    private lazy var addressTagIconimageView : UIImageView = {
        let addressTagIconimageView = UIImageView()
        addressTagIconimageView.backgroundColor = .white
        addressTagIconimageView.translatesAutoresizingMaskIntoConstraints = false
        return addressTagIconimageView
    }()
    private lazy var addressTagCellLabel : UILabel = {
        let addressTagCellLabel = UILabel()
        addressTagCellLabel.textColor = .black
        addressTagCellLabel.font = UIFont.systemFont(ofSize: 20.0)
        addressTagCellLabel.translatesAutoresizingMaskIntoConstraints = false
        return addressTagCellLabel
    }()
    
    private lazy var editAddressButton : UIButton = {
        let editAddressButton = UIButton()
        editAddressButton.setTitle("EDIT", for: .normal)
        editAddressButton.setTitleColor(.black.withAlphaComponent(0.9), for: .normal)
        editAddressButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        editAddressButton.translatesAutoresizingMaskIntoConstraints = false
        editAddressButton.addTarget(self, action: #selector(didTapEditButton(sender:)), for: .touchUpInside)
        return editAddressButton
    }()
    
    
    private lazy var deleteAddressButton : UIButton = {
        let deleteAddressButton = UIButton()
        deleteAddressButton.setTitle("DELETE", for: .normal)
        deleteAddressButton.setTitleColor(.black.withAlphaComponent(0.9), for: .normal)
        deleteAddressButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        deleteAddressButton.translatesAutoresizingMaskIntoConstraints = false
        deleteAddressButton.addTarget(self, action: #selector(didTapDelete(sender:)), for: .touchUpInside)
        return deleteAddressButton
    }()

    
    @objc func didTapEditButton(sender: UIButton){
        delegate?.didTapEditButton(sender: sender)
    }
    
    @objc func didTapDelete(sender: UIButton){
        delegate?.didTapDelete(sender: sender)
    }
    
    private func updateAddressCellLabel(addressDetails : AddressDetails) {
       
        addressCellLabel.text = "\(addressDetails.doorNoAndBuildingNameAndBuildingNo), \(addressDetails.streetName), \(addressDetails.landmark), \(addressDetails.localityName), \(addressDetails.city), \(addressDetails.state),  \(addressDetails.country), \(addressDetails.pincode)"
   }
   
   
   
   private func updateAddressTagCellLabel(addressTag : String) {
       addressTagCellLabel.text = addressTag
   }
   
   private func updateAddressTagIconLabel(addressTag : String) {
       
       if(addressTag == "Home"){
           addressTagIconimageView.image = UIImage(systemName: "house")?.withTintColor(.black, renderingMode: .alwaysOriginal)
       }
       else if(addressTag == "Work"){
           addressTagIconimageView.image = UIImage(systemName: "bag")?.withTintColor(.black, renderingMode: .alwaysOriginal)
       }
       else{
           addressTagIconimageView.image = UIImage(systemName: "location")?.withTintColor(.black, renderingMode: .alwaysOriginal)
       }
      
   }
   

    

   
    
    private func getAddressTagCellLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [addressTagCellLabel.topAnchor.constraint(equalTo: addressTableViewCellContentView.topAnchor,constant: 10),
            addressTagCellLabel.leftAnchor.constraint(equalTo: addressTagIconimageView.rightAnchor,constant: 10),
            addressTagCellLabel.rightAnchor.constraint(equalTo: addressTableViewCellContentView.rightAnchor),
            addressTagCellLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    
    private func getAddressTagIconimageViewContraints() -> [NSLayoutConstraint]{
        let labelContraints = [addressTagIconimageView.topAnchor.constraint(equalTo: addressTableViewCellContentView.topAnchor,constant: 10),
                               addressTagIconimageView.leftAnchor.constraint(equalTo: addressTableViewCellContentView.leftAnchor),
                               addressTagIconimageView.widthAnchor.constraint(equalToConstant: 30),
                               addressTagIconimageView.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    
    private func getAddressCellLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            addressCellLabel.topAnchor.constraint(equalTo: addressTagCellLabel.bottomAnchor),
            addressCellLabel.leftAnchor.constraint(equalTo: addressTagIconimageView.rightAnchor,constant: 10),
            addressCellLabel.rightAnchor.constraint(equalTo: addressTableViewCellContentView.rightAnchor),
            addressCellLabel.heightAnchor.constraint(equalToConstant: 100)]
        return labelContraints
        
    }
    
    private func getEditAddressButtonContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            editAddressButton.topAnchor.constraint(equalTo: addressCellLabel.bottomAnchor,constant: 10),
            editAddressButton.leftAnchor.constraint(equalTo: addressTableViewCellContentView.leftAnchor,constant: 10),
            editAddressButton.rightAnchor.constraint(equalTo: addressTableViewCellContentView.centerXAnchor),
            editAddressButton.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
        
    }
    
    private func getDeleteAddressButtonContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            deleteAddressButton.topAnchor.constraint(equalTo: addressCellLabel.bottomAnchor,constant: 10),
            deleteAddressButton.leftAnchor.constraint(equalTo: addressTableViewCellContentView.centerXAnchor),
            deleteAddressButton.rightAnchor.constraint(equalTo: addressTableViewCellContentView.rightAnchor),
            deleteAddressButton.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
        
    }

 
    private func initialiseViewElements(){
        addressTableViewCellContentView.addSubview(addressCellLabel)
        addressTableViewCellContentView.addSubview(addressTagCellLabel)
        addressTableViewCellContentView.addSubview(addressTagIconimageView)
        addressTableViewCellContentView.addSubview(editAddressButton)
        addressTableViewCellContentView.addSubview(deleteAddressButton)
        NSLayoutConstraint.activate(getAddressTagIconimageViewContraints())
        NSLayoutConstraint.activate(getAddressCellLabelContraints())
        NSLayoutConstraint.activate(getAddressTagCellLabelContraints())
        NSLayoutConstraint.activate(getEditAddressButtonContraints())
        NSLayoutConstraint.activate(getDeleteAddressButtonContraints())
    }
    
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(addressTableViewCellContentView)
        initialiseViewElements()
       
    }

    func configureContents(userAddress : UserAddressDetails,editButtonTag : Int,deleteButtonTag  :Int){
        print(userAddress)
        updateAddressTagIconLabel(addressTag: userAddress.addressTag)
        updateAddressTagCellLabel(addressTag: userAddress.addressTag)
        updateAddressCellLabel(addressDetails: userAddress.addressDetails)
        editAddressButton.tag = editButtonTag
        deleteAddressButton.tag = deleteButtonTag
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addressTableViewCellContentView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: cellHeight)
       
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        addressCellLabel.text = nil
        addressTagCellLabel.text = nil
        addressTagIconimageView.image = nil
        deleteAddressButton.tag = 0x0
        editAddressButton.tag = 0x0
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

protocol ManageAddressesViewCellDelegate : AnyObject{
    func didTapEditButton(sender: UIButton)
    func didTapDelete(sender: UIButton)
}

