//
//  UserCartView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 03/03/22.
//

import Foundation
import UIKit

class UserCartView : UIView,UserCartViewProtocol{
    
    weak var delegate : UserCartViewDelegate?
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        navigationBarTitleView.addSubview(restaurantNameNavigationBarLabel)
        navigationBarTitleView.addSubview(totalItemsAndPriceNavigationBarLabel)
        createRestaurantDetailsContents()
        self.addSubview(menuDetailsTableView)
        self.addSubview(addMoreMenuButton)
        self.addSubview(instructionsToRestaurantTextView)
        self.addSubview(applyCouponTextField)
        self.addSubview(applyCouponButton)
        self.addSubview(removeCouponButton)
        self.addSubview(couponAppliedLabel)
        self.addSubview(billView)
        createBillDetailsElements()
        self.addSubview(cancelOrderInstructionsLabel)
        NSLayoutConstraint.activate(getMenuDetailsTableViewLayoutConstraints())
       
        NSLayoutConstraint.activate(getAddMoreMenusLayoutContrainst())
        
        NSLayoutConstraint.activate(getInstructionsToRestaurantTextViewLayoutContrainst())
        
        NSLayoutConstraint.activate(getApplyCouponTextFieldLayoutContrainst())
        
        NSLayoutConstraint.activate(getapplyCouponButtonLayoutContrainst())
        
        NSLayoutConstraint.activate(getremoveCouponButtonLayoutContrainst())
        
        NSLayoutConstraint.activate(getCouponAppliedLabelLayoutContrainst())
        
        NSLayoutConstraint.activate(getBillViewLayOutConstraints())
       
        NSLayoutConstraint.activate(
            getCancelOrderInstructionsLabelConstraints())
    }
    
    var navigationBarTitleView : UIView = {
        let navigationBarTitleView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 100, height: 44))
        navigationBarTitleView.backgroundColor = .white
        return navigationBarTitleView
    }()
   
    lazy var restaurantNameNavigationBarLabel : UILabel = {
        let restaurantNameNavigationBarLabel = UILabel()
        restaurantNameNavigationBarLabel.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 22)
        restaurantNameNavigationBarLabel.textColor = .black.withAlphaComponent(0.9)
        restaurantNameNavigationBarLabel.backgroundColor = .white
        restaurantNameNavigationBarLabel.font = UIFont(name:"ArialRoundedMTBold", size: 25.0)
        restaurantNameNavigationBarLabel.adjustsFontSizeToFitWidth = true
        restaurantNameNavigationBarLabel.textAlignment = .left
        return restaurantNameNavigationBarLabel
    }()
    
    lazy var totalItemsAndPriceNavigationBarLabel : UILabel = {
        let totalItemsAndPriceNavigationBarLabel = UILabel()
        totalItemsAndPriceNavigationBarLabel.frame = CGRect(x: 0, y: restaurantNameNavigationBarLabel.frame.maxY, width: UIScreen.main.bounds.width, height: 22)
        totalItemsAndPriceNavigationBarLabel.textColor = .gray
        totalItemsAndPriceNavigationBarLabel.backgroundColor = .white
        totalItemsAndPriceNavigationBarLabel.textAlignment = .left
        totalItemsAndPriceNavigationBarLabel.font = UIFont(name:"ArialRoundedMTBold", size: 12.0)
        totalItemsAndPriceNavigationBarLabel.lineBreakMode = .byWordWrapping
        totalItemsAndPriceNavigationBarLabel.adjustsFontSizeToFitWidth = true
        return totalItemsAndPriceNavigationBarLabel
    }()
    lazy var addMoreMenuButton : UIButton = {
        let addMoreMenuButton = UIButton()
        addMoreMenuButton.translatesAutoresizingMaskIntoConstraints = false
        addMoreMenuButton.backgroundColor = .white
        addMoreMenuButton.setTitle(" + Add More Menus", for: .normal)
        addMoreMenuButton.setTitleColor(.systemGreen, for: .normal)
        addMoreMenuButton.titleLabel?.textAlignment = .center
        addMoreMenuButton.addTarget(self, action: #selector(didTapAddMoreMenu), for: .touchUpInside)
        return addMoreMenuButton
    
    }()
    
     var restaurantNameLabel : UILabel = {
        let  restaurantNameLabel = UILabel()
        restaurantNameLabel.textColor = .black.withAlphaComponent(0.9)
        restaurantNameLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantNameLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        restaurantNameLabel.textAlignment = .left
        return restaurantNameLabel
    }()
    
     var restaurantLocalityLabel : UILabel = {
        let restaurantLocalityLabel = UILabel()
        restaurantLocalityLabel.textColor = .systemGray
        restaurantLocalityLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantLocalityLabel.font = UIFont(name:"ArialRoundedMTBold", size: 13.0)
        return restaurantLocalityLabel
    }()
    
     var restaurantProfileImageView : UIImageView = {
        let restaurantProfileImageView = UIImageView()
        restaurantProfileImageView.translatesAutoresizingMaskIntoConstraints = false
        restaurantProfileImageView.backgroundColor = .white
        restaurantProfileImageView.layer.cornerRadius = 10
        restaurantProfileImageView.contentMode = .scaleAspectFill
        restaurantProfileImageView.clipsToBounds = true
        return restaurantProfileImageView
    }()
    
    var instructionsToRestaurantTextView : UITextView = {
        let instructionsToRestaurant = UITextView()
        instructionsToRestaurant.returnKeyType = .default
        instructionsToRestaurant.backgroundColor = .white
        instructionsToRestaurant.keyboardType = .default
        instructionsToRestaurant.textColor = .systemGray3
        instructionsToRestaurant.autocorrectionType = .no
        instructionsToRestaurant.font = UIFont(name: "ArialHebrew", size: 16)
        instructionsToRestaurant.translatesAutoresizingMaskIntoConstraints = false
        instructionsToRestaurant.layer.cornerRadius = 5
        instructionsToRestaurant.layer.borderColor = UIColor.systemGray5.cgColor
        instructionsToRestaurant.layer.borderWidth = 1
        return instructionsToRestaurant
    }()
    var applyCouponTextField : UITextField = {
        let applyCouponTextField = CustomTextField()
        applyCouponTextField .backgroundColor = .white
        applyCouponTextField .textColor = .black
        applyCouponTextField .translatesAutoresizingMaskIntoConstraints = false
        applyCouponTextField.placeholder = "Coupon Code"
        applyCouponTextField.addLabelToTopBorder(labelText: " Coupon Code ", fontSize: 15)
        applyCouponTextField.keyboardType = .default
        applyCouponTextField.returnKeyType = .done
        return applyCouponTextField
    }()
    lazy var applyCouponButton : UIButton = {
        let applyCouponButton = UIButton()
        applyCouponButton.setTitle("Apply", for: .normal)
        applyCouponButton.setTitleColor(.white, for: .normal)
        applyCouponButton.backgroundColor = .systemGreen.withAlphaComponent(0.9)
        applyCouponButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        applyCouponButton.translatesAutoresizingMaskIntoConstraints = false
        applyCouponButton.layer.borderWidth = 1
        applyCouponButton.layer.borderColor = UIColor.systemGray5.cgColor
        applyCouponButton.layer.cornerRadius = 1
        applyCouponButton.addTarget(self, action: #selector(didTapApplyCouponCode), for: .touchUpInside)
        return applyCouponButton
    }()
    lazy var removeCouponButton :UIButton = {
        let removeCouponButton = UIButton()
        removeCouponButton.setTitle("Remove", for: .normal)
        removeCouponButton.setTitleColor(.white, for: .normal)
        removeCouponButton.backgroundColor = .systemRed.withAlphaComponent(0.9)
        removeCouponButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        removeCouponButton.translatesAutoresizingMaskIntoConstraints = false
        removeCouponButton.layer.borderWidth = 1
        removeCouponButton.layer.borderColor = UIColor.systemGray5.cgColor
        removeCouponButton.layer.cornerRadius = 1
        removeCouponButton.addTarget(self, action: #selector(didTapRemoveCoupon), for: .touchUpInside)
        return removeCouponButton
    }()
    var couponAppliedLabel : UILabel = {
        let  couponAppliedLabel = UILabel()
        couponAppliedLabel.textColor = .black
        couponAppliedLabel.backgroundColor = .white
        couponAppliedLabel.font = UIFont(name:"ArialRoundedMTBold", size: 14.0)
        couponAppliedLabel.lineBreakMode = .byWordWrapping
        couponAppliedLabel.numberOfLines = 2
        couponAppliedLabel.sizeToFit()
        couponAppliedLabel.textAlignment = .left
        couponAppliedLabel.translatesAutoresizingMaskIntoConstraints = false
        return couponAppliedLabel
    }()
    
     var billView : UIView = {
        let billView = UIView()
        billView.translatesAutoresizingMaskIntoConstraints = false
       return billView
    }()
    
    private var billDetailsLabel : UILabel = {
        let billDetailsLabel = UILabel()
         billDetailsLabel.textColor = .black
         billDetailsLabel.backgroundColor = .white
         billDetailsLabel.font = UIFont(name:"ArialRoundedMTBold", size: 25.0)
         billDetailsLabel.adjustsFontSizeToFitWidth = true
         billDetailsLabel.textAlignment = .left
         billDetailsLabel.text = "Bill Details"
         billDetailsLabel.translatesAutoresizingMaskIntoConstraints = false
         return billDetailsLabel
    }()
    
    private var itemTotalLabel : UILabel = {
        let itemTotalLabel = UILabel()
        itemTotalLabel.textColor = .black.withAlphaComponent(0.9)
        itemTotalLabel.backgroundColor = .white
        itemTotalLabel.font = UIFont.systemFont(ofSize: 14)
        itemTotalLabel.adjustsFontSizeToFitWidth = true
        itemTotalLabel.textAlignment = .left
        itemTotalLabel.text = "Item Total"
        itemTotalLabel.translatesAutoresizingMaskIntoConstraints = false
        return itemTotalLabel
    }()
    
    private var itemTotal : UILabel = {
        let itemTotal = UILabel()
        itemTotal.textColor = .black.withAlphaComponent(0.9)
        itemTotal.backgroundColor = .white
        itemTotal.font = UIFont.systemFont(ofSize: 14)
        itemTotal.adjustsFontSizeToFitWidth = true
        itemTotal.textAlignment = .left
        itemTotal.translatesAutoresizingMaskIntoConstraints = false
        return itemTotal
    }()
    private var deliveryFeeLabel : UILabel = {
        let deliveryFeeLabel = UILabel()
        deliveryFeeLabel.textColor = .black.withAlphaComponent(0.9)
        deliveryFeeLabel.backgroundColor = .white
        deliveryFeeLabel.font = UIFont.systemFont(ofSize: 14)
        deliveryFeeLabel.adjustsFontSizeToFitWidth = true
        deliveryFeeLabel.textAlignment = .left
        deliveryFeeLabel.text = "delivery fee"
        deliveryFeeLabel.translatesAutoresizingMaskIntoConstraints = false
        return deliveryFeeLabel
    }()
    
    private var deliveryFee : UILabel = {
        let  deliveryFee = UILabel()
        deliveryFee.textColor = .black.withAlphaComponent(0.9)
        deliveryFee.backgroundColor = .white
        deliveryFee.font = UIFont.systemFont(ofSize: 14)
        deliveryFee.adjustsFontSizeToFitWidth = true
        deliveryFee.textAlignment = .left
        deliveryFee.translatesAutoresizingMaskIntoConstraints = false
        return deliveryFee
    }()
    
    private var restaurantGSTLabel : UILabel = {
        let restaurantGSTLabel = UILabel()
        restaurantGSTLabel.textColor = .black.withAlphaComponent(0.9)
        restaurantGSTLabel.backgroundColor = .white
        restaurantGSTLabel.font = UIFont.systemFont(ofSize: 14)
        restaurantGSTLabel.adjustsFontSizeToFitWidth = true
        restaurantGSTLabel.textAlignment = .left
        restaurantGSTLabel.text = "Restaurant GST @ 5%"
        restaurantGSTLabel.translatesAutoresizingMaskIntoConstraints = false
        return restaurantGSTLabel
    }()
    
    private var restaurantPackagingChargesLabel : UILabel = {
        let restaurantPackagingChargesLabel = UILabel()
        restaurantPackagingChargesLabel.textColor = .black.withAlphaComponent(0.9)
        restaurantPackagingChargesLabel.backgroundColor = .white
        restaurantPackagingChargesLabel.font = UIFont.systemFont(ofSize: 14)
        restaurantPackagingChargesLabel.adjustsFontSizeToFitWidth = true
        restaurantPackagingChargesLabel.textAlignment = .left
        restaurantPackagingChargesLabel.text = "Restaurant Packaging Charges"
        restaurantPackagingChargesLabel.translatesAutoresizingMaskIntoConstraints = false
        return restaurantPackagingChargesLabel
    }()
    
    private var restaurantGST : UILabel = {
        let restaurantGST = UILabel()
        restaurantGST.textColor = .black.withAlphaComponent(0.9)
        restaurantGST.backgroundColor = .white
        restaurantGST.font = UIFont.systemFont(ofSize: 14)
        restaurantGST.adjustsFontSizeToFitWidth = true
        restaurantGST.textAlignment = .left
        restaurantGST.translatesAutoresizingMaskIntoConstraints = false
        return restaurantGST
    }()
    private var restaurantPackagingCharges : UILabel = {
        let restaurantPackagingCharges = UILabel()
        restaurantPackagingCharges.textColor = .black.withAlphaComponent(0.9)
        restaurantPackagingCharges.backgroundColor = .white
        restaurantPackagingCharges.font = UIFont.systemFont(ofSize: 14)
        restaurantPackagingCharges.adjustsFontSizeToFitWidth = true
        restaurantPackagingCharges.textAlignment = .left
        restaurantPackagingCharges.translatesAutoresizingMaskIntoConstraints = false
        return restaurantPackagingCharges
    }()
    
    private var itemDiscountLabel : UILabel = {
        let itemDiscountLabel = UILabel()
        itemDiscountLabel.textColor = .systemGreen
        itemDiscountLabel.backgroundColor = .white
        itemDiscountLabel.font = UIFont.systemFont(ofSize: 14)
        itemDiscountLabel.adjustsFontSizeToFitWidth = true
        itemDiscountLabel.textAlignment = .left
        itemDiscountLabel.text = "Item Discount"
        itemDiscountLabel.translatesAutoresizingMaskIntoConstraints = false
        return itemDiscountLabel
    }()
    private var itemDiscount : UILabel = {
        let  itemDiscount = UILabel()
        itemDiscount.textColor = .systemGreen.withAlphaComponent(0.9)
        itemDiscount.backgroundColor = .white
        itemDiscount.font = UIFont.systemFont(ofSize: 14)
        itemDiscount.adjustsFontSizeToFitWidth = true
        itemDiscount.textAlignment = .left
        itemDiscount.translatesAutoresizingMaskIntoConstraints = false
        return itemDiscount
    }()
    
    private var toPayLabel : UILabel = {
        let toPayLabel = UILabel()
        toPayLabel.textColor = .black
        toPayLabel.backgroundColor = .white
        toPayLabel.font = UIFont(name:"ArialRoundedMTBold", size: 25.0)
        toPayLabel.adjustsFontSizeToFitWidth = true
        toPayLabel.textAlignment = .left
        toPayLabel.text = "To Pay"
        toPayLabel.translatesAutoresizingMaskIntoConstraints = false
        return toPayLabel
    }()
    
    private  var toPayAmount : UILabel = {
        let toPayAmount = UILabel()
        toPayAmount.textColor = .black
        toPayAmount.backgroundColor = .white
        toPayAmount.font = UIFont(name:"ArialRoundedMTBold", size: 25.0)
        toPayAmount.adjustsFontSizeToFitWidth = true
        toPayAmount.textAlignment = .left
        toPayAmount.translatesAutoresizingMaskIntoConstraints = false
        return toPayAmount
    }()
    var billstackView : UIStackView = {
        let billstackView = UIStackView()
        billstackView.axis = .vertical
        billstackView.distribution = .fillProportionally
        billstackView.spacing = 5
        billstackView.translatesAutoresizingMaskIntoConstraints = false
        return billstackView
    }()
    lazy var itemTotalLabelsStackView : UIStackView = {
        let itemTotalLabelsStackView = getHorizontalStackView()
        itemTotalLabelsStackView.addArrangedSubview(itemTotalLabel)
        itemTotalLabelsStackView.addArrangedSubview(itemTotal)
        
        return itemTotalLabelsStackView
    }()
    lazy var deliveryFeeLabelsStackView : UIStackView = {
        let deliveryFeeLabelsStackView = getHorizontalStackView()
        deliveryFeeLabelsStackView.addArrangedSubview(deliveryFeeLabel)
        deliveryFeeLabelsStackView.addArrangedSubview(deliveryFee)
        
        return deliveryFeeLabelsStackView
    }()
    lazy var restaurantGSTLabelsStackView : UIStackView = {
        let restaurantGSTLabelsStackView = getHorizontalStackView()
        restaurantGSTLabelsStackView.addArrangedSubview(restaurantGSTLabel)
        restaurantGSTLabelsStackView.addArrangedSubview(restaurantGST)
        
        return restaurantGSTLabelsStackView
    }()
    
    lazy var restaurantPackagingChargesLabelsStackView : UIStackView = {
        let restaurantPackagingChargesLabelsStackView = getHorizontalStackView()
        restaurantPackagingChargesLabelsStackView.addArrangedSubview(restaurantPackagingChargesLabel)
        restaurantPackagingChargesLabelsStackView.addArrangedSubview(restaurantPackagingCharges)
        
        return restaurantPackagingChargesLabelsStackView
    }()
    
    lazy var itemDiscountLabelsStackView : UIStackView = {
        let itemDiscountLabelsStackView = getHorizontalStackView()
        itemDiscountLabelsStackView.addArrangedSubview(itemDiscountLabel)
        itemDiscountLabelsStackView.addArrangedSubview(itemDiscount)
        
        return itemDiscountLabelsStackView
    }()
    
    lazy var toPayLabelsStackView : UIStackView = {
        let  toPayLabelsStackView = getHorizontalStackView()
        toPayLabelsStackView.addArrangedSubview(toPayLabel)
        toPayLabelsStackView.addArrangedSubview(toPayAmount)
        
        return toPayLabelsStackView
    }()
    
    
    var cancelOrderInstructionsLabel : UILabel = {
        let cancelOrderInstructionsLabel = UILabel()
        cancelOrderInstructionsLabel.textColor = .black
        cancelOrderInstructionsLabel.backgroundColor = .white
        cancelOrderInstructionsLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        cancelOrderInstructionsLabel.adjustsFontSizeToFitWidth = true
        cancelOrderInstructionsLabel.textAlignment = .left
        cancelOrderInstructionsLabel.text = "Review your order and address details to avoid cancellations"
        cancelOrderInstructionsLabel.translatesAutoresizingMaskIntoConstraints = false
        cancelOrderInstructionsLabel.numberOfLines = 5
        cancelOrderInstructionsLabel.lineBreakMode = .byWordWrapping
        cancelOrderInstructionsLabel.textAlignment = .natural
        cancelOrderInstructionsLabel.sizeToFit()
        return cancelOrderInstructionsLabel
        
    }()
    
    
    var cartBottomView : CartBottomViewProtocol = {
        let cartBottomView = cartBottomView()
        return cartBottomView
    }()
    
    func updateTotalItemsAndPriceNavigationBarLabelText(menuCount : Int,totalPrice : Int){
        if(menuCount == 1){
            totalItemsAndPriceNavigationBarLabel.text =
            " \(menuCount) Item | ₹\(totalPrice)"
        }
        else{
            totalItemsAndPriceNavigationBarLabel.text = " \(menuCount) Items | ₹\(totalPrice)"
        }
    }
    
    func addNavigationBarTitleView(restaurantName : String, menuCount : Int,totalPrice : Int){
        restaurantNameNavigationBarLabel.text =  restaurantName.uppercased()
        updateTotalItemsAndPriceNavigationBarLabelText(menuCount: menuCount, totalPrice: totalPrice)
        
    }
  
    
    private func getRestaurantProfileImageViewContraints() -> [NSLayoutConstraint]{
        let imageContraints = [
            restaurantProfileImageView.topAnchor.constraint(equalTo: self.topAnchor , constant: 10),
            restaurantProfileImageView.leftAnchor.constraint(equalTo: self.leftAnchor , constant: 10),
            restaurantProfileImageView.widthAnchor.constraint(equalToConstant: 60),
            restaurantProfileImageView.heightAnchor.constraint(equalToConstant:50)]
        return imageContraints
    }
    
    private func getRestaurantNameLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [restaurantNameLabel.topAnchor.constraint(equalTo: restaurantProfileImageView.topAnchor , constant: 5 ),
            restaurantNameLabel.leftAnchor.constraint(equalTo: restaurantProfileImageView.rightAnchor,constant: 5),
            restaurantNameLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
            restaurantNameLabel.heightAnchor.constraint(equalToConstant: 25)]
        return labelContraints
    }
    private func getRestaurantLocalityLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
            restaurantLocalityLabel.topAnchor.constraint(equalTo: restaurantNameLabel.bottomAnchor),
            restaurantLocalityLabel.leftAnchor.constraint(equalTo: restaurantProfileImageView.rightAnchor,constant: 5),
            restaurantLocalityLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
            restaurantLocalityLabel.heightAnchor.constraint(equalToConstant: 15)]
        return labelContraints
    }
    
    private func createRestaurantDetailsContents(){
        self.addSubview(restaurantNameLabel)
        self.addSubview(restaurantLocalityLabel)
        self.addSubview(restaurantProfileImageView)
        NSLayoutConstraint.activate(getRestaurantProfileImageViewContraints())
        NSLayoutConstraint.activate(getRestaurantNameLabelContraints())
        NSLayoutConstraint.activate(getRestaurantLocalityLabelContraints())
    }
    
    func updateRestaurantDetailsContents(restaurantProfileImage : Data,restaurantName : String , restaurantLocality: String){
        restaurantProfileImageView.image = UIImage(data: restaurantProfileImage)
        restaurantNameLabel.text = restaurantName
        restaurantLocalityLabel.text = restaurantLocality
    }
    
    var menuDetailsTableView : UITableView = {
        let menuDetailsTableView = UITableView()
        menuDetailsTableView.backgroundColor = .white
        menuDetailsTableView.isScrollEnabled = false
        menuDetailsTableView.separatorStyle = .none
        menuDetailsTableView.bounces = false
        menuDetailsTableView.register(CartMenuDetailsTableViewCell.self, forCellReuseIdentifier: CartMenuDetailsTableViewCell.cellIdentifier)
        menuDetailsTableView.translatesAutoresizingMaskIntoConstraints = false
        return menuDetailsTableView
    }()
    
    func getCartMenuCellHeight() -> CGFloat{
        return 60
    }
    
    
    
    func createMenuDetailsContentView(indexPath : IndexPath ,tableView : UITableView,menuCellContents : ( menuName : String, menuTarianType : String,  menuAvailable : Int, menuAvailableNextAt  : String,menuId : Int)) -> CartMenuDetailsTableViewCellProtocol{
        let cartMenuDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: CartMenuDetailsTableViewCell.cellIdentifier, for: indexPath) as? CartMenuDetailsTableViewCell
        cartMenuDetailsTableViewCell?.cellHeight = 60
        cartMenuDetailsTableViewCell?.configureContent(menuCellContents: menuCellContents)
        return cartMenuDetailsTableViewCell ?? CartMenuDetailsTableViewCell()
    }
    
   
    
    private func getMenuDetailsTableViewLayoutConstraints() -> [NSLayoutConstraint]{
            let tableViewContraints = [menuDetailsTableView.topAnchor.constraint(equalTo: self.topAnchor,constant: 70),
                                       menuDetailsTableView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                                       menuDetailsTableView.rightAnchor.constraint(equalTo: self.rightAnchor,constant: 10)]
            return tableViewContraints
    }
    
   
        
    
    private func getAddMoreMenusLayoutContrainst() -> [NSLayoutConstraint]{
        let buttonConstraints = [addMoreMenuButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),addMoreMenuButton.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),addMoreMenuButton.topAnchor.constraint(equalTo: menuDetailsTableView.bottomAnchor,constant: 5),addMoreMenuButton.heightAnchor.constraint(equalToConstant: 20)]
        return buttonConstraints
    }
    
    
    
    private func getInstructionsToRestaurantTextViewLayoutContrainst() -> [NSLayoutConstraint]{
        let textViewConstraints = [instructionsToRestaurantTextView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),instructionsToRestaurantTextView.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),instructionsToRestaurantTextView.topAnchor.constraint(equalTo: addMoreMenuButton.bottomAnchor,constant: 15),instructionsToRestaurantTextView.heightAnchor.constraint(equalToConstant: 40)]
        return textViewConstraints
    }
    
   
    
    
    
    
    
    private func getremoveCouponButtonLayoutContrainst() -> [NSLayoutConstraint]{
        let buttonConstraints = [removeCouponButton.widthAnchor.constraint(equalToConstant: 100),removeCouponButton.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),removeCouponButton.topAnchor.constraint(equalTo: instructionsToRestaurantTextView.bottomAnchor,constant: 15),removeCouponButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonConstraints
    }
    
    private func getapplyCouponButtonLayoutContrainst() -> [NSLayoutConstraint]{
        let buttonConstraints = [applyCouponButton.widthAnchor.constraint(equalToConstant: 100),applyCouponButton.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),applyCouponButton.topAnchor.constraint(equalTo: instructionsToRestaurantTextView.bottomAnchor,constant: 15),applyCouponButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonConstraints
    }
    
    private func getApplyCouponTextFieldLayoutContrainst() -> [NSLayoutConstraint]{
        let textFieldConstraints = [applyCouponTextField.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),applyCouponTextField.rightAnchor.constraint(equalTo: applyCouponButton.leftAnchor),applyCouponTextField.topAnchor.constraint(equalTo: instructionsToRestaurantTextView.bottomAnchor,constant: 15),applyCouponTextField.heightAnchor.constraint(equalToConstant: 40)]
        return textFieldConstraints
    }
    
    
    
    private func getCouponAppliedLabelLayoutContrainst() -> [NSLayoutConstraint]{
        let labelConstraints = [couponAppliedLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),couponAppliedLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),couponAppliedLabel.topAnchor.constraint(equalTo: applyCouponButton.bottomAnchor,constant: 10),couponAppliedLabel.heightAnchor.constraint(equalToConstant: 50)]
        return labelConstraints
    }
   
    
    
    
    
    private func getHorizontalStackView() -> UIStackView{
        let horizontalStackView = UIStackView()
        horizontalStackView.axis = .horizontal
        horizontalStackView.distribution = .fillProportionally
        return horizontalStackView
    }
    
    func createBillDetailsElements(){
        createBillStackView()
        applyBillDetailsLabelConstrainst()
        applyItemTotalLabelsConstrainst()
        applyDeliveryFeeLabelsConstrainst()
        applyRestaurantGSTLabelsConstrainst()
        applyRestaurantPackageingChargesLabelsConstrainst()
        applyItemDiscountLabelsConstrainst()
        billstackView.addArrangedSubview(createLineView(color: .systemGray5))
        applyToPayLabelsConstrainst()
        
    }
    
    private func applyItemTotalLabelsConstrainst(){
        billstackView.addArrangedSubview(itemTotalLabelsStackView)
        itemTotal.widthAnchor.constraint(equalToConstant: 80).isActive = true
        itemTotal.heightAnchor.constraint(equalToConstant: 20).isActive = true
        itemTotalLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
    }
    
    private func applyDeliveryFeeLabelsConstrainst(){
        billstackView.addArrangedSubview(deliveryFeeLabelsStackView)
        deliveryFee.widthAnchor.constraint(equalToConstant: 80).isActive = true
        deliveryFee.heightAnchor.constraint(equalToConstant: 20).isActive = true
        deliveryFeeLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func applyRestaurantGSTLabelsConstrainst(){
        billstackView.addArrangedSubview(restaurantGSTLabelsStackView)
        restaurantGST.widthAnchor.constraint(equalToConstant: 80).isActive = true
        restaurantGST.heightAnchor.constraint(equalToConstant: 20).isActive = true
        restaurantGSTLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func applyRestaurantPackageingChargesLabelsConstrainst(){
        billstackView.addArrangedSubview(restaurantPackagingChargesLabelsStackView)
        restaurantPackagingCharges.widthAnchor.constraint(equalToConstant: 80).isActive = true
        restaurantPackagingCharges.heightAnchor.constraint(equalToConstant: 20).isActive = true
        restaurantPackagingChargesLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func applyItemDiscountLabelsConstrainst(){
        billstackView.addArrangedSubview(itemDiscountLabelsStackView)
        itemDiscount.widthAnchor.constraint(equalToConstant: 90).isActive = true
        itemDiscount.heightAnchor.constraint(equalToConstant: 20).isActive = true
        itemDiscountLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
    }
    
    
    
    private func applyToPayLabelsConstrainst(){
        billstackView.addArrangedSubview(toPayLabelsStackView)
        toPayAmount.widthAnchor.constraint(equalToConstant: 80).isActive = true
        toPayAmount.heightAnchor.constraint(equalToConstant: 20).isActive = true
        toPayLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
   
    
    private func  getBillViewLayOutConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [billView.topAnchor.constraint(equalTo: applyCouponTextField.bottomAnchor,constant: 60),
                               billView.leftAnchor.constraint(equalTo: self.leftAnchor),
                               billView.rightAnchor.constraint(equalTo: self.rightAnchor),
                               billView.bottomAnchor.constraint(equalTo: applyCouponTextField.bottomAnchor,constant: 260)]
        return viewConstraints
    }
    
    
    func updateBillDetails(itemTotalPrice : Int,restaurantPackagingChargePrice : Int, deliveryFeePrice : Int , itemDiscountPrice : Int){
        let gst = RestaurantGST.getGSTCost(totalPrice: itemTotalPrice)
        updateItemDiscount(itemDiscountPrice :itemDiscountPrice)
        updateBillDetialsLabels(itemTotalPrice: itemTotalPrice, restaurantGSTPrice: gst, toPay: itemTotalPrice + Int(round(gst)) + deliveryFeePrice + restaurantPackagingChargePrice - itemDiscountPrice)
        setDeliveryAndPackageingFee(deliveryFeePrice: deliveryFeePrice, restaurantPackagingChargePrice: restaurantPackagingChargePrice)
    }
    
    func getTopayAmount(itemTotalPrice : Int,restaurantPackagingChargePrice : Int, deliveryFeePrice : Int , itemDiscountPrice : Int) -> Int{
        let gst = RestaurantGST.getGSTCost(totalPrice: itemTotalPrice)
        return itemTotalPrice + Int(round(gst)) + deliveryFeePrice + restaurantPackagingChargePrice - itemDiscountPrice
    }
    
   
    private func updateItemDiscount(itemDiscountPrice :Int){
        if(itemDiscountPrice != 0){
            itemDiscountLabelsStackView.isHidden = false
            itemDiscount.text = "- ₹ \(itemDiscountPrice)"
        }
        else{
            itemDiscount.text = "- ₹ \(itemDiscountPrice)"
            itemDiscountLabelsStackView.isHidden = true
        }
    }
    
    func updateCouponAppliedLabel(couponCode : String , itemDiscountPrice : Int){
        couponAppliedLabel.isHidden = false
        if(couponCode != "" &&  itemDiscountPrice != 0){
            couponAppliedLabel.text = "The Coupon Code \("\(couponCode)") is applied and ₹ \(itemDiscountPrice) is saved on your bill"
            couponAppliedLabel.textColor = .systemGreen.withAlphaComponent(0.9)
        }
        else if(couponCode != ""){
            couponAppliedLabel.text = "The Coupon Code \("\(couponCode)") is not valid "
            couponAppliedLabel.textColor = .systemRed.withAlphaComponent(0.9)
        }
        else{
            couponAppliedLabel.text = ""
            couponAppliedLabel.isHidden = true
        }
    }
    
    private func updateBillDetialsLabels(itemTotalPrice : Int,restaurantGSTPrice : Double,toPay : Int){
        itemTotal.text = "₹ \(itemTotalPrice)"
        restaurantGST.text =  "₹ \(restaurantGSTPrice)"
        toPayAmount.text = "₹ \(toPay)"
        cartBottomView.totalAmountToBePaid.text = "₹ \(toPay)"
    }
    
    private func setDeliveryAndPackageingFee(deliveryFeePrice : Int,restaurantPackagingChargePrice : Int){
        deliveryFee.text = "₹ \(deliveryFeePrice)"
        restaurantPackagingCharges.text = "₹ \(restaurantPackagingChargePrice)"
    }
    
    private func createBillStackView(){
        
        billView.addSubview(billstackView)
        NSLayoutConstraint.activate(getBillStackViewLayOutConstraints())
    }
    
    private func getBillStackViewLayOutConstraints() -> [NSLayoutConstraint]{
        let stackConstraint = [billstackView.topAnchor.constraint(equalTo: billView.topAnchor,constant: 20),
                               billstackView.leftAnchor.constraint(equalTo: billView.leftAnchor,constant: 10),
                               billstackView.rightAnchor.constraint(equalTo: billView.rightAnchor,constant: -10),
                            
        ]
        return stackConstraint
    }
    
    
    
    
    private func applyBillDetailsLabelConstrainst(){
        billDetailsLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        billstackView.addArrangedSubview(billDetailsLabel)
       
    }
    
    
    
    private func createLineView(color : UIColor) -> UIView{
        let lineView = UIView()
        lineView.translatesAutoresizingMaskIntoConstraints = false
        lineView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        lineView.backgroundColor = color
        return lineView
    }
        
    private func getCancelOrderInstructionsLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            cancelOrderInstructionsLabel.topAnchor.constraint(equalTo: billView.bottomAnchor,constant: 50),
            cancelOrderInstructionsLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            cancelOrderInstructionsLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            cancelOrderInstructionsLabel.heightAnchor.constraint(equalToConstant: 60)]
        return labelContraints
    }
    
    @objc func didTapView(){
        delegate?.didTapView()
    }
    
    @objc func   didTapRemoveCoupon(){
        delegate?.didTapRemoveCoupon()
    }
    
    @objc func didTapApplyCouponCode(){
        delegate?.didTapApplyCouponCode()
    }
    
    @objc func didTapAddMoreMenu(){
        delegate?.didTapAddMoreMenu()
    }
    

}




class UserCartScrollView : UIScrollView , UserCartScrollViewProtocol{
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
       
        self.keyboardDismissMode = .onDrag
        self.isScrollEnabled = true
        self.bounces = false
        self.keyboardDismissMode = .interactive
        initialiseViewElements()
    }
    
    private func initialiseViewElements(){
        self.addSubview(emptyCartLabel)
        NSLayoutConstraint.activate(getEmptyCartLabelLayoutConstraints())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var emptyCartLabel : UILabel = {
        let  emptyCartLabel = UILabel()
        emptyCartLabel.translatesAutoresizingMaskIntoConstraints = false
        emptyCartLabel.text = "Your cart is empty. Add something from the menu"
        emptyCartLabel.backgroundColor = .white
        emptyCartLabel.textColor = .systemGray
        emptyCartLabel.numberOfLines = 5
        emptyCartLabel.lineBreakMode = .byWordWrapping
        emptyCartLabel.sizeToFit()
        emptyCartLabel.textAlignment = .center
        emptyCartLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return emptyCartLabel
    }()
    
    private func getEmptyCartLabelLayoutConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            emptyCartLabel.topAnchor.constraint(equalTo: self.centerYAnchor),
            emptyCartLabel.leftAnchor.constraint(equalTo: self.leftAnchor),
            emptyCartLabel.rightAnchor.constraint(equalTo: self.rightAnchor)]
        return labelContraints
    }
}


class cartBottomView : UIView ,CartBottomViewProtocol{
    
    weak var delegate : cartBottomViewDelegate?
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        self.addSubview(deliveryAddressView)
        deliveryAddressView.addSubview(addressLabel)
        deliveryAddressView.addSubview(addressTagLabel)
        deliveryAddressView.addSubview(addressTagIconimageView)
        deliveryAddressView.addSubview(changeAddressButton)
        self.addSubview(paymentModeLabel)
        self.addSubview(totalAmountToBePaid)
        self.addSubview(placeOrderButton)
        self.addSubview(loginButton)
        self.addSubview(menuItemUnavailableLabel)
        self.addSubview(notDeliverableLabel)
        self.addSubview(removeUnavailableItemsButton)
        self.addSubview(browseRestaurantsButton)
        self.addSubview(restaurantUnavailableLabel)
        NSLayoutConstraint.activate(getDeliveryAddressViewLayoutConstraints())
        
        NSLayoutConstraint.activate(getAddressCellLabelContraints())
        
        NSLayoutConstraint.activate(getAddressTagLabelContraints())
        
        NSLayoutConstraint.activate(getAddressTagIconimageViewContraints())
       
        NSLayoutConstraint.activate(getChangeAddressButtonContraints())
        
        NSLayoutConstraint.activate(getPaymentModeLabelLayoutConstraints())
       
        NSLayoutConstraint.activate(getTotalAmountToBePaidLayoutConstraints())
        
        NSLayoutConstraint.activate(getPlaceOrderButtonLayoutConstraints())
        
        NSLayoutConstraint.activate(getLoginButtonLayoutConstraints())
        
        NSLayoutConstraint.activate(getMenuItemUnavailableLabelLayoutConstraints())
        
        NSLayoutConstraint.activate(getnotDeliverableLabelButtonLayoutConstraints())
        
        NSLayoutConstraint.activate(getRemoveUnavailableItemsButtonLayoutConstraints())
        
        NSLayoutConstraint.activate(getBrowseRestaurantsButtonLayoutConstraints())
        
        NSLayoutConstraint.activate(getRestaurantUnavailableLabelLayoutConstraints())
    }
    
    
    
    
    var totalAmountToBePaid : UILabel = {
        let totalAmountToBePaid = UILabel()
        totalAmountToBePaid.translatesAutoresizingMaskIntoConstraints = false
        totalAmountToBePaid.backgroundColor = .white
        totalAmountToBePaid.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        totalAmountToBePaid.textAlignment = .center
        return totalAmountToBePaid
    }()
    
    lazy var placeOrderButton : UIButton = {
        let placeOrderButton = UIButton()
        placeOrderButton.translatesAutoresizingMaskIntoConstraints = false
        placeOrderButton.backgroundColor = .systemGreen
        placeOrderButton.layer.cornerRadius = 5
        placeOrderButton.setTitle("Place Order", for: .normal)
        placeOrderButton.setTitleColor(.white, for: .normal)
        placeOrderButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        placeOrderButton.addTarget(self, action: #selector(didTapPlaceOrderButton), for: .touchUpInside)
        return placeOrderButton
    }()
    var paymentModeLabel : UILabel = {
        let paymentModeLabel = UILabel()
        paymentModeLabel.translatesAutoresizingMaskIntoConstraints = false
        paymentModeLabel.backgroundColor = .white
        paymentModeLabel.font = UIFont(name: "ArialRoundedMTBold", size: 16)
        paymentModeLabel.textAlignment = .center
        paymentModeLabel.numberOfLines = 2
        paymentModeLabel.lineBreakMode = .byWordWrapping
        return paymentModeLabel
    }()
    var deliveryAddressView : UIView = {
        let deliveryAddressView = UIView()
        deliveryAddressView.translatesAutoresizingMaskIntoConstraints = false
        deliveryAddressView.backgroundColor = .white
        return deliveryAddressView
    }()
    
    
    
    var addressLabel : UILabel = {
        let addressLabel = UILabel()
        addressLabel.textColor = .black
        addressLabel.translatesAutoresizingMaskIntoConstraints = false
        addressLabel.backgroundColor = .white
        addressLabel.adjustsFontSizeToFitWidth = true
        addressLabel.numberOfLines = 4
        return addressLabel
    }()
    
    
    var addressTagLabel : UILabel = {
        let addressTagLabel = UILabel()
        addressTagLabel.textColor = .black
        addressTagLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        addressTagLabel.backgroundColor = .white
        addressTagLabel.translatesAutoresizingMaskIntoConstraints = false
        return addressTagLabel
    }()
    var addressTagIconimageView : UIImageView = {
        let addressTagIconimageView = UIImageView()
        addressTagIconimageView.backgroundColor = .white
        addressTagIconimageView.translatesAutoresizingMaskIntoConstraints = false
        return addressTagIconimageView
    }()
    
    lazy var loginButton : UIButton = {
        let loginButton = UIButton()
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.backgroundColor = .systemGreen
        loginButton.setTitle("login", for: .normal)
        loginButton.setTitleColor(.white, for: .normal)
        loginButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        loginButton.layer.cornerRadius = 5
        loginButton.addTarget(self, action: #selector(didTapLoginButton), for: .touchUpInside)
        return loginButton
    }()
    
    var menuItemUnavailableLabel : UILabel = {
        let menuItemUnavailableLabel = UILabel()
        menuItemUnavailableLabel.translatesAutoresizingMaskIntoConstraints = false
        menuItemUnavailableLabel.text = "Some of the menu items in the cart are not available"
        menuItemUnavailableLabel.backgroundColor = .white
        menuItemUnavailableLabel.textColor = .black
        menuItemUnavailableLabel.numberOfLines = 2
        menuItemUnavailableLabel.lineBreakMode = .byWordWrapping
        menuItemUnavailableLabel.textAlignment = .center
        menuItemUnavailableLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return menuItemUnavailableLabel
    }()
    
    lazy var removeUnavailableItemsButton : UIButton = {
        let removeUnavailableItemsButton = UIButton()
        removeUnavailableItemsButton.translatesAutoresizingMaskIntoConstraints = false
        removeUnavailableItemsButton.backgroundColor = .systemGreen
        removeUnavailableItemsButton.setTitle("Remove Unavailable Items", for: .normal)
        removeUnavailableItemsButton.setTitleColor(.white, for: .normal)
        removeUnavailableItemsButton.layer.cornerRadius = 5
        removeUnavailableItemsButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        removeUnavailableItemsButton.addTarget(self, action: #selector(didTapRemoveUnAvailableItems), for: .touchUpInside)
        return removeUnavailableItemsButton
    }()
    
     var restaurantUnavailableLabel : UILabel = {
        let restaurantUnavailableLabel = UILabel()
        restaurantUnavailableLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantUnavailableLabel.text = "Restaurant in closed Now, Please browse other restaurants"
        restaurantUnavailableLabel.backgroundColor = .white
        restaurantUnavailableLabel.textColor = .black
        restaurantUnavailableLabel.numberOfLines = 5
        restaurantUnavailableLabel.lineBreakMode = .byWordWrapping
        restaurantUnavailableLabel.textAlignment = .center
        restaurantUnavailableLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return restaurantUnavailableLabel
    }()
    lazy var browseRestaurantsButton : UIButton = {
        let browseRestaurantsButton = UIButton()
        browseRestaurantsButton.translatesAutoresizingMaskIntoConstraints = false
        browseRestaurantsButton.backgroundColor = .systemGreen
        browseRestaurantsButton.setTitle("Browse Restaurants", for: .normal)
        browseRestaurantsButton.setTitleColor(.white, for: .normal)
        browseRestaurantsButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        browseRestaurantsButton.layer.cornerRadius = 5
        browseRestaurantsButton.addTarget(self, action: #selector(didTabBrowseRestaurant), for: .touchUpInside)
        return browseRestaurantsButton
    }()
    lazy var changeAddressButton : UIButton = {
        let changeAddressButton = UIButton()
        changeAddressButton.translatesAutoresizingMaskIntoConstraints = false
        changeAddressButton.backgroundColor = .white
        changeAddressButton.layer.cornerRadius = 5
        changeAddressButton.setTitle("Change", for: .normal)
        changeAddressButton.setTitleColor(.systemBlue, for: .normal)
        changeAddressButton.titleLabel?.font = UIFont(name: "ArialRoundedMTBold", size: 18)
        changeAddressButton.addTarget(self, action: #selector(didTapChangeAddress), for: .touchUpInside)
        return changeAddressButton
    }()
    
    
     var notDeliverableLabel : UILabel = {
        let notDeliverableLabel = UILabel()
        notDeliverableLabel.translatesAutoresizingMaskIntoConstraints = false
        notDeliverableLabel.text = "Cannot be Delivered to this address"
        notDeliverableLabel.backgroundColor = .white
        notDeliverableLabel.textColor = .systemRed.withAlphaComponent(0.9)
        notDeliverableLabel.numberOfLines = 2
        notDeliverableLabel.lineBreakMode = .byWordWrapping
        notDeliverableLabel.textAlignment = .center
        notDeliverableLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return notDeliverableLabel
    }()
    
    
    func updatePaymentMode(paymentMode : String){
        paymentModeLabel.text = "Pay Using : \(paymentMode)"
    }
    
    
   
    func updateAddressDetailsInLabel(addressTag : String , address : AddressDetails){
        addressLabel.text = "\(address.doorNoAndBuildingNameAndBuildingNo), \(address.streetName), \(address.landmark), \(address.localityName), \(address.city), \(address.state), \(address.country), \(address.pincode)"
        addressTagLabel.text = "Deliver to " + addressTag
        if(addressTag == "Home"){
            addressTagIconimageView.image = UIImage(systemName: "house")?.withTintColor(.black, renderingMode: .alwaysOriginal)
        }
        else if(addressTag == "Work"){
            addressTagIconimageView.image = UIImage(systemName: "bag")?.withTintColor(.black, renderingMode: .alwaysOriginal)
        }
        else{
            addressTagIconimageView.image = UIImage(systemName: "location")?.withTintColor(.black, renderingMode: .alwaysOriginal)
        }
    }
    
  
    
    private func getAddressTagLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [addressTagLabel.topAnchor.constraint(equalTo: deliveryAddressView.topAnchor),
                               addressTagLabel.leftAnchor.constraint(equalTo: addressTagIconimageView.rightAnchor,constant: 5),
                               addressTagLabel.rightAnchor.constraint(equalTo: deliveryAddressView.centerXAnchor,constant: 50),
                               addressTagLabel.heightAnchor.constraint(equalToConstant: 20)]
        return labelContraints
    }
    
    private func getAddressTagIconimageViewContraints() -> [NSLayoutConstraint]{
        let labelContraints = [addressTagIconimageView.topAnchor.constraint(equalTo: deliveryAddressView.topAnchor),
                               addressTagIconimageView.leftAnchor.constraint(equalTo: deliveryAddressView.leftAnchor,constant: 5),
                               addressTagIconimageView.widthAnchor.constraint(equalToConstant: 20),
                               addressTagIconimageView.heightAnchor.constraint(equalToConstant: 20)]
        return labelContraints
    }
    
    private func getAddressCellLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            addressLabel.topAnchor.constraint(equalTo: addressTagLabel.bottomAnchor),
            addressLabel.leftAnchor.constraint(equalTo: addressTagIconimageView.rightAnchor,constant: 5),
            addressLabel.rightAnchor.constraint(equalTo: deliveryAddressView.rightAnchor),
            addressLabel.heightAnchor.constraint(equalToConstant: 50)]
        return labelContraints
    }
    
    private func getChangeAddressButtonContraints() -> [NSLayoutConstraint]{
        let labelContraints = [changeAddressButton.topAnchor.constraint(equalTo: deliveryAddressView.topAnchor),
                               changeAddressButton.leftAnchor.constraint(equalTo: deliveryAddressView.centerXAnchor,constant: 50),
                               changeAddressButton.rightAnchor.constraint(equalTo: deliveryAddressView.rightAnchor),
                               changeAddressButton.heightAnchor.constraint(equalToConstant: 20)]
        return labelContraints
    }
    
    private func getDeliveryAddressViewLayoutConstraints() -> [NSLayoutConstraint]{
        let viewContraints = [
            deliveryAddressView.topAnchor.constraint(equalTo: self.topAnchor),
            deliveryAddressView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            deliveryAddressView.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            deliveryAddressView.heightAnchor.constraint(equalToConstant: 70)]
        return viewContraints
    }
    
    
    
    private func getPaymentModeLabelLayoutConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            paymentModeLabel.topAnchor.constraint(equalTo: deliveryAddressView.bottomAnchor,constant: 5),
            paymentModeLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            paymentModeLabel.rightAnchor.constraint(equalTo: self.leftAnchor,constant: 100),
            paymentModeLabel.heightAnchor.constraint(equalToConstant: 40)]
        return labelContraints
    }
    
   
    
    private func getLoginButtonLayoutConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [
            loginButton.topAnchor.constraint(equalTo: deliveryAddressView.bottomAnchor,constant: 5),
            loginButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            loginButton.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            loginButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    private func getTotalAmountToBePaidLayoutConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            totalAmountToBePaid.topAnchor.constraint(equalTo: deliveryAddressView.bottomAnchor,constant: 5),
            totalAmountToBePaid.leftAnchor.constraint(equalTo: paymentModeLabel.rightAnchor),
            totalAmountToBePaid.rightAnchor.constraint(equalTo: self.centerXAnchor),
            totalAmountToBePaid.heightAnchor.constraint(equalToConstant: 40)]
        return labelContraints
    }
    
    private func getPlaceOrderButtonLayoutConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [
            placeOrderButton.topAnchor.constraint(equalTo: deliveryAddressView.bottomAnchor,constant: 5),
            placeOrderButton.leftAnchor.constraint(equalTo: self.centerXAnchor),
            placeOrderButton.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            placeOrderButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    private func getnotDeliverableLabelButtonLayoutConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [
            notDeliverableLabel.heightAnchor.constraint(equalToConstant: 50),
            notDeliverableLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            notDeliverableLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            notDeliverableLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor)]
        return buttonContraints
    }
    
    private func getMenuItemUnavailableLabelLayoutConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            menuItemUnavailableLabel.topAnchor.constraint(equalTo: self.topAnchor),
            menuItemUnavailableLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            menuItemUnavailableLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            menuItemUnavailableLabel.heightAnchor.constraint(equalToConstant: 70)]
        return labelContraints
    }
    
    
    private func getRemoveUnavailableItemsButtonLayoutConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [
            removeUnavailableItemsButton.topAnchor.constraint(equalTo: menuItemUnavailableLabel.bottomAnchor,constant: 5),
            removeUnavailableItemsButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            removeUnavailableItemsButton.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            removeUnavailableItemsButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    
    
    private func getBrowseRestaurantsButtonLayoutConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [
            browseRestaurantsButton.topAnchor.constraint(equalTo: restaurantUnavailableLabel.bottomAnchor,constant: 5),
            browseRestaurantsButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            browseRestaurantsButton.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            browseRestaurantsButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    
    
    private func getRestaurantUnavailableLabelLayoutConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            restaurantUnavailableLabel.topAnchor.constraint(equalTo: self.topAnchor),
            restaurantUnavailableLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            restaurantUnavailableLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            restaurantUnavailableLabel.heightAnchor.constraint(equalToConstant: 70)]
        return labelContraints
    }
    
    @objc func didTapChangeAddress(){
        delegate?.didTapChangeAddress()
    }
    
    @objc func didTabBrowseRestaurant(){
        delegate?.didTabBrowseRestaurant()
    }
    
    @objc func didTapLoginButton(){
        delegate?.didTapLoginButton()
    }
    
    @objc func didTapRemoveUnAvailableItems(){
        delegate?.didTapRemoveUnAvailableItems()
    }
    
    
    @objc func didTapPlaceOrderButton(){
        delegate?.didTapPlaceOrderButton()
    }
    
}


protocol cartBottomViewDelegate : AnyObject{
    func didTapChangeAddress()
    func didTabBrowseRestaurant()
    func didTapLoginButton()
    func didTapRemoveUnAvailableItems()
     func didTapPlaceOrderButton()
}


protocol UserCartViewDelegate : AnyObject{
    func didTapAddMoreMenu()
    func didTapApplyCouponCode()
    func didTapRemoveCoupon()
    func didTapView()
}
