//
//  UserCartMenuDetailsTableView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 03/03/22.
//

import Foundation
import UIKit



class CartMenuDetailsTableViewCell : UITableViewCell,CartMenuDetailsTableViewCellProtocol{
    static let cellIdentifier = "CartMenuDetailsTableViewCell"
    
    var cellHeight : CGFloat = 44
    
    weak open var delegate : CartMenuDetailsTableViewCellDelegate?
    
    lazy var menuDetailsContentView : UIView = {
        let menuDetailsContentView = UIView()
        menuDetailsContentView.backgroundColor = .white
        return menuDetailsContentView
    }()
   
    private lazy  var menuNameCellLabel : UILabel = {
        let  menuNameCellLabel = UILabel()
        menuNameCellLabel.backgroundColor = .white
        menuNameCellLabel.translatesAutoresizingMaskIntoConstraints = false
        menuNameCellLabel.textColor = .black
        menuNameCellLabel.textAlignment = .left
        menuNameCellLabel.numberOfLines = 2
        menuNameCellLabel.contentMode = .center
        menuNameCellLabel.lineBreakMode = .byWordWrapping
        menuNameCellLabel.sizeToFit()
        return menuNameCellLabel
    }()
    
    lazy var menuSubTotalPriceCellLabel : UILabel = {
        let menuSubTotalPriceCellLabel = UILabel()
        menuSubTotalPriceCellLabel.backgroundColor = .white
        menuSubTotalPriceCellLabel.translatesAutoresizingMaskIntoConstraints = false
        menuSubTotalPriceCellLabel.text = "₹ 0"
        menuSubTotalPriceCellLabel.textColor = .black.withAlphaComponent(0.7)
        menuSubTotalPriceCellLabel.adjustsFontSizeToFitWidth = true
        menuSubTotalPriceCellLabel.textAlignment = .left
        return menuSubTotalPriceCellLabel
    }()
    private lazy var menuTarianTypeCellImageView : UIImageView = {
        let menuTarianTypeCellImageView = UIImageView()
        menuTarianTypeCellImageView.translatesAutoresizingMaskIntoConstraints = false
        return menuTarianTypeCellImageView
    }()
    
    lazy var plusButton : UIButton = {
        let plusButton = UIButton()
        plusButton.setImage(UIImage(systemName: "plus")?.withTintColor(.systemGreen , renderingMode: .alwaysOriginal), for: .normal)
        plusButton.backgroundColor = .white
        plusButton.layer.borderWidth = 1
        plusButton.layer.borderColor = UIColor.white.cgColor
        plusButton.layer.cornerRadius = 5
        plusButton.translatesAutoresizingMaskIntoConstraints = false
        plusButton.addTarget(self, action: #selector(didTapPlusButton(sender:)), for: .touchUpInside)
        return plusButton
    }()
    lazy var minusButton : UIButton = {
        let minusButton = UIButton()
        minusButton.setImage(UIImage(systemName: "minus")?.withTintColor(.systemGray , renderingMode: .alwaysOriginal), for: .normal)
        minusButton.backgroundColor = .white
        minusButton.layer.borderWidth = 1
        minusButton.layer.borderColor = UIColor.white.cgColor
        minusButton.layer.cornerRadius = 5
        minusButton.translatesAutoresizingMaskIntoConstraints = false
        minusButton.addTarget(self, action: #selector(didTapMinusButton(sender:)), for: .touchUpInside)
        return minusButton
    }()
    
    lazy var menuCountLabel : UILabel = {
        let menuCountLabel = UILabel()
        menuCountLabel.backgroundColor = .white
        menuCountLabel.translatesAutoresizingMaskIntoConstraints = false
        menuCountLabel.text = "0"
        menuCountLabel.textColor = .systemGreen
        menuCountLabel.textAlignment = .center
        return menuCountLabel
    }()
    lazy var addMenuButtonView : UIView = {
        let addMenuButtonView = UIView()
        addMenuButtonView.backgroundColor = .white
        addMenuButtonView.translatesAutoresizingMaskIntoConstraints = false
        addMenuButtonView.layer.borderWidth = 1
        addMenuButtonView.layer.borderColor = UIColor.systemGray5.cgColor
        addMenuButtonView.layer.cornerRadius = 5
        return addMenuButtonView
    }()
    private lazy var menuNextAvailableAtCellLabel : UILabel = {
        let menuNextAvailableAtCellLabel = UILabel()
        menuNextAvailableAtCellLabel.translatesAutoresizingMaskIntoConstraints = false
        
        menuNextAvailableAtCellLabel.backgroundColor = .white
        menuNextAvailableAtCellLabel.textColor = .black.withAlphaComponent(0.7)
        menuNextAvailableAtCellLabel.layer.borderColor = UIColor.systemGray5.cgColor
        menuNextAvailableAtCellLabel.layer.borderWidth = 1
        menuNextAvailableAtCellLabel.layer.cornerRadius = 5
        menuNextAvailableAtCellLabel.numberOfLines = 3
        menuNextAvailableAtCellLabel.lineBreakMode = .byWordWrapping
        menuNextAvailableAtCellLabel.font = UIFont(name:"ArialRoundedMTBold", size: 8.0)
        menuNextAvailableAtCellLabel.sizeToFit()
        return menuNextAvailableAtCellLabel
    }()
    
    
   
    private func modifyMenuTarianTypeSymbolColor(menuTarianType : String){
        if(menuTarianType == menuTarianTypeEnum.veg.rawValue ){
            menuTarianTypeCellImageView.image = UIImage(systemName: "dot.square")?.withTintColor(.systemGreen, renderingMode: .alwaysOriginal)
        }
        else if(menuTarianType == menuTarianTypeEnum.nonVeg.rawValue){
            menuTarianTypeCellImageView.image = UIImage(systemName: "arrowtriangle.up.square")?.withTintColor(.red , renderingMode: .alwaysOriginal)
        }
        else{
            menuTarianTypeCellImageView.image = UIImage(systemName: "arrowtriangle.up.square")?.withTintColor(.systemYellow , renderingMode: .alwaysOriginal)
        }
    }


   
    
    private func modifyMenuBasedOnTheAvailability(menuAvailable : Int){
        if(menuAvailable == 1){
            menuNextAvailableAtCellLabel.isHidden = true
            addMenuButtonView.isHidden = false
        }
        else{
            menuNextAvailableAtCellLabel.isHidden = false
            addMenuButtonView.isHidden = true
        }
    }
    
    private func getMenuTarianTypeImageViewConstraint() -> [NSLayoutConstraint]{
        let imageViewContraints = [menuTarianTypeCellImageView.topAnchor.constraint(equalTo: menuDetailsContentView.topAnchor,constant: 20),
                                   menuTarianTypeCellImageView.leftAnchor.constraint(equalTo: menuDetailsContentView.leftAnchor,constant: 10),
                                   menuTarianTypeCellImageView.widthAnchor.constraint(equalToConstant: 20),
                                   menuTarianTypeCellImageView.heightAnchor.constraint(equalToConstant: 20)]
        return imageViewContraints
    }

    private func getMenuNameCellLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            menuNameCellLabel.topAnchor.constraint(equalTo: menuDetailsContentView.topAnchor),
            menuNameCellLabel.rightAnchor.constraint(equalTo: addMenuButtonView.leftAnchor,constant: -5),
            menuNameCellLabel.leftAnchor.constraint(equalTo: menuTarianTypeCellImageView.rightAnchor, constant: 5),
            menuNameCellLabel.heightAnchor.constraint(equalToConstant:60)]
        return labelContraints
    }

    private func getMenuSubTotalPriceCellLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            menuSubTotalPriceCellLabel.topAnchor.constraint(equalTo: menuDetailsContentView.topAnchor , constant: 10),
            menuSubTotalPriceCellLabel.widthAnchor.constraint(equalToConstant: 80),
            menuSubTotalPriceCellLabel.rightAnchor.constraint(equalTo: menuDetailsContentView.rightAnchor, constant: -10),
            menuSubTotalPriceCellLabel.heightAnchor.constraint(equalToConstant:30)]
        return labelContraints
    }

   


    
    
    private func getAddmenuButtonViewConstraints() -> [NSLayoutConstraint]{
        let constraints = [addMenuButtonView.topAnchor.constraint(equalTo: menuDetailsContentView.topAnchor,constant: 10),addMenuButtonView.widthAnchor.constraint(equalToConstant: 70),addMenuButtonView.rightAnchor.constraint(equalTo: menuSubTotalPriceCellLabel.leftAnchor,constant: -10),
                           addMenuButtonView.heightAnchor.constraint(equalToConstant: 30)]
        return constraints
    }
    
    
    private func getAddMenuButtonSubViewConstraints() -> [NSLayoutConstraint]{
        let constraints = [plusButton.topAnchor.constraint(equalTo: addMenuButtonView.topAnchor),
                           plusButton.bottomAnchor.constraint(equalTo: addMenuButtonView.bottomAnchor),
                           plusButton.rightAnchor.constraint(equalTo: addMenuButtonView.rightAnchor),
                           plusButton.widthAnchor.constraint(equalToConstant: 20),
                           menuCountLabel.bottomAnchor.constraint(equalTo: addMenuButtonView.bottomAnchor),
                           menuCountLabel.topAnchor.constraint(equalTo: addMenuButtonView.topAnchor),
                           menuCountLabel.rightAnchor.constraint(equalTo: plusButton.leftAnchor),
                           menuCountLabel.widthAnchor.constraint(equalToConstant: 30),
                           minusButton.topAnchor.constraint(equalTo: addMenuButtonView.topAnchor),
                           minusButton.bottomAnchor.constraint(equalTo: addMenuButtonView.bottomAnchor),
                           minusButton.widthAnchor.constraint(equalToConstant: 20),
                           minusButton.rightAnchor.constraint(equalTo: menuCountLabel.leftAnchor)]
        return constraints
    }
    
    private func getMenuNextAvailableAtCellLabelConstraint() -> [NSLayoutConstraint]{
        let constraints = [menuNextAvailableAtCellLabel.topAnchor.constraint(equalTo: menuDetailsContentView.topAnchor,constant: 10),menuNextAvailableAtCellLabel.widthAnchor.constraint(equalToConstant: 70),menuNextAvailableAtCellLabel.rightAnchor.constraint(equalTo: menuSubTotalPriceCellLabel.leftAnchor,constant: -5),
                           menuNextAvailableAtCellLabel.heightAnchor.constraint(equalToConstant: 30)]
                               return constraints
    }
    

    private func initialiseViewElements(){
        menuDetailsContentView.addSubview(menuNameCellLabel)
        menuDetailsContentView.addSubview(menuSubTotalPriceCellLabel)
        menuDetailsContentView.addSubview(menuTarianTypeCellImageView)
        menuDetailsContentView.addSubview(addMenuButtonView)
        menuDetailsContentView.addSubview(menuNextAvailableAtCellLabel)
        addMenuButtonView.addSubview(plusButton)
        addMenuButtonView.addSubview(minusButton)
        addMenuButtonView.addSubview(menuCountLabel)
        NSLayoutConstraint.activate(getMenuTarianTypeImageViewConstraint())
        NSLayoutConstraint.activate(getMenuNameCellLabelConstraints())
        NSLayoutConstraint.activate(getMenuSubTotalPriceCellLabelConstraints())
        NSLayoutConstraint.activate(getAddmenuButtonViewConstraints())
        NSLayoutConstraint.activate(getAddMenuButtonSubViewConstraints())
        NSLayoutConstraint.activate(getMenuNextAvailableAtCellLabelConstraint())
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(menuDetailsContentView)
        initialiseViewElements()
    }
    
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        menuDetailsContentView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: 60)
        
    }
    
    func configureContent(menuCellContents : ( menuName : String, menuTarianType : String,  menuAvailable : Int, menuAvailableNextAt  : String,menuId : Int)){
        
        plusButton.tag = menuCellContents.menuId
        minusButton.tag = menuCellContents.menuId
        menuCountLabel.tag = menuCellContents.menuId
        addMenuButtonView.tag = menuCellContents.menuId
        menuNameCellLabel.text = menuCellContents.menuName
        modifyMenuTarianTypeSymbolColor(menuTarianType: menuCellContents.menuTarianType)
    
        menuNextAvailableAtCellLabel.text =
                """
                Next Available At
                \(menuCellContents.menuAvailableNextAt)
                """
       
        modifyMenuBasedOnTheAvailability(menuAvailable: menuCellContents.menuAvailable)
        
        
    }
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        menuNameCellLabel.text = nil
        menuSubTotalPriceCellLabel.text = nil
        menuTarianTypeCellImageView.image = nil
        menuNextAvailableAtCellLabel.text = nil
        plusButton.tag = 0x0
        minusButton.tag = 0x0
        menuCountLabel.tag = 0x0
        addMenuButtonView.tag = 0x0
        
    }
    
    
    
    @objc func didTapPlusButton(sender : UIButton){
        delegate?.didTapPlusButton(sender: sender)
    }
    
    @objc func didTapMinusButton(sender : UIButton){
        delegate?.didTapMinusButton(sender: sender)
    }

}


protocol CartMenuDetailsTableViewCellDelegate : AnyObject{
    
    
    
     func didTapPlusButton(sender : UIButton)
    
    
     func didTapMinusButton(sender : UIButton)
    
}

