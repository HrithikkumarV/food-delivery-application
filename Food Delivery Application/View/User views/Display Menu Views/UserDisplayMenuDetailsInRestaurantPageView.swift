//
//  UserDisplayMenuDetailsPageView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 04/02/22.
//

import Foundation
import UIKit

class UserDisplayMenuDetailsInRestaurantPageView : UIView ,UserDisplayMenuDetailsPageViewProtocol{
    
    weak var delegate  :UserDisplayMenuDetailsInRestaurantPageViewDelegate?
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func initialiseViewElements(){
        self.addSubview(restaurantNameLabel)
        self.addSubview(restaurantCuisineLabel)
        self.addSubview(restaurantDescriptionLabel)
        self.addSubview(restaurantAddressLabel)
        self.addSubview(restaurantStarRatingLabel)
        self.addSubview(totalNumberOfrestaurantStarRatingLabel)
        self.addSubview(starIconImageView)
        self.addSubview(availableLabel)
        self.addSubview(menuVegOnlyToggleSwitch)
        self.addSubview(vegOnlyLabel)
        addLineAtTopAndBottomOfStarRatingAndDeliveryTiming()
        activateRestaurantContentConstraints()
        self.addSubview(menuTableView)
        NSLayoutConstraint.activate(getmenuTableViewContraints())
        
    }
    
    
    
    var restaurantNameLabel : UILabel = {
        let restaurantNameLabel = UILabel()
        restaurantNameLabel.textColor = .black
        restaurantNameLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantNameLabel.font = UIFont(name:"ArialRoundedMTBold", size: 30.0)
        restaurantNameLabel.textAlignment = .left
        restaurantNameLabel.adjustsFontSizeToFitWidth = true
        restaurantNameLabel.sizeToFit()
        return restaurantNameLabel
    }()
    var restaurantCuisineLabel : UILabel = {
        let restaurantCuisineLabel = UILabel()
        restaurantCuisineLabel.textColor = .systemGray
        restaurantCuisineLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantCuisineLabel.font = UIFont.systemFont(ofSize: 14)
        restaurantCuisineLabel.sizeToFit()
        return restaurantCuisineLabel
    }()
    
    var restaurantDescriptionLabel : UILabel = {
        let restaurantDescriptionLabel = UILabel()
        restaurantDescriptionLabel.textColor = .systemGray
        restaurantDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantDescriptionLabel.font = UIFont.systemFont(ofSize: 14)
        restaurantDescriptionLabel.adjustsFontSizeToFitWidth = true
        restaurantDescriptionLabel.lineBreakMode = .byWordWrapping
        restaurantDescriptionLabel.numberOfLines = 5
        return restaurantDescriptionLabel
    }()
    var restaurantAddressLabel : UILabel = {
        let restaurantAddressLabel = UILabel()
        restaurantAddressLabel.textColor = .systemGray
        restaurantAddressLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantAddressLabel.adjustsFontSizeToFitWidth = true
        restaurantAddressLabel.backgroundColor = .white
        restaurantAddressLabel.font = UIFont.systemFont(ofSize: 14)
        restaurantAddressLabel.lineBreakMode = .byWordWrapping
        restaurantAddressLabel.numberOfLines = 5
       return restaurantAddressLabel
    }()
     var restaurantStarRatingLabel : UILabel = {
        let restaurantStarRatingLabel = UILabel()
        restaurantStarRatingLabel.textColor = .black
        restaurantStarRatingLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantStarRatingLabel.adjustsFontSizeToFitWidth = true
        restaurantStarRatingLabel.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        return restaurantStarRatingLabel
    }()
     var totalNumberOfrestaurantStarRatingLabel : UILabel = {
        let totalNumberOfrestaurantStarRatingLabel = UILabel()
        totalNumberOfrestaurantStarRatingLabel.textColor = .black.withAlphaComponent(0.9)
        totalNumberOfrestaurantStarRatingLabel.translatesAutoresizingMaskIntoConstraints = false
        totalNumberOfrestaurantStarRatingLabel.adjustsFontSizeToFitWidth = true
        totalNumberOfrestaurantStarRatingLabel.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        totalNumberOfrestaurantStarRatingLabel.textAlignment = .center
        return totalNumberOfrestaurantStarRatingLabel
    }()
     var starIconImageView : UIImageView = {
        let starIconImageView = UIImageView()
        starIconImageView.image = UIImage(systemName: "star.fill")?.withTintColor(.black , renderingMode: .alwaysOriginal)
        starIconImageView.translatesAutoresizingMaskIntoConstraints = false
        return starIconImageView
    }()
    var availableLabel : UILabel = {
        let availableLabel = UILabel()
        availableLabel.translatesAutoresizingMaskIntoConstraints = false
        availableLabel.textColor = .black
        availableLabel.textAlignment = .center
        availableLabel.numberOfLines = 4
        availableLabel.lineBreakMode = .byWordWrapping
        return availableLabel
    }()
    
     lazy var menuVegOnlyToggleSwitch : UISwitch = {
        let menuVegOnlyToggleSwitch = UISwitch()
        menuVegOnlyToggleSwitch.translatesAutoresizingMaskIntoConstraints = false
        menuVegOnlyToggleSwitch.backgroundColor = .white
        menuVegOnlyToggleSwitch.addTarget(self, action: #selector(didTapSwitchMenuOnlyVeg(sender:)), for: .touchUpInside)
        return menuVegOnlyToggleSwitch
    }()
     var vegOnlyLabel : UILabel = {
        let vegOnlyLabel = UILabel()
        vegOnlyLabel.text = "VEG ONLY"
        vegOnlyLabel.font = UIFont(name:"ArialRoundedMTBold", size: 14.0)
        vegOnlyLabel.backgroundColor = .white
        vegOnlyLabel.textColor = .systemGray
        vegOnlyLabel.translatesAutoresizingMaskIntoConstraints = false
        return vegOnlyLabel
    }()
    
    
    
    
    
    
    

    
    
    private func addLineAtTopAndBottomOfStarRatingAndDeliveryTiming(){
        createLineView(bottonOfItem: restaurantAddressLabel, paddingConstant: 10, color: .systemGray5)
        createLineView(bottonOfItem: totalNumberOfrestaurantStarRatingLabel, paddingConstant: 5, color: .systemGray5)
    }
    
    private func createLineView(bottonOfItem : UIView,paddingConstant : CGFloat,color : UIColor){
        let lineView = UIView()
        self.addSubview(lineView)
        lineView.translatesAutoresizingMaskIntoConstraints = false
        lineView.topAnchor.constraint(equalTo: bottonOfItem.bottomAnchor,constant: paddingConstant).isActive = true
        lineView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 15).isActive = true
        lineView.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -15).isActive = true
        lineView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        lineView.backgroundColor = color
        
    }
    
    
    
    func updateRestaurantDetails(restaurantContentDetails : RestaurantContentDetails){

        restaurantNameLabel.text = restaurantContentDetails.restaurantDetails.restaurantName
        restaurantCuisineLabel.text = restaurantContentDetails.restaurantDetails.restaurantCuisine
        restaurantDescriptionLabel.text = restaurantContentDetails.restaurantDetails.restaurantDescription
        restaurantAddressLabel.text = "\(restaurantContentDetails.restaurantAddress.doorNoAndBuildingNameAndBuildingNo), \(restaurantContentDetails.restaurantAddress.streetName), \(restaurantContentDetails.restaurantAddress.landmark), \(restaurantContentDetails.restaurantAddress.localityName), \(restaurantContentDetails.restaurantAddress.city), \(restaurantContentDetails.restaurantAddress.state), \(restaurantContentDetails.restaurantAddress.country), \(restaurantContentDetails.restaurantAddress.pincode)"
        restaurantStarRatingLabel.text = String(restaurantContentDetails.restaurantStarRating.prefix(upTo: restaurantContentDetails.restaurantStarRating.index(restaurantContentDetails.restaurantStarRating.startIndex, offsetBy: 3)))
        
        totalNumberOfrestaurantStarRatingLabel.text = "\(restaurantContentDetails.totalNumberOfRating)+ ratings"
        updateRestaurantStatus(deliveryTiming: Delivery.getDeaultDelieveryTimeInMins(), restaurantContentDetails: restaurantContentDetails)
    }
    
    func updateRestaurantStatus(deliveryTiming : Int,restaurantContentDetails : RestaurantContentDetails){
        if(!restaurantContentDetails.isDeliverable){
            availableLabel.text = "NOT DELIVERABLE"
            availableLabel.textColor = .systemRed
            availableLabel.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        }
        else if(restaurantContentDetails.restaurantIsAvailable == 1){
            availableLabel.text =
        """
           \(deliveryTiming) mins
        Delivery time
        """
            availableLabel.textColor = .black
            availableLabel.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        }
        else{
            availableLabel.text =
            """
                        CLOSED
            Opens Next At \(restaurantContentDetails.restaurantOpensNextAt)
            """
            availableLabel.textColor = .black.withAlphaComponent(0.9)
            availableLabel.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        }
    }
    
   
    
    private func activateRestaurantContentConstraints(){
        NSLayoutConstraint.activate(getRestaurantNameLabelContraints())
        NSLayoutConstraint.activate(getRestaurantCuisineLabelContraints())
        NSLayoutConstraint.activate(getRestaurantDescriptionLabelContraints())
        NSLayoutConstraint.activate(getRestaurantAddressLabelContraints())
        NSLayoutConstraint.activate(getStarIconCellImageViewContraints())
        NSLayoutConstraint.activate(getTotalNumberOfrestaurantStarRatingLabelContraints())
        NSLayoutConstraint.activate(getRestaurantStarRatingLabelContraints())
        NSLayoutConstraint.activate(getAvailablityLabelContraints())
        NSLayoutConstraint.activate(getVegOnlyToggleSwitchConstraints())
        NSLayoutConstraint.activate(getVegOnlyLabelConstraints())
    }
    
    func createMenuCategoryButton(menuCategory : String) -> UIButton{
        let menuCategoryButton = UIButton()
        menuCategoryButton.setTitle(menuCategory, for: .normal)
        menuCategoryButton.backgroundColor = .white
        menuCategoryButton.setTitleColor(.black, for: .normal)
        menuCategoryButton.titleLabel?.font = UIFont.systemFont(ofSize: 25, weight: .bold)
        menuCategoryButton.addRightImageToButton(systemName: "chevron.up", imageColor: .lightGray, padding: 20)
        menuCategoryButton.contentHorizontalAlignment = .left
        menuCategoryButton.addTarget(self, action: #selector(didTapMenuCategoryButton(sender:)), for: .touchUpInside)
        return menuCategoryButton
    }


    var menuTableView : UITableView = {
        let  menuTableView = UITableView()
        menuTableView.backgroundColor = .white
        menuTableView.isScrollEnabled = false
        menuTableView.register(UserMenuDisplayTableViewCell.self, forCellReuseIdentifier: UserMenuDisplayTableViewCell.cellIdentifier)
        menuTableView.translatesAutoresizingMaskIntoConstraints = false
        return menuTableView
    }()
    
    
    func getMenuTabelViewCellHeight() -> CGFloat{
        return 120
    }
    
    
    
    func createMenuTableViewCell(indexPath : IndexPath ,tableView : UITableView, menuCellContents : (menuImage : Data? , menuName : String , menuDescription : String, menuPrice : Int , menuTarianType : String, menuAvailable : Int, menuAvailableNextAt  : String ,menuId : Int)) -> UserMenuElementTableViewCellProtocol{
        let menuTableViewCell = tableView.dequeueReusableCell(withIdentifier: UserMenuDisplayTableViewCell.cellIdentifier, for: indexPath) as? UserMenuDisplayTableViewCell
        menuTableViewCell?.cellHeight = 120
        menuTableViewCell?.configureContent(menuCellContents: menuCellContents)
        return menuTableViewCell!
    }

    
   
    private func getMenuTableViewConstraints() -> [NSLayoutConstraint]{
        let menuTableViewConstraints = [menuTableView.topAnchor.constraint(equalTo: menuVegOnlyToggleSwitch.bottomAnchor ,constant: 20),
                                    menuTableView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
                                    menuTableView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10)
        ]
        return menuTableViewConstraints
    }
    
   

    private func getRestaurantNameLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [restaurantNameLabel.topAnchor.constraint(equalTo: self.topAnchor,constant: 5),
            restaurantNameLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            restaurantNameLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
            restaurantNameLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    
    private func getRestaurantCuisineLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
            restaurantCuisineLabel.topAnchor.constraint(equalTo: restaurantNameLabel.bottomAnchor,constant: 5 ),
            restaurantCuisineLabel.leftAnchor.constraint(equalTo: self.leftAnchor , constant: 10),
            restaurantCuisineLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),restaurantCuisineLabel.heightAnchor.constraint(equalToConstant: 20)]
        return labelContraints
    }
    
    private func getRestaurantDescriptionLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
            restaurantDescriptionLabel.topAnchor.constraint(equalTo: restaurantCuisineLabel.bottomAnchor ,constant: 5),
            restaurantDescriptionLabel.leftAnchor.constraint(equalTo: self.leftAnchor , constant: 10),
            restaurantDescriptionLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
            restaurantDescriptionLabel.heightAnchor.constraint(equalToConstant: 25)
        ]
        return labelContraints
    }
    
    

    private func getRestaurantAddressLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            restaurantAddressLabel.topAnchor.constraint(equalTo: restaurantDescriptionLabel.bottomAnchor ,constant: 5),
            restaurantAddressLabel.leftAnchor.constraint(equalTo: self.leftAnchor , constant: 10),
            restaurantAddressLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
            restaurantAddressLabel.heightAnchor.constraint(equalToConstant: 35),
        ]
        return labelContraints
    }

    private func getStarIconCellImageViewContraints() -> [NSLayoutConstraint]{
        let imageContraints = [
            starIconImageView.topAnchor.constraint(equalTo: restaurantAddressLabel.bottomAnchor ,constant: 20),
            starIconImageView.rightAnchor.constraint(equalTo: totalNumberOfrestaurantStarRatingLabel.centerXAnchor),
            starIconImageView.widthAnchor.constraint(equalToConstant: 30),
            starIconImageView.heightAnchor.constraint(equalToConstant: 30)]
        return imageContraints
    }

    private func getTotalNumberOfrestaurantStarRatingLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            totalNumberOfrestaurantStarRatingLabel.topAnchor.constraint(equalTo: starIconImageView.bottomAnchor),
            totalNumberOfrestaurantStarRatingLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            totalNumberOfrestaurantStarRatingLabel.rightAnchor.constraint(equalTo: self.centerXAnchor ,constant: -10),
            totalNumberOfrestaurantStarRatingLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    private func getRestaurantStarRatingLabelContraints() ->[NSLayoutConstraint] {
        let labelContraints = [
            restaurantStarRatingLabel.topAnchor.constraint(equalTo: restaurantAddressLabel.bottomAnchor,constant: 20),
            restaurantStarRatingLabel.leftAnchor.constraint(equalTo: starIconImageView.rightAnchor),
            restaurantStarRatingLabel.rightAnchor.constraint(equalTo: self.centerXAnchor ,constant: -10),
            restaurantStarRatingLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    private func getAvailablityLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
            availableLabel.topAnchor.constraint(equalTo: restaurantAddressLabel.bottomAnchor,constant: 20),
            availableLabel.leftAnchor.constraint(equalTo: self.centerXAnchor,constant: 30),
            availableLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
            availableLabel.heightAnchor.constraint(equalToConstant: 60)]
        return labelContraints
    }
    
    
    private func getVegOnlyToggleSwitchConstraints() -> [NSLayoutConstraint] {
        let switchConstraints = [menuVegOnlyToggleSwitch.topAnchor.constraint(equalTo: totalNumberOfrestaurantStarRatingLabel.bottomAnchor,constant: 15),
                                 menuVegOnlyToggleSwitch.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                                 menuVegOnlyToggleSwitch.widthAnchor.constraint(equalToConstant: 50),menuVegOnlyToggleSwitch.heightAnchor.constraint(equalToConstant: 30)]
        return switchConstraints
    }
    
    private func getVegOnlyLabelConstraints() -> [NSLayoutConstraint]{
        let labelConstraints = [vegOnlyLabel.topAnchor.constraint(equalTo: totalNumberOfrestaurantStarRatingLabel.bottomAnchor,constant: 15),
                                vegOnlyLabel.leftAnchor.constraint(equalTo: menuVegOnlyToggleSwitch.rightAnchor,constant: 5),
                                vegOnlyLabel.widthAnchor.constraint(equalToConstant: 80),
                                vegOnlyLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelConstraints
    }
    
    
   
    

    
    
    
    private func getmenuTableViewContraints() -> [NSLayoutConstraint]{
        let tableViewContraints = [menuTableView.topAnchor.constraint(equalTo: vegOnlyLabel.bottomAnchor,constant: 10),
            menuTableView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            menuTableView.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10)]
        return tableViewContraints
    }
    
    
    
    
    
    
    
    
    
    @objc func didTapSwitchMenuOnlyVeg(sender : UISwitch){
        delegate?.didTapSwitchMenuOnlyVeg(sender: sender)
    }
    
    @objc func didTapMenuCategoryButton(sender : UIButton){
        delegate?.didTapMenuCategoryButton(sender: sender)
    }
}

protocol UserDisplayMenuDetailsInRestaurantPageViewDelegate : AnyObject{
    func didTapSwitchMenuOnlyVeg(sender : UISwitch)
    func didTapMenuCategoryButton(sender : UIButton)
}




class UserDisplayMenuDetailsInRestaurantPageViewNavigationBarTitleView : UIView,UserDisplayMenuDetailsPageNavigationBarTitleViewProtocol{
    
    init(){
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44)
        self.backgroundColor = .white
       initialiseViewElements()
                            
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        self.addSubview(restaurantNameNavigationBarLabel)
        self.addSubview(restaurantAddressNavigationBarLabel)
        NSLayoutConstraint.activate(getRestaurantNameNavigationBarLabelContraints())
        NSLayoutConstraint.activate(getRestaurantAddressNavigationBarLabelContraints())
        
    }
    
    
    var restaurantNameNavigationBarLabel : UILabel = {
        let restaurantNameNavigationBarLabel = UILabel()
        restaurantNameNavigationBarLabel.textColor = .black
        restaurantNameNavigationBarLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantNameNavigationBarLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        restaurantNameNavigationBarLabel.textAlignment = .left
        restaurantNameNavigationBarLabel.tag = 99
        return restaurantNameNavigationBarLabel
    }()
    
    var restaurantAddressNavigationBarLabel : UILabel = {
        let restaurantAddressNavigationBarLabel = UILabel()
        restaurantAddressNavigationBarLabel.textColor = .systemGray
        restaurantAddressNavigationBarLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantAddressNavigationBarLabel.backgroundColor = .white
        restaurantAddressNavigationBarLabel.font = UIFont(name:"ArialRoundedMTBold", size: 10.0)
        restaurantAddressNavigationBarLabel.tag = 99
        return restaurantAddressNavigationBarLabel
    }()
    
    func updateRestaurantDetails(restaurantContentDetails  :RestaurantContentDetails){
        restaurantNameNavigationBarLabel.text = restaurantContentDetails.restaurantDetails.restaurantName.uppercased()
        
        restaurantAddressNavigationBarLabel.text = "\(restaurantContentDetails.restaurantAddress.doorNoAndBuildingNameAndBuildingNo), \(restaurantContentDetails.restaurantAddress.streetName), \(restaurantContentDetails.restaurantAddress.landmark), \(restaurantContentDetails.restaurantAddress.localityName), \(restaurantContentDetails.restaurantAddress.city), \(restaurantContentDetails.restaurantAddress.state), \(restaurantContentDetails.restaurantAddress.country), \(restaurantContentDetails.restaurantAddress.pincode)" + String(repeating: " ", count: 50)
    }
    
    private func getRestaurantNameNavigationBarLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            restaurantNameNavigationBarLabel.topAnchor.constraint(equalTo:         self.topAnchor),
            restaurantNameNavigationBarLabel.leftAnchor.constraint(equalTo : self.leftAnchor),
            restaurantNameNavigationBarLabel.rightAnchor.constraint(equalTo: self.rightAnchor),
            restaurantNameNavigationBarLabel.heightAnchor.constraint(equalToConstant:         self.frame.height/2)]
        return labelContraints
    }

    private func getRestaurantAddressNavigationBarLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
            restaurantAddressNavigationBarLabel.topAnchor.constraint(equalTo: restaurantNameNavigationBarLabel.bottomAnchor),
            restaurantAddressNavigationBarLabel.leftAnchor.constraint(equalTo: self.leftAnchor),
            restaurantAddressNavigationBarLabel.rightAnchor.constraint(equalTo:         self.rightAnchor),
            restaurantAddressNavigationBarLabel.heightAnchor.constraint(equalToConstant:         self.frame.height/2 - 1)]
        return labelContraints
    }
    
}
