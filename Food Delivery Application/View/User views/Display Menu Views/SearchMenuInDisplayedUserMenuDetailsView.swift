//
//  SearchMenuInDisplayMenuView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 16/02/22.
//

import Foundation
import UIKit

class SearchMenuInDisplayedUserMenuDetailsView : UIView ,SearchMenuInDisplayedUserMenuDetailsViewProtocol{
    
    weak var delegate : SearchMenuInDisplayedUserMenuDetailsViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
        self.bringSubviewToFront(moveToCartView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func initialiseViewElements(){
        self.addSubview(menuTableView)
        self.addSubview(moveToCartView)
        
        NSLayoutConstraint.activate(getmenuTableViewContraints())
        NSLayoutConstraint.activate(getmoveToCartViewLayoutConstraints())
    }
   
    
    var menuTableView : UITableView = {
        let  menuTableView = UITableView()
        menuTableView.backgroundColor = .white
        menuTableView.isScrollEnabled = false
        menuTableView.register(UserMenuDisplayTableViewCell.self, forCellReuseIdentifier: UserMenuDisplayTableViewCell.cellIdentifier)
        menuTableView.translatesAutoresizingMaskIntoConstraints = false
        return menuTableView
    }()
    
    
    func getMenuTabelViewCellHeight() -> CGFloat{
        return 120
    }
    
    
    
    
    
    func createMenuTableViewCell(indexPath : IndexPath ,tableView : UITableView, menuCellContents : (menuImage : Data? , menuName : String , menuDescription : String, menuPrice : Int , menuTarianType : String, menuAvailable : Int, menuAvailableNextAt  : String ,menuId : Int)) -> UserMenuElementTableViewCellProtocol{
        let menuTableViewCell = tableView.dequeueReusableCell(withIdentifier: UserMenuDisplayTableViewCell.cellIdentifier, for: indexPath) as? UserMenuDisplayTableViewCell
        menuTableViewCell?.cellHeight = 120
        menuTableViewCell?.configureContent(menuCellContents: menuCellContents)
        return menuTableViewCell!
    }

    
    lazy var moveToCartView : MoveToCartViewProtocol = {
        let moveToCartView = MoveToCartView()
        moveToCartView.translatesAutoresizingMaskIntoConstraints = false
        return moveToCartView
    }()
    
    
    private func getmenuTableViewContraints() -> [NSLayoutConstraint]{
        let tableViewContraints = [menuTableView.topAnchor.constraint(equalTo: self.topAnchor,constant: 5),
            menuTableView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            menuTableView.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
                                   menuTableView.bottomAnchor.constraint(equalTo: self.bottomAnchor)]
        return tableViewContraints
    }
    
    private func getmoveToCartViewLayoutConstraints() -> [NSLayoutConstraint]{
        let viewContraints = [moveToCartView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -10),
            moveToCartView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            moveToCartView.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            moveToCartView.heightAnchor.constraint(equalToConstant: 50)]
        return viewContraints
    }
    
    @objc func didTapView(){
        delegate?.didTapView()
    }
    
}


protocol SearchMenuInDisplayedUserMenuDetailsViewDelegate : AnyObject{
    func didTapView()
}


