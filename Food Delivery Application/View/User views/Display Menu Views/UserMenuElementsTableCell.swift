//
//  SearchMenuInDisplayedMenuView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 16/02/22.
//


import UIKit



enum menuTarianTypeEnum : String{
    case veg = "Veg"
    case nonVeg = "Non Veg"
    case egg = "Egg"
}


class UserMenuDisplayTableViewCell : UITableViewCell,UserMenuElementTableViewCellProtocol{
    static let cellIdentifier = "UserMenuDisplayTableViewCell"
    
    var cellHeight : CGFloat = 44
    
    weak open var delegate : MenuTableViewCellDelegate?
    
    private var menuImageViewBottomAnchorConstraint : NSLayoutConstraint?
    
    lazy var plusButton : UIButton = {
        let plusButton = UIButton()
        plusButton.setImage(UIImage(systemName: "plus")?.withTintColor(.systemGreen , renderingMode: .alwaysOriginal), for: .normal)
        plusButton.backgroundColor = .white
        plusButton.layer.borderWidth = 1
        plusButton.layer.borderColor = UIColor.white.cgColor
        plusButton.layer.cornerRadius = 5
        plusButton.translatesAutoresizingMaskIntoConstraints = false
        plusButton.addTarget(self, action: #selector(didTapPlusButton(sender:)), for: .touchUpInside)
        return plusButton
    }()
    
    lazy var minusButton : UIButton = {
        let minusButton = UIButton()
       minusButton.setImage(UIImage(systemName: "minus")?.withTintColor(.systemGray , renderingMode: .alwaysOriginal), for: .normal)
       minusButton.backgroundColor = .white
       minusButton.layer.borderWidth = 1
       minusButton.layer.borderColor = UIColor.white.cgColor
       minusButton.layer.cornerRadius = 5
       minusButton.translatesAutoresizingMaskIntoConstraints = false
        minusButton.addTarget(self, action: #selector(didTapMinusButton(sender:)), for: .touchUpInside)
       return minusButton
    }()
    
    var menuCountLabel : UILabel = {
        let  menuCountLabel = UILabel()
        menuCountLabel.backgroundColor = .white
        menuCountLabel.translatesAutoresizingMaskIntoConstraints = false
        menuCountLabel.text = "0"
        menuCountLabel.font = UIFont.systemFont(ofSize: 17,weight: .bold)
        menuCountLabel.textColor = .systemGreen
        menuCountLabel.textAlignment = .center
        return menuCountLabel
    }()
    
    lazy var addMenuCellButton : UIButton = {
        let addMenuCellButton = UIButton()
        addMenuCellButton.translatesAutoresizingMaskIntoConstraints = false
        addMenuCellButton.setTitle("ADD", for: .normal)
        addMenuCellButton.titleLabel?.font = UIFont(name: "ArialRoundedMTBold", size: 20)
        addMenuCellButton.setTitleColor(.systemGreen, for: .normal)
        addMenuCellButton.backgroundColor = .white
        addMenuCellButton.layer.borderWidth = 1
        addMenuCellButton.layer.borderColor = UIColor.systemGray4.cgColor
        addMenuCellButton.layer.cornerRadius = 5
        addMenuCellButton.addTarget(self, action: #selector(didTapAddMenu(sender:)), for: .touchUpInside)
        return addMenuCellButton
    }()
    
    lazy var addMenuButtonView : UIView = {
        let addMenuButtonView = UIView()
        addMenuButtonView.backgroundColor = .white
        addMenuButtonView.translatesAutoresizingMaskIntoConstraints = false
        addMenuButtonView.dropShadow()
        addMenuButtonView.layer.borderWidth = 1
        addMenuButtonView.layer.borderColor = UIColor.systemGray5.cgColor
        addMenuButtonView.layer.cornerRadius = 5
        return addMenuButtonView
    }()
    
    var menuTableViewCellContentView : UIView = {
        let menuTableViewCellContentView = UIView()
        menuTableViewCellContentView.backgroundColor = .white
        return menuTableViewCellContentView
    }()
    
    var menuNameCellLabel : UILabel = {
        let menuNameCellLabel = UILabel()
        menuNameCellLabel.backgroundColor = .white
        menuNameCellLabel.translatesAutoresizingMaskIntoConstraints = false
        menuNameCellLabel.textColor = .black
        menuNameCellLabel.textAlignment = .left
        menuNameCellLabel.adjustsFontSizeToFitWidth = true
        menuNameCellLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        return menuNameCellLabel
    }()
    
    var menuPriceCellLabel : UILabel = {
        let menuPriceCellLabel = UILabel()
        menuPriceCellLabel.backgroundColor = .white
        menuPriceCellLabel.translatesAutoresizingMaskIntoConstraints = false
        menuPriceCellLabel.textColor = .black.withAlphaComponent(0.7)
        menuPriceCellLabel.textAlignment = .left
        return menuPriceCellLabel
    }()
    
    var menuTarianTypeCellImageView : UIImageView = {
        let  menuTarianTypeCellImageView = UIImageView()
        menuTarianTypeCellImageView.translatesAutoresizingMaskIntoConstraints = false
        return menuTarianTypeCellImageView
    }()
    var menuDescriptionCellLabel : UILabel = {
        let menuDescriptionCellLabel = UILabel()
        menuDescriptionCellLabel.translatesAutoresizingMaskIntoConstraints = false
        menuDescriptionCellLabel.backgroundColor = .white
        menuDescriptionCellLabel.textColor = .black.withAlphaComponent(0.7)
        menuDescriptionCellLabel.lineBreakMode = .byWordWrapping
        menuDescriptionCellLabel.numberOfLines  = 2
        menuDescriptionCellLabel.textAlignment = .left
        menuDescriptionCellLabel.sizeToFit()
        menuDescriptionCellLabel.font = UIFont(name: "ArialRoundedMTBold", size: 12)
        return menuDescriptionCellLabel
    }()
    
    
   
    
    var menuNextAvailableAtCellLabel : UILabel = {
        let menuNextAvailableAtCellLabel = UILabel()
        menuNextAvailableAtCellLabel.translatesAutoresizingMaskIntoConstraints = false
        menuNextAvailableAtCellLabel.backgroundColor = .white
        menuNextAvailableAtCellLabel.textColor = .black.withAlphaComponent(0.7)
        menuNextAvailableAtCellLabel.layer.borderColor = UIColor.systemGray5.cgColor
        menuNextAvailableAtCellLabel.layer.borderWidth = 1
        menuNextAvailableAtCellLabel.layer.cornerRadius = 5
        menuNextAvailableAtCellLabel.numberOfLines = 3
        menuNextAvailableAtCellLabel.lineBreakMode = .byWordWrapping
        menuNextAvailableAtCellLabel.font = UIFont(name:"ArialRoundedMTBold", size: 8.0)
        menuNextAvailableAtCellLabel.sizeToFit()
        return menuNextAvailableAtCellLabel
    }()
    
    var menuImageCellImageView : UIImageView = {
        let menuImageCellImageView = UIImageView()
        menuImageCellImageView.translatesAutoresizingMaskIntoConstraints = false
        menuImageCellImageView.backgroundColor = .white
        menuImageCellImageView.layer.cornerRadius = 10
        menuImageCellImageView.contentMode = .scaleAspectFill
        menuImageCellImageView.clipsToBounds = true
        return menuImageCellImageView
    }()

     
    

    

    func modifyMenuTarianTypeSymbolColor(menuTarianType : String){
        if(menuTarianType == menuTarianTypeEnum.veg.rawValue){
            menuTarianTypeCellImageView.image = UIImage(systemName: "dot.square")?.withTintColor(.systemGreen, renderingMode: .alwaysOriginal)
        }
        else if(menuTarianType == menuTarianTypeEnum.nonVeg.rawValue){
            menuTarianTypeCellImageView.image = UIImage(systemName: "arrowtriangle.up.square")?.withTintColor(.red , renderingMode: .alwaysOriginal)
        }
        else{
            menuTarianTypeCellImageView.image = UIImage(systemName: "arrowtriangle.up.square")?.withTintColor(.systemYellow , renderingMode: .alwaysOriginal)
        }
    }


    
    
    func modifyMenuBasedOnTheAvailability(menuAvailable : Int){
        if(menuAvailable == 1){
            menuNextAvailableAtCellLabel.isHidden = true
            addMenuCellButton.isHidden = false
        }
        else{
            menuNextAvailableAtCellLabel.isHidden = false
            addMenuCellButton.isHidden = true
        }
    }
    
    private func getMenuTarianTypeImageViewConstraint() -> [NSLayoutConstraint]{
        let imageViewContraints = [menuTarianTypeCellImageView.topAnchor.constraint(equalTo: menuTableViewCellContentView.topAnchor,constant: 10),
                                   menuTarianTypeCellImageView.leftAnchor.constraint(equalTo: menuTableViewCellContentView.leftAnchor),
                                   menuTarianTypeCellImageView.widthAnchor.constraint(equalToConstant: 20),
                                   menuTarianTypeCellImageView.heightAnchor.constraint(equalToConstant: 20)]
        return imageViewContraints
    }

    private func getMenuImageCellImageViewContraints() -> [NSLayoutConstraint]{
        let imageContraints = [
            menuImageCellImageView.topAnchor.constraint(equalTo: menuTableViewCellContentView.topAnchor,constant: 10),
            menuImageCellImageView.widthAnchor.constraint(equalToConstant: 110),
            menuImageCellImageView.rightAnchor.constraint(equalTo: menuTableViewCellContentView.rightAnchor, constant: -10),
            menuImageCellImageView.heightAnchor.constraint(equalToConstant:90)]
        return imageContraints
    }

    private func getMenuNameCellLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            menuNameCellLabel.topAnchor.constraint(equalTo: menuTarianTypeCellImageView.bottomAnchor),
            menuNameCellLabel.rightAnchor.constraint(equalTo: menuImageCellImageView.leftAnchor),
            menuNameCellLabel.leftAnchor.constraint(equalTo: menuTableViewCellContentView.leftAnchor),
            menuNameCellLabel.heightAnchor.constraint(equalToConstant:30)]
        return labelContraints
    }

    private func getMenuPriceCellLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            menuPriceCellLabel.topAnchor.constraint(equalTo: menuNameCellLabel.bottomAnchor),
            menuPriceCellLabel.leftAnchor.constraint(equalTo: menuTableViewCellContentView.leftAnchor),
            menuPriceCellLabel.rightAnchor.constraint(equalTo: menuImageCellImageView.leftAnchor, constant: -10),
            menuPriceCellLabel.heightAnchor.constraint(equalToConstant:20)]
        return labelContraints
    }

    private func getMenuDescriptionCellLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            menuDescriptionCellLabel.topAnchor.constraint(equalTo: menuPriceCellLabel.bottomAnchor),
            menuDescriptionCellLabel.leftAnchor.constraint(equalTo: menuTableViewCellContentView.leftAnchor),
            menuDescriptionCellLabel.rightAnchor.constraint(equalTo: menuImageCellImageView.leftAnchor, constant: -10),
            menuDescriptionCellLabel.heightAnchor.constraint(equalToConstant:40)]
        return labelContraints
    }

    private func getAddMenuCellButtonConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [addMenuCellButton.centerYAnchor.constraint(equalTo: menuImageCellImageView.bottomAnchor),addMenuCellButton.heightAnchor.constraint(equalToConstant: 30),addMenuCellButton.leftAnchor.constraint(equalTo: menuImageCellImageView.leftAnchor,constant: 20),addMenuCellButton.rightAnchor.constraint(equalTo: menuImageCellImageView.rightAnchor,constant: -20)]
        return buttonContraints
    }

    private func getMenuNextAvailableAtCellLabelConstraint() -> [NSLayoutConstraint]{
        let labelContraints = [menuNextAvailableAtCellLabel.leftAnchor.constraint(equalTo: menuImageCellImageView.leftAnchor,constant: 20),menuNextAvailableAtCellLabel.heightAnchor.constraint(equalToConstant: 30),
                                menuNextAvailableAtCellLabel.centerYAnchor.constraint(equalTo: menuImageCellImageView.bottomAnchor),menuNextAvailableAtCellLabel.rightAnchor.constraint(equalTo: menuImageCellImageView.rightAnchor,constant: -20)
        ]
        return labelContraints
    }
    
    private func getAddmenuButtonViewConstraints() -> [NSLayoutConstraint]{
        let constraints = [addMenuButtonView.centerYAnchor.constraint(equalTo: menuImageCellImageView.bottomAnchor),addMenuButtonView.leftAnchor.constraint(equalTo: menuImageCellImageView.leftAnchor,constant: 20),addMenuButtonView.rightAnchor.constraint(equalTo: menuImageCellImageView.rightAnchor,constant: -20),
                           addMenuButtonView.heightAnchor.constraint(equalToConstant: 30)]
        return constraints
    }
    
    private func modifyMenuTabelViewCellContentsBasedOnMenuImageAvailability(menuImage : Data?){
        if menuImage == nil {
            menuImageViewBottomAnchorConstraint = menuImageCellImageView.bottomAnchor.constraint(equalTo: menuTableViewCellContentView.centerYAnchor)
            menuImageViewBottomAnchorConstraint?.isActive = true
        }
        else{
            menuImageViewBottomAnchorConstraint?.isActive = false
        }
    }
    
    private func getAddMenuButtonSubViewConstraints() -> [NSLayoutConstraint]{
        let constraints = [plusButton.topAnchor.constraint(equalTo: addMenuButtonView.topAnchor),
                           plusButton.bottomAnchor.constraint(equalTo: addMenuButtonView.bottomAnchor),
                           plusButton.rightAnchor.constraint(equalTo: addMenuButtonView.rightAnchor),
                           plusButton.widthAnchor.constraint(equalToConstant: 20),
                           menuCountLabel.bottomAnchor.constraint(equalTo: addMenuButtonView.bottomAnchor),
                           menuCountLabel.topAnchor.constraint(equalTo: addMenuButtonView.topAnchor),
                           menuCountLabel.rightAnchor.constraint(equalTo: plusButton.leftAnchor),
                           menuCountLabel.widthAnchor.constraint(equalToConstant: 30),
                           minusButton.topAnchor.constraint(equalTo: addMenuButtonView.topAnchor),
                           minusButton.bottomAnchor.constraint(equalTo: addMenuButtonView.bottomAnchor),
                           minusButton.widthAnchor.constraint(equalToConstant: 20),
                           minusButton.rightAnchor.constraint(equalTo: menuCountLabel.leftAnchor)]
        return constraints
    }
    
    private func initialiseViewElements(){
        menuTableViewCellContentView.addSubview(menuNameCellLabel)
        menuTableViewCellContentView.addSubview(menuPriceCellLabel)
        menuTableViewCellContentView.addSubview(menuImageCellImageView)
        menuTableViewCellContentView.addSubview(menuTarianTypeCellImageView)
        menuTableViewCellContentView.addSubview(menuDescriptionCellLabel)
        menuTableViewCellContentView.addSubview(addMenuCellButton)
        menuTableViewCellContentView.addSubview(menuNextAvailableAtCellLabel)
        menuTableViewCellContentView.addSubview(addMenuButtonView)
        addMenuButtonView.addSubview(plusButton)
        addMenuButtonView.addSubview(minusButton)
        addMenuButtonView.addSubview(menuCountLabel)
        NSLayoutConstraint.activate(getMenuTarianTypeImageViewConstraint())
        
        NSLayoutConstraint.activate(getAddMenuCellButtonConstraints())
        NSLayoutConstraint.activate(getAddmenuButtonViewConstraints())
        NSLayoutConstraint.activate(getMenuNameCellLabelConstraints())
        NSLayoutConstraint.activate(getMenuPriceCellLabelConstraints())
        NSLayoutConstraint.activate(getMenuDescriptionCellLabelConstraints())
        NSLayoutConstraint.activate(getMenuNextAvailableAtCellLabelConstraint())
        NSLayoutConstraint.activate(getAddMenuButtonSubViewConstraints())
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(menuTableViewCellContentView)
        initialiseViewElements()
    }
    
    

    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        menuTableViewCellContentView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: 120)
        NSLayoutConstraint.activate(getMenuImageCellImageViewContraints())
    }
    
    func configureContent(menuCellContents : (menuImage : Data? , menuName : String , menuDescription : String, menuPrice : Int , menuTarianType : String, menuAvailable : Int, menuAvailableNextAt  : String , menuId : Int)){
        
        plusButton.tag = menuCellContents.menuId
        minusButton.tag = menuCellContents.menuId
        menuCountLabel.tag = menuCellContents.menuId
        addMenuCellButton.tag = menuCellContents.menuId
        addMenuButtonView.tag = menuCellContents.menuId
        if let menuImage = menuCellContents.menuImage {
            menuImageCellImageView.image = UIImage(data: menuImage)
        }
        menuNameCellLabel.text = menuCellContents.menuName
        menuPriceCellLabel.text = "₹ \(Int(menuCellContents.menuPrice))"
        modifyMenuTarianTypeSymbolColor(menuTarianType: menuCellContents.menuTarianType)
        menuDescriptionCellLabel.text = menuCellContents.menuDescription
        menuNextAvailableAtCellLabel.text =
                """
                Next Available At
                \(menuCellContents.menuAvailableNextAt)
                """
        modifyMenuTarianTypeSymbolColor(menuTarianType : menuCellContents.menuTarianType)
        modifyMenuBasedOnTheAvailability(menuAvailable: menuCellContents.menuAvailable)
        modifyMenuTabelViewCellContentsBasedOnMenuImageAvailability(menuImage: menuCellContents.menuImage)
    }
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        menuImageCellImageView.image = nil
        menuNameCellLabel.text = nil
        menuPriceCellLabel.text = nil
        menuTarianTypeCellImageView.image = nil
        menuDescriptionCellLabel.text = nil
        menuNextAvailableAtCellLabel.text = nil
        plusButton.tag = 0x0
        minusButton.tag = 0x0
        menuCountLabel.tag = 0x0
        addMenuCellButton.tag = 0x0
        addMenuButtonView.tag = 0x0
        
        
    }
    
    @objc func didTapAddMenu(sender : UIButton){
        delegate?.didTapAddMenu(sender: sender)
    }
    
    @objc func didTapPlusButton(sender : UIButton){
        delegate?.didTapPlusButton(sender: sender)
    }
    
    @objc func didTapMinusButton(sender : UIButton){
        delegate?.didTapMinusButton(sender: sender)
    }

}


protocol MenuTableViewCellDelegate : AnyObject{
     func didTapAddMenu(sender : UIButton)
    
    
     func didTapPlusButton(sender : UIButton)
    
    
     func didTapMinusButton(sender : UIButton)
    
}

