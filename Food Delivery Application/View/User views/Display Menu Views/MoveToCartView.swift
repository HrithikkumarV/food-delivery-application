//
//  MoveToCartView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 18/04/22.
//

import UIKit
class MoveToCartView : UIView , MoveToCartViewProtocol{

    weak var delegate : MoveToCartViewDelegate?
    
    private var menuCountAndPriceLabel : UILabel = {
        let menuCountAndPriceLabel = UILabel()
        menuCountAndPriceLabel.textColor = .white
        menuCountAndPriceLabel.backgroundColor = .clear
        menuCountAndPriceLabel.textAlignment = .left
        menuCountAndPriceLabel.adjustsFontSizeToFitWidth = true
        menuCountAndPriceLabel.translatesAutoresizingMaskIntoConstraints = false
        menuCountAndPriceLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return menuCountAndPriceLabel
    }()
    
    private var viewCartLabel : UILabel = {
        let viewCartLabel = UILabel()
        viewCartLabel.textColor = .white
        viewCartLabel.backgroundColor = .clear
        viewCartLabel.text = "View Cart"
        viewCartLabel.textAlignment = .right
        viewCartLabel.translatesAutoresizingMaskIntoConstraints = false
        viewCartLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return viewCartLabel
    }()
    private var restaurantNameInCartView : UILabel = {
        let restaurantNameInCartView = UILabel()
        restaurantNameInCartView.textColor = .white
        restaurantNameInCartView.backgroundColor = .clear
        restaurantNameInCartView.textAlignment = .left
        restaurantNameInCartView.adjustsFontSizeToFitWidth = true
        restaurantNameInCartView.translatesAutoresizingMaskIntoConstraints = false
        restaurantNameInCartView.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return restaurantNameInCartView
    }()
    
    
    
    init(){
        super.init(frame: .zero)
        
        self.backgroundColor = .systemGreen
        self.layer.cornerRadius = 5
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapMoveToCart)))
        self.dropShadow()
       initialiseViewElements()
     
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        self.addSubview(menuCountAndPriceLabel)
        self.addSubview(restaurantNameInCartView)
        self.addSubview(viewCartLabel)
        NSLayoutConstraint.activate(getMenuItemCountAndPriceLabelLayoutConstraints())
        NSLayoutConstraint.activate(getViewCartLabelLayoutConstraints())
        NSLayoutConstraint.activate(getRestaurantNameLabelInCartViewLayoutConstraints())
    }
    
    
   
    
    private func getMenuItemCountAndPriceLabelLayoutConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            menuCountAndPriceLabel.topAnchor.constraint(equalTo: self.topAnchor,constant: 5),
            menuCountAndPriceLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            menuCountAndPriceLabel.rightAnchor.constraint(equalTo: self.centerXAnchor),
            menuCountAndPriceLabel.bottomAnchor.constraint(equalTo: restaurantNameInCartView.bottomAnchor,constant: -5)]
        return labelContraints
    }
    
    private func getRestaurantNameLabelInCartViewLayoutConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            restaurantNameInCartView.heightAnchor.constraint(equalToConstant: 15),
            restaurantNameInCartView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            restaurantNameInCartView.rightAnchor.constraint(equalTo: self.centerXAnchor),
            restaurantNameInCartView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -5 )]
        return labelContraints
    }
    
    private func getViewCartLabelLayoutConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            viewCartLabel.topAnchor.constraint(equalTo: self.topAnchor,constant: 5),
            viewCartLabel.leftAnchor.constraint(equalTo: self.centerXAnchor),
            viewCartLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            viewCartLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -5 )]
        return labelContraints
    }
    
    

    func updateRestaurantNameInCartLabelText(restaurantNameInUserCart : String){
        restaurantNameInCartView.text = "  From : \(restaurantNameInUserCart)"
    }
    
    func updateMenuItemCountAndPriceLabelText(menuCount : Int,totalPrice : Int){
        if(menuCount == 1){
            menuCountAndPriceLabel.text = " \(menuCount) Item | ₹\(totalPrice)"
        }
        else{
            menuCountAndPriceLabel.text = " \(menuCount) Items | ₹\(totalPrice)"
        }
    }
    
    func hideOrShowmoveToCartViewBasedOnMenuCount(menuCount :Int){
        if(menuCount == 0){
            self.isHidden = true
        }
        else{
            self.isHidden = false
        }
    }
    
    @objc func didTapMoveToCart(){
        delegate?.didTapMoveToCart()
    }
}


protocol MoveToCartViewDelegate : AnyObject{
    func didTapMoveToCart()
}
