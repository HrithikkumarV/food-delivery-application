//
//  AddMenuView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/01/22.
//

import Foundation
import UIKit

class RestaurantAddMenuDetailsView : UIView, RestaurantAddMenuDetailsViewProtocol{
    
    weak var delegate : RestaurantAddMenuDetailsViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        menuImageSectionView.addSubview(addMenuImageButton)
        menuImageSectionView.addSubview(menuImageView)
        applyMenuImageViewConstraints()
        applyAddMenuImageButtonConstraints()
        self.addSubview(stackViewForMenuDetails)
        self.addSubview(saveButton)
        NSLayoutConstraint.activate(getMenuDetailsStackViewConstraints())
        NSLayoutConstraint.activate(getSaveButtonConstraints())
    }
    
    lazy var saveButton : UIButton = {
        let saveButton = UIButton()
        saveButton.translatesAutoresizingMaskIntoConstraints = false
        saveButton.setTitle("Save", for: .normal)
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.backgroundColor = .systemBlue
        saveButton.layer.borderWidth = 1
        saveButton.layer.borderColor = UIColor.systemGray6.cgColor
        saveButton.layer.cornerRadius = 5
        saveButton.addTarget(self, action: #selector(didTapSave), for: .touchUpInside)
        return saveButton
        
    }()
    var stackViewForMenuDetails : UIStackView = {
        let stackViewForMenuDetails = UIStackView()
        stackViewForMenuDetails.axis = .vertical
        stackViewForMenuDetails.spacing = 15
        stackViewForMenuDetails.distribution = .fillProportionally
        stackViewForMenuDetails.translatesAutoresizingMaskIntoConstraints = false
        return stackViewForMenuDetails
    }()
    
    
   
    
    
    
    lazy var menuNameTextField : UITextField = {
        let menuNameTextField = createTextField()
        menuNameTextField.placeholder = "Menu Name"
        menuNameTextField.addLabelToTopBorder(labelText: " Menu Name ", fontSize: 15)
        menuNameTextField.keyboardType = .default
        menuNameTextField.returnKeyType = .next
        menuNameTextField.layer.borderColor = UIColor.systemGray5.cgColor
        return menuNameTextField
    }()
    
    
    lazy var menuPriceTextField :  UITextField = {
        let menuPriceTextField = createTextField()
        menuPriceTextField.placeholder = "Menu Price"
        menuPriceTextField.addLabelToTopBorder(labelText: " Menu Price ", fontSize: 15)
        menuPriceTextField.keyboardType = .decimalPad
        menuPriceTextField.returnKeyType = .next
        menuPriceTextField.layer.borderColor = UIColor.systemGray5.cgColor
        let menuPriceKeyboardNextBarButton = ToolBarUtils().createBarButton(title: "Next")
        let menuPriceKeyboardToolBar = ToolBarUtils().createKeyboardToolBar()
        menuPriceKeyboardToolBar.items = [ToolBarUtils().createFlexibleSpaceForToolBar(),menuPriceKeyboardNextBarButton]
        menuPriceTextField.inputAccessoryView = menuPriceKeyboardToolBar
        menuPriceKeyboardNextBarButton.target = self
        menuPriceKeyboardNextBarButton.action = #selector(didTapMenuPriceKeyboardNextBarButton)
        return menuPriceTextField
    }()
   
    
    
    
    lazy var menuDescriptionTextView : UITextView = {
        let menuDescriptionTextView = UITextView()
        menuDescriptionTextView.returnKeyType = .default
        menuDescriptionTextView.backgroundColor = .white
        menuDescriptionTextView.keyboardType = .default
        menuDescriptionTextView.textColor = .systemGray3
        menuDescriptionTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        menuDescriptionTextView.autocorrectionType = .no
        menuDescriptionTextView.font = UIFont(name: "ArialHebrew", size: 16)
        menuDescriptionTextView.translatesAutoresizingMaskIntoConstraints = false
        menuDescriptionTextView.layer.borderWidth = 1
        menuDescriptionTextView.layer.borderColor = UIColor.systemGray5.cgColor
        let menuDescriptionKeyboardNextBarButton = ToolBarUtils().createBarButton(title: "Next")
        let menuDescriptionKeyboardToolBar = ToolBarUtils().createKeyboardToolBar()
        let flexibleSpaceForToolBar = ToolBarUtils().createFlexibleSpaceForToolBar()
        menuDescriptionKeyboardToolBar.items = [flexibleSpaceForToolBar,menuDescriptionKeyboardNextBarButton]
        menuDescriptionTextView.inputAccessoryView = menuDescriptionKeyboardToolBar
        menuDescriptionKeyboardNextBarButton.target = self
        menuDescriptionKeyboardNextBarButton.action = #selector(didTapMenuDescriptionKeyboardNextBarButton)
        
        return menuDescriptionTextView
    }()
    
    
    
    
    lazy var menuTarianTypeDropDownListButton : UIButton = {
        let menuTarianTypeDropDownListButton = UIButton()
        menuTarianTypeDropDownListButton.setTitle(" Select Tarian Type", for: .normal)
        menuTarianTypeDropDownListButton.addLabelToTopBorder(labelText: " Tarian Type ", borderColor: .systemGray5, borderWidth: 1,leftPadding: 15, topPadding: 0,textColor: .lightGray, fontSize: 15)
        menuTarianTypeDropDownListButton.hideTopBorderLabelInView()
        menuTarianTypeDropDownListButton.contentHorizontalAlignment = .left
        menuTarianTypeDropDownListButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        menuTarianTypeDropDownListButton.setTitleColor(UIColor.systemGray3, for: .normal)
        menuTarianTypeDropDownListButton.titleLabel?.textAlignment = .left
        menuTarianTypeDropDownListButton.backgroundColor = .white
        menuTarianTypeDropDownListButton.translatesAutoresizingMaskIntoConstraints = false
        menuTarianTypeDropDownListButton.heightAnchor.constraint(equalToConstant: 50).isActive = true

        menuTarianTypeDropDownListButton.addRightImageToButton(systemName: "chevron.down", imageColor: .lightGray, padding: 20)
        menuTarianTypeDropDownListButton.addTarget(self, action: #selector(didTapMenuTarianType), for: .touchUpInside)
        
        return menuTarianTypeDropDownListButton
    }()
    
    lazy var menuCategoryTypeLabel : UILabel = {
        let menuCategoryTypeLabel = UILabel()
        menuCategoryTypeLabel.backgroundColor = .white
        menuCategoryTypeLabel.translatesAutoresizingMaskIntoConstraints = false
        menuCategoryTypeLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        menuCategoryTypeLabel.lineBreakMode = .byWordWrapping
        menuCategoryTypeLabel.numberOfLines = 2
        menuCategoryTypeLabel.sizeToFit()
        menuCategoryTypeLabel.addLabelToTopBorder(labelText: " Category ", borderColor: .systemGray5, borderWidth: 1,leftPadding: 15, topPadding: 0,textColor: .lightGray, fontSize: 15)
       
        return menuCategoryTypeLabel
    }()
    
//    menuCategoryTypeLabel.text = " \(menuCategory)"
    
    lazy var addMenuImageButton : UIButton = {
        let addMenuImageButton = UIButton()
        addMenuImageButton.setTitle("Upload Menu Image", for: .normal)
        addMenuImageButton.setTitleColor(.systemBlue, for: .normal)
        addMenuImageButton.backgroundColor = .white
        addMenuImageButton.translatesAutoresizingMaskIntoConstraints = false
        addMenuImageButton.layer.borderColor = UIColor.systemGray6.cgColor
        addMenuImageButton.layer.borderWidth = 1
        addMenuImageButton.layer.cornerRadius = 5
        addMenuImageButton.addTarget(self, action: #selector(didTapAddMenuImage), for: .touchUpInside)
        return addMenuImageButton
    }()
    
    var menuImageView : UIImageView  = {
        let menuImageView = UIImageView()
        menuImageView.layer.borderColor = UIColor.systemGray.cgColor
        menuImageView.layer.borderWidth = 1
        menuImageView.translatesAutoresizingMaskIntoConstraints = false
        return menuImageView
    }()
    
    lazy var menuImageSectionView : UIView = {
        let menuImageSectionView = UIView()
        
        menuImageSectionView.addLabelToTopBorder(labelText: " Menu Image ", borderColor: .systemGray5, borderWidth: 1,leftPadding: 15, topPadding: 0,textColor: .lightGray, fontSize: 15)
        menuImageSectionView.heightAnchor.constraint(equalToConstant: 120).isActive = true

        menuImageSectionView.layer.cornerRadius = 5 
        menuImageSectionView.translatesAutoresizingMaskIntoConstraints =  false
        
        return menuImageSectionView
    }()
    
    private func createTextField() -> UITextField{
        let textField = CustomTextField()
        textField.addBorder(side: .top, color: .systemGray5, width: 1)
        textField.addBorder(side: .bottom, color: .systemGray5, width: 1)
        textField.addBorder(side: .left, color: .systemGray5, width: 1)
        textField.addBorder(side: .right, color: .systemGray5, width: 1)
        textField .backgroundColor = .white
        textField .textColor = .black
        textField .translatesAutoresizingMaskIntoConstraints = false
        textField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        textField.autocorrectionType = .no
        return textField
    }
    
    
    private func applyAddMenuImageButtonConstraints(){
        addMenuImageButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        addMenuImageButton.centerYAnchor.constraint(equalTo: menuImageSectionView.centerYAnchor).isActive = true
        
        addMenuImageButton.leftAnchor.constraint(equalTo: menuImageSectionView.leftAnchor , constant: 120).isActive = true
        
        addMenuImageButton.rightAnchor.constraint(equalTo: menuImageSectionView.rightAnchor , constant: -20).isActive = true
        
    }
    
    private func applyMenuImageViewConstraints(){
        menuImageView.topAnchor.constraint(equalTo: menuImageSectionView.topAnchor ,constant: 10).isActive = true
        
        menuImageView.bottomAnchor.constraint(equalTo: menuImageSectionView.bottomAnchor ,constant: -10).isActive = true
        
        menuImageView.leftAnchor.constraint(equalTo: menuImageSectionView.leftAnchor ,constant: 10).isActive = true
        
        menuImageView.rightAnchor.constraint(equalTo: menuImageSectionView.leftAnchor ,constant: 110).isActive = true
    }
    
   
    

    private func getMenuDetailsStackViewConstraints() -> [NSLayoutConstraint]{
        let stackViewConstraints = [stackViewForMenuDetails.topAnchor.constraint(equalTo:self.topAnchor ,constant: 50),
                                    stackViewForMenuDetails.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
                                    stackViewForMenuDetails.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40)
        ]
        return stackViewConstraints
    }
    
    
   
   
    
    
    private func getSaveButtonConstraints() -> [NSLayoutConstraint]{
        let buttonConstraints = [saveButton.topAnchor.constraint(equalTo:stackViewForMenuDetails.bottomAnchor,constant: 50),
                                 saveButton.leftAnchor.constraint(equalTo: stackViewForMenuDetails.leftAnchor , constant: 50),
                                 saveButton.rightAnchor.constraint(equalTo: stackViewForMenuDetails.rightAnchor ,constant: -50),
                                 saveButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonConstraints
    }
    
    @objc func didTapView(){
        delegate?.didTapView()
    }
    
    @objc func didTapSave(){
        delegate?.didTapSave()
    }
    
    @objc func didTapMenuDescriptionKeyboardNextBarButton(){
        delegate?.didTapMenuDescriptionKeyboardNextBarButton()
    }
    
    @objc func didTapMenuPriceKeyboardNextBarButton(){
        delegate?.didTapMenuPriceKeyboardNextBarButton()
    }
    
    @objc func didTapAddMenuImage(){
        delegate?.didTapAddMenuImage()
    }
    
    @objc func didTapMenuTarianType(){
        delegate?.didTapMenuTarianType()
    }
}



protocol RestaurantAddMenuDetailsViewDelegate : AnyObject{
     func didTapView()
     func didTapSave()
     func didTapMenuDescriptionKeyboardNextBarButton()
     func didTapMenuPriceKeyboardNextBarButton()
     func didTapAddMenuImage()
     func didTapMenuTarianType()
}
