//
//  RestaurantMenuElementsTableView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 18/02/22.
//

import UIKit



class RestaurantMenuTableViewCell : UITableViewCell,RestaurantMenuTableViewCellProtocol{
   
    
    
    
    static let cellIdentifier = "RestaurantMenuTableViewCell"
    
    var cellHeight : CGFloat = 44
    
    weak open var delegate : RestaurantMenuTableViewCellDelegate?
    
    
    var menuTableViewCellContentView : UIView = {
        let menuTableViewCellContentView = UIView()
        menuTableViewCellContentView.backgroundColor = .white
        return menuTableViewCellContentView
    }()
    lazy var editMenuCellButton : UIButton = {
        let editMenuCellButton = UIButton()
        editMenuCellButton.translatesAutoresizingMaskIntoConstraints = false
        editMenuCellButton.setTitle("Edit", for: .normal)
        editMenuCellButton.titleLabel?.font = UIFont(name: "ArialRoundedMTBold", size: 16)
        editMenuCellButton.setTitleColor(.black, for: .normal)
        editMenuCellButton.backgroundColor = .white
        editMenuCellButton.layer.borderWidth = 1
        editMenuCellButton.layer.borderColor = UIColor.systemGray4.cgColor
        editMenuCellButton.layer.cornerRadius = 5
        editMenuCellButton.dropShadow()
        editMenuCellButton.addTarget(self, action: #selector(didTapEditMenu(sender:)), for: .touchUpInside)
        return editMenuCellButton
    }()
    lazy var removeMenuCellButton : UIButton = {
        let removeMenuCellButton = UIButton()
        removeMenuCellButton.translatesAutoresizingMaskIntoConstraints = false
        removeMenuCellButton.setTitle("Remove", for: .normal)
        removeMenuCellButton.titleLabel?.font = UIFont(name: "ArialRoundedMTBold", size: 16)
        removeMenuCellButton.setTitleColor(.systemRed, for: .normal)
        removeMenuCellButton.backgroundColor = .white
        removeMenuCellButton.layer.borderWidth = 1
        removeMenuCellButton.layer.borderColor = UIColor.systemGray4.cgColor
        removeMenuCellButton.layer.cornerRadius = 5
        removeMenuCellButton.dropShadow()
        removeMenuCellButton.addTarget(self, action: #selector(didTapRemoveMenu(sender:)), for: .touchUpInside)
        return removeMenuCellButton
    }()
    
    var menuAvailablityCellLabel : UILabel = {
        let menuAvailablityCellLabel = UILabel()
        menuAvailablityCellLabel.translatesAutoresizingMaskIntoConstraints = false
        menuAvailablityCellLabel.backgroundColor = .white
        menuAvailablityCellLabel.layer.cornerRadius = 10
        menuAvailablityCellLabel.layer.masksToBounds = true
        menuAvailablityCellLabel.textAlignment = .center
        return menuAvailablityCellLabel
    }()
    var menuImageCellImageView : UIImageView = {
        let menuImageCellImageView = UIImageView()
        menuImageCellImageView.translatesAutoresizingMaskIntoConstraints = false
        menuImageCellImageView.backgroundColor = .white
        menuImageCellImageView.layer.cornerRadius = 10
        menuImageCellImageView.contentMode = .scaleAspectFill
        menuImageCellImageView.clipsToBounds = true
        return menuImageCellImageView
    }()
    
    var menuNameCellLabel : UILabel = {
        let menuNameCellLabel = UILabel()
        menuNameCellLabel.backgroundColor = .white
        menuNameCellLabel.translatesAutoresizingMaskIntoConstraints = false
        menuNameCellLabel.textColor = .black
        menuNameCellLabel.textAlignment = .left
        menuNameCellLabel.adjustsFontSizeToFitWidth = true
        menuNameCellLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        return menuNameCellLabel
    }()
    
    var menuPriceCellLabel : UILabel = {
        let  menuPriceCellLabel = UILabel()
        menuPriceCellLabel.backgroundColor = .white
        menuPriceCellLabel.translatesAutoresizingMaskIntoConstraints = false
       
        menuPriceCellLabel.textColor = .black.withAlphaComponent(0.7)
        menuPriceCellLabel.textAlignment = .left
        return menuPriceCellLabel
    }()
    
    var menuTarianTypeCellImageView : UIImageView = {
        let menuTarianTypeCellImageView = UIImageView()
        menuTarianTypeCellImageView.backgroundColor = .white
        menuTarianTypeCellImageView.layer.cornerRadius  = 5
        menuTarianTypeCellImageView.translatesAutoresizingMaskIntoConstraints = false
        return menuTarianTypeCellImageView
    }()
    
    var menuDescriptionCellLabel : UILabel = {
        let menuDescriptionCellLabel = UILabel()
        menuDescriptionCellLabel.translatesAutoresizingMaskIntoConstraints = false
        menuDescriptionCellLabel.backgroundColor = .white
        menuDescriptionCellLabel.textColor = .black.withAlphaComponent(0.7)
        menuDescriptionCellLabel.lineBreakMode = .byWordWrapping
        menuDescriptionCellLabel.numberOfLines  = 2
        menuDescriptionCellLabel.textAlignment = .left
        menuDescriptionCellLabel.sizeToFit()
        return menuDescriptionCellLabel
    }()

    private func modifyMenuTarianTypeSymbolColor(menuTarianType : String){
        if(menuTarianType == menuTarianTypeEnum.veg.rawValue){
            menuTarianTypeCellImageView.image = UIImage(systemName: "dot.square")?.withTintColor(.systemGreen, renderingMode: .alwaysOriginal)
        }
        else if(menuTarianType == menuTarianTypeEnum.nonVeg.rawValue){
            menuTarianTypeCellImageView.image = UIImage(systemName: "arrowtriangle.up.square")?.withTintColor(.red , renderingMode: .alwaysOriginal)
        }
        else{
            menuTarianTypeCellImageView.image = UIImage(systemName: "arrowtriangle.up.square")?.withTintColor(.systemYellow , renderingMode: .alwaysOriginal)
        }
    }

    
    func modifyMenuBasedOnTheAvailability(menuAvailable : Int,menuStatus : Int,menuNextAvailableAt : String){
        if(menuStatus == 0){
            menuAvailablityCellLabel.text = "Delisted"
            menuAvailablityCellLabel.textColor = .systemRed
            menuAvailablityCellLabel.font = UIFont(name:"ArialRoundedMTBold", size: 16.0)
            menuAvailablityCellLabel.numberOfLines = 0
        }
        else if (menuAvailable == 1){
            menuAvailablityCellLabel.text = "Available"
            menuAvailablityCellLabel.textColor = .systemGreen
            menuAvailablityCellLabel.font = UIFont(name:"ArialRoundedMTBold", size: 16.0)
            menuAvailablityCellLabel.numberOfLines = 0
        }
        else{
            menuAvailablityCellLabel.text =
                    """
                    Next Available At
                    \(menuNextAvailableAt)
                    """
            menuAvailablityCellLabel.font = UIFont(name:"ArialRoundedMTBold", size: 8.0)
            menuAvailablityCellLabel.numberOfLines = 3
            menuAvailablityCellLabel.lineBreakMode = .byWordWrapping
            menuAvailablityCellLabel.textColor = .black.withAlphaComponent(0.8)
        }
    }
    
    private func getMenuTarianTypeImageViewConstraint() -> [NSLayoutConstraint]{
        let imageViewContraints = [menuTarianTypeCellImageView.topAnchor.constraint(equalTo: menuTableViewCellContentView.topAnchor,constant: 20),
                                   menuTarianTypeCellImageView.leftAnchor.constraint(equalTo: menuTableViewCellContentView.leftAnchor,constant: 30),
                                   menuTarianTypeCellImageView.widthAnchor.constraint(equalToConstant: 25),
                                   menuTarianTypeCellImageView.heightAnchor.constraint(equalToConstant: 25)]
        return imageViewContraints
    }

    private func getMenuImageCellImageViewContraints() -> [NSLayoutConstraint]{
        let imageContraints = [
            menuImageCellImageView.topAnchor.constraint(equalTo: menuTableViewCellContentView.topAnchor,constant: 10),
            menuImageCellImageView.rightAnchor.constraint(equalTo: menuTableViewCellContentView.rightAnchor,constant: -20),
            menuImageCellImageView.leftAnchor.constraint(equalTo: menuTableViewCellContentView.leftAnchor, constant: 20),
            menuImageCellImageView.heightAnchor.constraint(equalToConstant: 200)]
        return imageContraints
    }

    private func getMenuNameCellLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            menuNameCellLabel.topAnchor.constraint(equalTo: menuImageCellImageView.bottomAnchor,constant: 10),
            menuNameCellLabel.leftAnchor.constraint(equalTo: menuTableViewCellContentView.leftAnchor,constant: 20),
            menuNameCellLabel.rightAnchor.constraint(equalTo: menuTableViewCellContentView.rightAnchor, constant: -20),
            menuNameCellLabel.heightAnchor.constraint(equalToConstant:30)]
        return labelContraints
    }

    private func getMenuPriceCellLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            menuPriceCellLabel.topAnchor.constraint(equalTo: menuNameCellLabel.bottomAnchor),
            menuPriceCellLabel.leftAnchor.constraint(equalTo: menuTableViewCellContentView.leftAnchor,constant: 20),
            menuPriceCellLabel.rightAnchor.constraint(equalTo: menuTableViewCellContentView.rightAnchor, constant: -20),
            menuPriceCellLabel.heightAnchor.constraint(equalToConstant:30)]
        return labelContraints
    }

    private func getMenuDescriptionCellLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            menuDescriptionCellLabel.topAnchor.constraint(equalTo: menuPriceCellLabel.bottomAnchor),
            menuDescriptionCellLabel.leftAnchor.constraint(equalTo: menuTableViewCellContentView.leftAnchor,constant: 20),
            menuDescriptionCellLabel.rightAnchor.constraint(equalTo: menuTableViewCellContentView.rightAnchor, constant: -20),
            menuDescriptionCellLabel.heightAnchor.constraint(equalToConstant:40)]
        return labelContraints
    }
    
    private func getRemoveMenuCellButtonConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [removeMenuCellButton.topAnchor.constraint(equalTo: menuDescriptionCellLabel.bottomAnchor,constant: 5),removeMenuCellButton.heightAnchor.constraint(equalToConstant: 40),removeMenuCellButton.leftAnchor.constraint(equalTo : menuTableViewCellContentView.centerXAnchor,constant: 20),removeMenuCellButton.rightAnchor.constraint(equalTo: menuTableViewCellContentView.rightAnchor,constant: -20)]
        return buttonContraints
    }
    
    

    private func getEditMenuCellButtonConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [editMenuCellButton.topAnchor.constraint(equalTo: menuDescriptionCellLabel.bottomAnchor,constant: 5),editMenuCellButton.heightAnchor.constraint(equalToConstant: 40),editMenuCellButton.leftAnchor.constraint(equalTo : menuTableViewCellContentView.leftAnchor,constant: 20),editMenuCellButton.rightAnchor.constraint(equalTo: menuTableViewCellContentView.centerXAnchor,constant: -20)]
        return buttonContraints
    }

    private func getMenuAvailablityCellLabelConstraint() -> [NSLayoutConstraint]{
        let labelContraints = [menuAvailablityCellLabel.topAnchor.constraint(equalTo: menuTableViewCellContentView.topAnchor,constant: 20),menuAvailablityCellLabel.heightAnchor.constraint(equalToConstant: 30),menuAvailablityCellLabel.widthAnchor.constraint(equalToConstant: 80),menuAvailablityCellLabel.rightAnchor.constraint(equalTo: menuTableViewCellContentView.rightAnchor,constant: -30)]
        
        return labelContraints
    }
    
    func modifyMenuTabelViewCellContentsBasedOnMenuImageAvailability(menuImage : Data?){
        if menuImage == nil {
            menuImageCellImageView.widthAnchor.constraint(equalToConstant: 1).isActive = true
        }
    }
    
 

    private func initialiseViewElements(){
        menuTableViewCellContentView.addSubview(menuNameCellLabel)
        menuTableViewCellContentView.addSubview(menuPriceCellLabel)
        menuTableViewCellContentView.addSubview(menuImageCellImageView)
        menuTableViewCellContentView.addSubview(menuTarianTypeCellImageView)
        menuTableViewCellContentView.addSubview(menuDescriptionCellLabel)
        menuTableViewCellContentView.addSubview(editMenuCellButton)
        menuTableViewCellContentView.addSubview(menuAvailablityCellLabel)
        menuTableViewCellContentView.addSubview(removeMenuCellButton)
        NSLayoutConstraint.activate(getMenuTarianTypeImageViewConstraint())
        NSLayoutConstraint.activate(getMenuImageCellImageViewContraints())
        NSLayoutConstraint.activate(getMenuNameCellLabelConstraints())
        NSLayoutConstraint.activate(getMenuPriceCellLabelConstraints())
        NSLayoutConstraint.activate(getMenuDescriptionCellLabelConstraints())
        NSLayoutConstraint.activate(getMenuAvailablityCellLabelConstraint())
        NSLayoutConstraint.activate(getEditMenuCellButtonConstraints())
        NSLayoutConstraint.activate(getRemoveMenuCellButtonConstraints())
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(menuTableViewCellContentView)
        initialiseViewElements()
    }
    
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        menuTableViewCellContentView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: cellHeight)
        
    }
    
    func configureContent(menuCellContents : (menuImage : Data? , menuName : String , menuDescription : String, menuPrice : Int , menuTarianType : String, menuAvailableNextAt  : String ,menuStatus  : Int,menuAvailable : Int, menuId : Int)){
        menuNameCellLabel.text = menuCellContents.menuName
        menuPriceCellLabel.text = "₹ \(Int(menuCellContents.menuPrice))"
        modifyMenuTarianTypeSymbolColor(menuTarianType: menuCellContents.menuTarianType)
        menuDescriptionCellLabel.text = menuCellContents.menuDescription
        modifyMenuBasedOnTheAvailability(menuAvailable: menuCellContents.menuAvailable, menuStatus: menuCellContents.menuStatus, menuNextAvailableAt: menuCellContents.menuAvailableNextAt)
        
        editMenuCellButton.tag = menuCellContents.menuId
        removeMenuCellButton.tag = menuCellContents.menuId
        modifyMenuTabelViewCellContentsBasedOnMenuImageAvailability(menuImage : menuCellContents.menuImage)
        
        if let menuImage = menuCellContents.menuImage {
            menuImageCellImageView.image = UIImage(data: menuImage)
        }
        else{
            menuImageCellImageView.image = UIImage(systemName: "fork.knife.circle")?.withTintColor(.systemGray, renderingMode: .alwaysOriginal)
        }
        if let menuImage = menuCellContents.menuImage {
            menuImageCellImageView.image = UIImage(data: menuImage)
        }
    }
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        menuImageCellImageView.image = nil
        menuNameCellLabel.text = nil
        menuPriceCellLabel.text = nil
        menuTarianTypeCellImageView.image = nil
        menuDescriptionCellLabel.text = nil
        editMenuCellButton.tag = 0x0
        removeMenuCellButton.tag = 0x0
        menuAvailablityCellLabel.text = nil
        
    }
    
    @objc func didTapEditMenu(sender : UIButton){
        delegate?.didTapEditMenu(sender: sender)
    }
    
    @objc func didTapRemoveMenu(sender : UIButton){
        delegate?.didTapRemoveMenu(sender: sender)
    }
}


protocol RestaurantMenuTableViewCellDelegate : AnyObject{
    
    
     func didTapEditMenu(sender : UIButton)
    
    
     func didTapRemoveMenu(sender : UIButton)
    
    
}


