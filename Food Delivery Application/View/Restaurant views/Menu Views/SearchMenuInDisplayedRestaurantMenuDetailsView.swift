//
//  SearchMenuInDisplayedRestaurantMenuDetailsView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 18/02/22.
//

import Foundation
import UIKit

class SearchMenuInDisplayedRestaurantMenuDetailsView : UIView ,SearchMenusInDisplayedRestaurantMenuDetailsViewProtocol{
   
    weak var delegate : SearchMenuInDisplayedRestaurantMenuDetailsViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func initialiseViewElements(){
        self.addSubview(menuTableView)
        NSLayoutConstraint.activate(getmenuTableViewContraints())
    }
    
    
    private func getmenuTableViewContraints() -> [NSLayoutConstraint]{
        let tableViewContraints = [menuTableView.topAnchor.constraint(equalTo: self.topAnchor,constant: 5),
            menuTableView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            menuTableView.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
                                   menuTableView.bottomAnchor.constraint(equalTo: self.bottomAnchor)]
        return tableViewContraints
    }
    
    lazy var menuTableView : UITableView = {
        let menuTableView = UITableView()
        menuTableView.backgroundColor = .white
        menuTableView.isScrollEnabled = false
        menuTableView.separatorStyle = .singleLine
        menuTableView.register(RestaurantMenuTableViewCell.self, forCellReuseIdentifier: RestaurantMenuTableViewCell.cellIdentifier)
        menuTableView.translatesAutoresizingMaskIntoConstraints = false
        return menuTableView
    }()
    

    func getMenuTabelViewCellHeight() -> CGFloat{
        return 380
    }

    
    func createMenuTableViewCell(indexPath : IndexPath ,tableView : UITableView, menuCellContents : (menuImage : Data? , menuName : String , menuDescription : String, menuPrice : Int , menuTarianType : String, menuAvailableNextAt  : String ,menuStatus  : Int,menuAvailable : Int, menuId : Int)) -> RestaurantMenuTableViewCellProtocol{
        let menuTableViewCell = tableView.dequeueReusableCell(withIdentifier: RestaurantMenuTableViewCell.cellIdentifier, for: indexPath) as? RestaurantMenuTableViewCell
        
        menuTableViewCell?.cellHeight = 380
        menuTableViewCell?.configureContent(menuCellContents: menuCellContents)
        return menuTableViewCell ?? RestaurantMenuTableViewCell()
    }

    @objc func didTapView(){
        delegate?.didTapView()
    }
    
}

protocol  SearchMenuInDisplayedRestaurantMenuDetailsViewDelegate : AnyObject {
    func didTapView()
}
