//
//  RestaurantEditMenuDetailsView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 16/02/22.
//

import Foundation
import UIKit

class RestaurantEditMenuDetailsView : RestaurantAddMenuDetailsView,RestaurantEditMenuDetailsViewProtocol{
    
    
    weak var subDelegate : RestaurantEditMenuDetailsViewDelegate?{
        didSet{
            delegate = subDelegate
        }
    }
    
    override init() {
        super.init()
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        createMenuIsAvailableView()
        createMenuDelistToggleView()
        createMenuImageSectionView()
    }
    
    private var menuIsAvailableViewConstraint : NSLayoutConstraint!
    
     var menuIsAvailableView : UIView = {
        let  menuIsAvailableView = UIView()
        menuIsAvailableView.translatesAutoresizingMaskIntoConstraints = false
        menuIsAvailableView.backgroundColor = .white
        menuIsAvailableView.layer.borderColor = UIColor.systemGray5.cgColor
        menuIsAvailableView.layer.borderWidth = 1
        menuIsAvailableView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        return menuIsAvailableView
    }()
    
    lazy var datePicker : UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.timeZone =  NSTimeZone.local
        datePicker.backgroundColor = .white
        datePicker.minimumDate = Date()
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        datePicker.datePickerMode = .dateAndTime
        datePicker.addTarget(self, action: #selector(didChangeMenuDateAndtime(sender:)), for: .valueChanged)
        return datePicker
    }()
     var  menuNextAvailbaleDatePickerLabel : UILabel = {
        let menuNextAvailbaleDatePickerLabel = UILabel()
        menuNextAvailbaleDatePickerLabel.text = "Set Next Available :"
        menuNextAvailbaleDatePickerLabel.adjustsFontSizeToFitWidth = true
        menuNextAvailbaleDatePickerLabel.font = UIFont(name: "ArialRoundedMTBold", size: 14)
        menuNextAvailbaleDatePickerLabel.backgroundColor = .white
        menuNextAvailbaleDatePickerLabel.translatesAutoresizingMaskIntoConstraints = false
        return menuNextAvailbaleDatePickerLabel
    }()
    
    
    
    
    lazy var  changeMenuImageButton :  UIButton = {
        let addOrChangeMenuImageButton  = UIButton()
        addOrChangeMenuImageButton.setTitleColor(.systemBlue, for: .normal)
        addOrChangeMenuImageButton.backgroundColor = .white
        addOrChangeMenuImageButton.translatesAutoresizingMaskIntoConstraints = false
        addOrChangeMenuImageButton.layer.borderColor = UIColor.systemGray6.cgColor
        addOrChangeMenuImageButton.layer.borderWidth = 1
        addOrChangeMenuImageButton.layer.cornerRadius = 5
        addOrChangeMenuImageButton.addTarget(self, action: #selector(didTapChangeMenuImage), for: .touchUpInside)
        return addOrChangeMenuImageButton
    }()
    
    func changeAddOrChangeImageButtonTitle(menuImage : UIImage?,changeMenuImageButton  : UIButton){
        if(menuImage != nil){
            changeMenuImageButton.setTitle("Change Image", for: .normal)
        }
        else{
            changeMenuImageButton.setTitle("Add Image", for: .normal)
        }
    }
    
    lazy var removeImageButton :  UIButton = {
        let removeImageButton = UIButton()
        removeImageButton.setTitle("Upload Menu Image", for: .normal)
        removeImageButton.setTitleColor(.systemBlue, for: .normal)
        removeImageButton.backgroundColor = .white
        removeImageButton.translatesAutoresizingMaskIntoConstraints = false
        removeImageButton.layer.borderColor = UIColor.systemGray6.cgColor
        removeImageButton.layer.borderWidth = 1
        removeImageButton.layer.cornerRadius = 5
        removeImageButton.addTarget(self, action: #selector(didTapRemoveMenuImage), for: .touchUpInside)
        removeImageButton.setTitle("Remove Image", for: .normal)
        return removeImageButton
    }()
    
    
    
    private func createMenuImageSectionView(){
        menuImageSectionView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        menuImageSectionView.addLabelToTopBorder(labelText: " Menu Image ", borderColor: .systemGray5, borderWidth: 1,leftPadding: 15, topPadding: 0,textColor: .lightGray, fontSize: 15)
        menuImageSectionView.layer.cornerRadius = 5
        menuImageSectionView.translatesAutoresizingMaskIntoConstraints =  false
        menuImageSectionView.backgroundColor = .white
        menuImageSectionView.addSubview(changeMenuImageButton)
        menuImageSectionView.addSubview(removeImageButton)
        menuImageSectionView.addSubview(menuImageView)
        addMenuImageButton.removeFromSuperview()
        applyMenuImageViewConstraints()
        applyChangeMenuImageButtonConstraints()
        applyRemoveMenuImageButtonConstraints()
    }
    
    
    private func applyChangeMenuImageButtonConstraints(){
        changeMenuImageButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        changeMenuImageButton.topAnchor.constraint(equalTo: menuImageSectionView.topAnchor,constant: 15).isActive = true
        
        changeMenuImageButton.leftAnchor.constraint(equalTo: menuImageSectionView.leftAnchor , constant: 120).isActive = true
        
        changeMenuImageButton.rightAnchor.constraint(equalTo: menuImageSectionView.rightAnchor , constant: -20).isActive = true
        
    }
    
    private func applyRemoveMenuImageButtonConstraints(){
        removeImageButton .heightAnchor.constraint(equalToConstant: 40).isActive = true
        removeImageButton.bottomAnchor.constraint(equalTo: menuImageSectionView.bottomAnchor,constant: -15).isActive = true
        
        removeImageButton.leftAnchor.constraint(equalTo: menuImageSectionView.leftAnchor , constant: 120).isActive = true
        
        removeImageButton.rightAnchor.constraint(equalTo: menuImageSectionView.rightAnchor , constant: -20).isActive = true
        
    }
    
    private func applyMenuImageViewConstraints(){
        menuImageView.topAnchor.constraint(equalTo: menuImageSectionView.topAnchor ,constant: 10).isActive = true
        
        menuImageView.bottomAnchor.constraint(equalTo: menuImageSectionView.bottomAnchor ,constant: -10).isActive = true
        
        menuImageView.leftAnchor.constraint(equalTo: menuImageSectionView.leftAnchor ,constant: 10).isActive = true
        
        menuImageView.rightAnchor.constraint(equalTo: menuImageSectionView.leftAnchor ,constant: 110).isActive = true
    }
    
    var menuDelistLabel : UILabel = {
        let menuDelistLabel = UILabel()
        menuDelistLabel.text = "Menu Delist : "
        menuDelistLabel.backgroundColor = .white
        menuDelistLabel.sizeToFit()
        menuDelistLabel.translatesAutoresizingMaskIntoConstraints = false
        return menuDelistLabel
    }()
    
    lazy var menuDelistToggleSwitch : UISwitch = {
        let menuDelistToggleSwitch = UISwitch()
        menuDelistToggleSwitch.translatesAutoresizingMaskIntoConstraints = false
        menuDelistToggleSwitch.sizeToFit()
        menuDelistToggleSwitch.onTintColor = .systemRed
        menuDelistToggleSwitch.tintColor = .systemGreen
        menuDelistToggleSwitch.backgroundColor = .systemGreen
        menuDelistToggleSwitch.layer.cornerRadius = 16
        menuDelistToggleSwitch.addTarget(self, action: #selector(didTapMenuDelistSwitch(sender:)), for: .touchUpInside)
        return menuDelistToggleSwitch
    }()
    
    var menuDelistToggleSwitchView : UIView = {
        let menuDelistToggleSwitchView = UIView()
        menuDelistToggleSwitchView.translatesAutoresizingMaskIntoConstraints = false
        menuDelistToggleSwitchView.backgroundColor = .white
        menuDelistToggleSwitchView.layer.borderColor = UIColor.systemGray5.cgColor
        menuDelistToggleSwitchView.layer.borderWidth = 1
        menuDelistToggleSwitchView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        return menuDelistToggleSwitchView
    }()
    
    private func createMenuDelistToggleView() {
        menuDelistToggleSwitchView.addSubview(menuDelistToggleSwitch)
        menuDelistToggleSwitchView.addSubview(menuDelistLabel)
        applyMenuDelistToggleSwitchConstraints()
        applyMenuDelistLabelConstraints()
        
    }
    
    
    private func applyMenuDelistLabelConstraints(){
        menuDelistLabel .heightAnchor.constraint(equalToConstant: 40).isActive = true
        menuDelistLabel.centerYAnchor.constraint(equalTo: menuDelistToggleSwitchView.centerYAnchor).isActive = true
        
        menuDelistLabel.leftAnchor.constraint(equalTo: menuDelistToggleSwitchView.leftAnchor,constant: 10).isActive = true

        menuDelistLabel.rightAnchor.constraint(equalTo: menuDelistToggleSwitch.leftAnchor, constant: -10).isActive = true
        
    }
    
    private func applyMenuDelistToggleSwitchConstraints()  {
        menuDelistToggleSwitch.centerYAnchor.constraint(equalTo: menuDelistToggleSwitchView.centerYAnchor).isActive = true
        menuDelistToggleSwitch.rightAnchor.constraint(equalTo: menuDelistToggleSwitchView.rightAnchor,constant: -10).isActive = true
        menuDelistToggleSwitch.widthAnchor.constraint(equalToConstant: 50).isActive = true
        menuDelistToggleSwitch.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    var menuIsAvailableLabel :  UILabel = {
        let menuIsAvailableLabel = UILabel()
        menuIsAvailableLabel.text = "Menu Availablity : "
        menuIsAvailableLabel.backgroundColor = .white
        menuIsAvailableLabel.sizeToFit()
        menuIsAvailableLabel.translatesAutoresizingMaskIntoConstraints = false
        return menuIsAvailableLabel
    }()
    
    lazy var menuIsAvailableToggleSwitch : UISwitch = {
        let menuIsAvailableToggleSwitch = UISwitch()
        menuIsAvailableToggleSwitch.translatesAutoresizingMaskIntoConstraints = false
        menuIsAvailableToggleSwitch.sizeToFit()
        menuIsAvailableToggleSwitch.onTintColor = .systemRed
        menuIsAvailableToggleSwitch.tintColor = .systemGreen
        menuIsAvailableToggleSwitch.backgroundColor = .systemGreen
        menuIsAvailableToggleSwitch.layer.cornerRadius = 16
        menuIsAvailableToggleSwitch.addTarget(self, action: #selector(didTapMenuIsAvailableSwitch(sender:)), for: .touchUpInside)
        return menuIsAvailableToggleSwitch
    }()
   
    func hideSetNextAvailablityItems(){
        menuNextAvailbaleDatePickerLabel.isHidden = true
        datePicker.isHidden = true
        menuIsAvailableViewConstraint?.isActive = false
        menuIsAvailableViewConstraint = menuIsAvailableView.heightAnchor.constraint(equalToConstant:  60)
        menuIsAvailableViewConstraint?.isActive = true
    }
    
    func showSetNextAvailablityItems(){
        menuNextAvailbaleDatePickerLabel.isHidden = false
        datePicker.isHidden = false
        menuIsAvailableViewConstraint?.isActive = false
        menuIsAvailableViewConstraint = menuIsAvailableView.heightAnchor.constraint(equalToConstant: 120)
        menuIsAvailableViewConstraint?.isActive = true
    }
    
    private func createMenuIsAvailableView(){
        
        menuIsAvailableView.addSubview(menuIsAvailableToggleSwitch)
        menuIsAvailableView.addSubview(menuIsAvailableLabel)
        menuIsAvailableView.addSubview(menuNextAvailbaleDatePickerLabel)
        menuIsAvailableView.addSubview(datePicker)
        applyMenuIsAvailableToggleSwitchConstraints()
        applyMenuIsAvailableLabelConstraints()
        applyMenuNextAvailbaleDatePickerConstraints()
        applyMenuNextAvailableLabelsConstraints()
    }
    
    private func applyMenuIsAvailableLabelConstraints(){
        menuIsAvailableLabel .heightAnchor.constraint(equalToConstant: 30).isActive = true
        menuIsAvailableLabel.topAnchor.constraint(equalTo: menuIsAvailableView.topAnchor,constant: 10).isActive = true
        
        menuIsAvailableLabel.leftAnchor.constraint(equalTo: menuIsAvailableView.leftAnchor,constant: 10).isActive = true

        menuIsAvailableLabel.rightAnchor.constraint(equalTo: menuIsAvailableToggleSwitch.leftAnchor, constant: -10).isActive = true
        
    }
    
    private func applyMenuIsAvailableToggleSwitchConstraints()  {
        menuIsAvailableToggleSwitch.topAnchor.constraint(equalTo: menuIsAvailableView.topAnchor,constant: 10).isActive = true
        menuIsAvailableToggleSwitch.rightAnchor.constraint(equalTo: menuIsAvailableView.rightAnchor,constant: -10).isActive = true
        menuIsAvailableToggleSwitch.widthAnchor.constraint(equalToConstant: 50).isActive = true
        menuIsAvailableToggleSwitch.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    
    
    private func applyMenuNextAvailableLabelsConstraints(){
        menuNextAvailbaleDatePickerLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        menuNextAvailbaleDatePickerLabel.leftAnchor.constraint(equalTo: menuIsAvailableView.leftAnchor,constant: 10).isActive = true
        menuNextAvailbaleDatePickerLabel.rightAnchor.constraint(equalTo: datePicker.leftAnchor,constant: -10).isActive = true
        menuNextAvailbaleDatePickerLabel.topAnchor.constraint(equalTo: menuIsAvailableView.topAnchor,constant: 60).isActive = true
        
        
    }
    
    private func applyMenuNextAvailbaleDatePickerConstraints(){
        datePicker.widthAnchor.constraint(equalToConstant: 200).isActive = true
        datePicker.rightAnchor.constraint(equalTo: menuIsAvailableView.rightAnchor,constant: -10).isActive = true
        datePicker.topAnchor.constraint(equalTo: menuIsAvailableView.topAnchor,constant: 55).isActive = true
        datePicker.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
    }
    
    @objc func didChangeMenuDateAndtime(sender: UIDatePicker){
        subDelegate?.didChangeMenuDateAndtime(sender: sender)
    }
    
    @objc func didTapRemoveMenuImage(){
        subDelegate?.didTapRemoveMenuImage()
    }
    
    @objc func didTapMenuIsAvailableSwitch(sender: UISwitch){
        subDelegate?.didTapMenuIsAvailableSwitch(sender: sender)
    }
    
    @objc func didTapMenuDelistSwitch(sender: UISwitch){
        subDelegate?.didTapMenuDelistSwitch(sender: sender)
    }
    
    @objc func didTapChangeMenuImage(){
        subDelegate?.didTapChangeMenuImage()
    }
    
}


protocol RestaurantEditMenuDetailsViewDelegate : RestaurantAddMenuDetailsViewDelegate{
     func didChangeMenuDateAndtime(sender: UIDatePicker)
     func didTapRemoveMenuImage()
     func didTapMenuIsAvailableSwitch(sender: UISwitch)
    func didTapMenuDelistSwitch(sender: UISwitch)
     func didTapChangeMenuImage()
}
