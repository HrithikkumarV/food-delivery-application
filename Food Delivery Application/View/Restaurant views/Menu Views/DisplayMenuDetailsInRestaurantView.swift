//
//  DisplayMenusInRestaurantView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 18/02/22.
//

import Foundation

import UIKit

class DisplayMenuDetailsInRestaurantView :  UIView,DisplayMenuDetailsInRestaurantViewProtocol{
    
    weak var delegate : DisplayMenuDetailsInRestaurantViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        self.addSubview(setUpCategoryAndMenusLabel)
        self.addSubview(menuTableView)
        createAddMenuCategoryView()
        NSLayoutConstraint.activate(getmenuTableViewContraints())
        NSLayoutConstraint.activate(getSetUpCategoryAndMenusLabelConstraints())
    }
    
    func getMenuCategoryView() -> MenuCategoryHeaderViewProtocol{
        return MenuCategoryHeaderView()
    }
    
    lazy var addMenuCategoryButton : UIButton = {
        let addMenuCategoryButton = UIButton()
        addMenuCategoryButton.translatesAutoresizingMaskIntoConstraints = false
        addMenuCategoryButton.setTitle("Add New Menu Category", for: .normal)
        addMenuCategoryButton.titleLabel?.font = UIFont(name: "ArialRoundedMTBold", size: 20)
        addMenuCategoryButton.setTitleColor(.systemGreen, for: .normal)
        addMenuCategoryButton.backgroundColor = .white
        addMenuCategoryButton.layer.borderWidth = 1
        addMenuCategoryButton.layer.borderColor = UIColor.systemGray5.cgColor
        addMenuCategoryButton.layer.cornerRadius = 5
        addMenuCategoryButton.dropShadow()
        addMenuCategoryButton.addTarget(self, action: #selector(didTapAddMenuCategoryButton), for: .touchUpInside)
        return addMenuCategoryButton
        
    }()
    lazy var addMenuCategoryTextField : UITextField = {
        let  addMenuCategoryTextField = CustomTextField()
        addMenuCategoryTextField.placeholder = "Enter New Menu Category Name"
        addMenuCategoryTextField.addLabelToTopBorder(labelText: " Menu Category ", borderColor: .systemGray5, borderWidth: 1, leftPadding: 15, topPadding: 0, textColor:.black.withAlphaComponent(0.7), fontSize: 15)
        addMenuCategoryTextField.backgroundColor = .white
        addMenuCategoryTextField.translatesAutoresizingMaskIntoConstraints =  false
        addMenuCategoryTextField.keyboardType = .default
        addMenuCategoryTextField.returnKeyType = .done
        addMenuCategoryTextField.layer.borderColor = UIColor.systemGray5.cgColor
        return addMenuCategoryTextField
    }()
    var addMenuCategoryView : UIView = {
        let addMenuCategoryView = UIView()
        addMenuCategoryView.backgroundColor = .white
        addMenuCategoryView.translatesAutoresizingMaskIntoConstraints = false
        return addMenuCategoryView
    }()
    lazy var saveCategoryButton : UIButton = {
        let saveCategoryButton = UIButton()
        saveCategoryButton.translatesAutoresizingMaskIntoConstraints = false
        saveCategoryButton.setTitle("Save Category", for: .normal)
        saveCategoryButton.titleLabel?.font = UIFont(name: "ArialRoundedMTBold", size: 14)
        saveCategoryButton.setTitleColor(.white, for: .normal)
        saveCategoryButton.backgroundColor = .systemGreen
        saveCategoryButton.layer.borderWidth = 1
        saveCategoryButton.layer.borderColor = UIColor.systemGray4.cgColor
        saveCategoryButton.layer.cornerRadius = 5
        saveCategoryButton.dropShadow()
        saveCategoryButton.addTarget(self, action: #selector(didTapSaveMenuCategoryButton), for: .touchUpInside)
        return saveCategoryButton
    }()
    
     var setUpCategoryAndMenusLabel : UILabel = {
        let setUpCategoryAndMenusLabel = UILabel()
        setUpCategoryAndMenusLabel.text = " Once You Add the Categories and menus , It will appear here "
        setUpCategoryAndMenusLabel.textColor = .systemGray
        setUpCategoryAndMenusLabel.font = UIFont.systemFont(ofSize: 25, weight: .medium)
        setUpCategoryAndMenusLabel.lineBreakMode = .byWordWrapping
        setUpCategoryAndMenusLabel.numberOfLines = 3
        setUpCategoryAndMenusLabel.textAlignment = .center
        setUpCategoryAndMenusLabel.translatesAutoresizingMaskIntoConstraints = false
        setUpCategoryAndMenusLabel.sizeToFit()
        return setUpCategoryAndMenusLabel
    }()
    private var addMenuCategoryViewConstraint : NSLayoutConstraint!
    
    
    
    
    
   
    
    private func createAddMenuCategoryView(){
        self.addSubview(addMenuCategoryView)
        NSLayoutConstraint.activate(getAddMenuCategoryViewConstraints())
        addMenuCategoryView.addSubview(addMenuCategoryButton)
        addMenuCategoryView.addSubview(addMenuCategoryTextField)
        addMenuCategoryView.addSubview(saveCategoryButton)
        NSLayoutConstraint.activate(getAddMenuCategoryButtonConstraints())
        NSLayoutConstraint.activate(getAddMenuCategoryTextFieldConstraints())
        NSLayoutConstraint.activate(getSaveCategoryButtonConstraints())
        hideAddMenuCategoryViewItems()
    }
    
    func hideAddMenuCategoryViewItems(){
        addMenuCategoryTextField.isHidden = true
        saveCategoryButton.isHidden = true
        addMenuCategoryViewConstraint?.isActive = false
        addMenuCategoryViewConstraint = addMenuCategoryView.heightAnchor.constraint(equalToConstant:  60)
        addMenuCategoryViewConstraint?.isActive = true
        addMenuCategoryTextField.text = ""
        addMenuCategoryTextField.hideTopBorderLabelInView()
    }
    
    func showAddMenuCategoryViewItems(){
        addMenuCategoryTextField.isHidden = false
        saveCategoryButton.isHidden = false
        addMenuCategoryViewConstraint?.isActive = false
        addMenuCategoryViewConstraint = addMenuCategoryView.heightAnchor.constraint(equalToConstant: 180)
        addMenuCategoryViewConstraint?.isActive = true
    }
    
    lazy var menuTableView : UITableView = {
        let menuTableView = UITableView()
        menuTableView.backgroundColor = .white
        menuTableView.isScrollEnabled = false
        menuTableView.separatorStyle = .singleLine
        menuTableView.register(RestaurantMenuTableViewCell.self, forCellReuseIdentifier: RestaurantMenuTableViewCell.cellIdentifier)
        menuTableView.translatesAutoresizingMaskIntoConstraints = false
        return menuTableView
    }()
    
    func getMenuTabelViewCellHeight() -> CGFloat{
        return 380
    }

    
    func createMenuTableViewCell(indexPath : IndexPath ,tableView : UITableView, menuCellContents : (menuImage : Data? , menuName : String , menuDescription : String, menuPrice : Int , menuTarianType : String, menuAvailableNextAt  : String ,menuStatus  : Int,menuAvailable : Int, menuId : Int)) -> RestaurantMenuTableViewCellProtocol{
        let menuTableViewCell = tableView.dequeueReusableCell(withIdentifier: RestaurantMenuTableViewCell.cellIdentifier, for: indexPath) as? RestaurantMenuTableViewCell
        
        menuTableViewCell?.cellHeight = 380
        menuTableViewCell?.configureContent(menuCellContents: menuCellContents)
        return menuTableViewCell ?? RestaurantMenuTableViewCell()
    }
   
    
    private func getSetUpCategoryAndMenusLabelConstraints() -> [NSLayoutConstraint]{
        let labelConstraints = [setUpCategoryAndMenusLabel.centerYAnchor.constraint(equalTo:self.topAnchor, constant: UIScreen.main.bounds.midY - 100),
                            
                                setUpCategoryAndMenusLabel.leftAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leftAnchor),
                                setUpCategoryAndMenusLabel.rightAnchor.constraint(equalTo: self.safeAreaLayoutGuide.rightAnchor)]
        return labelConstraints
    }
    
    private func getAddMenuCategoryButtonConstraints() -> [NSLayoutConstraint]{
        let buttonConstraints = [addMenuCategoryButton.topAnchor.constraint(equalTo: addMenuCategoryView.topAnchor,constant: 10),addMenuCategoryButton.leftAnchor.constraint(equalTo: addMenuCategoryView.leftAnchor,constant: 10),addMenuCategoryButton.rightAnchor.constraint(equalTo:  addMenuCategoryView.rightAnchor,constant: -10),
            addMenuCategoryButton.heightAnchor.constraint(equalToConstant: 50)]
        return buttonConstraints
    }
    
    private func getAddMenuCategoryTextFieldConstraints() -> [NSLayoutConstraint]{
        let buttonConstraints = [addMenuCategoryTextField.topAnchor.constraint(equalTo: addMenuCategoryButton.bottomAnchor,constant: 10),addMenuCategoryTextField.leftAnchor.constraint(equalTo: addMenuCategoryView.leftAnchor,constant: 10),addMenuCategoryTextField.rightAnchor.constraint(equalTo:  addMenuCategoryView.rightAnchor,constant: -10),
            addMenuCategoryTextField.heightAnchor.constraint(equalToConstant: 50)]
        return buttonConstraints
    }
    
    private func getSaveCategoryButtonConstraints() -> [NSLayoutConstraint]{
        let buttonConstraints = [saveCategoryButton.topAnchor.constraint(equalTo: addMenuCategoryTextField.bottomAnchor,constant: 10),saveCategoryButton.centerXAnchor.constraint(equalTo: addMenuCategoryTextField.centerXAnchor),saveCategoryButton.widthAnchor.constraint(equalToConstant: 150),
                                 saveCategoryButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonConstraints
    }
    
    private func getAddMenuCategoryViewConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [addMenuCategoryView.topAnchor.constraint(equalTo: self.topAnchor),addMenuCategoryView.leftAnchor.constraint(equalTo: self.leftAnchor),addMenuCategoryView.rightAnchor.constraint(equalTo: self.rightAnchor)]
        return viewConstraints
    }

    private func getmenuTableViewContraints() -> [NSLayoutConstraint]{
        let tableViewContraints = [menuTableView.topAnchor.constraint(equalTo: addMenuCategoryView.bottomAnchor,constant: 10),
            menuTableView.leftAnchor.constraint(equalTo: self.leftAnchor),
            menuTableView.rightAnchor.constraint(equalTo: self.rightAnchor)]
        return tableViewContraints
    }
    
    @objc func didTapAddMenuCategoryButton(){
        delegate?.didTapAddMenuCategoryButton()
    }
    
    @objc func didTapSaveMenuCategoryButton(){
        delegate?.didTapSaveMenuCategoryButton()
    }

    @objc func didTapView(){
        delegate?.didTapView()
    }

}

protocol DisplayMenuDetailsInRestaurantViewDelegate : AnyObject{
     func didTapSaveMenuCategoryButton()
     func didTapAddMenuCategoryButton()
     func didTapView()
}


class MenuCategoryHeaderView : UIView , MenuCategoryHeaderViewProtocol{
    
    weak var delegate : MenuCategoryHeaderViewDelegate?
    
    
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        self.addSubview(menuCategoryButton)
        self.addSubview(editCategoryButton)
        self.addSubview(removeCategoryButton)
        self.addSubview(addMenuButton)
        NSLayoutConstraint.activate(getAddMenuButtonConstraints())
        NSLayoutConstraint.activate(getEditCategoryButtonConstraints())
        NSLayoutConstraint.activate(getMenuCategoryButtonConstraints())
        NSLayoutConstraint.activate(getRemoveCategoryButtonConstraints())
    }
    
    lazy var menuCategoryButton : UIButton = {
        let menuCategoryButton = UIButton()
        menuCategoryButton.translatesAutoresizingMaskIntoConstraints = false
        menuCategoryButton.backgroundColor = .white
        menuCategoryButton.setTitleColor(.black, for: .normal)
        menuCategoryButton.titleLabel?.font = UIFont(name: "ArialRoundedMTBold", size: 20)
        menuCategoryButton.addRightImageToButton(systemName: "chevron.up", imageColor: .lightGray, padding: 5)
        menuCategoryButton.contentHorizontalAlignment = .left
        menuCategoryButton.layer.cornerRadius = 5
        menuCategoryButton.titleLabel?.sizeToFit()
        menuCategoryButton.titleLabel?.lineBreakMode = .byTruncatingTail
        menuCategoryButton.titleLabel?.numberOfLines = 2
        menuCategoryButton.addTarget(self, action: #selector(didTapMenuCategoryButton(sender:)), for: .touchUpInside)
        return menuCategoryButton
    }()
    
    lazy var editCategoryButton : UIButton = {
         let editCategoryButton = UIButton()
        editCategoryButton.setImage(UIImage(systemName: "pencil")?.withTintColor(.black, renderingMode: .alwaysOriginal), for: .normal)
        editCategoryButton.sizeToFit()
        editCategoryButton.translatesAutoresizingMaskIntoConstraints = false
        editCategoryButton.imageView?.contentMode = .scaleAspectFill
        editCategoryButton.backgroundColor = .white
        editCategoryButton.layer.borderWidth = 1
        editCategoryButton.layer.borderColor = UIColor.white.cgColor
        editCategoryButton.layer.cornerRadius = 5
        editCategoryButton.contentVerticalAlignment = .fill
        editCategoryButton.contentHorizontalAlignment = .fill
        editCategoryButton.imageEdgeInsets = UIEdgeInsets(top: 13, left: 0, bottom: 13, right: 0)
        editCategoryButton.addTarget(self, action: #selector(didTapEditCategoryButton(sender :)), for: .touchUpInside)
        return editCategoryButton
    }()
    
    lazy var addMenuButton : UIButton = {
        let addMenuButton =  UIButton()
        addMenuButton.translatesAutoresizingMaskIntoConstraints = false
        addMenuButton.setTitle("Add Menu", for: .normal)
        addMenuButton.titleLabel?.font = UIFont(name: "ArialRoundedMTBold", size: 18)
        addMenuButton.setTitleColor(.systemGreen, for: .normal)
        addMenuButton.backgroundColor = .white
        addMenuButton.layer.borderWidth = 1
        addMenuButton.layer.borderColor = UIColor.systemGray5.cgColor
        addMenuButton.layer.cornerRadius = 5
        addMenuButton.dropShadow()
        addMenuButton.addTarget(self, action: #selector(didTapAddMenuButton(sender: )), for: .touchUpInside)
        return addMenuButton
    }()
    
    lazy var removeCategoryButton : UIButton = {
         let removeCategoryButton =  UIButton()
        removeCategoryButton.translatesAutoresizingMaskIntoConstraints = false
        removeCategoryButton.setTitle("Remove Category", for: .normal)
        removeCategoryButton.titleLabel?.font = UIFont(name: "ArialRoundedMTBold", size: 18)
        removeCategoryButton.setTitleColor(.systemRed, for: .normal)
        removeCategoryButton.backgroundColor = .white
        removeCategoryButton.titleLabel?.sizeToFit()
        removeCategoryButton.layer.borderWidth = 1
        removeCategoryButton.layer.borderColor = UIColor.systemGray5.cgColor
        removeCategoryButton.layer.cornerRadius = 5
        removeCategoryButton.dropShadow()
        removeCategoryButton.addTarget(self, action: #selector(didTapRemoveCategoryButton(sender: )), for: .touchUpInside)
        return removeCategoryButton
    }()
   
    
   

    private func getEditCategoryButtonConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [editCategoryButton.topAnchor.constraint(equalTo: self.topAnchor),editCategoryButton.heightAnchor.constraint(equalToConstant: 50),editCategoryButton.widthAnchor.constraint(equalToConstant: 30),editCategoryButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 20)]
        return buttonContraints
    }



    private func getMenuCategoryButtonConstraints() -> [NSLayoutConstraint]{
        let buttonConstraints = [menuCategoryButton.topAnchor.constraint(equalTo: self.topAnchor),menuCategoryButton.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -20),menuCategoryButton.leftAnchor.constraint(equalTo:  editCategoryButton.rightAnchor,constant: 10),
                                 menuCategoryButton.heightAnchor.constraint(equalToConstant: 50)]
        return buttonConstraints
    }
    
    private func getAddMenuButtonConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [addMenuButton.topAnchor.constraint(equalTo: self.topAnchor,constant: 55),addMenuButton.heightAnchor.constraint(equalToConstant: 40),addMenuButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 20),addMenuButton.rightAnchor.constraint(equalTo: self.centerXAnchor,constant: -20)]
        return buttonContraints
    }
    
    private func getRemoveCategoryButtonConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [removeCategoryButton.topAnchor.constraint(equalTo: self.topAnchor,constant: 55),removeCategoryButton.heightAnchor.constraint(equalToConstant: 40),removeCategoryButton.leftAnchor.constraint(equalTo: self.centerXAnchor,constant: 20),removeCategoryButton.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10)]
        return buttonContraints
    }
    
    @objc func didTapEditCategoryButton(sender : UIButton){
        delegate?.didTapEditCategoryButton(sender: sender)
    }
    
    @objc func didTapAddMenuButton(sender : UIButton){
        delegate?.didTapAddMenuButton(sender: sender)
    }
    
    @objc func  didTapRemoveCategoryButton(sender: UIButton){
        delegate?.didTapRemoveCategoryButton(sender: sender)
    }
    
    @objc func didTapMenuCategoryButton(sender : UIButton){
        delegate?.didTapMenuCategoryButton(sender: sender)
    }
}


protocol MenuCategoryHeaderViewDelegate : AnyObject{
     func didTapEditCategoryButton(sender : UIButton)
     func didTapAddMenuButton(sender : UIButton)
     func  didTapRemoveCategoryButton(sender: UIButton)
     func didTapMenuCategoryButton(sender : UIButton)
}
