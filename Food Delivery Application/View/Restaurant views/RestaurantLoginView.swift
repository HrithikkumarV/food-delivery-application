//
//  RestaurantloginPageView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 11/11/21.
//

import Foundation
import UIKit

class RestaurantLoginView : UIView, RestaurantLoginViewProtocol{
    
    weak var delegate : RestaurantLoginViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
    }
    
    
    
    private func initialiseViewElements(){
        self.addSubview(stackView)
        self.addSubview(orLable)
        self.addSubview(leftLine)
        self.addSubview(rightLine)
        self.addSubview(createAccountButton)
        NSLayoutConstraint.activate(getStackViewConstraints())
        NSLayoutConstraint.activate(getCreateAccountButtonContraints())
        NSLayoutConstraint.activate(getOrLabelWithLinesContraints())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var phoneNumberTextField : UITextField = {
        let phoneNumberTextField = CustomTextField()
        phoneNumberTextField.placeholder = "Phone number"
        phoneNumberTextField.addLabelToTopBorder(labelText: " Phone number ", fontSize: 15)
        phoneNumberTextField.backgroundColor = .white
        phoneNumberTextField.keyboardType = .phonePad
        let phoneNumberkeyboardNextBarButton = ToolBarUtils().createBarButton(title: "Next")
        let phoneNumberkeyboardToolBar = ToolBarUtils().createKeyboardToolBar()
        let flexibleSpaceForToolBar = ToolBarUtils().createFlexibleSpaceForToolBar()
        phoneNumberkeyboardToolBar.items = [flexibleSpaceForToolBar,phoneNumberkeyboardNextBarButton]
        phoneNumberTextField.inputAccessoryView = phoneNumberkeyboardToolBar
        phoneNumberkeyboardNextBarButton.target = self
        phoneNumberkeyboardNextBarButton.action = #selector(didTapPhoneNumberkeyboardNextBarButton)
        return phoneNumberTextField
    }()
    
    lazy var passwordTextField : UITextField = {
        passwordTextField = CustomTextField()
        passwordTextField.placeholder = "Password"
        passwordTextField.addLabelToTopBorder(labelText: "Password", fontSize: 15)
        passwordTextField.backgroundColor = .white
        passwordTextField.isSecureTextEntry = true
        passwordTextField.keyboardType = .default
        passwordTextField.returnKeyType = .continue
        passwordTextField.enablePasswordToggle()
        return passwordTextField
    }()
    
    lazy var changePasswordButton : UIButton = {
        let changePassword = UIButton()
        changePassword.setTitle("Forgot Password ?", for: .normal)
        changePassword.setTitleColor(.systemBlue, for: .normal)
        changePassword.titleLabel?.textAlignment = .center
        changePassword.addTarget(self, action: #selector(didTapChangePassword), for: .touchUpInside)
        return changePassword
    }()
    
    let orLable : UILabel = {
        let orLable = UILabel()
        orLable.text = "OR"
        orLable.textColor = .systemGray
        orLable.textAlignment = .center
        orLable.translatesAutoresizingMaskIntoConstraints = false
        return orLable
    }()
    
    let leftLine : UIView = {
        let leftLine = UIView()
        leftLine.backgroundColor = .systemGray
        leftLine.translatesAutoresizingMaskIntoConstraints = false
        return leftLine
    }()
    
    let rightLine : UIView = {
        let rightLine = UIView()
        rightLine.backgroundColor = .systemGray
        rightLine.translatesAutoresizingMaskIntoConstraints = false
        return rightLine
    }()
    
    lazy var createAccountButton : UIButton = {
        let createAccountButton = UIButton()
        createAccountButton.translatesAutoresizingMaskIntoConstraints = false
        createAccountButton.setTitle("Create New Account", for: .normal)
        createAccountButton.setTitleColor(.white, for: .normal)
        createAccountButton.backgroundColor = .systemGreen
        createAccountButton.layer.cornerRadius = 5
        createAccountButton.titleLabel?.adjustsFontSizeToFitWidth = true
        createAccountButton.addTarget(self, action: #selector(didTapCreateAccount), for: .touchUpInside)
        return createAccountButton
    }()
    
    lazy var loginButton : UIButton = {
        let loginButton = UIButton()
        loginButton.setTitle("Login", for: .normal)
        loginButton.setTitleColor(.white, for: .normal)
        loginButton.backgroundColor = .systemBlue
        loginButton.layer.cornerRadius = 5
        loginButton.addTarget(self, action: #selector(didTapLogin), for: .touchUpInside)
        return loginButton
    }()
    
    lazy var stackView : UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [phoneNumberTextField, passwordTextField ,loginButton ,changePasswordButton])
        stackView.axis = .vertical
        stackView.spacing = 15
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    
    private func getOrLabelWithLinesContraints() -> [NSLayoutConstraint]{
        let Contraints = [
            orLable.centerYAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 50),
            orLable.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            orLable.heightAnchor.constraint(equalToConstant: 30),
            orLable.widthAnchor.constraint(equalToConstant: 25),
            leftLine.heightAnchor.constraint(equalToConstant: 1),
            leftLine.leftAnchor.constraint(equalTo: self.leftAnchor ,constant: 40),
            leftLine.rightAnchor.constraint(equalTo: orLable.leftAnchor),
            leftLine.centerYAnchor.constraint(equalTo: orLable.centerYAnchor),
            rightLine.heightAnchor.constraint(equalToConstant: 1),
            rightLine.rightAnchor.constraint(equalTo: self.rightAnchor ,constant: -40),
            rightLine.leftAnchor.constraint(equalTo: orLable.rightAnchor),
            rightLine.centerYAnchor.constraint(equalTo: orLable.centerYAnchor)
        ]
        return Contraints
    }
    private func getCreateAccountButtonContraints() -> [NSLayoutConstraint]{
        let createAccountButtonConstraints = [
            createAccountButton.centerYAnchor.constraint(equalTo: orLable.bottomAnchor , constant: 50),
            createAccountButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -UIScreen.main.bounds.width/5),
            createAccountButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: UIScreen.main.bounds
                .width/5),
            createAccountButton.heightAnchor.constraint(equalToConstant: 50)
            ]
        return createAccountButtonConstraints
    }
    
   
    
    
    private func getStackViewConstraints() -> [NSLayoutConstraint]{
        let stackViewConstraints = [stackView.centerYAnchor.constraint(equalTo: self.topAnchor,constant: UIScreen.main.bounds.height/3),
       stackView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
       stackView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40),
       stackView.heightAnchor.constraint(equalToConstant: 240)]
        return stackViewConstraints
    }
    
    @objc func didTapLogin(){
        delegate?.didTapLogin()
    }
    
    @objc func didTapChangePassword(){
        delegate?.didTapChangePassword()
    }
    
    @objc func didTapCreateAccount(){
        delegate?.didTapCreateAccount()
    }
    
    @objc func didTapView(){
        delegate?.didTapView()
    }
    
    @objc func didTapPhoneNumberkeyboardNextBarButton(){
        delegate?.didTapPhoneNumberkeyboardNextBarButton()
    }
}

protocol RestaurantLoginViewDelegate :AnyObject{
    func didTapLogin()
    func didTapCreateAccount()
    func didTapChangePassword()
    func didTapView()
    func didTapPhoneNumberkeyboardNextBarButton()
}
