//
//  ChangePasswordPhoneNumberFormView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 20/12/21.
//

import Foundation

import UIKit

class ChangePasswordPhoneNumberFormView : UIView,    ChangePasswordPhoneNumberFormViewProtocol {
    
    weak var delegate : ChangePasswordPhoneNumberFormViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
    }
    
    private func initialiseViewElements(){
        self.addSubview(phoneNumberTextField)
        self.addSubview(verifyButton)
        NSLayoutConstraint.activate(getPhoneNumberTextFieldConstraints())
        NSLayoutConstraint.activate(getVerifyButtonContraints())
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy  var phoneNumberTextField : UITextField = {
        let phoneNumberTextField = createTextField()
        phoneNumberTextField.placeholder = "Registered Phone number"
        phoneNumberTextField.addLabelToTopBorder(labelText: " Phone Number ", fontSize: 15)
        phoneNumberTextField.keyboardType = .phonePad
        let phoneNumberkeyboardNextBarButton = ToolBarUtils().createBarButton(title: "Next")
        let phoneNumberkeyboardToolBar = ToolBarUtils().createKeyboardToolBar()
        let flexibleSpaceForToolBar = ToolBarUtils().createFlexibleSpaceForToolBar()
        phoneNumberkeyboardToolBar.items = [flexibleSpaceForToolBar,phoneNumberkeyboardNextBarButton]
        phoneNumberTextField.inputAccessoryView = phoneNumberkeyboardToolBar
        phoneNumberkeyboardNextBarButton.target = self
        phoneNumberkeyboardNextBarButton.action = #selector(didTapPhoneNumberkeyboardNextBarButton)
        return phoneNumberTextField
    }()
    
    
    lazy var verifyButton : UIButton = {
        let verifyButton = UIButton()
        verifyButton.setTitle("Verify", for: .normal)
        verifyButton.setTitleColor(.white, for: .normal)
        verifyButton.backgroundColor = .systemBlue
        verifyButton.translatesAutoresizingMaskIntoConstraints = false
        verifyButton.backgroundColor = .systemBlue
        verifyButton.layer.borderWidth = 1
        verifyButton.layer.borderColor = UIColor.systemGray6.cgColor
        verifyButton.layer.cornerRadius = 5
        verifyButton.addTarget(self, action: #selector(didTapVerify), for: .touchUpInside)
        return verifyButton
    }()
   
    
  
    
   
    private func createTextField() -> UITextField{
        let textField = CustomTextField()
        textField .backgroundColor = .white
        textField .textColor = .black
        textField .translatesAutoresizingMaskIntoConstraints = false
        return textField
    }
   
  
   
   
   
    
    private func getPhoneNumberTextFieldConstraints() -> [NSLayoutConstraint]{
        let textFieldContraints = [phoneNumberTextField.topAnchor.constraint(equalTo: self.topAnchor , constant: 20),
                                   phoneNumberTextField.leftAnchor.constraint(equalTo: self.leftAnchor ,constant: 40),
                                   phoneNumberTextField.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -40),
                                   phoneNumberTextField.heightAnchor.constraint(equalToConstant : 50)]
        return textFieldContraints
    }
    
    private func getVerifyButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [ verifyButton.topAnchor.constraint(equalTo: phoneNumberTextField.bottomAnchor, constant: 10),
                                 verifyButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
                                 verifyButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40),
                                 verifyButton.heightAnchor.constraint(equalToConstant: 50)]
        return buttonContraints
    }
    
    
    @objc func didTapVerify(){
        delegate?.didTapVerify()
    }
    
    @objc func didTapPhoneNumberkeyboardNextBarButton(){
        delegate?.didTapPhoneNumberkeyboardNextBarButton()
    }
    
}


protocol ChangePasswordPhoneNumberFormViewDelegate : AnyObject{
    func didTapVerify()
    func didTapPhoneNumberkeyboardNextBarButton()
}
