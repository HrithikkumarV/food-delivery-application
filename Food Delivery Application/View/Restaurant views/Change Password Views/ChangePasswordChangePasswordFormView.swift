//
//  ChangePasswordChangePasswordView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 20/12/21.
//

import Foundation
import UIKit

class ChangePasswordChangePasswordFormView :  UIView, ChangePasswordChangePasswordFormViewProtocol {

   
    weak var delegate : ChangePasswordChangePasswordFormViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        self.addSubview(stackView)
        self.addSubview(saveChangesButton)
        NSLayoutConstraint.activate(getStackViewConstraints())
    }
    
    
    lazy var saveChangesButton : UIButton = {
        let saveChangesButton = UIButton()
        saveChangesButton.translatesAutoresizingMaskIntoConstraints = false
        saveChangesButton.setTitle("save changes", for: .normal)
        saveChangesButton.setTitleColor(.white, for: .normal)
        saveChangesButton.backgroundColor = .systemBlue
        saveChangesButton.layer.cornerRadius = 5
        saveChangesButton.addTarget(self, action: #selector(didTapSaveChangesButton), for: .touchUpInside)
        return saveChangesButton
    }()
    lazy var stackView : UIStackView = {
        let  stackView = UIStackView(arrangedSubviews: [passwordTextField,confirmPasswordTextField ,saveChangesButton])
        stackView.axis = .vertical
        stackView.spacing = 15
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    
    private func createTextField() -> UITextField{
        let textField = CustomTextField()
        textField .backgroundColor = .white
        textField .textColor = .black
        return textField
    }
    
    
    lazy var passwordTextField : UITextField = {
        let passwordTextField = createTextField()
        passwordTextField.placeholder = "New Password"
        passwordTextField.addLabelToTopBorder(labelText: " New Password ", fontSize: 15)
        passwordTextField.isSecureTextEntry = true
        
        passwordTextField.keyboardType = .default
        passwordTextField.returnKeyType = .next
        passwordTextField.autocorrectionType = .no
        passwordTextField.textContentType = .newPassword
        passwordTextField.enablePasswordToggle()
        return passwordTextField
    }()
    
    lazy var confirmPasswordTextField :  UITextField = {
        let confirmPasswordTextField = createTextField()
        confirmPasswordTextField.placeholder = "Confirm New Password"
        confirmPasswordTextField.addLabelToTopBorder(labelText: " Confirm New Password ", fontSize: 15)
        confirmPasswordTextField.isSecureTextEntry = true
        confirmPasswordTextField.keyboardType = .default
        confirmPasswordTextField.returnKeyType = .done
        confirmPasswordTextField.autocorrectionType = .no
        confirmPasswordTextField.textContentType = .newPassword
        confirmPasswordTextField.enablePasswordToggle()
        return confirmPasswordTextField
    }()
    
    
    
    
    
    
    
  

    private func getStackViewConstraints() -> [NSLayoutConstraint]{
        let stackViewConstraints = [stackView.topAnchor.constraint(equalTo:self.topAnchor ,constant: 20),
                                    stackView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
                                    stackView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40),
                                    stackView.heightAnchor.constraint(equalToConstant: 195)]
        return stackViewConstraints
    }
    
  
   
    @objc func didTapSaveChangesButton(){
        delegate?.didTapSaveChangesButton()
    }
    
    
    @objc func didTapView(){
        delegate?.didTapView()
    }
}

protocol ChangePasswordChangePasswordFormViewDelegate : AnyObject{
     func didTapSaveChangesButton()
    
     func didTapView()
}
