//
//  RestaurantCreateAccountRestaurantDetailsView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 08/12/21.
//

import Foundation

import UIKit
class RestaurantCreateAccountRestaurantDetailsView : UIView,    RestaurantCreateAccountRestaurantDetailsViewProtocol {
   
    weak var delegate : RestaurantCreateAccountRestaurantDetailsViewDelegate?
   
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        createAddRestaurantProfileImageSectionView()
        self.addSubview(stackViewForRestaurantDetails)
        self.addSubview(continueTosetAddressButton)
        NSLayoutConstraint.activate(getCreateAccountRestaurantDetailsStackViewConstraints())
        NSLayoutConstraint.activate(getContinueToSetAddressButtonContraints())
    }
    
    
    lazy var restaurantProfileImageSectionView : UIView = {
        let restaurantProfileImageSectionView = UIView()
        restaurantProfileImageSectionView.addLabelToTopBorder(labelText: " Restaurant Image ", borderColor: .systemGray5, borderWidth: 1,leftPadding: 15, topPadding: 0,textColor: .lightGray, fontSize: 15)
        restaurantProfileImageSectionView.translatesAutoresizingMaskIntoConstraints =  false
        restaurantProfileImageSectionView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        return restaurantProfileImageSectionView
    }()
    
    lazy var continueTosetAddressButton : UIButton = {
        let  continueTosetAddressButton = UIButton()
        continueTosetAddressButton.translatesAutoresizingMaskIntoConstraints = false
        continueTosetAddressButton.setTitle("continue", for: .normal)
        continueTosetAddressButton.setTitleColor(.white, for: .normal)
        continueTosetAddressButton.backgroundColor = .systemBlue
        continueTosetAddressButton.layer.borderWidth = 1
        continueTosetAddressButton.layer.borderColor = UIColor.systemGray6.cgColor
        continueTosetAddressButton.layer.cornerRadius = 5
        continueTosetAddressButton.addTarget(self, action: #selector(didTapContinueToSetAddress), for: .touchUpInside)
        return continueTosetAddressButton
    }()
    
    lazy var stackViewForRestaurantDetails : UIStackView = {
        let  stackViewForRestaurantDetails = UIStackView(arrangedSubviews: [restaurantNameTextField,restaurantCuisineButton,restaurantDescriptionTextView,restaurantProfileImageSectionView])
        stackViewForRestaurantDetails.axis = .vertical
        stackViewForRestaurantDetails.spacing = 15
        stackViewForRestaurantDetails.distribution = .fillProportionally
        stackViewForRestaurantDetails.translatesAutoresizingMaskIntoConstraints = false
        return stackViewForRestaurantDetails
    }()
    
    
   
    
    
    lazy var restaurantNameTextField : UITextField = {
        let restaurantNameTextField = createTextField()
        restaurantNameTextField.placeholder = "Restaurant Name"
        restaurantNameTextField.addLabelToTopBorder(labelText: " Restaurant Name ", fontSize: 15)
        restaurantNameTextField.keyboardType = .default
        restaurantNameTextField.returnKeyType = .next
        restaurantNameTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        return restaurantNameTextField
    }()
    
    lazy var restaurantCuisineButton : UIButton = {
       
        let button = UIButton()
        button.setTitle("Restaurant Cuisine", for: .normal)
        button.addLabelToTopBorder(labelText: " Restaurant Cuisine ", borderColor: .systemGray5, borderWidth: 1,leftPadding: 15, topPadding: 0,textColor: .lightGray, fontSize: 15)
        button.hideTopBorderLabelInView()
        button.contentHorizontalAlignment = .left
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        button.setTitleColor(UIColor.systemGray3, for: .normal)
        button.titleLabel?.textAlignment = .left
        button.backgroundColor = .white
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.addRightImageToButton(systemName: "chevron.down", imageColor: .lightGray, padding: 20)
        button.addTarget(self, action: #selector(didTapRestaurantCuisine), for: .touchUpInside)
        return button
    }()
    
   
    
    
    
    lazy var restaurantDescriptionTextView : UITextView =  {
        let restaurantDescriptionTextView = UITextView()
        restaurantDescriptionTextView.returnKeyType = .default
        restaurantDescriptionTextView.backgroundColor = .white
        restaurantDescriptionTextView.keyboardType = .default
        restaurantDescriptionTextView.textColor = .systemGray3
        restaurantDescriptionTextView.autocorrectionType = .no
        restaurantDescriptionTextView.font = UIFont(name: "ArialHebrew", size: 16)
        restaurantDescriptionTextView.translatesAutoresizingMaskIntoConstraints = false
        restaurantDescriptionTextView.layer.borderColor = UIColor.systemGray5.cgColor
        restaurantDescriptionTextView.layer.borderWidth = 1
        restaurantDescriptionTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        let restaurantDescriptionKeyboardNextBarButton = ToolBarUtils().createBarButton(title: "Next")
        let restaurantDescriptionKeyboardToolBar = ToolBarUtils().createKeyboardToolBar()
        let flexibleSpaceForToolBar = ToolBarUtils().createFlexibleSpaceForToolBar()
        restaurantDescriptionKeyboardToolBar.items = [flexibleSpaceForToolBar,restaurantDescriptionKeyboardNextBarButton]
        restaurantDescriptionTextView.inputAccessoryView = restaurantDescriptionKeyboardToolBar
        restaurantDescriptionKeyboardNextBarButton.target = self
        restaurantDescriptionKeyboardNextBarButton.action = #selector(didTapRestaurantDescriptionKeyboardNextBarButton)
        return restaurantDescriptionTextView
    }()
    
    
    lazy var addRestaurantProfileImageButton : UIButton = {
        let addRestaurantProfileImageButton = UIButton()
        addRestaurantProfileImageButton.setTitle(" Upload Restaurant Image", for: .normal)
        addRestaurantProfileImageButton.setTitleColor(.systemBlue, for: .normal)
        addRestaurantProfileImageButton.backgroundColor = .white
        addRestaurantProfileImageButton.translatesAutoresizingMaskIntoConstraints = false
        addRestaurantProfileImageButton.layer.borderColor = UIColor.systemGray5.cgColor
        addRestaurantProfileImageButton.layer.borderWidth = 1
        addRestaurantProfileImageButton.layer.cornerRadius = 5
        addRestaurantProfileImageButton.titleLabel?.adjustsFontSizeToFitWidth = true
        addRestaurantProfileImageButton.addTarget(self, action: #selector(didTapAddRestaurantProfileImage), for: .touchUpInside)
        return addRestaurantProfileImageButton
    }()
    
    lazy var restaurantProfileImageView :  UIImageView = {
        let restaurantProfileImageView = UIImageView()
        restaurantProfileImageView.layer.borderColor = UIColor.systemGray.cgColor
        restaurantProfileImageView.layer.borderWidth = 1
        restaurantProfileImageView.translatesAutoresizingMaskIntoConstraints = false
        return restaurantProfileImageView
    }()
    
    private func createTextField() -> UITextField{
        let textField = CustomTextField()
        textField.addBorder(side: .top, color: .systemGray5, width: 1)
        textField.addBorder(side: .bottom, color: .systemGray5, width: 1)
        textField.addBorder(side: .left, color: .systemGray5, width: 1)
        textField.addBorder(side: .right, color: .systemGray5, width: 1)
        textField .backgroundColor = .white
        textField .textColor = .black
        textField .translatesAutoresizingMaskIntoConstraints = false
        textField.autocorrectionType = .no
        return textField
    }
    
    
    private func createAddRestaurantProfileImageSectionView(){
        restaurantProfileImageSectionView.addSubview(addRestaurantProfileImageButton)
        restaurantProfileImageSectionView.addSubview(restaurantProfileImageView)
        applyRestaurantProfileImageViewConstraints()
        applyAddRestaurantProfileImageButtonConstraints()
    }
    
    
    private func applyAddRestaurantProfileImageButtonConstraints(){
        addRestaurantProfileImageButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        addRestaurantProfileImageButton.centerYAnchor.constraint(equalTo: restaurantProfileImageSectionView.centerYAnchor).isActive = true
        
        addRestaurantProfileImageButton.leftAnchor.constraint(equalTo: restaurantProfileImageSectionView.leftAnchor , constant: 120).isActive = true
        
        addRestaurantProfileImageButton.rightAnchor.constraint(equalTo: restaurantProfileImageSectionView.rightAnchor , constant: -20).isActive = true
        
    }
    
    private func applyRestaurantProfileImageViewConstraints(){
        restaurantProfileImageView.topAnchor.constraint(equalTo: restaurantProfileImageSectionView.topAnchor ,constant: 10).isActive = true
        
        restaurantProfileImageView.bottomAnchor.constraint(equalTo: restaurantProfileImageSectionView.bottomAnchor ,constant: -10).isActive = true
        
        restaurantProfileImageView.leftAnchor.constraint(equalTo: restaurantProfileImageSectionView.leftAnchor ,constant: 10).isActive = true
        
        restaurantProfileImageView.rightAnchor.constraint(equalTo: restaurantProfileImageSectionView.leftAnchor ,constant: 110).isActive = true
    }
    

    private func getCreateAccountRestaurantDetailsStackViewConstraints() -> [NSLayoutConstraint]{
        let stackViewConstraints = [stackViewForRestaurantDetails.topAnchor.constraint(equalTo:self.topAnchor,constant: 10),
                                    stackViewForRestaurantDetails.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
                                    stackViewForRestaurantDetails.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40)
        ]
        return stackViewConstraints
    }
    

    
   
   
    
    
    private func getContinueToSetAddressButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [continueTosetAddressButton.topAnchor.constraint(equalTo:stackViewForRestaurantDetails.bottomAnchor ,constant: 40),
                                continueTosetAddressButton.leftAnchor.constraint(equalTo: stackViewForRestaurantDetails.leftAnchor,constant: 40),
                                continueTosetAddressButton.rightAnchor.constraint(equalTo: stackViewForRestaurantDetails.rightAnchor,constant: -40),
                                continueTosetAddressButton.heightAnchor.constraint(equalToConstant: 50)]
        return buttonContraints
    }
    
    
    @objc func didTapView(){
        delegate?.didTapView()
    }
    
    @objc func didTapContinueToSetAddress(){
        delegate?.didTapContinueToSetAddress()
    }
    
    @objc func didTapRestaurantDescriptionKeyboardNextBarButton(){
        delegate?.didTapRestaurantDescriptionKeyboardNextBarButton()
    }
    
    @objc func didTapRestaurantCuisine(){
        delegate?.didTapRestaurantCuisine()
    }
    
    @objc func didTapAddRestaurantProfileImage(){
        delegate?.didTapAddRestaurantProfileImage()
    }
}


protocol RestaurantCreateAccountRestaurantDetailsViewDelegate : AnyObject{
    func didTapView()
    func didTapContinueToSetAddress()
    func didTapRestaurantDescriptionKeyboardNextBarButton()
     func didTapRestaurantCuisine()
    func didTapAddRestaurantProfileImage()
}
