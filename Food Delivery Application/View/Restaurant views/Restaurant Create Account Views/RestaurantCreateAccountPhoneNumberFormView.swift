//
//  RestaurantCreateAccountPhoneNumberFormView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 08/12/21.
//

import Foundation
import UIKit

class RestaurantCreateAccountPhoneNumberFormView : UIView , RestaurantCreateAccountPhoneNumberFormViewProtocol{
    
    weak var delegate : RestaurantCreateAccountPhoneNumberFormViewDelegate?
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
    }
   
    
    private func initialiseViewElements(){
        self.addSubview(phoneNumberTextField)
        self.addSubview(verifyWithOTPButton)
        self.addSubview(welcomeLabel)
        NSLayoutConstraint.activate(getPhoneNumberTextFieldConstraints())
        NSLayoutConstraint.activate(getVerifyWithOTPButtonContraints())
        NSLayoutConstraint.activate(getWelcomeLabelContraints())

    }
    lazy var phoneNumberTextField : UITextField = {
        let phoneNumberTextField = createTextField()
        phoneNumberTextField.placeholder = "Phone Number"
        phoneNumberTextField.addLabelToTopBorder(labelText: " Phone Number ", fontSize: 15)
        phoneNumberTextField.keyboardType = .phonePad
        let phoneNumberkeyboardNextBarButton = ToolBarUtils().createBarButton(title: "Next")
        phoneNumberkeyboardNextBarButton.target = self
        phoneNumberkeyboardNextBarButton.action = #selector(didTapPhoneNumberkeyboardNextBarButton)
        let phoneNumberkeyboardToolBar = ToolBarUtils().createKeyboardToolBar()
        let flexibleSpaceForToolBar = ToolBarUtils().createFlexibleSpaceForToolBar()
        phoneNumberkeyboardToolBar.items = [flexibleSpaceForToolBar,phoneNumberkeyboardNextBarButton]
        phoneNumberTextField.inputAccessoryView = phoneNumberkeyboardToolBar
        
        return phoneNumberTextField
    }()
    
    lazy var verifyWithOTPButton : UIButton = {
        let verifyWithOTPButton = UIButton()
        verifyWithOTPButton.setTitle("Verify With OTP", for: .normal)
        verifyWithOTPButton.setTitleColor(.white, for: .normal)
        verifyWithOTPButton.backgroundColor = .systemBlue
        verifyWithOTPButton.translatesAutoresizingMaskIntoConstraints = false
        verifyWithOTPButton.backgroundColor = .systemBlue
        verifyWithOTPButton.layer.borderWidth = 1
        verifyWithOTPButton.layer.borderColor = UIColor.systemGray6.cgColor
        verifyWithOTPButton.layer.cornerRadius = 5
        verifyWithOTPButton.addTarget(self, action: #selector(didTapVerifyWithOTP), for: .touchUpInside)
        
        return verifyWithOTPButton
    }()
    var welcomeLabel : UILabel = {
        let welcomeLabel = UILabel()
        welcomeLabel.text = "Register with us"
        welcomeLabel.textColor = .black
        welcomeLabel.translatesAutoresizingMaskIntoConstraints = false
        welcomeLabel.backgroundColor = .white
        
        return welcomeLabel
    }()
    
   
    private func createTextField() -> UITextField{
        let textField = CustomTextField()
        textField .backgroundColor = .white
        textField .textColor = .black
        textField .translatesAutoresizingMaskIntoConstraints = false
        return textField
    }
    
   
    
    
   
   
    
    
    
    private func getWelcomeLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [welcomeLabel.topAnchor.constraint(equalTo: self.topAnchor,constant: 10),
                               welcomeLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
                               welcomeLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40),
                               welcomeLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    
    private func getPhoneNumberTextFieldConstraints() -> [NSLayoutConstraint]{
        let textFieldContraints = [phoneNumberTextField.topAnchor.constraint(equalTo: welcomeLabel.bottomAnchor ,constant: 15),
                                   phoneNumberTextField.leftAnchor.constraint(equalTo: self.leftAnchor ,constant: 40),
                                   phoneNumberTextField.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -40),
                                   phoneNumberTextField.heightAnchor.constraint(equalToConstant : 50)]
        return textFieldContraints
    }
    
    private func getVerifyWithOTPButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [ verifyWithOTPButton.topAnchor.constraint(equalTo: phoneNumberTextField.bottomAnchor, constant: 10),
                                 verifyWithOTPButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
                                 verifyWithOTPButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40),
                                 verifyWithOTPButton.heightAnchor.constraint(equalToConstant: 50)]
        return buttonContraints
    }
    
    @objc func didTapVerifyWithOTP(){
        delegate?.didTapVerifyWithOTP()
    }
    
    @objc func didTapPhoneNumberkeyboardNextBarButton(){
        delegate?.didTapPhoneNumberkeyboardNextBarButton()
    }
}

protocol RestaurantCreateAccountPhoneNumberFormViewDelegate : AnyObject{
    func didTapVerifyWithOTP()
     func didTapPhoneNumberkeyboardNextBarButton()
}
