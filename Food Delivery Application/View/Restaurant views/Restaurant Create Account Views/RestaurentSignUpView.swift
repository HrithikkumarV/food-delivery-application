////
////  RestaurentSignUpView.swift
////  Food Delivery Application
////
////  Created by Hrithik Kumar V on 12/11/21.
////
//
//import Foundation
//import UIKit
//class RestaurentSignUpView {
//
//
//
//    private let rootView : UIView
//    private var welcomeLabel : UILabel!
//    private var resendOTPLabel : UILabel!
//    private var emailIdTextField : UITextField!
//    private var phoneNumberTextField : UITextField!
//    private var OTPTextField : UITextField!
//    private var continueForPhoneNumberButton : UIButton!
//    private var continueForRestaurentDetailsButton : UIButton!
//    private var generateOTPButton : UIButton!
//    private var signUpButton : UIButton!
//    private var continueTosetAddressButton : UIButton!
//    private var stackViewForRestaurentDetails : UIStackView!
//    private var stackViewForAuthenticationDetails : UIStackView!
//
//    init(rootView : UIView){
//        self.rootView = rootView
//    }
//
//    func createWelcomeLabel() -> UILabel{
//        welcomeLabel = UILabel()
//        welcomeLabel.text = "Register with us"
//        welcomeLabel.textColor = .black
//        welcomeLabel.translatesAutoresizingMaskIntoConstraints = false
//        welcomeLabel.backgroundColor = .white
//        rootView.addSubview(welcomeLabel)
//        return welcomeLabel
//    }
//    private func createTextField() -> UITextField{
//        let textField = UITextField()
//        textField .borderStyle = .roundedRect
//        textField .backgroundColor = .white
//        textField .textColor = .black
//        textField .translatesAutoresizingMaskIntoConstraints = false
//        return textField
//    }
//    func createEmailIdTextField() -> UITextField{
//        emailIdTextField = createTextField()
//        emailIdTextField.placeholder = "Email Address"
//        emailIdTextField.keyboardType = .emailAddress
//        emailIdTextField.returnKeyType = .next
//
//        rootView.addSubview(emailIdTextField)
//        return emailIdTextField
//    }
//    func createPhoneNumberTextField() -> UITextField{
//        phoneNumberTextField = createTextField()
//        phoneNumberTextField.placeholder = "Phone number"
//        phoneNumberTextField.keyboardType = .phonePad
//        phoneNumberTextField.isHidden = true
//        rootView.addSubview(phoneNumberTextField)
//        return phoneNumberTextField
//    }
//
//    func createOTPTextField() -> UITextField{
//        OTPTextField = createTextField()
//        OTPTextField.placeholder = "OTP"
//        OTPTextField.keyboardType = .numberPad
//        OTPTextField.isHidden = true
//        rootView.addSubview(OTPTextField)
//        return OTPTextField
//    }
//
//    func createRestaurentNameTextField() -> UITextField{
//        let restaurentNameTextField = createTextField()
//        restaurentNameTextField.placeholder = "Restaurent Name"
//       restaurentNameTextField.keyboardType = .default
//        restaurentNameTextField.returnKeyType = .next
//        rootView.addSubview(restaurentNameTextField)
//        return restaurentNameTextField
//    }
//
//    func createRestaurentTypeTextField() -> UITextField{
//        let restaurentTypeTextField = createTextField()
//        restaurentTypeTextField.placeholder = "Restaurent Type"
//        restaurentTypeTextField.keyboardType = .default
//        restaurentTypeTextField.returnKeyType = .next
//        rootView.addSubview(restaurentTypeTextField)
//        return restaurentTypeTextField
//    }
//
//    func createSecretCodeTextField() -> UITextField{
//        let secretCodeTextField = createTextField()
//        secretCodeTextField.placeholder = "Secret Code"
//         secretCodeTextField.keyboardType = .numberPad
//        secretCodeTextField.returnKeyType = .next
//        rootView.addSubview(secretCodeTextField)
//        return secretCodeTextField
//    }
//    func createPasswordTextField() -> UITextField{
//        let passwordTextField = createTextField()
//        passwordTextField.placeholder = "Password"
//        passwordTextField.isSecureTextEntry = true
//        passwordTextField.keyboardType = .default
//        passwordTextField.returnKeyType = .next
//        rootView.addSubview(passwordTextField)
//        return passwordTextField
//    }
//    func createConfirmPasswordTextField() -> UITextField{
//        let confirmPasswordTextField = createTextField()
//        confirmPasswordTextField.placeholder = "Confirm Password"
//        confirmPasswordTextField.isSecureTextEntry = true
//        confirmPasswordTextField.keyboardType = .default
//        confirmPasswordTextField.returnKeyType = .done
//        rootView.addSubview(confirmPasswordTextField)
//        return confirmPasswordTextField
//    }
//
//
//    func createRestaurentDescriptionTextView() -> UITextView {
//        let restaurentDescriptionTextView = UITextView()
//        restaurentDescriptionTextView.text = "Write the Description about your restaurant"
//        restaurentDescriptionTextView.returnKeyType = .default
//        restaurentDescriptionTextView.backgroundColor = .white
//        restaurentDescriptionTextView.keyboardType = .default
//        restaurentDescriptionTextView.textColor = .systemGray3
//        restaurentDescriptionTextView.font = UIFont(name: "ArialHebrew", size: 16)
//        restaurentDescriptionTextView.translatesAutoresizingMaskIntoConstraints = false
//        restaurentDescriptionTextView.layer.cornerRadius = 5
//        restaurentDescriptionTextView.layer.borderWidth = 1
//        restaurentDescriptionTextView.layer.borderColor = UIColor.systemGray5.cgColor
//        rootView.addSubview(restaurentDescriptionTextView)
//        return restaurentDescriptionTextView
//    }
//
//    func setUpSignUpRestaurentDetailsStackView(restaurentName : UITextField, restaurentType : UITextField ,restaurentDescription : UITextView, continueToSetAddressButton : UIButton) -> UIStackView{
//        stackViewForRestaurentDetails = UIStackView(arrangedSubviews: [restaurentName,restaurentType,restaurentDescription ,continueToSetAddressButton])
//        stackViewForRestaurentDetails.axis = .vertical
//        stackViewForRestaurentDetails.spacing = 10
//        stackViewForRestaurentDetails.distribution = .fillEqually
//        stackViewForRestaurentDetails.translatesAutoresizingMaskIntoConstraints = false
//        stackViewForRestaurentDetails.isHidden = true
//        rootView.addSubview(stackViewForRestaurentDetails)
//        return stackViewForRestaurentDetails
//    }
//
//
//    func getSignUpRestaurentDetailsStackViewConstraints() -> [NSLayoutConstraint]{
//        let stackViewConstraints = [stackViewForRestaurentDetails.topAnchor.constraint(equalTo:rootView.topAnchor, constant: 150),
//                                    stackViewForRestaurentDetails.leftAnchor.constraint(equalTo: rootView.leftAnchor, constant: 40),
//                                    stackViewForRestaurentDetails.rightAnchor.constraint(equalTo: rootView.rightAnchor, constant: -40),
//                                    stackViewForRestaurentDetails.heightAnchor.constraint(equalToConstant: 240)]
//        return stackViewConstraints
//    }
//
//
//    func setUpSignUpAuthenticationDetailsStackView(secretCode : UITextField, password : UITextField ,ConfirmPassword : UITextField, signUpButton : UIButton) -> UIStackView{
//        stackViewForAuthenticationDetails = UIStackView(arrangedSubviews: [secretCode,password,ConfirmPassword ,signUpButton])
//        stackViewForAuthenticationDetails.axis = .vertical
//        stackViewForAuthenticationDetails.spacing = 10
//        stackViewForAuthenticationDetails.distribution = .fillEqually
//        stackViewForAuthenticationDetails.translatesAutoresizingMaskIntoConstraints = false
//        stackViewForAuthenticationDetails.isHidden = true
//        rootView.addSubview(stackViewForAuthenticationDetails)
//        return stackViewForAuthenticationDetails
//    }
//
//
//    func getSignUpAuthenticationDetailsStackViewConstraints() -> [NSLayoutConstraint]{
//        let stackViewConstraints = [stackViewForAuthenticationDetails.topAnchor.constraint(equalTo:rootView.topAnchor, constant: 150),
//                                    stackViewForAuthenticationDetails.leftAnchor.constraint(equalTo: rootView.leftAnchor, constant: 40),
//                                    stackViewForAuthenticationDetails.rightAnchor.constraint(equalTo: rootView.rightAnchor, constant: -40),
//                                    stackViewForAuthenticationDetails.heightAnchor.constraint(equalToConstant: 240)]
//        return stackViewConstraints
//    }
//
//    func getEmailIdTextFieldConstraints() -> [NSLayoutConstraint]{
//        let textFieldContraints = [emailIdTextField.topAnchor.constraint(equalTo: rootView.topAnchor , constant: 200),
//         emailIdTextField.leftAnchor.constraint(equalTo: rootView.leftAnchor ,constant: 40),
//         emailIdTextField.rightAnchor.constraint(equalTo: rootView.rightAnchor,constant: -40),
//         emailIdTextField.heightAnchor.constraint(equalToConstant : 50)]
//        return textFieldContraints
//    }
//
//    func getWelcomeLabelContraints() -> [NSLayoutConstraint]{
//        let labelContraints = [welcomeLabel.topAnchor.constraint(equalTo: rootView.topAnchor, constant: 160),
//                               welcomeLabel.leftAnchor.constraint(equalTo: rootView.leftAnchor, constant: 40),
//                               welcomeLabel.rightAnchor.constraint(equalTo: rootView.rightAnchor, constant: -40),
//                               welcomeLabel.heightAnchor.constraint(equalToConstant: 30)]
//        return labelContraints
//    }
//
//
//    func createSignUpButton() -> UIButton {
//        signUpButton = UIButton()
//        signUpButton.translatesAutoresizingMaskIntoConstraints = false
//        signUpButton.setTitle("Sign Up", for: .normal)
//        signUpButton.setTitleColor(.white, for: .normal)
//        signUpButton.backgroundColor = .systemBlue
//        signUpButton.layer.cornerRadius = 5
//        rootView.addSubview(signUpButton)
//        return signUpButton
//    }
//
//    func createContinueForPhoneNumberButton() -> UIButton {
//        continueForPhoneNumberButton = UIButton()
//        continueForPhoneNumberButton.translatesAutoresizingMaskIntoConstraints = false
//        continueForPhoneNumberButton.setTitle("Continue", for: .normal)
//        continueForPhoneNumberButton.setTitleColor(.white, for: .normal)
//        continueForPhoneNumberButton.backgroundColor = .systemBlue
//        continueForPhoneNumberButton.layer.cornerRadius = 5
//        rootView.addSubview(continueForPhoneNumberButton)
//        return continueForPhoneNumberButton
//    }
//    
//    func createContinueForRestaurentDetailsButton() -> UIButton {
//        continueForRestaurentDetailsButton = UIButton()
//        continueForRestaurentDetailsButton.translatesAutoresizingMaskIntoConstraints = false
//        continueForRestaurentDetailsButton.setTitle("Continue", for: .normal)
//        continueForRestaurentDetailsButton.setTitleColor(.white, for: .normal)
//        continueForRestaurentDetailsButton.backgroundColor = .systemBlue
//        continueForRestaurentDetailsButton.layer.cornerRadius = 5
//        continueForRestaurentDetailsButton.isHidden = true
//        rootView.addSubview(continueForRestaurentDetailsButton)
//        return continueForRestaurentDetailsButton
//    }
//
//    func getContinueForPhoneNumberButtonContraints() -> [NSLayoutConstraint]{
//        let buttonContraints = [ continueForPhoneNumberButton.topAnchor.constraint(equalTo: emailIdTextField.bottomAnchor, constant: 10),
//                                 continueForPhoneNumberButton.leftAnchor.constraint(equalTo: rootView.leftAnchor, constant: 40),
//                                 continueForPhoneNumberButton.rightAnchor.constraint(equalTo: rootView.rightAnchor, constant: -40),
//                                 continueForPhoneNumberButton.heightAnchor.constraint(equalToConstant: 50)]
//        return buttonContraints
//    }
//
//    func getContinueForRestaurentDetailsButtonContraints() -> [NSLayoutConstraint]{
//        let buttonContraints = [ continueForRestaurentDetailsButton.topAnchor.constraint(equalTo: OTPTextField.bottomAnchor, constant: 10),
//                                 continueForRestaurentDetailsButton.leftAnchor.constraint(equalTo: rootView.leftAnchor, constant: 40),
//                                 continueForRestaurentDetailsButton.rightAnchor.constraint(equalTo: rootView.rightAnchor, constant: -40),
//                                 continueForRestaurentDetailsButton.heightAnchor.constraint(equalToConstant: 50)]
//        return buttonContraints
//    }
//
//    func createContinueToSetAddressButton() -> UIButton {
//        continueTosetAddressButton = UIButton()
//        continueTosetAddressButton.translatesAutoresizingMaskIntoConstraints = false
//        continueTosetAddressButton.setTitle("continue", for: .normal)
//        continueTosetAddressButton.setTitleColor(.white, for: .normal)
//        continueTosetAddressButton.backgroundColor = .systemBlue
//        continueTosetAddressButton.layer.borderWidth = 1
//        continueTosetAddressButton.layer.borderColor = UIColor.systemGray6.cgColor
//        continueTosetAddressButton.layer.cornerRadius = 5
//        rootView.addSubview(continueTosetAddressButton)
//        return continueTosetAddressButton
//    }
//
//    func createGenerateOTPButton() -> UIButton{
//        generateOTPButton = UIButton()
//        generateOTPButton.setTitle("Generate OTP", for: .normal)
//        generateOTPButton.setTitleColor(.white, for: .normal)
//        generateOTPButton.backgroundColor = .systemBlue
//        generateOTPButton.translatesAutoresizingMaskIntoConstraints = false
//        generateOTPButton.backgroundColor = .systemBlue
//        generateOTPButton.layer.borderWidth = 1
//        generateOTPButton.layer.borderColor = UIColor.systemGray6.cgColor
//        generateOTPButton.layer.cornerRadius = 5
//        generateOTPButton.isHidden = true
//        rootView.addSubview(generateOTPButton)
//        return generateOTPButton
//    }
//
//    func getPhoneNumberTextFieldConstraints() -> [NSLayoutConstraint]{
//        let textFieldContraints = [phoneNumberTextField.topAnchor.constraint(equalTo: rootView.topAnchor , constant: 200),
//                                   phoneNumberTextField.leftAnchor.constraint(equalTo: rootView.leftAnchor ,constant: 40),
//                                   phoneNumberTextField.rightAnchor.constraint(equalTo: rootView.rightAnchor,constant: -40),
//                                   phoneNumberTextField.heightAnchor.constraint(equalToConstant : 50)]
//        return textFieldContraints
//    }
//
//    func getGenerateOTPButtonContraints() -> [NSLayoutConstraint]{
//        let buttonContraints = [ generateOTPButton.topAnchor.constraint(equalTo: phoneNumberTextField.bottomAnchor, constant: 10),
//                                 generateOTPButton.leftAnchor.constraint(equalTo: rootView.leftAnchor, constant: 40),
//                                 generateOTPButton.rightAnchor.constraint(equalTo: rootView.rightAnchor, constant: -40),
//                                 generateOTPButton.heightAnchor.constraint(equalToConstant: 50)]
//        return buttonContraints
//    }
//
//    func getOTPTextFieldContarints() -> [NSLayoutConstraint]{
//        let textFieldContraints = [OTPTextField.topAnchor.constraint(equalTo: rootView.topAnchor , constant: 200),
//                                   OTPTextField.leftAnchor.constraint(equalTo: rootView.leftAnchor ,constant: 40),
//                                   OTPTextField.rightAnchor.constraint(equalTo: rootView.rightAnchor,constant: -40),
//                                   OTPTextField.heightAnchor.constraint(equalToConstant : 50)]
//        return textFieldContraints
//    }
//
//    func createResendOTPLabel() -> UILabel{
//        resendOTPLabel = UILabel()
//        resendOTPLabel.text = "Resend OTP"
//        resendOTPLabel.textColor = .systemBlue
//        resendOTPLabel.textAlignment = .center
//        resendOTPLabel.isHidden = true
//        resendOTPLabel.isUserInteractionEnabled = true
//        resendOTPLabel.translatesAutoresizingMaskIntoConstraints = false
//        rootView.addSubview(resendOTPLabel)
//        return resendOTPLabel
//    }
//
//    func getResendOTPLabelContraints() -> [NSLayoutConstraint]{
//        let labelContraints = [resendOTPLabel.centerYAnchor.constraint(equalTo: continueForRestaurentDetailsButton.bottomAnchor ,constant: 30),
//                           resendOTPLabel.heightAnchor.constraint(equalToConstant: 30),
//                           resendOTPLabel.widthAnchor.constraint(equalToConstant: 100),
//                        resendOTPLabel.centerXAnchor.constraint(equalTo: rootView.centerXAnchor)
//        ]
//        return labelContraints
//    }
//
//    func createBackNavigationBarButton() -> UIBarButtonItem{
//        let backButton = UIBarButtonItem()
//        let backArrow = UIImage(systemName: "arrow.backward")
//        backButton.image = backArrow
//        return backButton
//    }
//
//
//    func createNextBarButton() ->  UIBarButtonItem {
//
//        let nextButton = UIBarButtonItem()
//        nextButton.title = "next"
//        nextButton.style = .done
//        return nextButton
//    }
//
//    func createKeyboardToolBar() -> UIToolbar{
//        let keyboardToolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: rootView.frame.size.width, height: 50))
//        return keyboardToolBar
//    }
//
//    func createFlexibleSpaceForToolBar() -> UIBarButtonItem{
//        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//        return flexibleSpace
//    }
//
//}
