//
//  RestaurantCreateAccountAuthenticationDetailsFormView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 08/12/21.
//

import Foundation
import UIKit
class RestaurantCreateAccountAuthenticationDetailsView : UIView,    RestaurantCreateAccountAuthenticationDetailsViewProtocol{

    weak var delegate : RestaurantCreateAccountAuthenticationDetailsViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func initialiseViewElements(){
        self.addSubview(createAccountButton)
        self.addSubview(stackViewForAuthenticationDetails)
        NSLayoutConstraint.activate(getCreateAccountAuthenticationDetailsStackViewConstraints())
    }
    
    lazy var createAccountButton : UIButton = {
        let  createAccountButton = UIButton()
        createAccountButton.translatesAutoresizingMaskIntoConstraints = false
        createAccountButton.setTitle("Create Account", for: .normal)
        createAccountButton.setTitleColor(.white, for: .normal)
        createAccountButton.backgroundColor = .systemBlue
        createAccountButton.layer.cornerRadius = 5
        createAccountButton.addTarget(self, action: #selector(didTapCreateAccountButton), for: .touchUpInside)
        return createAccountButton
    }()
    lazy var stackViewForAuthenticationDetails : UIStackView = {
        let stackViewForAuthenticationDetails = UIStackView(arrangedSubviews: [secretCodeTextField,passwordTextField,confirmPasswordTextField ,createAccountButton])
        stackViewForAuthenticationDetails.axis = .vertical
        stackViewForAuthenticationDetails.spacing = 15
        stackViewForAuthenticationDetails.distribution = .fillEqually
        stackViewForAuthenticationDetails.translatesAutoresizingMaskIntoConstraints = false
        return stackViewForAuthenticationDetails
    }()
   
    private func createTextField() -> UITextField{
        let textField = CustomTextField()
        textField .backgroundColor = .white
        textField .textColor = .black
        return textField
    }
    
    lazy var secretCodeTextField : UITextField = {
        let secretCodeTextField = createTextField()
        secretCodeTextField.placeholder = "Secret Code"
        secretCodeTextField.addLabelToTopBorder(labelText: " Secret Code ", fontSize: 15)
        secretCodeTextField.keyboardType = .numberPad
        secretCodeTextField.returnKeyType = .next
        let flexibleSpaceForToolBar = ToolBarUtils().createFlexibleSpaceForToolBar()
        let secretCodekeyboardNextBarButton = ToolBarUtils().createBarButton(title: "Next")
        let secretCodeKeyboardToolBar = ToolBarUtils().createKeyboardToolBar()
        secretCodeKeyboardToolBar.items = [flexibleSpaceForToolBar,secretCodekeyboardNextBarButton]
        secretCodeTextField.inputAccessoryView = secretCodeKeyboardToolBar
        secretCodekeyboardNextBarButton.target = self
        secretCodekeyboardNextBarButton.action = #selector(didTapSecretCodeKeyboardNextBarButton)
        return secretCodeTextField
    }()
    
    lazy var passwordTextField : UITextField = {
        let passwordTextField = createTextField()
        passwordTextField.placeholder = "Password"
        passwordTextField.addLabelToTopBorder(labelText: " Password ", fontSize: 15)
        passwordTextField.isSecureTextEntry = true
        passwordTextField.keyboardType = .default
        passwordTextField.returnKeyType = .next
        passwordTextField.autocorrectionType = .no
        passwordTextField.textContentType = .newPassword
        passwordTextField.enablePasswordToggle()
        return passwordTextField
    }()
    lazy var confirmPasswordTextField : UITextField = {
        let confirmPasswordTextField = createTextField()
        confirmPasswordTextField.placeholder = "Confirm Password"
        confirmPasswordTextField.addLabelToTopBorder(labelText: " Confirm Password ", fontSize: 15)
        confirmPasswordTextField.isSecureTextEntry = true
        confirmPasswordTextField.keyboardType = .default
        confirmPasswordTextField.returnKeyType = .done
        confirmPasswordTextField.autocorrectionType = .no
        confirmPasswordTextField.textContentType = .newPassword
        confirmPasswordTextField.enablePasswordToggle()
        return confirmPasswordTextField
    }()

    private func getCreateAccountAuthenticationDetailsStackViewConstraints() -> [NSLayoutConstraint]{
        let stackViewConstraints = [stackViewForAuthenticationDetails.topAnchor.constraint(equalTo:self.topAnchor,constant: 10),
                                    stackViewForAuthenticationDetails.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
                                    stackViewForAuthenticationDetails.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40),
                                    stackViewForAuthenticationDetails.heightAnchor.constraint(equalToConstant: 260)]
        return stackViewConstraints
    }
    
  
    @objc func didTapView(){
        delegate?.didTapView()
    }

    @objc func didTapCreateAccountButton(){
        delegate?.didTapCreateAccountButton()
    }
    
    @objc func didTapSecretCodeKeyboardNextBarButton(){
        delegate?.didTapSecretCodeKeyboardNextBarButton()
    }
    
    
}

protocol RestaurantCreateAccountAuthenticationDetailsViewDelegate : AnyObject{
    func didTapView()
    func didTapCreateAccountButton()
    func didTapSecretCodeKeyboardNextBarButton()
}
