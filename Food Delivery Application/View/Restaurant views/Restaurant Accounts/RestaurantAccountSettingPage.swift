//
//  RestaurantAccountMoreOptionPage.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 01/04/22.
//

import Foundation
import UIKit

class RestaurantAccountSettingPageView : UIView , RestaurantAccountSettingPageViewProtocol{
    
    weak var delegate :  RestaurantAccountSettingPageViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
    }
    
    private func initialiseViewElements(){
        createRestaurantDelistToggleView()
        self.addSubview(stackView)
        NSLayoutConstraint.activate(getEditAccountRestaurantDetailsStackViewConstraints())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
     let restaurantDelistLabel : UILabel = {
        let restaurantDelistLabel = UILabel()
        restaurantDelistLabel.text = "Restaurant Delist : "
        restaurantDelistLabel.backgroundColor = .white
        restaurantDelistLabel.sizeToFit()
        restaurantDelistLabel.translatesAutoresizingMaskIntoConstraints = false
        return restaurantDelistLabel
    }()
    lazy var stackView : UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [restaurantDelistToggleSwitchView])
        stackView.axis = .vertical
        stackView.spacing = 25
        stackView.distribution = .fillProportionally
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    
    lazy var restaurantDelistToggleSwitch : UISwitch = {
        let restaurantDelistToggleSwitch = UISwitch()
        restaurantDelistToggleSwitch.translatesAutoresizingMaskIntoConstraints = false
        restaurantDelistToggleSwitch.sizeToFit()
        restaurantDelistToggleSwitch.onTintColor = .systemRed
        restaurantDelistToggleSwitch.tintColor = .systemGreen
        restaurantDelistToggleSwitch.backgroundColor = .systemGreen
        restaurantDelistToggleSwitch.layer.cornerRadius = 16
        restaurantDelistToggleSwitch.addTarget(self, action:#selector(didTapRestaurantDelistSwitch(sender:)) , for: .touchUpInside)
        return restaurantDelistToggleSwitch
    }()
    
    var restaurantDelistToggleSwitchView : UIView =  {
        let restaurantDelistToggleSwitchView = UIView()
        restaurantDelistToggleSwitchView.translatesAutoresizingMaskIntoConstraints = false
        restaurantDelistToggleSwitchView.backgroundColor = .white
        restaurantDelistToggleSwitchView.layer.borderColor = UIColor.systemGray5.cgColor
        restaurantDelistToggleSwitchView.layer.borderWidth = 1
        restaurantDelistToggleSwitchView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        return restaurantDelistToggleSwitchView
    }()
    
    private func createRestaurantDelistToggleView(){
        restaurantDelistToggleSwitchView.addSubview(restaurantDelistToggleSwitch)
        restaurantDelistToggleSwitchView.addSubview(restaurantDelistLabel)
        applyRestaurantDelistToggleSwitchConstraints()
        applyRestaurantDelistLabelConstraints()
       
    }
    
    
    private func applyRestaurantDelistLabelConstraints(){
        restaurantDelistLabel .heightAnchor.constraint(equalToConstant: 40).isActive = true
        restaurantDelistLabel.centerYAnchor.constraint(equalTo: restaurantDelistToggleSwitchView.centerYAnchor).isActive = true
        
        restaurantDelistLabel.leftAnchor.constraint(equalTo: restaurantDelistToggleSwitchView.leftAnchor,constant: 10).isActive = true

        restaurantDelistLabel.rightAnchor.constraint(equalTo: restaurantDelistToggleSwitch.leftAnchor, constant: -10).isActive = true
        
    }
    
    private func applyRestaurantDelistToggleSwitchConstraints()  {
        restaurantDelistToggleSwitch.centerYAnchor.constraint(equalTo: restaurantDelistToggleSwitchView.centerYAnchor).isActive = true
        restaurantDelistToggleSwitch.rightAnchor.constraint(equalTo: restaurantDelistToggleSwitchView.rightAnchor,constant: -10).isActive = true
        restaurantDelistToggleSwitch.widthAnchor.constraint(equalToConstant: 50).isActive = true
        restaurantDelistToggleSwitch.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }

    private func getEditAccountRestaurantDetailsStackViewConstraints() -> [NSLayoutConstraint]{
        let stackViewConstraints = [stackView.topAnchor.constraint(equalTo:self.topAnchor,constant: 20),
                                    stackView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
                                    stackView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20)
        ]
        return stackViewConstraints
    }
    
    @objc func didTapView(){
        delegate?.didTapView()
    }
    @objc func didTapRestaurantDelistSwitch(sender: UISwitch){
        delegate?.didTapRestaurantDelistSwitch(sender: sender)
    }
}


protocol RestaurantAccountSettingPageViewDelegate : AnyObject{
    func didTapView()
    func didTapRestaurantDelistSwitch(sender: UISwitch)
}
