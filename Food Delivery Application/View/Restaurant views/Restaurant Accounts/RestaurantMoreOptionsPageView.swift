//
//  RestaurantMoreOptionsPageView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 01/04/22.
//

import Foundation
import UIKit

class RestaurantMoreOptionsPageView : UIView ,RestaurantMoreOptionsPageViewProtocol{
    weak var delegate : RestaurantMoreOptionsPageViewDelegate?
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        self.addSubview(stackView)
        NSLayoutConstraint.activate(getMoreOptionsStackViewConstraints())
    }
    lazy var stackView : UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [restaurantPackagingChargesTextField,updateAndCancelRestaurantPackagingChargesHorizontalStackView])
        stackView.axis = .vertical
        stackView.spacing = 25
        stackView.distribution = .fillProportionally
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    
   
    
    lazy var restaurantPackagingChargesTextField :  UITextField = {
        let restaurantPackagingChargesTextField = CustomTextField()
        restaurantPackagingChargesTextField .backgroundColor = .white
        restaurantPackagingChargesTextField .textColor = .black
        restaurantPackagingChargesTextField .translatesAutoresizingMaskIntoConstraints = false
        restaurantPackagingChargesTextField.autocorrectionType = .no
        restaurantPackagingChargesTextField.addLabelToTopBorder(labelText: "RESTAURANT PACKAGING CHARGES", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.6), fontSize: 15)
        restaurantPackagingChargesTextField.keyboardType = .decimalPad
        restaurantPackagingChargesTextField.returnKeyType = .next
        restaurantPackagingChargesTextField.addBorder(side: .bottom, color: .systemGray, width: 1)
        restaurantPackagingChargesTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        return restaurantPackagingChargesTextField
    }()
    
    lazy var cancelUpdateRestaurantPackagingChargesButton  : UIButton = {
        let cancelButton = UIButton()
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.setTitle("CANCEL", for: .normal)
        cancelButton.setTitleColor(.systemBlue, for: .normal)
        cancelButton.backgroundColor = .white
        cancelButton.layer.borderWidth = 1
        cancelButton.layer.borderColor = UIColor.systemBlue.cgColor
        cancelButton.layer.cornerRadius = 5
        cancelButton.addTarget(self, action: #selector(didTapCancelUpdateRestaurantPackaingChargesButton), for: .touchUpInside)
        return  cancelButton
    }()
    
    lazy var  updateRestaurantPackagingChargesButton : UIButton = {
        let updateButton = UIButton()
        updateButton.translatesAutoresizingMaskIntoConstraints = false
        updateButton.setTitle("UPDATE", for: .normal)
        updateButton.setTitleColor(.white, for: .normal)
        updateButton.backgroundColor = .systemBlue
        updateButton.layer.cornerRadius = 5
        updateButton.addTarget(self, action: #selector(didTapUpdateRestaurantPackaingChargesButton), for: .touchUpInside)
        return  updateButton
    }()
    
    lazy var updateAndCancelRestaurantPackagingChargesHorizontalStackView : UIStackView  = {
        let horizontalStackView = UIStackView(arrangedSubviews: [updateRestaurantPackagingChargesButton,cancelUpdateRestaurantPackagingChargesButton])
        horizontalStackView.axis = .horizontal
        horizontalStackView.distribution = .fillEqually
        horizontalStackView.spacing = 20
        horizontalStackView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return horizontalStackView
    }()
    
    
    

    private func getMoreOptionsStackViewConstraints() -> [NSLayoutConstraint]{
        let stackViewConstraints = [stackView.topAnchor.constraint(equalTo:self.topAnchor,constant: 50),
                                    stackView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
                                    stackView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20)
        ]
        return stackViewConstraints
    }
    
    @objc func didTapCancelUpdateRestaurantPackaingChargesButton(){
        delegate?.didTapCancelUpdateRestaurantPackaingChargesButton()
    }
    
    @objc func didTapUpdateRestaurantPackaingChargesButton(){
        delegate?.didTapUpdateRestaurantPackaingChargesButton()
    }
    
    @objc func didTapView(){
        delegate?.didTapView()
    }
    
    
}

protocol RestaurantMoreOptionsPageViewDelegate : AnyObject {
     func didTapCancelUpdateRestaurantPackaingChargesButton()
     func didTapUpdateRestaurantPackaingChargesButton()
     func didTapView()
    
}
