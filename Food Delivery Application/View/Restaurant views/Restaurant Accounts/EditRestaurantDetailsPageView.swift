//
//  swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 30/03/22.
//

import Foundation
import UIKit

class EditRestaurantDetailsPageView : UIView,EditRestaurantDetailsPageViewProtocol{
    
    
    weak var delegate : EditRestaurantDetailsPageViewDelegate?
   
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapView)))
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        self.addSubview(stackView)
        
        createAddRestaurantProfileImageSectionView()
        stackView.addArrangedSubview(restaurantProfileImageSectionView)
        stackView.addArrangedSubview(restaurantNameTextField)
        stackView.addArrangedSubview(updateAndCancelNameHorizontalStackView)
        stackView.addArrangedSubview(restaurantPhoneNumberTextField)
        stackView.addArrangedSubview(verifyAndCancelPhoneNumberHorizontalStackView)
        stackView.addArrangedSubview(restaurantCuisineButton)
        stackView.addArrangedSubview(updateAndCancelCuisineHorizontalStackView)
        stackView.addArrangedSubview(createTextviewHolderView(textview: restaurantDescriptionTextView, topLabelText: "RESTAURANT DESCRIPTION"))
        stackView.addArrangedSubview(updateAndCancelDescriptionHorizontalStackView)
        stackView.addArrangedSubview(restaurantAddressLabel)
        stackView.addArrangedSubview(secretCodeTextField)
        stackView.addArrangedSubview(verifyAndCancelAddressHorizontalStackView)
        updateAndCancelDescriptionHorizontalStackView.isHidden = true
        verifyAndCancelAddressHorizontalStackView.isHidden = true
        updateAndCancelCuisineHorizontalStackView.isHidden = true
        updateAndCancelNameHorizontalStackView.isHidden = true
        verifyAndCancelPhoneNumberHorizontalStackView.isHidden = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        NSLayoutConstraint.activate(getEditAccountRestaurantDetailsStackViewConstraints())
    }
   
    private func createTextField() -> UITextField{
        let textField = CustomTextField()
        textField .backgroundColor = .white
        textField .textColor = .black
        textField .translatesAutoresizingMaskIntoConstraints = false
        textField.autocorrectionType = .no
        return textField
    }
    
    var stackView : UIStackView  = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 25
        stackView.distribution = .fillProportionally
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
   
   
   
    lazy var updateNameButton : UIButton = {
        let  updateNameButton = createUpdateButton()
        updateNameButton.addTarget(self, action: #selector(didTapUpdateNameButton), for: .touchUpInside)
        return updateNameButton
    }()
    lazy var cancelUpdateNameButton : UIButton = {
        let cancelUpdateNameButton = createCancelButton()
        cancelUpdateNameButton.addTarget(self, action: #selector(didTapCancelUpdateNameButton), for: .touchUpInside)
        return cancelUpdateNameButton
    }()
    
    lazy var updateAndCancelNameHorizontalStackView : UIStackView = {
        let updateAndCancelNameHorizontalStackView =  createHorizontalStackView(updateButton: updateNameButton, cancelButton: cancelUpdateNameButton)
        
        return updateAndCancelNameHorizontalStackView
    }()
   
    lazy var verifyPhoneNumberButton : UIButton = {
        let verifyPhoneNumberButton = createUpdateButton()
        verifyPhoneNumberButton.setTitle("VERIFY", for: .normal)
        verifyPhoneNumberButton.addTarget(self, action: #selector(didTapVerifyPhoneNumberButton), for: .touchUpInside)
        return verifyPhoneNumberButton
    }()
    
    lazy var cancelUpdatePhoneNumberButton : UIButton = {
        let cancelUpdatePhoneNumberButton = createCancelButton()
        cancelUpdatePhoneNumberButton.addTarget(self, action: #selector(didTapCancelUpdatePhoneNumberButton), for: .touchUpInside)
        return cancelUpdatePhoneNumberButton
    }()
    lazy var verifyAndCancelPhoneNumberHorizontalStackView : UIStackView = {
        let verifyAndCancelPhoneNumberHorizontalStackView = createHorizontalStackView(updateButton: verifyPhoneNumberButton, cancelButton: cancelUpdatePhoneNumberButton)
        return verifyAndCancelPhoneNumberHorizontalStackView
    }()
   
    lazy var updateCuisineButton : UIButton = {
        let updateCuisineButton = createUpdateButton()
        updateCuisineButton.addTarget(self, action: #selector(didTapUpdateCuisineButton), for: .touchUpInside)
        return updateCuisineButton
        
    }()
    lazy var cancelUpdateCuisineButton : UIButton = {
        let cancelUpdateCuisineButton = createCancelButton()
        cancelUpdateCuisineButton.addTarget(self, action: #selector(didTapCancelUpdateCuisineButton), for: .touchUpInside)
        return cancelUpdateCuisineButton
    }()
    lazy var updateAndCancelCuisineHorizontalStackView : UIStackView = {
        let updateAndCancelCuisineHorizontalStackView = createHorizontalStackView(updateButton: updateCuisineButton, cancelButton: cancelUpdateCuisineButton)
        return updateAndCancelCuisineHorizontalStackView
    }()
  
    lazy var updateDescriptionButton : UIButton = {
        let  updateDescriptionButton = createUpdateButton()
        updateDescriptionButton.addTarget(self, action: #selector(didTapUpdateDescriptionButton), for: .touchUpInside)
        return updateDescriptionButton
    }()
    
    lazy var cancelUpdateDescriptionButton : UIButton = {
        let cancelUpdateDescriptionButton = createCancelButton()
        cancelUpdateDescriptionButton.addTarget(self, action: #selector(didTapCancelUpdateDescriptionButton), for: .touchUpInside)
        return cancelUpdateDescriptionButton
    }()
    
    lazy var updateAndCancelDescriptionHorizontalStackView : UIStackView = {
        let updateAndCancelDescriptionHorizontalStackView = createHorizontalStackView(updateButton: updateDescriptionButton, cancelButton: cancelUpdateDescriptionButton)
        return updateAndCancelDescriptionHorizontalStackView
    }()
    
   
    lazy var verifysecretCodeToUpdateAddressButton : UIButton = {
        let verifysecretCodeToUpdateAddressButton = createUpdateButton()
        verifysecretCodeToUpdateAddressButton.setTitle("VERIFY", for: .normal)
        verifysecretCodeToUpdateAddressButton.addTarget(self, action: #selector(didTapVerifySecretCodeButton), for: .touchUpInside)
        return verifysecretCodeToUpdateAddressButton
    }()
    
    lazy var cancelUpdateAddressButton : UIButton = {
        let  cancelUpdateAddressButton = createCancelButton()
        cancelUpdateAddressButton.addTarget(self, action: #selector(didTapCancelUpdateAddressButton), for: .touchUpInside)
        return cancelUpdateAddressButton
    }()
    
    lazy var verifyAndCancelAddressHorizontalStackView : UIStackView = {
        let verifyAndCancelAddressHorizontalStackView = createHorizontalStackView(updateButton: verifysecretCodeToUpdateAddressButton, cancelButton: cancelUpdateAddressButton)
        return verifyAndCancelAddressHorizontalStackView
    }()
  
    
    
    lazy var restaurantNameTextField :  UITextField = {
        let restaurantNameTextField = createTextField()
        restaurantNameTextField.addLabelToTopBorder(labelText: "RESTAURANT NAME", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.6), fontSize: 15)
        restaurantNameTextField.keyboardType = .default
        restaurantNameTextField.returnKeyType = .done
        restaurantNameTextField.addBorder(side: .bottom, color: .systemGray, width: 1)
        restaurantNameTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        return restaurantNameTextField
    }()
    
    lazy var restaurantCuisineButton : UIButton = {
        let restaurantCuisineButton = UIButton()
        restaurantCuisineButton.setTitle("Restaurant Cuisine", for: .normal)
        restaurantCuisineButton.addLabelToTopBorder(labelText: " Restaurant Cuisine ", borderColor: .clear, borderWidth: 1,leftPadding: 0, topPadding: 0,textColor: .black.withAlphaComponent(0.6), fontSize: 15)
        restaurantCuisineButton.contentHorizontalAlignment = .left
        restaurantCuisineButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        restaurantCuisineButton.setTitleColor(.black, for: .normal)
        restaurantCuisineButton.titleLabel?.textAlignment = .left
        restaurantCuisineButton.backgroundColor = .white
        restaurantCuisineButton.translatesAutoresizingMaskIntoConstraints = false
        restaurantCuisineButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        restaurantCuisineButton.addRightImageToButton(systemName: "chevron.down", imageColor: .lightGray, padding: 20)
        restaurantCuisineButton.addBorder(side: .bottom, color: .systemGray, width: 1)
        restaurantCuisineButton.addTarget(self, action: #selector(didTapRestaurantCuisine), for: .touchUpInside)
        return restaurantCuisineButton
    }()
    
    lazy var restaurantPhoneNumberTextField : UITextField  = {
        let restaurantPhoneNumberTextField = createTextField()
        restaurantPhoneNumberTextField.addLabelToTopBorder(labelText: "PHONE NUMBER", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.6), fontSize: 15)
        restaurantPhoneNumberTextField.keyboardType = .default
        restaurantPhoneNumberTextField.addBorder(side: .bottom, color: .systemGray, width: 1)
        restaurantPhoneNumberTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        return restaurantPhoneNumberTextField
    }()
    
   
    
    
    
    var restaurantDescriptionTextView :  UITextView =  {
        let restaurantDescriptionTextView = UITextView()
        restaurantDescriptionTextView.returnKeyType = .default
        restaurantDescriptionTextView.backgroundColor = .white
        restaurantDescriptionTextView.keyboardType = .default
        restaurantDescriptionTextView.autocorrectionType = .no
        restaurantDescriptionTextView.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        restaurantDescriptionTextView.translatesAutoresizingMaskIntoConstraints  = false
        return restaurantDescriptionTextView
    }()
    
    func createTextviewHolderView(textview : UITextView,topLabelText : String) -> UIView{
        let holderView = UIView()
        holderView.addSubview(textview)
        holderView.translatesAutoresizingMaskIntoConstraints = false
        holderView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        textview.heightAnchor.constraint(equalTo : holderView.heightAnchor).isActive = true
        textview.widthAnchor.constraint(equalTo : holderView.widthAnchor).isActive = true
        holderView.addLabelToTopBorder(labelText: topLabelText, borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.6), fontSize: 15)
        holderView.addBorder(side: .bottom, color: .systemGray, width: 1)
        return holderView
    }
    
    
    lazy var changeRestaurantProfileImageButton : UIButton = {
        let changeRestaurantProfileImageButton = UIButton()
        changeRestaurantProfileImageButton.setTitle("Change Profile Image", for: .normal)
        changeRestaurantProfileImageButton.setTitleColor(.systemBlue, for: .normal)
        changeRestaurantProfileImageButton.backgroundColor = .white
        changeRestaurantProfileImageButton.translatesAutoresizingMaskIntoConstraints = false
        changeRestaurantProfileImageButton.titleLabel?.adjustsFontSizeToFitWidth = true
        changeRestaurantProfileImageButton.addTarget(self, action: #selector(didTapChangeProfileImage), for: .touchUpInside)
        return changeRestaurantProfileImageButton
    }()
    
    var restaurantProfileImageView : UIImageView = {
        let restaurantProfileImageView = UIImageView()
        restaurantProfileImageView.translatesAutoresizingMaskIntoConstraints = false
        restaurantProfileImageView.layer.cornerRadius = 20
        restaurantProfileImageView.layer.masksToBounds = true
        return restaurantProfileImageView
    }()
    var restaurantProfileImageSectionView : UIView = {
        let  restaurantProfileImageSectionView = UIView()
        restaurantProfileImageSectionView.translatesAutoresizingMaskIntoConstraints =  false
        restaurantProfileImageSectionView.heightAnchor.constraint(equalToConstant: 180).isActive = true
        restaurantProfileImageSectionView.addBorder(side: .bottom, color: .systemGray, width: 1)
        return restaurantProfileImageSectionView
    }()
    
    
    private func createAddRestaurantProfileImageSectionView(){
        restaurantProfileImageSectionView.addSubview(changeRestaurantProfileImageButton)
        restaurantProfileImageSectionView.addSubview(restaurantProfileImageView)
        applyRestaurantProfileImageViewConstraints()
        applyChangeRestaurantProfileImageButtonConstraints()
    }
    
    private func applyChangeRestaurantProfileImageButtonConstraints(){
        changeRestaurantProfileImageButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        changeRestaurantProfileImageButton.centerXAnchor.constraint(equalTo: restaurantProfileImageView.centerXAnchor).isActive = true
        changeRestaurantProfileImageButton.topAnchor.constraint(equalTo: restaurantProfileImageView.bottomAnchor , constant: 10).isActive = true
        
        
    }
    
    private func applyRestaurantProfileImageViewConstraints(){
        restaurantProfileImageView.topAnchor.constraint(equalTo: restaurantProfileImageSectionView.topAnchor ,constant: 10).isActive = true
        
        restaurantProfileImageView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        
        restaurantProfileImageView.widthAnchor.constraint(equalToConstant: 120).isActive = true
        
        restaurantProfileImageView.centerXAnchor.constraint(equalTo: restaurantProfileImageSectionView.centerXAnchor).isActive = true
    }
    
    var restaurantAddressLabel : UILabel = {
        let restaurantAddressLabel = UILabel()
        restaurantAddressLabel.addLabelToTopBorder(labelText: "ADDRESS", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        restaurantAddressLabel.textColor = .black
        restaurantAddressLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantAddressLabel.backgroundColor = .white
        restaurantAddressLabel.textAlignment = .left
        restaurantAddressLabel.sizeToFit()
        restaurantAddressLabel.lineBreakMode = .byWordWrapping
        restaurantAddressLabel.numberOfLines = 5
        restaurantAddressLabel.heightAnchor.constraint(equalToConstant: 60).isActive = true
        return restaurantAddressLabel
    }()
    
    lazy var secretCodeTextField :  UITextField = {
        let secretCodeTextField = createTextField()
        secretCodeTextField.placeholder = "Enter Secrect Code To Update Address"
        secretCodeTextField.addLabelToTopBorder(labelText: "SECRET CODE", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.6), fontSize: 15)
        secretCodeTextField.keyboardType = .decimalPad
        secretCodeTextField.returnKeyType = .done
        secretCodeTextField.addBorder(side: .bottom, color: .systemGray, width: 1)
        secretCodeTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        return secretCodeTextField
    }()
    
    private func createCancelButton() -> UIButton {
        let cancelButton = UIButton()
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.setTitle("CANCEL", for: .normal)
        cancelButton.setTitleColor(.systemBlue, for: .normal)
        cancelButton.backgroundColor = .white
        cancelButton.layer.borderWidth = 1
        cancelButton.layer.borderColor = UIColor.systemBlue.cgColor
        cancelButton.layer.cornerRadius = 5
        return  cancelButton
    }
    
    private func createUpdateButton() -> UIButton {
        let updateButton = UIButton()
        updateButton.translatesAutoresizingMaskIntoConstraints = false
        updateButton.setTitle("UPDATE", for: .normal)
        updateButton.setTitleColor(.white, for: .normal)
        updateButton.backgroundColor = .systemBlue
        updateButton.layer.cornerRadius = 5
        return  updateButton
    }
    
    private func createHorizontalStackView(updateButton  : UIButton , cancelButton : UIButton) -> UIStackView{
        let horizontalStackView = UIStackView(arrangedSubviews: [updateButton,cancelButton])
        horizontalStackView.axis = .horizontal
        horizontalStackView.distribution = .fillEqually
        horizontalStackView.spacing = 20
        horizontalStackView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return horizontalStackView
    }
    
    
    
    
   
    
    
 
    private func getEditAccountRestaurantDetailsStackViewConstraints() -> [NSLayoutConstraint]{
        let stackViewConstraints = [stackView.topAnchor.constraint(equalTo:self.topAnchor),
                                    stackView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
                                    stackView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20)
        ]
        return stackViewConstraints
    }
    
    @objc func didTapView(){
        
    }
    
    @objc func didTapChangeProfileImage(){
        delegate?.didTapChangeProfileImage()
    }
    
    @objc func didTapCancelUpdateNameButton(){
        delegate?.didTapCancelUpdateNameButton()
    }
   
    @objc func didTapUpdateNameButton(){
        delegate?.didTapUpdateNameButton()
    }
    
    @objc func didTapVerifyPhoneNumberButton(){
        delegate?.didTapVerifyPhoneNumberButton()
    }
    @objc func didTapCancelUpdatePhoneNumberButton(){
        delegate?.didTapCancelUpdatePhoneNumberButton()
    }
    
    @objc func didTapUpdateCuisineButton(){
        delegate?.didTapUpdateCuisineButton()
    }
    
    @objc func didTapCancelUpdateCuisineButton(){
        delegate?.didTapCancelUpdateCuisineButton()
    }
    
    @objc func didTapUpdateDescriptionButton(){
        delegate?.didTapUpdateDescriptionButton()
    }
    
    @objc func didTapCancelUpdateDescriptionButton(){
        delegate?.didTapCancelUpdateDescriptionButton()
    }
    
    @objc func didTapVerifySecretCodeButton(){
        delegate?.didTapVerifySecretCodeButton()
    }
    
    @objc func didTapCancelUpdateAddressButton(){
        delegate?.didTapCancelUpdateAddressButton()
    }
    
    @objc  func didTapRestaurantCuisine(){
        delegate?.didTapRestaurantCuisine()
    }
}

protocol EditRestaurantDetailsPageViewDelegate : AnyObject{
    func didTapChangeProfileImage()
    func didTapCancelUpdateNameButton()
    func didTapUpdateNameButton()
    func didTapVerifyPhoneNumberButton()
    func didTapCancelUpdatePhoneNumberButton()
    func didTapUpdateCuisineButton()
    func didTapCancelUpdateCuisineButton()
    func didTapUpdateDescriptionButton()
    func didTapCancelUpdateDescriptionButton()
    func didTapVerifySecretCodeButton()
    func didTapCancelUpdateAddressButton()
    func didTapRestaurantCuisine()
    func didTapView()
}
