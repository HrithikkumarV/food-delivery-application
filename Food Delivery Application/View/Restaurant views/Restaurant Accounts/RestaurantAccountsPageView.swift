//
//  RestaurantAccountsPageView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 18/03/22.
//

import Foundation
import UIKit

class RestaurantAccountsPageView : UIView, RestaurantAccountsPageViewProtocol{
    
    weak var delegate : RestaurantAccountsPageViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        activateRestaurantContentConstraints()
    }
    
    private func initialiseViewElements(){
        
        self.addSubview(editButton)
        self.addSubview(logOutButton)
        self.addSubview(changePasswordButton)
        self.addSubview(moreButton)
        self.addSubview(settingButton)
        self.addSubview(contactUSLabel)
        self.addSubview(orderDetailsTableView)
        self.addSubview(restaurantIsAvailableView)
        self.addSubview(viewMoreOrdersButton)
        createRestaurantIsAvailableView()
        createRestaurantDetailsContentsInView()
        
       
        NSLayoutConstraint.activate(getOrderDetailsTableViewLayoutConstraints())
        NSLayoutConstraint.activate(getContactUsLabelContraints())
        NSLayoutConstraint.activate(getEditButtonContraints())
       
        NSLayoutConstraint.activate(getLogoutButtonContraints())
        
        NSLayoutConstraint.activate(getChangePasswordButtonContraints())
       
        NSLayoutConstraint.activate(getMoreButtonContraints())
        
        NSLayoutConstraint.activate(getSettingsButtonContraints())
        
        NSLayoutConstraint.activate(getviewMoreOrdersButtonContraints())
       
    }
    
     
    var restaurantIsAvailableView : UIView = {
        let restaurantIsAvailableView = UIView()
        restaurantIsAvailableView.translatesAutoresizingMaskIntoConstraints = false
        restaurantIsAvailableView.backgroundColor = .white
        return restaurantIsAvailableView
    }()
    var restaurantIsAvailableLabel : UILabel = {
        let restaurantIsAvailableLabel = UILabel()
        restaurantIsAvailableLabel.text = "RECEIVE ORDERS : "
        restaurantIsAvailableLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        restaurantIsAvailableLabel.backgroundColor = .white
        restaurantIsAvailableLabel.sizeToFit()
        restaurantIsAvailableLabel.translatesAutoresizingMaskIntoConstraints = false
        return restaurantIsAvailableLabel
    }()
    lazy var restaurantIsAvailableToggleSwitch : UISwitch = {
        let restaurantIsAvailableToggleSwitch = UISwitch()
        restaurantIsAvailableToggleSwitch.translatesAutoresizingMaskIntoConstraints = false
        restaurantIsAvailableToggleSwitch.sizeToFit()
        restaurantIsAvailableToggleSwitch.onTintColor = .systemRed
        restaurantIsAvailableToggleSwitch.tintColor = .systemGreen
        restaurantIsAvailableToggleSwitch.backgroundColor = .systemGreen
        restaurantIsAvailableToggleSwitch.layer.cornerRadius = 16
        restaurantIsAvailableToggleSwitch.addTarget(self, action: #selector(didTapRestaurantIsAvailableSwitch(sender:)), for: .touchUpInside)
        return restaurantIsAvailableToggleSwitch
    }()
     lazy var datePicker : UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.timeZone =  NSTimeZone.local
        datePicker.backgroundColor = .white
        datePicker.minimumDate = Date()
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        datePicker.datePickerMode = .dateAndTime
         datePicker.addTarget(self, action: #selector(didChangeDateAndtime(sender:)), for: .valueChanged)
        return datePicker
    }()
    var restaurantNextAvailbaleDatePickerLabel : UILabel = {
        let  restaurantNextAvailbaleDatePickerLabel = UILabel()
        restaurantNextAvailbaleDatePickerLabel.text = "Set Next Available :"
        restaurantNextAvailbaleDatePickerLabel.adjustsFontSizeToFitWidth = true
        restaurantNextAvailbaleDatePickerLabel.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        restaurantNextAvailbaleDatePickerLabel.backgroundColor = .white
        restaurantNextAvailbaleDatePickerLabel.translatesAutoresizingMaskIntoConstraints = false
       
        return restaurantNextAvailbaleDatePickerLabel
    }()
    
    
    
    lazy var cancelRestaurantAvailablityButton : UIButton = {
        let cancelButton = UIButton()
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.setTitle("CANCEL", for: .normal)
        cancelButton.setTitleColor(.systemBlue, for: .normal)
        cancelButton.backgroundColor = .white
        cancelButton.layer.borderWidth = 1
        cancelButton.layer.borderColor = UIColor.systemBlue.cgColor
        cancelButton.layer.cornerRadius = 5
        cancelButton.addTarget(self, action: #selector(didTapCancelUpdateRestaurantAvailablity), for: .touchUpInside)
        return  cancelButton
    }()
    
    lazy var updateRestaurantAvailablityButton : UIButton = {
        let updateButton = UIButton()
        updateButton.translatesAutoresizingMaskIntoConstraints = false
        updateButton.setTitle("UPDATE", for: .normal)
        updateButton.setTitleColor(.white, for: .normal)
        updateButton.backgroundColor = .systemBlue
        updateButton.layer.cornerRadius = 5
        updateButton.addTarget(self, action: #selector(didTapUpdateRestaurantAvailablity), for: .touchUpInside)
        return  updateButton
    }()
    
    lazy var restaurantNextAvailablityUpdateAndCancelButtonHorizontalStackView : UIStackView = {
        let restaurantNextAvailablityUpdateAndCancelButtonHorizontalStackView = UIStackView(arrangedSubviews: [updateRestaurantAvailablityButton,cancelRestaurantAvailablityButton])
        restaurantNextAvailablityUpdateAndCancelButtonHorizontalStackView.axis = .horizontal
        restaurantNextAvailablityUpdateAndCancelButtonHorizontalStackView.distribution = .fillEqually
        restaurantNextAvailablityUpdateAndCancelButtonHorizontalStackView.spacing = 20
        restaurantNextAvailablityUpdateAndCancelButtonHorizontalStackView.translatesAutoresizingMaskIntoConstraints =  false
        return restaurantNextAvailablityUpdateAndCancelButtonHorizontalStackView
    }()
    var restaurantProfileImageView : UIImageView = {
       let restaurantProfileImageView = UIImageView()
       restaurantProfileImageView.translatesAutoresizingMaskIntoConstraints = false
       restaurantProfileImageView.backgroundColor = .white
       restaurantProfileImageView.layer.cornerRadius = 20
       restaurantProfileImageView.contentMode = .scaleAspectFill
       restaurantProfileImageView.clipsToBounds = true
       return restaurantProfileImageView
   }()
    var restaurantNameLabel : UILabel = {
        let restaurantNameLabel = UILabel()
        restaurantNameLabel.textColor = .black
        restaurantNameLabel.addLabelToTopBorder(labelText: "🏨 RESTAURANT NAME", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -10, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        restaurantNameLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantNameLabel.font = UIFont.systemFont(ofSize: 25, weight: .medium)
        restaurantNameLabel.textAlignment = .left
        restaurantNameLabel.sizeToFit()
        return restaurantNameLabel
    }()
    var restaurantPhoneNumberLabel : UILabel = {
        let restaurantPhoneNumberLabel = UILabel()
        restaurantPhoneNumberLabel.textColor = .black.withAlphaComponent(0.9)
        restaurantPhoneNumberLabel.addLabelToTopBorder(labelText: "📞 PHONE NUMBER", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -10,textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        restaurantPhoneNumberLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantPhoneNumberLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        restaurantPhoneNumberLabel.textAlignment = .left
        restaurantPhoneNumberLabel.adjustsFontSizeToFitWidth = true
        return restaurantPhoneNumberLabel
    }()
    var restaurantCuisineLabel : UILabel = {
        let restaurantCuisineLabel = UILabel()
        restaurantCuisineLabel.sizeToFit()
        restaurantCuisineLabel.textColor = .black.withAlphaComponent(0.9)
        restaurantCuisineLabel.addLabelToTopBorder(labelText: "🍱 CUISINE", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -10, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        restaurantCuisineLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantCuisineLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        restaurantCuisineLabel.textAlignment = .left
        return restaurantCuisineLabel
    }()
    var restaurantDescriptionLabel : UILabel = {
        let restaurantDescriptionLabel = UILabel()
        restaurantDescriptionLabel.textColor = .black.withAlphaComponent(0.9)
        restaurantDescriptionLabel.addLabelToTopBorder(labelText: "✍🏽 DESCRIPTION", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -10, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        restaurantDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantDescriptionLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        restaurantDescriptionLabel.textAlignment = .left
        restaurantDescriptionLabel.lineBreakMode = .byWordWrapping
        restaurantDescriptionLabel.numberOfLines = 5
        restaurantDescriptionLabel.sizeToFit()
        return restaurantDescriptionLabel
    }()
    var restaurantAddressLabel : UILabel = {
        let restaurantAddressLabel = UILabel()
        restaurantAddressLabel.addLabelToTopBorder(labelText: "🏠 ADDRESS", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -10, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        restaurantAddressLabel.textColor = .black.withAlphaComponent(0.9)
        restaurantAddressLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantAddressLabel.backgroundColor = .white
        restaurantAddressLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        restaurantAddressLabel.textAlignment = .left
        restaurantAddressLabel.sizeToFit()
        restaurantAddressLabel.lineBreakMode = .byWordWrapping
        restaurantAddressLabel.numberOfLines = 5
        return restaurantAddressLabel
    }()
    
    var restaurantStarRatingLabel : UILabel = {
        let restaurantStarRatingLabel = UILabel()
        restaurantStarRatingLabel.textColor = .black
        restaurantStarRatingLabel.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        restaurantStarRatingLabel.translatesAutoresizingMaskIntoConstraints = false
        return restaurantStarRatingLabel
    }()
    
    var totalNumberOfrestaurantStarRatingLabel : UILabel = {
        let totalNumberOfrestaurantStarRatingLabel = UILabel()
        totalNumberOfrestaurantStarRatingLabel.textColor = .black
        totalNumberOfrestaurantStarRatingLabel.translatesAutoresizingMaskIntoConstraints = false
        totalNumberOfrestaurantStarRatingLabel.adjustsFontSizeToFitWidth = true
        totalNumberOfrestaurantStarRatingLabel.font = UIFont.systemFont(ofSize: 20,weight: .regular)
        totalNumberOfrestaurantStarRatingLabel.textAlignment = .center
        return totalNumberOfrestaurantStarRatingLabel
    }()
    
    var starIconImageView : UIImageView = {
        let starIconImageView = UIImageView()
        starIconImageView.image = UIImage(systemName: "star.fill")?.withTintColor(.black , renderingMode: .alwaysOriginal)
        starIconImageView.translatesAutoresizingMaskIntoConstraints = false
        return starIconImageView
    }()
    var availableLabel : UILabel = {
        let availableLabel = UILabel()
        availableLabel.translatesAutoresizingMaskIntoConstraints = false
        availableLabel.textAlignment = .center
        availableLabel.adjustsFontSizeToFitWidth = true
        availableLabel.numberOfLines = 4
        availableLabel.lineBreakMode = .byWordWrapping
        return availableLabel
    }()
    
    lazy var editButton : UIButton = {
        let editButton = UIButton()
        editButton.setTitle("EDIT", for: .normal)
        editButton.setTitleColor(.systemBlue, for: .normal)
        editButton.translatesAutoresizingMaskIntoConstraints = false
        editButton.addTarget(self, action: #selector(didTapEditButton), for: .touchUpInside)
        return editButton
    }()
    lazy var settingButton : UIButton = {
        let settingButton = UIButton()
        settingButton.setTitle(" Settings ", for: .normal)
        settingButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        settingButton.setTitleColor(.black, for: .normal)
        settingButton.setImage(UIImage(systemName: "gearshape")?.withTintColor(.black, renderingMode: .alwaysOriginal), for: .normal)
        settingButton.contentMode = .left
        settingButton.contentHorizontalAlignment = .left
        settingButton.translatesAutoresizingMaskIntoConstraints = false
        settingButton.addTarget(self, action: #selector(didTapSettingsButton), for: .touchUpInside)
        return settingButton
    }()
    lazy var moreButton : UIButton = {
        let moreButton = UIButton()
        moreButton.setTitle(" More", for: .normal)
        moreButton.setTitleColor(.black, for: .normal)
        moreButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        moreButton.contentMode = .left
        moreButton.setImage(UIImage(systemName: "list.dash")?.withTintColor(.black, renderingMode: .alwaysOriginal), for: .normal)
        moreButton.contentHorizontalAlignment = .left
        moreButton.translatesAutoresizingMaskIntoConstraints = false
        moreButton.addTarget(self, action: #selector(didTapMoreButton), for: .touchUpInside)
        return moreButton
    }()
    lazy var changePasswordButton : UIButton = {
        let changePasswordButton = UIButton()
        changePasswordButton.setTitle("Change Password", for: .normal)
        changePasswordButton.setTitleColor(.systemBlue, for: .normal)
        changePasswordButton.contentMode = .left
        changePasswordButton.contentHorizontalAlignment = .left
        changePasswordButton.translatesAutoresizingMaskIntoConstraints = false
        changePasswordButton.addTarget(self, action: #selector(didTapChangePassword), for: .touchUpInside)
        return changePasswordButton
        
    }()
    lazy var logOutButton : UIButton = {
        let logOutButton = UIButton()
        logOutButton.setTitle("LOGOUT", for: .normal)
        logOutButton.setTitleColor(.systemBlue, for: .normal)
        logOutButton.contentMode = .left
        logOutButton.contentHorizontalAlignment = .left
        logOutButton.translatesAutoresizingMaskIntoConstraints = false
        logOutButton.addTarget(self, action: #selector(didTapLogout), for: .touchUpInside)
        return logOutButton
    }()
    var orderDetailsTableView : UITableView = {
        let orderDetailsTableView = UITableView()
        orderDetailsTableView.backgroundColor = .white
        orderDetailsTableView.bounces = false
        orderDetailsTableView.register(RestaurantOrderDetailsTableViewCell.self, forCellReuseIdentifier: RestaurantOrderDetailsTableViewCell.cellIdentifier)
        orderDetailsTableView.translatesAutoresizingMaskIntoConstraints = false
        orderDetailsTableView.separatorStyle = .none
        orderDetailsTableView.isScrollEnabled = false
        return orderDetailsTableView
    }()
    
    func getOrderTabelViewCellHeight() -> CGFloat{
       
        return 150
    }
    
    func createOrdersTableViewCell(indexPath : IndexPath ,tableView : UITableView, orderCellContents : (orderId : String , orderStatus : OrderStatus,orderTotal : Int , menuNameAndQuantities : [(menuName : String, quantity : Int)],dateAndTime : String)) -> UITableViewCell{
        let orderDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: RestaurantOrderDetailsTableViewCell.cellIdentifier, for: indexPath) as? RestaurantOrderDetailsTableViewCell
        orderDetailsTableViewCell?.cellHeight = 150
        orderDetailsTableViewCell?.configureContents(orderCellContents: orderCellContents)
        return orderDetailsTableViewCell ?? UITableViewCell()
    }
    lazy var viewMoreOrdersButton : UIButton = {
        let viewMoreOrdersButton = UIButton()
        viewMoreOrdersButton.setTitle("View More Orders", for: .normal)
        viewMoreOrdersButton.setTitleColor(.systemGreen, for: .normal)
        viewMoreOrdersButton.backgroundColor = .white
        viewMoreOrdersButton.translatesAutoresizingMaskIntoConstraints = false
        viewMoreOrdersButton.contentMode = .left
        viewMoreOrdersButton.contentHorizontalAlignment = .left
        viewMoreOrdersButton.addTarget(self, action: #selector(didTapViewMorePastOrders), for: .touchUpInside)
        return viewMoreOrdersButton
    }()
    
    var contactUSLabel : UILabel = {
        let contactUSLabel = UILabel()
        
        contactUSLabel.adjustsFontSizeToFitWidth = true
        contactUSLabel.textAlignment = .left
        contactUSLabel.translatesAutoresizingMaskIntoConstraints = false
        contactUSLabel.addBorder(side: .bottom, color: .systemGray4, width: 1)
        return contactUSLabel
    }()

    
    func updateRestaurantDetails(restaurantContentDetails :   RestaurantContentDetails){
        restaurantProfileImageView.image = UIImage(data: restaurantContentDetails.restaurantDetails.restaurantProfileImage)
        restaurantNameLabel.text = restaurantContentDetails.restaurantDetails.restaurantName
        restaurantPhoneNumberLabel.text = restaurantContentDetails.restaurantDetails.restaurantContactNumber
        restaurantCuisineLabel.text = restaurantContentDetails.restaurantDetails.restaurantCuisine
        restaurantDescriptionLabel.text = restaurantContentDetails.restaurantDetails.restaurantDescription
        let address = restaurantContentDetails.restaurantAddress
        restaurantAddressLabel.text = "\(address.doorNoAndBuildingNameAndBuildingNo), \(address.streetName), \(address.landmark), \(address.localityName), \(address.city), \(address.state), \(address.country), \(address.pincode)"
        restaurantStarRatingLabel.text = String(restaurantContentDetails.restaurantStarRating.prefix(upTo: restaurantContentDetails.restaurantStarRating.index(restaurantContentDetails.restaurantStarRating.startIndex, offsetBy: 3)))
        totalNumberOfrestaurantStarRatingLabel.text = "\(restaurantContentDetails.totalNumberOfRating)+ ratings"
        updateRestaurantStatus(restaurantContentDetails: restaurantContentDetails)
        contactUSLabel.text = "Contact us : " + ContactTeam.getContactTeamEmailId()
    }
    
    func updateRestaurantStatus(restaurantContentDetails : RestaurantContentDetails){
        if(restaurantContentDetails.restaurantAccountStatus == 0){
            availableLabel.text = "DELISTED"
            availableLabel.textColor = .systemRed
            availableLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        }
        else if(restaurantContentDetails.restaurantIsAvailable == 1){
            availableLabel.text = "Available For Orders"
            availableLabel.textColor = .systemGreen
            availableLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        }
        else{
            availableLabel.text =
            """
            CLOSED
            Opens Next At
            \(restaurantContentDetails.restaurantOpensNextAt)
            """
            availableLabel.textColor = .black.withAlphaComponent(0.9)
            availableLabel.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        }
    }
    
   
    private func createRestaurantIsAvailableView(){
        restaurantIsAvailableView.addSubview(restaurantIsAvailableToggleSwitch)
        restaurantIsAvailableView.addSubview(restaurantIsAvailableLabel)
        restaurantIsAvailableView.addSubview(restaurantNextAvailbaleDatePickerLabel)
        restaurantIsAvailableView.addSubview(datePicker)
        restaurantIsAvailableView.addSubview(restaurantNextAvailablityUpdateAndCancelButtonHorizontalStackView)
        applyrestaurantIsAvailableToggleSwitchConstraints()
        applyRestaurantIsAvailableLabelConstraints()
        applyRestaurantNextAvailbaleDatePickerConstraints()
        applyRestaurantNextAvailableLabelsConstraints()
        applyRestaurantNextAvailbaleUpdateAndCancelHorizontalStackViewConstraints()
    }
    
    private func applyRestaurantIsAvailableLabelConstraints(){
        restaurantIsAvailableLabel .heightAnchor.constraint(equalToConstant: 30).isActive = true
        restaurantIsAvailableLabel.topAnchor.constraint(equalTo: restaurantIsAvailableView.topAnchor,constant: 10).isActive = true
        
        restaurantIsAvailableLabel.leftAnchor.constraint(equalTo: restaurantIsAvailableView.leftAnchor,constant: 10).isActive = true

        restaurantIsAvailableLabel.rightAnchor.constraint(equalTo: restaurantIsAvailableToggleSwitch.leftAnchor, constant: -10).isActive = true
        
    }
    
    private func applyrestaurantIsAvailableToggleSwitchConstraints()  {
        restaurantIsAvailableToggleSwitch.topAnchor.constraint(equalTo: restaurantIsAvailableView.topAnchor,constant: 10).isActive = true
        restaurantIsAvailableToggleSwitch.rightAnchor.constraint(equalTo: restaurantIsAvailableView.rightAnchor,constant: -10).isActive = true
        restaurantIsAvailableToggleSwitch.widthAnchor.constraint(equalToConstant: 50).isActive = true
        restaurantIsAvailableToggleSwitch.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
   
    private func applyRestaurantNextAvailableLabelsConstraints(){
        restaurantNextAvailbaleDatePickerLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        restaurantNextAvailbaleDatePickerLabel.leftAnchor.constraint(equalTo: restaurantIsAvailableView.leftAnchor,constant: 10).isActive = true
        restaurantNextAvailbaleDatePickerLabel.rightAnchor.constraint(equalTo: datePicker.leftAnchor,constant: -10).isActive = true
        restaurantNextAvailbaleDatePickerLabel.topAnchor.constraint(equalTo: restaurantIsAvailableView.topAnchor,constant: 60).isActive = true
        
        
    }
    
    private func applyRestaurantNextAvailbaleDatePickerConstraints(){
        datePicker.widthAnchor.constraint(equalToConstant: 200).isActive = true
        datePicker.rightAnchor.constraint(equalTo: restaurantIsAvailableView.rightAnchor,constant: -10).isActive = true
        datePicker.topAnchor.constraint(equalTo: restaurantIsAvailableView.topAnchor,constant: 55).isActive = true
        datePicker.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
    }
    
    private func applyRestaurantNextAvailbaleUpdateAndCancelHorizontalStackViewConstraints(){
        restaurantNextAvailablityUpdateAndCancelButtonHorizontalStackView.leftAnchor.constraint(equalTo : restaurantIsAvailableView.leftAnchor,constant: 10).isActive = true
        restaurantNextAvailablityUpdateAndCancelButtonHorizontalStackView.rightAnchor.constraint(equalTo: restaurantIsAvailableView.rightAnchor,constant: -10).isActive = true
        restaurantNextAvailablityUpdateAndCancelButtonHorizontalStackView.topAnchor.constraint(equalTo: restaurantIsAvailableView.topAnchor,constant: 110).isActive = true
        restaurantNextAvailablityUpdateAndCancelButtonHorizontalStackView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
    }
    
    private var restaurantIsAvailableViewConstraint : NSLayoutConstraint!
    
    func hideSetNextAvailablityItems(){
        restaurantNextAvailbaleDatePickerLabel.isHidden = true
        datePicker.isHidden = true
        restaurantNextAvailablityUpdateAndCancelButtonHorizontalStackView.isHidden = true
        restaurantIsAvailableViewConstraint?.isActive = false
        restaurantIsAvailableViewConstraint = restaurantIsAvailableView.heightAnchor.constraint(equalToConstant:  60)
        restaurantIsAvailableViewConstraint?.isActive = true
    }
    
    func showSetNextAvailablityItems(){
        restaurantNextAvailbaleDatePickerLabel.isHidden = false
        datePicker.isHidden = false
        restaurantNextAvailablityUpdateAndCancelButtonHorizontalStackView.isHidden = false
        restaurantIsAvailableViewConstraint?.isActive = false
        restaurantIsAvailableViewConstraint = restaurantIsAvailableView.heightAnchor.constraint(equalToConstant: 180)
        restaurantIsAvailableViewConstraint?.isActive = true
    }
    
   
  
    
    private func createRestaurantDetailsContentsInView(){
        self.addSubview(restaurantProfileImageView)
        self.addSubview(restaurantNameLabel)
        self.addSubview(restaurantPhoneNumberLabel)
        self.addSubview(restaurantCuisineLabel)
        self.addSubview(restaurantDescriptionLabel)
        self.addSubview(restaurantAddressLabel)
        self.addSubview(restaurantStarRatingLabel)
        self.addSubview(totalNumberOfrestaurantStarRatingLabel)
        self.addSubview(starIconImageView)
        self.addSubview(availableLabel)
        addLineAtTopAndBottomOfStarRatingAndAvailablity()
    }
    
    private func addLineAtTopAndBottomOfStarRatingAndAvailablity(){
        createLineView(bottonOfItem: restaurantIsAvailableView, paddingConstant: 10, color: .systemGray5)
        createLineView(bottonOfItem: totalNumberOfrestaurantStarRatingLabel, paddingConstant: 5, color: .systemGray5)
    }
    
    private func createLineView(bottonOfItem : UIView,paddingConstant : CGFloat,color : UIColor){
        let lineView = UIView()
        self.addSubview(lineView)
        lineView.translatesAutoresizingMaskIntoConstraints = false
        lineView.topAnchor.constraint(equalTo: bottonOfItem.bottomAnchor,constant: paddingConstant).isActive = true
        lineView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 15).isActive = true
        lineView.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -15).isActive = true
        lineView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        lineView.backgroundColor = color
        
    }
    
   
    
    private func activateRestaurantContentConstraints(){
        NSLayoutConstraint.activate(getRestaurantProfileImageViewContraints())
        NSLayoutConstraint.activate(getRestaurantNameLabelContraints())
        NSLayoutConstraint.activate(getRestaurantPhoneNumberLabelContraints())
        NSLayoutConstraint.activate(getRestaurantCuisineLabelContraints())
        NSLayoutConstraint.activate(getRestaurantDescriptionLabelContraints())
        NSLayoutConstraint.activate(getRestaurantAddressLabelContraints())
        NSLayoutConstraint.activate(getRestaurantIsAvailableViewContraints())
        NSLayoutConstraint.activate(getStarIconCellImageViewContraints())
        NSLayoutConstraint.activate(getTotalNumberOfrestaurantStarRatingLabelContraints())
        NSLayoutConstraint.activate(getRestaurantStarRatingLabelContraints())
        NSLayoutConstraint.activate(getAvailablityLabelContraints())
        
        
    }
    
    private func getEditButtonContraints() -> [NSLayoutConstraint] {
        let buttonContraints = [
            editButton.topAnchor.constraint(equalTo: self.topAnchor,constant: 10),
            editButton.widthAnchor.constraint(equalToConstant: 70),
            editButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
            editButton.heightAnchor.constraint(equalToConstant: 20)]
        return buttonContraints
    }
    
    
    private func getRestaurantProfileImageViewContraints() -> [NSLayoutConstraint]{
        let imageContraints = [
            restaurantProfileImageView.topAnchor.constraint(equalTo: self.topAnchor , constant: 10),
            restaurantProfileImageView.centerXAnchor.constraint(equalTo: self.centerXAnchor , constant: 10),
            restaurantProfileImageView.widthAnchor.constraint(equalToConstant:(120)),
            restaurantProfileImageView.heightAnchor.constraint(equalToConstant:120)]
        return imageContraints
    }
    
    private func getRestaurantNameLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [restaurantNameLabel.topAnchor.constraint(equalTo: restaurantProfileImageView.bottomAnchor,constant: 40),
            restaurantNameLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            restaurantNameLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10)]
        return labelContraints
    }
    
    private func getRestaurantPhoneNumberLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [restaurantPhoneNumberLabel.topAnchor.constraint(equalTo: restaurantNameLabel.bottomAnchor,constant: 40),
                               restaurantPhoneNumberLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                               restaurantPhoneNumberLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
                               ]
        return labelContraints
    }
    
    private func getRestaurantCuisineLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
            restaurantCuisineLabel.topAnchor.constraint(equalTo: restaurantPhoneNumberLabel.bottomAnchor,constant: 40),
            restaurantCuisineLabel.leftAnchor.constraint(equalTo: self.leftAnchor , constant: 10),
            restaurantCuisineLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10)]
        return labelContraints
    }
    
    private func getRestaurantDescriptionLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
            restaurantDescriptionLabel.topAnchor.constraint(equalTo: restaurantCuisineLabel.bottomAnchor,constant: 40),
            restaurantDescriptionLabel.leftAnchor.constraint(equalTo: self.leftAnchor , constant: 10),
            restaurantDescriptionLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10)
        ]
        return labelContraints
    }
    
    

    private func getRestaurantAddressLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            restaurantAddressLabel.topAnchor.constraint(equalTo: restaurantDescriptionLabel.bottomAnchor ,constant: 40),
            restaurantAddressLabel.leftAnchor.constraint(equalTo: self.leftAnchor , constant: 10),
            restaurantAddressLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10)
        ]
        return labelContraints
    }
    
    private func getRestaurantIsAvailableViewContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            restaurantIsAvailableView.topAnchor.constraint(equalTo: restaurantAddressLabel.bottomAnchor ,constant: 30),
            restaurantIsAvailableView.leftAnchor.constraint(equalTo: self.leftAnchor),
            restaurantIsAvailableView.rightAnchor.constraint(equalTo: self.rightAnchor)
        ]
        return labelContraints
    }

    private func getStarIconCellImageViewContraints() -> [NSLayoutConstraint]{
        let imageContraints = [
            starIconImageView.topAnchor.constraint(equalTo: restaurantIsAvailableView.bottomAnchor ,constant: 20),
            starIconImageView.rightAnchor.constraint(equalTo: totalNumberOfrestaurantStarRatingLabel.centerXAnchor),
            starIconImageView.widthAnchor.constraint(equalToConstant: 30),
            starIconImageView.heightAnchor.constraint(equalToConstant: 30)]
        return imageContraints
    }

    private func getTotalNumberOfrestaurantStarRatingLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            totalNumberOfrestaurantStarRatingLabel.topAnchor.constraint(equalTo: starIconImageView.bottomAnchor),
            totalNumberOfrestaurantStarRatingLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            totalNumberOfrestaurantStarRatingLabel.rightAnchor.constraint(equalTo: self.centerXAnchor ,constant: -10),
            totalNumberOfrestaurantStarRatingLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    private func getRestaurantStarRatingLabelContraints() ->[NSLayoutConstraint] {
        let labelContraints = [
            restaurantStarRatingLabel.topAnchor.constraint(equalTo: restaurantIsAvailableView.bottomAnchor,constant: 20),
            restaurantStarRatingLabel.leftAnchor.constraint(equalTo: starIconImageView.rightAnchor),
            restaurantStarRatingLabel.rightAnchor.constraint(equalTo: self.centerXAnchor ,constant: -10),
            restaurantStarRatingLabel.heightAnchor.constraint(equalToConstant: 30)]
        return labelContraints
    }
    private func getAvailablityLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
            availableLabel.topAnchor.constraint(equalTo: restaurantIsAvailableView.bottomAnchor,constant: 20),
            availableLabel.leftAnchor.constraint(equalTo: self.centerXAnchor,constant: 30),
            availableLabel.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
            availableLabel.heightAnchor.constraint(equalToConstant: 60)]
        return labelContraints
    }
    
    private func getContactUsLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [contactUSLabel.topAnchor.constraint(equalTo: totalNumberOfrestaurantStarRatingLabel.bottomAnchor,constant: 20),
                               contactUSLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                               contactUSLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
                               contactUSLabel.heightAnchor.constraint(equalToConstant: 40)]
        return labelContraints
    }
    
    private func getSettingsButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [settingButton.topAnchor.constraint(equalTo: moreButton.bottomAnchor,constant: 10),
                                settingButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                                settingButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
                                settingButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    private func getMoreButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [moreButton.topAnchor.constraint(equalTo: contactUSLabel.bottomAnchor,constant: 10),
                                moreButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                                moreButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
                                moreButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    private func getChangePasswordButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [changePasswordButton.topAnchor.constraint(equalTo: settingButton.bottomAnchor,constant: 10),
                                changePasswordButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                                changePasswordButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
                                changePasswordButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    private func getLogoutButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [logOutButton.topAnchor.constraint(equalTo: changePasswordButton.bottomAnchor,constant: 10),
                                logOutButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                                logOutButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
                                logOutButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
   
    
    private func getOrderDetailsTableViewLayoutConstraints() -> [NSLayoutConstraint]{
        let tableViewConstraints = [
            orderDetailsTableView.topAnchor.constraint(equalTo: logOutButton.bottomAnchor,constant: 10),
            orderDetailsTableView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            orderDetailsTableView.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10)
            ]
        return tableViewConstraints
    }
    
   

   
    
    private func getviewMoreOrdersButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [viewMoreOrdersButton.topAnchor.constraint(equalTo: orderDetailsTableView.bottomAnchor,constant: 10),
                                viewMoreOrdersButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                                viewMoreOrdersButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),
                                viewMoreOrdersButton.heightAnchor.constraint(equalToConstant: 25)]
        return buttonContraints
    }
    
    @objc func didTapLogout(){
        delegate?.didTapLogout()
    }
    
    @objc func didTapCancelUpdateRestaurantAvailablity(){
        delegate?.didTapCancelUpdateRestaurantAvailablity()
    }
    @objc func didTapUpdateRestaurantAvailablity(){
        delegate?.didTapUpdateRestaurantAvailablity()
    }
    
    @objc func didChangeDateAndtime(sender: UIDatePicker){
        delegate?.didChangeDateAndtime(sender: sender)
    }
    
    @objc func didTapRestaurantIsAvailableSwitch(sender: UISwitch){
        delegate?.didTapRestaurantIsAvailableSwitch(sender: sender)
    }
    
    @objc func didTapViewMorePastOrders(){
        delegate?.didTapViewMorePastOrders()
    }
    @objc func didTapSettingsButton(){
        delegate?.didTapSettingsButton()
    }
    
    @objc func didTapMoreButton(){
        delegate?.didTapMoreButton()
    }
    @objc func didTapChangePassword(){
        delegate?.didTapChangePassword()
    }
    
    @objc func didTapEditButton(){
        delegate?.didTapEditButton()
    }
}

protocol RestaurantAccountsPageViewDelegate : AnyObject{
    func didTapLogout()
    func didTapCancelUpdateRestaurantAvailablity()
    func didTapUpdateRestaurantAvailablity()
    func didChangeDateAndtime(sender: UIDatePicker)
    func didTapRestaurantIsAvailableSwitch(sender: UISwitch)
    func didTapViewMorePastOrders()
    func didTapSettingsButton()
    func didTapMoreButton()
    func didTapChangePassword()
    func didTapEditButton()
}
