//
//  File.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 20/03/22.
//

import Foundation
import UIKit

class PastOrdersDetailsView : OrderDetailsPageView ,PastOrdersDetailsViewProtocol{
   
    
   
    
    override init() {
        super.init()
        
    }
    
    override func initialiseViewElements() {
        addNavigationBarTitleView()
        createOrderAndUserDetailsContents()
        createMenuDetailsTableView()
        createInstructionToRestaurantTextView()
        createBillDetailsElements()
        self.addSubview(cancelOrderReasonLabel)
        NSLayoutConstraint.activate(
            getCancelOrderReasonLabelConstraints())
        self.addSubview(restaurantRatingLabel)
        NSLayoutConstraint.activate(
            getRestaurantRatingLabelConstraints())
    }
    
    override func layoutSubviews() {
        
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var cancelOrderReasonLabel : UILabel = {
        let  cancelOrderReasonLabel = UILabel()
        cancelOrderReasonLabel.textColor = .black
        cancelOrderReasonLabel.backgroundColor = .white
        cancelOrderReasonLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        cancelOrderReasonLabel.adjustsFontSizeToFitWidth = true
        cancelOrderReasonLabel.textAlignment = .left
        cancelOrderReasonLabel.translatesAutoresizingMaskIntoConstraints = false
        cancelOrderReasonLabel.numberOfLines = 5
        cancelOrderReasonLabel.lineBreakMode = .byWordWrapping
        cancelOrderReasonLabel.textAlignment = .natural
        cancelOrderReasonLabel.sizeToFit()
        return cancelOrderReasonLabel
    }()
    
    var restaurantRatingLabel : UILabel = {
        let  restaurantRatingLabel = UILabel()
        restaurantRatingLabel.textColor = .black
        restaurantRatingLabel.backgroundColor = .white
        restaurantRatingLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        restaurantRatingLabel.textAlignment = .left
        restaurantRatingLabel.translatesAutoresizingMaskIntoConstraints = false
        restaurantRatingLabel.numberOfLines = 5
        restaurantRatingLabel.lineBreakMode = .byWordWrapping
        restaurantRatingLabel.textAlignment = .left
        restaurantRatingLabel.sizeToFit()
        restaurantRatingLabel.addLabelToTopBorder(labelText: "Restaurant Rating And Feed back", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        return restaurantRatingLabel
    }()
   
    

    override func createMenuDetailsTableView(){
        menuDetailsTableView.register(RestaurantPastOrderMenuDetailsTableViewCell.self, forCellReuseIdentifier: RestaurantPastOrderMenuDetailsTableViewCell.cellIdentifier)
        super.createMenuDetailsTableView()
    }
    
    
    override func getOrderMenuCellHeight() -> CGFloat{
        return 60
    }
    
   
    
    func createMenuDetailsContentView(indexPath : IndexPath ,tableView : UITableView,menuCellContents : ( menuName : String, menuTarianType : String, menuPriceSubTotal : Int, quantity : Int)) -> UITableViewCell{
        let menuDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: RestaurantPastOrderMenuDetailsTableViewCell.cellIdentifier, for: indexPath) as? RestaurantPastOrderMenuDetailsTableViewCell
        menuDetailsTableViewCell?.cellHeight = 60
        menuDetailsTableViewCell?.configureContents(menuCellContents: menuCellContents)
        
        return menuDetailsTableViewCell ?? UITableViewCell()
    }
    
    
    
    func updateCancelOrderReasonLabel(cancelReason : String,orderStatus : OrderStatus){
        if(orderStatus == .Cancelled){
            cancelOrderReasonLabel.addLabelToTopBorder(labelText: "Cancellation Reason : ", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        }
        else if(orderStatus == .Declined){
            cancelOrderReasonLabel.addLabelToTopBorder(labelText: "Declination Reason : ", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        }
        cancelOrderReasonLabel.text = cancelReason
    }
    
    private func getCancelOrderReasonLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            cancelOrderReasonLabel.topAnchor.constraint(equalTo: billView.bottomAnchor,constant: 50),
            cancelOrderReasonLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            cancelOrderReasonLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            cancelOrderReasonLabel.heightAnchor.constraint(equalToConstant: 60)]
        return labelContraints
    }
    
    
   
    
    func updateRestaurantRatingLabel(rating : Int , restaurantFeedback : String){
        if(rating != 0 ){
            restaurantRatingLabel.text = "\(rating) ⭐️  |  \(restaurantFeedback.count > 0 ? restaurantFeedback : "No Feedback")"
        }
        else{
            restaurantRatingLabel.text = "Not Rated Yet"
        }
    }
    
    private func getRestaurantRatingLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            restaurantRatingLabel.topAnchor.constraint(equalTo: billView.bottomAnchor,constant: 100),
            restaurantRatingLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            restaurantRatingLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            restaurantRatingLabel.heightAnchor.constraint(equalToConstant: 60)]
        return labelContraints
    }
    
    func activateContentViewBottomAnchorWithRespectToRestaurantRatingLabelConstraints(){
        if(restaurantRatingLabel.frame.maxY > 0){
            self.removeHeightAnchorConstraints()
            self.heightAnchor.constraint(equalToConstant:  restaurantRatingLabel.frame.maxY + 100).isActive = true
        }
        else{
            self.removeHeightAnchorConstraints()
            self.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.maxY).isActive = true
        }
    }
    
}
