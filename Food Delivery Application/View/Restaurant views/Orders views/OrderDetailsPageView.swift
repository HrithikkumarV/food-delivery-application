//
//  OrderDetailsPageView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 16/03/22.
//

import Foundation
import UIKit

class OrderDetailsPageView : UIView,OrderDetailsPageViewProtocol{
    
    weak var delegate : OrderDetailsPageViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialiseViewElements(){
        addNavigationBarTitleView()
        createOrderAndUserDetailsContents()
        createMenuDetailsTableView()
        createInstructionToRestaurantTextView()
        createBillDetailsElements()
        createOrderBottomView()
        createDeclineOrderButton()
        createupdateOrderStatusButtonButton()
    }
   
    override func layoutSubviews() {
        super.layoutSubviews()
        NSLayoutConstraint.activate(getOrderBottomViewLayoutConstraints())
    }
    
    private var orderStausNavigationBarLabel : UILabel = {
        let orderStausNavigationBarLabel = UILabel()
        orderStausNavigationBarLabel.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 22)
        orderStausNavigationBarLabel.textColor = .black.withAlphaComponent(0.9)
        orderStausNavigationBarLabel.backgroundColor = .clear
        orderStausNavigationBarLabel.font = UIFont(name:"ArialRoundedMTBold", size: 25.0)
        orderStausNavigationBarLabel.adjustsFontSizeToFitWidth = true
        orderStausNavigationBarLabel.textAlignment = .left
        return orderStausNavigationBarLabel
    }()
    
    lazy var totalItemsAndPriceNavigationBarLabel : UILabel  = {
        let  totalItemsAndPriceNavigationBarLabel = UILabel()
        totalItemsAndPriceNavigationBarLabel.frame = CGRect(x: 0, y: orderStausNavigationBarLabel.frame.maxY, width: UIScreen.main.bounds.width, height: 22)
        totalItemsAndPriceNavigationBarLabel.textColor = .gray
        totalItemsAndPriceNavigationBarLabel.backgroundColor = .clear
        totalItemsAndPriceNavigationBarLabel.textAlignment = .left
        totalItemsAndPriceNavigationBarLabel.font = UIFont(name:"ArialRoundedMTBold", size: 12.0)
        totalItemsAndPriceNavigationBarLabel.lineBreakMode = .byWordWrapping
        totalItemsAndPriceNavigationBarLabel.adjustsFontSizeToFitWidth = true
        return totalItemsAndPriceNavigationBarLabel
    }()
    var navigationBarTitleView : UIView = {
        let   navigationBarTitleView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 100, height: 44))
        navigationBarTitleView.backgroundColor = .clear
        return navigationBarTitleView
    }()
    
    private var orderDetailsStackView : UIStackView  = {
        let orderDetailsStackView = UIStackView()
        orderDetailsStackView.axis = .vertical
        orderDetailsStackView.distribution = .fillProportionally
        orderDetailsStackView.spacing = 25
        orderDetailsStackView.translatesAutoresizingMaskIntoConstraints = false
        return orderDetailsStackView
    }()
    private var orderIdLabel : UILabel = {
        let orderIdLabel = UILabel()
        orderIdLabel.backgroundColor = .white
        orderIdLabel.translatesAutoresizingMaskIntoConstraints = false
        orderIdLabel.textColor = .black
        orderIdLabel.addLabelToTopBorder(labelText: "Order ID : ", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        orderIdLabel.textAlignment = .left
        orderIdLabel.adjustsFontSizeToFitWidth = true
        orderIdLabel.font = UIFont.systemFont(ofSize: 20)
        return orderIdLabel
    }()
    private var dateAndTimeLabel : UILabel = {
        let dateAndTimeLabel = UILabel()
        dateAndTimeLabel.addLabelToTopBorder(labelText: "Order Time : ", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        dateAndTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        dateAndTimeLabel.backgroundColor = .white
        dateAndTimeLabel.textColor = .black
        dateAndTimeLabel.textAlignment = .left
        dateAndTimeLabel.adjustsFontSizeToFitWidth = true
        dateAndTimeLabel.font = UIFont.systemFont(ofSize: 20)
        return dateAndTimeLabel
    }()
    private var userNameLabel : UILabel = {
        let userNameLabel = UILabel()
        userNameLabel.addLabelToTopBorder(labelText: "Customer Name : ", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        userNameLabel.textColor = .black
        userNameLabel.translatesAutoresizingMaskIntoConstraints = false
        userNameLabel.font = UIFont.systemFont(ofSize: 20,weight: .medium)
        userNameLabel.textAlignment = .left
        return userNameLabel
    }()
    
    private var userPhoneNumberLabel : UILabel = {
        let userPhoneNumberLabel = UILabel()
        userPhoneNumberLabel.addLabelToTopBorder(labelText: "Customer Phone Number : ", borderColor: .clear, borderWidth: 1, leftPadding: 0, topPadding: -7, textColor: .black.withAlphaComponent(0.7), fontSize: 15)
        userPhoneNumberLabel.textColor = .black
        userPhoneNumberLabel.translatesAutoresizingMaskIntoConstraints = false
        userPhoneNumberLabel.font = UIFont.systemFont(ofSize: 20,weight: .medium)
        return userPhoneNumberLabel
    }()
    
    private var instructionsToRestaurant : UITextView  = {
        let instructionsToRestaurant = UITextView()
        instructionsToRestaurant.isUserInteractionEnabled = false
        instructionsToRestaurant.backgroundColor = .white
        instructionsToRestaurant.textColor = .systemGray
        instructionsToRestaurant.font = UIFont(name: "ArialHebrew", size: 16)
        instructionsToRestaurant.translatesAutoresizingMaskIntoConstraints = false
        instructionsToRestaurant.layer.cornerRadius = 5
        instructionsToRestaurant.layer.borderColor = UIColor.systemGray5.cgColor
        instructionsToRestaurant.layer.borderWidth = 1
        return instructionsToRestaurant
    }()
    var billView : UIView = {
        let billView = UIView()
        billView.translatesAutoresizingMaskIntoConstraints = false
        return billView
    }()
    private var billDetailsLabel : UILabel = {
        let billDetailsLabel = UILabel()
        billDetailsLabel.textColor = .black
        billDetailsLabel.backgroundColor = .white
        billDetailsLabel.font = UIFont(name:"ArialRoundedMTBold", size: 25.0)
        billDetailsLabel.adjustsFontSizeToFitWidth = true
        billDetailsLabel.textAlignment = .left
        billDetailsLabel.text = "Bill Details"
        billDetailsLabel.translatesAutoresizingMaskIntoConstraints = false
        return billDetailsLabel
    }()
    
    private var itemTotalLabel : UILabel = {
        let itemTotalLabel = UILabel()
        itemTotalLabel.textColor = .black.withAlphaComponent(0.9)
        itemTotalLabel.backgroundColor = .white
        itemTotalLabel.font = UIFont.systemFont(ofSize: 14)
        itemTotalLabel.adjustsFontSizeToFitWidth = true
        itemTotalLabel.textAlignment = .left
        itemTotalLabel.text = "Item Total"
        itemTotalLabel.translatesAutoresizingMaskIntoConstraints = false
        return itemTotalLabel
    }()
    private var itemTotal : UILabel = {
        let  itemTotal = UILabel()
        itemTotal.textColor = .black.withAlphaComponent(0.9)
        itemTotal.backgroundColor = .white
        itemTotal.font = UIFont.systemFont(ofSize: 14)
        itemTotal.adjustsFontSizeToFitWidth = true
        itemTotal.textAlignment = .left
        itemTotal.text = "Hii"
        itemTotal.translatesAutoresizingMaskIntoConstraints = false
        return itemTotal
    }()
    private var deliveryFeeLabel : UILabel = {
        let  deliveryFeeLabel = UILabel()
        deliveryFeeLabel.textColor = .black.withAlphaComponent(0.9)
        deliveryFeeLabel.backgroundColor = .white
        deliveryFeeLabel.font = UIFont.systemFont(ofSize: 14)
        deliveryFeeLabel.adjustsFontSizeToFitWidth = true
        deliveryFeeLabel.textAlignment = .left
        deliveryFeeLabel.text = "delivery fee"
        deliveryFeeLabel.translatesAutoresizingMaskIntoConstraints = false
        return deliveryFeeLabel
    }()
    private var deliveryFee : UILabel = {
        let deliveryFee = UILabel()
        deliveryFee.textColor = .black.withAlphaComponent(0.9)
        deliveryFee.backgroundColor = .white
        deliveryFee.font = UIFont.systemFont(ofSize: 14)
        deliveryFee.adjustsFontSizeToFitWidth = true
        deliveryFee.textAlignment = .left
        deliveryFee.translatesAutoresizingMaskIntoConstraints = false
        return deliveryFee
    }()
    private var restaurantGSTLabel : UILabel = {
        let restaurantGSTLabel = UILabel()
        restaurantGSTLabel.textColor = .black.withAlphaComponent(0.9)
        restaurantGSTLabel.backgroundColor = .white
        restaurantGSTLabel.font = UIFont.systemFont(ofSize: 14)
        restaurantGSTLabel.adjustsFontSizeToFitWidth = true
        restaurantGSTLabel.textAlignment = .left
        restaurantGSTLabel.text = "Restaurant GST @ 5%"
        restaurantGSTLabel.translatesAutoresizingMaskIntoConstraints = false
        return restaurantGSTLabel
    }()
    private var restaurantPackagingChargesLabel : UILabel = {
        let restaurantPackagingChargesLabel = UILabel()
        restaurantPackagingChargesLabel.textColor = .black.withAlphaComponent(0.9)
        restaurantPackagingChargesLabel.backgroundColor = .white
        restaurantPackagingChargesLabel.font = UIFont.systemFont(ofSize: 14)
        restaurantPackagingChargesLabel.adjustsFontSizeToFitWidth = true
        restaurantPackagingChargesLabel.textAlignment = .left
        restaurantPackagingChargesLabel.text = "Restaurant Packaging Charges"
        restaurantPackagingChargesLabel.translatesAutoresizingMaskIntoConstraints = false
        return restaurantPackagingChargesLabel
    }()
    
    private var restaurantGST : UILabel = {
        let restaurantGST = UILabel()
        restaurantGST.textColor = .black.withAlphaComponent(0.9)
        restaurantGST.backgroundColor = .white
        restaurantGST.font = UIFont.systemFont(ofSize: 14)
        restaurantGST.adjustsFontSizeToFitWidth = true
        restaurantGST.textAlignment = .left
        restaurantGST.translatesAutoresizingMaskIntoConstraints = false
        return restaurantGST
    }()
    
    private var restaurantPackagingCharges : UILabel = {
        let  restaurantPackagingCharges = UILabel()
        restaurantPackagingCharges.textColor = .black.withAlphaComponent(0.9)
        restaurantPackagingCharges.backgroundColor = .white
        restaurantPackagingCharges.font = UIFont.systemFont(ofSize: 14)
        restaurantPackagingCharges.adjustsFontSizeToFitWidth = true
        restaurantPackagingCharges.textAlignment = .left
        restaurantPackagingCharges.translatesAutoresizingMaskIntoConstraints = false
        return restaurantPackagingCharges
    }()
    
    private var itemDiscountLabel : UILabel = {
        let  itemDiscountLabel = UILabel()
        itemDiscountLabel.textColor = .systemGreen
        itemDiscountLabel.backgroundColor = .white
        itemDiscountLabel.font = UIFont.systemFont(ofSize: 14)
        itemDiscountLabel.adjustsFontSizeToFitWidth = true
        itemDiscountLabel.textAlignment = .left
        itemDiscountLabel.text = "Item Discount"
        itemDiscountLabel.translatesAutoresizingMaskIntoConstraints = false
        return itemDiscountLabel
    }()
    private var itemDiscount : UILabel = {
        let itemDiscount = UILabel()
        itemDiscount.textColor = .systemGreen.withAlphaComponent(0.9)
        itemDiscount.backgroundColor = .white
        itemDiscount.font = UIFont.systemFont(ofSize: 14)
        itemDiscount.adjustsFontSizeToFitWidth = true
        itemDiscount.textAlignment = .left
        itemDiscount.translatesAutoresizingMaskIntoConstraints = false
        return itemDiscount
    }()
    
    private var totalAmountLabel : UILabel = {
        let  totalAmountLabel = UILabel()
        totalAmountLabel.textColor = .black
        totalAmountLabel.backgroundColor = .white
        totalAmountLabel.font = UIFont(name:"ArialRoundedMTBold", size: 25.0)
        totalAmountLabel.adjustsFontSizeToFitWidth = true
        totalAmountLabel.textAlignment = .left
        totalAmountLabel.text = "Bill Total"
        totalAmountLabel.translatesAutoresizingMaskIntoConstraints = false
        return totalAmountLabel
    }()
    private var totalAmount : UILabel = {
        let  totalAmount = UILabel()
        totalAmount.textColor = .black
        totalAmount.backgroundColor = .white
        totalAmount.font = UIFont(name:"ArialRoundedMTBold", size: 25.0)
        totalAmount.adjustsFontSizeToFitWidth = true
        totalAmount.textAlignment = .left
        totalAmount.translatesAutoresizingMaskIntoConstraints = false
        return totalAmount
    }()
    
    private var billstackView : UIStackView  = {
        let billstackView = UIStackView()
        billstackView.axis = .vertical
        billstackView.distribution = .fillProportionally
        billstackView.spacing = 5
        billstackView.translatesAutoresizingMaskIntoConstraints = false
        return billstackView
    }()
    lazy var itemTotalLabelsStackView : UIStackView = {
        let  itemTotalLabelsStackView = getHorizontalStackView()
        itemTotalLabelsStackView.addArrangedSubview(itemTotalLabel)
        itemTotalLabelsStackView.addArrangedSubview(itemTotal)
        return itemTotalLabelsStackView
    }()
    lazy var deliveryFeeLabelsStackView : UIStackView = {
        let deliveryFeeLabelsStackView = getHorizontalStackView()
        deliveryFeeLabelsStackView.addArrangedSubview(deliveryFeeLabel)
        deliveryFeeLabelsStackView.addArrangedSubview(deliveryFee)
        return deliveryFeeLabelsStackView
    }()
    lazy var restaurantGSTLabelsStackView : UIStackView = {
        let restaurantGSTLabelsStackView = getHorizontalStackView()
        restaurantGSTLabelsStackView.addArrangedSubview(restaurantGSTLabel)
        restaurantGSTLabelsStackView.addArrangedSubview(restaurantGST)
        return restaurantGSTLabelsStackView
    }()
    lazy var restaurantPackagingChargesLabelsStackView : UIStackView = {
        let restaurantPackagingChargesLabelsStackView = getHorizontalStackView()
        restaurantPackagingChargesLabelsStackView.addArrangedSubview(restaurantPackagingChargesLabel)
        restaurantPackagingChargesLabelsStackView.addArrangedSubview(restaurantPackagingCharges)
        return restaurantPackagingChargesLabelsStackView
    }()
    lazy var itemDiscountLabelsStackView : UIStackView = {
        let itemDiscountLabelsStackView = getHorizontalStackView()
        itemDiscountLabelsStackView.addArrangedSubview(itemDiscountLabel)
        itemDiscountLabelsStackView.addArrangedSubview(itemDiscount)
        return itemDiscountLabelsStackView
    }()
    
    lazy var totalAmountLabelsStackView : UIStackView  = {
        let totalAmountLabelsStackView = getHorizontalStackView()
        totalAmountLabelsStackView.addArrangedSubview(totalAmountLabel)
        totalAmountLabelsStackView.addArrangedSubview(totalAmount)
        
        return totalAmountLabelsStackView
    }()
    
    lazy var declineOrderButton : UIButton = {
        let  declineOrderButton = UIButton()
        declineOrderButton.translatesAutoresizingMaskIntoConstraints = false
        declineOrderButton.backgroundColor = .white
        declineOrderButton.setTitle("Decline", for: .normal)
        declineOrderButton.setTitleColor(.systemRed, for: .normal)
        declineOrderButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        declineOrderButton.layer.cornerRadius = 5
        declineOrderButton.layer.borderWidth = 1
        declineOrderButton.layer.borderColor = UIColor.systemRed.cgColor
        declineOrderButton.addTarget(self, action: #selector(didTapDeclineOrder), for: .touchUpInside)
        return declineOrderButton
    }()
    
    lazy var updateOrderStatusButton : UIButton = {
        let updateOrderStatusButton = UIButton()
        updateOrderStatusButton.translatesAutoresizingMaskIntoConstraints = false
        updateOrderStatusButton.backgroundColor = .white
        updateOrderStatusButton.setTitle("Confirm", for: .normal)
        updateOrderStatusButton.setTitleColor(.systemGreen, for: .normal)
        updateOrderStatusButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        updateOrderStatusButton.layer.cornerRadius = 5
        updateOrderStatusButton.layer.borderWidth = 1
        updateOrderStatusButton.layer.borderColor = UIColor.systemGreen.cgColor
        updateOrderStatusButton.addTarget(self, action: #selector(didTapUpdateOrderStatusButton), for: .touchUpInside)
        return updateOrderStatusButton
    }()
    var orderBottomView : UIView = {
        let orderBottomView = UIView()
        orderBottomView.translatesAutoresizingMaskIntoConstraints = false
        orderBottomView.backgroundColor = .white
        return orderBottomView
    }()
  
    
    
    private func createOrderStatusNavigationBarLabel(){
        
        navigationBarTitleView.addSubview(orderStausNavigationBarLabel)
    }
    
    
       
    
    private func createTotalItemsAndPriceNavigationBarLabel(){
       
        navigationBarTitleView.addSubview(totalItemsAndPriceNavigationBarLabel)
    }
    
    func updateTotalItemsAndPriceNavigationBarLabelText(menuCount : Int,totalPrice : Int){
        
        if(menuCount == 1){
            totalItemsAndPriceNavigationBarLabel.text =
            " \(menuCount) Item | ₹\(totalPrice)"
        }
        else{
            totalItemsAndPriceNavigationBarLabel.text = " \(menuCount) Items | ₹\(totalPrice)"
        }
    }
    
    func updateOrderStatus(orderStatus : OrderStatus){
        switch orderStatus {
        case .Pending:
            orderStausNavigationBarLabel.text = "Order Pending"
            orderStausNavigationBarLabel.textColor = .systemRed
            totalItemsAndPriceNavigationBarLabel.textColor = .systemRed

        case .Preparing:
            orderStausNavigationBarLabel.text = "Order Preparing"
            orderStausNavigationBarLabel.textColor = .systemGreen
            totalItemsAndPriceNavigationBarLabel.textColor = .systemGreen

        case .Ready:
            orderStausNavigationBarLabel.text = "Order Ready"
            orderStausNavigationBarLabel.textColor = .systemGreen
            totalItemsAndPriceNavigationBarLabel.textColor = .systemGreen

        case .Delivered:
            orderStausNavigationBarLabel.text = "Order Delivered"
            orderStausNavigationBarLabel.textColor = .systemGreen
            totalItemsAndPriceNavigationBarLabel.textColor = .systemGreen

        case .Cancelled:
            orderStausNavigationBarLabel.text = "Order Cancelled"
            orderStausNavigationBarLabel.textColor = .systemRed
            totalItemsAndPriceNavigationBarLabel.textColor = .systemRed

        case .Declined:
            orderStausNavigationBarLabel.text = "Order Declined"
            orderStausNavigationBarLabel.textColor = .systemRed
            totalItemsAndPriceNavigationBarLabel.textColor = .systemRed

        }

    }
    
    func addNavigationBarTitleView() {

        createOrderStatusNavigationBarLabel()
        createTotalItemsAndPriceNavigationBarLabel()
    }
    
    func updateNavigationBarTitleViewLabel(menuCount : Int,totalPrice : Int,orderStatus : OrderStatus){
        updateTotalItemsAndPriceNavigationBarLabelText(menuCount: menuCount, totalPrice: totalPrice)
        updateOrderStatus(orderStatus: orderStatus)
    }
    private func getOrderStackViewContraints() -> [NSLayoutConstraint]{
        let labelContraints = [orderDetailsStackView.topAnchor.constraint(equalTo: self.topAnchor , constant: 25),
                               orderDetailsStackView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                               orderDetailsStackView.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -10),]
        return labelContraints
    }
    
    private func getOrderIdLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [ orderIdLabel.heightAnchor.constraint(equalToConstant: 40)]
        return labelContraints
    }
    private func getDateAndTimeLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
           
            dateAndTimeLabel.heightAnchor.constraint(equalToConstant: 40)]
        return labelContraints
    }
    
 
    
    private func getUserNameLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            userNameLabel.heightAnchor.constraint(equalToConstant: 40)]
        return labelContraints
    }
    private func getUserPhoneNumberLabelContraints() -> [NSLayoutConstraint] {
        let labelContraints = [
            
            userPhoneNumberLabel.heightAnchor.constraint(equalToConstant: 40)]
        return labelContraints
    }
    
    func createOrderAndUserDetailsContents(){
        self.addSubview(orderDetailsStackView)
        orderDetailsStackView.addArrangedSubview(orderIdLabel)
        orderDetailsStackView.addArrangedSubview(dateAndTimeLabel)
        orderDetailsStackView.addArrangedSubview(userNameLabel)
        orderDetailsStackView.addArrangedSubview(userPhoneNumberLabel)
        NSLayoutConstraint.activate(getOrderStackViewContraints())
        NSLayoutConstraint.activate(getOrderIdLabelContraints())
        NSLayoutConstraint.activate(getDateAndTimeLabelContraints())
        NSLayoutConstraint.activate(getUserNameLabelContraints())
        NSLayoutConstraint.activate(getUserPhoneNumberLabelContraints())
    }
    
    func updateUserDetailsAndOrderDetailsLabel(userName : String,userPhoneNumber : String,dateAndTime : String,orderId :String){
        userNameLabel.text = userName
        userPhoneNumberLabel.text = userPhoneNumber
        dateAndTimeLabel.text =   dateAndTime
        orderIdLabel.text = orderId
    }
    
    lazy var menuDetailsTableView : UITableView = {
        let menuDetailsTableView = UITableView()
        menuDetailsTableView.backgroundColor = .white
        menuDetailsTableView.isScrollEnabled = false
        menuDetailsTableView.separatorStyle = .none
        menuDetailsTableView.layer.borderColor = UIColor.systemGray4.cgColor
        menuDetailsTableView.layer.borderWidth = 1
        menuDetailsTableView.layer.cornerRadius = 5
        menuDetailsTableView.bounces = false
        menuDetailsTableView.register(RestaurantOrderMenuDetailsTableViewCell.self, forCellReuseIdentifier: RestaurantOrderMenuDetailsTableViewCell.cellIdentifier)
        menuDetailsTableView.translatesAutoresizingMaskIntoConstraints = false
        return menuDetailsTableView
    }()
    
    
    
    func getOrderMenuCellHeight() -> CGFloat{
        return 60
    }
    
    
    func createMenuDetailsContentView(indexPath : IndexPath ,tableView : UITableView,menuCellContents : ( menuName : String, menuTarianType : String, menuPriceSubTotal : Int,  menuAvailable : Int, quantity : Int)) -> UITableViewCell{
        let menuDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: RestaurantOrderMenuDetailsTableViewCell.cellIdentifier, for: indexPath) as? RestaurantOrderMenuDetailsTableViewCell
        menuDetailsTableViewCell?.cellHeight = 60
        menuDetailsTableViewCell?.configureContents(menuCellContents: menuCellContents)
       
        return menuDetailsTableViewCell ?? UITableViewCell()
    }
    
    func createMenuDetailsTableView(){
        self.addSubview(menuDetailsTableView)
        NSLayoutConstraint.activate(getMenuDetailsTableViewLayoutConstraints())

    }
    

    
    private func getMenuDetailsTableViewLayoutConstraints() -> [NSLayoutConstraint]{
            let tableViewContraints = [menuDetailsTableView.topAnchor.constraint(equalTo: userPhoneNumberLabel.bottomAnchor,constant: 30),
                                       menuDetailsTableView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
                                       menuDetailsTableView.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),]
            return tableViewContraints
    }
    
    func updateMenuDetailsTableViewHeightLayoutConstraints(numberOfRows : Int){
        menuDetailsTableView.heightAnchor.constraint(equalToConstant: CGFloat(numberOfRows) * getOrderMenuCellHeight()).isActive = true
    }
    
    func createInstructionToRestaurantTextView() {
        
        self.addSubview(instructionsToRestaurant)
        NSLayoutConstraint.activate(getInstructionsToRestaurantTextViewLayoutContrainst())
    }
    
    func updateInstructionToRestaurantTextView(instructionsToRestaurantText : String){
        instructionsToRestaurant.text = instructionsToRestaurantText
    }
    
    private func getInstructionsToRestaurantTextViewLayoutContrainst() -> [NSLayoutConstraint]{
        let textViewConstraints = [instructionsToRestaurant.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),instructionsToRestaurant.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),instructionsToRestaurant.topAnchor.constraint(equalTo: menuDetailsTableView.bottomAnchor,constant: 15),instructionsToRestaurant.heightAnchor.constraint(equalToConstant: 80)]
        return textViewConstraints
    }
    
    
    
    
    
    
    private func getHorizontalStackView() -> UIStackView{
        let horizontalStackView = UIStackView()
        horizontalStackView.axis = .horizontal
        horizontalStackView.distribution = .fillProportionally
        return horizontalStackView
    }
    
    func createBillDetailsElements(){
        createBillView()
        createBillStackView()
        applyBillDetailsLabelConstrainst()
        applyItemTotalLabelsConstrainst()
        applyDeliveryFeeLabelsConstrainst()
        applyRestaurantGSTLabelsConstrainst()
        applyRestaurantPackageingChargesLabelsConstrainst()
        applyItemDiscountLabelsConstrainst()
        billstackView.addArrangedSubview(createLineView(color: .systemGray5))
        applyToPayLabelsConstrainst()
        
    }
    
    private func createBillView(){
        
        self.addSubview(billView)
        NSLayoutConstraint.activate(getBillViewLayOutConstraints())
        
    }
    
    private func  getBillViewLayOutConstraints() -> [NSLayoutConstraint]{
        let viewConstraints = [billView.topAnchor.constraint(equalTo: instructionsToRestaurant.bottomAnchor,constant: 20),
                               billView.leftAnchor.constraint(equalTo: self.leftAnchor),
                               billView.rightAnchor.constraint(equalTo: self.rightAnchor),
                               billView.heightAnchor.constraint(equalToConstant: 200)]
        return viewConstraints
    }
    
    
    func updateBillDetails(itemTotalPrice : Int,restaurantPackagingChargePrice : Int, deliveryFeePrice : Int , totalAmount : Int,discountAmount : Int , restaurantGST : Double){
        updateItemDiscount(itemDiscountPrice : discountAmount)
        updateBillDetialsLabels(itemTotalPrice: itemTotalPrice, restaurantGSTPrice: restaurantGST, totalAmountpaid: totalAmount)
        setDeliveryAndPackageingFee(deliveryFeePrice: deliveryFeePrice, restaurantPackagingChargePrice: restaurantPackagingChargePrice)
    }
    
   
    private func updateItemDiscount(itemDiscountPrice :Int){
        if(itemDiscountPrice != 0){
            itemDiscountLabelsStackView.isHidden = false
            itemDiscount.text = "- ₹ \(itemDiscountPrice)"
        }
        else{
            itemDiscount.text = "- ₹ \(itemDiscountPrice)"
            itemDiscountLabelsStackView.isHidden = true
        }
    }
    
   
    
    private func updateBillDetialsLabels(itemTotalPrice : Int,restaurantGSTPrice : Double,totalAmountpaid : Int){
        itemTotal.text = "₹ \(itemTotalPrice)"
        restaurantGST.text =  "₹ \(restaurantGSTPrice)"
        totalAmount.text = "₹ \(totalAmountpaid)"
    }
    
    private func setDeliveryAndPackageingFee(deliveryFeePrice : Int,restaurantPackagingChargePrice : Int){
        deliveryFee.text = "₹ \(deliveryFeePrice)"
        restaurantPackagingCharges.text = "₹ \(restaurantPackagingChargePrice)"
    }
    
    private func createBillStackView(){
        
        billView.addSubview(billstackView)
        NSLayoutConstraint.activate(getBillStackViewLayOutConstraints())
    }
    
    private func getBillStackViewLayOutConstraints() -> [NSLayoutConstraint]{
        let stackConstraint = [billstackView.topAnchor.constraint(equalTo: billView.topAnchor,constant: 20),
                               billstackView.leftAnchor.constraint(equalTo: billView.leftAnchor,constant: 10),
                               billstackView.rightAnchor.constraint(equalTo: billView.rightAnchor,constant: -10),
                            
        ]
        return stackConstraint
    }
    
   
    private func applyBillDetailsLabelConstrainst(){
        billDetailsLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        billstackView.addArrangedSubview(billDetailsLabel)
       
    }
    
    private func applyItemTotalLabelsConstrainst(){
       
        billstackView.addArrangedSubview(itemTotalLabelsStackView)
        itemTotal.widthAnchor.constraint(equalToConstant: 80).isActive = true
        itemTotal.heightAnchor.constraint(equalToConstant: 20).isActive = true
        itemTotalLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
    }
    
    private func applyDeliveryFeeLabelsConstrainst(){
        
        billstackView.addArrangedSubview(deliveryFeeLabelsStackView)
        deliveryFee.widthAnchor.constraint(equalToConstant: 80).isActive = true
        deliveryFee.heightAnchor.constraint(equalToConstant: 20).isActive = true
        deliveryFeeLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func applyRestaurantGSTLabelsConstrainst(){
        
        billstackView.addArrangedSubview(restaurantGSTLabelsStackView)
        restaurantGST.widthAnchor.constraint(equalToConstant: 80).isActive = true
        restaurantGST.heightAnchor.constraint(equalToConstant: 20).isActive = true
        restaurantGSTLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func applyRestaurantPackageingChargesLabelsConstrainst(){
        
        billstackView.addArrangedSubview(restaurantPackagingChargesLabelsStackView)
        restaurantPackagingCharges.widthAnchor.constraint(equalToConstant: 80).isActive = true
        restaurantPackagingCharges.heightAnchor.constraint(equalToConstant: 20).isActive = true
        restaurantPackagingChargesLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func applyItemDiscountLabelsConstrainst(){
        
        billstackView.addArrangedSubview(itemDiscountLabelsStackView)
        itemDiscount.widthAnchor.constraint(equalToConstant: 90).isActive = true
        itemDiscount.heightAnchor.constraint(equalToConstant: 20).isActive = true
        itemDiscountLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
    }
    
    
    
    private func applyToPayLabelsConstrainst(){
        
        billstackView.addArrangedSubview(totalAmountLabelsStackView)
        totalAmount.widthAnchor.constraint(equalToConstant: 80).isActive = true
        totalAmount.heightAnchor.constraint(equalToConstant: 20).isActive = true
        totalAmountLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    private func createLineView(color : UIColor) -> UIView{
        let lineView = UIView()
        lineView.translatesAutoresizingMaskIntoConstraints = false
        lineView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        lineView.backgroundColor = color
        return lineView
    }
    
    
    private func createOrderBottomView(){
        
        self.addSubview(orderBottomView)
        
    }
    
    private func getOrderBottomViewLayoutConstraints() -> [NSLayoutConstraint]{
        let viewContraints = [
            orderBottomView.bottomAnchor.constraint(equalTo: UIApplication.shared.windows.first?.safeAreaLayoutGuide.bottomAnchor ?? self.safeAreaLayoutGuide.bottomAnchor),
            orderBottomView.leftAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leftAnchor),
            orderBottomView.rightAnchor.constraint(equalTo: self.safeAreaLayoutGuide.rightAnchor),
            orderBottomView.heightAnchor.constraint(equalToConstant: 50)]
        return viewContraints
    }
    
    private func createupdateOrderStatusButtonButton(){
        
        orderBottomView.addSubview(updateOrderStatusButton)
        NSLayoutConstraint.activate(getUpdateOrderStatusButtonLayoutConstraints())
        
    }
    
    private func getUpdateOrderStatusButtonLayoutConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [
            updateOrderStatusButton.topAnchor.constraint(equalTo: orderBottomView.topAnchor,constant: 5),
            updateOrderStatusButton.leftAnchor.constraint(equalTo: orderBottomView.centerXAnchor,constant: 5),
            updateOrderStatusButton.rightAnchor.constraint(equalTo: orderBottomView.rightAnchor,constant: -10),
            updateOrderStatusButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
   
    func createDeclineOrderButton() {
       
        orderBottomView.addSubview(declineOrderButton)
        NSLayoutConstraint.activate(getDeclineOrderButtonLayoutConstraints())
      
    }
    
    private func getDeclineOrderButtonLayoutConstraints() -> [NSLayoutConstraint]{
        let buttonContraints = [
            declineOrderButton.topAnchor.constraint(equalTo: orderBottomView.topAnchor,constant: 5),
            declineOrderButton.leftAnchor.constraint(equalTo: orderBottomView.leftAnchor,constant: 10),
            declineOrderButton.rightAnchor.constraint(equalTo: orderBottomView.centerXAnchor,constant: -5),
            declineOrderButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    
    
  
    
    func activateContentViewBottomAnchorWithRespectToBillView(){
        if(billView.frame.maxY > UIScreen.main.bounds.maxY){
            self.removeHeightAnchorConstraints()
            self.heightAnchor.constraint(equalToConstant:  billView.frame.maxY + 100).isActive = true
        }
        else{
            self.removeHeightAnchorConstraints()
            self.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.maxY).isActive = true
        }

    }
   
    @objc func didTapUpdateOrderStatusButton(){
        delegate?.didTapUpdateOrderStatusButton()
    }
    
    @objc func didTapDeclineOrder(){
        delegate?.didTapDeclineOrder()
    }
}

protocol OrderDetailsPageViewDelegate  :AnyObject{
    func didTapUpdateOrderStatusButton()
    func didTapDeclineOrder()
}
