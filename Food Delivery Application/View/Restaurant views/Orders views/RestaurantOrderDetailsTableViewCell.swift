//
//  OrdersTableView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 17/03/22.
//


import UIKit



class RestaurantOrderDetailsTableViewCell  : UITableViewCell{
   
    static let cellIdentifier = "orderDetailsCell"
    
    var cellHeight : CGFloat = 44
    
    private lazy var orderIdLabel : UILabel = {
        let  orderIdLabel = UILabel()
        orderIdLabel.backgroundColor = .white
        orderIdLabel.translatesAutoresizingMaskIntoConstraints = false
        orderIdLabel.textColor = .black.withAlphaComponent(0.7)
        orderIdLabel.textAlignment = .left
        orderIdLabel.adjustsFontSizeToFitWidth = true
        orderIdLabel.font = UIFont(name:"ArialRoundedMTBold", size: 20.0)
        return orderIdLabel
    }()
    
    private lazy var menuNameAndQuantityLabel : UILabel = {
        let menuNameAndQuantityLabel = UILabel()
        menuNameAndQuantityLabel.backgroundColor = .white
        menuNameAndQuantityLabel.translatesAutoresizingMaskIntoConstraints = false
        menuNameAndQuantityLabel.textColor =  .black
        menuNameAndQuantityLabel.textAlignment = .left
        return menuNameAndQuantityLabel
        
    }()
    
    private lazy var orderTotalAmountLabel : UILabel = {
        let orderTotalAmountLabel = UILabel()
        orderTotalAmountLabel.translatesAutoresizingMaskIntoConstraints = false
        orderTotalAmountLabel.backgroundColor = .white
        orderTotalAmountLabel.textColor = .black.withAlphaComponent(0.6)
        orderTotalAmountLabel.textAlignment = .left
        orderTotalAmountLabel.adjustsFontSizeToFitWidth = true
        orderTotalAmountLabel.font = UIFont(name: "ArialRoundedMTBold", size: 25)
        return orderTotalAmountLabel
    }()
    
    private lazy var dateAndTimeLabel : UILabel = {
        let dateAndTimeLabel = UILabel()
        dateAndTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        dateAndTimeLabel.backgroundColor = .white
        dateAndTimeLabel.textColor = .black.withAlphaComponent(0.6)
        dateAndTimeLabel.textAlignment = .left
        dateAndTimeLabel.adjustsFontSizeToFitWidth = true
        dateAndTimeLabel.font = UIFont(name: "ArialRoundedMTBold", size: 20)
        return dateAndTimeLabel
    }()
    
    private lazy var orderStatusLabel : UILabel = {
        let orderStatusLabel = UILabel()
        orderStatusLabel.backgroundColor = .white
        orderStatusLabel.translatesAutoresizingMaskIntoConstraints = false
        orderStatusLabel.layer.cornerRadius  = 10
        orderStatusLabel.font = UIFont.systemFont(ofSize: 17,weight: .bold)
        orderStatusLabel.textAlignment = .center
        orderStatusLabel.backgroundColor = .white
        return orderStatusLabel
    }()
    
    private lazy var orderDetailsTableViewCellContentView : UIView = {
       let orderDetailsTableViewCellContentView = UIView()
        orderDetailsTableViewCellContentView.backgroundColor = .white
        orderDetailsTableViewCellContentView.addBorder(side: .bottom, color: .black.withAlphaComponent(0.9), width: 1)
        return orderDetailsTableViewCellContentView
    }()
    
   
    
    
    private func updateOrderStaus(orderStatus : OrderStatus) {
        
        switch orderStatus {
        case .Pending:
            orderStatusLabel.text = "Pending"
            orderStatusLabel.textColor = .systemRed
            
        case .Preparing:
            orderStatusLabel.text = "Preparing"
            orderStatusLabel.textColor = .systemGreen
           
        case .Ready:
            orderStatusLabel.text = "Ready"
            orderStatusLabel.textColor = .systemGreen
           
        case .Delivered:
            orderStatusLabel.text = "Delivered"
            orderStatusLabel.textColor = .systemGreen
           
        case .Cancelled:
            orderStatusLabel.text = "Cancelled"
            orderStatusLabel.textColor = .systemRed
        case .Declined :
            orderStatusLabel.text = "Declined"
            orderStatusLabel.textColor = .systemRed
            
        }
    }
    
    
        private func getOrderStatusLabelConstraints() -> [NSLayoutConstraint]{
            let labelContraints = [
                orderStatusLabel.topAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.topAnchor,constant: 5),
                orderStatusLabel.rightAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.rightAnchor,constant: -10),
                orderStatusLabel.widthAnchor.constraint(equalToConstant: 150),
                orderStatusLabel.heightAnchor.constraint(equalToConstant:30)]
            return labelContraints
        }
        
        
    private func getOrderIdLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            orderIdLabel.topAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.topAnchor,constant: 5),
            orderIdLabel.rightAnchor.constraint(equalTo: orderStatusLabel.leftAnchor,constant: -10),
            orderIdLabel.leftAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.leftAnchor),
            orderIdLabel.heightAnchor.constraint(equalToConstant:30)]
        return labelContraints
    }

    private func getMenuNameAndQuantityLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            menuNameAndQuantityLabel.topAnchor.constraint(equalTo: orderIdLabel.bottomAnchor,constant: 10),
            menuNameAndQuantityLabel.rightAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.rightAnchor),
            menuNameAndQuantityLabel.leftAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.leftAnchor),
            menuNameAndQuantityLabel.heightAnchor.constraint(equalToConstant:50)]
        return labelContraints
    }

    private func getOrderTotalAmountLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            orderTotalAmountLabel.topAnchor.constraint(equalTo: menuNameAndQuantityLabel.bottomAnchor,constant: 5),
            orderTotalAmountLabel.leftAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.leftAnchor),
            orderTotalAmountLabel.rightAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.centerXAnchor),
            orderTotalAmountLabel.heightAnchor.constraint(equalToConstant:30)]
        return labelContraints
    }
    
    private func getDateAndTimeLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            dateAndTimeLabel.topAnchor.constraint(equalTo: menuNameAndQuantityLabel.bottomAnchor,constant: 5),
            dateAndTimeLabel.leftAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.centerXAnchor),
            dateAndTimeLabel.rightAnchor.constraint(equalTo: orderDetailsTableViewCellContentView.rightAnchor, constant: -10),
            dateAndTimeLabel.heightAnchor.constraint(equalToConstant:30)]
        return labelContraints
    }

    
    private func initialiseViewElements(){
        orderDetailsTableViewCellContentView.addSubview(orderIdLabel)
        orderDetailsTableViewCellContentView.addSubview(menuNameAndQuantityLabel)
        orderDetailsTableViewCellContentView.addSubview(orderTotalAmountLabel)
        orderDetailsTableViewCellContentView.addSubview(dateAndTimeLabel)
        orderDetailsTableViewCellContentView.addSubview(orderStatusLabel)
        NSLayoutConstraint.activate(getOrderStatusLabelConstraints())
        NSLayoutConstraint.activate(getOrderIdLabelConstraints())
        NSLayoutConstraint.activate(getMenuNameAndQuantityLabelConstraints())
        NSLayoutConstraint.activate(getOrderTotalAmountLabelConstraints())
        NSLayoutConstraint.activate(getDateAndTimeLabelConstraints())
    }
   
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(orderDetailsTableViewCellContentView)
        initialiseViewElements()
    }

    func configureContents(orderCellContents : (orderId : String , orderStatus : OrderStatus,orderTotal : Int , menuNameAndQuantities : [(menuName : String, quantity : Int)],dateAndTime : String)){
        orderIdLabel.text = "Order Id : \(orderCellContents.orderId[..<orderCellContents.orderId.index(orderCellContents.orderId.startIndex, offsetBy: 5)])"
        var menuNameAndQuantityString = ""
        for menuNameAndQuantity in orderCellContents.menuNameAndQuantities {
            menuNameAndQuantityString += "\(menuNameAndQuantity.menuName) X \(menuNameAndQuantity.quantity) "
        }
        menuNameAndQuantityLabel.text = menuNameAndQuantityString
        orderTotalAmountLabel.text = "₹\(orderCellContents.orderTotal)"
        dateAndTimeLabel.text = orderCellContents.dateAndTime
        updateOrderStaus(orderStatus: orderCellContents.orderStatus)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        orderDetailsTableViewCellContentView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: cellHeight)
        
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        menuNameAndQuantityLabel.text = nil
        orderTotalAmountLabel.text = nil
        dateAndTimeLabel.text = nil
        orderStatusLabel.text = nil
        orderStatusLabel.textColor = nil
        orderIdLabel.text = nil
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}





