//
//  OrdersPageView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 14/03/22.
//

import Foundation
import UIKit

class OrderPageView : UIView, OrdersPageViewProtocol{
    
    weak var delegate : OrderPageViewDelegate?
    
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        initialiseViewElements()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialiseViewElements(){
        self.addSubview(waitingForOrdersLabel)
        self.addSubview(refreshPageButton)
        self.addSubview(orderDetailsTableView)
        NSLayoutConstraint.activate(getOrderDetailsTableViewLayoutConstraints())
        NSLayoutConstraint.activate(getRefreshPageButtonContraints())
        NSLayoutConstraint.activate(getWaitingForOrdersLabelConstraints())
    }
    var waitingForOrdersLabel : UILabel = {
        let  waitingForOrdersLabel = UILabel()
        waitingForOrdersLabel.text = " Waiting  for  Orders As of now you don't have any orders "
        waitingForOrdersLabel.textColor = .systemGray
        waitingForOrdersLabel.font = UIFont.systemFont(ofSize: 25, weight: .medium)
        waitingForOrdersLabel.lineBreakMode = .byWordWrapping
        waitingForOrdersLabel.numberOfLines = 2
        waitingForOrdersLabel.textAlignment = .center
        waitingForOrdersLabel.translatesAutoresizingMaskIntoConstraints = false
        waitingForOrdersLabel.sizeToFit()
       
        return waitingForOrdersLabel
    }()
    
    lazy var refreshPageButton : UIButton = {
        let  refreshPageButton = UIButton()
        refreshPageButton.setTitle("Refresh", for: .normal)
        refreshPageButton.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        refreshPageButton.setTitleColor(.white, for: .normal)
        refreshPageButton.backgroundColor = .systemGreen
        refreshPageButton.translatesAutoresizingMaskIntoConstraints = false
        refreshPageButton.layer.cornerRadius = 5
        refreshPageButton.contentMode = .left
        refreshPageButton.addTarget(self, action: #selector(didTapRefreshButton), for: .touchUpInside)

        return refreshPageButton
    }()
    
    
    
    var orderDetailsTableView : UITableView = {
        let orderDetailsTableView = UITableView()
        orderDetailsTableView.backgroundColor = .white
        orderDetailsTableView.isScrollEnabled = true
        orderDetailsTableView.bounces = false
        orderDetailsTableView.register(RestaurantOrderDetailsTableViewCell.self, forCellReuseIdentifier: RestaurantOrderDetailsTableViewCell.cellIdentifier)
        orderDetailsTableView.translatesAutoresizingMaskIntoConstraints = false
        orderDetailsTableView.separatorStyle = .none
        return orderDetailsTableView
    }()
    
    func getOrderTabelViewCellHeight() -> CGFloat{
        return 150
    }
    
    func createOrdersTableViewCell(indexPath : IndexPath ,tableView : UITableView, orderCellContents : (orderId : String , orderStatus : OrderStatus,orderTotal : Int , menuNameAndQuantities : [(menuName : String, quantity : Int)],dateAndTime : String)) -> UITableViewCell{
        let orderDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: RestaurantOrderDetailsTableViewCell.cellIdentifier, for: indexPath) as? RestaurantOrderDetailsTableViewCell
        orderDetailsTableViewCell?.cellHeight = 150
        orderDetailsTableViewCell?.configureContents(orderCellContents: orderCellContents)
        return orderDetailsTableViewCell ?? UITableViewCell()
    }
   
    private func getOrderDetailsTableViewLayoutConstraints() -> [NSLayoutConstraint]{
        let tableViewConstraints = [
            orderDetailsTableView.topAnchor.constraint(equalTo: self.topAnchor),
            orderDetailsTableView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10),
            orderDetailsTableView.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -10),
            orderDetailsTableView.bottomAnchor.constraint(equalTo: self.bottomAnchor)]
        return tableViewConstraints
    }
    

    private func getWaitingForOrdersLabelConstraints() -> [NSLayoutConstraint]{
        let labelConstraints = [waitingForOrdersLabel.centerYAnchor.constraint(equalTo:self.centerYAnchor, constant: -100),
                                waitingForOrdersLabel.leftAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leftAnchor),
                                waitingForOrdersLabel.rightAnchor.constraint(equalTo: self.safeAreaLayoutGuide.rightAnchor)]
        return labelConstraints
    }
    
   
       
    
    private func getRefreshPageButtonContraints() -> [NSLayoutConstraint]{
        let buttonContraints = [refreshPageButton.topAnchor.constraint(equalTo: waitingForOrdersLabel.bottomAnchor,constant: 10),
                                refreshPageButton.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 40),
                                refreshPageButton.rightAnchor.constraint(equalTo: self.rightAnchor , constant: -40),
                                refreshPageButton.heightAnchor.constraint(equalToConstant: 40)]
        return buttonContraints
    }
    
    @objc func didTapRefreshButton(){
        delegate?.didTapRefreshButton()
    }
    
}

enum OrderStatus : String {
    case Pending
    case Preparing
    case Ready
    case Delivered
    case Cancelled
    case Declined
}


protocol OrderPageViewDelegate : AnyObject{
     func didTapRefreshButton()
}
