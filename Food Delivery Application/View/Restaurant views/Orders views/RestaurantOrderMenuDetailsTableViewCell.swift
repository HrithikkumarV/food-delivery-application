//
//  OrderMenuDetailsTableView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 16/03/22.
//

import Foundation
import UIKit



class RestaurantOrderMenuDetailsTableViewCell  : UITableViewCell{
   
    static let cellIdentifier = "RestaurantOrderMenuDetailsTableViewCell"
    
    var cellHeight : CGFloat = 44
    
    lazy var menuDetailsContentView : UIView = {
        let  menuDetailsContentView = UIView()
        menuDetailsContentView.backgroundColor = .white
        return menuDetailsContentView
    }()
    
    lazy var menuNameCellLabel : UILabel = {
        let menuNameCellLabel = UILabel()
        menuNameCellLabel.backgroundColor = .white
        menuNameCellLabel.translatesAutoresizingMaskIntoConstraints = false
        menuNameCellLabel.textColor = .black
       
        menuNameCellLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        menuNameCellLabel.textAlignment = .left
        menuNameCellLabel.numberOfLines = 2
        menuNameCellLabel.contentMode = .center
        menuNameCellLabel.lineBreakMode = .byWordWrapping
        menuNameCellLabel.sizeToFit()
        return menuNameCellLabel
    }()
    
    lazy var menuSubTotalPriceCellLabel : UILabel = {
        let  menuSubTotalPriceCellLabel = UILabel()
        menuSubTotalPriceCellLabel.backgroundColor = .white
        menuSubTotalPriceCellLabel.translatesAutoresizingMaskIntoConstraints = false
        
        menuSubTotalPriceCellLabel.textColor = .black.withAlphaComponent(0.9)
        menuSubTotalPriceCellLabel.adjustsFontSizeToFitWidth = true
        menuSubTotalPriceCellLabel.textAlignment = .left
        menuSubTotalPriceCellLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        return menuSubTotalPriceCellLabel
    }()
    
    lazy var menuTarianTypeCellImageView : UIImageView = {
        let  menuTarianTypeCellImageView = UIImageView()
        
        menuTarianTypeCellImageView.translatesAutoresizingMaskIntoConstraints = false
        return menuTarianTypeCellImageView
    }()
    
    lazy var menuCountLabel : UILabel = {
        let  menuCountLabel = UILabel()
        menuCountLabel.backgroundColor = .white
        menuCountLabel.translatesAutoresizingMaskIntoConstraints = false
        
        menuCountLabel.textColor = .black.withAlphaComponent(0.9)
        menuCountLabel.textAlignment = .center
        menuCountLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        return menuCountLabel
    }()
    
    lazy var menuIsAvailableLabel : UILabel = {
        let menuIsAvailableLabel = UILabel()
        menuIsAvailableLabel.translatesAutoresizingMaskIntoConstraints = false
        menuIsAvailableLabel.backgroundColor = .white
        menuIsAvailableLabel.layer.cornerRadius = 5
        menuIsAvailableLabel.font = UIFont(name:"ArialRoundedMTBold", size: 16.0)
        menuIsAvailableLabel.adjustsFontSizeToFitWidth = true
        return menuIsAvailableLabel
    }()
    
    

    private func modifyMenuTarianTypeSymbolColor(menuTarianType : String){
        if(menuTarianType == menuTarianTypeEnum.veg.rawValue ){
            menuTarianTypeCellImageView.image = UIImage(systemName: "dot.square")?.withTintColor(.systemGreen, renderingMode: .alwaysOriginal)
        }
        else if(menuTarianType == menuTarianTypeEnum.nonVeg.rawValue){
            menuTarianTypeCellImageView.image = UIImage(systemName: "arrowtriangle.up.square")?.withTintColor(.red , renderingMode: .alwaysOriginal)
        }
        else{
            menuTarianTypeCellImageView.image = UIImage(systemName: "arrowtriangle.up.square")?.withTintColor(.systemYellow , renderingMode: .alwaysOriginal)
        }
    }

   
    
    private func modifyMenuBasedOnTheAvailability(menuAvailable : Int){
        if(menuAvailable == 0){
            menuIsAvailableLabel.text = "Unavailable"
            
            menuIsAvailableLabel.textColor = .systemRed
        }
        else{
            menuIsAvailableLabel.text = "Available"
            
            menuIsAvailableLabel.textColor = .systemGreen
        }
    }
    
    private func getMenuTarianTypeImageViewConstraint() -> [NSLayoutConstraint]{
        let imageViewContraints = [menuTarianTypeCellImageView.topAnchor.constraint(equalTo: menuDetailsContentView.topAnchor,constant: 20),
                                   menuTarianTypeCellImageView.leftAnchor.constraint(equalTo: menuDetailsContentView.leftAnchor,constant: 10),
                                   menuTarianTypeCellImageView.widthAnchor.constraint(equalToConstant: 20),
                                   menuTarianTypeCellImageView.heightAnchor.constraint(equalToConstant: 20)]
        return imageViewContraints
    }

    private func getMenuNameCellLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            menuNameCellLabel.topAnchor.constraint(equalTo: menuDetailsContentView.topAnchor),
            menuNameCellLabel.rightAnchor.constraint(equalTo: menuCountLabel.leftAnchor,constant: -5),
            menuNameCellLabel.leftAnchor.constraint(equalTo: menuTarianTypeCellImageView.rightAnchor, constant: 5),
            menuNameCellLabel.heightAnchor.constraint(equalToConstant:60)]
        return labelContraints
    }

    private func getMenuSubTotalPriceCellLabelConstraints() -> [NSLayoutConstraint]{
        let labelContraints = [
            menuSubTotalPriceCellLabel.topAnchor.constraint(equalTo: menuDetailsContentView.topAnchor , constant: 15),
            menuSubTotalPriceCellLabel.widthAnchor.constraint(equalToConstant: 80),
            menuSubTotalPriceCellLabel.rightAnchor.constraint(equalTo: menuIsAvailableLabel.leftAnchor, constant: -5),
            menuSubTotalPriceCellLabel.heightAnchor.constraint(equalToConstant:30)]
        return labelContraints
    }

   


    
    
    private func getMenuCountLabelConstraints() -> [NSLayoutConstraint]{
        let constraints = [menuCountLabel.topAnchor.constraint(equalTo: menuDetailsContentView.topAnchor,constant: 15),menuCountLabel.widthAnchor.constraint(equalToConstant: 50),menuCountLabel.rightAnchor.constraint(equalTo: menuSubTotalPriceCellLabel.leftAnchor,constant: -10),
                           menuCountLabel.heightAnchor.constraint(equalToConstant: 30)]
        return constraints
    }
    
    
    
    
    private func getMenuIsAvailableCellLabelConstraint() -> [NSLayoutConstraint]{
        let constraints = [menuIsAvailableLabel.topAnchor.constraint(equalTo: menuDetailsContentView.topAnchor,constant: 15),menuIsAvailableLabel.widthAnchor.constraint(equalToConstant: 70),menuIsAvailableLabel.rightAnchor.constraint(equalTo: menuDetailsContentView.rightAnchor,constant: -10),
                           menuIsAvailableLabel.heightAnchor.constraint(equalToConstant: 30)]
                               return constraints
    }
    
    private func initilaseViewElements(){
        menuDetailsContentView.addSubview(menuNameCellLabel)
        menuDetailsContentView.addSubview(menuSubTotalPriceCellLabel)
        menuDetailsContentView.addSubview(menuTarianTypeCellImageView)
        menuDetailsContentView.addSubview(menuCountLabel)
        menuDetailsContentView.addSubview(menuIsAvailableLabel)
        NSLayoutConstraint.activate(getMenuTarianTypeImageViewConstraint())
        NSLayoutConstraint.activate(getMenuNameCellLabelConstraints())
        NSLayoutConstraint.activate(getMenuSubTotalPriceCellLabelConstraints())
        NSLayoutConstraint.activate(getMenuCountLabelConstraints())
        NSLayoutConstraint.activate(getMenuIsAvailableCellLabelConstraint())
    }
   
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(menuDetailsContentView)
        initilaseViewElements()
    }

    func configureContents(menuCellContents : ( menuName : String, menuTarianType : String, menuPriceSubTotal : Int,  menuAvailable : Int, quantity : Int)){
        menuNameCellLabel.text = menuCellContents.menuName
        menuSubTotalPriceCellLabel.text = "₹\(menuCellContents.menuPriceSubTotal)"
        modifyMenuTarianTypeSymbolColor(menuTarianType: menuCellContents.menuTarianType)
        menuCountLabel.text = "X \(menuCellContents.quantity)"
        modifyMenuBasedOnTheAvailability(menuAvailable: menuCellContents.menuAvailable)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        menuDetailsContentView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: cellHeight)
       
        
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        menuNameCellLabel.text = nil
        menuSubTotalPriceCellLabel.text = nil
        menuTarianTypeCellImageView.image = nil
        menuCountLabel.text = nil
        menuIsAvailableLabel.text = nil
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}





