//
//  UserCreateAccountOTPFormView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 21/12/21.
//

import Foundation


import UIKit
class OTPFormView : UIView,OTPFormViewProtocol {

    
    
    weak var delegate : OTPFormViewDelegate?
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    init(){
        super.init(frame: .zero)
        self.backgroundColor = .white
        
        initialiseViewElements()
    }
    
    private func initialiseViewElements(){
        self.addSubview(verificationExplanationLabel)
        self.addSubview(clickHereToChangePhoneNumberLabel)
        self.addSubview(OTPTextField)
        self.addSubview(continueToVerifyButton)
        self.addSubview(resendOTPLabel)
        NSLayoutConstraint.activate(getVerficationAuthenticationLabelContraints())
        NSLayoutConstraint.activate(getOTPTextFieldContarints())
        NSLayoutConstraint.activate(getContinueToVerifyButtonContraints())
        NSLayoutConstraint.activate(getClickHereToChangePhoneNumberLabelContraints())
        NSLayoutConstraint.activate(getResendOTPLabelContraints())
    }
    
    
    private lazy var resendOTPLabel : UILabel = {
        let resendOTPLabel = UILabel()
        resendOTPLabel.text = "Resend OTP"
        resendOTPLabel.textColor = .systemBlue
        resendOTPLabel.textAlignment = .center
        resendOTPLabel.isUserInteractionEnabled = true
        resendOTPLabel.translatesAutoresizingMaskIntoConstraints = false
        resendOTPLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapResendOTP)))
        return resendOTPLabel
    }()
    
    private lazy var clickHereToChangePhoneNumberLabel : UILabel = {
        let clickHereToChangePhoneNumberLabel =  UILabel()
        clickHereToChangePhoneNumberLabel.textColor = .systemBlue
        clickHereToChangePhoneNumberLabel.textAlignment = .left
        clickHereToChangePhoneNumberLabel.isUserInteractionEnabled = true
        clickHereToChangePhoneNumberLabel.translatesAutoresizingMaskIntoConstraints = false
        clickHereToChangePhoneNumberLabel.text = "Click here to change phone number"
        clickHereToChangePhoneNumberLabel.numberOfLines = 2
        clickHereToChangePhoneNumberLabel.lineBreakMode = .byWordWrapping
        clickHereToChangePhoneNumberLabel.isUserInteractionEnabled = true
        clickHereToChangePhoneNumberLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(didTapChangePhoneNumber)))
        return clickHereToChangePhoneNumberLabel
    }()
    
    private lazy var verificationExplanationLabel : UILabel = {
        let verificationExplanationLabel = UILabel()
        verificationExplanationLabel.textColor = .black
        verificationExplanationLabel.textAlignment = .left
        verificationExplanationLabel.translatesAutoresizingMaskIntoConstraints = false
        verificationExplanationLabel.lineBreakMode = .byWordWrapping
        verificationExplanationLabel.numberOfLines = 4
        verificationExplanationLabel.sizeToFit()
        return verificationExplanationLabel
    }()
    
    lazy var OTPTextField : UITextField = {
            let  OTPTextField = CustomTextField()
            OTPTextField .backgroundColor = .white
            OTPTextField .textColor = .black
            OTPTextField .translatesAutoresizingMaskIntoConstraints = false
            OTPTextField.placeholder = "OTP"
            OTPTextField.addLabelToTopBorder(labelText: " OTP ", fontSize: 15)
            OTPTextField.keyboardType = .numberPad
            let flexibleSpaceForToolBar = ToolBarUtils().createFlexibleSpaceForToolBar()
        let OTPkeyboardToolBar = ToolBarUtils().createKeyboardToolBar()
        let OTPKeyboardDoneBarButton = ToolBarUtils().createBarButton(title: "Done")
        OTPKeyboardDoneBarButton.target = self
        OTPKeyboardDoneBarButton.action = #selector(didTapOTPkeyboardDoneBarButton)
        OTPkeyboardToolBar.items = [flexibleSpaceForToolBar,OTPKeyboardDoneBarButton]
        OTPTextField.inputAccessoryView = OTPkeyboardToolBar
            return OTPTextField
    }()
    
    private lazy var continueToVerifyButton : UIButton = {
        let continueToVerifyButton = UIButton()
        continueToVerifyButton.translatesAutoresizingMaskIntoConstraints = false
        continueToVerifyButton.setTitle("Continue", for: .normal)
        continueToVerifyButton.setTitleColor(.white, for: .normal)
        continueToVerifyButton.backgroundColor = .systemBlue
        continueToVerifyButton.layer.cornerRadius = 5
        continueToVerifyButton.titleLabel?.adjustsFontSizeToFitWidth = true
        continueToVerifyButton.addTarget(self, action: #selector(didTapContinueToVerify), for: .touchUpInside)
        return continueToVerifyButton
    }()
    
    
    
    func getMaxYOfLastObject() -> CGFloat{
        self.layoutIfNeeded()
        return resendOTPLabel.frame.maxY
        
    }

    
    func updatePhonenumberInVerificationExplanationLabel(phoneNumber : String){
        verificationExplanationLabel.text = "We,ve sent an One Time Password (OTP) to the \(phoneNumber). please enter it to complete verification."
    }
    
    func getContinueToVerifyButtonContraints() -> [NSLayoutConstraint]{
            let buttonContraints = [ continueToVerifyButton.topAnchor.constraint(equalTo: OTPTextField.bottomAnchor, constant: 10),
                                     continueToVerifyButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 40),
                                     continueToVerifyButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -40),
                                     continueToVerifyButton.heightAnchor.constraint(equalToConstant: 50)]
            return buttonContraints
        }
        
       
        func getVerficationAuthenticationLabelContraints() -> [NSLayoutConstraint]{
            let labelContraints = [verificationExplanationLabel.topAnchor.constraint(equalTo: self.topAnchor,constant: 20),
                                   verificationExplanationLabel.leftAnchor.constraint(equalTo: self.leftAnchor ,constant: 40),
                                   verificationExplanationLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -40),
                                   ]
            return labelContraints
        }
        
        func getClickHereToChangePhoneNumberLabelContraints() -> [NSLayoutConstraint]{
            let labelContraints = [clickHereToChangePhoneNumberLabel.topAnchor.constraint(equalTo: verificationExplanationLabel.bottomAnchor),
                                   clickHereToChangePhoneNumberLabel.leftAnchor.constraint(equalTo: self.leftAnchor ,constant: 40),
                                   clickHereToChangePhoneNumberLabel.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -40),
                                   clickHereToChangePhoneNumberLabel.heightAnchor.constraint(equalToConstant : 50)]
            return labelContraints
        }
        
        
       
        func getOTPTextFieldContarints() -> [NSLayoutConstraint]{
            let textFieldContraints = [OTPTextField.topAnchor.constraint(equalTo: clickHereToChangePhoneNumberLabel.bottomAnchor , constant: 10),
                                       OTPTextField.leftAnchor.constraint(equalTo: self.leftAnchor ,constant: 40),
                                       OTPTextField.rightAnchor.constraint(equalTo: self.rightAnchor,constant: -40),
                                       OTPTextField.heightAnchor.constraint(equalToConstant : 50)]
            return textFieldContraints
        }
        

    
    private func getResendOTPLabelContraints() -> [NSLayoutConstraint]{
        let labelContraints = [resendOTPLabel.topAnchor.constraint(equalTo: continueToVerifyButton.bottomAnchor, constant: 10),
                           resendOTPLabel.heightAnchor.constraint(equalToConstant: 30),
                           resendOTPLabel.widthAnchor.constraint(equalToConstant: 100),
                        resendOTPLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ]
        return labelContraints
    }
    
    @objc func didTapContinueToVerify(){
        delegate?.didTapContinueToVerify()
    }
    
    
    @objc func didTapOTPkeyboardDoneBarButton(){
        delegate?.didTapOTPkeyboardDoneBarButton()
    }
    
   
    @objc func didTapResendOTP(){
       
        delegate?.didTapResendOTP()
    }
    
    
    @objc func didTapChangePhoneNumber(){
        delegate?.didTapChangePhoneNumber()
    }
    
    
    
    

}


protocol OTPFormViewDelegate : AnyObject{
     func didTapContinueToVerify()
    
    
    
     func didTapOTPkeyboardDoneBarButton()
    
    
   
     func didTapResendOTP()
    
    
     func didTapChangePhoneNumber()
    
}
