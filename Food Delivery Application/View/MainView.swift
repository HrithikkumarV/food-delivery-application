
//  MainView.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 11/11/21.
//

import Foundation
import UIKit
class MainView : UIView,MainViewProtocol{
    
    weak var delegate : MainViewDelegate?
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(){
        super.init(frame: .zero)
        
        self.backgroundColor = .white
        
        initialiseViewElements()
    }
    
    private func initialiseViewElements(){
        self.addSubview(userButton)
        self.addSubview(restaurantButton)
        NSLayoutConstraint.activate(getUserButtonContraints())
        NSLayoutConstraint.activate(getRestaurantButtonContraints())
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateButtonsFrame()
    }
    
    private func updateButtonsFrame(){
        let maxFrameLength =  getMaxFrameLength()
        userButton.removeHeightAnchorConstraints()
        userButton.removeWidthAnchorConstraints()
        userButton.widthAnchor.constraint(equalToConstant: maxFrameLength/4).isActive = true
        userButton.heightAnchor.constraint(equalToConstant: maxFrameLength/8).isActive = true
        restaurantButton.removeHeightAnchorConstraints()
        restaurantButton.removeWidthAnchorConstraints()
        restaurantButton.widthAnchor.constraint(equalToConstant: maxFrameLength/4).isActive = true
        restaurantButton.heightAnchor.constraint(equalToConstant: maxFrameLength/8).isActive = true
    }
    
    private func getMaxFrameLength() -> CGFloat{
        
        if(UIScreen.main.bounds.height > UIScreen.main.bounds.width){
            return UIScreen.main.bounds.height
        }
        else{
            return UIScreen.main.bounds.width
        }
        
    }
   
    
    private lazy var userButton : UIButton = {
        let userButton = UIButton()
        userButton.translatesAutoresizingMaskIntoConstraints = false
        userButton.setTitle("USER", for: .normal)
        userButton.setTitleColor(.white, for: .normal)
        userButton.backgroundColor = .systemBlue
        userButton.layer.cornerRadius = 10
        userButton.titleLabel?.adjustsFontSizeToFitWidth = true
        userButton.titleLabel?.font = UIFont(name: "ArialRoundedMTBold", size: 20)
        userButton.titleLabel?.sizeToFit()
        userButton.addTarget(self, action: #selector(didTapUserButton), for: .touchUpInside)
        return userButton
    }()
    
    private lazy var restaurantButton : UIButton = {
        let restaurantButton = UIButton()
        restaurantButton.translatesAutoresizingMaskIntoConstraints = false
        restaurantButton.setTitle("RESTAURANT", for: .normal)
        restaurantButton.setTitleColor(.white, for: .normal)
        restaurantButton.backgroundColor = .systemGreen
        restaurantButton.titleLabel?.adjustsFontSizeToFitWidth = true
        restaurantButton.layer.cornerRadius = 10
        restaurantButton.titleLabel?.font = UIFont(name: "ArialRoundedMTBold", size: 20)
        restaurantButton.titleLabel?.sizeToFit()
        restaurantButton.addTarget(self, action: #selector(didTapRestaurantButton), for: .touchUpInside)
        return restaurantButton
    }()
    
        
    private func getUserButtonContraints() -> [NSLayoutConstraint]{
        let userButtonConstraints = [
            userButton.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            userButton.bottomAnchor.constraint(equalTo: self.centerYAnchor,constant: -20),
            ]
        return userButtonConstraints
    }
    
    private func getRestaurantButtonContraints() -> [NSLayoutConstraint]{
        let restaurantButtonConstraints = [
            restaurantButton.centerXAnchor.constraint(equalTo: userButton.centerXAnchor),
            restaurantButton.topAnchor.constraint(equalTo:  userButton.bottomAnchor, constant: 20)
            ]
        return restaurantButtonConstraints
    }
    
    
    @objc func didTapUserButton(){
        delegate?.didTapUserButton()
    }
    @objc func didTapRestaurantButton(){
        delegate?.didTapRestaurantButton()
    }
    
}
    

    

protocol MainViewDelegate  : AnyObject{
    func didTapUserButton()
    func didTapRestaurantButton()
}
